//
//  main.m
//  Data Generation
//
//  Created by Anurag S Rathor on 11/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "DDFileReader.h"
#import "VehicleModel.h"
#import "CountryCode.h"
#import "CurrencyInfo.h"
#import "TutorialShownDB.h"
#import "OneSignalPlayerid.h"

static NSManagedObjectModel *managedObjectModel(NSString *dataModelPath)
{
    NSManagedObjectModel *model = nil;
    dataModelPath = [dataModelPath stringByDeletingPathExtension];
    NSURL *modelURL = [NSURL fileURLWithPath:[dataModelPath stringByAppendingPathExtension:@"momd"]];
    model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return model;
}

static NSManagedObjectContext *createManagedObjectContext(NSManagedObjectModel *dataModel,NSString *dataFileRelativePath)
{
    NSManagedObjectContext *context = nil;
    @autoreleasepool {
        context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:dataModel];
        context.persistentStoreCoordinator = coordinator;
        context.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy;
        NSString *STORE_TYPE = NSSQLiteStoreType;
        NSString *presentPath = [[NSProcessInfo processInfo] arguments][1];
        NSString * dataFilePath = [presentPath stringByAppendingPathComponent:dataFileRelativePath];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:dataFilePath]) {
            NSError *error;
            BOOL success = [fileManager removeItemAtPath:dataFilePath error:&error];
            if (!success) {
                NSLog(@"Error: %@", [error localizedDescription]);
            }
        }
        NSURL *url = [NSURL fileURLWithPath:dataFilePath];
        NSError *error;
        NSMutableDictionary *pragmaOptions = [NSMutableDictionary dictionary];
        [pragmaOptions setObject:@"DELETE" forKey:@"journal_mode"];
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, pragmaOptions, NSSQLitePragmasOption, nil];//iOS7 change
        NSPersistentStore *newStore = [coordinator addPersistentStoreWithType:STORE_TYPE configuration:nil URL:url options:options error:&error];
        if (newStore == nil) {
            NSLog(@"Store Configuration Failure %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
        }
    }
    return context;
}

static NSManagedObjectContext *managedObjectContext(NSString *dataModelPath,NSString *dataFileRelativePath){
    NSManagedObjectModel *dataModel = managedObjectModel(dataModelPath);
    NSManagedObjectContext *context = createManagedObjectContext(dataModel, dataFileRelativePath);
    return context;
}

static void createVehicleModels(NSManagedObjectContext *context){
    NSString *pathToVehicleModelList = [[[[NSProcessInfo processInfo] arguments][1] stringByAppendingPathComponent:@"Data Generation/Resources"] stringByAppendingPathComponent:@"VehicleModels"];
    DDFileReader * reader = [[DDFileReader alloc] initWithFilePath:pathToVehicleModelList];
    [reader enumerateLinesUsingBlock:^(NSString * line, BOOL * stop) {
        [VehicleModel createVehicleModel: [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] inContext:context];
    }];
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Error while saving %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
    }
}

static void createCountryCodes(NSManagedObjectContext *context){
    NSError *error;
    NSString *pathToCountryCodesJSON =  [[[[NSProcessInfo processInfo] arguments][1] stringByAppendingPathComponent:@"Data Generation/Resources"] stringByAppendingPathComponent:@"countryCodes.json"];
    NSData *countryCodesData = [NSData dataWithContentsOfFile:pathToCountryCodesJSON];
    NSArray *countryCodesArray = [NSJSONSerialization JSONObjectWithData:countryCodesData options:(NSJSONReadingMutableContainers) error:&error];
    if (error) {
        NSLog(@"Internal error %@", error);
    }
    else{
        [countryCodesArray enumerateObjectsUsingBlock:^(id obj,NSUInteger idx, BOOL *stop) {
            NSLog(@"obj is %@", obj);
            [CountryCode createCountryCodeWithISDCode:obj[@"isdCode"] andName:obj[@"countryName"] andISOCode:obj[@"isoCode"] andCurrency:obj[@"currency"] andDistanceUnit:obj[@"distanceUnit"] andDistanceSubUnit:obj[@"distanceSubUnit"]  inContext:context];
        }];
        [context save:&error];
        if (error) {
            NSLog(@"Internal error in creating country codes - %@", error);
        }
    }
}

static void createCurrencyCodes(NSManagedObjectContext *context){
    NSError *error;
    NSString *pathToCurrencyCodesJSON =  [[[[NSProcessInfo processInfo] arguments][1] stringByAppendingPathComponent:@"Data Generation/Resources"] stringByAppendingPathComponent:@"currencyCodes.json"];
    NSData *currecyCodesData = [NSData dataWithContentsOfFile:pathToCurrencyCodesJSON];
    NSArray *currencyCodesArray = [NSJSONSerialization JSONObjectWithData:currecyCodesData options:(NSJSONReadingMutableContainers) error:&error];
    if (error) {
        NSLog(@"Internal error %@", error);
    }
    else{
        [currencyCodesArray enumerateObjectsUsingBlock:^(id obj,NSUInteger idx, BOOL *stop) {
            [CurrencyInfo createCurrencyInfoWithCurrencyCode:obj[@"currencyCode"] andLocale:obj[@"locale"] andUnicode:obj[@"unicode"] andUseUnicode:[NSNumber numberWithInt:[obj[@"useUnicode"] intValue]]
            withIsoCode:obj[@"isoCode"] inContext:context];
        }];
        [context save:&error];
        if (error) {
            NSLog(@"Internal error in creating country codes - %@", error);
        }
    }
}
static void createTutorialsDB(NSManagedObjectContext *context){
    [TutorialShownDB insertTutorialIntoDB:@"0" andisInCreateRideShown:@"0" andisInPublishRideShown:@"0" inContext:context];
     //storeDefaultsValuesForAllInContext:context];
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Error while saving %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
    }
}

static void createOneSignalDB(NSManagedObjectContext *context){
    [OneSignalPlayerid insertOneSignalPlayerIdIntoDB:@"" inContext:context];
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Error while saving %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
    }
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSManagedObjectContext *context = managedObjectContext(@"ZifyDataModel",@"zifyResources/Database/Zify.sqlite");
        createVehicleModels(context);
        createCountryCodes(context);
        createCurrencyCodes(context);
         NSManagedObjectContext *userDatacontext = managedObjectContext(@"ZifyUserDataModel",@"zify/Resources/Database/ZifyUserData.sqlite");
        createTutorialsDB(userDatacontext);
        createOneSignalDB(userDatacontext)

    }
}
