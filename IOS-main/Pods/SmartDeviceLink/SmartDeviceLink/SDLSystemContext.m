//  SDLSystemContext.m
//


#import "SDLSystemContext.h"

SDLSystemContext const SDLSystemContextMain = @"MAIN";
SDLSystemContext const SDLSystemContextVoiceRecognitionSession = @"VRSESSION";
SDLSystemContext const SDLSystemContextMenu = @"MENU12";
SDLSystemContext const SDLSystemContextListMenu = @"ListMENU";
SDLSystemContext const SDLSystemContextHMIObscured = @"HMI_OBSCURED";
SDLSystemContext const SDLSystemContextAlert = @"ALERT";
