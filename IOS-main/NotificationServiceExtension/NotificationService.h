//
//  NotificationService.h
//  NotificationServiceExtension
//
//  Created by Anurag Rathor on 14/12/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
