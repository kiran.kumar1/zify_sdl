//
//  NotificationService.m
//  NotificationServiceExtension
//
//  Created by Anurag Rathor on 11/12/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "NotificationService.h"
#import "LocalisationConstants.h"
#import <UIKit/UIKit.h>


NSString * const RIDREQUESTNOTIFITICATION  = @"RIDEREQUEST";   //RIDEREQUEST
NSString * const RIDEREQUESTACCEPTACTION = @"RIDE_REQUEST_ACCEPT";
NSString * const RIDEREQUESTDECLINEACTION = @"RIDE_REQUEST_DECLINE";
#define PAYLOAD_KEY @"payload"
#define NOTIFICATION_TYPE_KEY @"notification_type"

#define TYPE_RIDE_REQUEST 1
#define TYPE_TRIP_NOTIFICATION 2
#define TYPE_APP_UPGRADE 3
#define TYPE_TRIP_COMPLETE_NOTIFICATION 4
#define TYPE_CHAT_NOTIFICATION 15

@interface NotificationService (){
    NSString *notificationTitle;
    NSString *notificationSubTitle;
    NSDateFormatter*  timeFormatter;
}

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    NSDictionary *userInfo = request.content.userInfo;
    NSLog(@"notifi is %@", userInfo);
    if (userInfo == nil) {
        [self contentComplete];
        return;
    }
    [self setNotificationTitleFromPushNotification:userInfo];
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
   // NSLog(@"Use this as an opportunity to deliver your best attempt at modified content, otherwise the original push payload will be used.");
    [self contentComplete];
    
}
- (void)contentComplete {
    self.contentHandler(self.bestAttemptContent);
}

- (NSString *)fileExtensionForMediaType:(NSString *)type {
    NSString *ext = type;
    if ([type isEqualToString:@"image"]) {
        ext = @"jpg";
    }
    
    if ([type isEqualToString:@"video"]) {
        ext = @"mp4";
    }
    if ([type isEqualToString:@"audio"]) {
        ext = @"mp3";
    }
    return [@"." stringByAppendingString:ext];
}

- (void)loadAttachmentForUrlString:(NSString *)urlString withType:(NSString *)type completionHandler:(void(^)(UNNotificationAttachment *))completionHandler  {
    
    __block UNNotificationAttachment *attachment = nil;
    NSURL *attachmentURL = [NSURL URLWithString:urlString];
    NSString *fileExt = [self fileExtensionForMediaType:type];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session downloadTaskWithURL:attachmentURL
                completionHandler:^(NSURL *temporaryFileLocation, NSURLResponse *response, NSError *error) {
                    if (error != nil) {
                        NSLog(@"error while download ==>%@", error.localizedDescription);
                    } else {
                        NSFileManager *fileManager = [NSFileManager defaultManager];
                        NSURL *localURL = [NSURL fileURLWithPath:[temporaryFileLocation.path stringByAppendingString:fileExt]];
                        [fileManager moveItemAtURL:temporaryFileLocation toURL:localURL error:&error];
                        
                        NSError *attachmentError = nil;
                        attachment = [UNNotificationAttachment attachmentWithIdentifier:@"" URL:localURL options:nil error:&attachmentError];
                        if (attachmentError) {
                            NSLog(@"attachmentError.localizedDescription =>%@", attachmentError.localizedDescription);
                        }
                    }
                    completionHandler(attachment);
                }] resume];
}


-(void)setNotificationTitleFromPushNotification:(NSDictionary *)customObject{
    notificationTitle = @"";
    notificationSubTitle = @"";
    NSDictionary *notificationDict = [[self.bestAttemptContent.userInfo objectForKey:@"custom"] objectForKey:@"a"];
    int notificationType = [notificationDict[NOTIFICATION_TYPE_KEY] intValue];
    int category = [notificationDict[@"category"] intValue];
    switch (notificationType){
        case TYPE_RIDE_REQUEST:{
            [self addActionButtonsForRideRequest];
            self.bestAttemptContent.categoryIdentifier = RIDREQUESTNOTIFITICATION;
            NSString *requestedUser = notificationDict[@"userDetails"][@"firstName"];
            NSString *source = notificationDict [@"tripDetails"][@"srcAddress"];
            NSString *destination = notificationDict[@"tripDetails"][@"destAddress"];
            NSString *atTime =[self getDateAndTimeBasedOnTimeZone:notificationDict[@"tripDetails"] [@"tripTime"]];
            NSString *title = NSLocalizedString(push_ridequest_title, nil);  //notificationDict[@"title"]
            NSString *subTitle = [NSString stringWithFormat:NSLocalizedString(push_ridequest_message, nil),requestedUser,source,destination,atTime];
            notificationTitle = title;
            notificationSubTitle = subTitle;
            
            break;
        }
        case TYPE_TRIP_NOTIFICATION:{
            if(category == 4){
                NSString *requestedUser = notificationDict[@"userDetails"][@"firstName"];
                NSString *atTime =[self getDateAndTimeBasedOnTimeZone:notificationDict[@"tripDetails"] [@"tripTime"]];
                notificationTitle = NSLocalizedString(push_riderequest_accepted_title, nil);
                notificationSubTitle = [NSString stringWithFormat:NSLocalizedString(push_riderequest_accepted_message, nil),requestedUser,atTime];
            }else if(category == 5){
                NSString *requestedUser = notificationDict[@"userDetails"][@"firstName"];
                notificationTitle = NSLocalizedString(push_riderequest_declined_title, nil);
                notificationSubTitle = [NSString stringWithFormat:NSLocalizedString(push_riderequest_declined_message, nil),requestedUser];
            }else if(category == 6){
                NSString *requestedUser = notificationDict[@"userDetails"][@"firstName"];
                notificationTitle = NSLocalizedString(push_ride_cancelled_title, nil);
                NSString *atTime =[self getDateAndTimeBasedOnTimeZone:notificationDict[@"tripDetails"] [@"tripTime"]];
                notificationSubTitle = [NSString stringWithFormat:NSLocalizedString(push_ride_cancelled_message, nil),requestedUser,atTime];
            }else if(category == 7){
                NSString *requestedUser = notificationDict[@"userDetails"][@"firstName"];
                notificationTitle = NSLocalizedString(push_drive_cancelled_title, nil);
                NSString *atTime =[self getDateAndTimeBasedOnTimeZone:notificationDict[@"tripDetails"] [@"tripTime"]];
                notificationSubTitle = [NSString stringWithFormat:NSLocalizedString(push_drive_cancelled_message, nil),requestedUser,atTime];
            }else if(category == 8){
                NSString *requestedUser = notificationDict[@"userDetails"][@"firstName"];
                notificationTitle = NSLocalizedString(push_drive_started_title, nil);
                notificationSubTitle = [NSString stringWithFormat:NSLocalizedString(push_drive_started_message, nil),requestedUser,requestedUser];
            }
            break;
        }
        case TYPE_APP_UPGRADE:{
            
            break;
        }
        case TYPE_TRIP_COMPLETE_NOTIFICATION:{
            if(category == 9){
                
                NSUserDefaults *prefs =[[NSUserDefaults alloc] initWithSuiteName:@"group.com.zify.extension"];
                NSString *currencyCode = [prefs objectForKey:@"userCurrencyCodeFromIsoCode"];
                if(currencyCode == nil){
                    currencyCode = @"INR";
                }
                NSString *requestedUser = notificationDict[@"userDetails"][@"firstName"];
                NSString *amouuntDoubleStr = notificationDict [@"tripDetails"][@"amount"];
                NSString *amount = [NSString stringWithFormat:@"%.2f",amouuntDoubleStr.floatValue];
                notificationTitle = NSLocalizedString(push_drive_completed_title, nil);
                notificationSubTitle = [NSString stringWithFormat:NSLocalizedString(push_ride_completed_message, nil),requestedUser,currencyCode,amount];
            }
        }
            break;
        default:{
                [self loadImageFromNotification:notificationDict];
                return;
            break;
        }
    }
    if(notificationTitle.length > 0){
        self.bestAttemptContent.title = notificationTitle;
    }
    if(notificationSubTitle.length > 0){
        self.bestAttemptContent.body = notificationSubTitle;
    }
    [self contentComplete];
    
}

-(NSString *)getDateAndTimeBasedOnTimeZone:(NSString *)dateInISoFormat{
    NSDateFormatter* dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
    NSDate *departureDateTime = [dateTimeFormatter1 dateFromString:dateInISoFormat];
    timeFormatter = [[NSDateFormatter alloc] init];
    NSUserDefaults *prefs =[[NSUserDefaults alloc] initWithSuiteName:@"group.com.zify.extension"];
    BOOL isTimein24 = [prefs boolForKey:@"24hoursFormatEnabled"];
    if(!isTimein24){
        [self setTimeIn24HoursFormat];
    }else{
        [self setTimeIn12HoursFormat];
    }
    return [NSString stringWithFormat:@"%@, %@",[dateFormatter1 stringFromDate:departureDateTime],[timeFormatter stringFromDate:departureDateTime]];
}


-(void)setTimeIn24HoursFormat{
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [timeFormatter setDateFormat:@"HH:mm"];
    [timeFormatter setLocale:locale];
}
-(void)setTimeIn12HoursFormat{
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [timeFormatter setDateFormat:@"hh:mm a"];
    [timeFormatter setLocale:locale];
}

-(void)loadImageFromNotification:(NSDictionary *)userInfo{
    NSString *mediaUrl = userInfo[@"image_url"];
    NSString *mediaType = userInfo[@"mediaType"];
    if (mediaUrl == nil || mediaType == nil) {
        [self contentComplete];
        return;
    }
    [self loadAttachmentForUrlString:mediaUrl
                            withType:mediaType
                   completionHandler:^(UNNotificationAttachment *attachment) {
                       if (attachment) {
                           self.bestAttemptContent.attachments = [NSArray arrayWithObject:attachment];
                       }
                       [self contentComplete];
                   }];
}
-(void)addActionButtonsForRideRequest{
    UNNotificationAction *rideRequestDeclineAction = [UNNotificationAction actionWithIdentifier:RIDEREQUESTDECLINEACTION title:NSLocalizedString(CMON_PNMANAGER_DECLINE, nil) options:UNNotificationActionOptionForeground];
    
    UNNotificationAction *rideRequestAcceptAction = [UNNotificationAction actionWithIdentifier:RIDEREQUESTACCEPTACTION title:NSLocalizedString(CMON_PNMANAGER_ACCEPT, nil) options:UNNotificationActionOptionForeground];
    UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:RIDREQUESTNOTIFITICATION actions:@[rideRequestDeclineAction,rideRequestAcceptAction] intentIdentifiers:@[] options:UNNotificationCategoryOptionNone];

    NSSet *categories = [NSSet setWithObject:category];
    [[UNUserNotificationCenter currentNotificationCenter] setNotificationCategories:categories];
}



@end

