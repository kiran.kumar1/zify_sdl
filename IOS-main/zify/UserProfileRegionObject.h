//
//  UserProfileRegionObject.h
//  zify
//
//  Created by Anurag Rathor on 12/07/19.
//  Copyright © 2019 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserProfileRegionObject : NSObject

@property (nonatomic,strong) NSString *countryCode;
@property (nonatomic,strong) NSString *countryName;
@property (nonatomic,strong) NSString *currency;
@property (nonatomic,strong) NSString *distanceUnit;
@property (nonatomic,strong) NSString *isdCode;
@property (nonatomic,strong) NSString *languageISOCode;
@property (nonatomic,strong) NSNumber *perKmRate;
@property (nonatomic,strong) NSString *timezone;


+(RKObjectMapping*)getUserProfileRegionObjectMapping;


@end


NS_ASSUME_NONNULL_END
