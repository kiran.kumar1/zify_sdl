//
//  ForgotPasswordController.swift
//  zify
//
//  Created by Anurag on 04/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class ForgotPasswordController: ScrollableContentViewController {
    
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var otpShadowView: UIView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var otp: TextFieldWithDone!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var constHeight_otpView: NSLayoutConstraint!
    @IBOutlet weak var btnResetPassword: UIButton!
    @IBOutlet weak var btnSendOTP: UIButton!
    
    var emailStr:String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        otpView.layer.borderColor = UIColor.lightGray.cgColor
        otpView.layer.borderWidth = 0.5
        
        if( emailStr.count > 0){
            self.email.text = emailStr;
        }
        
        /*otp.layer.borderColor = UIColor.lightGray.cgColor
        otp.layer.borderWidth = 0.5
        password.layer.borderColor = UIColor.lightGray.cgColor
        password.layer.borderWidth = 0.5
        confirmPassword.layer.borderColor = UIColor.lightGray.cgColor
        confirmPassword.layer.borderWidth = 0.5*/
        
        if AppDelegate.getInstance().emailStr != nil {
            if AppDelegate.getInstance().emailStr!.count > 0 {
                email.text = AppDelegate.getInstance().emailStr
            }
        }
        addLeftView(forTextField: email)
        addLeftView(forTextField: otp)
        addLeftView(forTextField: password )
        addLeftView(forTextField: confirmPassword)
        
        showShadow(view: otpView, color: .lightGray, opacity: 1, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
        
      /*  let maskPath = UIBezierPath(roundedRect: otpShadowView.bounds, byRoundingCorners: [.bottomRight , .bottomLeft], cornerRadii: CGSize(width: 10.0, height: 10.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = otpShadowView.bounds
        maskLayer.path = maskPath.cgPath
        otpShadowView.layer.mask = maskLayer*/
       // otpShadowView.backgroundColor = UIColor.red
        isEnableResetPwdBtnPwdTextFields(isenable: false)
        
        
       // roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect:otpShadowView.bounds,
                                byRoundingCorners:[.bottomRight],
                                cornerRadii: CGSize(width: 10, height:  10))
        
        let maskLayer = CAShapeLayer()
        otpShadowView.clipsToBounds = true
        maskLayer.path = path.cgPath
        if #available(iOS 11.0, *) {
            otpShadowView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        otpShadowView.layer.mask = maskLayer
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Actions
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: false)
    }
    
    @IBAction func sendOTP(_ sender: Any) {
        super.dismissKeyboard()
        if validateSendOTPRequest() {
            //PasswordRequestType.GETFORGOTPASSWORDOTP
            let changeRequest = PasswordChangeRequest(dUserEmail: email.text, andUserPassword: "", andOtp: "", andRequestType:GETFORGOTPASSWORDOTP)
            messageHandler.showBlockingLoadView(handler: {
                ServerInterface.sharedInstance().getResponse(changeRequest, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            self.isEnableResetPwdBtnPwdTextFields(isenable: true)
                            self.messageHandler.showSuccessMessage(NSLocalizedString(USS_CHANGEPASSWORD_OTP_SEND_SUCCESS, comment: ""))
                            self.btnSendOTP.setTitle(NSLocalizedString(UOS_VERIFYMOBILE_BTN_RESENDCODE, comment: ""), for: .normal)
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        }
                    })
                })
            })
        }
    }
    
    @IBAction func restPassword(_ sender: Any) {
        super.dismissKeyboard()
        if validateResetPasswordRequest() {
            let changeRequest = PasswordChangeRequest(dUserEmail: email.text, andUserPassword: password.text, andOtp: otp.text, andRequestType:SAVEFORGOTPASSWORD)
            messageHandler.showBlockingLoadView(handler: {
                ServerInterface.sharedInstance().getResponse(changeRequest, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            self.messageHandler.showSuccessMessage(NSLocalizedString(password_change_success, comment: ""))
                            self.dismiss(animated: true)
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        }
                    })
                })
            })
        }
    }
    
    func viewToAnimateTop() {
        otpView.isHidden = true
        self.constHeight_otpView.constant = 0
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
        }
    }
    
    func isEnableResetPwdBtnPwdTextFields(isenable : Bool) {
        if isenable == true {
            btnResetPassword.alpha = 1
            otp.alpha = 1
            password.alpha = 1
            confirmPassword.alpha = 1
        } else {
            btnResetPassword.alpha = 0.2
            otp.alpha = 0.2
            password.alpha = 0.2
            confirmPassword.alpha = 0.2
        }
        btnResetPassword.isEnabled = isenable
        otp.isEnabled = isenable
        password.isEnabled = isenable
        confirmPassword.isEnabled = isenable
    }
    
    
    func validateSendOTPRequest() -> Bool {
        if ("" == email.text) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_FORGOTPASSWORD_EMAILVAL, comment: ""))
            return false
        }
        if !validateEmail(email.text) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_FORGOTPASSWORD_EMAILVALIDVAL, comment: ""))
            return false
        }
        return true
    }
    
    func validateResetPasswordRequest() -> Bool {
        if otp.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_FORGOTPASSWORD_OTPVAL, comment: ""))
            return false
        }
        if password.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_FORGOTPASSWORD_PASSWORDVAL, comment: ""))
            return false
        }
        if (password.text?.count)! < 5 || (password.text?.count)! > 25 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_PASSWARD_MIN_LENGHT_VAL, comment: ""))
            return false
        }
        if confirmPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_FORGOTPASSSWORD_CONFIRMPASSWORDVAL, comment: ""))
            return false
        }
        if !(password.text == confirmPassword.text) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_FORGOTPASSWORD_PASSWORDMATCHVAL, comment: ""))
            return false
        }
        return true
    }
    
    func validateEmail(_ email: String?) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return predicate.evaluate(with: email)
    }
    
    func addLeftView(forTextField textField: UITextField?) {
        textField?.clipsToBounds = true
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        textField?.leftViewMode = UITextFieldViewMode.always
        textField?.leftView = view
    }
    
    // MARK:- Textfield Delegate
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == password {
            confirmPassword.becomeFirstResponder()
        }
        return true
    }
    
    
    
    /*
    enum PasswordReqType : Int {
        case GETFORGOTPASSWORDOTP
        case SAVEFORGOTPASSWORD
        case GETRESETPASSWORDOTP
        case SAVERESETPASSWORD
    }
    
    
    class PasswordChangeReq: ServerRequest {
        
        let OTP_PARAM = "otp"
        let USER_EMAIL_PARAM = "userEmail"
        let NEW_PASSWORD_PARAM = "newPswd"
        let ISO_CODE_PARAM = "isoCode"
        let COUNTRY_PARAM = "country"
        let USER_LAT_PARAM = "userLat"
        let USER_LONG_PARAM = "userLong"
        
        
        var userEmail = ""
        var userPassword = ""
        var otp = ""
        var requestType: PasswordReqType?
        
        init(dUserEmail userEmail: String?, andUserPassword userPassword: String?, andOtp otp: String?, andRequestType requestType: PasswordReqType) {
            super.init()
            
            self.userEmail = userEmail!
            self.userPassword = userPassword!
            self.otp = otp!
            self.requestType = requestType
            
        }
        
        override func urlparams() -> [AnyHashable : Any]? {
            var params: [AnyHashable : Any] = [:]
            let isoCode = CurrentLocale.sharedInstance().getMobileISOCode()
            switch requestType! {
            case PasswordReqType.SAVEFORGOTPASSWORD://SAVEFORGOTPASSWORD:
                params[OTP_PARAM] = otp
                params[NEW_PASSWORD_PARAM] = userPassword
            case PasswordReqType.GETFORGOTPASSWORDOTP:
                params[USER_EMAIL_PARAM] = userEmail
                if isoCode != "" {
                    params[ISO_CODE_PARAM] = isoCode
                } else {
                    let localityInfo: LocalityInfo? = CurrentLocationTracker.sharedInstance().currentLocalityInfo
                    if localityInfo != nil {
                        if let aCountry = localityInfo?.country == nil ? "" : localityInfo?.country {
                            params[COUNTRY_PARAM] = aCountry
                        }
                        params[USER_LAT_PARAM] = "\(localityInfo?.latLng.latitude ?? 0.0)"
                        params[USER_LONG_PARAM] = "\(localityInfo?.latLng.longitude ?? 0.0)"
                    }
                }
            case PasswordReqType.GETRESETPASSWORDOTP:
                params = getUserCommonParams() as! [AnyHashable : Any]
                params[USER_EMAIL_PARAM] = userEmail
            case PasswordReqType.SAVERESETPASSWORD:
                params = getUserCommonParams() as! [AnyHashable : Any]
                params[OTP_PARAM] = otp
                params[NEW_PASSWORD_PARAM] = userPassword
            default:
                break
            }
            return params
            
        }
        
        
        override func serverURL() -> String? {
            switch requestType! {
            case PasswordReqType.GETFORGOTPASSWORDOTP:
                return "/user/getForgotPswdOTP.htm"
            case PasswordReqType.SAVEFORGOTPASSWORD:
                return "/user/saveForgotPswd.htm"
            case PasswordReqType.GETRESETPASSWORDOTP:
                return "/user/getForgotPswdOTP.htm" //@"/user/getResetPswdOTP.htm";
            case PasswordReqType.SAVERESETPASSWORD:
                return "/user/saveResetPswd.htm"
            default:
                return nil
            }
        }
        
    }
 */
}
