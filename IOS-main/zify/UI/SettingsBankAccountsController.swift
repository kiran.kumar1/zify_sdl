//
//  SettingsBankAccountsController.swift
//  zify
//
//  Created by Anurag on 05/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class SettingsBankAccountsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var bankAccountsView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addBankAccountButton: UIButton!
    @IBOutlet weak var noAccountsView: UIView!
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var lblAddedAccountsText: UILabel!

    var bankAccounts: [AnyObject] = []
    var isGlobalLocale = false
    var cellbankAccountDetails: BankAccountDetailsCellSwift?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIScreen.main.sizeType == .iPhone5 {
            lblAddedAccountsText.fontSize = 14
            cellbankAccountDetails?.bankName.fontSize = 12
            cellbankAccountDetails?.accountNumber?.fontSize = 14
            cellbankAccountDetails?.ifscCode.fontSize = 12
            cellbankAccountDetails?.lblAccountNoText.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6 {
            lblAddedAccountsText.fontSize = 14
            cellbankAccountDetails?.bankName.fontSize = 13
            cellbankAccountDetails?.accountNumber?.fontSize = 15
            cellbankAccountDetails?.ifscCode.fontSize = 13
            cellbankAccountDetails?.lblAccountNoText.fontSize = 15
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            lblAddedAccountsText.fontSize = 16
            cellbankAccountDetails?.bankName.fontSize = 14
            cellbankAccountDetails?.accountNumber?.fontSize = 16
            cellbankAccountDetails?.ifscCode.fontSize = 14
            cellbankAccountDetails?.lblAccountNoText.fontSize = 16
        }
        
        bankAccounts = []
        bankAccountsView.isHidden = true
        noAccountsView.isHidden = true
        let currentLocale = CurrentLocale.sharedInstance()
        //isGlobalLocale = currentLocale?.isGlobal()
        isGlobalLocale = (currentLocale?.isGlobalLocale())!
        tableView.estimatedRowHeight = 140.0
        tableView.rowHeight = UITableViewAutomaticDimension

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bankAccountsView.isHidden = true
        noAccountsView.isHidden = true
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(BankAccountRequest(requestType: SAVEDBANKDETAILS), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        if let anObject = response?.responseObject as? [Any] {
                            self.bankAccounts = anObject as [AnyObject]
                        }
                        self.handleBankAccountsResponse()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        messageHandler.dismissErrorView()
        messageHandler.dismissSuccessView()
    }
    
    // MARK:- TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if bankAccounts.count == 0 {
            return 0
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if bankAccounts.count == 1 && indexPath.section == 1 {
            let addBankAccountCell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "addBankAccount")
            return addBankAccountCell!
        }
        let bankAccountDetails = bankAccounts[indexPath.section] as? BankAccountDetails
        cellbankAccountDetails = self.tableView.dequeueReusableCell(withIdentifier: "BankAccountDetailsCellSwift") as? BankAccountDetailsCellSwift
        cellbankAccountDetails?.bankName.text = bankAccountDetails?.bankName.uppercased()
        if isGlobalLocale {
            cellbankAccountDetails?.lblAccountNoText.text = NSLocalizedString(VC_SETTINGSBANKACCOUNT_IBANNUMBER, comment: "")
            cellbankAccountDetails?.accountNumber?.text = bankAccountDetails?.ifscCode

            cellbankAccountDetails?.ifscCodeLabel.isHidden = true
            cellbankAccountDetails?.ifscCode.isHidden = true

        } else {
            cellbankAccountDetails?.ifscCodeLabel.text = NSLocalizedString(VC_SETTINGSBANKACCOUNT_IFSCCODE, comment: "")
            cellbankAccountDetails?.accountNumber?.text = bankAccountDetails?.accountNumber
            cellbankAccountDetails?.ifscCode.text = bankAccountDetails?.ifscCode
        }
        return cellbankAccountDetails!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bankAccounts.count == 1 && indexPath.section == 1 {
            performSegue(withIdentifier: "addBankAccount", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }

    // MARK:- Actions
    @IBAction func dismiss(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func addBankAccount(_ sender: UIButton) {
        performSegue(withIdentifier: "addBankAccount", sender: sender)
    }
    
    @IBAction func deleteAccount(_ sender: UIButton) {
        var indexPath: IndexPath? = nil
        if let aSender = getTableViewCell(sender) {
            indexPath = tableView.indexPath(for: aSender)
        }
        let bankDetails = bankAccounts[indexPath?.section ?? 0] as? BankAccountDetails
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(BankAccountRequest(requestType: DELETEBANK, andBankAccountDetaiId: bankDetails?.bankDetailId, andRedeemAmount: nil), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.bankAccounts.remove(at: indexPath?.section ?? 0)
                        self.handleBankAccountsResponse()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    func handleBankAccountsResponse() {
        if bankAccounts.count == 0 {
            bankAccountsView.isHidden = true
            noAccountsView.isHidden = false
        } else {
            bankAccountsView.isHidden = false
            noAccountsView.isHidden = true
            tableView.reloadData()
        }
    }
    /*
     -(UITableViewCell*)getTableViewCell:(id)button{
     while(![button isKindOfClass:[UITableViewCell class]]){
     button=[button superview];
     }
     return (UITableViewCell*)button;
     }
     */
    func getTableViewCell(_ button: AnyObject) -> UITableViewCell? {
        var btn : AnyObject = button
        while !(btn is UITableViewCell) {
            //btn = (btn as AnyObject).superview()
            btn = btn.superview as AnyObject
        }
        return btn as? UITableViewCell
    }

}
class BankAccountDetailsCellSwift : UITableViewCell {
    @IBOutlet weak var bankSelectButton: UIButton!
    @IBOutlet weak var bankName: UILabel!
    @IBOutlet weak var accountNumber: UILabel!
    @IBOutlet weak var ifscCodeLabel: UILabel!
    @IBOutlet weak var ifscCode: UILabel!
    @IBOutlet weak var lblAccountNoText: UILabel!
}






