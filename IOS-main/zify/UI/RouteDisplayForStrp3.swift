//
//  RouteDisplayForStrp3.swift
//  zify
//
//  Created by Anurag S Rathor on 21/11/17.
//  Copyright © 2017 zify. All rights reserved.
//

import UIKit

class RouteDisplayForStrp3: UIViewController {
    
    @IBOutlet var messageHandler:MessageHandler! = MessageHandler()
    
    var isFromHomeToOffice:Bool = Bool()
    var driveRoutes:NSArray = NSArray()
    var routesCount:Int = Int()
    var currentRouteIndex:Int = Int()
    var presentRoute:DriveRoute = DriveRoute()
    
    
   // var backward:UIButton = UIButton()
   // var forward:UIButton = UIButton()
  //  var doneBtn:UIButton = UIButton()
  //  var distanceLbl:UILabel = UILabel()
   // var routeLbl:UILabel = UILabel()

    
   @IBOutlet weak var mapContainerView:MapContainerView!
    @IBOutlet weak var backward:UIButton!
    @IBOutlet weak var distanceLbl:UILabel!
    @IBOutlet weak var routeLbl:UILabel!
    @IBOutlet weak var forward:UIButton!
    @IBOutlet weak var doneBtn:UIButton!
    @IBOutlet weak var driveInfoView:UIView!
    @IBOutlet weak var bottomView:UIView!


    var isViewInitialised:Bool = false

  /*  -(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.driveInfoView.hidden = true;
    [self.publish setUserInteractionEnabled:YES];
    }
    
    -(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised) [self initialiseView];
    else self.driveInfoView.hidden = false;
    }
*/
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        drawShadow(toAView: self.bottomView, with: UIColor(red: 186.0 / 255.0, green: 184.0 / 255.0, blue: 184.0 / 255.0, alpha: 0.8), withCornerRadius: 12, withShadowRadius: 5, withOffset: CGSize(width: 0, height: -3))
        self.driveInfoView.isHidden = true;

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(!isViewInitialised) {
            self.initialiseView()
        }else{
            self.driveInfoView.isHidden = false;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isViewInitialised = false;

        self.view.backgroundColor = UIColor.white
        navigationController?.isNavigationBarHidden = false
        self.getRoutesFromSourceToDestination()
        self.navigationItem.title = NSLocalizedString(VC_PUBLISHRIDE_SELECTROUTE, comment: "")
        navigationController?.navigationBar.barTintColor = UIColor.init(hex: "2662FF")
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.addBarButtonItem()
    }
    
    func initialiseView() -> Void {
  
    }
    
  
    func showRoutesOnMap() -> Void {
        presentRoute = self.driveRoutes.object(at: 0) as! DriveRoute
        routesCount = self.driveRoutes.count
        currentRouteIndex = 1
        
        self.mapContainerView.createMapView()
        let mapView = self.mapContainerView.mapView!
        mapView.sourceCoordinate = CLLocationCoordinate2DMake(Double(presentRoute.srcLat)!, Double(presentRoute.srcLong)!)

        mapView.destCoordinate = CLLocationCoordinate2DMake(Double(presentRoute.destLat)!,Double(presentRoute.destLong)!);
        mapView.overviewPolylinePoints = presentRoute.overviewPolylinePoints;
        mapView.mapPadding = UIEdgeInsets(top: 70, left: 0, bottom: driveInfoView.frame.height, right: 0)//UIEdgeInsetsMake(70, 0, CGRectGetHeight(driveInfoView.frame), 0);
        driveInfoView.isHidden = false;
        mapView.drawMap()
        self.redrawMapPath()
        
        
        self.addFontsForControls()
        self.backward.isEnabled = false
        if(routesCount == 1){self.forward.isEnabled = false}
        else{self.forward.isEnabled = true}
    }
    
 
    func drawShadow(toAView viewShadow: UIView?, with color: UIColor?, withCornerRadius radius: Int, withShadowRadius shadowRadius: Int, withOffset size: CGSize) {
        viewShadow?.backgroundColor = UIColor.white
        viewShadow?.layer.shadowOpacity = 1
        viewShadow?.layer.shadowOffset = size
        viewShadow?.layer.shadowRadius = CGFloat(shadowRadius)
        viewShadow?.layer.masksToBounds = false
        viewShadow?.layer.cornerRadius = CGFloat(radius)
        viewShadow?.layer.shadowColor = color?.cgColor
        
    }
    
    func addFontsForControls() -> Void {
        let titleFont:CGFloat = 14;
        distanceLbl.font = UIFont.init(name: NormalFont, size: titleFont)
        routeLbl.font = UIFont.init(name: MediumFont, size: titleFont)
        doneBtn.titleLabel?.font = UIFont.init(name: MediumFont, size: 20)
    }
   @IBAction func moveForward() -> Void {
        currentRouteIndex = currentRouteIndex + 1
        backward.isEnabled = true
        if(currentRouteIndex == routesCount){forward.isEnabled = false}
        self.redrawMapPath()
    }
    
    @IBAction func moveBackward() -> Void {
        currentRouteIndex = currentRouteIndex - 1
        forward.isEnabled = true
        if(currentRouteIndex == 1){backward.isEnabled = false}
        self.redrawMapPath()
    }
    func displayTextOnView() -> Void {
        routeLbl.text = String(format: "%@ %d/%d", NSLocalizedString(VC_PUBLISHRIDE_ROUTELBL, comment: ""),currentRouteIndex,routesCount)
        distanceLbl.text = String(format: "%.2f %@", Float(presentRoute.distance)!,CurrentLocale.sharedInstance().getDistanceUnit())
    }
    
    func redrawMapPath() -> Void {
        presentRoute = driveRoutes.object(at: currentRouteIndex - 1) as! DriveRoute
        self.displayTextOnView()
        mapContainerView.mapView.overviewPolylinePoints = presentRoute.overviewPolylinePoints
        mapContainerView.mapView.drawPath(for: Int32(currentRouteIndex - 1))
    }
    @IBAction func doneBtnTapped(sender:UIButton!) -> Void {
        //*,*oFFToHomeDriveRoute;
        
        if(isFromHomeToOffice){
            AppDelegate.getInstance().homeToOffDriveRoute = presentRoute
        }else{
            AppDelegate.getInstance().oFFToHomeDriveRoute = presentRoute
        }
//        self.dismiss(animated: true) {
//        }
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func dismiss(sender:UIButton!){
//        self.dismiss(animated: true) {
//        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func getRoutesFromSourceToDestination() -> Void {
        self.messageHandler.showBlockingLoadView {
            let source:LocalityInfo?
            let dest:LocalityInfo?
            
            if(self.isFromHomeToOffice){
                source = AppDelegate.getInstance().homeAddress;
                dest = AppDelegate.getInstance().officeAddress;
            }else{
                source = AppDelegate.getInstance().officeAddress;
                dest = AppDelegate.getInstance().homeAddress;
            }
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let userRideReq:UserOfferRideRequest  = UserOfferRideRequest.init(sourceLocality: source, andDestinationLocality: dest, andRideDate: "", andSeats: 1, andMerchantId: "") as UserOfferRideRequest
            //appDelegate.makeRequest(withSourceLocality: source, andDestinationLocality: dest, andRideDate: "", andSeats: 1, andMerchantId: "")
            //UserOfferRideRequest(sourceLocality: source, andDestinationLocality: dest, andRideDate: "", andSeats: 1, andMerchantId: "") as UserOfferRideRequest
            //UserOfferRideRequest.init(SourceLocality: source, andDestinationLocality: dest, andRideDate: "", andSeats:1, andMerchantId: "") as UserOfferRideRequest
            
            ServerInterface.sharedInstance().getResponse(userRideReq, withHandler: { (response:ServerResponse?, error) in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if(response != nil){
                        let routesArr = response?.responseObject as! NSArray
                        if(routesArr.count == 0) {
                            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SEARCHRIDE_NODRIVESMESSAGE, comment: ""))
                        } else {
                            print(routesArr);
                            self.driveRoutes = routesArr;
                            self.showRoutesOnMap()
                        }
                    } else {
                    self.doneBtn.isHidden = true
                     self.messageHandler.showErrorMessage(NSLocalizedString(CMON_GENERIC_INTERNALERROR, comment: ""))
                    }
                })
            })
        }
    }
    
    func addBarButtonItem(){
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "left_arrow.png"), for: .normal)
        btn1.setImage(UIImage(named: "left_arrow.png"), for: .selected)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.init(hex: "2662FF")
        
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
