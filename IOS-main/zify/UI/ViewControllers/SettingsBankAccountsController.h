//
//  SettingsBankAccountsController.h
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"

@interface SettingsBankAccountsController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UIView *bankAccountsView;
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIButton *addBankAccountButton;
@property (nonatomic,weak) IBOutlet UIView *noAccountsView;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (weak, nonatomic) IBOutlet UILabel *lblAddedAccountsText;


@end
