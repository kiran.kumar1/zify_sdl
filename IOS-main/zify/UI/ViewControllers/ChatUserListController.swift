//
//  ChatUserListController.swift
//  zify
//
//  Created by Anurag Rathor on 07/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class ChatUserListController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var receivingUser: UserContactDetail?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noChatView: UIView!
    @IBOutlet weak var messageHandler: MessageHandler!
    
    var MessagesDict = [String: Message]()
    var messages = [Message]()

    
    var contactsRef:DatabaseReference!

   
    var profileImagePlaceHolder: UIImage?
  //  var chatUserList: NSMutableArray = NSMutableArray.init()
  //  var chatUserList : NSMutableArray = NSMutableArray.init()

    var chatUserList = [LastMessage]()
  //  var messages = [Message]()

    var dateTimeFormatter: DateFormatter?
    var selectedChatReceiver: UserContactDetail?
    var selectedChatUser: QBChatUser?
    var createdDialog: QBChatDialog?
    var isNewChatUser = false
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        contactsRef?.removeAllObservers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let uid = Auth.auth().currentUser?.uid {
            self.contactsRef = Database.database().reference().child("Contacts").child(uid)
        }
        //chatUserList = NSArray.init() as? Array<LastMessage>
        profileImagePlaceHolder = UIImage(named: "placeholder_profile_image")
        dateTimeFormatter = DateFormatter()
        dateTimeFormatter?.dateFormat = "dd MMM, HH:mm"
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
   /* func moveToIndividualChatScreenOnPush(){
        AppDelegate.getInstance().chatuserIdFromPush = ""
        return
        if(AppDelegate.getInstance().chatuserIdFromPush.count > 0){
            print("chatuserIdFromPush is \(String(describing: AppDelegate.getInstance().chatuserIdFromPush))")
            let chatUser = QBChatUser.getWithIdentifier(AppDelegate.getInstance().chatuserIdFromPush ?? "", in: DBUserDataInterface.sharedInstance().sharedContext)
            if(chatUser != nil){
            selectedChatUser = chatUser
            selectedChatReceiver = UserContactDetail(chatIdentifier: chatUser?.chatIdentifier, andCallIdentifier: nil, andFirstName: chatUser?.firstName, andLastName: chatUser?.lastName, andImageUrl: chatUser?.imageUrl)
            createUserChatDialog()
            }
            AppDelegate.getInstance().chatuserIdFromPush = ""
        }
    }*/
    
   override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.isHidden = false
        noChatView.isHidden = true
    if (Auth.auth().currentUser?.uid) != nil {
        fetchChatUserList()
    }
    if (receivingUser != nil) {
            selectedChatReceiver = receivingUser
            createUserChatDialog()
            receivingUser = nil
        }
    }
    
  /*  func fetchChatUserList() {
        messageHandler.showBlockingLoadView(handler: {
            FirebaseChatClass.sharedInstance.getChatHistoryBasedOnUserId { dataArray in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    self.chatUserList = dataArray as NSArray as? Array<LastMessage>
                    if self.chatUserList?.count == 0 {
                        self.tableView.isHidden = true
                        self.noChatView.isHidden = false
                    } else {
                        self.tableView.isHidden = false
                        self.noChatView.isHidden = true
                        self.tableView.reloadData()
                    }
                })
            }
        })
        /*   chatUserList = ChatManager.sharedInstance().getChatUserList()! as NSArray
         if chatUserList?.count == 0 {
         tableView.isHidden = true
         noChatView.isHidden = false
         } else {
         tableView.isHidden = false
         noChatView.isHidden = true
         tableView.reloadData()
         }*/
        
    }*/
    
    
    func fetchChatUserList() {

        guard (Auth.auth().currentUser?.uid) != nil else {
            return
        }
        
        
        
        messageHandler.showBlockingLoadView(handler: {

        
        self.contactsRef.observe(.value, with: { (snapshot) in
           // print("snapshot is===== \(snapshot)")
          //  self.chatUserList = NSMutableArray.init()
            self.chatUserList = [LastMessage]()
          //  print("children are \(snapshot.children)")
                for child in snapshot.children{
                    let childObj = child as? DataSnapshot
               //     print("child obj = \(childObj?.value)")
                    guard let datavalue = childObj?.value as? [String: AnyObject] else{
                        return
                    }
                    let messageDict = datavalue["LastActivity"] as! [String : AnyObject]
                    let message = LastMessage()
                    let personIdKey = childObj?.key ?? ""
                    message.id = personIdKey
                    message.setValuesForKeys(messageDict)
                    self.chatUserList.append(message)

                  /*  let isAvailable = self.checkIfPersonIsalreadyExistsInArray(withPersonId: personIdKey)
                    if(isAvailable){
                        let index = self.getIndexOfPersonIfExists(withPersonId: personIdKey)
                        if(index != -1 && index < self.chatUserList.count){
                            self.chatUserList.removeObject(at: index)
                        }
                        self.chatUserList.insert(message, at: 0)
                    }else{
                        self.chatUserList.add(message)
                    }*/
                }
            
            self.messageHandler.dismissBlockingLoadView(handler: {
            self.handelReloadTable()
            })

            
            

            
          /*  let userId = snapshot.key
            Database.database().reference().child("Contacts").child(uid).child(userId).observe(.childAdded, with: { (mSnapshot) in
                guard let dict = mSnapshot.value as? [String: AnyObject] else{
                    return
                }
                print("last activity snapshot is \(mSnapshot)")
             
             
             
                let messageDict = dict["LastActivity"] as! [String : AnyObject]
                let message = LastMessage()
                message.id = snapshot.key
                message.setValuesForKeys(messageDict)
                self.chatUserList.add(message)
                self.handelReloadTable()
             
            }, withCancel: nil)*/
        }, withCancel: nil)
        
       })
    }
        
    
    
    func checkIfPersonIsalreadyExistsInArray(withPersonId:String) -> Bool{
        for massage in self.chatUserList{
            let lastmessage = massage as? LastMessage
            if(lastmessage?.id == withPersonId){
                return  true
            }
        }
        return false
    }
    
    func getIndexOfPersonIfExists(withPersonId:String) -> Int{
           var indexOfperson = -1
            for index in 0..<self.chatUserList.count{
                let lastmessage = self.chatUserList[index] as? LastMessage
                if(lastmessage?.id == withPersonId){
                    indexOfperson = index
                    return indexOfperson
                    
                }
            }
            return indexOfperson
        
    }
    func reArrangeTheItemsInArrayAfterModifyingArray(personID:String){
        let isPersonAvailable = self.checkIfPersonIsalreadyExistsInArray(withPersonId: personID)
        if(isPersonAvailable){
            
        }
    }
    
    
    func handelReloadTable() {
        // this will crash because of background thread , so lets use dispatch_thread
        DispatchQueue.main.async {
            
            
            self.chatUserList =  self.chatUserList.sorted(by: { (message1, message2) -> Bool in
                return Date.init(timeIntervalSince1970: TimeInterval(message1.dateStamp!.int64Value)) > Date.init(timeIntervalSince1970: TimeInterval(message2.dateStamp!.int64Value))
            })
            
            
            self.tableView.reloadData()
            if self.chatUserList.count == 0 {
                self.tableView.isHidden = true
                self.noChatView.isHidden = false
            } else {
                self.tableView.isHidden = false
                self.noChatView.isHidden = true
                self.tableView.reloadData()
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userDetailCell:ChatUserDetailCell = (tableView.dequeueReusableCell(withIdentifier: "chatUserDetailCell") as? ChatUserDetailCell)!
        let lastMessage = chatUserList[indexPath.row] as? LastMessage
        
        
        
        userDetailCell.userImage.layer.cornerRadius = userDetailCell.userImage.frame.size.width/2
        userDetailCell.userImage.clipsToBounds = true
        userDetailCell.userImage.image = profileImagePlaceHolder
        let imageUrl = lastMessage?.profileImageurl ?? ""
        //print("url is \(imageUrl)")
        if(imageUrl.count > 0){
          //  print("url is \(imageUrl)")
            let url =  (URL.init(string: imageUrl))!
            userDetailCell.userImage.sd_setImage(with: url, placeholderImage: profileImagePlaceHolder, options: []) { (image, error, cacheType, newUrl) in
            }
        }
        
        if let fName = lastMessage?.userFirstname, let lName = lastMessage?.userLastname {
            let nameStr:String = "\(fName) \(lName)"
            userDetailCell.userName.text = nameStr.capitalized
        }
        if let lastMsg = lastMessage?.lastMessage {
            userDetailCell.lastMessage.text = "\(lastMsg)"
        }
        let unreadCount:Int = Int(lastMessage?.unreadCount ?? 0)
        if(unreadCount == 0){
            userDetailCell.unreadCount.isHidden = true
        }else{
            userDetailCell.unreadCount.isHidden = false
            userDetailCell.unreadCount.text = "\(unreadCount)"
        }
        if let seconds = lastMessage?.dateStamp?.int64Value {
          //  let timesnapDate = Date.init(timeIntervalSince1970:TimeInterval(seconds))
            
            let timesnapDate = Date.init(timeIntervalSince1970: TimeInterval(Int64(seconds)/1000))
        //    messageTime = timesnapDate
            let timeStr = dateTimeFormatter?.string(from: timesnapDate as Date) ?? ""
            //  let timeStr = (dateTimeFormatter?.string(from: lastMsgTime.stringValue as! Date))!
            userDetailCell.lastMessageDate.text =  timeStr  //"\(timeStr)"
        }
              
        
        
        
      //  let managedObject = (chatUserList![indexPath.row] as? NSManagedObject)!
        //userDetailCell.userImage.isHidden = true
        
        /*
        userDetailCell.userImage.layer.cornerRadius = userDetailCell.userImage.frame.size.width/2
        userDetailCell.userImage.clipsToBounds = true
        userDetailCell.userImage.image = profileImagePlaceHolder
        let imageUrl = (managedObject.value(forKey: "imageUrl") as? String) ?? ""
        print("url is \(imageUrl)")
        if(imageUrl.count > 0){
            print("url is \(imageUrl)")
        let url =  (URL.init(string: imageUrl))!
        userDetailCell.userImage.sd_setImage(with: url, placeholderImage: profileImagePlaceHolder, options: []) { (image, error, cacheType, newUrl) in
              }
        }
        
         if let fName = managedObject.value(forKey: "firstName"), let lName = managedObject.value(forKey: "lastName") {
            let nameStr:String = "\(fName) \(lName)"
            userDetailCell.userName.text = nameStr.capitalized
        }
        if let lastMsg = managedObject.value(forKey: "lastMessage") {
            userDetailCell.lastMessage.text = "\(lastMsg)"
        }
        let unreadCount:Int = (managedObject.value(forKey: "unreadCount") as? Int)!
        if(unreadCount == 0){
                userDetailCell.unreadCount.isHidden = true
            }else{
                userDetailCell.unreadCount.isHidden = false
                userDetailCell.unreadCount.text = "\(unreadCount)"
            }
        if let lastMsgTime = managedObject.value(forKey: "lastMessageTime") {
            let timeStr = (dateTimeFormatter?.string(from: lastMsgTime as! Date))!
            userDetailCell.lastMessageDate.text = "\(timeStr)"
        }
        if(indexPath.row == chatUserList!.count - 1){
            self.moveToIndividualChatScreenOnPush()
        }*/
        return userDetailCell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatUserList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let lastMessage = chatUserList[indexPath.row] as? LastMessage
        selectedChatReceiver = UserContactDetail(chatIdentifier: lastMessage?.id, andCallIdentifier: nil, andFirstName: lastMessage?.userFirstname, andLastName: lastMessage?.lastMessage, andImageUrl: lastMessage?.profileImageurl)
        createUserChatDialog()
      /*  let obj = chatUserList![indexPath.row] as? NSManagedObject
        let identifier = obj?.value(forKey: "chatIdentifier")
        let chatUser = QBChatUser.getWithIdentifier(identifier as? String, in: DBUserDataInterface.sharedInstance().sharedContext) //DBUserDataInterface.hsharedInstance] sharedContext];)
        selectedChatUser = chatUser
        selectedChatReceiver = UserContactDetail(chatIdentifier: chatUser?.chatIdentifier, andCallIdentifier: nil, andFirstName: chatUser?.firstName, andLastName: chatUser?.lastName, andImageUrl: chatUser?.imageUrl)
        createUserChatDialog()*/
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }


    @IBAction func refreshChatUserList(_ sender: Any) {
        // [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(fetchChatUserList) userInfo:nil repeats:YES];
        fetchChatUserList()
    }
    
    func moveToONe(toOneChatWindow chatUser: QBChatUser?) {
        selectedChatUser = chatUser
        selectedChatReceiver = UserContactDetail(chatIdentifier: chatUser?.chatIdentifier, andCallIdentifier: nil, andFirstName: chatUser?.firstName, andLastName: chatUser?.lastName, andImageUrl: chatUser?.imageUrl)
        createUserChatDialog()
    }
    
    func createUserChatDialog() {
        
        if (self.selectedChatReceiver != nil) {
            self.performSegue(withIdentifier: "showChatMessage", sender: self)
        }
        return
        
        messageHandler.showBlockingLoadView(handler: {
            if (self.selectedChatReceiver != nil) {
                self.selectedChatUser = self.chatManager()?.getChatUser(forIdentifier: self.selectedChatReceiver?.chatIdentifier)
            }
            var convId: String = ""
            var isNewUser = true
            if (self.selectedChatUser != nil) {
                convId = (self.selectedChatUser?.dialogId)!
                isNewUser = false
                self.chatManager()?.resetMessageUnreadCount(for: self.selectedChatUser)
            }
            self.chatManager()?.createDialog(withConvId: convId, andChatIdentifier: self.selectedChatReceiver!.chatIdentifier, withHandler: { dialog in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if dialog != nil {
                        self.isNewChatUser = isNewUser
                        self.createdDialog = dialog
                        self.performSegue(withIdentifier: "showChatMessage", sender: self)
                    }
                })
            })
        })
    }

    class func createChatNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        let chatNavController = storyBoard.instantiateViewController(withIdentifier: "chatNavController") as? UINavigationController
        return chatNavController
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showChatMessage") {
            let messagesController:ChatMessagesController = segue.destination as! ChatMessagesController
            messagesController.receivingUser = selectedChatReceiver
            messagesController.isNewUser = isNewChatUser
            messagesController.chatDialog = createdDialog
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func chatManager() -> ChatManager? {
        return ChatManager.sharedInstance()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
