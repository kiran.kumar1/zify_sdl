//
//  AboutController.h
//  zify
//
//  Created by Anurag S Rathor on 26/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import  <MessageUI/MFMailComposeViewController.h>

@interface AboutController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>
@property (nonatomic,strong) IBOutlet UILabel *versionNumber;
@property (nonatomic,strong) IBOutlet UITableView *tableView;
+(UINavigationController *)createAboutNavController;
@end
