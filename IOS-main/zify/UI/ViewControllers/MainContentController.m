//
//  MainContentController.m
//  zify
//
//  Created by Anurag S Rathor on 27/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "MainContentController.h"

@implementation MainContentController

-(void) viewDidLoad{
    [self performSegueWithIdentifier:@"mainContentNavSegue" sender:self];
}

#pragma mark-
#pragma mark Setting Main ViewController
#pragma mark-

-(void)setMainContentNavController:(UIViewController *)mainNavViewController{
    [self.mainContentNavController removeFromParentViewController];
    if (mainNavViewController) {
        [self addChildViewController:mainNavViewController];
    }
    [self.view addSubview:mainNavViewController.view];
    _mainContentNavController = mainNavViewController;
}

+(MainContentController *)createMainContentController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    MainContentController *mainContentController = [storyBoard instantiateViewControllerWithIdentifier:@"mainContentController"];
    return mainContentController;
}

+(MainContentController *)createMainContentGuestController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"GuestUser" bundle:[NSBundle mainBundle]];
    MainContentController *mainContentController = [storyBoard instantiateViewControllerWithIdentifier:@"mainContentController"];
    return mainContentController;
}
@end
