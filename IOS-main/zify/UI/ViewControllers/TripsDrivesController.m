//
//  TripsDrivesController.m
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripsDrivesController.h"
#import  "UpcomingDriveDetailCell.h"
#import "ServerInterface.h"
#import "TripDrive.h"
#import "TripDriveRiderDetail.h"
#import "RiderProfile.h"
#import "GenericRequest.h"
#import "RideDriveActionRequest.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RouteInfoView.h"
#import "CurrentDriveController.h"
#import "UniversalAlert.h"
#import "ChatManager.h"
#import "UserContactDetail.h"
#import "ContactUserListController.h"
#import "LocalNotificationManager.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "zify-Swift.h"

@interface TripsDrivesController ()

@end

@implementation TripsDrivesController {
    NSMutableArray *tripDrives;
    UIImage *profileImagePlaceHolder;
    NSDateFormatter *dateTimeFormatter1;
    NSDateFormatter *dateFormatter1;
   // NSDateFormatter *timeFormatter1;
    NSDate *currentDayDate;
    BOOL isUserChatConnected;
    BOOL isViewInitialised;
    LocalNotificationManager *localNotificationManager;
    
    UpcomingDriveDetailCell *cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (IS_IPHONE_5) {
        cell.source.fontSize = 13;
        cell.destination.fontSize = 13;
        cell.tripTime.fontSize = 12;
        cell.driveId.fontSize = 12;
        cell.onTripLabel.fontSize = 12;
        cell.tapToConnentLbl.fontSize = 10;
    } else if (IS_IPHONE_6) {
        cell.source.fontSize = 13;
        cell.destination.fontSize = 13;
        cell.tripTime.fontSize = 13;
        cell.driveId.fontSize = 13;
        cell.onTripLabel.fontSize = 13;
        cell.tapToConnentLbl.fontSize = 10;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        cell.source.fontSize = 13;
        cell.destination.fontSize = 13;
        cell.tripTime.fontSize = 14;
        cell.driveId.fontSize = 14;
        cell.onTripLabel.fontSize = 14;
        cell.tapToConnentLbl.fontSize = 11;
    }
    
    tripDrives = nil;
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
//    timeFormatter1 = [[NSDateFormatter alloc] init];
//    [timeFormatter1 setDateFormat:@"HH:mm"];
//    [timeFormatter1 setLocale:locale];
    currentDayDate = [self getCurrentDayDate];
    isViewInitialised = false;
    isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    localNotificationManager = [LocalNotificationManager sharedInstance];
    _tableView.estimatedRowHeight = 180.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised){
        self.tableView.hidden = false;
        self.noDriveView.hidden = true;
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance]getResponse:[[GenericRequest alloc] initWithRequestType:UPCOMINGDRIVESREQUEST] withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        tripDrives = [NSMutableArray arrayWithArray:(NSArray *)response.responseObject];
                        [self handleTripDrivesResponse];
                        [localNotificationManager cancelNotificationsWithType:DRIVEALARM];
                        [localNotificationManager handleTripDrivesNotifications:tripDrives];
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
        isViewInitialised = true;
    }
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tripDrives.count == 0) return 0;
    return [tripDrives count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    cell = [_tableView dequeueReusableCellWithIdentifier:@"upcomingDriveDetailCell"];
   
    
    cell.tapToConnentLbl.hidden = YES;
    cell.srcImageView.backgroundColor = [UIColor whiteColor];
    cell.srcImageView.layer.borderColor = [UIColor orangeColor].CGColor;
    cell.srcImageView.layer.borderWidth = 2.0;
    cell.srcImageView.layer.cornerRadius = cell.srcImageView.frameHeight/2;
    cell.destImageView.image = [[AppDelegate getAppDelegateInstance] getNewImageWithOldImage:[UIImage imageNamed:@"loc_123.png"] withNewColor:[UIColor grayColor]];
    TripDrive *drive = [tripDrives objectAtIndex:indexPath.section];
    cell.source.text = drive.srcAddress;
    cell.destination.text = drive.destAddress;
    NSDate *departureTime = [dateTimeFormatter1 dateFromString:drive.departureTime];
    NSString *dateStr = [self getDateStringFromDepartureTime:departureTime];
    cell.tripTime.text = [NSString stringWithFormat:@"%@, %@",dateStr, [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureTime]];
    if([NSLocalizedString(CMON_GENERIC_TODAY, nil) isEqualToString:dateStr]){
      //  cell.tripDate.textColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
        cell.tripTime.textColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
    } else{
       // cell.tripDate.textColor = [UIColor lightGrayColor];
        cell.tripTime.textColor = [UIColor lightGrayColor];
    }
    
    //VC_TRIPSHISTORY_DRIVEID
    cell.driveId.text = [NSString stringWithFormat:NSLocalizedString(VC_TRIPSHISTORY_DRIVEID, @"Drive Id: {Drive ID}"), drive.driveId.stringValue];;
    for(int index =0; index < cell.riderImages.count ; index ++){
        UIImageView *riderImage = [cell.riderImages objectAtIndex:index];
        UIButton *riderImageButton = [cell.riderImageButtons objectAtIndex:index];
        riderImage.hidden = YES;
        riderImageButton.hidden = YES;
    };
    __block BOOL showContactButton = false;
    [drive.driveRiders enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TripDriveRiderDetail *riderDetail = (TripDriveRiderDetail *)obj;
        RiderProfile *riderProfile = riderDetail.riderProfile;
        UIImageView *riderImage = [cell.riderImages objectAtIndex:idx];
        UIButton *riderImageButton = [cell.riderImageButtons objectAtIndex:idx];
        cell.tapToConnentLbl.hidden = NO;
        [riderImage sd_setImageWithURL:[NSURL URLWithString:riderProfile.profileImageURL] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
            riderImage.hidden = NO;
        }];
        riderImage.layer.cornerRadius = riderImage.frame.size.width / 2;
        riderImage.clipsToBounds = YES;
        riderImageButton.hidden = NO;
        if((isUserChatConnected && riderProfile.chatProfile) || riderProfile.callId)
            showContactButton = YES;
    }];
    if(showContactButton) cell.contactButton.hidden = NO;
    else cell.contactButton.hidden = YES;
    if([@"RUNNING" isEqualToString:drive.status]){
        cell.cancelButton.hidden = YES;
        cell.cancelButtonSeparator.hidden = YES;
        cell.onTripLabel.hidden = NO;
    } else{
        cell.cancelButton.hidden = NO;
        cell.cancelButtonSeparator.hidden = NO;
        cell.onTripLabel.hidden = YES;
    }
    cell.driveRiders = drive.driveRiders;
    cell.contactButton.hidden = YES;
    
    [self addShadowToView:cell.contentView];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if(section == tripDrives.count - 1) return 10.0f;
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)showRouteInfo:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    TripDrive *tripDrive = [tripDrives objectAtIndex:indexPath.section];
    [RouteInfoView showRouteInfoOnView:self.navigationController.view WithTripRouteDetail:tripDrive.routeDetail];
}

-(IBAction) showUserContactDetails:(UIButton *)sender{
    NSMutableArray *contactUsersArray = [[NSMutableArray alloc] init];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    TripDrive *tripDrive = [tripDrives objectAtIndex:indexPath.section];
    [tripDrive.driveRiders enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TripDriveRiderDetail *riderDetail = (TripDriveRiderDetail *)obj;
        RiderProfile *riderProfile = riderDetail.riderProfile;
        if((isUserChatConnected && riderProfile.chatProfile) || riderProfile.callId){
            UserContactDetail *userContactDetail = [[UserContactDetail alloc] initWithChatIdentifier:isUserChatConnected && riderProfile.chatProfile ? riderProfile.chatProfile.chatUserId :nil andCallIdentifier:riderProfile.callId andFirstName:riderProfile.firstName andLastName:riderProfile.lastName andImageUrl:riderProfile.profileImageURL];
            [contactUsersArray addObject:userContactDetail];
        }
    }];
    [ContactUserListController createAndOpenContactListController:self.navigationController WithUserContacts:contactUsersArray];
}
-(IBAction)showUserContactDetailsBasedOnSelectedUser:(UIButton *)sender{
    NSUInteger indexOfSelectedUser = sender.tag;
    NSMutableArray *contactUsersArray = [[NSMutableArray alloc] init];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    TripDrive *tripDrive = [tripDrives objectAtIndex:indexPath.section];
    TripDriveRiderDetail *riderDetail = (TripDriveRiderDetail *)[tripDrive.driveRiders objectAtIndex:indexOfSelectedUser] ;
    RiderProfile *riderProfile = riderDetail.riderProfile;
    isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    NSLog(@"isUserChatConnected is %d", isUserChatConnected);
    NSLog(@"chatProfile is %@", riderProfile.chatProfile);
    NSLog(@"callId is %@", riderProfile.callId);

    if((isUserChatConnected && riderProfile.chatProfile) || riderProfile.callId){
        UserContactDetail *userContactDetail = [[UserContactDetail alloc] initWithChatIdentifier:isUserChatConnected && riderProfile.chatProfile ? riderProfile.chatProfile.chatUserId :nil andCallIdentifier:riderProfile.callId andFirstName:riderProfile.firstName andLastName:riderProfile.lastName andImageUrl:riderProfile.profileImageURL];
        [contactUsersArray addObject:userContactDetail];
    }
    if(contactUsersArray.count > 0){
    [ContactUserListController createAndOpenContactListController:self.navigationController WithUserContacts:contactUsersArray];
    }else{
      //  Need to show alert message
    }
}

-(IBAction) cancelDrive:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    TripDrive *tripDrive = [tripDrives objectAtIndex:indexPath.section];
    if(tripDrive.driveRiders.count != 0){
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_TRIPDRIVES_CANCELALERTTITLE, nil) WithMessage:NSLocalizedString(VC_TRIPDRIVES_CANCELALERMESSAGE, nil)];
        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
            [self cancelDriveForTripDrive:tripDrive andIndexPath:indexPath];
        }];
        [alert addButton:BUTTON_CANCEL WithTitle:NSLocalizedString(CMON_GENERIC_CANCEL, nil) WithAction:^(void *action){
            
        }];
        [alert showInViewController:self];
    } else{
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_TRIPDRIVES_CANCELALERTTITLE, nil) WithMessage:NSLocalizedString(VC_TRIPDRIVE_CANCELALERMESSAGE, nil)];
        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
            [self cancelDriveForTripDrive:tripDrive andIndexPath:indexPath];
        }];
        [alert addButton:BUTTON_CANCEL WithTitle:NSLocalizedString(CMON_GENERIC_CANCEL, nil) WithAction:^(void *action){
            
        }];
        [alert showInViewController:self];
    }
}

-(void)cancelDriveForTripDrive:(TripDrive *)tripDrive andIndexPath:(NSIndexPath *)indexPath{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:CANCELDRIVE andDriveId:tripDrive.driveId] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [tripDrives removeObjectAtIndex:indexPath.section];
                    [self handleTripDrivesResponse];
                    [localNotificationManager cancelNotificationWithIdentifier:tripDrive.driveId.stringValue andType:DRIVEALARM];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void)handleTripDrivesResponse{
    if([tripDrives count] == 0){
        self.tableView.hidden = true;
        self.noDriveView.hidden = false;
    } else{
        self.tableView.hidden = false;
        self.noDriveView.hidden = true;
        [self.tableView reloadData];
    }
}


-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}

-(NSDate *)getCurrentDayDate{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [calendar dateFromComponents:components];
}

-(NSString *)getDateStringFromDepartureTime:(NSDate *)date{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:currentDayDate toDate:date options:0];
    NSInteger difference = [components day];
    if(difference == 0) return NSLocalizedString(CMON_GENERIC_TODAY, nil);
    else if(difference == 1) return NSLocalizedString(CMON_GENERIC_TOMORROW, nil);
    else return [dateFormatter1 stringFromDate:date];
}
-(void)addShadowToView:(UIView *)v{
    [v.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [v.layer setShadowOpacity:0.8];
    [v.layer setShadowRadius:3.0];
    // [v.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    v.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
}

@end
