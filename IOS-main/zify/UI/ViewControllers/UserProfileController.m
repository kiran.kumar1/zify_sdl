//
//  UserProfileController.m
//  zify
//
//  Created by Anurag S Rathor on 27/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserProfileController.h"
#import "UserProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UserDocuments.h"
#import "UIImage+ProportionalFill.h"
#import "CurrentLocale.h"
 
#import "UserAddressPreferencesController.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

@interface UserProfileController ()

@end

@implementation UserProfileController{
    UserProfile *userProfile;
    UIImage *profileImagePlaceHolder;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (IS_IPHONE_5) {
        _userName.fontSize = 28;
        _email.fontSize = 13;
        _phoneNumber.fontSize = 13;
        _vehicleDetails.fontSize = 13;
        _idCardDetails.fontSize = 13;
        
        _lblPreferenceTitle.fontSize = 13;
        _lblPreferenceSubTitle.fontSize = 13;
        _lblPhoneNumberTitle.fontSize = 13;
        _lblEmailTitle.fontSize = 13;
        _lblVehicleDetailsTitle.fontSize = 13;
        _lblIdentificationTitle.fontSize = 13;
        _btnEditProfile.titleLabel.fontSize = 13;
        
    } else if (IS_IPHONE_6) {
        _userName.fontSize = 29;
        _email.fontSize = 14;
        _phoneNumber.fontSize = 14;
        _vehicleDetails.fontSize = 14;
        _idCardDetails.fontSize = 14;
        
        _lblPreferenceTitle.fontSize = 14;
        _lblPreferenceSubTitle.fontSize = 14;
        _lblPhoneNumberTitle.fontSize = 14;
        _lblEmailTitle.fontSize = 14;
        _lblVehicleDetailsTitle.fontSize = 14;
        _lblIdentificationTitle.fontSize = 14;
        _btnEditProfile.titleLabel.fontSize = 15;
        
    }  else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _userName.fontSize = 30;
        _email.fontSize = 14;
        _phoneNumber.fontSize = 14;
        _vehicleDetails.fontSize = 14;
        _idCardDetails.fontSize = 14;
        
        _lblPreferenceTitle.fontSize = 14;
        _lblPreferenceSubTitle.fontSize = 14;
        _lblPhoneNumberTitle.fontSize = 14;
        _lblEmailTitle.fontSize = 14;
        _lblVehicleDetailsTitle.fontSize = 14;
        _lblIdentificationTitle.fontSize = 14;
        _btnEditProfile.titleLabel.fontSize = 15;
    }
    
    
    BOOL isShowIdScreen = [[NSUserDefaults standardUserDefaults] boolForKey:@"showIDScreenToUpload"];
    if(isShowIdScreen){
        [self showIDCardDetails:nil];
    }
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self clearCache];
    [self populateUserDetails];
    
    self.navigationItem.title = NSLocalizedString(UPS_PROFILE_NAV_TITLE, nil);
    
    
}
- (void)clearCache{
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
    [imageCache cleanDisk];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) dismiss:(id)sender{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void) populateUserDetails{
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_large_image.png"];
    userProfile = [UserProfile getCurrentUser];
    // _userImage.hidden = YES;
    [_userImage sd_setImageWithURL:[NSURL URLWithString:userProfile.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        _userImage.hidden = NO;
    }];
    _userImage.layer.cornerRadius = _userImage.frame.size.width / 2;
    _userImage.clipsToBounds = YES;
    _userImage.layer.borderColor = [UIColor whiteColor].CGColor;
    _userImage.layer.borderWidth = 5.0f;
    if([@"VERIFIED" isEqualToString:userProfile.userStatus]){
        _userVerificationImage.hidden = NO;
    } else {
        _userVerificationImage.hidden = YES;
    }
    _userName.text = [NSString stringWithFormat:@"%@ %@!",NSLocalizedString(VC_USERPROFILE_HI, nil) ,userProfile.firstName];
    _maleRatingView.rating = [userProfile.userRatingMale floatValue];
    _maleRatingView.editable = NO;
    _femaleRatingView.rating = [userProfile.userRatingFemale floatValue];
    _femaleRatingView.editable = NO;
    _phoneNumber.text = [NSString stringWithFormat:@"%@ %@",userProfile.isdCode,userProfile.mobile];
    UserDocuments *userDocuments = userProfile.userDocuments;
    if([userProfile.mobileVerified intValue]){
        _mobileVerifiedImage.hidden = false;
    } else {
        _mobileVerifiedImage.hidden = true;
    }
    _email.text = userProfile.emailId;
    
    
    if(userDocuments.vehicleModel.length != 0 && userDocuments.vehicleRegistrationNum.length != 0){
        _vehicleDetails.text = [NSString stringWithFormat:@"%@, %@",userDocuments.vehicleModel,userDocuments.vehicleRegistrationNum];
    }else{
        _vehicleDetails.text = NSLocalizedString(VC_USERPROFILE_NA, nil);
    }
    
  /*  if([userDocuments.isVehicleImgUploaded intValue]){
        _vehicleDetails.text = [NSString stringWithFormat:@"%@, %@",userDocuments.vehicleModel,userDocuments.vehicleRegistrationNum];
    } else{
        _vehicleDetails.text = NSLocalizedString(VC_USERPROFILE_NA, nil);
    }*/
    if([userDocuments.isIdCardDocUploaded intValue]){
        _idCardDetails.text = [[CurrentLocale sharedInstance] getIdentityCardTypeValue:userDocuments.idCardType];
    } else{
        _idCardDetails.text = NSLocalizedString(VC_USERPROFILE_ID_NA, nil);
    }
}

-(IBAction) showVehicleDetails:(id)sender{
   /* if([userProfile.userDocuments.isVehicleImgUploaded intValue] && ![userProfile.userDocuments.vehicleImgUrl isKindOfClass:[NSNull class]] && ![@"" isEqualToString:userProfile.userDocuments.vehicleImgUrl]){
        [self performSegueWithIdentifier:@"showVehicleDetailsReadOnly" sender:self];
    } else{
        [self performSegueWithIdentifier:@"showVehicleDetails" sender:self];
    }*/
    if( userProfile.userDocuments.vehicleImgUrl.length > 0 ){
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setBool:YES forKey:@"forPush"];
        [prefs synchronize];
        [self performSegueWithIdentifier:@"showVehicleDetailsAvailable" sender:self];
    }else if (userProfile.userDocuments.vehicleModel.length != 0 && userProfile.userDocuments.vehicleRegistrationNum.length != 0) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setBool:YES forKey:@"forPush"];
        [prefs synchronize];
        [self performSegueWithIdentifier:@"showTextImgUploadController" sender:self];
    } else {
        [self performSegueWithIdentifier:@"showVehicleTextDetails" sender:self];
    }
}

-(IBAction) showUserPreferences:(id)sender {
    NewTPScreenViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"newTpScreen"];
    [self.navigationController pushViewController:obj animated:TRUE];
    return;
    UserAddressPreferencesController *addressPreferencesController = [UserAddressPreferencesController createUserAddressPreferencesController];
    addressPreferencesController.showPageIndicator = NO;
    addressPreferencesController.isUpgradeFlow = NO;
    [self.navigationController pushViewController:addressPreferencesController animated:TRUE];
    //[self presentViewController:addressPreferencesController animated:NO completion:nil];
    

}

-(IBAction) showIDCardDetails:(id)sender {
    //phase - 1
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setBool:YES forKey:@"forPush"];
    [pref synchronize];
   
    
    BOOL isShowIdScreen = [[NSUserDefaults standardUserDefaults] boolForKey:@"showIDScreenToUpload"];
    if(isShowIdScreen){
        [pref setBool:NO forKey:@"forPush"];
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"showIDScreenToUpload"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
     [pref synchronize];
    if ([userProfile.userDocuments.idCardImgUrl isEqualToString:@""] || [userProfile.userDocuments.idCardImgUrl isKindOfClass:[NSNull class]] || userProfile.userDocuments.idCardImgUrl == nil) {
        
        if ([userProfile.userPreferences.userMode isEqualToString:@"CAR OWNER"]) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"IDCard" bundle:nil];
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"UserIDDrivingLicenceController"];
            [self.navigationController pushViewController:vc animated:TRUE];
        } else {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"IDCard" bundle:nil];
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"UserIDCardController"];
            [self.navigationController pushViewController:vc animated:TRUE];
        }
        
    } else {
        //[self presentViewController:[UserIDCardDetailsController createUserIDCardDetailsController] animated:NO completion:nil];
        //[self.navigationController pushViewController:[UserIDCardDetailsController userIDCardDetailsNavCtrl] animated:TRUE];
        //[self presentViewController:[UserIDCardDetailsController userIDCardDetailsNavCtrl] animated:TRUE completion:nil];
        
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        [pref setBool:YES forKey:@"forPush"];
        [pref synchronize];
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"UserOnboard" bundle:[NSBundle mainBundle]];
        UserIDCardDetailsController *userIDCardController = [sb instantiateViewControllerWithIdentifier:@"userIDCardDetailsController"];
        
        [self.navigationController pushViewController:userIDCardController animated:TRUE];
        
    }
    
}

+(UINavigationController *)createUserProfileNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"UserProfile" bundle:[NSBundle mainBundle]];
    UINavigationController *userProfileNavController = [storyBoard instantiateViewControllerWithIdentifier:@"userProfileNavController"];
    return userProfileNavController;
}


@end






