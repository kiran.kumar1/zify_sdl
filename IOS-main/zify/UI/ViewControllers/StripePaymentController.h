//
//  StripePaymentController.h
//  zify
//
//  Created by Anurag S Rathor on 16/11/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Stripe/Stripe.h>
#import "MessageHandler.h"
#import "StripeCardPaymentView.h"

@protocol StripePaymentDelegate
-(void) createStripePaymentWithToken:(STPToken *)token;
-(void) stripePaymentFailedWithError:(NSError *)error;
@end

@interface StripePaymentController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *rechargeAmountLabel;
@property (weak, nonatomic) IBOutlet UIView *payNowActionView;
@property (weak, nonatomic) IBOutlet MessageHandler *messageHandler;
@property (weak, nonatomic) IBOutlet StripeCardPaymentView *paymentTextField;
@property (nonatomic) NSString *amount;
@property (nonatomic, weak) id<StripePaymentDelegate> paymentDelegate;
+(UINavigationController *)createStripePaymentNavController;

@property (weak, nonatomic) IBOutlet UILabel *lblRechargeAmountText;
@property (weak, nonatomic) IBOutlet UILabel *lblPleaseEnterCardDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnPayNow;

@end
