//
//  MyReferralController.swift
//  zify
//
//  Created by Anurag on 07/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class MyReferralController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblYouhavetReferredAnyoneYet: UILabel!
    @IBOutlet weak var viewNoReferred: UIView!
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var lblReferralAmount: UILabel!
    @IBOutlet weak var lblZifyCoinsRecvdText: UILabel!
    @IBOutlet weak var lblReferralCount: UILabel!

    @IBOutlet weak var tblReferralHistory: UITableView!
    var arrMyReferralsData : [AnyObject]? = []
    let dateTimeFormatter11 = DateFormatter.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblReferralHistory.tableFooterView = UIView()
        lblZifyCoinsRecvdText.text = NSLocalizedString(zify_coins_received, comment: "")
        self.serviceCall_getMyReferrals()
        viewNoReferred.isHidden = true
        lblReferralAmount.isHidden = true
        lblZifyCoinsRecvdText.isHidden = true
        lblReferralCount.isHidden = true
        tblReferralHistory.isHidden = true
        dateTimeFormatter11.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        self.navigationItem.title = NSLocalizedString(my_referrals, comment: "")


    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.title = NSLocalizedString(my_referrals, comment: "")
    }
    
    // MARK:- Actions
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- UITableView Delegates
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: AppDelegate.screen_WIDTH(), height: 44))
        viewHeader.backgroundColor = UIColor.init(hex: "F3F3F3")
        let lblSectionHeaderText = UILabel.init(frame: CGRect.init(x: 10, y: 0, width: AppDelegate.screen_WIDTH(), height: 44))
        lblSectionHeaderText.text = NSLocalizedString(referral_history, comment: "")
        lblSectionHeaderText.font = UIFont.init(name: MediumFont, size: 18)
        if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblSectionHeaderText.font = UIFont.init(name: MediumFont, size: 19)
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblSectionHeaderText.font = UIFont.init(name: MediumFont, size: 20)
        }
        viewHeader.addSubview(lblSectionHeaderText)
        return viewHeader
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMyReferralsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellMyReferralHistry : MyReferralHistoryCell = (self.tblReferralHistory.dequeueReusableCell(withIdentifier: "MyReferralHistoryCell") as? MyReferralHistoryCell)!
        cellMyReferralHistry.imgReferralHistoryProfile.image = UIImage.init(named: "profilePlaceHolder.png")
        
        let dateStr = "\(((self.arrMyReferralsData)?[indexPath.row] as? NSDictionary)?.value(forKey: "created_on") as? String ?? "")"
        let date1: Date = dateTimeFormatter11.date(from: dateStr)!
        let dateTimeFormatter22 = DateFormatter()
        dateTimeFormatter22.dateFormat = "dd MMM YYYY"//dateFormat = "dd-MMM-YYYY"
        let strOutputDateString: String = dateTimeFormatter22.string(from: date1)
        
        //let sortedArray = self.arrMyReferralsData.sorted{ ($0["created_on"] as? Date)! > ($1["created_on"] as? Date)! }
        ///print(sortedArray)
        
        cellMyReferralHistry.lblReferralHistoryDate.text = "\(NSLocalizedString(joined_on, comment: "")) " + strOutputDateString
        cellMyReferralHistry.lblReferralHistoryName.text = "\(((self.arrMyReferralsData)?[indexPath.row] as? NSDictionary)?.value(forKey: "user_name") as? String ?? "")"
        let zifyCoinsCount = (((self.arrMyReferralsData)?[indexPath.row] as? NSDictionary)?.value(forKey: "referral_amt_from_child_to_parent") as AnyObject)
        cellMyReferralHistry.lblReferralHistoryCoins.text = "\(zifyCoinsCount) \(NSLocalizedString(coins, comment: ""))"
        let url = URL.init(string: "\(((self.arrMyReferralsData)?[indexPath.row] as? NSDictionary)?.value(forKey: "profile_img_url") as? String ?? "")")
        cellMyReferralHistry.imgReferralHistoryProfile.sd_setImage(with: url) { (img, error, cache, url) in
            
        }
        cellMyReferralHistry.selectionStyle = .none
        return cellMyReferralHistry
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
 
    // MARK:- Service Call
    func serviceCall_getMyReferrals() {
        
        let myReferralData = MyReferralData()
        debugPrint("---my referral data---", myReferralData as Any)
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance()?.getResponse(myReferralData, withHandler: { (response, error) in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.lblReferralAmount.isHidden = false
                        self.lblZifyCoinsRecvdText.isHidden = false
                        self.lblReferralCount.isHidden = false
                        self.tblReferralHistory.isHidden = false
                        debugPrint("----my refrl daata Response SUCCESSS--------", response as Any)
                        self.arrMyReferralsData = (response?.responseObject as! NSDictionary).value(forKey: "referredUserList")! as! NSArray as [AnyObject]
                        self.lblReferralAmount.text = "\((response?.responseObject as! NSDictionary).value(forKey: "total_referral_coins") as? Int ?? 0)"
                        self.lblReferralCount.text = "\(NSLocalizedString(referral_count, comment: "")) " + "\(self.arrMyReferralsData?.count ?? 0)"
                        debugPrint("----arrRespponse--------", self.arrMyReferralsData ?? [])
                        self.tblReferralHistory.reloadData()
                        if (self.arrMyReferralsData?.count)! > 0 {
                            self.viewNoReferred.isHidden = true
                            self.tblReferralHistory.isHidden = false
                        } else {
                            self.viewNoReferred.isHidden = false
                            self.tblReferralHistory.isHidden = true
                        }
                    } else {
                        self.viewNoReferred.isHidden = false
                        self.tblReferralHistory.isHidden = true
                        self.lblReferralAmount.text = "0"
                        self.lblReferralCount.text = "\(referral_count) : 0"
                        debugPrint("----my refrl daata Response NILLL FAILUREE--------", error?.localizedDescription ?? "----Response NILLL FAILUREE----")
                    }
                })
            })
        })
    }
    
    
    
}

class MyReferralHistoryCell : UITableViewCell {
    
    @IBOutlet weak var lblReferralHistoryCoins: UILabel!
    @IBOutlet weak var lblReferralHistoryDate: UILabel!
    @IBOutlet weak var lblReferralHistoryName: UILabel!
    @IBOutlet weak var imgReferralHistoryProfile: UIImageView!
}


// MARK: - Server Request for Save Referral Data

class MyReferralData : ServerRequest {
    
    override func urlparams() -> [AnyHashable : Any]! {
        
        let dictRefer = [
            "user_id" : UserProfile.getCurrentUserId()!,
            "country_code" : UserProfile.getCurrentUser()?.country ?? ""
            ] as [String : Any]
        debugPrint("---get  my referrals list---",dictRefer)
        return dictRefer
    }
    
    override func serverURL() -> String! {
        return "/zifyreferandearn/referandearn/referralDashBoard"
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
    override func isJSONRequestFormat() -> Bool {
        return false
    }
    
    override func overrideBaseURL() -> Bool {
        return true
    }
    override func isForReferAndEarn() -> Bool {
        return true
    }
    
}

