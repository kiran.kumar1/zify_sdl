//
//  EditProfileController.m
//  zify
//
//  Created by Anurag S Rathor on 29/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "EditProfileController.h"
#import "UserProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServerInterface.h"
#import "EditProfileRequest.h"
#import "ImageHelper.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "UIImage+ProportionalFill.h"
#import "AppDelegate.h"


@interface EditProfileController ()

@end

enum UserSelectingPicker{
    SelectingGender,SelectingBloodGroup
};

@implementation EditProfileController{
    NSArray *genderPickerValues;
    NSDictionary *genderValuesMap;
    NSArray *bloodGroupPickerValues;
    UIImage *editedImage;
    UIImage *profileImagePlaceHolder;
    enum UserSelectingPicker selectingPicker;
    NSDateFormatter *displayDateFormatter;
    NSDateFormatter *dateTimeFormatter;
    NSString *selectedDob;
    BOOL isViewInitialised;
    NSString *selectedBloodGroup;
    NSString *selectedGender;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addKeyboardNotifications];
    [self initialiseView];
    
    if (IS_IPHONE_5) {
        
        _userName.fontSize = 20;
        _gender.titleLabel.fontSize = 14;
        _dob.titleLabel.fontSize = 14;
        [_companyName setFont:[UIFont systemFontOfSize:14]];
        [_companyEmail setFont:[UIFont systemFontOfSize:14]];
        _bloodGroup.titleLabel.fontSize = 14;
        [_emergencyContact setFont:[UIFont systemFontOfSize:14]];
        _lblPersonalDetails.fontSize = 14;
        _lblCompanyDetails.fontSize = 14;
        _lblOtherDetails.fontSize = 14;
        _btnSave.titleLabel.fontSize = 16;
    } else if (IS_IPHONE_6) {
        _userName.fontSize = 21;
        _gender.titleLabel.fontSize = 15;
        _dob.titleLabel.fontSize = 15;
        [_companyName setFont:[UIFont systemFontOfSize:15]];
        [_companyEmail setFont:[UIFont systemFontOfSize:15]];
        _bloodGroup.titleLabel.fontSize = 15;
        [_emergencyContact setFont:[UIFont systemFontOfSize:15]];
        _lblPersonalDetails.fontSize = 15;
        _lblCompanyDetails.fontSize = 15;
        _lblOtherDetails.fontSize = 15;
        _btnSave.titleLabel.fontSize = 18;
    }  else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _userName.fontSize = 22;
        _gender.titleLabel.fontSize = 16;
        _dob.titleLabel.fontSize = 16;
        [_companyName setFont:[UIFont systemFontOfSize:16]];
        [_companyEmail setFont:[UIFont systemFontOfSize:16]];
        _bloodGroup.titleLabel.fontSize = 16;
        [_emergencyContact setFont:[UIFont systemFontOfSize:16]];
        _lblPersonalDetails.fontSize = 16;
        _lblCompanyDetails.fontSize = 16;
        _lblOtherDetails.fontSize = 16;
        _btnSave.titleLabel.fontSize = 20;
    }
    
    UIImage *cameraImage = [UIImage imageNamed:@"photo-camera.png"];
    cameraImage = [cameraImage imageScaledToFitSize:CGSizeMake(20, 20)];
    [self.btnEditProfile setImage:cameraImage forState:UIControlStateNormal];
    [self.btnEditProfile setImage:cameraImage forState:UIControlStateSelected];
    _blurImageView.backgroundColor = [UIColor clearColor];
    [self initialiseViewForBlur];
}
-(void)initialiseViewForBlur{
    isViewInitialised = false;
    [self applyVerticalGradient:_blurImageView];
}

-(void)addKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewWillAppear:(BOOL)animated {
   
}


-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised){
        isViewInitialised = true;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) dismiss:(id)sender{
    if(editedImage){
        [[SDImageCache sharedImageCache] clearMemory];
        [[SDImageCache sharedImageCache] clearDisk];
    }
   // [self.messageHandler dismissErrorView];
   // [self.messageHandler dismissSuccessView];
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction) selectUserProfileImage:(id)sender{
    [self openCameraORGalleryselectionView];
}
-(IBAction)btnEditProfileTapped:(id)sender{
    [self openCameraORGalleryselectionView];
}
-(void)openCameraORGalleryselectionView{
    [ImageSelectionViewer sharedInstance].imageSelectionDelegate = self;
    [ImageSelectionViewer sharedInstance].finalImageSize = self.displayProfileImage.frame.size;//CGSizeMake(320.0, 320.0);
    [[ImageSelectionViewer sharedInstance] selectImage];
}

-(IBAction)selectGender:(id)sender{
    [super dismissKeyboard];
    [self.view endEditing:YES];
    self.genderPickerView.delegate = self;
    self.genderPickerView.pickervalues = genderPickerValues;
    self.genderPickerView.currentValue = self.gender.titleLabel.text;
    selectingPicker = SelectingGender;
    [self.genderPickerView showInView:self.navigationController.view];
}

-(IBAction) selectDOB:(id)sender{
    [self.view endEditing:YES];
    if(selectedDob){
        self.dobPickerView.selectedDate = [displayDateFormatter dateFromString:selectedDob];
    }
    [self.dobPickerView showInView:self.view];
}

-(IBAction)selectBloodGroup:(id)sender{
    [super dismissKeyboard];
    self.bloodGroupPickerView.delegate = self;
    self.bloodGroupPickerView.pickervalues = bloodGroupPickerValues;
    self.bloodGroupPickerView.currentValue = self.bloodGroup.titleLabel.text;
    selectingPicker = SelectingBloodGroup;
    [self.bloodGroupPickerView showInView:self.navigationController.view];
}

-(IBAction) editUserProfile:(id)sender{
    [super dismissKeyboard];
    NSLog(@"selected gender is %@", [genderValuesMap objectForKey:selectedGender]);
    if([self validateUserInfo]){
        NSDate *sendingDate = [displayDateFormatter dateFromString:selectedDob];
        NSString *reqDate = [dateTimeFormatter stringFromDate:sendingDate];
        NSString *gender = [genderValuesMap objectForKey:selectedGender];
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            EditProfileRequest *editProfileRequest = [[EditProfileRequest alloc] initWithGender:gender andDOB:reqDate andCompanyName:self.companyName.text andCompanyEmail:self.companyEmail.text andBloodGroup:selectedBloodGroup andEmergencyContact:self.emergencyContact.text andCountryCode:  @"IN"];
            [[ServerInterface sharedInstance] getResponse:editProfileRequest withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                      //  [self.messageHandler showSuccessMessage:NSLocalizedString(user_profile_updated_success,nil)];
                        [MoEngageEventsClass setUserValuesToMoengageDashboard];
                        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:AppName WithMessage:NSLocalizedString(user_profile_updated_success,nil)];
                        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK,nil) WithAction:^(void *action) {
                            [self dismiss:nil];
                        }];
                        [alert showInViewController:self];
                        self.btnSave.userInteractionEnabled = NO;
                     //   [self performSelector:@selector(dismiss:) withObject:self afterDelay:1.0];
                    } else{
                        [UIAlertController showErrorWithMessage:error.userInfo[@"message"] inViewController:self];
                        //[self.messageHandler showErrorMessage:];
                    }
                }];
            } andImage:editedImage];
        }];
    }
}

-(void) userSelectedFinalImage:(UIImage *)image{
    editedImage = image;
    self.displayProfileImage.image = image;
}

-(void)openCropImgeVc:(UIImage *)withImage{
    
}
    


-(void) selectedPickerValue:(NSString *)value{
    if(selectingPicker == SelectingGender){
        selectedGender = value;
        [_gender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_gender setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [self.gender setTitle:value forState:UIControlStateNormal];
    } else{
        if( [value isEqualToString:NSLocalizedString(profile_blood_group_not_disclose,nil)]){
            selectedBloodGroup = @"";
        }else{
        selectedBloodGroup = value;
        }
        [_bloodGroup setTitleColor:[UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.bloodGroup setTitle:value forState:UIControlStateNormal];
    }
}

-(BOOL) allowImageEditing{
    return NO;
}


-(void) selectedDate:(NSDate *)selectedDate{
    selectedDob = [displayDateFormatter stringFromDate:selectedDate];
    [_dob setTitleColor:[UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [_dob setTitle:[displayDateFormatter stringFromDate:selectedDate] forState:UIControlStateNormal];

}

-(void) initialiseView{
    genderPickerValues = [NSArray arrayWithObjects:NSLocalizedString(CMON_GENERIC_MALE, nil),NSLocalizedString(CMON_GENERIC_FEMALE, nil),NSLocalizedString(CMON_GENERIC_OTHERS, nil),nil];
    genderValuesMap = @{NSLocalizedString(CMON_GENERIC_MALE,nil):@"Male",NSLocalizedString(CMON_GENERIC_FEMALE,nil):@"Female",
                        NSLocalizedString(CMON_GENERIC_OTHERS,nil):@"Others"};
    bloodGroupPickerValues = [NSArray arrayWithObjects:@"A+",@"A-",@"B+",@"B-",@"O+",@"O-",@"AB+",@"AB-",NSLocalizedString(profile_blood_group_not_disclose,nil),nil];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [displayDateFormatter setLocale:locale];
    displayDateFormatter = [[NSDateFormatter alloc] init];
    [displayDateFormatter setDateFormat:@"dd MMM yyyy"];
    [displayDateFormatter setLocale:locale];
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"MMM dd, yyyy"];
    [dateTimeFormatter setLocale:locale];
   
    _dobPickerView.delegate = self;
    _dobPickerView.isDatePicker = YES;
    
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_large_image.png"];
    UserProfile *userProfile = [UserProfile getCurrentUser];
  //  _displayProfileImage.hidden = YES;
    [_displayProfileImage sd_setImageWithURL:[NSURL URLWithString:userProfile.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        //_displayProfileImage.hidden = NO;
    }];
     _userName.text = [[NSString stringWithFormat:@"%@ %@",userProfile.firstName,userProfile.lastName] capitalizedString];
    if(userProfile.gender.length != 0){
        NSString *gendertitle = NSLocalizedString([@"CMON_GENERIC_" stringByAppendingString: userProfile.gender.uppercaseString],nil);
    selectedGender = gendertitle;
    [_gender setTitle:gendertitle forState:UIControlStateNormal];
        [_gender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_gender setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    }else{
        selectedGender = @"";
         UIColor *color = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0];
        [_gender setTitleColor:color forState:UIControlStateNormal];
        [_gender setTitleColor:color forState:UIControlStateSelected];

    }
    _companyName.text = [userProfile.companyName capitalizedString];
    _companyEmail.text = userProfile.companyEmail;
    _emergencyContact.text = userProfile.emergencyContact;
    if(userProfile.bloodGroup.length > 0){
        selectedBloodGroup = userProfile.bloodGroup;
        [_bloodGroup setTitle:userProfile.bloodGroup forState:UIControlStateNormal];
    } else{
        selectedBloodGroup = @"";
        UIColor *color = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0];
        [_bloodGroup setTitleColor:color forState:UIControlStateNormal];
        [_bloodGroup setTitleColor:color forState:UIControlStateSelected];
        //UPS_EDITPROFILE_BTN_BLOODGROUP
        [_bloodGroup setTitle:NSLocalizedString(profile_blood_group_not_disclose, nil) forState:UIControlStateNormal];
    }
    if(userProfile.dob){
        selectedDob = [displayDateFormatter stringFromDate:[dateTimeFormatter dateFromString:userProfile.dob]];
        [_dob setTitle:selectedDob forState:UIControlStateNormal];
        [_dob setTitleColor:[UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    }
}
-(BOOL) validateUserInfo{
    if(![@"" isEqualToString:self.companyEmail.text] && ![self validateEmail:self.companyEmail.text]){
       // [self.messageHandler showErrorMessage:NSLocalizedString(VC_EDITPROFILE_EMAILVALIDVAL, nil)];
         [UIAlertController showErrorWithMessage:NSLocalizedString(VC_EDITPROFILE_EMAILVALIDVAL,nil) inViewController:self];
        return false;
    }
    return true;
}

-(BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [predicate evaluateWithObject:email];
}
-(void)applyVerticalGradient:(UIView *)view{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(view.frameX, view.frameY, [AppDelegate SCREEN_WIDTH], view.frameHeight);
    gradientLayer.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:13.0/255.0 green:13.0/255.0 blue:13.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0/255.0 green:2.0/255.0 blue:2.0/255.0 alpha:0.8].CGColor,(id)[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0].CGColor, nil];
    gradientLayer.locations = @[@0,@0.3,@0.5,@0.8,@1.0];
    [view.layer addSublayer:gradientLayer];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if(textField == self.companyName){
        [self.companyEmail becomeFirstResponder];
    }
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height + 44), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width + 44), 0.0);
    }
    
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        self.objScroll.contentInset = contentInsets;// insert content inset value here
        self.objScroll.scrollIndicatorInsets = contentInsets; // insert content inset value here
    }];
    
    //  [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        self.objScroll.contentInset = UIEdgeInsetsZero;
        self.objScroll.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}

@end
