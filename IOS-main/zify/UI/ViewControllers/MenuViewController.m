//
//  MenuViewController.m
//  zify
//
//  Created by Anurag S Rathor on 09/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuProfileCell.h"
#import "MenuOptionCell.h"
#import "UserProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "zify-Swift.h"

/*#define PROFILE_SECTION 0
 #define REFER_AND_EARN_SECTION 1
 #define CURRENT_TRIP_SECTION 2
 #define CHAT_SECTION 3
 #define WALLET_SECTION 4
 #define TRIPS_SECTION 5
 #define ACCOUNT_SECTION 6*/

#define PROFILE_SECTION 0
#define TRAVEL_PREFERENCE 1
#define ACTIVE_TRIP 2
#define MESSAGES_CHAT 3
#define WALLET_SECTION 4
#define UPCOMING_TRIPS 5
#define ACCOUNT_SECTION 6
#define REFER_AND_EARN_SECTION 7
#define LOYALTY_AND_REWARDS 8
#define SETTINGS_OPTION 9
#define ABOUT_OPTION 10
#define LEAVE_FEEDBACK 11

@interface MenuViewController ()

@end


@implementation MenuViewController{
    UserProfile *userProfile;
    UIImage *profileImagePlaceHolder;
    BOOL isGlobalLocale;
    BOOL isGlobalPayment;
    NSLayoutConstraint *contentViewBeforeAnimateConstraint;
    NSLayoutConstraint *contentViewAfterAnimateConstraint;
    BOOL isViewAnimated;
    float progressValue;
    
}

-(void) viewDidLoad{
    [super viewDidLoad];
    progressValue = 0.0f;
    [self initialiseView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!isViewAnimated){
        [self.view removeConstraint:_contentViewLeftConstraint];
        [self.view removeConstraint:contentViewAfterAnimateConstraint];
        [self.view addConstraint:contentViewBeforeAnimateConstraint];
        [self.view layoutIfNeeded];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewAnimated){
        [self.view removeConstraint:contentViewBeforeAnimateConstraint];
        [self.view addConstraint:contentViewAfterAnimateConstraint];
        [UIView animateWithDuration:0.3  animations:^{
            self.contentView.alpha =1;
            [self.view layoutIfNeeded];
        }completion:^(BOOL finished){
            isViewAnimated = true;
        }];
    }
}

-(void) initialiseView{
    
    // _dataOptinsArr = [[NSArray alloc] initWithObjects:@"",@"Your Travel Preferences",@"Active Trip",@"Messages",@"Wallet",@"Upcoming Trips",@"Account",@"Refer And Earn",@"Settings", @"About",@"Leave Feedback", nil];
    self.contentView.alpha =0;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [[UIScreen mainScreen] bounds].size.height <568.0){
        _tableView.scrollEnabled = YES;
    } else{
        _tableView.scrollEnabled = NO;
    }
    _tableView.scrollEnabled = YES;
    userProfile = [UserProfile getCurrentUser];
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    isGlobalLocale = [currentLocale isGlobalLocale];
    isGlobalPayment = [currentLocale isGlobalPayment];
    _tableView.estimatedRowHeight = 135.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    contentViewAfterAnimateConstraint = [NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.0f];
    contentViewBeforeAnimateConstraint = [NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.0f];
    isViewAnimated = false;
    
    NSString *countryCode = [[CurrentLocale sharedInstance] getUserCountryISOCodeString];
    NSLog(@"=-----%@-----", [[[AppDelegate.getAppDelegateInstance remoteConfig] configValueForKey:@"lr_enabled_countries"] stringValue]);
    NSString *strLREnableCountries = [[[AppDelegate.getAppDelegateInstance remoteConfig] configValueForKey:@"lr_enabled_countries"] stringValue];
    NSData *data = [strLREnableCountries dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *arrJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSLog(@"=--josn---%@-----",arrJson);
    
    _dataOptinsArr = [[NSArray alloc] initWithObjects:
                      SIDE_MENU_NEW_ACTIVE_TRIPS,
                      SIDE_MENU_NEW_UPCOMING_TRIPS,
                      SIDE_MENU_NEW_REFER_AND_EARN,
                      SIDE_MENU_NEW_WALLET,
                      SIDE_MENU_NEW_LEAVE_FEEDBACK,
                      SIDE_MENU_NEW_SETTINGS,
                      SIDE_MENU_NEW_ABOUT, nil];
    
    _imageNamesArr = [[NSArray alloc] initWithObjects:
                      @"activeTrip.png",
                      @"upcomingtrip.png",
                      @"referAndEarn",
                      @"wallet",
                      @"support",
                      @"settings.png",
                      @"about",nil];
        
    if ([arrJson containsObject:[userProfile country]]) {
        _dataOptinsArr = [[NSArray alloc] initWithObjects:
                          SIDE_MENU_NEW_ACTIVE_TRIPS,
                          SIDE_MENU_NEW_UPCOMING_TRIPS,
                          SIDE_MENU_NEW_REWARDS,
                          SIDE_MENU_NEW_REFER_AND_EARN,
                          SIDE_MENU_NEW_WALLET,
                          SIDE_MENU_NEW_LEAVE_FEEDBACK,
                          SIDE_MENU_NEW_SETTINGS,
                          SIDE_MENU_NEW_ABOUT, nil];
        
        _imageNamesArr = [[NSArray alloc] initWithObjects:
                          @"activeTrip.png",
                          @"upcomingtrip.png",
                          @"offersAndRewards",
                          @"referAndEarn",
                          @"wallet",
                          @"support",
                          @"settings.png",
                          @"about",nil];
    }
    
    else if ([[userProfile country] isEqualToString:@"IN"]) {
        
        _dataOptinsArr = [[NSArray alloc] initWithObjects:
                          SIDE_MENU_NEW_ACTIVE_TRIPS,
                          SIDE_MENU_NEW_UPCOMING_TRIPS,
                          SIDE_MENU_NEW_REWARDS,
                          SIDE_MENU_NEW_REFER_AND_EARN,
                          SIDE_MENU_NEW_WALLET,
                          SIDE_MENU_NEW_LEAVE_FEEDBACK,
                          SIDE_MENU_NEW_SETTINGS,
                          SIDE_MENU_NEW_ABOUT, nil];
        
        _imageNamesArr = [[NSArray alloc] initWithObjects:
                          @"activeTrip.png",
                          @"upcomingtrip.png",
                          @"offersAndRewards",
                          @"referAndEarn",
                          @"wallet",
                          @"support",
                          @"settings.png",
                          @"about",nil];
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == REFER_AND_EARN_SECTION && isGlobalLocale) return 0;
    return _dataOptinsArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /* if (indexPath.row == PROFILE_SECTION) {
     MenuProfileCell *profileCell = [tableView dequeueReusableCellWithIdentifier:@"profileCell"];
     profileCell.userImage.hidden = YES;
     profileCell.userImage.layer.cornerRadius = profileCell.userImage.frame.size.width / 2;
     profileCell.userImage.clipsToBounds = YES;
     [profileCell.userImage sd_setImageWithURL:[NSURL URLWithString:userProfile.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
     profileCell.userImage.hidden = NO;
     }];
     profileCell.userName.text = [NSString stringWithFormat:@"%@ %@",userProfile.firstName,userProfile.lastName];
     if([@"VERIFIED" isEqualToString:userProfile.userStatus]){
     profileCell.verificationImage.image =[UIImage imageNamed:@"icn_menu_tick.png"];
     profileCell.verificationMsg.text = @"";
     } else{
     profileCell.verificationImage.image =[UIImage imageNamed:@"icn_menu_warning.png"];
     profileCell.verificationMsg.text = NSLocalizedString(VC_MENU_PROFILEVERIFICATIONDUE, nil);
     }
     profileCell.profileCompleteness = [userProfile.profileCompleteness floatValue] / 100.0;
     if([userProfile.profileCompleteness intValue] == 100){
     profileCell.completenessProgress.progressTintColor = [UIColor colorWithRed:(127.0/255.0) green:(255.0/255.0) blue:(212.0/255.0) alpha:1.0];
     profileCell.completenessPercent.textColor = [UIColor colorWithRed:(127.0/255.0) green:(255.0/255.0) blue:(212.0/255.0) alpha:1.0];
     } else {
     profileCell.completenessProgress.progressTintColor =[UIColor colorWithRed:(255.0/255.0) green:(228.0/255.0) blue:(110.0/255.0) alpha:1.0];
     profileCell.completenessPercent.textColor = [UIColor colorWithRed:(255.0/255.0) green:(255.0/228.0) blue:(110.0/255.0) alpha:1.0];
     }
     [profileCell startProgress];
     return profileCell;
     }*/
    MenuOptionCell *optionCell = [tableView dequeueReusableCellWithIdentifier:@"optionCell"];
    
    if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_REFER_AND_EARN]){
        NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
        imageAttachment.image = [UIImage imageNamed:@"NewTag"];
        CGFloat imageOffsetY = -3.0;
        imageAttachment.bounds = CGRectMake(0, imageOffsetY, imageAttachment.image.size.width, imageAttachment.image.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
        NSMutableAttributedString *completeText= [[NSMutableAttributedString alloc] initWithString:NSLocalizedString([_dataOptinsArr objectAtIndex:indexPath.row], nil)];
        NSMutableAttributedString *spaceAftertext = [[NSMutableAttributedString alloc] initWithString:@" "];
        [completeText appendAttributedString:spaceAftertext];
        [completeText appendAttributedString:attachmentString];
        optionCell.optionName.textAlignment=NSTextAlignmentLeft;
        optionCell.optionName.attributedText=completeText;
    } else {
        optionCell.optionName.text = NSLocalizedString([_dataOptinsArr objectAtIndex:indexPath.row], nil);
    }
    optionCell.optionImage.image = [UIImage imageNamed:[_imageNamesArr objectAtIndex:indexPath.row]];
    
    if (IS_IPHONE_5) {
        optionCell.optionName.fontSize = 13;
    } else if (IS_IPHONE_6) {
        optionCell.optionName.fontSize = 13;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        optionCell.optionName.fontSize = 15;
    }
    

    
    
    /*  if(indexPath.section == REFER_AND_EARN_SECTION){
     optionCell.optionImage.image = [UIImage imageNamed:@"icn_refernearn.png"];
     optionCell.optionName.text = NSLocalizedString(VC_MENU_REFERANDEARN, nil);
     } else if(indexPath.section == CURRENT_TRIP_SECTION){
     optionCell.optionImage.image = [UIImage imageNamed:@"icn_current_ride.png"];
     optionCell.optionName.text = NSLocalizedString(VC_MENU_ACTIVETRIP, nil);
     } else if(indexPath.section == WALLET_SECTION){
     optionCell.optionImage.image = [UIImage imageNamed:@"icn_menu_wallet.png"];
     if(isGlobalLocale && !isGlobalPayment) optionCell.optionName.text = NSLocalizedString(VC_MENU_STATEMENT, nil);
     else optionCell.optionName.text = NSLocalizedString(VC_MENU_WALLET, nil);
     } else if(indexPath.section == TRIPS_SECTION){
     optionCell.optionImage.image = [UIImage imageNamed:@"icn_trips.png"];
     optionCell.optionName.text = NSLocalizedString(VC_MENU_UPCOMINGTRIPS, nil);
     } else if(indexPath.section == ACCOUNT_SECTION){
     optionCell.optionImage.image = [UIImage imageNamed:@"icn_account.png"];
     optionCell.optionName.text = NSLocalizedString(VC_MENU_ACCOUNT, nil);
     } else if(indexPath.section == MESSAGES){
     optionCell.optionImage.image = [UIImage imageNamed:@"icn_menu_messages.png"];
     optionCell.optionName.text = NSLocalizedString(VC_MENU_MESSAGES, nil);
     }*/
    return optionCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissMenuWithCompletion:^{
        /* if(indexPath.row == PROFILE_SECTION){
         [self.menuDelegate selectedMenuOption:PROFILE];
         }else*/
        if(!isGlobalLocale){
            [self movieToAppropriateScreenForLocalUser:indexPath];
        }
        else{
            [self movieToAppropriateScreenForGlobalUser:indexPath];
        }
    }];
}
-(void)movieToAppropriateScreenForLocalUser:(NSIndexPath *)indexPath{
    
    if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_TRAVEl_PREFERENCE]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_TRAVEL_PREFERENCE];
    }
    else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_ACTIVE_TRIPS]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_ACTIVE_TRIPS];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_MESSAGE]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_MESSAGE];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_WALLET]){
        if(isGlobalLocale && !isGlobalPayment) [self.menuDelegate selectedMenuOption:SIDE_MENU_WALLET];
        [self.menuDelegate selectedMenuOption:SIDE_MENU_WALLET];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_UPCOMING_TRIPS]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_UPCOMING_TRIPS];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_HISTORY]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_HISTORY];
    }
    else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_REFER_AND_EARN]){
        [self.menuDelegate selectedMenuOption:REFERANDEARN];
    }
    else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_REWARDS]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_REWARDS];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_SETTINGS]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_SETTINGS];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_ABOUT]){  //About
        [self.menuDelegate selectedMenuOption:SIDE_MENU_ABOUT];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_LEAVE_FEEDBACK]){  //feedback
        [self.menuDelegate selectedMenuOption:SIDE_MENU_LEAVE_FEEDBACK];
    }
}

-(void)movieToAppropriateScreenForGlobalUser:(NSIndexPath *)indexPath{
    if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_TRAVEl_PREFERENCE]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_TRAVEL_PREFERENCE];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_ACTIVE_TRIPS]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_ACTIVE_TRIPS];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_MESSAGE]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_MESSAGE];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_WALLET]){
        if(isGlobalLocale && !isGlobalPayment) [self.menuDelegate selectedMenuOption:SIDE_MENU_WALLET];
        [self.menuDelegate selectedMenuOption:SIDE_MENU_WALLET];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_UPCOMING_TRIPS]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_UPCOMING_TRIPS];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_HISTORY]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_HISTORY];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_SETTINGS]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_SETTINGS];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_ABOUT]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_ABOUT];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_LEAVE_FEEDBACK]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_LEAVE_FEEDBACK];
    }else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_REWARDS]){
        [self.menuDelegate selectedMenuOption:SIDE_MENU_REWARDS];
    }
    else if([[_dataOptinsArr objectAtIndex:indexPath.row]  isEqual: SIDE_MENU_NEW_REFER_AND_EARN]){
        [self.menuDelegate selectedMenuOption:REFERANDEARN];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 145.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frameWidth, 145)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = view.frame;
    [view addSubview:imageView];
    imageView.userInteractionEnabled = YES;
    imageView.backgroundColor = [UIColor whiteColor];
    // imageView.image = [UIImage imageNamed:@"rsz_nav_menu_bg.png"];
    [self createHeaderView:imageView];
    
    
    UITapGestureRecognizer *singleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(handleSingleTapOnView:)];
    [singleTapRecognizer setNumberOfTouchesRequired:1];
    [view addGestureRecognizer: singleTapRecognizer];
    
    
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

- (IBAction)dismissMenu:(id)sender {
    [self dismissMenuWithCompletion:nil];
}

-(void) dismissMenuWithCompletion:(void(^)())handler{
    [self.view removeConstraint:contentViewAfterAnimateConstraint];
    [self.view addConstraint:contentViewBeforeAnimateConstraint];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }completion:^(BOOL finished){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        if(handler) handler();
    }];
}

+(MenuViewController *)createAndOpenMenuWithSourceController:(UIViewController *)srcViewController andMenuDelegate:(id<MenuDelegate>)menuDelegate{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Commons" bundle:[NSBundle mainBundle]];
    MenuViewController *menuController = [storyBoard instantiateViewControllerWithIdentifier:@"menuView"];
    [srcViewController addChildViewController:menuController];
    [srcViewController.view addSubview:menuController.view];
    NSDictionary *viewsDictionary = @{@"menuView":menuController.view};
    menuController.view.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[menuView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[menuView]|" options:0 metrics:nil views:viewsDictionary];
    [srcViewController.view addConstraints:horizontalConsts];
    [srcViewController.view addConstraints:verticalConsts];
    menuController.menuDelegate = menuDelegate;
    return menuController;
}

- (IBAction) userSelectedSettings:(UIButton *)sender{
    [self dismissMenuWithCompletion:^{
        [self.menuDelegate selectedMenuOption:SIDE_MENU_SETTINGS];
    }];
}

- (IBAction) userSelectedAbout:(UIButton *)sender{
    [self dismissMenuWithCompletion:^{
        [self.menuDelegate selectedMenuOption:SIDE_MENU_ABOUT];
    }];
}

-(void)createHeaderView:(UIView *)superView{
    
    UIView *shadowView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, superView.frame.size.width, 140)];
    shadowView.backgroundColor = [UIColor clearColor];
    [superView addSubview:shadowView];
    
    shadowView.layer.shadowColor = [UIColor grayColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    shadowView.layer.shadowRadius = 8.0f;
    shadowView.layer.shadowOpacity = 0.7f;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(0.0, 0.0)];
    [path addLineToPoint:CGPointMake(0.0, CGRectGetHeight(shadowView.frame))];
    [path addLineToPoint:CGPointMake(CGRectGetWidth(shadowView.frame), CGRectGetHeight(shadowView.frame))];
    [path addLineToPoint:CGPointMake(CGRectGetWidth(shadowView.frame), 0.0)];
    shadowView.layer.shadowPath = path.CGPath;
    
    
    baseView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, superView.frame.size.width, 140)];
    baseView.backgroundColor = [UIColor whiteColor];
    [superView addSubview:baseView];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:baseView.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = baseView.bounds;
    maskLayer.path  = maskPath.CGPath;
    baseView.layer.mask = maskLayer;
    
    UIImageView *profileImageView = [[UIImageView alloc] init];
    profileImageView.frame = CGRectMake(15, 25, 50, 50);
    profileImageView.backgroundColor = [UIColor greenColor];
    profileImageView.layer.cornerRadius = profileImageView.frameHeight/2;
    profileImageView.clipsToBounds = YES;
    [baseView addSubview:profileImageView];
    CGFloat useranemLblfontSize = 15;
    CGFloat verificationPendingfontSize = 13;
    CGFloat profileCompLblfontSize = 11;
    if (IS_IPHONE_6) {
        useranemLblfontSize = 15;
        verificationPendingfontSize = 13;
        profileCompLblfontSize = 12;
    } else if (IS_IPHONE_6_PLUS) {
        useranemLblfontSize = 16;
        verificationPendingfontSize = 14;
        profileCompLblfontSize = 13;
    }  else if (IS_IPHONE_X) {
        useranemLblfontSize = 19;
        verificationPendingfontSize = 14;
        profileCompLblfontSize = 13;
    }
    
    
    UILabel *useranemLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImageView.frameMaxX + 5, profileImageView.frameY , baseView.frameWidth - (profileImageView.frameMaxX + 10+25), profileImageView.frameHeight)];
    useranemLbl.backgroundColor = [UIColor clearColor];
    useranemLbl.textColor = [UIColor blackColor];
    useranemLbl.font = [UIFont fontWithName:MediumFont size:useranemLblfontSize];
    useranemLbl.numberOfLines = 2;
    [baseView addSubview:useranemLbl];
    
    UIImageView *rightArrowImageView = [[UIImageView alloc] init];
    rightArrowImageView.frame = CGRectMake(useranemLbl.frameMaxX +2, useranemLbl.frame.origin.y + (useranemLbl.frameHeight - 25)/2, 25, 25);
    [rightArrowImageView setContentMode:UIViewContentModeScaleAspectFit];
    [rightArrowImageView setImage:[UIImage imageNamed:@"publish_right_arrow.png"]];
    rightArrowImageView.backgroundColor = [UIColor clearColor];
    rightArrowImageView.layer.cornerRadius = profileImageView.frameHeight/2;
    rightArrowImageView.clipsToBounds = YES;
    [baseView addSubview:rightArrowImageView];
    
    UIImageView *iconImg = [[UIImageView alloc] init];
    iconImg.frame = CGRectMake(profileImageView.frameX, profileImageView.frameMaxY+10, 15, 15);
    iconImg.backgroundColor = [UIColor clearColor];
    [baseView addSubview:iconImg];
    
    UILabel *verificationPending = [[UILabel alloc] initWithFrame:CGRectMake(iconImg.frameMaxX + 5, iconImg.frameY, baseView.frameWidth -(iconImg.frameMaxX + 5+5) , iconImg.frameHeight)];
    verificationPending.backgroundColor = [UIColor clearColor];
    [baseView addSubview:verificationPending];
    verificationPending.textColor = [UIColor lightGrayColor];
    verificationPending.font = [UIFont fontWithName:NormalFont size:verificationPendingfontSize];
    
    _completenessProgress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    _completenessProgress.progressTintColor = [UIColor colorWithRed:38.0/255 green:98.0/255 blue:255.0/255 alpha:1.0];
    [[_completenessProgress layer]setFrame:CGRectMake(profileImageView.frameX, iconImg.frameMaxY+5, baseView.frameWidth - 2*profileImageView.frameX  , 5)];
    _completenessProgress.trackTintColor = [UIColor lightGrayColor];
    [[_completenessProgress layer]setCornerRadius:_completenessProgress.frame.size.height / 2];
    [[_completenessProgress layer]setMasksToBounds:TRUE];
    _completenessProgress.clipsToBounds = YES;
    [baseView addSubview:_completenessProgress];
    
    _profileCompLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImageView.frameX, _completenessProgress.frameMaxY+5, baseView.frameWidth - 2*profileImageView.frameX , iconImg.frameHeight)];
    _profileCompLbl.backgroundColor = [UIColor clearColor];
    [baseView addSubview:_profileCompLbl];
    _profileCompLbl.textColor = verificationPending.textColor;
    _profileCompLbl.font = [UIFont fontWithName:NormalFont size:profileCompLblfontSize];
    
    [profileImageView sd_setImageWithURL:[NSURL URLWithString:userProfile.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
    }];
    NSString *nameStr = [NSString stringWithFormat:@"%@ %@",userProfile.firstName,userProfile.lastName];
    useranemLbl.text = [nameStr capitalizedString];
    if([@"VERIFIED" isEqualToString:userProfile.userStatus]){
        iconImg.image =[UIImage imageNamed:@"icn_menu_tick.png"];
        verificationPending.text = NSLocalizedString(VC_MENU_PROFILEVERIFICATION_DONE, nil);
    } else{
        iconImg.image =[UIImage imageNamed:@"ic_action_info.png"];
        verificationPending.text = NSLocalizedString(VC_MENU_PROFILEVERIFICATIONDUE, nil);
    }
    _profileCompleteness = [userProfile.profileCompleteness floatValue] / 100.0;
    if (userProfile.profileCompleteness.integerValue ==  100) {
        _completenessProgress.backgroundColor = [UIColor colorWithRed:127.0 / 255.0 green:255.0 / 255.0 blue:212.0 / 255.0 alpha:1.0];
        _profileCompLbl.textColor = [UIColor redColor];
    } else {
        _completenessProgress.backgroundColor = [UIColor colorWithRed:83.0 / 255.0 green:93.0 / 255.0 blue:92.0 / 255.0 alpha:1.0];
        _profileCompLbl.textColor = [UIColor redColor];
    }
    _completenessProgress.backgroundColor = [UIColor colorWithRed:38.0 / 255.0 green:98.0 / 255.0 blue:255.0 / 255.0 alpha:1.0];

    [self performSelector:@selector(increaseProgressValue) withObject:self afterDelay:0.4];
}

-(void)increaseProgressValue{
    if(self.completenessProgress.progress < self.profileCompleteness){
        progressValue = progressValue + 0.01;
        self.profileCompLbl.text = [NSString stringWithFormat:@"%@ %d%%",NSLocalizedString(CS_MENU_LBL_PROFILECOMPLETENESS, nil),(int)(progressValue * 100)];
        self.completenessProgress.progress = progressValue;
        [self performSelector:@selector(increaseProgressValue) withObject:self afterDelay:0.01];
    } else{
        progressValue = 0.0;
     //   self.profileCompLbl.text = [NSString stringWithFormat:@"%@ %.2f%%",NSLocalizedString(CS_MENU_LBL_PROFILECOMPLETENESS, nil),userProfile.profileCompleteness.floatValue];
        self.profileCompLbl.text = [NSString stringWithFormat:@"%@ %d%%",NSLocalizedString(CS_MENU_LBL_PROFILECOMPLETENESS, nil),userProfile.profileCompleteness.intValue];

    }
    //[self.profileCompLbl sizeToFit];
    if(self.profileCompLbl.frame.size.height <= 15){
        CGRect originalFrame = self.profileCompLbl.frame;
        originalFrame.size.height = 15;
        self.profileCompLbl.frame = originalFrame;
    }else{
        CGRect originalFrame = baseView.frame;
        originalFrame.size.height = self.profileCompLbl.frame.origin.y + self.profileCompLbl.frame.size.height + 5;
        baseView.frame = originalFrame;
    }
    
   // self.profileCompLbl.lineBreakMode = NSLineBreakByTruncatingMiddle;
}
-(void)addDropShadowForView:(UIView *)viewShadow{
    viewShadow.backgroundColor = [UIColor yellowColor];
    viewShadow.layer.shadowColor = [UIColor purpleColor].CGColor;
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = CGSizeZero;
    viewShadow.layer.shadowRadius = 5;
    viewShadow.layer.masksToBounds = NO;
}
-(void)handleSingleTapOnView:(id)sender{
    [self moveToProfileScreen];
}
-(void)moveToProfileScreen{
    [self dismissMenuWithCompletion:^{
        [self.menuDelegate selectedMenuOption:SIDE_MENU_PROFILE];
    }];
}
@end
