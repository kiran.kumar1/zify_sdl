//
//  GuestMenuController.h
//  zify
//
//  Created by Anurag S Rathor on 19/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

enum GuestMenuOptions {
    GUESTPROFILE,GUESTABOUT
};

@protocol GuestMenuDelegate <NSObject>
-(void) selectedMenuOption:(enum GuestMenuOptions)option;
@end

@interface GuestMenuController : UIViewController
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) id<GuestMenuDelegate> menuDelegate;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *contentViewLeftConstraint;
+(GuestMenuController *)createAndOpenGuestMenuWithSourceController:(UIViewController *)srcViewController andMenuDelegate:(id<GuestMenuDelegate>)menuDelegate;
@end
