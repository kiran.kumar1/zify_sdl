//
//  RideInvoiceController.swift
//  zify
//
//  Created by Anurag on 26/06/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class RideInvoiceController: ScrollableContentViewController, RateViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let COMMENTS_MAX_LENGTH = 200
    let COMMENTS_TEXT_TAG = 1234

    private var profileImagePlaceHolder: UIImage?
    private var isViewInitialised = false
    private var selectedRating: Float = 0
    private var ratingRide: TripRide?
    
    @IBOutlet weak var messageHandler: MessageHandler!
    var ratingResponse: CurrentTripNPendingRatingResponse?
    var showEmptyDrive = false
    
   //var arrRiders = ["Sanjay Sahu", "Chanchala Devi", "Sampoonesh babu", "Dwayne Johnson"]
    
    @IBOutlet weak var lblHowWasYourTrip: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet var tblRiderFeedback: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIScreen.main.sizeType == .iPhone5 {
            btnSubmit.titleLabel?.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            btnSubmit.titleLabel?.fontSize = 16
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            btnSubmit.titleLabel?.fontSize = 16
        } else {
            btnSubmit.titleLabel?.fontSize = 16
        }
        
        btnSubmit.setTitle(NSLocalizedString(CTS_DRIVEINVOICE_BTN_SUBMIT, comment: ""), for: .normal)
        self.navigationItem.title = NSLocalizedString(CTS_DRIVEINVOICE_NAV_TITLE, comment: "")
        self.tblRiderFeedback.rowHeight = UITableViewAutomaticDimension
        self.tblRiderFeedback.estimatedRowHeight = 44.0
        
        profileImagePlaceHolder = UIImage(named: "placeholder_profile_image")
        isViewInitialised = false
        automaticallyAdjustsScrollViewInsets = false
        
        ratingRide = ratingResponse?.pendingRideRating
        
        debugPrint(ratingRide)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
    }
   
    
   
    func rateView(_ rateView: RatingView?, ratingDidChange rating: Float) {
        selectedRating = rating
    }

    @IBAction func tapOnSupport(_ sender: UIBarButtonItem) {
        Freshchat.sharedInstance().showConversations(self)
    }
    
    @IBAction func submitRating(_ sender: UIButton) {
        if selectedRating == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_RIDEINVOICE_RATINGVAL, comment: ""), navigationCtrl: self.navigationController!)
            //errorText.text = NSLocalizedString(VC_RIDEINVOICE_RATINGVAL, comment: "")
            return
        }
        let commentTxt = ""
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance()?.getResponse(TripRatingsRequest.init(with: RIDERATING, andTripId: self.ratingRide?.rideId, andRatingTripIds: [self.ratingRide?.rideId! as Any], andRatings: [self.selectedRating], andComments: [commentTxt] as [Any] ), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
        
//                        self.willMove(toParentViewController: nil)
//                        self.beginAppearanceTransition(false, animated: false)
//                        self.endAppearanceTransition()
//                        self.view.removeFromSuperview()
//                        self.removeFromParentViewController()
                        self.dismiss(animated: true, completion:nil)
                        
                        if ((self.ratingResponse?.currentDrive) != nil) {
                            if self.showEmptyDrive || self.ratingResponse?.currentDrive.numOfSeats.intValue ?? 0 > 0 {
                                let currentDriveNavController = CurrentDriveController.createCurrentDriveNavController()
                                let currentDriveController = currentDriveNavController?.topViewController as? CurrentDriveController
                                currentDriveController?.currentDrive = self.ratingResponse?.currentDrive
                                AppUtilites.applicationVisibleViewController().present((currentDriveNavController ?? nil)!, animated: true)
                            } else if ((self.ratingResponse?.currentRide) != nil) {
                                let currentRideNavController = CurrentRideController.createCurrentRideNavController()
                                let currentRideController = currentRideNavController?.topViewController as? CurrentRideController
                                currentRideController?.currentRide = self.ratingResponse?.currentRide
                                AppUtilites.applicationVisibleViewController().present((currentRideNavController ?? nil)!, animated: true)
                            }
                        } else {
                            //self.errorText.text = (error as NSError?)?.userInfo["message"] as? String
                        }
                    }
                })
            })
        })
    }

    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replacementText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if textField.tag == COMMENTS_TEXT_TAG && (replacementText?.count ?? 0) > COMMENTS_MAX_LENGTH {
            return false
        }
        return true
    }

    /*
    class func showRideInvoiceController(_ viewController: UIViewController?, andRatingResponse ratingResponse: CurrentTripNPendingRatingResponse?, andShowEmptyDrive showEmptyDrive: Bool) {
        var viewController = viewController
        
        //let storyBoard = UIStoryboard(name: "CurrentTrip", bundle: Bundle.main)
        //chaged the storyboard CurrentTrip to Feedback storyboard
        let storyBoard = UIStoryboard(name: "Feedback", bundle: Bundle.main)
        if viewController?.navigationController != nil {
            viewController = viewController?.navigationController
        }
        
        
        let visibleController: UIViewController? = AppUtilites.applicationVisibleViewController()
        if visibleController?.presentedViewController != nil {
            visibleController?.dismiss(animated: false)
        }
        
        
        
        
        let rideInvoiceController = storyBoard.instantiateViewController(withIdentifier: "rideInvoiceController") as? RideInvoiceController
        rideInvoiceController?.ratingResponse = ratingResponse
        rideInvoiceController?.showEmptyDrive = showEmptyDrive
        if let rideInvoiceController = rideInvoiceController {
            viewController?.addChildViewController(rideInvoiceController)
        }
        if let view = rideInvoiceController?.view {
            viewController?.view.addSubview(view)
        }
        rideInvoiceController?.view.translatesAutoresizingMaskIntoConstraints = false
        
        let viewsDictionary = [
            "invoiceView": rideInvoiceController?.view
        ]
        
        let horizontalConsts = NSLayoutConstraint.constraints(withVisualFormat: "|[invoiceView]|", options: [], metrics: nil, views: viewsDictionary as [String : Any])
        let verticalConsts = NSLayoutConstraint.constraints(withVisualFormat: "V:|[invoiceView]|", options: [], metrics: nil, views: viewsDictionary as [String : Any])
        viewController?.view.addConstraints(horizontalConsts)
        viewController?.view.addConstraints(verticalConsts)
        rideInvoiceController?.view.alpha = 0.0
        rideInvoiceController?.beginAppearanceTransition(true, animated: true)
        rideInvoiceController?.view.alpha = 1.0
        rideInvoiceController?.endAppearanceTransition()
        rideInvoiceController?.didMove(toParentViewController: viewController)
        
    }
*/

    
    // MARK: - TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let sectionLabel = UILabel(frame: CGRect(x: 10, y: 10, width:
            tableView.bounds.size.width, height: 40))
        
        switch(section) {
        case 0 :
            sectionLabel.text =  NSLocalizedString(ride_details, comment: "")
        case 1 :
            sectionLabel.text =  NSLocalizedString(money_debited, comment: "")
        case 4 :
            sectionLabel.text =  NSLocalizedString(share_your_feedback, comment: "")
        default :sectionLabel.text =  ""
        }
        
        //sectionLabel.font = UIFont(name: BoldFont, size: 18)
        sectionLabel.font = setFontSizes(anyObj: sectionLabel, fontStyle: BoldFont, defaultSize: 17, iphone6Size: 17, iphoneXRSize: 17, iphone6PlusSize: 19, iphoneXSize: 19, iphoneXSMaxSize: 19)

        
        sectionLabel.textColor = UIColor.black
        sectionLabel.sizeToFit()
        headerView.addSubview(sectionLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 || section == 4 {
            return 40.0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellRiderInvoiceRideDetails : RiderInvoiceDetailCell = (self.tblRiderFeedback.dequeueReusableCell(withIdentifier: "RiderInvoiceDetailCell") as! RiderInvoiceDetailCell)
            //cellRideDetails.lblDistance.text = "992.3847568923 kms"
            
            cellRiderInvoiceRideDetails.lblSourceAddress.text = ratingRide?.driveDetail.srcAdd
            cellRiderInvoiceRideDetails.lblDestinationAddress.text = ratingRide?.driveDetail.destAdd
            
            cellRiderInvoiceRideDetails.lblDriveId.text = "\(NSLocalizedString(ride_id, comment: "")) : \(ratingRide?.rideId ?? 0)"
           
            let distanceValue = String(format: "%.02@", ratingRide?.distance ?? 0.0)
            cellRiderInvoiceRideDetails.lblDistance.text = "\(distanceValue) " + CurrentLocale.sharedInstance().getDistanceUnit()
            
            
            cellRiderInvoiceRideDetails.lblStartTime.text = getDateFormatToTimeForInvoice(strDate: "\(ratingRide?.rideStartTime ?? "") ") //"\(ratingRide?.rideStartTime ?? "") "
            cellRiderInvoiceRideDetails.lblEndTime.text = getDateFormatToTimeForInvoice(strDate: "\(ratingRide?.rideEndTime ?? "") ") //"\(ratingRide?.rideEndTime ?? "") "
            
            cellRiderInvoiceRideDetails.lblDistanceTitle.text = NSLocalizedString(trip_distance, comment: "")
            cellRiderInvoiceRideDetails.lblStartTimeTitle.text = NSLocalizedString(start_time, comment: "")
            cellRiderInvoiceRideDetails.lblEndTimeTitle.text = NSLocalizedString(end_time, comment: "")
            
            cellRiderInvoiceRideDetails.lblSourceAddress.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblSourceAddress, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 15, iphoneXSize: 14, iphoneXSMaxSize: 15)
            cellRiderInvoiceRideDetails.lblDestinationAddress.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblDestinationAddress, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 15, iphoneXSize: 14, iphoneXSMaxSize: 15)
            cellRiderInvoiceRideDetails.lblDriveId.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblDriveId, fontStyle: NormalFont, defaultSize: 13, iphone6Size: 13, iphoneXRSize: 13, iphone6PlusSize: 14, iphoneXSize: 14, iphoneXSMaxSize: 14)
            cellRiderInvoiceRideDetails.lblDistance.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblDistance, fontStyle: BoldFont, defaultSize: 13, iphone6Size: 13, iphoneXRSize: 13, iphone6PlusSize: 15, iphoneXSize: 14, iphoneXSMaxSize: 15)
            cellRiderInvoiceRideDetails.lblStartTime.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblStartTime, fontStyle: BoldFont, defaultSize: 13, iphone6Size: 13, iphoneXRSize: 13, iphone6PlusSize: 15, iphoneXSize: 14, iphoneXSMaxSize: 15)
            cellRiderInvoiceRideDetails.lblEndTime.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblEndTime, fontStyle: BoldFont, defaultSize: 13, iphone6Size: 13, iphoneXRSize: 13, iphone6PlusSize: 15, iphoneXSize: 14, iphoneXSMaxSize: 15)
            cellRiderInvoiceRideDetails.lblDistanceTitle.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblDistanceTitle, fontStyle: NormalFont, defaultSize: 12, iphone6Size: 12, iphoneXRSize: 12, iphone6PlusSize: 14, iphoneXSize: 13, iphoneXSMaxSize: 14)
            cellRiderInvoiceRideDetails.lblStartTimeTitle.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblStartTimeTitle, fontStyle: NormalFont, defaultSize: 12, iphone6Size: 12, iphoneXRSize: 12, iphone6PlusSize: 14, iphoneXSize: 13, iphoneXSMaxSize: 14)
            cellRiderInvoiceRideDetails.lblEndTimeTitle.font = setFontSizes(anyObj: cellRiderInvoiceRideDetails.lblEndTimeTitle, fontStyle: NormalFont, defaultSize: 12, iphone6Size: 12, iphoneXRSize: 12, iphone6PlusSize: 14, iphoneXSize: 13, iphoneXSMaxSize: 14)
            
            
            return cellRiderInvoiceRideDetails
            
        } else if indexPath.section == 1 {
            let cellMoneyDebtdUser : RiderInvoiceMoneyDebitedUserCell = (self.tblRiderFeedback.dequeueReusableCell(withIdentifier: "RiderInvoiceMoneyDebitedUserCell") as? RiderInvoiceMoneyDebitedUserCell)!
            cellMoneyDebtdUser.lblUserName.text = ratingRide?.driverDetail.firstName.capitalized//arrRiders[indexPath.row]
            
            let driveDetail = ratingRide?.driverDetail
            
            //let riderDetail = ratingRide?.driveRiders[indexPath.row] as? TripDriveRiderDetail
            let perKmRateValue = driveDetail?.regionDetail.perKmRate.stringValue ?? " "

            let distanceValue = String(format: "%.02@", ratingRide?.distance ?? 0.0)
            cellMoneyDebtdUser.lblKms.text = "\(distanceValue) " + CurrentLocale.sharedInstance().getDistanceUnit() + " x " + perKmRateValue //" x 3 "
            
            let amountValue = String(format: "%.02@", (ratingRide?.zifyPoints)!)
            cellMoneyDebtdUser.lblPrice.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator: amountValue)
            
            
            cellMoneyDebtdUser.lblUserName.font = setFontSizes(anyObj: cellMoneyDebtdUser.lblUserName, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellMoneyDebtdUser.lblKms.font = setFontSizes(anyObj: cellMoneyDebtdUser.lblKms, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 15, iphoneXSize: 15, iphoneXSMaxSize: 16)
            cellMoneyDebtdUser.lblPrice.font = setFontSizes(anyObj: cellMoneyDebtdUser.lblPrice, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 15, iphoneXSize: 15, iphoneXSMaxSize: 15)
            
            return cellMoneyDebtdUser
            
        } else if indexPath.section == 2 {
            
            let cellRiderInvoiceMoneyDebtdTotal : RiderInvoiceMoneyDebitedTotalCell = (self.tblRiderFeedback.dequeueReusableCell(withIdentifier: "RiderInvoiceMoneyDebitedTotalCell") as? RiderInvoiceMoneyDebitedTotalCell)!
            cellRiderInvoiceMoneyDebtdTotal.lblTotalTitle.text = "\(NSLocalizedString(total, comment: "")) "
         
            let amountValue = String(format: "%.02@", (ratingRide?.zifyPoints)!)
            cellRiderInvoiceMoneyDebtdTotal.lblTotalPrice.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator: amountValue)
            
            cellRiderInvoiceMoneyDebtdTotal.lblTotalTitle.font = setFontSizes(anyObj: cellRiderInvoiceMoneyDebtdTotal.lblTotalTitle, fontStyle: BoldFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellRiderInvoiceMoneyDebtdTotal.lblTotalPrice.font = setFontSizes(anyObj: cellRiderInvoiceMoneyDebtdTotal.lblTotalPrice, fontStyle: BoldFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            
            return cellRiderInvoiceMoneyDebtdTotal
            
        } else if indexPath.section == 3 {
            let cellRiderInvoiceZifyCoinsEarned : RiderInvoiceZifyCoinsEarnedCell = (self.tblRiderFeedback.dequeueReusableCell(withIdentifier: "RiderInvoiceZifyCoinsEarnedCell") as? RiderInvoiceZifyCoinsEarnedCell)!
            cellRiderInvoiceZifyCoinsEarned.lblZifyCoinsEarnedTitle.text = NSLocalizedString(zify_coins_earned, comment: "")
            cellRiderInvoiceZifyCoinsEarned.lblZifyCoinsCount.text = "\(ratingRide?.zifyCoinsEarned ?? "0")"
            cellRiderInvoiceZifyCoinsEarned.lblzifyCoinsDescription.text = NSLocalizedString(rider_coins_description, comment: "")
            
            cellRiderInvoiceZifyCoinsEarned.lblZifyCoinsEarnedTitle.font = setFontSizes(anyObj: cellRiderInvoiceZifyCoinsEarned.lblZifyCoinsEarnedTitle, fontStyle: BoldFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellRiderInvoiceZifyCoinsEarned.lblZifyCoinsCount.font = setFontSizes(anyObj: cellRiderInvoiceZifyCoinsEarned.lblZifyCoinsCount, fontStyle: BoldFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellRiderInvoiceZifyCoinsEarned.lblzifyCoinsDescription.font = setFontSizes(anyObj: cellRiderInvoiceZifyCoinsEarned.lblzifyCoinsDescription, fontStyle: NormalFont, defaultSize: 12, iphone6Size: 12, iphoneXRSize: 12, iphone6PlusSize: 13, iphoneXSize: 13, iphoneXSMaxSize: 13)
            
            return cellRiderInvoiceZifyCoinsEarned
            
        } else if indexPath.section == 4 {
            let cellRiderInvoiceRatingCell : RiderInvoiceRatingCell = (self.tblRiderFeedback.dequeueReusableCell(withIdentifier: "RiderInvoiceRatingCell") as? RiderInvoiceRatingCell)!
            cellRiderInvoiceRatingCell.lblUserName?.text = ratingRide?.driverDetail.firstName.capitalized ?? "" //arrRiders[indexPath.row]
            
            cellRiderInvoiceRatingCell.imgProfile.sd_setImage(with: URL(string: ratingRide?.driverDetail.profileImgUrl ?? ""), placeholderImage: profileImagePlaceHolder, completed: { image, error, cacheType, imageURL in
            })
            //cellRiderInvoiceRatingCell.ratingView.rating = 0.0
            
            cellRiderInvoiceRatingCell.ratingView.rating = selectedRating
            cellRiderInvoiceRatingCell.ratingView.editable = true
            cellRiderInvoiceRatingCell.ratingView.delegate = self
            cellRiderInvoiceRatingCell.ratingView.backgroundColor = UIColor.white

            cellRiderInvoiceRatingCell.lblUserName.font = setFontSizes(anyObj: cellRiderInvoiceRatingCell.lblUserName, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            
            return cellRiderInvoiceRatingCell
        }
        
        return UITableViewCell()
        
    }
    

    class func createRideInvoiceNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Feedback", bundle: Bundle.main)
        let rideeInvoiceNavController = storyBoard.instantiateViewController(withIdentifier: "RideInvoiceNavigationController") as? UINavigationController
        return rideeInvoiceNavController
    }
}

class RiderInvoiceDetailCell : UITableViewCell {
    
    @IBOutlet weak var lblSourceAddress: UILabel!
    @IBOutlet weak var lblDestinationAddress: UILabel!
    @IBOutlet weak var lblDistanceTitle: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblStartTimeTitle: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTimeTitle: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblDriveId: UILabel!
}

class RiderInvoiceMoneyDebitedUserCell : UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblKms: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
}

class RiderInvoiceMoneyDebitedTotalCell : UITableViewCell {
    
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
}

class RiderInvoiceZifyCoinsEarnedCell : UITableViewCell {
    
    @IBOutlet weak var lblZifyCoinsEarnedTitle: UILabel!
    @IBOutlet weak var lblZifyCoinsCount: UILabel!
    @IBOutlet weak var lblzifyCoinsDescription: UILabel!
}

class RiderInvoiceRatingCell : UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var ratingView: RatingView!
}

class RiderInvoiceSubmitCell : UITableViewCell {
    
    @IBOutlet weak var btnSubmit: UIButton!
}

