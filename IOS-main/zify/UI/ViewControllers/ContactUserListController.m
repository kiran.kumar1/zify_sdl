//
//  ContactUserListController.m
//  zify
//
//  Created by Anurag S Rathor on 05/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "ContactUserListController.h"
#import "ContactUserDetailCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UserContactDetail.h"
#import "ServerInterface.h"
#import "UserCallRequest.h"
#import "AppUtilites.h"
#import "zify-Swift.h"

@interface ContactUserListController ()

@end

@implementation ContactUserListController{
    UIImage *profileImagePlaceHolder;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 110.f;
    _tableView.hidden = true;
    // Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view removeConstraint:_tableViewTopConstraint];
    [self.view removeConstraint:_tableViewBottomConstraint];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView addConstraint:[NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:_tableView.contentSize.height]];
    _tableView.hidden = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table view delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactUserDetailCell *userDetailCell = [tableView dequeueReusableCellWithIdentifier:@"contactUserDetailCell"];
    UserContactDetail *contactDetail = [_contactUsers objectAtIndex:indexPath.row];
    userDetailCell.userImage.hidden = YES;
    userDetailCell.userImage.layer.cornerRadius = userDetailCell.userImage.frame.size.width / 2;
    userDetailCell.userImage.clipsToBounds = YES;
    [userDetailCell.userImage sd_setImageWithURL:[NSURL URLWithString:contactDetail.userImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        userDetailCell.userImage.hidden = NO;
    }];
    userDetailCell.userName.text = [NSString stringWithFormat:@"%@ %@",contactDetail.firstName,contactDetail.lastName];
    if(contactDetail.chatIdentifier) userDetailCell.chatUserView.hidden = NO;
    else userDetailCell.chatUserView.hidden = YES;
    if(contactDetail.callId) userDetailCell.callUserView.hidden = NO;
    else userDetailCell.callUserView.hidden = YES;
    if(indexPath.row == _contactUsers.count - 1) userDetailCell.separatorLine.hidden = YES;
    else userDetailCell.separatorLine.hidden = NO;
    return userDetailCell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_contactUsers count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}


-(IBAction) showChat:(UIButton *)sender{
    [self close];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    UserContactDetail *contactDetail = [_contactUsers objectAtIndex:indexPath.row];
    UINavigationController *chatNavController = [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    chatUserListContoller.receivingUser = contactDetail;
    [_srcController presentViewController:chatNavController animated:YES completion:nil];
}

-(IBAction) showCall:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    UserContactDetail *contactDetail = [_contactUsers objectAtIndex:indexPath.row];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[UserCallRequest alloc] initWithRequestType:INITIATECALL andCalleeId:contactDetail.callId] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                 if(response && ![response.responseObject isKindOfClass:[NSNull class]]){
                     [self close];
                     [AppUtilites openCallAppWithNumber:response.responseObject];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(IBAction) closeContacListController:(id)sender{
    [self close];
}

-(void)close{
    [self willMoveToParentViewController:nil];
    [self beginAppearanceTransition:NO animated:NO];
    [self endAppearanceTransition];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}

+(ContactUserListController *)createAndOpenContactListController:(UIViewController *)srcViewController WithUserContacts:(NSArray *)contacts{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Chat" bundle:[NSBundle mainBundle]];
    ContactUserListController *contactController = [storyBoard instantiateViewControllerWithIdentifier:@"contactUserListContoller"];
    contactController.srcController = srcViewController;
    contactController.contactUsers = contacts;
    contactController.view.frame =CGRectMake(0, 0, srcViewController.view.frame.size.width, srcViewController.view.frame.size.height);
    [srcViewController addChildViewController:contactController];
    [srcViewController.view addSubview:contactController.view];
    contactController.view.alpha = 0.0;
    [contactController beginAppearanceTransition:YES animated:YES];
    [UIView
     animateWithDuration:0.3
     delay:0.0
     options:UIViewAnimationOptionCurveEaseIn
     animations:^(void){
         contactController.view.alpha = 1.0;
     }
     completion:^(BOOL finished) {
         [contactController endAppearanceTransition];
         [contactController didMoveToParentViewController:srcViewController];
     }
     ];
    return contactController;
}
@end
