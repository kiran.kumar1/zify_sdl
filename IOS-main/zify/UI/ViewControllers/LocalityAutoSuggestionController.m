//
//  LocalityAutoSuggestionController.m
//  zify
//
//  Created by Anurag S Rathor on 21/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "LocalityAutoSuggestionController.h"
#import "UIImage+ProportionalFill.h"
#import "UserProfile.h"
#import "UserPreferences.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "SearchRideViewController.h"
#import "LocalAddressSearch.h"
#import "DBUserDataInterface.h"

@import GooglePlaces;

#define PICK_MAP_SECTION  0
#define USER_ADDRESSES_SECTION 1

#define HOME_ADDRESS_ROW 0
#define OFFICE_ADDRESS_ROW 1


@interface LocalityAutoSuggestionController ()

@end

@implementation LocalityAutoSuggestionController{
    NSArray *filteredLocalities;
    UIImage *locationImage;
    UIImage *historyImage;
    UIImage *pickMapImage;
    UIImage *homeAddressImage;
    UIImage *officeAddressImage;
    UserPreferences *currentUserPreferences;
    MapLocationHelper *mapLocationHelper;
    UserProfile *currentUser;
    BOOL isGlobalLocale;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(_isForSourceAddress){
        if(_sourceTextField.text.length > 0){
            [_sourceTextField selectAll:nil];
        }
        [_sourceTextField becomeFirstResponder];
    }else{
        [_destinationTextField becomeFirstResponder];
        if(_destinationTextField.text.length > 0){
            [_destinationTextField selectAll:nil];
        }
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addKeyboardNotifications];
   // self.sourceImageView.layer.borderColor = [UIColor whiteColor].CGColor;
   // self.sourceImageView.layer.borderWidth = 3.0;
    self.sourceImageView.backgroundColor = [UIColor clearColor];
    self.sourceImageView.image = [UIImage imageNamed:@"source_123.png"];
  //  self.sourceImageView.layer.cornerRadius = self.sourceImageView.frameHeight/2;
   //self.sourceImageView.clipsToBounds = YES;
    
    self.destImageView.layer.cornerRadius = self.sourceImageView.frameHeight/2;
    self.destImageView.clipsToBounds = YES;
    self.destImageView.image = [[AppDelegate getAppDelegateInstance] getNewImageWithOldImage:[UIImage imageNamed:@"loc_123.png"] withNewColor:[UIColor whiteColor]];
    [self addBottomborder:_sourceTextField];
    [self addBottomborder:_destinationTextField];
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, _tableView.bounds.size.width, 0.01f)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    pickMapImage= [UIImage imageNamed:@"icn_onboard_pickmap.png"];
    homeAddressImage = [UIImage imageNamed:@"icn_onboard_homeadress.png"];
    officeAddressImage = [UIImage imageNamed:@"icn_onboard_officeaddress.png"];
    locationImage = [UIImage imageNamed:@"futuro_icons_352.png"];
    historyImage = [UIImage imageNamed:@"futuro_icons_146.png"];

    currentUser = [UserProfile getCurrentUser];
    if(currentUser) currentUserPreferences = currentUser.userPreferences;
    mapLocationHelper = [[MapLocationHelper alloc] init];
    mapLocationHelper.mapLocationDelegate = self;
    isGlobalLocale = [[CurrentLocale sharedInstance] isGlobalLocale];
    _sourceTextField.text = _sourceLocality.address;
    _destinationTextField.text = _destinationLocality.address;
    //Need to call this to update the flag
    oldHistoryDataArr = [self getRecentHistoryForSearchAddresses:YES];
    [self.tableView reloadData];
    // Do any additional setup after loading the view.
   // NSArray *oldSearchData = [self getRecentHistoryForSearchAddresses];
}
-(void)addKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(textFieldtextLength <=2 && !_showAutosuggestOnly) {
        if(!isOldSearchAvailable){
            //NSLog(@"sections is %d", sectionsCount);
            sectionsCount = 2;
            return sectionsCount;
        }else{
            sectionsCount = 3;
            return sectionsCount;
        }
    }
    sectionsCount = 1;
    return sectionsCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(textFieldtextLength <=2 && !_showAutosuggestOnly) {
        if(isOldSearchAvailable && section == sectionsCount - 3 ){
            return oldHistoryDataArr.count;
        }
        if(section == sectionsCount - 2) {
           // if(!isGlobalLocale) return 1;
           // else return 0;
            return 1;
        }
        if(section == sectionsCount - 1) {
            if(currentUser) { //Not to show add home/work for guest user
                if(currentUserPreferences) return 2;
                return 1;
            }
            return 0;
        }
    }
    return filteredLocalities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if(textFieldtextLength <=2 && !_showAutosuggestOnly){
        if(isOldSearchAvailable && indexPath.section == sectionsCount - 3 ){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
                [cell willTransitionToState:UITableViewCellStateEditingMask];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.textLabel.font = [UIFont fontWithName:NormalFont size:13.0];
            cell.imageView.contentMode = UIViewContentModeScaleToFill;
            cell.imageView.image = [historyImage imageScaledToFitSize:CGSizeMake(24,24)];
            
           // CGFloat widthScale = tableView.frameWidth / 24;
           // CGFloat heightScale = 55 / 24;
            //this line will do it!
            
            LocalityInfo *objLocation = [oldHistoryDataArr objectAtIndex:indexPath.row];
            cell.textLabel.text = objLocation.address;
            cell.imageView.backgroundColor = [UIColor clearColor];
            return cell;
        }
        if(indexPath.section == sectionsCount - 2){
            static NSString *pickMapCellIdentifier = @"pickMapCell";
            cell = [tableView dequeueReusableCellWithIdentifier:pickMapCellIdentifier];
            if(cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:pickMapCellIdentifier];
                [cell willTransitionToState:UITableViewCellStateEditingMask];
            }
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.textLabel.font = [UIFont fontWithName:NormalFont size:14.0];
            cell.imageView.image = [pickMapImage imageScaledToFitSize:CGSizeMake(25.0f,25.0f)];
            cell.textLabel.text = NSLocalizedString(VC_LAS_PICKONMAP, nil);
            cell.imageView.backgroundColor = [UIColor clearColor];
        } else if(indexPath.section == sectionsCount - 1){
            if(currentUserPreferences){
                static NSString *addressCellIdentifier = @"addressCell";
                cell = [tableView dequeueReusableCellWithIdentifier:addressCellIdentifier];
                if(cell == nil)
                {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:addressCellIdentifier];
                    [cell willTransitionToState:UITableViewCellStateEditingMask];
                }
                cell.textLabel.textColor = [UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0];
                cell.textLabel.font = [UIFont fontWithName:NormalFont size:14.0];
                cell.detailTextLabel.textColor = [UIColor lightGrayColor];
                cell.detailTextLabel.font = [UIFont fontWithName:NormalFont size:12.0];
                if(indexPath.row == HOME_ADDRESS_ROW){
                    cell.imageView.image = [homeAddressImage imageScaledToFitSize:CGSizeMake(28.0f,28.0f)];
                    cell.textLabel.text = NSLocalizedString(VC_LAS_HOME, nil);
                    cell.detailTextLabel.text = currentUserPreferences.sourceAddress;
                    
                } else if(indexPath.row == OFFICE_ADDRESS_ROW){
                    cell.imageView.image = [officeAddressImage imageScaledToFitSize:CGSizeMake(28.0f,28.0f)];
                    cell.textLabel.text = NSLocalizedString(VC_LAS_OFFICE, nil);
                    cell.detailTextLabel.text = currentUserPreferences.destinationAddress;
                }
            } else {
                static NSString *addAddressCellIdentifier = @"addAddressCell";
                cell = [tableView dequeueReusableCellWithIdentifier:addAddressCellIdentifier];
                if(cell == nil)
                {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addAddressCellIdentifier];
                    [cell willTransitionToState:UITableViewCellStateEditingMask];
                }
                cell.textLabel.textColor = [UIColor lightGrayColor];
                cell.textLabel.font = [UIFont fontWithName:NormalFont size:14.0];
                cell.imageView.image = [officeAddressImage imageScaledToFitSize:CGSizeMake(25.0f,23.0f)];
                cell.textLabel.text = NSLocalizedString(VC_LAS_ADDPREFERENCES, nil);
           }
        }
 
    } else{
        static NSString *locationCellIdentifier = @"locationCell";
        cell = [tableView dequeueReusableCellWithIdentifier:locationCellIdentifier];
        
        id filteredResult = [filteredLocalities objectAtIndex:indexPath.row];
        //if([filteredResult isKindOfClass:[GMSAutocompletePrediction class]]){
            GMSAutocompletePrediction *result = (GMSAutocompletePrediction *)filteredResult;
            if(cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:locationCellIdentifier];
                [cell willTransitionToState:UITableViewCellStateEditingMask];
            }
        cell.imageView.contentMode = UIViewContentModeScaleToFill;
            cell.detailTextLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.font = [UIFont fontWithName:NormalFont size:12.0];
            cell.textLabel.attributedText = result.attributedPrimaryText;
          cell.detailTextLabel.attributedText = result.attributedSecondaryText;
         /* } else if([filteredResult isKindOfClass:[NMAAutoSuggest class]]){
            NMAAutoSuggest *result = (NMAAutoSuggest *)filteredResult;
            if(cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:locationCellIdentifier];
                [cell willTransitionToState:UITableViewCellStateEditingMask];
            }
            cell.textLabel.text = result.title;
        }*/
        cell.imageView.image = [locationImage imageScaledToFitSize:CGSizeMake(24,24)];

        cell.textLabel.textColor = [UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0];
        cell.textLabel.font = [UIFont fontWithName:NormalFont size:14.0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (IBAction)dismiss:(id)sender {
    [_searchBar resignFirstResponder];
    [self.view endEditing:YES];
    BOOL isAnimated = YES;
     if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
         isAnimated = NO;
     }
    [self.navigationController dismissViewControllerAnimated:isAnimated completion:nil];
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    return view;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try{
        [self.view endEditing:YES];
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            
            if(textFieldtextLength <= 2 && !_showAutosuggestOnly){
                if(isOldSearchAvailable && indexPath.section == sectionsCount - 3){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        [self selectAppropriateLocationFromPreviousHistory:indexPath];
                    }];
                }
                else if(indexPath.section == sectionsCount - 2){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        [mapLocationHelper showPickOnMap:[self navigationController]];
                    }];
                } else if(indexPath.section ==  sectionsCount - 1){
                    if(currentUserPreferences){
                        [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                            
                            if(indexPath.row == HOME_ADDRESS_ROW){
                                [self completeLocationHandlingWithCoordinate:CLLocationCoordinate2DMake(currentUserPreferences.sourceLatitude.doubleValue, currentUserPreferences.sourceLongitude.doubleValue) andAddress:currentUserPreferences.sourceAddress];
                            } else if(indexPath.row == OFFICE_ADDRESS_ROW){
                                [self completeLocationHandlingWithCoordinate:CLLocationCoordinate2DMake(currentUserPreferences.destinationLatitude.doubleValue, currentUserPreferences.destinationLongitude.doubleValue) andAddress:currentUserPreferences.destinationAddress];
                            }
                        }];
                    }else{
                        [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                            UserAddressPreferencesController *addressPreferencesController = [UserAddressPreferencesController createUserAddressPreferencesController];
                            addressPreferencesController.showPageIndicator = NO;
                            addressPreferencesController.isUpgradeFlow = NO;
                            addressPreferencesController.preferencesDelegate = self;
                            [self presentViewController:addressPreferencesController animated:NO completion:nil];
                        }];
                    }
                }
                
            } else{
                if(filteredLocalities.count - 1 < indexPath.row){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        
                    }];
                } else{
                    if(indexPath.row < filteredLocalities.count){
                    [mapLocationHelper getLocationInfoFromAutosuggest:[filteredLocalities objectAtIndex:indexPath.row]];
                    }
                }
            }
        }];
    }@catch(NSException *exp){
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}
//_searchBar.text.length ==> textFieldtextLength
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(textFieldtextLength <=2 && !_showAutosuggestOnly) {
        if(isOldSearchAvailable && section == sectionsCount - 3){
            return 20.0f;
        }
        return 20.0f;
    }
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
      return 0.01f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    if(isOldSearchAvailable && section == sectionsCount - 3){
        view.frame = CGRectMake(0, 0, tableView.frameWidth, 20);
        view.backgroundColor =[UIColor clearColor];
        
        UILabel *lblHistory = [[UILabel alloc] init];
        lblHistory.frame = CGRectMake(10, 0, view.frameWidth, view.frameHeight);
        lblHistory.text = NSLocalizedString(AUTO_FILL_LOCATION_RECENT_HISTORY, nil);
        lblHistory.font = [UIFont fontWithName:NormalFont size:11];
        [view addSubview:lblHistory];
    }
    return view;
}



-(void)completeLocationHandlingWithCoordinate:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)currentAddress{
    [mapLocationHelper getLocalityInfoWithCoordinate:coordinate andAddress:currentAddress];
}

#pragma mark - UISearchBarDelegate methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [mapLocationHelper getAutoSuggestResultsForQuery:searchText];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}
#pragma Text Field Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == _sourceTextField){
        _isForSourceAddress = YES;
    }else{
        _isForSourceAddress = NO;
    }
    if(textField.text.length > 0){
        [textField selectAll:nil];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * searchText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(searchText.length == 0){
        if(_sourceTextField == textField){
            _sourceLocality = nil;
        }
        if(_destinationTextField == textField){
            _destinationLocality = nil;
        }
    }
    //NSLog(@"%@",searchText);
    textFieldtextLength = (int)searchText.length;
    [mapLocationHelper getAutoSuggestResultsForQuery:searchText];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(_sourceTextField == textField){
        if(textField.text.length == 0)
           _sourceLocality = nil;
    }
    if(_destinationTextField == textField){
        if(textField.text.length == 0)
        _destinationLocality = nil;
    }
    [self.view endEditing:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}
#pragma mark - MapLocationHelperDelegate methods

-(void) selectedLocationInfo:(LocalityInfo *)locationInfo andError:(NSError *)error{
    if(error != nil){
        [self.messageHandler dismissBlockingLoadViewWithHandler:^{
            [self.messageHandler showErrorMessage:NSLocalizedString(VC_LAS_LOCATIONERROR, nil)];
        }];
        
    } else{
        if(locationInfo != nil){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(self.isForSourceAddress){
                    _sourceLocality = locationInfo;
                }else{
                    _destinationLocality = locationInfo;
                }
                [self saveSearchLocalitiesDataintoArray:locationInfo];
                [self moveToSearchScreenAfterPickingLocations];
                
                //[self.selectionDelegate selectedLocality:locationInfo];
                //[self.selectionDelegate selectedLocality:_sourceLocality withDestnationLocation:_destinationLocality];
               // [self dismissViewControllerAnimated:NO completion:nil];
            }];
        } else{
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                
            }];
        }
    }
}

-(void) queryResultsForAutosuggest:(NSArray *)results{
    filteredLocalities = results;
    // NEed to call this to update the boolean flag
    //oldHistoryDataArr = [self getRecentHistoryForSearchAddresses];
    [self.tableView reloadData];
}

#pragma mark - UserAddressPreferencesDelegate methods

-(void)updateUserAddressPreferences{
    UserProfile *newUser = [UserProfile getCurrentUser];
    currentUser = newUser;
    currentUserPreferences = currentUser.userPreferences;
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:USER_ADDRESSES_SECTION] withRowAnimation:UITableViewRowAnimationBottom];
}

+(UINavigationController *)createLocalityAutoSuggestNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *autosuggestNavController = [storyBoard instantiateViewControllerWithIdentifier:@"localityAutosuggestNavController"];
    return autosuggestNavController;
}

-(void)addBottomborder:(UIView *)view{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, view.frame.size.height - borderWidth, view.frameWidth, 1);
    border.borderWidth = borderWidth;
    [view.layer addSublayer:border];
    view.layer.masksToBounds = YES;
}
- (IBAction)btnSwapTapped:(UIButton *)sender{
    [UIView animateWithDuration:0.6f delay:0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [sender setTransform:CGAffineTransformRotate(sender.transform, 1*M_PI)];
    } completion:nil];
    
    LocalityInfo *locInfo = _sourceLocality;
    _sourceLocality = _destinationLocality;
    _destinationLocality = locInfo;
    NSString *sourceAddreessStr = _sourceTextField.text;
    _sourceTextField.text = _destinationTextField.text;
    _destinationTextField.text = sourceAddreessStr;
    
    
    if(_sourceLocality && _destinationLocality){
     // LocalityInfo *tempLocalityInfo = _sourceLocality;
    // _sourceLocality = _destinationLocality;
   //  _destinationLocality = tempLocalityInfo;
        [self moveToSearchScreenAfterPickingLocations];
    }else if(_sourceTextField.text.length == 0){
        [_sourceTextField becomeFirstResponder];
    }else if(_destinationTextField.text.length == 0){
        [_destinationTextField becomeFirstResponder];
    }
}

-(void)saveSearchLocalitiesDataintoArray:(LocalityInfo *)searchedAddress{
    @try{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    NSArray *storedDataArr = [self getRecentHistoryForSearchAddresses:NO];
    int index = -1;
    for(int i=0; i<storedDataArr.count; i++){
        LocalAddressSearch *obj = [storedDataArr objectAtIndex:i];
        if([obj.address isEqualToString:searchedAddress.address]){
            index = i;
        }
    }
    if(index >= 0){
        [LocalAddressSearch deleteOldFirstRecordIfCountIsGreaterThanFIVEInContext:context atIndex:index];
    }
    NSUInteger searchesCount = [LocalAddressSearch getUserSearchAddressCountInContext:context];
    NSString *latStr = [[NSString alloc] initWithFormat:@"%f", searchedAddress.latLng.latitude];
    NSString *longStr = [[NSString alloc] initWithFormat:@"%f", searchedAddress.latLng.longitude];
    
    if(searchesCount >= 5){
        [LocalAddressSearch deleteOldFirstRecordIfCountIsGreaterThanFIVEInContext:context atIndex:0];
    }
    [LocalAddressSearch insertAddressSearchIntoDB:searchedAddress.name andAddress:searchedAddress.address andCity:searchedAddress.city andState:searchedAddress.state andCountry:searchedAddress.country andLatitude:latStr andLongitude:longStr inContext:context];
    }@catch(NSException *exception){
        NSLog(@"exception is %@", [exception description]);
    }

   //  searchesCount = [LocalAddressSearch getUserSearchAddressCountInContext:context];


   /* NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSArray *searchesLocationsArray = [self getRecentHistoryForSearchAddresses:NO];
    NSMutableArray *locatinsArray = [[NSMutableArray alloc] init];
    for(LocalityInfo *location in searchesLocationsArray){
        if(![searchedAddress.address isEqualToString:location.address]){
            [locatinsArray addObject:location];
        }
    }
    [locatinsArray addObject:searchedAddress];
    if(locatinsArray.count > 5){
        [locatinsArray removeObjectAtIndex:0];
    }
    NSData *locationEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:locatinsArray];
    [prefs setObject:locationEncodedObject forKey:@"locationsSearchFromLocal"];
    [prefs synchronize];
    NSLog(@"search date history is %@", locatinsArray);*/
}

-(NSArray *) getRecentHistoryForSearchAddresses:(BOOL)isRevrsed
{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    NSUInteger searchesCount = [LocalAddressSearch getUserSearchAddressCountInContext:context];
    NSLog(@"count is %lu", (unsigned long)searchesCount);

    NSArray *fetchedObjsArr = [LocalAddressSearch getSearchDataArrayFrmDBInContext:context];
    NSMutableArray *actualResultsArr = [[NSMutableArray alloc] init];
    for(LocalAddressSearch *obj in fetchedObjsArr){
        CLLocationCoordinate2D coordinate =  CLLocationCoordinate2DMake([obj.latitude doubleValue], [obj.longitude doubleValue]);
       // NSLog(@"fetching  address =%@, lat = %f, long=%f",obj.address, coordinate.latitude,coordinate.longitude);
        LocalityInfo *localInfo = [[LocalityInfo alloc] initWithName:obj.name andAddress:obj.address andCity:obj.city andState:obj.state andCountry:obj.country andLatLng:coordinate];
        [actualResultsArr addObject:localInfo];
    }
    NSArray *dataArr = [[NSArray alloc] initWithArray:actualResultsArr];
    if(actualResultsArr.count && isRevrsed){
      dataArr =  [[actualResultsArr reverseObjectEnumerator] allObjects];
        //NSLog(@"dataArr is %@", dataArr);
    }
    if(dataArr.count == 0){
        isOldSearchAvailable = NO;
    }else{
        isOldSearchAvailable = YES;
    }
    return dataArr;
   /* NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:@"locationsSearchFromLocal"];
    NSArray *objectArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
    if(isRevrsed){
      objectArray =  [[objectArray reverseObjectEnumerator] allObjects];
    }

    if(objectArray.count == 0){
        isOldSearchAvailable = NO;
    }else{
        isOldSearchAvailable = YES;
    }
    return objectArray;*/
}
-(void)selectAppropriateLocationFromPreviousHistory:(NSIndexPath *)path{
    LocalityInfo *selectedLocalityInfo = [oldHistoryDataArr objectAtIndex:path.row];
    //NSLog(@"selectedLocality address is %@", selectedLocalityInfo.address);
    if(_isForSourceAddress){
        _sourceLocality = selectedLocalityInfo;
        _sourceTextField.text = _sourceLocality.address;
    }else{
        _destinationLocality = selectedLocalityInfo;
        _destinationTextField.text = _destinationLocality.address;

    }
    [self moveToSearchScreenAfterPickingLocations];
   
}
-(void)moveToSearchScreenAfterPickingLocations{
    if(_sourceLocality && _destinationLocality){
    [self.selectionDelegate selectedLocality:_sourceLocality withDestnationLocation:_destinationLocality];
    [self dismissViewControllerAnimated:NO completion:nil];
    }
    
    if(_sourceLocality){
        _sourceTextField.text = _sourceLocality.address;
    }
    if(_destinationLocality){
        _destinationTextField.text = _destinationLocality.address;
    }
   
    if(!_sourceLocality){
        textFieldtextLength = 0;
        [_sourceTextField becomeFirstResponder];
    }
    if(!_destinationLocality){
        textFieldtextLength = 0;
        [_destinationTextField becomeFirstResponder];
    }
    oldHistoryDataArr = [self getRecentHistoryForSearchAddresses:YES];
    filteredLocalities = nil;
    _showAutosuggestOnly = NO;
    [_tableView reloadData];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height + 44), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width + 44), 0.0);
    }
    
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        self.tableView.contentInset = contentInsets;// insert content inset value here
        self.tableView.scrollIndicatorInsets = contentInsets; // insert content inset value here
    }];
    
  //  [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        self.tableView.contentInset = UIEdgeInsetsZero;
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}


@end
