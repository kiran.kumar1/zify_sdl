//
//  AddBankAccountController.swift
//  zify
//
//  Created by Anurag on 10/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class AddBankAccountController: ScrollableContentViewController {
    
    @IBOutlet weak var accountTypeView: UIView!
    @IBOutlet var accountTypes: [UIButton]!
    @IBOutlet weak var accountHolderName: UITextField!
    @IBOutlet weak var accountNumber: UITextField!
    @IBOutlet weak var ifscCode: UITextField!
    @IBOutlet weak var bankName: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var lblAccountTypeText: UILabel!
    @IBOutlet weak var lblSavingsText: UIButton!
    @IBOutlet weak var lblCurrentText: UIButton!
    @IBOutlet weak var lblBankDetailsText: UILabel!
    
    let ACCEPTABLE_CHARACTERS_FOR_NAME = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz."
    let ACCEPTABLE_CHARACTERS_FOR_IFSCCODE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    
    var isGlobalLocale = false
    var selectedAccountTypeIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIScreen.main.sizeType == .iPhone5 {
            saveButton.titleLabel?.fontSize = 16
            lblAccountTypeText.fontSize = 12
            lblSavingsText.titleLabel?.fontSize = 13
            lblCurrentText.titleLabel?.fontSize = 13
            lblBankDetailsText.fontSize = 12
        } else if UIScreen.main.sizeType == .iPhone6 {
            saveButton.titleLabel?.fontSize = 17
            lblAccountTypeText.fontSize = 13
            lblSavingsText.titleLabel?.fontSize = 14
            lblCurrentText.titleLabel?.fontSize = 14
            lblBankDetailsText.fontSize = 13
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            saveButton.titleLabel?.fontSize = 18
            lblAccountTypeText.fontSize = 14
            lblSavingsText.titleLabel?.fontSize = 15
            lblCurrentText.titleLabel?.fontSize = 15
            lblBankDetailsText.fontSize = 14
        }
        addLeftView(accountHolderName)
        addLeftView(accountNumber)
        addLeftView(ifscCode)
        addLeftView(bankName)
        
        let currentUser = UserProfile.getCurrentUser()!
        print("global account is\(String(describing: currentUser.isGlobal! ))")
        if(currentUser.isGlobal == 1){
            print("Global account")
            self.accountTypeView.removeFromSuperview()
            self.accountNumber.removeFromSuperview()
        }else{
            print("Local Account")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftView(_ textField: UITextField?) {
        let customLeftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 15))
        textField?.leftView = customLeftView
        textField?.leftViewMode = .always
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initialiseView()
    }
    
    // MARK:- Actions
    
    @IBAction func selectAccountType(_ sender: UIButton) {
        for index in 0..<accountTypes.count {
            let button = accountTypes[index] //as? UIButton
            if button == sender {
                button.isSelected = true
                selectedAccountTypeIndex = index
            } else {
                button.isSelected = false
            }
        }
        print("selected account type is \(getAccountType(for: selectedAccountTypeIndex) ?? "")")
    }
    
    @IBAction func saveBankAccountDetails(_ sender: UIButton) {
        super.dismissKeyboard()
        if validateBankAccountInfo() {
            var accountTypeStr = ""
            var accountNumStr = ""
            if(!isGlobalLocale){
                accountTypeStr = self.getAccountType(for: self.selectedAccountTypeIndex)!;
                accountNumStr = self.accountNumber.text!;
            }
            messageHandler.showBlockingLoadView(handler: {
                let addBankAccountRequest = BankAccountRequest(requestType: ADDBANK, andAccountType:accountTypeStr , andAccountHolderName: self.accountHolderName.text, andAccountNumber:accountNumStr , andifscCode: self.ifscCode.text, andBankname: self.bankName.text)
                
                ServerInterface.sharedInstance().getResponse(addBankAccountRequest, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            //self.messageHandler.showSuccessMessage(response?.message)
                            self.perform(#selector(self.dismiss(_:)), with: self, afterDelay: 1.0)
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        }
                    })
                })
            })
        }
    }
    
    @IBAction func dismiss(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    func initialiseView() {
        selectDefaultAccountType()
        accountHolderName.becomeFirstResponder()
        isGlobalLocale = CurrentLocale.sharedInstance().isGlobalLocale()
        if isGlobalLocale {
            ifscCode.placeholder = NSLocalizedString(VC_ADDBANKACCOUNT_IBANNUMBER, comment: "")
        } else {
            ifscCode.placeholder = NSLocalizedString(VC_ADDBANKACCOUNT_IFSCCODE, comment: "")
        }
    }
    
    func selectDefaultAccountType() {
        for index in 0..<accountTypes.count {
            let button = accountTypes[index] //as? UIButton
            print("btn title is \(button.titleLabel?.text ?? "")")
            //if([button.titleLabel.text isEqualToString:NSLocalizedString(VC_ADDBANKACCOUNT_SAVINGS, nil)]){
            button.isSelected = false
            
            if index == 0                    {
                button.isSelected = true
                selectedAccountTypeIndex = index
            } else {
            }
        }
        print("selected account type  defult is \(getAccountType(for: selectedAccountTypeIndex) ?? "")")
    }
    
    func validateBankAccountInfo() -> Bool {
        if ("" == accountHolderName.text) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_ADDBANKACCOUNT_ACCOUNTNAMEVAL, comment: ""))
            return false
        }
        if(!isGlobalLocale){
            if ("" == accountNumber?.text) {
                messageHandler.showErrorMessage(NSLocalizedString(VC_ADDBANKACCOUNT_ACCOUNTNUMVAL, comment: ""))
                return false
            }
        }
        if ("" == bankName.text) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_ADDBANKACCOUNT_BANKNAMEVAL, comment: ""))
            return false
        }
        if ("" == ifscCode.text) {
            if isGlobalLocale {
                messageHandler.showErrorMessage(NSLocalizedString(VC_ADDBANKACCOUNT_IBANNUMBERVAL, comment: ""))
            } else {
                messageHandler.showErrorMessage(NSLocalizedString(VC_ADDBANKACCOUNT_IFSCCODEVAL, comment: ""))
            }
            return false
        }
        return true
    }
    
    func getAccountType(for index: Int) -> String? {
        if index == 1 {
            return "Current"
        }
        return "Savings"
    }
    
    // MARK :- TextField Delegate
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.count == 0 {
            return true
        }
        var set: CharacterSet?
        if textField == accountHolderName || textField == bankName {
            set = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_FOR_NAME)
        } else if textField == ifscCode {
            set = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_FOR_IFSCCODE)
        }
        
        if let aSet = set?.inverted {
            if Int((string as NSString).rangeOfCharacter(from: aSet).location) == NSNotFound {
                return true
            } else {
                return false
            }
        }
        return false
        
    }
    
    
    class func createAddBankAccountController() -> AddBankAccountController? {
        let storyBoard = UIStoryboard(name: "UserSettings", bundle: Bundle.main)
        let addBankController = storyBoard.instantiateViewController(withIdentifier: "addBankController") as? AddBankAccountController
        return addBankController
    }
}
