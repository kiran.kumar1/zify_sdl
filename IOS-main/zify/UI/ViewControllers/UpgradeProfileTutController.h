//
//  UpgradeProfileTutController.h
//  zify
//
//  Created by Anurag S Rathor on 02/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpgradeProfileTutController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) IBOutlet UIView *upgradeActionView;
+(UIViewController *)createUpgradeProfileTutNavController;

@property (weak, nonatomic) IBOutlet UILabel *lblPageTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRemindLater;
@property (weak, nonatomic) IBOutlet UIButton *btnGetStarted;

@end
