//
//  ForgotPasswordController.h
//  zify
//
//  Created by Anurag S Rathor on 12/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollableContentViewController.h"
#import "MessageHandler.h"
#import "TextFieldWithDone.h"

@interface ForgotPasswordController : ScrollableContentViewController<UITextFieldDelegate>
@property (nonatomic,weak) IBOutlet UIView *otpView;
@property (nonatomic,weak) IBOutlet UITextField *email;
@property (nonatomic,weak) IBOutlet TextFieldWithDone *otp;
@property (nonatomic,weak) IBOutlet UITextField *password;
@property (nonatomic,weak) IBOutlet UITextField *confirmPassword;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@end
