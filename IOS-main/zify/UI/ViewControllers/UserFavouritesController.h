//
//  UserFavouritesController.h
//  zify
//
//  Created by Anurag S Rathor on 23/02/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"

@interface UserFavouritesController : UIViewController
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *noUserFavouritesView;
+(UINavigationController *)createUserFavouritesNavController;
@end
