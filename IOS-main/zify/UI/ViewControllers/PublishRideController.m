//
//  PublishRideController.m
//  zify
//
//  Created by Anurag S Rathor on 23/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "PublishRideController.h"
#import <CoreLocation/CoreLocation.h>
#import "ServerInterface.h"
#import "UserProfile.h"
#import "UIImage+initWithColor.h"
#import "UniversalAlert.h"
#import "AppUtilites.h"
#import "TripsTabController.h"
#import "WSCoachMarksView.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "TutorialShownDB.h"
#import "AppDelegate.h"
#import "TripDrive.h"
#import "LocalNotificationManager.h"
#import "zify-Swift.h"

@interface PublishRideController ()

@end

@implementation PublishRideController{
    int routesCount;
    int currentRouteIndex;
    NSArray *offerRideSeatPickerValues;
    NSNumber *selectedSeats;
    BOOL isViewInitialised;
    CurrentLocale *currentLocale;
    LocalNotificationManager *localNotificationManager;
    
   // BOOL isAlreadyGetErrorMsg;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isViewInitialised = false;
    //isAlreadyGetErrorMsg = false;
    currentLocale = [CurrentLocale sharedInstance];
    [self drawShadowToAView:_btnIncreaseNum];
    [self drawShadowToAView:_btnDecreaseNum];
    [self drawShadowToAView:_seatsNumView];
    [self drawShadowToAView:_mapRoutesInfoView];
    localNotificationManager = [LocalNotificationManager sharedInstance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.driveInfoView.hidden = true;
    [self.publish setUserInteractionEnabled:YES];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised) [self initialiseView];
    else self.driveInfoView.hidden = false;
}

-(void)initialiseView{
    offerRideSeatPickerValues = [NSArray arrayWithObjects:@"1",@"2",@"3",@"4",nil];
    selectedSeats = [NSNumber numberWithInt:4];
    //_publishDriveRequest.numOfSeats = selectedSeats;
    UIImage *navigationBackgroundImage = [UIImage imageWithColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5]];
    [self.navigationController.navigationBar setBackgroundImage:navigationBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    DriveRoute *route = [_routes objectAtIndex:0];
    _sourceLocality.text = route.srcAdd;
    _destinationLocality.text = route.destAdd;
    _distance.text =[NSString stringWithFormat:@"%.2f %@",route.distance.floatValue,[currentLocale getDistanceUnit]];
    routesCount = (int)_routes.count;
    currentRouteIndex = 1;
    self.backward.enabled = NO;
    if(routesCount == 1){
        self.forward.enabled = NO;
    } else{
        self.forward.enabled = YES;
    }
    _index.text = [NSString stringWithFormat:@"%@ %d/%d",NSLocalizedString(VC_PUBLISHRIDE_ROUTELBL, nil), currentRouteIndex,routesCount];
    
    [_mapContainerView createMapView];
    MapView *mapView = _mapContainerView.mapView;
    mapView.sourceCoordinate = CLLocationCoordinate2DMake(route.srcLat.doubleValue,route.srcLong.doubleValue);
    mapView.destCoordinate = CLLocationCoordinate2DMake(route.destLat.doubleValue,route.destLong.doubleValue);
    mapView.overviewPolylinePoints = route.overviewPolylinePoints;
    mapView.mapPadding = UIEdgeInsetsMake(70, 0, CGRectGetHeight(_driveInfoView.frame), 0);
    _driveInfoView.hidden = false;
    [mapView drawMap];
    isViewInitialised = true;
    if(_isRouteSelectOnly){
        [_publish setTitle:NSLocalizedString(VC_PUBLISHRIDE_SELECTROUTE, nil) forState:UIControlStateNormal];
        _seatsNumView.hidden = true;
        _btnIncreaseNum.hidden = true;
        _btnDecreaseNum.hidden = true;
    } else{
        [self showCoachMarks];
    }
}

-(IBAction) dismiss:(id)sender{
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction) moveForward:(id)sender{
    currentRouteIndex ++;
    self.backward.enabled = YES;
    if(currentRouteIndex == routesCount){
        self.forward.enabled = NO;
    }
   [self redrawMapPath];
}

-(IBAction) moveBackward:(id)sender{
    currentRouteIndex --;
    self.forward.enabled = YES;
    if(currentRouteIndex == 1){
        self.backward.enabled = NO;
    }
    [self redrawMapPath];
}

-(void) redrawMapPath{
     _index.text = [NSString stringWithFormat:@"%@ %d/%d",NSLocalizedString(VC_PUBLISHRIDE_ROUTELBL, nil),currentRouteIndex,routesCount];
     DriveRoute *route = [_routes objectAtIndex:currentRouteIndex-1];
     _mapContainerView.mapView.overviewPolylinePoints = route.overviewPolylinePoints;
    _distance.text =[NSString stringWithFormat:@"%.2f %@",route.distance.floatValue,[currentLocale getDistanceUnit]];
    [_mapContainerView.mapView drawPathForIndex:currentRouteIndex-1];
}



-(IBAction) publishRide:(id)sender{
    UIButton *buton = (UIButton*)sender;
   // [buton setUserInteractionEnabled:NO];
    
    if(_isRouteSelectOnly){
        [buton setUserInteractionEnabled:YES];
        DriveRoute *route = [_routes objectAtIndex:currentRouteIndex-1];
        [self.navigationController dismissViewControllerAnimated:NO completion:^{
            [self.publishRideDelegate selectedDriveRoute:route];
        }];
    } else{
        UserProfile *userProfile = [UserProfile getCurrentUser];
        if(![userProfile.userDocuments.isVehicleImgUploaded intValue]){
            [buton setUserInteractionEnabled:YES];
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_PUBLISHRIDE_VEHICLEDETAILSERRTITLE, nil) WithMessage:NSLocalizedString(VC_PUBLISHRIDE_VEHICLEDETAILSERRMESSAGE, nil)];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                UINavigationController *vehicleTextNavController = [VehicleTextController createVehcileTextNavControllerWithIsNavPresent:YES];
                [self presentViewController:vehicleTextNavController animated:YES completion:nil];
            }];
            [alert showInViewController:self];
        } else{
            //_userSelectedRoute = [_routes objectAtIndex:currentRouteIndex-1];
            //[self callAPiForTheGeoDataObjects];
            
            DriveRoute *route = [_routes objectAtIndex:currentRouteIndex-1];
            [_publishDriveRequest setSeats:selectedSeats andRouteId:route.routeId];
            [self.messageHandler showBlockingLoadViewWithHandler:^{
                [[ServerInterface sharedInstance] getResponse:_publishDriveRequest withHandler:^(ServerResponse *response, NSError *error){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        if(response){
                            NSLog(@"obj %@",response.responseObject);
                            [self.messageHandler showSuccessMessage:response.message];
                            NSArray *drivesArr = [response.responseObject objectForKey:@"drives"];
                            if(drivesArr.count > 0){
                                NSDictionary *driveDictInfo = [drivesArr objectAtIndex:0];
                                TripDrive *driveInfo = [[TripDrive alloc] initWithDriveDict:driveDictInfo];
                                [localNotificationManager handleTripDrivesNotificationAfterCreatingANewDrive:driveInfo];
                            }
                            [self performSelector:@selector(showActiveTrip) withObject:self afterDelay:1.0];
                        } else{
                            [buton setUserInteractionEnabled:YES];
                            if(error.code == 2){
                                UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_ERROR, nil) WithMessage:error.userInfo[@"message"]];
                                [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                                    [self showUpcomingTrips];
                                }];
                                [alert showInViewController:self];
                            } else{
                                [buton setUserInteractionEnabled:YES];
                                [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                            }
                        }
                    }];
                }];
            }];
        
        }
    }
}

/*
-(void)callAPiForTheGeoDataObjects{
    
    if(self.sourceAddGeoInfo == nil){
      [self callApiForGeoDataBasedOnLocation:_sourceAddLocInfo isForSource:true];
    }
    if(self.destAddGeoInfo == nil){
      [self callApiForGeoDataBasedOnLocation:_destAddLocInfo isForSource:false];
    }
    
    if(self.polylineRouteObj == nil){
        [self callAPIForRoutePolyline];
    }
    
    if(self.sourceAddGeoInfo && self.destAddGeoInfo && self.polylineRouteObj){
        [self callAPiForThePublishNewDriverInV3];
    }
    
}
-(void)callApiForGeoDataBasedOnLocation:(LocalityInfo *)adddress isForSource:(BOOL)forSource {
    GetNearestPointsToALocationRequest *req = [[GetNearestPointsToALocationRequest alloc] initWithLocation:adddress];
    
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:req withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    NSDictionary *resultDict = response.responseObject;
                    NSArray *userGeoDataArray = [resultDict objectForKey:@"userGeoData"];
                    if(userGeoDataArray.count > 0){
                        if(forSource){
                            self.sourceAddGeoInfo = userGeoDataArray.firstObject;
                        }else{
                            self.destAddGeoInfo = userGeoDataArray.firstObject;
                        }
                        [self callAPiForThePublishNewDriverInV3];
                        
                    }else{
                        NSDictionary *userGeoDataDict = [[NSDictionary alloc] initWithObjectsAndKeys:adddress.address,@"address", adddress.city,@"city",[NSNumber numberWithBool:false],@"exists", [NSNumber numberWithInt:0],@"geoDataId", adddress.latLng.latitude,@"lat",adddress.latLng.longitude,@"lng",nil];
                        if(forSource){
                            self.sourceAddGeoInfo = userGeoDataDict;
                        }else{
                            self.destAddGeoInfo = userGeoDataDict;
                        }
                        [self callAPiForThePublishNewDriverInV3];
                    }
                }else{
                    // failed to get geoDataObj
                    if (error) {
                        
                        if (forSource == TRUE ) {
                            if (isAlreadyGetErrorMsg == TRUE) {
                                
                            } else {
                                isAlreadyGetErrorMsg = TRUE;
                                [self.messageHandler showErrorMessage:@"Please try again."];
                            }
                        } else if (forSource == FALSE) {
                            if (isAlreadyGetErrorMsg == TRUE) {
                                
                            } else {
                                isAlreadyGetErrorMsg = TRUE;
                                [self.messageHandler showErrorMessage:@"Please try again."];
                            }
                        }
                    }
                    //[self callAPiForThePublishNewDriverInV3];
                }
            }];
        }];
    }];

}

-(void)callAPIForRoutePolyline {
    GetGEOPolylineForRouteRequest *req = [[GetGEOPolylineForRouteRequest alloc] initWithRoute:_userSelectedRoute];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:req withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    NSDictionary *resultDict = response.responseObject;
                    _polylineRouteObj = [resultDict objectForKey:@"userRoute"];
                }else{
                    _polylineRouteObj = nil;
                    if (isAlreadyGetErrorMsg == TRUE) {
                        
                    } else {
                        isAlreadyGetErrorMsg = TRUE;
                        [self.messageHandler showErrorMessage:@"Please try again."];
                    }
                }
                [self callAPiForThePublishNewDriverInV3];
            }];
        }];
    }];
}

-(void)callAPiForThePublishNewDriverInV3{
    
    if( self.sourceAddGeoInfo == nil || self.destAddGeoInfo == nil || self.polylineRouteObj == nil ){
        NSLog(@"error occuered.. while fetching  geo datta");
    }else{
        // call Api for publish drive
        
        PublishDriveRequest *request = [[PublishDriveRequest alloc] initWithSTartPoint:self.sourceAddGeoInfo andEndPoint:self.destAddGeoInfo andPolylineRoute:self.polylineRouteObj andTravelMode:@"CAR" andNumberOfSeats:_publishDriveRequest.numOfSeats andDepartureTime:_publishDriveRequest.departureTime];
        //  _publishDriveRequest.departureTime
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance] getResponse:request withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        NSLog(@"obj %@",response.responseObject);
                        [self.messageHandler showSuccessMessage:response.message];
                        NSArray *drivesArr = [response.responseObject objectForKey:@"drives"];
                        if(drivesArr.count > 0){
                            NSDictionary *driveDictInfo = [drivesArr objectAtIndex:0];
                            TripDrive *driveInfo = [[TripDrive alloc] initWithDriveDict:driveDictInfo];
                            [localNotificationManager handleTripDrivesNotificationAfterCreatingANewDrive:driveInfo];
                        }
                        [self performSelector:@selector(showActiveTrip) withObject:self afterDelay:1.0];
                    } else{
                        //[buton setUserInteractionEnabled:YES];
                        if(error.code == 2){
                            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_ERROR, nil) WithMessage:error.userInfo[@"message"]];
                            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                                [self showUpcomingTrips];
                            }];
                            [alert showInViewController:self];
                        } else{
                            //  [buton setUserInteractionEnabled:YES];
                            [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                        }
                    }
                }];
            }];
        }];
    }
}
 */

-(IBAction) selectSeats:(id)sender{
    return;
    self.seatPickerView.delegate = self;
    self.seatPickerView.pickervalues = offerRideSeatPickerValues;
    self.seatPickerView.currentValue = [selectedSeats stringValue];
    [self.seatPickerView showInView:self.navigationController.view];
}

-(void) selectedPickerValue:(NSString *)value{
    selectedSeats = [NSNumber numberWithInt:[value intValue]];
    self.seatsNumLabel.text = selectedSeats.stringValue;
}

-(void)showActiveTrip{
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        [self.publishRideDelegate showActiveTripOnPublishRide];
    }];
}

-(void)showUpcomingTrips{
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        UINavigationController *tripsTabNavController = [TripsTabController createTripsTabNavController];
        TripsTabController *tripsTabContoller = (TripsTabController *)tripsTabNavController.topViewController;
        [tripsTabContoller selectTripsDrives];
        [[AppUtilites applicationVisibleViewController] presentViewController:tripsTabNavController animated:YES completion:nil];
    }];
}

-(void)showCoachMarks{
   // BOOL isTutorialShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"publishRideTutorialShown"];
    TutorialShownDB *obj = [[AppDelegate getAppDelegateInstance] getTutorialsDBObject];
    NSLog(@"obj is %@", obj.isInGuestHomeShown);
    if([obj.isInPublishRideShown isEqualToString:@"0"]){
        
   //l if(!isTutorialShown){
        NSArray *coachMarks = @[
                                @{@"rect": [NSValue valueWithCGRect:(CGRect){[_driveInfoView convertPoint:_seatsNumView.frame.origin toView:self.view],{_seatsNumView.frame.size.width,_seatsNumView.frame.size.height}}],
                                  @"caption": NSLocalizedString(VC_PUBLISHRIDE_SEATSTUT, nil),
                                  @"shape": @"square"
                                  },
                                @{@"rect": [NSValue valueWithCGRect:(CGRect){[_mapRoutesInfoView convertPoint:_mapRoutesNavigationView.frame.origin toView:self.view],{_mapRoutesNavigationView.frame.size.width,_mapRoutesNavigationView.frame.size.height}}],
                                  @"caption": NSLocalizedString(VC_PUBLISHRIDE_ROUTESTUT, nil)
                                  },
                                @{@"rect": [NSValue valueWithCGRect:(CGRect){[_driveInfoView convertPoint:_publish.frame.origin toView:self.view],{_publish.frame.size.width,_publish.frame.size.height}}],
                                  @"caption": NSLocalizedString(VC_PUBLISHRIDE_PUBLISHTUT, nil),
                                  @"shape": @"square"
                                  }
                                ];
        WSCoachMarksView *coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
        coachMarksView.animationDuration = 0.3f;
        coachMarksView.enableContinueLabel = YES;
        coachMarksView.enableSkipButton = NO;
        coachMarksView.maskColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
        [self.navigationController.view addSubview:coachMarksView];
        [coachMarksView start];
        obj.isInPublishRideShown = @"1";
        [[AppDelegate getAppDelegateInstance] updateTutorialDBObject:obj];
      //  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"publishRideTutorialShown"];
    }
}
-(IBAction) seatsIncrement:(id)sender{
    int seatsCount = [_seatsNumLabel.text intValue];
    if(seatsCount < 4){
        seatsCount ++;
    }
    selectedSeats = [NSNumber numberWithInt:seatsCount];
    _seatsNumLabel.text = [NSString stringWithFormat:@"%d", seatsCount];
}
-(IBAction) seatsDecrement:(id)sender{
    int seatsCount = [_seatsNumLabel.text intValue];
    if(seatsCount > 1){
        seatsCount --;
    }
    selectedSeats = [NSNumber numberWithInt:seatsCount];
    _seatsNumLabel.text = [NSString stringWithFormat:@"%d", seatsCount];
}
+(UINavigationController *)createPublishRideNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *userProfileNavController = [storyBoard instantiateViewControllerWithIdentifier:@"publishRideNavController"];
    return userProfileNavController;
}
-(void)drawShadowToAView:(UIView *)shadeView{
    CALayer *layer = shadeView.layer;
    layer.masksToBounds = NO;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    layer.shadowRadius = 2.0f;
    layer.shadowOpacity = 1.0f;
    if(shadeView != _mapRoutesInfoView){
        layer.shadowColor = [[UIColor colorWithRed:186.0/255.0 green:184.0/255.0 blue:184.0/255.0 alpha:0.8] CGColor];
    layer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:layer.bounds cornerRadius:layer.cornerRadius] CGPath];
    }
}

@end
