//
//  WalletStatementController.h
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageHandler.h"

@interface WalletStatementController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSString *nodataMsgStr;
}
@property (nonatomic,weak) IBOutlet UIImageView *imgNoStmtRecharge;
@property (nonatomic,weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *noTransactionView;
@property (nonatomic,weak) IBOutlet UILabel *noDataLbl;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *noTransactionViewTopConstraint  ;
+(UINavigationController *)createWalletStatementNavController;
@end
