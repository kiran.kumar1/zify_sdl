//
//  LocationDisabledViewController.m
//  zify
//
//  Created by Anurag Rathor on 19/02/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "LocationDisabledViewController.h"
#import "CurrentLocationTracker.h"
#import "AppDelegate.h"
#import "LocalityAutoSuggestionController.h"
#import "LocalisationConstants.h"
@interface LocationDisabledViewController ()

@end

@implementation LocationDisabledViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
  
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnTypeAddresstapped:(UIButton *)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:@"isFromDeniedScreen"];
    [prefs synchronize];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnLocationServicesEnabledTapped:(UIButton *)sender {
    
    [[UIApplication sharedApplication] openURL:
       [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}
@end
