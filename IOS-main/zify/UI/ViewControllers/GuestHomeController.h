//
//  GuestHomeController.h
//  zify
//
//  Created by Anurag S Rathor on 19/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "GuestMenuController.h"
#import "LocalityAutoSuggestionController.h"
#import "MessageHandler.h"
#import "DateTimePickerView.h"
#import "CurrentLocationTracker.h"
#import "MapContainerView.h"
#import "WSCoachMarksView.h"

@interface GuestHomeController :UIViewController<GuestMenuDelegate,LocalitySelectionDelegate,DateTimePickerDelegate,CurrentLocationTrackerDelegate,MapViewDelegate,WSCoachMarksViewDelegate> {
    NSDate *selectedDateFromPicker;
    
}
@property (nonatomic,weak) IBOutlet UILabel *sourceLocality;
@property (nonatomic,weak) IBOutlet UILabel *destinationLocality;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet DateTimePickerView *dateTimePickerView;
@property (nonatomic,weak) IBOutlet UIView *sourceInfoView;
@property (nonatomic,weak) IBOutlet UIView *destinationInfoView;
@property (nonatomic,weak) IBOutlet UIView *destinationInfoDetailsView;
@property (nonatomic,weak) IBOutlet UIView *rideDateAndLocationView;
@property (nonatomic,weak) IBOutlet UIView *rideDateTimeView;
@property (nonatomic,weak) IBOutlet UILabel *rideDateText;
@property (nonatomic,weak) IBOutlet UILabel *rideTimeText;
@property (nonatomic,weak) IBOutlet UIButton *rideDateTimeButton;
@property (nonatomic,weak) IBOutlet UIButton *searchButton;
@property (nonatomic,weak) IBOutlet UIButton *offerButton;
@property (nonatomic,weak) IBOutlet UIButton *heremapsButton;
@property (nonatomic,weak) IBOutlet MapContainerView *mapContainerView;
-(void)displayproperViewForLocationEnabledOrDisabled;


@property (nonatomic,weak) IBOutlet UIView *viewChooseMode;
@property (nonatomic,weak) IBOutlet UIView *viewDestnTimeDate;
@property (nonatomic,weak) IBOutlet UIView *viewDestnTimeDateTravelPref;
@property (nonatomic,weak) IBOutlet UIView *viewTravelPref;
@property (nonatomic,weak) IBOutlet UIView *viewFindOrShareRide;
@property (weak, nonatomic) IBOutlet UIButton *btnSwitchMode;
@property (weak, nonatomic) IBOutlet UIButton *btnFindOrShareRide;
@property (weak, nonatomic) IBOutlet UITextField *txtDestination;
@property (weak, nonatomic) IBOutlet UIView *viewTimeDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIButton *btnFirstTimeShowDateTimePicker;
@property (weak, nonatomic) IBOutlet UIButton *btnTravelPreferences;
@property (strong, nonatomic) IBOutlet UIView *viewHomeWork;
@property (weak, nonatomic) IBOutlet UILabel *lblHome;
@property (weak, nonatomic) IBOutlet UILabel *lblWork;


@end
