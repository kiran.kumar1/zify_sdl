//
//  GuestUserRideController.h
//  zify
//
//  Created by Anurag S Rathor on 20/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"
#import "UserDataManager.h"

@interface GuestUserRideController : UIViewController {
    UIVisualEffectView *visualEffectView;
}
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *fetchingView;
@property (nonatomic,weak) IBOutlet LoadingSpinner *loader;
@property (nonatomic,weak) IBOutlet UIView *noDrivesView;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,strong) UserSearchData *userSearchData;
+(UINavigationController *)createGuestRideNavController;

//phase - 1
@property (strong, nonatomic) IBOutlet UIView *viewJoinNowSignIn;

@end
