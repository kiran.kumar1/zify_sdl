//
//  GenericWebController.m
//  zify
//
//  Created by Anurag S Rathor on 03/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "GenericWebController.h"

@implementation GenericWebController

-(void) viewDidLoad{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //[self.navigationController.navigationItem setTitle:_title];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_webViewURL]]];
}


-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark
#pragma mark WebView delegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}

+(UINavigationController *)createGenericWebNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Commons" bundle:[NSBundle mainBundle]];
    UINavigationController *genericWebNavController = [storyBoard instantiateViewControllerWithIdentifier:@"genericWebNavController"];
    return genericWebNavController;
}
@end
