//
//  StripePaymentController.m
//  zify
//
//  Created by Anurag S Rathor on 16/11/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "StripePaymentController.h"
#import "MessageHandler.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

@interface StripePaymentController()< STPPaymentCardTextFieldDelegate>


@end

@implementation StripePaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initilaiseView];
    
    if (IS_IPHONE_5) {
        _rechargeAmountLabel.fontSize = 34;
        _lblRechargeAmountText.fontSize = 13;
        _lblPleaseEnterCardDetails.fontSize = 13;
        _btnPayNow.titleLabel.fontSize = 16;
    } else if (IS_IPHONE_6) {
        _rechargeAmountLabel.fontSize = 35;
        _lblRechargeAmountText.fontSize = 13;
        _lblPleaseEnterCardDetails.fontSize = 13;
        _btnPayNow.titleLabel.fontSize = 16;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _rechargeAmountLabel.fontSize = 36;
        _lblRechargeAmountText.fontSize = 14;
        _lblPleaseEnterCardDetails.fontSize = 14;
        _btnPayNow.titleLabel.fontSize = 18;
    }
    
}

-(void) initilaiseView{
     self.rechargeAmountLabel.text = [NSString stringWithFormat:@"%@%@",[[CurrentLocale sharedInstance] getCurrencySymbol],self.amount];
    _paymentTextField.delegate = self;
}

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)payNow:(UIButton *)sender{
    if (![self.paymentTextField isValid]) {
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_STRIPEPAYMENT_CARDDETAILSVAL, nil)];
        return;
    }
    if (![Stripe defaultPublishableKey]) {
        NSError *error = [NSError errorWithDomain:StripeDomain
                            code:STPInvalidRequestError
                            userInfo:@{NSLocalizedDescriptionKey: NSLocalizedString(VC_STRIPEPAYMENT_STRIPEKEYVAL, nil)}];
        [self dismissViewControllerAnimated:NO completion:^{
            [self.paymentDelegate stripePaymentFailedWithError:error];
        }];
        return;
    }
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[STPAPIClient sharedClient] createTokenWithCard:self.paymentTextField.cardParams
                                              completion:^(STPToken *token, NSError *error) {
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if (error) {
                        [self dismissViewControllerAnimated:NO completion:^{
                            [self.paymentDelegate stripePaymentFailedWithError:error];
                        }];
                    } else{
                        [self dismissViewControllerAnimated:NO completion:^{
                            [self.paymentDelegate createStripePaymentWithToken:token];
                        }];
                    }
            }];
        }];
    }];
}

+(UINavigationController *)createStripePaymentNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Wallet" bundle:[NSBundle mainBundle]];
    UINavigationController *accountNavController = [storyBoard instantiateViewControllerWithIdentifier:@"stripePaymentNavController"];
    return accountNavController;
}
@end
