//
//  MainContentController.h
//  zify
//
//  Created by Anurag S Rathor on 27/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainContentController : UIViewController
@property (weak, nonatomic) UIViewController *mainContentNavController;
+(MainContentController *)createMainContentController;
+(MainContentController *)createMainContentGuestController;
@end
