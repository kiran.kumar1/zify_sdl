//
//  AboutController.m
//  zify
//
//  Created by Anurag S Rathor on 26/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "AboutController.h"
#import "AboutSupportController.h"
#import "GenericWebController.h"
#import "AppUtilites.h"
#import "LocalisationConstants.h"

#define LIKE_FACEBOOK_ROW 0
#define TNC_ROW 1
//#define SUPPORT_ROW 2
//#define RATEUS_ROW 3
#define FAQ_ROW 2

@implementation AboutController
-(void) viewDidLoad{
    [self initialiseView];
    
    
}

#pragma mark - TableView delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.textColor = [UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0];
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:16.0];
    if(indexPath.row == LIKE_FACEBOOK_ROW){
        cell.textLabel.text = NSLocalizedString(VC_ABOUT_LIKEFB, nil);
        cell.imageView.image = [UIImage imageNamed:@"facebook-logo.png"];
    } else if(indexPath.row == TNC_ROW){
        cell.textLabel.text = NSLocalizedString(CMON_GENERIC_TNC, nil);
        cell.imageView.image = [UIImage imageNamed:@"termsAndCond.png"];
    }/* else if(indexPath.row == SUPPORT_ROW) {
        cell.textLabel.text = NSLocalizedString(VC_ABOUT_SUPPORT, nil);
        cell.imageView.image = [UIImage imageNamed:@"Support.png"];
    } else if(indexPath.row == RATEUS_ROW){
        cell.textLabel.text = NSLocalizedString(VC_ABOUT_RATEUS, nil);
        cell.imageView.image = [UIImage imageNamed:@"icn_about_rateus.png"];
    }*/ else{
        cell.textLabel.text = NSLocalizedString(CMON_GENERIC_FAQ, nil);
        cell.imageView.image = [UIImage imageNamed:@"FAQ.png"];
    }
    
    cell.textLabel.font = [UIFont fontWithName:NormalFont size:16];
    if (IS_IPHONE_6) {
        cell.textLabel.font = [UIFont fontWithName:NormalFont size:17];
        
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        cell.textLabel.font = [UIFont fontWithName:NormalFont size:18];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == LIKE_FACEBOOK_ROW){
        NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/1430144977216466"];
        if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
            [[UIApplication sharedApplication] openURL:facebookURL];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/zify.co"]];
        }
    }else if(indexPath.row == TNC_ROW){
        UINavigationController *genericWebNavController = [GenericWebController createGenericWebNavController];
        GenericWebController *webController = (GenericWebController *)genericWebNavController. topViewController;
        webController.webViewURL = NSLocalizedString(CMON_GENERIC_TERMSURL,nil);
        webController.title = NSLocalizedString(CMON_GENERIC_TNC, nil);
        [self presentViewController:genericWebNavController animated:YES completion:nil];
    }/* else if(indexPath.row == SUPPORT_ROW){
        [self performSegueWithIdentifier:@"showSupport" sender:self];
    } else if(indexPath.row == RATEUS_ROW){
        [AppUtilites openAppStore];
    } */else{
        UINavigationController *genericWebNavController = [GenericWebController createGenericWebNavController];
        GenericWebController *webController = (GenericWebController *)genericWebNavController. topViewController;
        webController.webViewURL = @"https://support.zify.co/portal/kb";
        webController.title = NSLocalizedString(CMON_GENERIC_FAQ, nil);
        [self presentViewController:genericWebNavController animated:YES completion:nil];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

-(void) initialiseView{
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 0.01f)];
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    _versionNumber.text = [NSString stringWithFormat:NSLocalizedString(VC_ABOUT_VERSION, @"Version : {App Version}"),majorVersion];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showSupport"]){
        AboutSupportController *supportController = segue.destinationViewController;
        supportController.messageDelegate = self;
    }
}

#pragma mark - MFMailCompose delegates

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

+(UINavigationController *)createAboutNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"UserSettings" bundle:[NSBundle mainBundle]];
    UINavigationController *aboutNavController = [storyBoard instantiateViewControllerWithIdentifier:@"aboutNavController"];
    return aboutNavController;
}
@end
