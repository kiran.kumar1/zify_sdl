//
//  RideRequestsController.h
//  zify
//
//  Created by Anurag S Rathor on 26/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"
#import "UserNotificationDetail.h"

@interface RideRequestsController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *noRideRequestsView;
@property(nonatomic,assign) BOOL isViewInitialised;
+(UINavigationController *)createRideRequestsNavController;
@property(nonatomic,strong) UserNotificationDetail *userNotification;
@property(nonatomic,strong)NSIndexPath* selectedIndexPath;
-(void)callServiceAndLoadData;
@end
