//
//  TripsHistoryController.m
//  zify
//
//  Created by Anurag S Rathor on 24/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripsHistoryController.h"
#import "TripHistoryCell.h"
#import "ServerInterface.h"
#import "GenericRequest.h"
#import "TripHistory.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

@interface TripsHistoryController ()

@end

@implementation TripsHistoryController{
    NSArray *tripHistoryArray;
    NSDateFormatter *dateFormatter;
    NSDateFormatter *timeFormatter;
    NSDateFormatter *dateTimeFormatter;
    BOOL isViewInitialised;
    CurrentLocale *currentLocale;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter setLocale:locale];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    [timeFormatter setLocale:locale];
    isViewInitialised = false;
    currentLocale = [CurrentLocale sharedInstance];
    _tableView.estimatedRowHeight = 91.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised){
        self.tableView.hidden = false;
        self.noHistoryView.hidden = true;
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance]getResponse:[[GenericRequest alloc] initWithRequestType:TRIPHISTORYREQUEST] withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        tripHistoryArray = (NSArray *)response.responseObject;
                        if([tripHistoryArray count] == 0){
                            self.tableView.hidden = true;
                            self.noHistoryView.hidden = false;
                        } else{
                            self.tableView.hidden = false;
                            self.noHistoryView.hidden = true;
                            [self.tableView reloadData];
                        }
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
        isViewInitialised = true;
    }
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tripHistoryArray.count == 0) return 0;
    return [tripHistoryArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TripHistory *tripHistory = (TripHistory *)[tripHistoryArray objectAtIndex:indexPath.section];
    TripHistoryCell *historyCell =[tableView dequeueReusableCellWithIdentifier:@"tripHistoryCell"];
    if([@"Ride" isEqualToString:tripHistory.travelType]){
        [self populateCell:historyCell WithTripRide:tripHistory.tripRide AndIndexPath:indexPath];
    } else {
        [self populateCell:historyCell WithTripDrive:tripHistory.tripDrive AndIndexPath:indexPath];
    }
    return historyCell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

- (void) populateCell:(TripHistoryCell *)historyCell WithTripRide:(TripRide *)tripRide AndIndexPath:(NSIndexPath *)indexPath{
    historyCell.referenceNumber.text = [NSString stringWithFormat:NSLocalizedString(VC_TRIPSHISTORY_RIDEID, @"Ride Id: {User Ride Id}"), tripRide.rideId.stringValue];
    historyCell.source.text = tripRide.srcAddress;
    historyCell.destination.text = tripRide.destAddress;
    NSDate *tripDepartureTime = [dateTimeFormatter dateFromString:tripRide.departureTime];
    historyCell.tripDate.text = [NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:tripDepartureTime],[timeFormatter stringFromDate:tripDepartureTime]];
    historyCell.totalDistance.text = [NSString stringWithFormat:@"%@ %@",tripRide.distance, [currentLocale getDistanceUnit]];
    historyCell.totalAmount.text = [NSString stringWithFormat:@"%@%@",[currentLocale getCurrencySymbol],tripRide.zifyPoints];
    historyCell.driveDetail = tripRide.driveDetail;
    historyCell.driverDetail = tripRide.driverDetail;
    historyCell.tripType = RIDE;

    if (IS_IPHONE_5) {
        historyCell.referenceNumber.fontSize = 13;
        historyCell.source.fontSize = 15;
        historyCell.destination.fontSize = 15;
        historyCell.tripDate.fontSize = 11;
        historyCell.totalDistance.fontSize = 15;
        historyCell.totalAmount.fontSize = 14;
        historyCell.lblSourceTitle.fontSize = 13;
        historyCell.lblDestinationTitle.fontSize = 13;
    } else if (IS_IPHONE_6) {
        historyCell.referenceNumber.fontSize = 13;
        historyCell.source.fontSize = 15;
        historyCell.destination.fontSize = 15;
        historyCell.tripDate.fontSize = 11;
        historyCell.totalDistance.fontSize = 15;
        historyCell.totalAmount.fontSize = 14;
        historyCell.lblSourceTitle.fontSize = 13;
        historyCell.lblDestinationTitle.fontSize = 13;
        
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        historyCell.referenceNumber.fontSize = 14;
        historyCell.source.fontSize = 16;
        historyCell.destination.fontSize = 16;
        historyCell.tripDate.fontSize = 12;
        historyCell.totalDistance.fontSize = 16;
        historyCell.totalAmount.fontSize = 15;
        historyCell.lblSourceTitle.fontSize = 14;
        historyCell.lblDestinationTitle.fontSize = 14;
    }
    

    
}

- (void) populateCell:(TripHistoryCell *)historyCell WithTripDrive:(TripDrive *)tripDrive AndIndexPath:(NSIndexPath *)indexPath{
    historyCell.referenceNumber.text = [NSString stringWithFormat:NSLocalizedString(VC_TRIPSHISTORY_DRIVEID, @"Drive Id: {User Drive Id}"),tripDrive.driveId.stringValue];
    historyCell.source.text = tripDrive.srcAddress;
    historyCell.destination.text = tripDrive.destAddress;
    NSDate *tripDepartureTime = [dateTimeFormatter dateFromString:tripDrive.departureTime];
    historyCell.tripDate.text = [NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:tripDepartureTime],[timeFormatter stringFromDate:tripDepartureTime]];
    historyCell.totalDistance.text = [NSString stringWithFormat:@"%@ %@",tripDrive.distance,tripDrive.distanceUnit];
     historyCell.totalAmount.text = [NSString stringWithFormat:@"%@%@",[currentLocale getCurrencySymbol],tripDrive.zifyPoints];
    historyCell.driveRiders = tripDrive.driveRiders;
    historyCell.tripType = DRIVE;
}
@end
