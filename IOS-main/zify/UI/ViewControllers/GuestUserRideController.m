//
//  GuestUserRideController.m
//  zify
//
//  Created by Anurag S Rathor on 20/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "GuestUserRideController.h"
#import "GuestCreateRideCell.h"
#import "GuestUserSearchRide.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServerInterface.h"
#import "CreateRideRequest.h"
#import "UIImage+ProportionalFill.h"
#import "LocalisationConstants.h"
#import "GuestUserRideRequest.h"
#import "CurrentLocale.h"
#import "ImageHelper.h"
#import "AppDelegate.h"
#import "UserOfferRideRequest.h"
#import "GuestSearchResultsCell.h"
#import "zify-Swift.h"

@implementation GuestUserRideController{
    UIImage *profileImagePlaceHolder;
    UIImage *carImagePlaceHolder;
    NSArray *searchRides;
    CurrentLocale *currentLocale;
    UIImage *sourceIconImage;
    UIImage *destinationIconImage;
    UIImage *lockImage;
    BOOL isViewInitialised;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialiseView];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _tableView.hidden = YES;
    _noDrivesView.hidden = YES;
    _fetchingView.hidden =  NO;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_loader startAnimating];
    if(!isViewInitialised){
        [[ServerInterface sharedInstance] getResponse:[[GuestUserRideRequest alloc] initWithSourceLocality:_userSearchData.sourceLocality andDestinationLocality:_userSearchData.destinationLocality andRideDate:_userSearchData.rideStartDate andSeats:@1 andMerchantId:_userSearchData.merchantId] withHandler:^(ServerResponse *response, NSError *error){
            [_loader stopAnimating];
            _fetchingView.hidden = YES;
            if(response){
                searchRides = (NSArray *)response.responseObject;
                if(searchRides.count > 0){
                    [_tableView reloadData];
                    _tableView.hidden = NO;
                    [self showBlurView];
                } else{
                    _noDrivesView.hidden = NO;
                }
            }
            else {
                _noDrivesView.hidden = NO;
            }
        }];
        isViewInitialised = YES;
    }
}

-(void) initialiseView {
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    carImagePlaceHolder = [UIImage imageNamed:@"placeholder_car_image.png"];
    _tableView.estimatedRowHeight = 318;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    currentLocale = [CurrentLocale sharedInstance];
    sourceIconImage = [UIImage imageNamed:@"icn_source_info.png"];
    destinationIconImage = [UIImage imageNamed:@"icn_destination_info.png"];
    lockImage = [UIImage imageNamed:@"icn_user_lock.png"];
    isViewInitialised = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)showBlurView {
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.alpha = 0.7;
    
    visualEffectView.frame = self.tableView.frame;
    [self.view addSubview:visualEffectView];
    //[self.tableView addSubview:visualEffectView];
    
    self.viewJoinNowSignIn.frame = CGRectMake(0, self.view.frame.size.height, AppDelegate.SCREEN_WIDTH, self.viewJoinNowSignIn.frame.size.height);
    
    [UIView animateWithDuration:0.5
                          delay:0.01
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         self.viewJoinNowSignIn.frame = CGRectMake(0, AppDelegate.SCREEN_HEIGHT - self.viewJoinNowSignIn.frame.size.height, AppDelegate.SCREEN_WIDTH, self.viewJoinNowSignIn.frame.size.height);
                     }
                     completion:^(BOOL finished){
                     }];
    
    _viewJoinNowSignIn.alpha = 1;
    [self.view addSubview:self.viewJoinNowSignIn];
}

#pragma mark - TableView Delegates

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section >= searchRides.count){
        return 44;
    }
    return 310;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return searchRides.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //GuestCreateRideCell *cell = [tableView dequeueReusableCellWithIdentifier:@"guestRideCell"];
    
    /*cell.amount.text = [NSString stringWithFormat:@"%@%.2lf",[currentLocale getCurrencySymbolFromCurrencyCode:ride.currency],ride.amountPerSeat.doubleValue];
    cell.maleRatingView.rating = 5.0f;
    cell.maleRatingView.editable = NO;
    cell.femaleRatingView.rating = 5.0f;
    cell.femaleRatingView.editable = NO;
    cell.vehicleImage.hidden = YES;
    cell.vehicleLockImage.hidden = YES;
    cell.vehicleLockImage.image = [lockImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.vehicleLockImage setTintColor:[UIColor colorWithRed:(168.0/255.0) green:(168.0/255.0) blue:(168.0/255.0) alpha:1.0]];
    [cell.vehicleImage sd_setImageWithURL:[NSURL URLWithString:ride.vehicleImgUrl] placeholderImage:carImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        cell.vehicleImage.image = [ImageHelper blurredImageWithImage:cell.vehicleImage.image];
        cell.vehicleImage.hidden = NO;
        cell.vehicleLockImage.hidden = NO;
    }];
    cell.driverImage.hidden = YES;
    cell.driverLockImage.hidden = YES;
    cell.driverLockImage.image = [lockImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.driverLockImage setTintColor:[UIColor colorWithRed:(168.0/255.0) green:(168.0/255.0) blue:(168.0/255.0) alpha:1.0]];
    [cell.driverImage sd_setImageWithURL:[NSURL URLWithString:ride.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
         cell.driverImage.image = [ImageHelper blurredImageWithImage:cell.driverImage.image];
        cell.driverImage.hidden = NO;
        cell.driverLockImage.hidden = NO;
    }];
    cell.availableSeats.text = ride.availableSeats.stringValue;
    cell.srcIcon.image = [sourceIconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.srcIcon setTintColor:[UIColor colorWithRed:(255.0/255.0) green:(178.0/255.0) blue:(63.0/255.0) alpha:1.0]];
    cell.destIcon.image = [destinationIconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.destIcon setTintColor:[UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0]];
    cell.srcAddress.text = ride.srcAdd;
    cell.destAddress.text = ride.destAdd;
    //return cell;
    */
    
    
    GuestUserSearchRide *ride = [searchRides objectAtIndex:indexPath.section];

    GuestSearchResultsCell *cellGuestSearch = [tableView dequeueReusableCellWithIdentifier:@"GuestSearchResultsCell"];
    cellGuestSearch.lblPriceNumber.text = [NSString stringWithFormat:@"%@%.2lf",[currentLocale getCurrencySymbolFromCurrencyCode:ride.currency],ride.amountPerSeat.doubleValue];
    cellGuestSearch.lblSourceAddress.text = ride.srcAdd;
    cellGuestSearch.lblDestinationAddress.text = ride.destAdd;
    cellGuestSearch.lblTravelDistance.text = ride.distance;
    
    [cellGuestSearch.imgDriverProfile sd_setImageWithURL:[NSURL URLWithString:ride.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        cellGuestSearch.imgDriverProfile.image = [ImageHelper blurredImageWithImage:cellGuestSearch.imgDriverProfile.image];
        cellGuestSearch.imgDriverProfile.hidden = NO;
    }];
    
    [self addDropShadowForView:cellGuestSearch.viewMain];
    [self addDropShadowForView:cellGuestSearch.viewDetails];
    
    [cellGuestSearch.viewMain setBackgroundColor:UIColor.whiteColor];
    [cellGuestSearch.viewDetails setBackgroundColor:UIColor.whiteColor];
    
    float pickupDeltaInt =  [ride.pickupDelta floatValue];
    if(pickupDeltaInt < 1) {
        pickupDeltaInt = pickupDeltaInt * 1000;
        cellGuestSearch.lblPickupDelta.text = [NSString stringWithFormat:@"%d Mtrs", (int)pickupDeltaInt];
    } else {
        cellGuestSearch.lblPickupDelta.text = [NSString stringWithFormat:@"%@ Kms", ride.pickupDelta];
    }
    
    float dropDeltaInt = [ride.dropDelta floatValue];
    if(dropDeltaInt < 1){
        dropDeltaInt = dropDeltaInt * 1000;
        cellGuestSearch.lblDropDelta.text = [NSString stringWithFormat:@"%d Mtrs",(int)dropDeltaInt ];
    } else {
        cellGuestSearch.lblDropDelta.text = [NSString stringWithFormat:@"%@ Kms",ride.dropDelta];
    }
    
    
    //cellGuestSearch.lblWorksAt.text = [NSString stringWithFormat:@"Works at %@", ride.companyName];
    cellGuestSearch.lblTime.text = [self getDateAndTimeForTrip:ride.departureTime strType:@"Time"];
    cellGuestSearch.lblDate.text = [self getDateAndTimeForTrip:ride.departureTime strType:@"Date"];
    
    
    
    return cellGuestSearch;
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0) return 0.0f;
    return 10.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

#pragma mark - Actions

-(IBAction) dismiss:(id)sender{
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction) loginToRequest:(UIButton *)sender{
    UserDataManager *userDataManager = [UserDataManager sharedInstance];
    [userDataManager setUserSearchData:_userSearchData];
    [(AppDelegate *)[UIApplication sharedApplication].delegate showLoginController];
    [userDataManager setShowDeepLinkController:NO];
}

-(IBAction) btnTappedJoinNow:(UIButton *)sender {
    
    UIStoryboard *stb = [UIStoryboard storyboardWithName:@"SignUp" bundle:NSBundle.mainBundle];
    SignUpNameController *signUpNameVC = [stb instantiateViewControllerWithIdentifier:@"SignUpNameController"];
    [self.navigationController pushViewController:signUpNameVC animated:true];
    
}

-(IBAction) btnTappedSignIn:(UIButton *)sender {
    UserDataManager *userDataManager = [UserDataManager sharedInstance];
    [userDataManager setUserSearchData:_userSearchData];
    [userDataManager setShowDeepLinkController:NO];
    [(AppDelegate *)[UIApplication sharedApplication].delegate showLoginController];
    
}



-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}

+(UINavigationController *)createGuestRideNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"GuestUser" bundle:[NSBundle mainBundle]];
    UINavigationController *guestRideNavController = [storyBoard instantiateViewControllerWithIdentifier:@"guestRideNavController"];
    return guestRideNavController;
}

//phase - 1

-(NSString *)getDateAndTimeForTrip:(NSString *)tripDateTime strType:(NSString *)strType {
    //NSLog(@"time is %@",tritDateTime);
    //ride.departureTime
    NSDateFormatter *dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    //[dateTimeFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    [dateTimeFormatter1 setDateFormat:@"MMM dd, yyyy HH:mm:ss a"];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    
    
    NSDate *departureTime = [dateTimeFormatter1 dateFromString:tripDateTime];
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    
    if ([strType  isEqual: @"Time"]) {
        NSString *timeStr = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureTime];
        return timeStr;
    } else if ([strType  isEqual: @"Date"]) {
        [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
        NSString *dateStr = [self getDateStringFromDepartureTime:departureTime];
        return dateStr;
    }
    
    NSString *timeStr = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureTime];
    NSString *dateStr = [self getDateStringFromDepartureTime:departureTime];
    return [NSString stringWithFormat:@"%@, %@", dateStr, timeStr];
}

-(NSString *)getDateStringFromDepartureTime:(NSDate *)date{
    NSDate *currentDayDate = [self getCurrentDayDate];
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:currentDayDate toDate:date options:0];
    NSInteger difference = [components day];
    if(difference == 0) return NSLocalizedString(CMON_GENERIC_TODAY, nil);
    else if(difference == 1) return NSLocalizedString(CMON_GENERIC_TOMORROW, nil);
    else return [dateFormatter1 stringFromDate:date];
}

-(NSDate *)getCurrentDayDate{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [calendar dateFromComponents:components];
}

-(void)addDropShadowForView:(UIView *)viewShadow{
    viewShadow.backgroundColor = [UIColor clearColor];
    viewShadow.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = CGSizeMake(0, 1);
    viewShadow.layer.shadowRadius = 2;
    viewShadow.layer.masksToBounds = NO;
}


@end
