//
//  ReferTermsAndCondController.swift
//  zify
//
//  Created by Anurag on 01/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class ReferTermsAndCondController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var tblReferTerms: UITableView!
    @IBOutlet weak var btnTermsAndCondNavTitle: UINavigationItem!
    
    var remoteConfig : RemoteConfig?
    let profileObjTemp = UserProfile.getCurrentUser()
    var arrZifyCoinsValues : [String] = ["","","","","200","300","500","1000"]
    let arrTerms : [String] = [
        NSLocalizedString(zify_coins_can_be_redeemed_by_person, comment: ""),
                               NSLocalizedString(zify_coins_will_be_only_given_for, comment: ""),
                               NSLocalizedString(zify_coins_can_be_credited_upto, comment: ""),
                               NSLocalizedString(company_strength, comment: ""),
                               "\(NSLocalizedString(below, comment: "")) 300",
                               "300 - 800",
                               "800 - 2000",
                               "\(NSLocalizedString(above, comment: "")) 2000",
                               NSLocalizedString(zify_coins_can_be_redeemed_from, comment: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblReferTerms.rowHeight = UITableViewAutomaticDimension
        tblReferTerms.estimatedRowHeight = 300
        tblReferTerms.tableFooterView = UIView()
        //let strAppLaunchOfferWall = AppDelegate.getInstance().remoteConfig.configValue(forKey: "app_launch_offerwalls").stringValue!
        //debugPrint(strAppLaunchOfferWall, "-----refer_your_org_terms-----terms obje")
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat, view : UIView) {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
        view.layoutSubviews()
    }
    
    // MARK:- UITableView Delegates
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: AppDelegate.screen_WIDTH(), height: 0))
        viewHeader.backgroundColor = UIColor.white
        let lblSectionHeaderText = UILabel.init(frame: CGRect.init(x: 10, y: 0, width: AppDelegate.screen_WIDTH(), height: 0))
        lblSectionHeaderText.text = NSLocalizedString(terms_and_conditions, comment: "")
        lblSectionHeaderText.font = UIFont.init(name: MediumFont, size: 20)
        if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblSectionHeaderText.font = UIFont.init(name: MediumFont, size: 22)
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblSectionHeaderText.font = UIFont.init(name: MediumFont, size: 23)
        }
        lblSectionHeaderText.numberOfLines = 0
        lblSectionHeaderText.sizeToFit()
        viewHeader.addSubview(lblSectionHeaderText)
        viewHeader.frameHeight = lblSectionHeaderText.frameHeight
        return viewHeader
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTerms.count + 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 8 {
            let cellTerms : ReferTermsAndCondCell = (self.tblReferTerms.dequeueReusableCell(withIdentifier: "ReferTermsAndCondCell") as? ReferTermsAndCondCell)!
            cellTerms.lblText.text = arrTerms[indexPath.row]
            cellTerms.selectionStyle = .none
            return cellTerms
        }
        
        if indexPath.row == 3 {
            let cellReferSubHeading : ReferTermsSubHeading = (self.tblReferTerms.dequeueReusableCell(withIdentifier: "ReferTermsSubHeading") as? ReferTermsSubHeading)!
            cellReferSubHeading.lblHeading1.text = arrTerms[indexPath.row]
            cellReferSubHeading.lblHeading2.text = NSLocalizedString(zify_coins, comment: "")
            return cellReferSubHeading
        }
        
        if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 {
            let cellReferTermsData : ReferTermsData = (self.tblReferTerms.dequeueReusableCell(withIdentifier: "ReferTermsData") as? ReferTermsData)!
            cellReferTermsData.lblDetailText1.text = arrTerms[indexPath.row]
            cellReferTermsData.lblDetailText2.text = arrZifyCoinsValues[indexPath.row]
            return cellReferTermsData
        }
        
        return UITableViewCell()
        
    }
    
    
}

class ReferTermsAndCondCell : UITableViewCell {
    @IBOutlet weak var lblText: UILabel!
}

class ReferTermsSubHeading : UITableViewCell {
    @IBOutlet weak var lblHeading1: UILabel!
    @IBOutlet weak var lblHeading2: UILabel!
    @IBOutlet weak var viewBg: UIView!
}

class ReferTermsData : UITableViewCell {
    @IBOutlet weak var lblDetailText1: UILabel!
    @IBOutlet weak var lblDetailText2: UILabel!
    @IBOutlet weak var viewBg: UIView!

}
