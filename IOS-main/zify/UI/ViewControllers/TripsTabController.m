//
//  TripsTabController.m
//  zify
//
//  Created by Anurag S Rathor on 22/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripsTabController.h"
#import "LocalisationConstants.h"

#define RIDES_CONTROLLER_INDEX 0
#define DRIVES_CONTROLLER_INDEX 1
#define HISTORY_CONTROLLER_INDEX 2

@interface TripsTabController ()

@end

@implementation TripsTabController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITabBar *tabBar = self.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
   // UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
   // [tabBar setTintColor:[UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0]];
    [tabBar setTintColor:[UIColor colorWithRed:38.0/255.0 green:98.0/255.0 blue:255.0/255.0 alpha:1.0]];
    tabBarItem1.title = NSLocalizedString(VC_TRIPSTAB_RIDES, nil);
    tabBarItem2.title = NSLocalizedString(VC_TRIPSTAB_DRIVES, nil);
   // tabBarItem3.title = NSLocalizedString(VC_TRIPSTAB_HISTORY, nil);
    [[UITabBarItem appearance]
     setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0] , NSForegroundColorAttributeName: [UIColor lightGrayColor]} forState:UIControlStateNormal];
    [[UITabBarItem appearance]
     setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:38.0/255.0 green:98.0/255.0 blue:255.0/255.0 alpha:1.0]} forState:UIControlStateSelected];
    [tabBarItem1 setImage:[UIImage imageNamed:@"icn_trips_search_ride.png"]];
    [tabBarItem2 setImage:[UIImage imageNamed:@"icn_trips_offer_ride.png"]];
   // [tabBarItem3 setImage:[UIImage imageNamed:@"icn_history.png"]];
    [tabBarItem1 setSelectedImage:[UIImage imageNamed:@"icn_trips_search_ride.png"]];
    [tabBarItem2 setSelectedImage:[UIImage imageNamed:@"icn_trips_offer_ride.png"]];
  //  [tabBarItem3 setSelectedImage:[UIImage imageNamed:@"icn_history.png"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(UINavigationController *)createTripsTabNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Trips" bundle:[NSBundle mainBundle]];
    UINavigationController *tripsTabNavController = [storyBoard instantiateViewControllerWithIdentifier:@"tripsTabNavController"];
    return tripsTabNavController;
}
-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)selectTripsDrives{
    [self setSelectedIndex:DRIVES_CONTROLLER_INDEX];
}
@end

