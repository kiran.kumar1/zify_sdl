//
//  UserFavouritesController.m
//  zify
//
//  Created by Anurag S Rathor on 23/02/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "UserFavouritesController.h"
#import "ChatManager.h"
#import "ServerInterface.h"
#import "UserFavouriteRequest.h"
#import "UserFavouriteCell.h"
#import "SearchRide.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DriverDetail.h"
#import "CurrentLocale.h"
#import "RouteInfoView.h"
#import "UserProfileDetailView.h"
#import "UniversalAlert.h"
//#import "UserWalletController.h"
#import "CreateRideRequest.h"
#import "AppUtilites.h"
#import "TripsTabController.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "zify-Swift.h"

@interface UserFavouritesController ()

@end

@implementation UserFavouritesController{
    NSMutableArray *userFavourites;
    NSDateFormatter *dateTimeFormatter1;
    NSDateFormatter *dateTimeFormatter2;
    NSDateFormatter *dateFormatter;
   // NSDateFormatter *timeFormatter;
    NSDate *currentDayDate;
    UIImage *profileImagePlaceHolder;
    BOOL isViewInitialised;
    BOOL isUserChatConnected;
    CurrentLocale *currentLocale;
    UIImage *sourceIconImage;
    UIImage *destinationIconImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isViewInitialised = false;
    userFavourites = nil;
    dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    dateTimeFormatter2 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter2 setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
//    timeFormatter = [[NSDateFormatter alloc] init];
//    [timeFormatter setDateFormat:@"HH:mm"];
//    [timeFormatter setLocale:locale];
    currentDayDate = [self getCurrentDayDate];
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    currentLocale = [CurrentLocale sharedInstance];
    sourceIconImage = [UIImage imageNamed:@"icn_source_info.png"];
    destinationIconImage = [UIImage imageNamed:@"icn_destination_info.png"];
    _tableView.estimatedRowHeight = 240.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    // Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised){
        [self getFavouriteDrives];
        isViewInitialised = true;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [userFavourites count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        UserFavouriteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userFavouriteCell"];
        SearchRide *ride = [userFavourites objectAtIndex:indexPath.section];
        DriverDetail *driverDetail = ride.driverDetail;
        cell.driverImage.hidden = YES;
        [cell.driverImage sd_setImageWithURL:[NSURL URLWithString:driverDetail.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
            cell.driverImage.hidden = NO;
        }];
        cell.driverImage.layer.cornerRadius = cell.driverImage.frame.size.width / 2;
        cell.driverImage.clipsToBounds = YES;
        cell.driverName.text = [NSString stringWithFormat:@"%@ %@",driverDetail.firstName,driverDetail.lastName];
        cell.amount.text = [NSString stringWithFormat:@"%@%.2lf",[currentLocale getCurrencySymbol],ride.amountPerSeat.doubleValue];
        cell.srcAddress.text = [NSString stringWithFormat:@"%@, %@",ride.srcAdd,ride.city];
        cell.destAddress.text = [NSString stringWithFormat:@"%@, %@",ride.destAdd,ride.destCity];
        cell.srcIcon.image = [sourceIconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.srcIcon setTintColor:[UIColor colorWithRed:(255.0/255.0) green:(178.0/255.0) blue:(63.0/255.0) alpha:1.0]];
        cell.destIcon.image = [destinationIconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [ cell.destIcon setTintColor:[UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0]];
        NSDate *departureDateTime = [dateTimeFormatter1 dateFromString:ride.departureTime];
        cell.departureDate.text = [self getDateStringFromDepartureTime:departureDateTime];
        cell.departureTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureDateTime];
        if([NSLocalizedString(CMON_GENERIC_TODAY, nil) isEqualToString:cell.departureDate.text]){
            cell.departureDate.textColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
            cell.departureTime.textColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
        } else{
            cell.departureDate.textColor = [UIColor lightGrayColor];
            cell.departureTime.textColor = [UIColor lightGrayColor];
        }
        if(isUserChatConnected && driverDetail.chatProfile){
            cell.chatImage.hidden = NO;
            cell.chatButton.hidden = NO;
        } else{
            cell.chatImage.hidden = YES;
            cell.chatButton.hidden = YES;
        }
        if(ride.isExpiredDrive.intValue){
            cell.expiryImage.hidden = NO;
            cell.requestButton.enabled = NO;
            [cell.requestButton setBackgroundColor:[UIColor lightGrayColor]];
        } else{
            cell.expiryImage.hidden = YES;
            cell.requestButton.enabled = YES;
             [cell.requestButton setBackgroundColor:[UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0]];
        }
        return cell;
 }


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction) showRouteMap:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRide *searchRide = [userFavourites objectAtIndex:indexPath.section];
    [RouteInfoView showRouteInfoOnView:self.navigationController.view WithSearchRide:searchRide];
}

-(IBAction) showChat:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRide *ride = [userFavourites objectAtIndex:indexPath.section];
    DriverDetail *driverDetail = ride.driverDetail;
    UserContactDetail *detail = [[UserContactDetail alloc] initWithChatIdentifier:driverDetail.chatProfile.chatUserId andCallIdentifier:nil andFirstName:driverDetail.firstName andLastName:driverDetail.lastName andImageUrl:driverDetail.profileImgUrl];
    UINavigationController *chatNavController = [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    chatUserListContoller.receivingUser = detail;
    [self presentViewController:chatNavController animated:YES completion:nil];
}


-(IBAction) removeFavourite:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRide *searchRide = [userFavourites objectAtIndex:indexPath.section];
    NSNumber *subscribeFlag = @0;
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[UserFavouriteRequest alloc] initWithRequestType:SUBSCRIBEFAVDRIVER andSearchRide:searchRide andSourceLocality:nil andDestLocality:nil andSubscibeFlag:subscribeFlag] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [self getFavouriteDrives];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}



-(IBAction) createRideRequest:(UIButton *)sender{
   /* NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRide *searchRide = [userFavourites objectAtIndex:indexPath.section];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[CreateRideRequest alloc] initWithSearchRide:searchRide] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    if(response.messageCode.intValue== 5){
                        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_USERFAVOURITES_RECHARGENOW, nil) WithMessage:response.message];
                        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                            //UINavigationController *walletNavController = (UINavigationController *) [AddMoneyController createUserWalletNavController];
                            //AddMoneyController *walletController = (AddMoneyController *)walletNavController.topViewController;
                            //walletController.rechargeAmount = response.responseObject;
                            //[self presentViewController:walletNavController animated:YES completion:nil];
                        }];
                        [alert showInViewController:self];
                    } else{
                        [self.messageHandler showSuccessMessage:response.message];
                        [self performSelector:@selector(showTrips) withObject:self afterDelay:1.0];
                    }
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];*/
}


+(UINavigationController *)createUserFavouritesNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Commons" bundle:[NSBundle mainBundle]];
    UINavigationController *userFavouritesNavController = [storyBoard instantiateViewControllerWithIdentifier:@"userFavouritesNavController"];
    return userFavouritesNavController;
}

-(void) getFavouriteDrives{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:[[UserFavouriteRequest alloc] initWithRequestType:GETUSERFAVOURITES andDepartureTime:[dateTimeFormatter2 stringFromDate:[NSDate date]]] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    userFavourites = [NSMutableArray arrayWithArray:(NSArray *)response.responseObject];
                    [self handleNotificationsResponse];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void)handleNotificationsResponse{
    if([userFavourites count] == 0){
        self.tableView.hidden = true;
        self.noUserFavouritesView.hidden = false;
    } else{
        self.tableView.hidden = false;
        self.noUserFavouritesView.hidden = true;
        [self.tableView reloadData];
    }
}


-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}


-(NSDate *)getCurrentDayDate{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [calendar dateFromComponents:components];
}

-(NSString *)getDateStringFromDepartureTime:(NSDate *)date{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:currentDayDate toDate:date options:0];
    NSInteger difference = [components day];
    if(difference == 0) return NSLocalizedString(CMON_GENERIC_TODAY, nil);
    else if(difference == 1) return NSLocalizedString(CMON_GENERIC_TOMORROW, nil);
    else return [dateFormatter stringFromDate:date];
}

-(void)showTrips{
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        [[AppUtilites applicationVisibleViewController] presentViewController:[TripsTabController createTripsTabNavController] animated:YES completion:nil];
    }];
}
@end
