//
//  UserProfileController.h
//  zify
//
//  Created by Anurag S Rathor on 27/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"
#import "ScrollableContentViewController.h"

@interface UserProfileController : ScrollableContentViewController
@property(nonatomic,weak) IBOutlet UIImageView *userImage;
@property(nonatomic,weak) IBOutlet UIImageView *userVerificationImage;
@property(nonatomic,weak) IBOutlet UILabel *userName;
@property(nonatomic,weak) IBOutlet RatingView *maleRatingView;
@property(nonatomic,weak) IBOutlet RatingView *femaleRatingView;
@property(nonatomic,weak) IBOutlet UILabel *phoneNumber;
@property(nonatomic,weak) IBOutlet UIImageView *mobileVerifiedImage;
@property(nonatomic,weak) IBOutlet UILabel *email;
@property(nonatomic,weak) IBOutlet UILabel *vehicleDetails;
@property(nonatomic,weak) IBOutlet UILabel *idCardDetails;
+(UINavigationController *)createUserProfileNavController;

@property(nonatomic,weak) IBOutlet UILabel *lblPreferenceTitle;
@property(nonatomic,weak) IBOutlet UILabel *lblPreferenceSubTitle;
@property(nonatomic,weak) IBOutlet UILabel *lblPhoneNumberTitle;
@property(nonatomic,weak) IBOutlet UILabel *lblEmailTitle;
@property(nonatomic,weak) IBOutlet UILabel *lblVehicleDetailsTitle;
@property(nonatomic,weak) IBOutlet UILabel *lblIdentificationTitle;
@property(nonatomic,weak) IBOutlet UIButton *btnEditProfile;


@end
