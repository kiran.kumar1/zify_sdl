//
//  AboutSupportController.m
//  zify
//
//  Created by Anurag S Rathor on 13/08/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "AboutSupportController.h"
#import "GenericWebController.h"
#import "AppUtilites.h"
#import "UserProfile.h"
#import "AppUtilites.h"
#import "ImageHelper.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "AboutController.h"
#import "zify-Swift.h"
#import <FreshchatSDK.h>

#define SCREENSHOT_PLACEHOLDER_DEFAULT_TAG 1000

@interface AboutSupportController ()

@end

@implementation AboutSupportController {
    NSMutableArray *selectedImages;
    NSInteger selectedImageIndex;
    UserProfile *currentUser;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addDropShadowForView:_btnFreshChat];
    [self initialiseView];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
    if (IS_IPHONE_5) {
        _lblPleaseDescYourPrblm.fontSize = 11;
        _btnReadOurFAQ.titleLabel.fontSize = 13;
        _lblAddScreenShots.fontSize = 12;
        _lblReadOurPrivacyPolicy.fontSize = 10;
        _btnPrivacyPolicy.titleLabel.fontSize = 11;
        _btnFreshChat.titleLabel.fontSize = 13;
    } else if (IS_IPHONE_6) {
        _lblPleaseDescYourPrblm.fontSize = 13;
        _btnReadOurFAQ.titleLabel.fontSize = 14;
        _lblAddScreenShots.fontSize = 13;
        _lblReadOurPrivacyPolicy.fontSize = 11;
        _btnPrivacyPolicy.titleLabel.fontSize = 12;
        _btnFreshChat.titleLabel.fontSize = 15;

    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _lblPleaseDescYourPrblm.fontSize = 13;
        _btnReadOurFAQ.titleLabel.fontSize = 15;
        _lblAddScreenShots.fontSize = 14;
        _lblReadOurPrivacyPolicy.fontSize = 12;
        _btnPrivacyPolicy.titleLabel.fontSize = 13;
        _btnFreshChat.titleLabel.fontSize = 15;
    }
    
}

-(void)initialiseView{
    selectedImages = [[NSMutableArray alloc] init];
    for(int counter = 0;counter < _screenshotPlaceholders.count;counter ++){
        [selectedImages addObject:[NSNull null]];
    }
  //  [self.issueTextView becomeFirstResponder];
    currentUser = [UserProfile getCurrentUser];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
}


-(IBAction) dismiss:(id)sender{
    [self.view endEditing:YES];
    NSUserDefaults *prefs =  [NSUserDefaults standardUserDefaults];
    BOOL isDismiss = [prefs boolForKey:@"forDismissing"];
    if(isDismiss){
        [prefs setBool:NO forKey:@"forDismissing"];
        [prefs synchronize];
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
       [self.navigationController popViewControllerAnimated:NO];
    }
}

-(IBAction) submitProblem:(id)sender{
    if([self validateSupportMailInfo]){
        [self.view endEditing:YES];
        NSString *messageBody = [NSString stringWithFormat:@"%@\n\n-----------------------\nEmail: %@\nBuild Version: %@ | iOS Version: %@ | Device Model: %@",self.issueTextView.text,currentUser.emailId, [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],[[UIDevice currentDevice] systemVersion],[[UIDevice currentDevice] model]];
        NSDictionary *dictionary = @{
                                     @"subject":@"Zify App Feedback [iOS]",
                                     @"messageBody":messageBody,
                                     @"recipients":@"support@zify.co"
                                     };
        //support@zify.co
        NSMutableArray *attachmentArray = [[NSMutableArray alloc] init];
        for(int index=0;index < selectedImages.count;index++){
            if(![selectedImages[index] isKindOfClass:[NSNull class]]){
                [attachmentArray addObject:UIImagePNGRepresentation(selectedImages[index])];
            }
        }
        NSUserDefaults *prefs =  [NSUserDefaults standardUserDefaults];
        BOOL isDismiss = [prefs boolForKey:@"forDismissing"];
        if(isDismiss){
            [prefs setBool:NO forKey:@"forDismissing"];
            [prefs synchronize];
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self.navigationController popViewControllerAnimated:NO];
        }
         [AppUtilites shareEmailWithData:dictionary withDelegate:_messageDelegate addAttachmentArray:attachmentArray andFileType:@"png" andMimeType:@"image/png"];
    }
}
-(IBAction) showFAQ:(id)sender{
    UINavigationController *genericWebNavController = [GenericWebController createGenericWebNavController];
    GenericWebController *webController = (GenericWebController *)genericWebNavController. topViewController;
    webController.webViewURL = @"https://support.zify.co/portal/kb";//@"https://zify.zendesk.com/hc/en-us"
    webController.title =  NSLocalizedString(CMON_GENERIC_FAQ, nil);
    [self presentViewController:genericWebNavController animated:YES completion:nil];
}

-(IBAction)selectImage:(UIButton *)sender{
    selectedImageIndex = [sender tag] - SCREENSHOT_PLACEHOLDER_DEFAULT_TAG;
    [ImageSelectionViewer sharedInstance].imageSelectionDelegate = self;
    [[ImageSelectionViewer sharedInstance] openGallery];
}

-(IBAction) showTerms:(id)sender{
    UINavigationController *genericWebNavController = [GenericWebController createGenericWebNavController];
    GenericWebController *webController = (GenericWebController *)genericWebNavController. topViewController;
    webController.webViewURL = NSLocalizedString(CMON_GENERIC_TERMSURL,nil);
    webController.title = NSLocalizedString(CMON_GENERIC_TNC, nil);
    [self presentViewController:genericWebNavController animated:YES completion:nil];
}

-(void) userSelectedFinalImage:(UIImage *)image{
    [selectedImages setObject:[ImageHelper imageWithImage:image scaledToSize:CGSizeMake(image.size.width/3, image.size.height/3)] atIndexedSubscript:selectedImageIndex];
    UIView *placeHolderView = [_screenshotPlaceholders objectAtIndex:selectedImageIndex];
    placeHolderView.hidden = YES;
    UIImageView *selectedImageView = [_screenshotImages objectAtIndex:selectedImageIndex];
    selectedImageView.image = image;
}

-(BOOL) allowImageEditing{
    return NO;
}

-(BOOL) validateSupportMailInfo{
    if([@"" isEqualToString:self.issueTextView.text]){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_ABOUTSUPPORT_PROBLEMDESCRIBEVAL, nil)];
        return false;
    }
    return true;
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(IBAction)showFreshChat:(id)sender{
    [[Freshchat sharedInstance] showConversations:self];
}
-(void)addDropShadowForView:(UIView *)viewShadow {
    viewShadow.backgroundColor = [UIColor whiteColor];
    viewShadow.layer.shadowColor = [UIColor colorWithRed:117.0/255.0 green:117.0/255.0 blue:117.0/255.0 alpha:0.4].CGColor;
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = CGSizeMake(-1, 0);
    viewShadow.layer.shadowRadius = 4;
    viewShadow.layer.masksToBounds = NO;
}
@end
