//
//  UserIDCardDetailsController.h
//  zify
//
//  Created by Anurag S Rathor on 11/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"
#import "CustomPickerView.h"
#import "ImageSelectionViewer.h"

@interface UserIDCardDetailsController : UIViewController<CustomPickerDelegate,ImageSelectionViewerDelegate>
@property (nonatomic,weak) IBOutlet UIImageView *closePageArrow;
@property (nonatomic,weak) IBOutlet UILabel *pageTitle;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic,weak) IBOutlet UILabel *identityCardType;
@property (nonatomic,weak) IBOutlet UIImageView *identityCardTypeArrow;
@property (nonatomic,weak) IBOutlet UIView *identityCardTypeView;
@property (nonatomic,weak) IBOutlet UIImageView *identityCardImage;
@property (nonatomic,weak) IBOutlet UIView *identityCardPlaceholder;
@property (nonatomic,weak) IBOutlet UILabel *errorMessage;
@property (nonatomic,weak) IBOutlet UIButton *uploadIdCardBtn;
@property (nonatomic,weak) IBOutlet UIView *uploadIdCardView;
@property (nonatomic,weak) IBOutlet UIView *remindLaterView;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet CustomPickerView *identityCardPickerView;
@property (nonatomic,weak) IBOutlet UILabel *lblSafe;
@property (nonatomic,weak) IBOutlet UILabel *lblIdCardTypeSelected;
@property (nonatomic,weak) IBOutlet UILabel *lblIdCardSecureStoredText;
@property (nonatomic,weak) IBOutlet UILabel *lblNotShowAnyOneIDCardText;

@property (nonatomic) BOOL isUpgradeFlow;
+(UserIDCardDetailsController *)createUserIDCardDetailsController;
+(UINavigationController *)userIDCardDetailsNavCtrl;

@property (weak, nonatomic) IBOutlet UILabel *lblTapToUploadText;
@property (weak, nonatomic) IBOutlet UILabel *lblTakePictureText;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnRemindLater;
//@property (weak, nonatomic) IBOutlet UINavigationItem *navBar;


@end
