//
//  UserBaseController.m
//  zify
//
//  Created by Anurag S Rathor on 30/08/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserBaseController.h"
#import "GenericRequest.h"
#import "ServerInterface.h"
#import "UniversalAlert.h"
#import "UserProfile.h"
#import "OnboardFlowHelper.h"
#import "FacebookLogin.h"
#import "ChatManager.h"
#import "CurrentLocale.h"
#import "PushNotificationManager.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "LOTAnimationView.h"
#import "zify-Swift.h"


@interface UserBaseController ()

@end

@implementation UserBaseController

- (void)viewDidLoad {
    [super viewDidLoad];
    [AppDelegate applyVerticalGradient:_bgImageView];
    UIImage *logoImage = [UIImage imageNamed:@"logoImg_New.png"];
    CGFloat factor = 4;
    if(!IS_IPHONE_5){
        factor = 4;
    }
    
    _logoImageView = [[UIImageView alloc] init];
    _logoImageView.frame = CGRectMake(0, 0, logoImage.size.width/factor, logoImage.size.height/factor);
    _logoImageView.image= logoImage;
    _logoImageView.frameX = ([AppDelegate SCREEN_WIDTH] - _logoImageView.frameWidth)/2;
    _logoImageView.frameY = ([AppDelegate SCREEN_HEIGHT] - _logoImageView.frameHeight)/2;
    [_bgImageView addSubview:_logoImageView];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"location_lottie_anim" ofType:@"json"];
    NSLog(@"path is %@", path);
    
   
    CGFloat height = 80;
    CGFloat posY = 25;
    if([AppDelegate SCREEN_HEIGHT] == 812.0){
        posY = 80;
    }
    LOTAnimationView *animationView =[LOTAnimationView animationFromJSON:[self JSONFromFile]];
    animationView.loopAnimation = YES;
    animationView.frame = CGRectMake(0, 0, height, height);
    animationView.frameX = ([AppDelegate SCREEN_WIDTH] - animationView.frameWidth)/2 - 10;
    animationView.frameY = ([AppDelegate SCREEN_HEIGHT] - animationView.frameHeight) - posY;
    animationView.backgroundColor = [UIColor clearColor];
    [_bgImageView addSubview:animationView];
    [animationView playWithCompletion:^(BOOL animationFinished) {
        // Do Something
    }];
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self autoLoginUser];


}
- (NSDictionary *)JSONFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"location_lottie_anim" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

-(void)autoLoginUser{
    if(![[ServerInterface sharedInstance] isConnectedToInternet]){
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORTITLE, nil) WithMessage:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORMESSAGE, nil)];
        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY, nil) WithAction:^(void *action){
            [self autoLoginUser];
        }];
        [alert showInViewController:self];
    } else{
        [[CurrentLocale sharedInstance] setLocaleValuesFromUser:[UserProfile getCurrentUser]];//Setting these values here to send in global request
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setBool:YES forKey:@"donotDisplayHud"];
        [prefs synchronize];
        [self.messageHandler showTransparentBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance] getResponse:[[GenericRequest alloc] initWithRequestType:AUTOLOGINREQUEST] withHandler:^(ServerResponse *response, NSError *error){
                 [self.messageHandler dismissTransparentBlockingLoadViewWithHandler:^{
                     if(response){
                         if(response.messageCode.intValue == 1){
                             [MoEngageEventsClass callAutoLoginEvent];
                             [[OnboardFlowHelper sharedInstance] handleLoginFlow:self andIsNewLogin:NO];
                             [[PushNotificationManager sharedInstance] handlePushRegisterWithUser];
                         } else{
                            [[PushNotificationManager sharedInstance] deregisterDeviceForPushOnLogout]; //Should be called before deleting user since userId is required here.
                             [UserProfile deleteCurrentUser];
                             [[ChatManager sharedInstance] deleteCurrentUserChatProfile];
                             [[OnboardFlowHelper sharedInstance] clearReminder];
                             [[FacebookLogin sharedInstance] closeSessionAndClearSavedTokens];
                             [(AppDelegate *)[UIApplication sharedApplication].delegate showLoginController];
                        }
                     } else{
                         UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:@"Error"  WithMessage:error.userInfo[@"message"]];
                         [alert addButton:BUTTON_OK WithTitle:@"Retry" WithAction:^(void *action){
                             [self autoLoginUser];
                         }];
                         [alert showInViewController:self];
                     }
                 }];
               
            }];
        }];
    }
}


@end
