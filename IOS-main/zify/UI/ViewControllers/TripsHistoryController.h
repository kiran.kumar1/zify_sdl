//
//  TripsHistoryController.h
//  zify
//
//  Created by Anurag S Rathor on 24/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"

@interface TripsHistoryController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *noHistoryView;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@end
