//
//  NotificationsController.m
//  zify
//
//  Created by Anurag S Rathor on 05/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "NotificationsController.h"
#import "ServerInterface.h"
#import "GenericRequest.h"
#import "UserNotification.h"
#import "UserNotificationDetail.h"
#import "RiderProfile.h"
#import "RideDriveActionRequest.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChatManager.h"
#import "ChatUserListController.h"
#import "UserProfileDetailView.h"
#import "NotificationViewCell.h"
#import "UserProfile.h"
#import "LocalisationConstants.h"
#import "UserIDCardDetailsController.h"
#import "UserAddressPreferencesController.h"
#import "VehicleDetailsController.h"
#import "zify-Swift.h"

@interface NotificationsController ()

@end

@implementation NotificationsController {
    UserProfile *currentUser;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = NSLocalizedString(CS_NOTIFICATIONS_NAV_TITILE, nil);
    [self setBoolInUserPrefences:NO];
    self.navigationController.navigationBarHidden = NO;
    self.tblViewNotifications.multipleTouchEnabled = NO;
    _noNotificationsView.hidden = YES;
    dataArray = [[NSMutableArray alloc] init];
    [_tblViewNotifications reloadData];
    [self getDateFromUserDetails];
    
   
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    //self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
   
}


-(void)getDateFromUserDetails{
   
    currentUser = [UserProfile getCurrentUser];

    NSString *isIdCardUploadedStr = [currentUser.userDocuments.isIdCardDocUploaded stringValue];
    NSString *isVehicleImgUploadedStr = [currentUser.userDocuments.isVehicleImgUploaded stringValue];

    if(![isIdCardUploadedStr isEqualToString:@"1"]){
    [self createDictionaryAndAdditToDataArray:0];
    }
    if(!currentUser.userPreferences){
    [self createDictionaryAndAdditToDataArray:1];
    }
    if(!currentUser.profileImgUrl || currentUser.profileImgUrl.length == 0){
            [self createDictionaryAndAdditToDataArray:2];
    }
    if(currentUser.dob.length == 0 || currentUser.gender.length == 0 || currentUser.companyName.length == 0 || currentUser.companyEmail.length == 0 || currentUser.bloodGroup.length == 0 || currentUser.emergencyContact.length == 0){
        [self createDictionaryAndAdditToDataArray:3];
    }
    if([currentUser.userPreferences.userMode isEqualToString:@"DRIVER"] && ![isVehicleImgUploadedStr isEqualToString:@"1"]){
       [self createDictionaryAndAdditToDataArray:4];
    }
    if(dataArray.count == 0){
        _tblViewNotifications.hidden = YES;
        _noNotificationsView.hidden = NO;
    }else{
        _tblViewNotifications.hidden = NO;
        _noNotificationsView.hidden = YES;
        [_tblViewNotifications reloadData];
    }
}
-(void)createDictionaryAndAdditToDataArray:(int)index{
    NSString *title,*subTitle,*imageName;
    NSString *position;
    switch (index) {
        case 0:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_UPLOAD_GOVT_ID, nil);
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_UPLOAD_GOVT_ID, nil);
            imageName = @"ID_Card";
            position = @"0";
            break;
            
        case 1:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_TP, nil);
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_TP, nil);
            imageName = @"Travel_Preferences";
            position = @"1";
            break;
            
        case 2:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_UPLOAD_PROFILE_PHOTO, nil);
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_UPLOAD_PROFILE_PHOTO, nil);
            imageName = @"ProfileNotifi";
            position = @"2";
            break;
            
        case 3:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_COMPLETE_PROFILE, nil);
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_COMPLETE_PROFILE, nil);
            imageName = @"ProfileCompleteN";
            position = @"3";
            break;
            
        case 4:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_FILL_CAR_DETAILS, nil);
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_FILL_CAR_DETAILS, nil);
            imageName = @"CarTP";
            position = @"4";
            break;
            
        default:
            break;
    }
    NSDictionary *infoDict = [[NSDictionary alloc] initWithObjectsAndKeys:title,@"title",subTitle,@"subTitle",imageName,@"imageName", position,@"position", nil];
    [dataArray addObject:infoDict];
    
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /* NotificationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell"];*/

    NSString *cellidentifier = @"notificationCell";
    NotificationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell == nil){
        cell = (NotificationViewCell *)[[NotificationViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
      //  cell.backgroundColor = [UIColor clearColor];
      //  cell.backgroundView = nil;
      //  cell.backgroundView.backgroundColor = [UIColor clearColor];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *infoDict = [dataArray objectAtIndex:indexPath.row];
    [cell setDataForCell:infoDict];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *infoDict = [dataArray objectAtIndex:indexPath.row];
    [self moveToAppropriateScreen:infoDict];
   // NSLog(@"selected rpw is %d", indexPath.row);
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(void)moveToAppropriateScreen:(NSDictionary *)infoDict{
    NSString *positionSytr = [infoDict objectForKey:@"position"];
    int position = [positionSytr intValue];
    if(positionSytr.length <= 0){
        position = -1;
    }
    switch (position) {
        case 0: [self moveToUploadGovtIDscreen];
                break;
        case 1: [self moveToTPScreen];
            break;
        case 2: [self moveToUploadProfilePhoto];
            break;
        case 3: [self moveToProfileScreen];
            break;
        case 4: [self moveToFillCarDetails];
            break;
        default:
            break;
    }
}
-(void)moveToUploadGovtIDscreen{
    [self setBoolInUserPrefences:YES];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"IDCard" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"UserIDCardController"];
    [self.navigationController pushViewController:vc animated:TRUE];
    //phase - 1
    
 /*   if ([currentUser.userDocuments.idCardImgUrl isEqualToString:@""] || [currentUser.userDocuments.idCardImgUrl isKindOfClass:[NSNull class]] || currentUser.userDocuments.idCardImgUrl == nil) {
        
        if ([currentUser.userPreferences.userMode isEqualToString:@"DRIVER"]) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"IDCard" bundle:nil];
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"UserIDDrivingLicenceController"];
            [self.navigationController pushViewController:vc animated:TRUE];
        } else {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"IDCard" bundle:nil];
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"UserIDCardController"];
            [self.navigationController pushViewController:vc animated:TRUE];
        }
    } else {
        [self.navigationController pushViewController:[UserIDCardDetailsController createUserIDCardDetailsController] animated:YES];
    }*/
    //[self.navigationController pushViewController:[UserIDCardDetailsController createUserIDCardDetailsController] animated:YES];
    
}

-(void)moveToTPScreen{
    [self setBoolInUserPrefences:YES];
    UserAddressPreferencesController *addressPreferencesController = [UserAddressPreferencesController createUserAddressPreferencesController];
    addressPreferencesController.showPageIndicator = NO;
    addressPreferencesController.isUpgradeFlow = NO;
    [self.navigationController pushViewController:addressPreferencesController animated:YES];
}
-(void)moveToUploadProfilePhoto{
    
    [self moveToProfileScreen];
}
-(void)moveToProfileScreen{
    [self setBoolInUserPrefences:YES];
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"UserProfile" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"editprofile"];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(void)moveToFillCarDetails{
   /* UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"UserProfile" bundle:[NSBundle mainBundle]];
    UINavigationController *vehicleDetailsNavController = [storyBoard instantiateViewControllerWithIdentifier:@"vehicleDetailsNavController"];
    VehicleDetailsController *vehicleDetailsController = (VehicleDetailsController *)vehicleDetailsNavController.topViewController;
    
    [self.navigationController pushViewController:vehicleDetailsController animated:YES];*/
    
    VehicleTextController *vehicleScreen = (VehicleTextController *) [VehicleTextController createVehcileTextController];
    [self.navigationController pushViewController:vehicleScreen animated:YES];
}
-(void)setBoolInUserPrefences:(BOOL)isForPush{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    [defs setBool:isForPush forKey:@"forPush"];
    [defs synchronize];
}

-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

+(UINavigationController *)createNotificationsNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Commons" bundle:[NSBundle mainBundle]];
    UINavigationController *walletNavController = [storyBoard instantiateViewControllerWithIdentifier:@"notificationsNavController"];
    return walletNavController;
}


@end
