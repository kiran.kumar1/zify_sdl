//
//  NotificationsController.h
//  zify
//
//  Created by Anurag S Rathor on 05/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArray;
}
@property (nonatomic,weak) IBOutlet UIView *noNotificationsView;
@property (nonatomic,weak)  IBOutlet UITableView *tblViewNotifications;
+(UINavigationController *_Nonnull)createNotificationsNavController;

@end
