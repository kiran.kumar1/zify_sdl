//
//  GuestHomeController.m
//  zify
//
//  Created by Anurag S Rathor on 19/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "GuestHomeController.h"

#import "SearchRideViewController.h"
#import "TripsTabController.h"
#import "AccountController.h"
//#import "NotificationsController.h"
#import "UserRideRequest.h"
#import "RideDriveActionRequest.h"
#import "UIImage+initWithColor.h"
#import "UniversalAlert.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "GuestUserRideController.h"
#import "UserDataManager.h"
#import "AppDelegate.h"
#import "TutorialShownDB.h"
#import "DBUserDataInterface.h"
#import "GetFDJpointsRequest.h"
#import "UserOfferRideRequest.h"

enum LocalityPickMode {
    PickingSourceLocality,
    PickingDestinationLocality
};

@interface GuestHomeController ()

@end

@implementation GuestHomeController {
    enum LocalityPickMode localityPickMode;
    LocalityInfo *sourceLocalityInfo;
    LocalityInfo *destinationLocalityInfo;
    NSArray *searchRides;
    NSString *rideStartDate;
    NSString *rideDateValue;
    NSString *rideTimeValue;
    NSDateFormatter *dateTimeFormatter1;
    NSDateFormatter *dateFormatter1;
    //NSDateFormatter *timeFormatter1;
    BOOL isInitialLocationFetch;
    CurrentLocale *currentLocale;
    BOOL isGlobalLocale;
    UserDataManager *userDataManager;
    UserSearchData *userSearchData;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.rideDateTimeView.hidden = true;
    self.destinationInfoView.hidden = true;
    
    [self.btnFirstTimeShowDateTimePicker.layer setBorderColor:UIColor.lightGrayColor.CGColor];
    [self.btnFirstTimeShowDateTimePicker.layer setBorderWidth:1];
    [self.btnFirstTimeShowDateTimePicker.layer setCornerRadius:5];
    [self.btnFirstTimeShowDateTimePicker.layer setMasksToBounds:NO];
    
    
    [self addDropShadowForView:self.viewDestnTimeDate];
    [self addDropShadowForView:self.viewTimeDate];
    
    [AppDelegate getAppDelegateInstance].topNavController = self;
    [self initialiseSearchRideScreen];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"TRUE" forKey:@"IsCarOwnerOrRiderSeleted"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.viewTimeDate.hidden = true;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnViewDateTime)];
    [self.viewTimeDate addGestureRecognizer:tapRecognizer];
    
}

- (void)tapOnViewDateTime {
    [self btnTappedShowDateTimePicker:self];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

/*-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self checkLocationServicesEnabledOrNOt];
    
}*/

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"status is %d", [CLLocationManager authorizationStatus]);
    
    [self checkToShow_FindOrShareRideView];
    
    [UIView animateWithDuration:1.5
                          delay:0.5
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                     }
                     completion:^(BOOL finished) {
                         if (finished)
                             
                        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"IsCarOwnerOrRiderSeleted"];
                         
                        _viewDestnTimeDateTravelPref.frame = CGRectMake(0, AppDelegate.SCREEN_HEIGHT - _viewDestnTimeDateTravelPref.frame.size.height + 30, AppDelegate.SCREEN_WIDTH, _viewDestnTimeDateTravelPref.frame.size.height);
                         
                        [self.view addSubview:_viewDestnTimeDateTravelPref];
                         
                     }];
    
    [self displayproperViewForLocationEnabledOrDisabled];
    
    
}

-(void)displayproperViewForLocationEnabledOrDisabled {
    if([CLLocationManager locationServicesEnabled]) {
        if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
            [self checkLocationServicesEnabledOrNOt];
        }else if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways){
            [self checkLocationServicesEnabledOrNOt];
        }else if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            if(sourceLocalityInfo && destinationLocalityInfo){
                [[AppDelegate getAppDelegateInstance] removeLocationAccessScreenIfEsists];
                [self setCurrentLocation:sourceLocalityInfo];
            }else{
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                BOOL isFromLocationDeniedScreen = [prefs boolForKey:@"isFromDeniedScreen"];
                if(isFromLocationDeniedScreen){
                    [prefs setBool:NO forKey:@"isFromDeniedScreen"];
                    [prefs synchronize];
                    [self moveToLocationSearchFromLocationsServicesDeniedScreen];
                }else{
                    [self checkLocationServicesEnabledOrNOt];
                }
            }
        }
    }
}

-(void)checkLocationServicesEnabledOrNOt {
    if([CLLocationManager locationServicesEnabled]) {
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
            [self.messageHandler dismissTransparentBlockingLoadViewWithHandler:^{
                [[AppDelegate getAppDelegateInstance] showLocationAccessScreen:self];
            }];
        } else {
                [[AppDelegate getAppDelegateInstance] removeLocationAccessScreenIfEsists];
            if(sourceLocalityInfo == nil){
                isInitialLocationFetch = true;
            }
            [self fetchCurrentLocation];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)initialiseSearchRideScreen{
    UIImage *navigationBackgroundImage = [UIImage imageWithColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4]];
    [self.navigationController.navigationBar setBackgroundImage:navigationBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSDate *currentDate = [NSDate date];
    rideStartDate = [dateTimeFormatter1 stringFromDate:currentDate];
    dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yy"];
    rideDateValue = [dateFormatter1 stringFromDate:currentDate];
    self.rideDateText.text = rideDateValue;
//    timeFormatter1 = [[NSDateFormatter alloc] init];
//    [timeFormatter1 setDateFormat:@"HH:mm"];
//    [timeFormatter1 setLocale:locale];
    rideTimeValue = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:currentDate];
    self.rideTimeText.text = rideTimeValue;
    
    rideStartDate = nil;
    rideDateValue = nil;
    rideTimeValue = nil;
    self.rideDateText.numberOfLines = 0;
    self.rideDateText.text = NSLocalizedString(SELECT_DATE_TIME, nil);
    self.rideTimeText.text = @"";
    
    isInitialLocationFetch = true;
    currentLocale = [CurrentLocale sharedInstance];
    userDataManager = [UserDataManager sharedInstance];
    _heremapsButton.hidden = YES;
}

-(void)fetchCurrentLocation{
    if(isInitialLocationFetch){
        if(![[ServerInterface sharedInstance] isConnectedToInternet]){
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORTITLE, nil) WithMessage:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORMESSAGE, nil)];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY,nil) WithAction:^(void *action){
                [self fetchCurrentLocation];
            }];
            [alert showInViewController:self];
        } else if(![[CurrentLocationTracker sharedInstance] isLocationTrackingEnabled]){
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORTITLE, nil) WithMessage:NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORMESSAGE, nil)];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY,nil) WithAction:^(void *action){
                [self fetchCurrentLocation];
            }];
            [alert showInViewController:self];
        } else{
            [self updateUserCurrentLocation];
        }
    }
}

#pragma mark - Actions
-(IBAction) btnTappedCarOwner:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:@"FALSE" forKey:@"IsCarOwnerOrRiderSeleted"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
   
    
}

-(IBAction) btnTappedTravelPreferences:(id)sender {
    
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:AppName
                                message:@"Proceed to login"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction
                                    actionWithTitle:@"Ok, Continue" style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [(AppDelegate *)[UIApplication sharedApplication].delegate showLoginController];
                                        [userDataManager setShowDeepLinkController:NO];
                                    }];
    UIAlertAction* skipAction = [UIAlertAction
                               actionWithTitle:@"Skip, May be later" style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
                               }];
    
    [alert addAction:skipAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    //    self.viewTravelPref.hidden = YES;
    //    self.viewFindOrShareRide.frame = CGRectMake(self.txtDestination.frame.origin.x, self.viewTravelPref.frame.origin.y, AppDelegate.SCREEN_WIDTH, self.viewFindOrShareRide.frame.size.height);
    //    [self.viewDestnTimeDateTravelPref addSubview:self.viewFindOrShareRide];
}

-(IBAction) btnTappedFindOrShareRide:(UIButton *)sender {
    if ([sender.titleLabel.text isEqual:@"Take a Ride"]) {
        [self searchRide:sender];
    } else if ([sender.titleLabel.text isEqual:@"Offer a Drive"]) {
        [self offerRide:sender];
    }
}

-(IBAction) btnTappedSwitchFindOrShareBtn:(UIButton *)sender {
    
    [UIView animateWithDuration:1 animations:^{
        self.btnFindOrShareRide.transform = CGAffineTransformMakeRotation(M_PI_4*2);
    }];
    
    
    [UIView transitionWithView:self.btnFindOrShareRide duration:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
    } completion:^(BOOL finished) {
        if (_btnSwitchMode.selected) {
            [self.btnFindOrShareRide setTitle:@"Take a Ride" forState:UIControlStateNormal];
            _btnSwitchMode.selected = false;
        } else {
            _btnSwitchMode.selected = true;
            [self.btnFindOrShareRide setTitle:@"Offer a Drive" forState:UIControlStateNormal];
        }
    }];
    
}

- (IBAction)btnTappedShowDateTimePicker:(id)sender {
    self.viewTimeDate.hidden = false;
    
    [self showDateAndTimePicker];
    
}

-(IBAction) showMenu:(id)sender{
    [GuestMenuController createAndOpenGuestMenuWithSourceController:self.navigationController.parentViewController andMenuDelegate:self];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

-(IBAction)selectSource:(id)sender{
    localityPickMode = PickingSourceLocality;
    UINavigationController *autoSugeestionNavController = [LocalityAutoSuggestionController createLocalityAutoSuggestNavController];
    LocalityAutoSuggestionController *autoSuggestionController = (LocalityAutoSuggestionController *)[autoSugeestionNavController topViewController];
    autoSuggestionController.isForSourceAddress = YES;
    autoSuggestionController.sourceLocality = sourceLocalityInfo;
    autoSuggestionController.destinationLocality = destinationLocalityInfo;
    NSString *title = NSLocalizedString(MS_SERACHRIDE_BTN_SEARCH, nil);
    autoSuggestionController.title = [title capitalizedString];
    autoSuggestionController.selectionDelegate = self;
    autoSuggestionController.showAutosuggestOnly = NO;
    [self presentViewController:autoSugeestionNavController animated:YES completion:nil];
}
-(IBAction)refreshUserCurrentLocation:(id)sender{
    [self updateUserCurrentLocation];
}

-(IBAction) selectRideDate:(UIButton *)sender{
    [self showDateAndTimePicker];
}

-(void)showDateAndTimePicker{
    if(selectedDateFromPicker != nil){
        self.dateTimePickerView.selectedDate = selectedDateFromPicker;
    }
    self.dateTimePickerView.delegate = self;
    [self.dateTimePickerView showInView:self.navigationController.view];
    
    if ((_btnFirstTimeShowDateTimePicker.isHidden == false) && [_txtDestination.text isEqual:@""]) {
        [self.viewTravelPref setHidden:FALSE];
    } else {
        [self.viewTravelPref setHidden:TRUE];
        
        self.viewFindOrShareRide.frame = CGRectMake(self.txtDestination.frame.origin.x, self.viewTravelPref.frame.origin.y, AppDelegate.SCREEN_WIDTH, self.viewFindOrShareRide.frame.size.height);
        [self.viewDestnTimeDateTravelPref addSubview:self.viewFindOrShareRide];
        
        NSString * text =  NSLocalizedString(GU_SWITCH_TIP, nil);

        
        //[EasyTipView showWithAnimated:YES forItem:_btnSwitchMode withinSuperview:self.navigationController.view text:text delegate:self];
    }
    
}

-(IBAction) selectDestination:(id)sender{
    localityPickMode = PickingDestinationLocality;
     UINavigationController *autoSugeestionNavController = [LocalityAutoSuggestionController createLocalityAutoSuggestNavController];
    LocalityAutoSuggestionController *autoSuggestionController = (LocalityAutoSuggestionController *)[autoSugeestionNavController topViewController];
    autoSuggestionController.isForSourceAddress = NO;
    autoSuggestionController.sourceLocality = sourceLocalityInfo;
    autoSuggestionController.destinationLocality = destinationLocalityInfo;
    NSString *title = NSLocalizedString(MS_SERACHRIDE_BTN_SEARCH, nil);
    autoSuggestionController.title =  [title capitalizedString];
    autoSuggestionController.selectionDelegate = self;
    autoSuggestionController.showAutosuggestOnly = NO;
  //  [self.navigationController pushViewController:autoSugeestionNavController animated:YES];
    [self presentViewController:autoSugeestionNavController animated:YES completion:nil];
}


-(IBAction)openHereMapsApp:(UIButton *)sender{
    [AppUtilites openHereMapsApp];
}

-(IBAction) searchRide:(UIButton *)sender{
    userSearchData = [[UserSearchData alloc] initWithSourceLocality:sourceLocalityInfo andDestLocality:destinationLocalityInfo andRideStartDate:rideStartDate andRideDateValue:rideDateValue andRideTimeValue:rideTimeValue andMerchantId:nil];
    [self showGuestRideController];
}

-(IBAction) offerRide:(UIButton *)sender{
    UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_ERROR, nil) WithMessage:NSLocalizedString(VC_GUESTHOME_OFFERRIDEVAL, nil)];
    [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK,nil) WithAction:^(void *action){
    }];
    [alert showInViewController:self];
}

-(void) updateUserCurrentLocation{
    [self.messageHandler showTransparentBlockingLoadViewWithHandler:^{
        [CurrentLocationTracker sharedInstance].currentLocationTrackerDelegate = self;
        [[CurrentLocationTracker sharedInstance] updateCurrentLocation];
    }];
}

#pragma mark - EasyTipView Delegate
//-(void)easyTipViewDidDismiss:(EasyTipView *)tipView{
    
//}


#pragma mark - Menu Delegate
-(void) selectedMenuOption:(enum GuestMenuOptions)option;{
//    if (option == GUESTPROFILE) {
//        [(AppDelegate *)[UIApplication sharedApplication].delegate showLoginController];
//        [userDataManager setShowDeepLinkController:NO];
//    }
//    else if(option == GUESTABOUT) [self presentViewController:[AboutController createAboutNavController] animated:YES completion:nil];
}

/*-(void)selectedLocality:(LocalityInfo *)location{
    if(localityPickMode == PickingSourceLocality){
        [self updateSourceLocalityInfo:location];
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [_mapContainerView.mapView moveToLocation:location.latLng];
        });
    } else{
        [self updateDestinationLocalityInfo:location];
    }
}*/

#pragma mark - Locality Delegate

-(void)selectedLocality:(LocalityInfo *)sourceLocation withDestnationLocation:(LocalityInfo *)destiLocation{
    if(sourceLocation == nil){
        sourceLocation = sourceLocalityInfo;
    }
    [self updateSourceLocalityInfo:sourceLocation];
    [self updateDestinationLocalityInfo:destiLocation];
    double delayInSeconds = 0.3;
    if(sourceLocation != nil){
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [_mapContainerView.mapView moveToLocation:sourceLocation.latLng];
        });
    }
}

#pragma mark - DateTime Picker Delegate
-(void) selectedDate:(NSDate *)selectedDate{
    selectedDateFromPicker = selectedDate;
    
    rideStartDate = [dateTimeFormatter1 stringFromDate:selectedDate];
    rideDateValue = [dateFormatter1 stringFromDate:selectedDate];
    self.rideDateText.text = rideDateValue;
    rideTimeValue = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:selectedDate];
    self.rideTimeText.text = rideTimeValue;
    
    self.lblDate.text = rideDateValue;
    self.lblTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:selectedDate];
    
}


- (void)checkToShow_FindOrShareRideView {
    if ((_btnFirstTimeShowDateTimePicker.isHidden == false) && [_txtDestination.text isEqual:@""]) {
        [self.viewTravelPref setHidden:FALSE];
    } else {
        [self.viewTravelPref setHidden:TRUE];
        self.viewFindOrShareRide.frame = CGRectMake(self.txtDestination.frame.origin.x, self.viewTravelPref.frame.origin.y, AppDelegate.SCREEN_WIDTH, self.viewFindOrShareRide.frame.size.height);
        [self.viewDestnTimeDateTravelPref addSubview:self.viewFindOrShareRide];
    }
}

-(void) setCurrentLocation:(LocalityInfo *)currentLocation{
    [self.messageHandler dismissTransparentBlockingLoadViewWithHandler:^{
        [self updateSourceLocalityInfo:currentLocation];
        if(isInitialLocationFetch){
            isGlobalLocale = [currentLocale setGlobalLocaleFromCountry:currentLocation && currentLocation.country ? currentLocation.country : nil];
           //if(isGlobalLocale) _heremapsButton.hidden = NO;
            userSearchData = [userDataManager getUserSearchData]; //If guest user came by searching
            [_mapContainerView createMapView];
            MapView *mapView = _mapContainerView.mapView;
            mapView.sourceCoordinate = currentLocation.latLng;
            mapView.mapPadding = UIEdgeInsetsMake(CGRectGetHeight(self.destinationInfoView.frame), 0, CGRectGetHeight(self.destinationInfoView.frame), 0);
            [mapView initialiseMapWithSourceCoordinate];
            mapView.resolveMapMoveGesture = YES;
            mapView.mapViewDelegate = self;
            [self showCoachMarks];
        } else{
            [self updateSourceLocalityInfo:currentLocation];
            MapView *mapView = _mapContainerView.mapView;
            [mapView moveToLocation:currentLocation.latLng];
        }
    }];
    
}


-(void) unableToFetchCurrentLocation:(NSError *)error{
    [self.messageHandler dismissTransparentBlockingLoadViewWithHandler:^{
        if(isInitialLocationFetch){
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORTITLE, nil)  WithMessage:error.userInfo[@"message"]];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY, nil) WithAction:^(void *action){
                 [self fetchCurrentLocation];
            }];
            [alert showInViewController:self];
        } else{
            [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
        }
    }];
}

-(void)completeInitialLocationFetch{
    isInitialLocationFetch = false;
    [userDataManager setShowDeepLinkController:YES];
    if([userDataManager isDeepLinkDataPresent]){
        [userDataManager handleDeepLinkParams];
    } else if(userSearchData){
        [userDataManager resetUserSearchData];
        [self showGuestRideController];
    }
}

-(void)showGuestRideController{
    if([self validateSearchInfo]){
       // [self showDateAndTimePicker];
        UINavigationController *guestRideNavController = [GuestUserRideController createGuestRideNavController];
        GuestUserRideController *guestRideController =  (GuestUserRideController *)[guestRideNavController topViewController];
        guestRideController.userSearchData = userSearchData;
        [self presentViewController:guestRideNavController animated:NO completion:nil];
    }
}

-(void)changedMapLocation:(LocalityInfo *)newLocation{
    [self updateSourceLocalityInfo:newLocation];
}

-(void) updateSourceLocalityInfo:(LocalityInfo *)location{
    sourceLocalityInfo = location;
    self.sourceLocality.text = location.address;
}

-(void) updateDestinationLocalityInfo:(LocalityInfo *)location{
    destinationLocalityInfo = location;
    self.destinationLocality.text = location.address;
    
    self.txtDestination.text = location.address;
}


-(BOOL) validateSearchInfo{
    if(sourceLocalityInfo == nil){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_GUESTHOME_SOURCEVAL, nil)];
        return false;
    }
    if(destinationLocalityInfo == nil){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_GUESTHOME_DESTVAL, nil) ];
        return false;
    }
    if(rideStartDate == nil){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_GUESTHOME_DEPDATEVAL, nil) ];
        return false;
    }
    return true;
}

-(void)showCoachMarks{
    TutorialShownDB *obj = [[AppDelegate getAppDelegateInstance] getTutorialsDBObject];
    if([obj.isInGuestHomeShown isEqualToString:@"0"]){
   // if(!isTutorialShown){
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            NSArray *coachMarks = @[
                                    @{@"rect": [NSValue valueWithCGRect:(CGRect){[self.view convertPoint:_sourceInfoView.frame.origin toView:self.view],{_sourceInfoView.frame.size.width,_sourceInfoView.frame.size.height}}],
                                      @"caption": NSLocalizedString(VC_GUESTHOME_SOURCETUT, nil),
                                      @"shape": @"square"
                                      },
                                    @{@"rect": [NSValue valueWithCGRect:(CGRect){[_viewDestnTimeDateTravelPref convertPoint:_viewDestnTimeDate.frame.origin toView:self.view],{_viewDestnTimeDate.frame.size.width,_viewDestnTimeDate.frame.size.height}}],
                                      @"caption": NSLocalizedString(VC_GUESTHOME_DESTTUT, nil),
                                      @"shape": @"square"
                                      },
                                    @{@"rect": [NSValue valueWithCGRect:(CGRect){[_viewDestnTimeDate convertPoint:_btnFirstTimeShowDateTimePicker.frame.origin toView:self.view],{_btnFirstTimeShowDateTimePicker.frame.size.width,_btnFirstTimeShowDateTimePicker.frame.size.height}}],
                                      @"caption": NSLocalizedString(VC_GUESTHOME_DEPTTIMETUT, nil),
                                      @"shape": @"square"
                                      },
                                    @{@"rect": [NSValue valueWithCGRect:(CGRect){[_viewTravelPref convertPoint:_btnTravelPreferences.frame.origin toView:self.view],{_btnTravelPreferences.frame.size.width,_btnTravelPreferences.frame.size.height}}],
                                      @"caption": NSLocalizedString(VC_GUESTHOME_TRAVELPREFERENCES, nil),
                                      @"shape": @"square"
                                      },
//                                    @{@"rect": [NSValue valueWithCGRect:(CGRect){[_destinationInfoView convertPoint:_offerButton.frame.origin toView:self.view],{_offerButton.frame.size.width,_offerButton.frame.size.height}}],
//                                      @"caption":NSLocalizedString(VC_GUESTHOME_OFFERBUTTONTUT, nil),
//                                      @"shape": @"square"
//                                      },
                                    ];
            WSCoachMarksView *coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
            coachMarksView.animationDuration = 0.3f;
            coachMarksView.enableContinueLabel = YES;
            coachMarksView.enableSkipButton = NO;
            coachMarksView.maskColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
            coachMarksView.delegate = self;
            [self.navigationController.view addSubview:coachMarksView];
            [coachMarksView start];
            obj.isInGuestHomeShown = @"1";
            [[AppDelegate getAppDelegateInstance] updateTutorialDBObject:obj];
           // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"homeTutorialShown"];
        });
    } else{
        [self completeInitialLocationFetch];
    }
}

- (void)coachMarksViewDidCleanup:(WSCoachMarksView*)coachMarksView{
    [self completeInitialLocationFetch];
}

-(void)moveToLocationSearchFromLocationsServicesDeniedScreen{
    UINavigationController *autoSugeestionNavController = [LocalityAutoSuggestionController createLocalityAutoSuggestNavController];
    LocalityAutoSuggestionController *autoSuggestionController = (LocalityAutoSuggestionController *)[autoSugeestionNavController topViewController];
    autoSuggestionController.isForSourceAddress = YES;
    autoSuggestionController.sourceLocality = nil;
    autoSuggestionController.destinationLocality = nil;
    NSString *title = NSLocalizedString(MS_SERACHRIDE_BTN_SEARCH, nil);
    autoSuggestionController.title =  [title capitalizedString];
    autoSuggestionController.selectionDelegate = self;
    autoSuggestionController.showAutosuggestOnly = NO;
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:@"isFromManuaalySearch"];
    [prefs synchronize];
    [self presentViewController:autoSugeestionNavController animated:NO completion:nil];
}
-(void)methodForLocationServicesAccessDenied{
    [self checkLocationServicesEnabledOrNOt];
}


-(void)addDropShadowForView:(UIView *)viewShadow{
    viewShadow.backgroundColor = [UIColor whiteColor];
    viewShadow.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = CGSizeMake(-1, 0);
    viewShadow.layer.shadowRadius = 1;
    viewShadow.layer.masksToBounds = NO;
}

@end
