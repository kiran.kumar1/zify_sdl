//
//  RazorpayPaymentController.h
//  zify
//
//  Created by Anurag S Rathor on 16/11/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Razorpay/Razorpay.h>
@protocol RazorpayPaymentDelegate <NSObject>
-(void) razorpayPaymentSuccuessWithPaymentId:(NSString *)paymentId;
-(void) razorpayPaymentFailedWithCode:(int)code andMessage:(NSString *)message; 
@end


@interface RazorpayPaymentController : NSObject<RazorpayPaymentCompletionProtocol>
@property (nonatomic, weak) id<RazorpayPaymentDelegate> paymentDelegate;
-(void)initiatePaymentWithAmount:(NSString *)amount;
@end
