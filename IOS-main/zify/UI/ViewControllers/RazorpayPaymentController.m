//
//  RazorpayPaymentController.m
//  zify
//
//  Created by Anurag S Rathor on 16/11/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "RazorpayPaymentController.h"
#import "UserProfile.h"

@implementation RazorpayPaymentController{
    Razorpay *razorpay;
    UserProfile *userProfile;
}

-(id)init{
     self = [super init];
     razorpay = [Razorpay initWithKey:@"rzp_live_lOGxfoAIFD5FPf" andDelegate:self];
     userProfile = [UserProfile getCurrentUser];
     return self;
}

- (void)onPaymentError:(int)code description:(nonnull NSString *)str {
    NSLog(@"%@", str);
    [self.paymentDelegate razorpayPaymentFailedWithCode:code andMessage:str];
}

- (void)onPaymentSuccess:(nonnull NSString*)payment_id  {
    NSLog(@"%@", payment_id);
    [self.paymentDelegate razorpayPaymentSuccuessWithPaymentId:payment_id];
}

-(void)initiatePaymentWithAmount:(NSString *)amount{
    
  //  NSString *imagePathName =  [[NSBundle mainBundle] pathForResource:@"logoImg_rajor_Pay" ofType:@"png"];
   // NSURL *urlStr = [NSURL URLWithString:imagePathName];

    NSDictionary *options = @{
                              @"amount": [NSString stringWithFormat:@"%d",(int)(amount.doubleValue * 100)],
                              @"currency": @"INR",
                              @"image":@"https://s3.ap-south-1.amazonaws.com/zify-logo/logo/Android_app_Logo.png",
                              @"prefill" : @{
                                      @"email":userProfile.emailId,
                                      @"contact":userProfile.mobile
                                      },
                              @"theme": @{
                                      @"color": @"#2662FF"
                                      }
                              };
    [razorpay open:options];
}
@end
