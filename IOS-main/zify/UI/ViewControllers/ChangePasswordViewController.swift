//
//  ChangePasswordViewController.swift
//  zify
//
//  Created by Anurag on 10/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class ChangePasswordViewController: ScrollableContentViewController {
    
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var otp: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var lblEmailText: UILabel!
    @IBOutlet weak var lblMobileText: UILabel!
    @IBOutlet weak var btnSendOTP: UIButton!
    @IBOutlet weak var lblAlreadyReceivedOTP: UILabel!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var lblAND: UILabel!
    @IBOutlet weak var viewOtp: UIView!
    @IBOutlet weak var constHeight_otpView: NSLayoutConstraint!
    
    
    var userProfile: UserProfile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLeftView(otp)
        addLeftView(password)
        addLeftView(confirmPassword)
        
        if UIScreen.main.sizeType == .iPhone5 {
            email.fontSize = 13
            mobile.fontSize = 13
            lblEmailText.fontSize = 13
            lblMobileText.fontSize = 13
            btnSendOTP.titleLabel?.fontSize = 13
            lblAlreadyReceivedOTP.fontSize = 12
            btnChangePassword.titleLabel?.fontSize = 16
            lblAND.fontSize = 16
        } else if UIScreen.main.sizeType == .iPhone6 {
            email.fontSize = 14
            mobile.fontSize = 14
            lblEmailText.fontSize = 14
            lblMobileText.fontSize = 14
            btnSendOTP.titleLabel?.fontSize = 14
            lblAlreadyReceivedOTP.fontSize = 12
            btnChangePassword.titleLabel?.fontSize = 16
            lblAND.fontSize = 16
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            email.fontSize = 15
            mobile.fontSize = 15
            lblEmailText.fontSize = 15
            lblMobileText.fontSize = 15
            btnSendOTP.titleLabel?.fontSize = 15
            lblAlreadyReceivedOTP.fontSize = 13
            btnChangePassword.titleLabel?.fontSize = 17
            lblAND.fontSize = 17
        }
        populateUserDetails()
        showShadow(view: viewOtp, color: .lightGray, opacity: 1, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
        
        isEnableChangePwdBtnPwdTextFields(isenable: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isEnableChangePwdBtnPwdTextFields(isenable : Bool) {
        if isenable == true {
            btnChangePassword.alpha = 1
            otp.alpha = 1
            password.alpha = 1
            confirmPassword.alpha = 1
        } else {
            btnChangePassword.alpha = 0.2
            otp.alpha = 0.2
            password.alpha = 0.2
            confirmPassword.alpha = 0.2
        }
        btnChangePassword.isEnabled = isenable
        otp.isEnabled = isenable
        password.isEnabled = isenable
        confirmPassword.isEnabled = isenable
    }
    
    // MARK:- Actions
    
    @IBAction func dismiss(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func sendOTP(_ sender: Any) {
        super.dismissKeyboard()
        let profile = UserProfile.getCurrentUser()
        let changeRequest = PasswordChangeRequest(dUserEmail: profile?.emailId, andUserPassword: "", andOtp: "", andRequestType: GETFORGOTPASSWORDOTP)
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(changeRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.isEnableChangePwdBtnPwdTextFields(isenable: true)
                        let successmsg = NSLocalizedString(USS_CHANGEPASSWORD_OTP_SEND_SUCCESS, comment: "")
                        //  self.viewToAnimateTop()
                        self.messageHandler.showSuccessMessage(successmsg)
                        self.btnSendOTP.setTitle(NSLocalizedString(UOS_VERIFYMOBILE_BTN_RESENDCODE, comment: ""), for: .normal)
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    @IBAction func changePassword(_ sender: Any) {
        super.dismissKeyboard()
        if validateChangePasswordRequest() {
            let profile = UserProfile.getCurrentUser()
            let changeRequest = PasswordChangeRequest(dUserEmail: profile?.emailId, andUserPassword: password.text, andOtp: otp.text, andRequestType: SAVEFORGOTPASSWORD)
            messageHandler.showBlockingLoadView(handler: {
                ServerInterface.sharedInstance().getResponse(changeRequest, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            PushNotificationManager.sharedInstance().deregisterDeviceForPushOnLogout() //Should be called before deleting user since userId is required here.
                            UserProfile.deleteCurrentUser()
                            ChatManager.sharedInstance().disconnectChatOnLogout()
                            OnboardFlowHelper.sharedInstance().clearReminder()
                            //FacebookLogin.sharedInstance().closeAndClearSavedTokens()
                            FacebookLogin.sharedInstance().closeSessionAndClearSavedTokens()
                            UserDataManager.sharedInstance().setDeepLinkFlagsOnLogout()
                            
                            self.dismissScreen()
                            
                            (UIApplication.shared.delegate as? AppDelegate)?.showLoginController()
                            // Allow the view controller to be deallocated
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        }
                    })
                })
            })
        }
    }
    
    func viewToAnimateTop() {
        
        viewOtp.isHidden = true
        self.constHeight_otpView.constant = 0
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
        }
    }
    
    func dismissScreen() {
        var visibleController : UIViewController? = AppUtilites.applicationVisibleViewController()
        let rootViewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        if rootViewController?.presentedViewController != nil {
            rootViewController?.dismiss(animated: false)
            visibleController = UIApplication.shared.keyWindow?.rootViewController
        }
    }
    
    func validateChangePasswordRequest() -> Bool {
        if otp.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_CHANGEPASSWORD_OTPVAL, comment: ""))
            return false
        }
        if password.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_CHANGEPASSWORD_PASSWORDVAL, comment: ""))
            return false
        }
        if (password.text?.count)! < 5 || (password.text?.count)! > 25 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_PASSWARD_MIN_LENGHT_VAL, comment: ""))
            return false
        }
        if confirmPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_CHANGEPASSSWORD_CONFIRMPASSWORDVAL, comment: ""))
            return false
        }
        if !(password.text == confirmPassword.text) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_CHANGEPASSWORD_PASSWORDMATCHVAL, comment: ""))
            return false
        }
        return true
    }
    
    func populateUserDetails() {
        userProfile = UserProfile.getCurrentUser()
        mobile.text = "\(userProfile?.isdCode ?? "") \(userProfile?.mobile ?? "")"
        email.text = userProfile?.emailId
    }
    
    func addLeftView(_ textField: UITextField?) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        textField?.leftView = view
        textField?.leftViewMode = .always
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == password {
            confirmPassword.becomeFirstResponder()
        }
        return true
    }
    
    
}
