//
//  ForgotPasswordController.m
//  zify
//
//  Created by Anurag S Rathor on 12/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ForgotPasswordController.h"
#import "ServerInterface.h"
#import "PasswordChangeRequest.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"

@interface ForgotPasswordController ()

@end

@implementation ForgotPasswordController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}
-(void)viewDidLoad{
    _otpView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _otpView.layer.borderWidth = 0.5;
    //_otp.layer.cornerRadius = 5;
    _otp.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _otp.layer.borderWidth = 0.5;
   // _password.layer.cornerRadius = 5;
    _password.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _password.layer.borderWidth = 0.5;
   // _confirmPassword.layer.cornerRadius = 5;
    _confirmPassword.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _confirmPassword.layer.borderWidth = 0.5;
    if([AppDelegate getAppDelegateInstance].emailStr.length > 0){
        _email.text = [AppDelegate getAppDelegateInstance].emailStr;
    }
    [self addLeftViewForTextField:_otp];
    [self addLeftViewForTextField:_password];
    [self addLeftViewForTextField:_confirmPassword];
}

-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)sendOTP:(id)sender{
    [super dismissKeyboard];
    if([self validateSendOTPRequest]){
        PasswordChangeRequest *changeRequest = [[PasswordChangeRequest alloc] initWithdUserEmail:self.email.text andUserPassword:@"" andOtp:@"" andRequestType:GETFORGOTPASSWORDOTP];
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance]getResponse:changeRequest withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        [self.messageHandler showSuccessMessage:response.message];
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
    }
}

-(IBAction) restPassword:(id)sender {
    [super dismissKeyboard];
    if([self validateResetPasswordRequest]) {
        PasswordChangeRequest *changeRequest = [[PasswordChangeRequest alloc] initWithdUserEmail:self.email.text andUserPassword:self.password.text andOtp:self.otp.text andRequestType:SAVEFORGOTPASSWORD];
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance]getResponse:changeRequest withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        [self.messageHandler showSuccessMessage:response.message];
                        [self dismissViewControllerAnimated:TRUE completion:nil];
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
    }
}

-(BOOL) validateSendOTPRequest {
    if([@"" isEqualToString: self.email.text]) {
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_FORGOTPASSWORD_EMAILVAL, nil)];
        return false;
    }
    if(![self validateEmail:self.email.text]) {
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_FORGOTPASSWORD_EMAILVALIDVAL, nil)];
        return false;
    }
    return true;
}

-(BOOL) validateResetPasswordRequest{
    if([self.otp.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_FORGOTPASSWORD_OTPVAL, nil)];
        return false;
    }
    if([self.password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_FORGOTPASSWORD_PASSWORDVAL, nil)];
        return false;
    }
    if(self.password.text.length < 5 || self.password.text.length >25){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_SIGNUP_PASSWARD_MIN_LENGHT_VAL, nil)];
        return false;
    }
    if([self.confirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_FORGOTPASSSWORD_CONFIRMPASSWORDVAL, nil)];
        return false;
    }
    if(![self.password.text isEqualToString:self.confirmPassword.text]){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_FORGOTPASSWORD_PASSWORDMATCHVAL, nil)];
        return false;
    }
    return true;
}

-(BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [predicate evaluateWithObject:email];
}
-(void)addLeftViewForTextField:(TextFieldWithDone *)textField{
    textField.clipsToBounds = YES;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.leftView = view;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if(textField == _password){
        [_confirmPassword becomeFirstResponder];
    }
    return YES;
}
@end
