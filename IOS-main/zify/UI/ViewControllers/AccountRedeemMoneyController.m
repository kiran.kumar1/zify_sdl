//
//  AccountRedeemMoneyController.m
//  zify
//
//  Created by Anurag S Rathor on 28/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "AccountRedeemMoneyController.h"
//#import "BankAccountDetailsCell.h"
#import "ServerInterface.h"
#import "BankAccountRequest.h"
#import "BankRedemptionDetails.h"
#import "CurrentLocale.h"
#import "UserProfile.h"
#import "UniversalAlert.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "zify-Swift.h"

@implementation AccountRedeemMoneyController{
    BankRedemptionDetails *redemptionDetails;
    NSArray *bankAccounts;
    NSInteger selectedIndex;
    UserProfile *currentUser;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (IS_IPHONE_5) {
        _availableBalance.fontSize = 14;
        _lblZifyWillProcessBasedOnText.fontSize = 12;
        _lblWithdrawPolicyDescribes.fontSize = 12;
        _btnHere.titleLabel.fontSize = 12;
        _btnWithDraw.titleLabel.fontSize = 16;
        _lblUnableToInitiate.fontSize = 15;
    } else if (IS_IPHONE_6) {
        _availableBalance.fontSize = 14;
        _lblZifyWillProcessBasedOnText.fontSize = 12;
        _lblWithdrawPolicyDescribes.fontSize = 12;
        _btnHere.titleLabel.fontSize = 12;
        _btnWithDraw.titleLabel.fontSize = 16;
        _lblUnableToInitiate.fontSize = 16;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _availableBalance.fontSize = 15;
        _lblZifyWillProcessBasedOnText.fontSize = 13;
        _lblWithdrawPolicyDescribes.fontSize = 13;
        _btnHere.titleLabel.fontSize = 13;
        _btnWithDraw.titleLabel.fontSize = 18;
        _lblUnableToInitiate.fontSize = 17;
    }
    self.withdrawImageView.image = [self getNewImageWithOldImage:self.withdrawImageView.image withNewColor:[UIColor whiteColor]];
    self.redeemAmountView.hidden = true;
    self.noAccountsView.hidden = true;
    self.unverifiedUserView.hidden = true;
    self.amountToRedeem.placeholder = [NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(AS_WITHDRAW_LBL_ENTERAMOUNT, nil),[[CurrentLocale sharedInstance] getCurrencySymbol]];
    [self addBottomborder:self.amountToRedeem];
    currentUser = [UserProfile getCurrentUser];
    _tableView.estimatedRowHeight = 70.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated {
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   // currentUser.userDocuments.isIdCardDocUploaded = 0;
    if(![currentUser.userDocuments.isIdCardDocUploaded intValue]){
        self.unverifiedUserView.hidden = false;
        self.lblIdcardNotUploadValue.text = NSLocalizedString(VC_USERPROFILE_ID_NA, nil);
        self.lblIdcardNotUploadValue.backgroundColor = [UIColor whiteColor];
    }else if(![@"VERIFIED" isEqualToString:currentUser.userStatus]){
        self.unverifiedUserView.hidden = false;
        self.lblIdcardNotUploadValue.hidden = true;
    } else{
        selectedIndex = 0;
        self.unverifiedUserView.hidden = true;
        self.redeemAmountView.hidden = true;
        self.noAccountsView.hidden = true;
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance]getResponse:[[BankAccountRequest alloc] initWithRequestType:REDEMPTIONDETAILS] withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        redemptionDetails = (BankRedemptionDetails *)response.responseObject;
                        bankAccounts = [NSMutableArray arrayWithArray:redemptionDetails.bankDetails];
                        [self handleRedemptionResponse];
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
    }
}


-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(bankAccounts.count == 0) return 0;
    return [bankAccounts count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //check crash
    BankAccountDetails *bankAccountDetails = [bankAccounts objectAtIndex:indexPath.section];
    BankAccountDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BankAccountDetailsCell"];
    cell.bankName.text = bankAccountDetails.bankName;
    cell.accountNumber.text = bankAccountDetails.accountNumber;
    if(selectedIndex == indexPath.section) {
        [cell.bankSelectButton setSelected:YES];
    } else {
        [cell.bankSelectButton setSelected:NO];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(selectedIndex != indexPath.section) {
        NSMutableArray *indexPathArray = [[NSMutableArray alloc] init];
        [indexPathArray addObject:indexPath];
        NSIndexPath *previousPath = [NSIndexPath indexPathForRow:0 inSection:selectedIndex];
        [indexPathArray addObject:previousPath];
        selectedIndex = indexPath.section;
        [self.tableView reloadRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

#pragma mark - Actions

- (IBAction)btnTappedSupport:(id)sender {
    [[Freshchat sharedInstance]showConversations:self];
}

-(IBAction) dismiss:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)withDrawAmount:(UIButton *)sender{
    if([self validateWithdrawAmount]){
        BankAccountDetails *bankDetails = [bankAccounts objectAtIndex:selectedIndex];
        NSDictionary *eventDict = [NSDictionary dictionaryWithObjectsAndKeys: [CurrentLocale sharedInstance].getCurrencySymbol,@"currency", _amountToRedeem.text, @"amount", nil];
        [MoEngageEventsClass callAccountWithdrawRequestInitiatedEventWithDataDict:eventDict];
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance] getResponse:[[BankAccountRequest alloc] initWithRequestType:REGISTERREDEEM andBankAccountDetaiId:bankDetails.bankDetailId andRedeemAmount:_amountToRedeem.text] withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        
                        //send Email service call
                        
                        [EmailClass sendMailWithStrLRCoins:@"" iRequestType:1 strWithdrawlAmount:_amountToRedeem.text];
                        
                        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_REDEEMMONEY_REQUESTACCEDPTEDTTITLE, nil) WithMessage:NSLocalizedString(VC_REDEEMMONEY_REQUESTACCEPTEDINFO, nil)];
                        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK,nil) WithAction:^(void *action){
                            [self performSelector:@selector(dismiss:) withObject:self afterDelay:0.1];
                        }];
                        [alert showInViewController:self];
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
    }
}

-(IBAction)showAddBankView:(UIButton *)sender{
    [self.navigationController pushViewController:[AddBankAccountController createAddBankAccountController] animated:YES];
}

-(IBAction)showWithDrawPolicy:(UIButton *)sender{
    UINavigationController *genericWebNavController = [GenericWebController createGenericWebNavController];
    GenericWebController *webController = (GenericWebController *)genericWebNavController. topViewController;
    webController.webViewURL = NSLocalizedString(CMON_GENERIC_TERMSURL,nil);
    webController.title = NSLocalizedString(CMON_GENERIC_TNC, nil);
    [self presentViewController:genericWebNavController animated:YES completion:nil];
}

-(void)handleRedemptionResponse{
    if([bankAccounts count] == 0){
        self.redeemAmountView.hidden = true;
        self.noAccountsView.hidden = false;
    } else{
        self.redeemAmountView.hidden = false;
        self.noAccountsView.hidden = true;
        self.availableBalance.text = [NSString stringWithFormat:@"%@ %@%d",NSLocalizedString(AS_WITHDRAW_LBL_AVAILABLEBALANCE, nil),[[CurrentLocale sharedInstance] getCurrencySymbol],redemptionDetails.redeemableAmount.intValue];
       // self.amountToRedeem.text = [NSString stringWithFormat:@"%d",redemptionDetails.redeemableAmount.intValue];
        [self.tableView reloadData];
    }
}

-(BOOL)validateWithdrawAmount{
    if(_amountToRedeem.text.doubleValue  == 0 || _amountToRedeem.text.doubleValue > redemptionDetails.redeemableAmount.doubleValue){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_REDEEEMMONEY_AMOUNTVAL, nil)];
        return false;
    }
    //rajesh
    if(_amountToRedeem.text.doubleValue < 100) {
        [self.messageHandler showErrorMessage:NSLocalizedString(wallet_withdrawal_min_amount_check, nil)];
        return false;
    }
    return true;
}

-(void)addBottomborder:(UIView *)view{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5;
    border.borderColor = [UIColor colorWithRed:214.0/255.0 green:214.0/255.0 blue:214.0/255.0 alpha:1.0].CGColor;
    border.frame = CGRectMake(0, view.frame.size.height - borderWidth, view.frameWidth, 1);
    border.borderWidth = borderWidth;
    [view.layer addSublayer:border];
    view.layer.masksToBounds = YES;
}
-(UIImage *)getNewImageWithOldImage:(UIImage *)oldImage withNewColor:(UIColor *)newColor{
    UIColor *color = newColor;
    UIImage *image = oldImage;// Image to mask with
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, CGRectMake(0, 0, image.size.width, image.size.height), [image CGImage]);
    CGContextFillRect(context, CGRectMake(0, 0, image.size.width, image.size.height));
    UIImage*  coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return coloredImg;
}

@end
