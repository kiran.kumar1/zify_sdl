//
//  RideRequestsController.m
//  zify
//
//  Created by Anurag S Rathor on 26/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "RideRequestsController.h"
#import "RideRequestCell.h"
#import "ServerInterface.h"
#import "GenericRequest.h"
#import "UserNotification.h"
#import "UserNotificationDetail.h"
#import "RiderProfile.h"
#import "RideDriveActionRequest.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChatManager.h"
#import "UserProfileDetailView.h"
#import "LocalNotificationManager.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "zify-Swift.h"

@interface RideRequestsController ()

@end

@implementation RideRequestsController{
    NSMutableArray *rideRequests;
    NSDateFormatter *dateFormatter;
   // NSDateFormatter *timeFormatter;
    NSDateFormatter *dateTimeFormatter;
    UIImage *profileImagePlaceHolder;
    BOOL isViewInitialised;
    BOOL isUserChatConnected;
}
@synthesize isViewInitialised;

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    self.navigationItem.title = NSLocalizedString(CS_RIDEREQUESTS_NAV_TITLE,nil) ;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isFromRideRequest = [prefs boolForKey:@"isFromRideRequestAccept"];
    if(isFromRideRequest){
        [prefs setBool:NO forKey:@"isFromRideRequestAccept"];
        [prefs synchronize];
        UserProfile *userProfile = [UserProfile getCurrentUser];
        UserDocuments *userDocuments = userProfile.userDocuments;
        if([userDocuments.isIdCardDocUploaded intValue]){
            [self callApiForRideRequestAccept];
        }
    }
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    isViewInitialised = false;
    rideRequests = nil;
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter setLocale:locale];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    
//    timeFormatter = [[NSDateFormatter alloc] init];
//    [timeFormatter setDateFormat:@"HH:mm"];
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    // Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self callServiceAndLoadData];
}
-(void)callServiceAndLoadData{
    if(!isViewInitialised){
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance]getResponse:[[GenericRequest alloc] initWithRequestType:USERNOTIFICATIONSREQUEST] withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        rideRequests = [NSMutableArray arrayWithArray:(NSArray *)response.responseObject];
                        [self handleNotificationsResponse];
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
        isViewInitialised = true;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [rideRequests count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RideRequestCell *rideRequestCell = [tableView dequeueReusableCellWithIdentifier:@"rideRequestCell"];
    UserNotification *userNotification = [rideRequests objectAtIndex:indexPath.section];
    UserNotificationDetail *userNotificationDetail;
    userNotificationDetail = userNotification.rideRequest;
    RiderProfile *riderProfile = userNotificationDetail.riderProfile;
    rideRequestCell.userImage.hidden = YES;
    [rideRequestCell.userImage sd_setImageWithURL:[NSURL URLWithString:riderProfile.profileImageURL] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        rideRequestCell.userImage.hidden = NO;
    }];
    if(![@"VERIFIED" isEqualToString:riderProfile.userStatus]){
        rideRequestCell.userVerifiedImage.hidden = NO;
    } else{
        rideRequestCell.userVerifiedImage.hidden = YES;
    }
    if(isUserChatConnected && riderProfile.chatProfile){
        rideRequestCell.chatButton.hidden = NO;
    } else{
        rideRequestCell.chatButton.hidden = YES;
    }
    rideRequestCell.name.text = [NSString stringWithFormat:@"%@ %@",riderProfile.firstName,riderProfile.lastName];
    rideRequestCell.maleRatingView.rating = riderProfile.userRatingMale.floatValue;
    rideRequestCell.maleRatingView.editable = NO;
    rideRequestCell.femaleRatingView.rating = riderProfile.userRatingFemale.floatValue;
    rideRequestCell.femaleRatingView.editable = NO;
    rideRequestCell.source.text = userNotificationDetail.srcAddress;
    rideRequestCell.destination.text = userNotificationDetail.destAddress;
    NSDate *tripDepartureTime = [dateTimeFormatter dateFromString:userNotificationDetail.tripTime];
    rideRequestCell.tripDate.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(VC_RIDEREQUEST_DATETXT,nil), [dateFormatter stringFromDate:tripDepartureTime]];
    rideRequestCell.tripTime.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(VC_RIDEREQUEST_TIMETXT,nil), [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:tripDepartureTime]];
    
    if (IS_IPHONE_5) {
        rideRequestCell.name.fontSize = 14;
        rideRequestCell.tripDate.fontSize = 12;
        rideRequestCell.tripTime.fontSize = 12;
        rideRequestCell.source.fontSize = 12;
        rideRequestCell.destination.fontSize = 12;
        rideRequestCell.lblSourceText.fontSize = 13;
        rideRequestCell.lblDestinationText.fontSize = 13;
        rideRequestCell.acceptButton.titleLabel.fontSize = 15;
        rideRequestCell.declineButton.titleLabel.fontSize = 15;
    } else if (IS_IPHONE_6 || IS_IPHONE_X) {
        rideRequestCell.name.fontSize = 14;
        rideRequestCell.tripDate.fontSize = 12;
        rideRequestCell.tripTime.fontSize = 12;
        rideRequestCell.source.fontSize = 12;
        rideRequestCell.destination.fontSize = 12;
        rideRequestCell.lblSourceText.fontSize = 13;
        rideRequestCell.lblDestinationText.fontSize = 13;
        rideRequestCell.acceptButton.titleLabel.fontSize = 15;
        rideRequestCell.declineButton.titleLabel.fontSize = 15;
    } else if (IS_IPHONE_6_PLUS) {
        rideRequestCell.name.fontSize = 15;
        rideRequestCell.tripDate.fontSize = 13;
        rideRequestCell.tripTime.fontSize = 13;
        rideRequestCell.source.fontSize = 13;
        rideRequestCell.destination.fontSize = 13;
        rideRequestCell.lblSourceText.fontSize = 14;
        rideRequestCell.lblDestinationText.fontSize = 14;
        rideRequestCell.acceptButton.titleLabel.fontSize = 16;
        rideRequestCell.declineButton.titleLabel.fontSize = 16;
    }
    
    return rideRequestCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 205.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0) return 0.0;
    return 10.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

+(UINavigationController *)createRideRequestsNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Commons" bundle:[NSBundle mainBundle]];
    UINavigationController *rideRequestsNavController = [storyBoard instantiateViewControllerWithIdentifier:@"rideRequestsNavController"];
    return rideRequestsNavController;
}


-(IBAction) showUserProfile:(UIButton *)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    UserNotification *userNotification = (UserNotification *)[rideRequests objectAtIndex:indexPath.section];
    RiderProfile *riderProfile = userNotification.rideRequest.riderProfile;
    [UserProfileDetailView showUserProfileOnView:self.navigationController.view WithRiderProfile:riderProfile];
}

-(IBAction) showChat:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    UserNotificationDetail *userNotification = [(UserNotification *)[rideRequests objectAtIndex:indexPath.section] rideRequest];
    RiderProfile *riderProfile = userNotification.riderProfile;
    UserContactDetail *detail = [[UserContactDetail alloc] initWithChatIdentifier:riderProfile.chatProfile.chatUserId andCallIdentifier:nil andFirstName:riderProfile.firstName andLastName:riderProfile.lastName andImageUrl:riderProfile.profileImageURL];
    UINavigationController *chatNavController = [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    chatUserListContoller.receivingUser = detail;
    [self presentViewController:chatNavController animated:YES completion:nil];
}

-(IBAction) declineRide:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    UserNotificationDetail *userNotification = [(UserNotification *)[rideRequests objectAtIndex:indexPath.section] rideRequest];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:DECLINERIDE andDriveId:userNotification.driveId andRideId:userNotification.rideId] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    NSDictionary *dataDictToMoengage = [[NSDictionary alloc] initWithObjectsAndKeys:userNotification.riderProfile.firstName,@"Rider_Firstname",userNotification.riderProfile.lastName,@"Rider_Lastname",[userNotification.riderProfile.riderId stringValue],@"ride_Rider_Id",userNotification.srcAddress,@"source_address", userNotification.destAddress ,@"destination_address",userNotification.tripTime,@"trip_Time", nil];
                    [MoEngageEventsClass callRideRequestDeclineEventWithDataDict:dataDictToMoengage];
                    
                    [rideRequests removeObjectAtIndex:indexPath.section];
                    [self handleNotificationsResponse];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(IBAction) confirmRide:(UIButton *)sender{
    
    self.selectedIndexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    self.userNotification = [(UserNotification *)[rideRequests objectAtIndex:self.selectedIndexPath.section] rideRequest];
  
    //Modified on  6-Aug-19
//    UserProfile *userProfile = [UserProfile getCurrentUser];
//    UserDocuments *userDocuments = userProfile.userDocuments;
//    if(![userDocuments.isIdCardDocUploaded intValue]){
//        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//        [prefs setBool:YES forKey:@"isFromRideRequestAccept"];
//        [prefs synchronize];
//        [[AppDelegate getAppDelegateInstance] showViewForIdCardUpload];
//        return;
//    }
    
    [self callApiForRideRequestAccept];
   
}

-(void)callApiForRideRequestAccept{
    if(self.userNotification == nil || self.selectedIndexPath == nil){
        return;
    }
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:CONFIRMRIDE andRideId:self.userNotification.rideId andDriveId:self.userNotification.driveId andSeatsNum:self.userNotification.numOfSeats] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    
                    NSDictionary *dataDictToMoengage = [[NSDictionary alloc] initWithObjectsAndKeys:self.userNotification.riderProfile.firstName,@"Rider_Firstname",self.userNotification.riderProfile.lastName,@"Rider_Lastname",[self.userNotification.riderProfile.riderId stringValue],@"ride_Rider_Id",self.userNotification.srcAddress,@"source_address", self.userNotification.destAddress ,@"destination_address",self.userNotification.tripTime,@"trip_Time", nil];
                    [MoEngageEventsClass callRideRequestAcceptEventWithDataDict:dataDictToMoengage];
                    
                    
                    [rideRequests removeObjectAtIndex:self.selectedIndexPath.section];
                    [self handleNotificationsResponse];
                    /* if(![self isDateAlreadyCompleted:[dateTimeFormatter dateFromString:userNotification.tripTime]]){
                     [[LocalNotificationManager sharedInstance] scheduleNotificationWithIdentifier:userNotification.driveId.stringValue andType:DRIVEALARM andMesssage:NSLocalizedString(VC_RIDEREQUEST_TRIPSTARTMESSAGE, nil) andDate:userNotification.tripTime];
                     }*/
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
    
}

-(void)handleNotificationsResponse{
    [self.tableView reloadData];
    if([rideRequests count] == 0){
        self.tableView.hidden = true;
        self.noRideRequestsView.hidden = false;
    } else{
        self.tableView.hidden = false;
        self.noRideRequestsView.hidden = true;
    }
}


-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}

-(BOOL)isDateAlreadyCompleted:(NSDate *)date{
    NSInteger seconds;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components: NSCalendarUnitSecond fromDate:[NSDate date] toDate:date options:0];
    seconds = [components second];
    if(seconds < 0) {
        return true;
    }
    return false;
}


@end
