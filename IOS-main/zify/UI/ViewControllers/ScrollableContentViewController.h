//
//  ScrollableContentViewController.h
//  zify
//
//  Created by Anurag S Rathor on 10/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollableContentViewController : UIViewController<UITextFieldDelegate>
@property(nonatomic,weak) IBOutlet UIScrollView *scrollview;
@property(nonatomic,strong) NSNumber *scrollDelta;
@property(nonatomic,weak) UITextField *activeField;
-(void) dismissKeyboard;
@end
