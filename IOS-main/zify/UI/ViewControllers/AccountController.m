//
//  AccountController.m
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "AccountController.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

@interface AccountController ()

@end

@implementation AccountController{
    UserWallet *wallet;
    BOOL isViewInitialised;
    BOOL isGlobalLocale;
    BOOL isGlobalPayment;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (IS_IPHONE_5) {
        _availableCash.fontSize = 14;
        _availablePoints.fontSize = 13;
        _distanceTravelled.fontSize = 17;
        _distanceOffered.fontSize = 17;
        _totalRides.fontSize = 17;
        _totalDrives.fontSize = 17;
        _distanceTravelledText.fontSize = 13;
        _distanceOfferedText.fontSize = 13;
        _totalRidesText.fontSize = 13;
        _totalDrivesText.fontSize = 13;
        _riderLocalCosting.fontSize = 13;
        _riderIntercityCosting.fontSize = 13;
        _driverLocalEarning.fontSize = 13;
        _driverIntercityEarning.fontSize = 13;
        _distanceUnit.fontSize = 13;
        _lblFareChartText.fontSize = 15;
        _lblWithDrawText.fontSize = 15;
        _lblFareDetailText.fontSize = 16;
        _lblPassengerText.fontSize = 13;
        _lblCarOwnerText.fontSize = 13;
        _lblPaysText.fontSize = 13;
        _lblEarnsText.fontSize = 13;
        _lblLocalText.fontSize = 13;
        _lblIntercityText.fontSize = 13;
        _lblNoMinimumChargesText.fontSize = 12;
        _btnTermsAndConditionsApplyText.titleLabel.fontSize = 12;
        _btnOk.titleLabel.fontSize = 17;
    } else if (IS_IPHONE_6) {
        _availableCash.fontSize = 14;
        _availablePoints.fontSize = 13;
        _distanceTravelled.fontSize = 17;
        _distanceOffered.fontSize = 17;
        _totalRides.fontSize = 17;
        _totalDrives.fontSize = 17;
        _distanceTravelledText.fontSize = 13;
        _distanceOfferedText.fontSize = 13;
        _totalRidesText.fontSize = 13;
        _totalDrivesText.fontSize = 13;
        _riderLocalCosting.fontSize = 13;
        _riderIntercityCosting.fontSize = 13;
        _driverLocalEarning.fontSize = 13;
        _driverIntercityEarning.fontSize = 13;
        _distanceUnit.fontSize = 13;
        _lblFareChartText.fontSize = 15;
        _lblWithDrawText.fontSize = 15;
        _lblFareDetailText.fontSize = 16;
        _lblPassengerText.fontSize = 13;
        _lblCarOwnerText.fontSize = 13;
        _lblPaysText.fontSize = 13;
        _lblEarnsText.fontSize = 13;
        _lblLocalText.fontSize = 13;
        _lblIntercityText.fontSize = 13;
        _lblNoMinimumChargesText.fontSize = 12;
        _btnTermsAndConditionsApplyText.titleLabel.fontSize = 12;
        _btnOk.titleLabel.fontSize = 17;
        
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _availableCash.fontSize = 15;
        _availablePoints.fontSize = 14;
        _distanceTravelled.fontSize = 17;
        _distanceOffered.fontSize = 17;
        _totalRides.fontSize = 17;
        _totalDrives.fontSize = 17;
        _distanceTravelledText.fontSize = 13;
        _distanceOfferedText.fontSize = 13;
        _totalRidesText.fontSize = 13;
        _totalDrivesText.fontSize = 13;
        _riderLocalCosting.fontSize = 13;
        _riderIntercityCosting.fontSize = 13;
        _driverLocalEarning.fontSize = 13;
        _driverIntercityEarning.fontSize = 13;
        _distanceUnit.fontSize = 14;
        _lblFareChartText.fontSize = 16;
        _lblWithDrawText.fontSize = 16;
        _lblFareDetailText.fontSize = 17;
        _lblPassengerText.fontSize = 13;
        _lblCarOwnerText.fontSize = 13;
        _lblPaysText.fontSize = 13;
        _lblEarnsText.fontSize = 13;
        _lblLocalText.fontSize = 13;
        _lblIntercityText.fontSize = 13;
        _lblNoMinimumChargesText.fontSize = 12;
        _btnTermsAndConditionsApplyText.titleLabel.fontSize = 12;
        _btnOk.titleLabel.fontSize = 18;
    }
    
    
    self.scrollview.hidden = true;
    isViewInitialised = false;
    [super viewDidLoad];
    self.scrollview.hidden = true;
    isViewInitialised = false;
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    isGlobalLocale = [currentLocale isGlobalLocale];
    isGlobalPayment = [currentLocale isGlobalPayment];
    if(isGlobalLocale && !isGlobalPayment){
        self.availableCash.hidden = YES;
        self.availablePoints.hidden = YES;
        self.fareChartClickView.hidden = YES;
        self.withdrawClickView.hidden = YES;
        CGRect walletImgFrame = self.walletImage.frame;
        walletImgFrame.origin.y = walletImgFrame.origin.y + 30;
        self.walletImage.frame = walletImgFrame;
    } else{
        self.fareChartClickGlobalView.hidden = YES;
    }
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised){
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance] getResponse:[[GenericRequest alloc] initWithRequestType:RIDERWALLETREQUEST] withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        wallet = (UserWallet *)response.responseObject;
                        self.availableCash.text =[NSString stringWithFormat:NSLocalizedString(VC_ACCOUNT_WALLETBALANCE, @"Wallet Balance {Currency} {Amount}"),[[CurrentLocale sharedInstance] getCurrencySymbol], wallet.zifyCash.stringValue];
                        NSString *availablPtsStr = [NSString stringWithFormat:NSLocalizedString(VC_ACCOUNT_PROMOCREDITS, @"Promotional Credits : {UserPromocredits}"),wallet.availablePoints.stringValue];
                        self.availablePoints.text = availablPtsStr;
                       self.totalDrives.text = wallet.totalDrives.stringValue;
                       // self.totalDrivesText.text = wallet.totalDrivesLabel;
                        self.totalRides.text = wallet.totalRides.stringValue;
                      //  self.totalRidesText.text = wallet.totalRidesLabel;
                        self.distanceTravelled.text = [NSString stringWithFormat:@"%.02f",wallet.distanceTravelled.floatValue];
                        //self.distanceTravelledText.text = wallet.sharedDistanceLabel;
                        self.distanceOffered.text = [NSString stringWithFormat:@"%.02f",wallet.distanceOffered.floatValue];
                        //self.distanceOfferedText.text = wallet.offeredDistanceLabel;
                        self.scrollview.hidden = false;
                    } else {
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
        isViewInitialised = true;
    }
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

-(IBAction)showFareMeterView:(id)sender{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:[[GenericRequest alloc] initWithRequestType:RATECARDREQUEST] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    RateCardResponse *rateCard = (RateCardResponse *)response.responseObject;
                    NSString *currency = [[CurrentLocale sharedInstance] getCurrencySymbol];
                    self.riderLocalCosting.text = [NSString stringWithFormat:@"%@ %@",currency,rateCard.riderCostingLocal];
                    self.riderIntercityCosting.text = [NSString stringWithFormat:@"%@ %@",currency,rateCard.riderCostingIntercity];
                    self.driverLocalEarning.text = [NSString stringWithFormat:@"%@ %@",currency,rateCard.driverEarningLocal];
                    self.driverIntercityEarning.text =[NSString stringWithFormat:@"%@ %@",currency,rateCard.driverEarningIntercity];
                    //self.distanceUnit.text = rateCard.label;
                     [self.navigationController.view addSubview:self.fareMeterView];;
                    NSDictionary *viewsDictionary = @{@"fareMeterView":self.fareMeterView};
                    self.fareMeterView.translatesAutoresizingMaskIntoConstraints = NO;
                    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[fareMeterView]|" options:0 metrics:nil views:viewsDictionary];
                    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[fareMeterView]|" options:0 metrics:nil views:viewsDictionary];
                    [self.navigationController.view addConstraints:horizontalConsts];
                    [self.navigationController.view addConstraints:verticalConsts];
                  
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(IBAction)showWithDrawMoneyView:(id)sender{
    [self performSegueWithIdentifier:@"showWithdrawMoney" sender:self];
}

-(IBAction) dismissFareMeterView:(UIButton *)sender{
    [self.fareMeterView removeFromSuperview];
}

-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

+(UINavigationController *)createAccountNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Account" bundle:[NSBundle mainBundle]];
    UINavigationController *accountNavController = [storyBoard instantiateViewControllerWithIdentifier:@"accountNavController"];
    return accountNavController;
}
@end
