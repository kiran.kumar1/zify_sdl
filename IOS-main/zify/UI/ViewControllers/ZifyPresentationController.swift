//
//  ZifyPresentationController.swift
//  zify
//
//  Created by Anurag Rathor on 19/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class ZifyPresentationController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var messageHandler: MessageHandler!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.delegate = self
        let url = URL (string: "https://zify-push-1.s3.ap-south-1.amazonaws.com/zifycorporatepresentation.pdf")
        let request = URLRequest(url: url!)
        self.messageHandler.showBlockingLoadView {
            self.webView.loadRequest(request)
        }
        
        
        
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.messageHandler.dismissBlockingLoadView {
        }
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.messageHandler.dismissBlockingLoadView {
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
