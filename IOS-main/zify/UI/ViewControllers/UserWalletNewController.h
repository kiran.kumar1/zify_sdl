//
//  UserWalletController.h
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageHandler.h"
#import "ScrollableContentViewController.h"
#import "RazorpayPaymentController.h"
#import "StripePaymentController.h"

@interface UserWalletNewController :  ScrollableContentViewController<RazorpayPaymentDelegate,StripePaymentDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic,strong) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,strong) IBOutlet UITextField *amount;
@property (nonatomic,strong) IBOutlet UILabel *availableCash;
@property (nonatomic,strong) IBOutlet UILabel *availablePoints;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *amountButtons;
@property (nonatomic, strong) IBOutlet UIButton *addMoney;
@property (nonatomic,strong) NSString *rechargeAmount;
+(UINavigationController *)createUserWalletNavController;

@property (nonatomic,strong) IBOutlet UILabel *lblAvailableBalanceText;
@property (nonatomic, strong) IBOutlet UIButton *btnViewStatement;
@property (nonatomic,strong) IBOutlet UILabel *lblAddMoney;

@property(nonatomic,strong) IBOutlet UITableView *tblView;
@property (nonatomic,strong) NSArray *arrList;


@end
