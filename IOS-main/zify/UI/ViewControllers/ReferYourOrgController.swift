//
//  ReferYourOrgController.swift
//  zify
//
//  Created by Anurag on 26/02/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class ReferYourOrgController: UIViewController, MFMailComposeViewControllerDelegate, UITextFieldDelegate {

    
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!

    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtStrength: TextFieldWithDone!
    @IBOutlet weak var txtNameAdminHR: UITextField!
    @IBOutlet weak var txtContactEmail: UITextField!
    @IBOutlet weak var txtCompanyCity: UITextField!

    @IBOutlet weak var btnViewPresentation: UIButton!
    
    @IBOutlet weak var lblSubText: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnTermsAndConditions: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftView(txtField: txtCompanyName)
        self.addLeftView(txtField: txtNameAdminHR)
        self.addLeftView(txtField: txtContactEmail)
        self.addLeftView(txtField: txtCompanyCity)
        self.addLeftView(txtField: txtStrength)
        lblHeader.text = NSLocalizedString(refer_your_org_and_earn, comment: "")
        lblSubHeader.text = NSLocalizedString(connect_us_to_your_admin, comment: "")
        txtCompanyName.placeholder = NSLocalizedString(company_name, comment: "")
        txtStrength.placeholder = NSLocalizedString(strength_approx, comment: "")
        txtNameAdminHR.placeholder = NSLocalizedString(name_admin_or_hr, comment: "")
        txtContactEmail.placeholder = NSLocalizedString(admin_or_hr_contact_number, comment: "")
        txtCompanyCity.placeholder = NSLocalizedString(company_city, comment: "")
        lblSubText.text = NSLocalizedString(our_business_team_contact, comment: "")
       
        btnSubmit.setTitle(NSLocalizedString(submit_refer_your_org, comment: ""), for: .normal)
        
        let viewZifyPresentation = NSLocalizedString(view_zify_presentation, comment: "")
        btnViewPresentation.setAttributedTitle(NSAttributedString(string: viewZifyPresentation, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]), for: .normal)
        
        let termsAndCond = NSLocalizedString(terms_and_conditions, comment: "")
        btnTermsAndConditions.setAttributedTitle(NSAttributedString(string: termsAndCond, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = NSLocalizedString(refer_your_organisation, comment: "")
    }

    func addLeftView(txtField:UITextField) {
        let customLeftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
        txtField.leftView = customLeftView;
        txtField.leftViewMode = .always
    }
    
    // MARK: - Actions
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionViewPresentation(_ sender: Any) {
        let stb = UIStoryboard.init(name: "ReferAndEarn", bundle: nil)
        let zifyPresentationCtrl =  stb.instantiateViewController(withIdentifier: "ZifyPresentationController") as? ZifyPresentationController
        self.navigationController?.pushViewController(zifyPresentationCtrl!, animated: true)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        let profileObj = UserProfile.getCurrentUser()
        guard txtCompanyName.text! != "" else {
            showAlertView(strMessage: NSLocalizedString(please_enter_company_name, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        guard txtNameAdminHR.text! != "" else {
            showAlertView(strMessage: NSLocalizedString(please_enter_name_admin_hr, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        guard txtContactEmail.text! != "" else {
            showAlertView(strMessage: NSLocalizedString(please_enter_contact_num_admin_hr, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        guard txtCompanyCity.text! != "" else {
            showAlertView(strMessage: NSLocalizedString(please_enter_city, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        
        
        let strhi = NSLocalizedString(email_content_hi, comment: "")
        let strPleasefindOrdDetails = NSLocalizedString(email_content_please_find_org_details, comment: "")
        let companyName = NSLocalizedString(email_content_company_name, comment: "") + txtCompanyName.text!
        let companyCity = NSLocalizedString(email_content_company_city, comment: "") + txtCompanyCity.text!
        let companyStrength = NSLocalizedString(email_content_company_strength, comment: "") + txtStrength.text!
        let nameAdminHr = NSLocalizedString(email_content_spoc_name, comment: "") + txtNameAdminHR.text!
        let companyAdminHrContact = NSLocalizedString(email_content_spoc_number, comment: "") + txtContactEmail.text!
        
        let referredBy = NSLocalizedString(email_content_referred_by, comment: "") + "\((profileObj?.firstName)! + " " + (profileObj?.lastName)!)"
        let referrerEmailId = NSLocalizedString(email_content_referrer_email_id, comment: "") + "\(profileObj?.emailId ?? "")"
        let referrerNumber = NSLocalizedString(email_content_referrer_number, comment: "") + "\(profileObj?.mobile ?? "") \n\n"
        let strThankYou = NSLocalizedString(email_content_thanks, comment: "")
        
        
        let messageStr = strhi + "\n" + strPleasefindOrdDetails + "\n" + "\n" + companyName + "\n" + companyCity + "\n" + companyStrength + "\n" + nameAdminHr + "\n" + companyAdminHrContact + "\n" + referredBy + "\n" + referrerEmailId + "\n" + referrerNumber + "\n" + "\n" + strThankYou
        
        let subjectStr = NSLocalizedString(refer_your_organisation, comment: "")
        
        let dictionary = ["subject": subjectStr, "messageBody": messageStr, "recipients": "support@zify.co"] as [String : Any]
        AppUtilites.shareEmail(withData: dictionary, with: self as MFMailComposeViewControllerDelegate)
        
        
    }
    
    @IBAction func actionTermsAndConditions(_ sender: Any) {
        self.present(ReferYourOrgController.createReferYourOrgTermsNavController(), animated: true, completion: nil)
    }
    
    class func createReferYourOrgTermsNavController() -> UINavigationController {
        let stb = UIStoryboard.init(name: "ReferAndEarn", bundle: nil)
        let vcReferTermsNavController =  stb.instantiateViewController(withIdentifier: "referYourOrgtermsAndCondNav") as? UINavigationController
        return vcReferTermsNavController!
    }
    
    // MARK:- Mail Compose Controller
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        let dataDict = ["organisation_name":self.txtCompanyName.text!,"city":self.txtCompanyCity.text!] as [String:Any]
        MoEngageEventsClass.callReferYourOrganisationEvent(withDataDict: dataDict)
        
        dismiss(animated: true) {
        }
        
        
    }
    
    // MARK:- UITextField Delegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtCompanyName {
            txtStrength.becomeFirstResponder()
        } else if textField == txtStrength {
            txtNameAdminHR.becomeFirstResponder()
        } else if textField == txtNameAdminHR {
            txtContactEmail.becomeFirstResponder()
        } else if textField == txtContactEmail {
            txtCompanyCity.becomeFirstResponder()
        } else if textField == txtCompanyCity {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
