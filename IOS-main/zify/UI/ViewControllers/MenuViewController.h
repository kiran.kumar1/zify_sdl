//
//  MenuViewController.h
//  zify
//
//  Created by Anurag S Rathor on 09/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>


enum MenuOptions {
   
    SIDE_MENU_ACTIVE_TRIPS, SIDE_MENU_UPCOMING_TRIPS, SIDE_MENU_REWARDS, SIDE_MENU_WALLET, SIDE_MENU_HISTORY, SIDE_MENU_LEAVE_FEEDBACK, SIDE_MENU_SETTINGS, REFERANDEARN, SIDE_MENU_ABOUT, SIDE_MENU_MESSAGE, SIDE_MENU_TRAVEL_PREFERENCE, SIDE_MENU_PROFILE
    
    /*TRIPS,ACCOUNT,WALLET,PROFILE,CURRENTTRIP,ABOUT,SETTINGS,REFERANDEARN,
     REWARDS,MESSAGES,WALLETSTATEMENT,TRAVELPREFERENCE,LEAVEFEEDBACK*/
};

@protocol MenuDelegate <NSObject>
-(void) selectedMenuOption:(enum MenuOptions)option;
@end

@interface MenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    UIView *baseView;
}
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *bottomView;
@property (nonatomic,weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) id<MenuDelegate> menuDelegate;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *contentViewLeftConstraint;
@property(nonatomic) float profileCompleteness;
@property(nonatomic,strong) UIProgressView* completenessProgress;
@property(nonatomic,strong) UILabel *profileCompLbl;


@property(nonatomic,strong)NSArray *dataOptinsArr, *imageNamesArr;
+(MenuViewController *)createAndOpenMenuWithSourceController:(UIViewController *)srcViewController andMenuDelegate:(id<MenuDelegate>)menuDelegate;
@end
