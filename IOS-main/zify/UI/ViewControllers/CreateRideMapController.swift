//
//  CreateRideMapController.swift
//  zify
//
//  Created by Anurag on 10/09/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit
import MSPeekCollectionViewDelegateImplementation


class CreateRideMapController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, GMSMapViewDelegate, MapViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var mapContainerView: MapContainerView!
    @IBOutlet var collView: UICollectionView!
    //  @IBOutlet var collectionViewTopConstraint:NSLayoutConstraint!
    @IBOutlet weak var messageHandler: MessageHandler!
    var profileImagePlaceHolder: UIImage?

    var mapview:MapView?
    
    var dictResponseObj : NSDictionary = [:]
    var iValueResutsObj : Int = 0
    var searchRidesObj: [AnyObject] = []
    
    var currentSearchRideEntity:SearchRideEntity?
    var searchRidesObjects: [AnyObject] = []
    var selectedIndex:Int = 0
    
    var padding:Float = 0
    var userSearchData: UserSearchData?
    var displayIndex:Int = 0
    var uploadView: Userprofile_New!

    var userData:UserSearchData?
    
    
   // var delegate: MSPeekCollectionViewDelegateImplementation!

    var peekImplementation: MSPeekCollectionViewDelegateImplementation!

    override func viewDidLoad() {
        super.viewDidLoad()
        profileImagePlaceHolder = UIImage(named: "placeholder_profile_image")

      //  debugPrint(searchRidesObj, "-----searchRidesObjects---------")
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        self.collView.collectionViewLayout = flowLayout
        self.collView.isPagingEnabled = true
        self.mapContainerView.createMapView()
        self.mapview = self.mapContainerView.mapView
        self.displayIndex = self.selectedIndex
        
       
        AppDelegate.getInstance().setbackgroungdImage(forNavigatuionBar: self.navigationController?.navigationBar)

        
//        if let flowlayout = collView.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowlayout.estimatedItemSize = CGSize(width: 315, height: 216)//UICollectionViewFlowLayoutAutomaticSize
//        }
        
        peekImplementation = MSPeekCollectionViewDelegateImplementation()
        peekImplementation.delegate = self as MSPeekImplementationDelegate
        collView.configureForPeekingDelegate()
        collView.delegate = peekImplementation
        collView.dataSource = self
        
        
        peekImplementation.cellSpacing = 0
        peekImplementation.cellPeekWidth = 20
        
      //  self.mapview?.drawMap()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.padding = Float(self.view.frame.size.height) - Float(self.collView.frame.origin.y)
        currentSearchRideEntity = searchRidesObj[selectedIndex] as? SearchRideEntity
        if let obj = currentSearchRideEntity {
            self.showMarkersOnMapViewFromUser(searchObj: obj,isForFirstTime: true)
        }

        let indexpath = IndexPath.init(row: selectedIndex, section: 0)
        self.collView.scrollToItem(at: indexpath, at: .right, animated: false)
    }
    
    func showMarkersOnMapViewFromUser(searchObj:SearchRideEntity, isForFirstTime:Bool){
        currentSearchRideEntity = searchObj
        
      //  DispatchQueue.global(qos: .background).async {
        
        print("This is run on the background queue")
        RouteInfoView.showMapInfo(on: self.mapview!, withSearchRide: searchObj, withCollectionViewHeight: CGFloat(self.padding), with: self.userSearchData, withIsforFirstTime: isForFirstTime)
        
   //     RouteInfoView.drawPolilinesBetweenorigin((mapview?.sourceCoordinate)!, withDestination: (mapview?.destCoordinate)!, withmapViw: self.mapview)
        //+(void)drawPolilinesBetweenorigin:(CLLocationCoordinate2D)origin withDestination:(CLLocationCoordinate2D)destination withmapViw:(MapView *)mapView
     //   self.drawDashedLinebetweenTwoPoints(originLocation: (mapview?.sourceCoordinate)!, destinationLocation: (mapview?.destCoordinate)!)
        
        
        //            DispatchQueue.main.async {
        //                print("This is run on the main queue, after the previous code in outer block")
        //            }
        
     //   }
        
    }
    
    // MARK: - Actions
    
    @IBAction func btnTappedBack(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - CollectionView Delegates
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) // top, left, bottom, right
//    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//
//        return 0.0
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchRidesObj.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("cell is loaded from CellForRow \(indexPath.row)")
        
        let cellDriveDetails = collView.dequeueReusableCell(withReuseIdentifier: "DriverDetailCollectionCell", for: indexPath) as? DriverDetailCollectionCell
        
        self.drawShadow(toAView: cellDriveDetails?.contentViewBg)
        

        
        cellDriveDetails?.backgroundColor = UIColor.clear
        cellDriveDetails?.backgroundView = nil
        cellDriveDetails?.contentView.backgroundColor = UIColor.clear
        let  tripDetails = searchRidesObj[indexPath.item] as? SearchRideEntity
        let driveDetails = tripDetails?.driverDetail
        cellDriveDetails?.lblTime.text = getDateAndTime(tripDetails?.departureTime, strType: "Time")
        cellDriveDetails?.lblDate.text = getDateAndTime(tripDetails?.departureTime, strType: "Date")
        
        cellDriveDetails?.lblPrice.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator:tripDetails?.amountPerSeat)
        cellDriveDetails?.lblSourceAddress.text = tripDetails?.srcAdd ?? ""
        cellDriveDetails?.lblDestinationAddress.text = tripDetails?.destAdd ?? ""
        cellDriveDetails?.lblDriverName.text = (driveDetails?.firstName.capitalized ?? "") + " " + (driveDetails?.lastName ?? "")
        if driveDetails?.companyName == nil || driveDetails?.companyName == "" {
            cellDriveDetails?.lblWorks.text = NSLocalizedString(UPDX_USERPROFILE_LBL_WORKSAT, comment: "")
        } else {
            cellDriveDetails?.lblWorks.text = (driveDetails?.companyName ?? "") //NSLocalizedString(UPDX_USERPROFILE_LBL_WORKSAT, comment: "") + " " + (driveDetails?.companyName ?? "")
        }
        
        
        //cellDriveDetails?[indexPath.item] = cellDriveDetails.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        
        cellDriveDetails?.imgProfile.sd_setImage(with: URL(string: tripDetails?.driverDetail.profileImgUrl ?? ""), placeholderImage: profileImagePlaceHolder, completed: { image, error, cacheType, imageURL in
        })
        if driveDetails?.driverStatus == "VERIFIED" {
            cellDriveDetails?.imgVerified.isHidden = false
        } else {
            cellDriveDetails?.imgVerified.isHidden = true
        }
        
        if driveDetails?.chatProfile.chatUserId != nil {
            cellDriveDetails?.btnChat.isHidden = false;
        } else {
            cellDriveDetails?.btnChat.isHidden = true;
        }
        
        cellDriveDetails?.btnRequestSend.addTarget(self, action: #selector(self.callApi(forRideRequest:)), for: .touchUpInside)
        cellDriveDetails?.btnChat.addTarget(self, action: #selector(self.showChat(_:)), for: .touchUpInside)
        cellDriveDetails?.btnProfile.addTarget(self, action: #selector(self.showUserProfile(_:)), for: .touchUpInside)

        
        cellDriveDetails?.lblTime.fontSize = 17
        cellDriveDetails?.lblDate.fontSize = 14
        cellDriveDetails?.lblPrice.fontSize = 21
        cellDriveDetails?.lblSourceAddress.fontSize = 12
        cellDriveDetails?.lblDestinationAddress.fontSize = 12
        cellDriveDetails?.lblDriverName.fontSize = 12
        cellDriveDetails?.lblWorks.fontSize = 10
        if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            cellDriveDetails?.lblTime.fontSize = 20
            cellDriveDetails?.lblDate.fontSize = 13
            cellDriveDetails?.lblPrice.fontSize = 14
            cellDriveDetails?.lblSourceAddress.fontSize = 12
            cellDriveDetails?.lblDestinationAddress.fontSize = 12
            cellDriveDetails?.lblDriverName.fontSize = 12
            cellDriveDetails?.lblWorks.fontSize = 11
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            cellDriveDetails?.lblTime.fontSize = 20
            cellDriveDetails?.lblDate.fontSize = 15
            cellDriveDetails?.lblPrice.fontSize = 16
            cellDriveDetails?.lblSourceAddress.fontSize = 14
            cellDriveDetails?.lblDestinationAddress.fontSize = 14
            cellDriveDetails?.lblDriverName.fontSize = 14
            cellDriveDetails?.lblWorks.fontSize = 12
        }
        
       // cellDriveDetails?.lblSourceAddress.text = "source ddress will be shown here... please proceed to display"
       // cellDriveDetails?.lblDestinationAddress.text = "lblDestinationAddress ddress will be shown here... please proceed to display"
        cellDriveDetails?.layoutIfNeeded()
        return cellDriveDetails ?? UICollectionViewCell()
    }
    
    
    
    func showPopUpForpProfile() {
        let viewsArray = Bundle.main.loadNibNamed("IDCardViews", owner: self, options: nil)
        
        for object: Any? in viewsArray ?? [] {
            if (object is Userprofile_New) {
                uploadView = object as? Userprofile_New
                break
            }
        }
        if let uploadView = uploadView {
            AppDelegate.getInstance().window.addSubview(uploadView)
        }
        uploadView?.translatesAutoresizingMaskIntoConstraints = false
        uploadView?.setDataForView(withSearchEntity: self.currentSearchRideEntity!)
        let viewsDictionary:Dictionary = NSDictionary.init(object: uploadView, forKey: "uploadView" as NSCopying) as Dictionary
        let horizontalConsts: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "|[uploadView]|", options: [], metrics: nil, views: viewsDictionary as! [String : Any])
        let verticalConsts: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|[uploadView]|", options: [], metrics: nil, views: viewsDictionary as! [String : Any])
        AppDelegate.getInstance().window.addConstraints(horizontalConsts)
        AppDelegate.getInstance().window.addConstraints(verticalConsts)
        
    }
    
  /*  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in self.collView.visibleCells{
            if let indexpath =  self.collView.indexPath(for: cell){
                print("showing cell is \(indexpath.row)")
                self.displayIndex = indexpath.row
                if(self.displayIndex == self.selectedIndex){
                    return
                }
                self.selectedIndex = indexpath.row
                if( indexpath.row <= self.searchRidesObj.count){
                    self.currentSearchRideEntity = self.searchRidesObj[indexpath.row] as? SearchRideEntity
                    if let obj = currentSearchRideEntity {
                        self.showMarkersOnMapViewFromUser(searchObj: obj)
                    }
                }
            }
        }
    }*/
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width:(collView.frame.width), height: collView.frame.height)
//    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        return CGSize(width:(collView.frame.width - 10), height: collView.frame.height)
//    }
    
    func getDateAndTime(_ tritDateTime: String?, strType: String?) -> String? {
        //NSLog(@"time is %@",tritDateTime);
        //ride.departureTime
        let dateTimeFormatter1 = DateFormatter()
        dateTimeFormatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateTimeFormatter1.locale = locale as Locale
        
        
        let departureTime = dateTimeFormatter1.date(from: tritDateTime ?? "")
        let dateFormatter1 = DateFormatter()
        
        if strType?.isEqual("Time") ?? false {
            var timeStr: String? = nil
            if let departureTime = departureTime {
                timeStr = AppDelegate.getInstance().timeFormatter.string(from: departureTime)
            }
            return timeStr
        } else if strType?.isEqual("Date") ?? false {
            dateFormatter1.dateFormat = "dd MMM" //"dd MMM yyyy"
            let dateStr = getDateString(fromDepartureTime: departureTime)
            return dateStr
        }
        
        var timeStr: String? = nil
        if let departureTime = departureTime {
            timeStr = AppDelegate.getInstance().timeFormatter.string(from: departureTime)
        }
        let dateStr = getDateString(fromDepartureTime: departureTime)
        return "\(dateStr ?? ""), \(timeStr ?? "")"
    }
    
    func getDateString(fromDepartureTime date: Date?) -> String? {
        let currentDayDate = getCurrentDayDate()
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd MMM"
        
        let calendar = Calendar(identifier: .gregorian)
        var components: DateComponents? = nil
        if let currentDayDate = currentDayDate, let date = date {
            components = calendar.dateComponents([.day], from: currentDayDate, to: date)
            //components = calendar.dateComponents(.day, from: currentDayDate, to: date, options: [])
        }
        let difference = components?.day
        if difference == 0 {
            return NSLocalizedString(CMON_GENERIC_TODAY, comment: "")
        } else if difference == 1 {
            return NSLocalizedString(CMON_GENERIC_TOMORROW, comment: "")
        } else {
            if let date = date {
                return dateFormatter1.string(from: date)
            }
            return nil
        }
    }
    
    func getCurrentDayDate() -> Date? {
        let calendar = Calendar(identifier: .gregorian)
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: Date())
        components.hour = 0
        components.minute = 0
        components.second = 0
        return calendar.date(from: components)
        
        return nil
    }
    
    func showChat(_ sender: UIButton?) {
        //let indexPath = collView.indexPath(for: getTableViewCell(sender!)!)
        let ride = self.currentSearchRideEntity //searchRidesObj[indexPath?.item ?? 0] as? SearchRideEntity
        let driverDetail = ride?.driverDetail
        if driverDetail?.chatProfile.chatUserId == nil {
            return
        }
        let detail = UserContactDetail(chatIdentifier: driverDetail?.chatProfile.chatUserId, andCallIdentifier: nil, andFirstName: driverDetail?.firstName, andLastName: driverDetail?.lastName, andImageUrl: driverDetail?.profileImgUrl)
        let chatNavController = ChatUserListController.createChatNavController()
        let chatUserListContoller = chatNavController?.topViewController as? ChatUserListController
        chatUserListContoller?.receivingUser = detail
        if let chatNavController = chatNavController {
            present(chatNavController, animated: true)
        }
    }
    
    func showUserProfile(_ sender: UIButton?) {
       self.showPopUpForpProfile()
    }
    
    func callApi(forRideRequest sender: UIButton?) {
        
        if self.currentSearchRideEntity == nil {
            return
        }
        
        
        let reqCreateRide = CreateRideRequest2.init(searchRideEntity: self.currentSearchRideEntity, driveid: self.currentSearchRideEntity?.driveId as! NSNumber, driverid : self.currentSearchRideEntity?.driverId as! NSNumber, srcnearlat: "\(mapview?.sourceCoordinate.latitude ?? 0)" as NSString, srcnearlong: "\(mapview?.sourceCoordinate.longitude ?? 0)" as NSString, destnearlat: "\(mapview?.destCoordinate.latitude ?? 0)"  as NSString, destnearlong: "\(mapview?.destCoordinate.longitude ?? 0)" as NSString, withUserSearchData: self.userSearchData!)
        /****/
        messageHandler.showBlockingLoadView(handler: {
            
            ServerInterface.sharedInstance()?.getResponse(reqCreateRide, withHandler: { (response, error) in
                
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == -30 {
                            //NSArray *extraObjDict = [[response.responseObject objectForKey:@"statusInfo"]  objectForKey:@"extraParams"];
                            
                            let extraObjDict = ((((response?.responseObject as? AnyObject)?.value(forKey: "statusInfo")) as? AnyObject)?.value(forKey: "extraParams")) as? [AnyObject]
                            
                           // let extraObjDict = (response?.responseObject?["statusInfo"] as? [AnyHashable : Any])?["extraParams"] as? [AnyHashable]
                            var currenyType = ""
                            var amountValue = ""
                            if (extraObjDict?.count ?? 0) >= 2 {
                                currenyType = extraObjDict?[0] as? String ?? ""
                                amountValue = extraObjDict?[1] as? String ?? ""
                            }
                            let message = String(format: NSLocalizedString(insufficient_recharge_error, comment: ""), currenyType, amountValue)
                            
                            let dataDictToMoengage = [
                               // "CarOwner_Firstname" : (self.currentSearchRideEntity as? AnyObject)?.driverDetail.firstName,
                               // "CarOwner_Lastname" : (self.currentSearchRideEntity as? AnyObject)?.driverDetail.lastName,
                               // "source_address" : (self.currentSearchRideEntity as? AnyObject)?.sourceLocality.address,
                                //"destination_address" : (self.currentSearchRideEntity as? AnyObject)?.destinationLocality.address,
                                //"trip_Time" : (self.currentSearchRideEntity as? AnyObject)?.departureTime,
                                "currency" : currenyType,
                                "amount" : amountValue
                                ] as [String : Any]
                            
                            MoEngageEventsClass.callRideRequestLowBalanceEvent(withDataDict: dataDictToMoengage)
                            let alert = UniversalAlert(title: NSLocalizedString(VC_CREATERIDE_RECHARGENOW, comment: ""), withMessage: message)
                            alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_OK, comment: ""), withAction: { action in
                                UserDefaults.standard.set(true, forKey: "toAddMoney")
                                UserDefaults.standard.synchronize()
                                let walletNavController = AddMoneyController.createUserWalletNavController()
                                let walletController = walletNavController?.topViewController as? WalletController
                                walletController?.rechargeAmount = amountValue
                                if let walletNavController = walletNavController {
                                    self.present(walletNavController, animated: true)
                                }
                            })
                            alert?.show(in: self)
                            sender?.isUserInteractionEnabled = true
                            //UIAlertController.showError(withMessage: response?.message, inViewController: self)
                            //  [self.messageHandler showSuccessMessage:response.message];
                        } else {
                            let dataDictToMoengage = [
                                //"CarOwner_Firstname" : searchRidesObj.driverDetail.firstName,
                               // "CarOwner_Lastname" : searchRidesObj.driverDetail.lastName,
                                "source_address" : self.userSearchData?.sourceLocality.address,
                                "destination_address" : self.userSearchData?.destinationLocality.address,
                               // "trip_Time" : searchRidesObj.departureTime
                            ]
                            
                            MoEngageEventsClass.callRideRequestEvent(withDataDict: dataDictToMoengage)
                            UIAlertController.showError(withMessage: response?.message, inViewController: self)
                            //  [self.messageHandler showSuccessMessage:response.message];
                            self.perform(#selector(self.showTrips), with: self, afterDelay: 1.0)
                            //self.perform(#selector(self.showTrips), with: self, afterDelay: 1.0)
                            
                        }
                    } else {
                        if let userInfo = (error as NSError?)?.userInfo {
                            print("error.userInfo is \(userInfo)")
                        }
                        UIAlertController.showError(withMessage: (error as NSError?)?.userInfo["message"] as? String, inViewController: self)
                        // [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                        sender?.isUserInteractionEnabled = true
                    }
                })
            })
        })
/*****/
        
    }
    
    func showTrips() {
        let rootController = AppDelegate.getInstance().window.rootViewController
        if rootController?.presentedViewController != nil {
            
            rootController?.dismiss(animated: false) {
                AppUtilites.applicationVisibleViewController().present(TripsTabController.createTripsTabNavController(), animated: true)
            }
        }
    }

    
    
    func getTableViewCell(_ button: AnyObject) -> UICollectionViewCell? {
        var btn : AnyObject = button
        while !(btn is UICollectionViewCell) {
            //btn = (btn as AnyObject).superview()
            btn = btn.superview as AnyObject
        }
        return btn as? UICollectionViewCell
    }
    
    
    
    
    func drawShadow(toAView viewShadow: UIView?) {
        
        
      
        
        let color = UIColor.lightGray//UIColor(red: 42.0 / 255.0, green: 42.0 / 255.0, blue: 42.0 / 255.0, alpha: 0.2)
        let shadowRadius = 5
        let radius = viewShadow?.layer.cornerRadius
        let size = CGSize(width: 0, height: 0)
        viewShadow?.backgroundColor = UIColor.white
        viewShadow?.layer.shadowOpacity = 0.5
        viewShadow?.layer.shadowOffset = size
        viewShadow?.layer.shadowRadius = CGFloat(shadowRadius)
        viewShadow?.layer.masksToBounds = false
        viewShadow?.layer.cornerRadius = CGFloat(radius ?? 0)
        viewShadow?.layer.shadowColor = color.cgColor
        
    }
    
    class func createRideMapNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let createRideMapNavCtrl = storyBoard.instantiateViewController(withIdentifier: "createRideMapNavController") as? UINavigationController
        return createRideMapNavCtrl
    }
    
    class func createRideMapController() -> CreateRideMapController? {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let addBankController = storyBoard.instantiateViewController(withIdentifier: "CreateRideMapController") as? CreateRideMapController
        return addBankController
    }
    
    
    
    func drawDashedLinebetweenTwoPoints(originLocation:CLLocationCoordinate2D, destinationLocation:CLLocationCoordinate2D){
        
 /*       let path = GMSMutablePath()
        path.add(originLocation)
        path.add(destinationLocation)
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.orange
        polyline.map = self.mapview
     
        let marker = GMSMarker.init()
        marker.position = CLLocationCoordinate2D.init(latitude: originLocation.latitude, longitude: originLocation.longitude)
        
        marker.icon = UIImage.init(named: "")
        marker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        marker.map = self.mapview
        
        let path = GMSMutablePath.init()
        path.add(originLocation)
        path.add(destinationLocation)
     
        let rectangle = GMSPolyline.init(path: path)
        rectangle.strokeWidth = 2.0
        rectangle.strokeColor = UIColor.orange
        rectangle.map = self.mapview
 */
    }
}

class DriverDetailCollectionCell : UICollectionViewCell {
    
    @IBOutlet var contentViewBg: UIView!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblSourceAddress: UILabel!
    @IBOutlet var lblDestinationAddress: UILabel!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var imgVerified: UIImageView!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblWorks: UILabel!
    @IBOutlet var btnChat: UIButton!
    @IBOutlet var btnRequestSend: UIButton!
    
    
    
    override func awakeFromNib() {
     //   super.awakeFromNib()
//        contentViewBg.translatesAutoresizingMaskIntoConstraints = false
//        let rightConstraint = contentViewBg.rightAnchor.constraint(equalTo: rightAnchor)
//        NSLayoutConstraint.activate([rightConstraint])
    }
    
}
extension String {
    
    var numberValue:NSNumber? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.number(from: self)
    }
}
extension CreateRideMapController: MSPeekImplementationDelegate {
    func peekImplementation(_ peekImplementation: MSPeekCollectionViewDelegateImplementation, didChangeActiveIndexTo activeIndex: Int) {
        print("Changed active index to \(activeIndex)")
        if(activeIndex < 0 || activeIndex >= self.searchRidesObj.count){
            return
        }
        self.displayIndex = activeIndex
        if(self.displayIndex == self.selectedIndex){
            return
        }
        self.selectedIndex = activeIndex
        self.currentSearchRideEntity = self.searchRidesObj[activeIndex] as? SearchRideEntity
        if let obj = currentSearchRideEntity {
            self.showMarkersOnMapViewFromUser(searchObj: obj,isForFirstTime: false)
        }
        
    }
    
    func peekImplementation(_ peekImplementation: MSPeekCollectionViewDelegateImplementation, didSelectItemAt indexPath: IndexPath) {
        print("Selected item at \(indexPath)")
    }
}
