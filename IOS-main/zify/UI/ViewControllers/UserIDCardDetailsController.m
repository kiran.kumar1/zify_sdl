//
//  UserIDCardDetailsController.m
//  zify
//
//  Created by Anurag S Rathor on 11/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserIDCardDetailsController.h"
#import "CurrentLocale.h"
#import "UserProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ProportionalFill.h"
#import "ServerInterface.h"
#import "UserIdentityCardSaveRequest.h"
#import "OnboardFlowHelper.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

@interface UserIDCardDetailsController ()

@end

@implementation UserIDCardDetailsController{
    NSString *identityCardTypeSelectedValue;
    NSArray *identityCardPickerValues;
    UserProfile *currentUser;
    UIImage *selectedIdentityCardImage;
    BOOL isIDCardUploaded;
    BOOL isViewInitialised;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    _lblSafe.hidden = YES;
    _lblIdCardTypeSelected.hidden = YES;
    isViewInitialised = false;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //self.navigationController.navigationBarHidden = YES;
    
    if (IS_IPHONE_5) {
        _lblSafe.fontSize = 13;
        _lblIdCardTypeSelected.fontSize = 13;
        _uploadIdCardBtn.titleLabel.fontSize = 14;
        _lblIdCardSecureStoredText.fontSize = 9;
        _lblNotShowAnyOneIDCardText.fontSize = 9;
        _lblTapToUploadText.fontSize = 10;
        _lblTakePictureText.fontSize = 10;
        _btnNext.titleLabel.fontSize = 14;
        _btnRemindLater.titleLabel.fontSize = 14;
        _pageTitle.fontSize = 14;
    } else if (IS_IPHONE_6) {
        _lblSafe.fontSize = 14;
        _lblIdCardTypeSelected.fontSize = 14;
        _uploadIdCardBtn.titleLabel.fontSize = 16;
        _lblIdCardSecureStoredText.fontSize = 10;
        _lblNotShowAnyOneIDCardText.fontSize = 10;
        _lblTapToUploadText.fontSize = 10;
        _lblTakePictureText.fontSize = 10;
        _btnNext.titleLabel.fontSize = 14;
        _btnRemindLater.titleLabel.fontSize = 14;
        _pageTitle.fontSize = 15;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _lblSafe.fontSize = 16;
        _lblIdCardTypeSelected.fontSize = 16;
        _uploadIdCardBtn.titleLabel.fontSize = 18;
        _lblIdCardSecureStoredText.fontSize = 11;
        _lblNotShowAnyOneIDCardText.fontSize = 11;
        _lblTapToUploadText.fontSize = 11;
        _lblTakePictureText.fontSize = 11;
        _btnNext.titleLabel.fontSize = 15;
        _btnRemindLater.titleLabel.fontSize = 15;
        _pageTitle.fontSize = 16;
    }
    
    if(!isViewInitialised) [self initilaiseView];
}

-(void) initilaiseView {
    identityCardPickerValues = [[CurrentLocale sharedInstance] getIdentityCardTyes];
    identityCardTypeSelectedValue = [identityCardPickerValues objectAtIndex:0];
    currentUser = [UserProfile getCurrentUser];
    isIDCardUploaded = false;
    
    _identityCardType.text = identityCardTypeSelectedValue;
    _errorMessage.text = @"";
    _identityCardTypeView.layer.cornerRadius = 3.0f;
    _identityCardTypeView.clipsToBounds = YES;
    _identityCardTypeView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _identityCardTypeView.layer.borderWidth = 1.0f;
    UIImage *arrowImage = [UIImage imageNamed:@"left_arrow.png"];
    _identityCardTypeArrow.image = [arrowImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_identityCardTypeArrow setTintColor:[UIColor lightGrayColor]];
    _identityCardTypeArrow.transform = CGAffineTransformMakeRotation(-M_PI_2);
    _closePageArrow.image = [arrowImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_closePageArrow setTintColor:[UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0]];
    if(!_isUpgradeFlow) {
        _pageTitle.text = NSLocalizedString(VC_USERIDCARD_IDENTITYCARD, nil);
        //self.navBar.title = NSLocalizedString(VC_USERIDCARD_IDENTITYCARD, nil);
        self.navigationItem.title = NSLocalizedString(VC_USERIDCARD_IDENTITYCARD, nil);
        
        _pageControl.hidden = YES;
        if([currentUser.userDocuments.isIdCardDocUploaded intValue]  && ![currentUser.userDocuments.idCardImgUrl isKindOfClass:[NSNull class]] && ![@"" isEqualToString:currentUser.userDocuments.idCardImgUrl]){
            identityCardTypeSelectedValue = [[CurrentLocale sharedInstance] getIdentityCardTypeValue:currentUser.userDocuments.idCardType];
            _identityCardType.text = identityCardTypeSelectedValue;
            _identityCardTypeView.userInteractionEnabled = NO;
            _identityCardPlaceholder.hidden = YES;
            _identityCardImage.userInteractionEnabled = NO;
             _identityCardImage.hidden = NO;
            NSURL *identityCardImageURL = [NSURL URLWithString:currentUser.userDocuments.idCardImgUrl];
            [_identityCardImage sd_setImageWithURL:identityCardImageURL completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
                if(image){
                    _identityCardImage.image = [image imageScaledToFitSize:_identityCardImage.frame.size];
                }
                _identityCardImage.hidden = NO;
            }];
            [_uploadIdCardBtn setTitle:NSLocalizedString(VC_USERIDCARD_UPLOADED, nil) forState:UIControlStateNormal];
            _uploadIdCardBtn.hidden = YES;
            _identityCardTypeView.hidden = YES;
            _uploadIdCardBtn.userInteractionEnabled = NO;
            _lblSafe.hidden = NO;
            _lblIdCardTypeSelected.hidden = NO;
            _lblIdCardTypeSelected.text = [identityCardTypeSelectedValue uppercaseString];
        }
        _remindLaterView.hidden = YES;
    } else{
        _closePageArrow.hidden = YES;
    }
    isViewInitialised = true;
}

-(IBAction) selectIdentityCard:(id)sender{
    self.identityCardPickerView.delegate = self;
    self.identityCardPickerView.pickervalues = identityCardPickerValues;
    self.identityCardPickerView.currentValue = identityCardTypeSelectedValue;
    [self.identityCardPickerView showInView:self.view];
}

-(IBAction) selectIdentityCardImage:(id)sender{
    [ImageSelectionViewer sharedInstance].imageSelectionDelegate = self;
    [ImageSelectionViewer sharedInstance].finalImageSize = CGSizeMake(300.0,300.0);
    [[ImageSelectionViewer sharedInstance] selectImage];
}


-(IBAction) uploadIdentityCardInfo:(id)sender{
   /* if([self validateIdentityCardDetails]){
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance] getResponse:[[UserIdentityCardSaveRequest alloc] initWithIdentityCardType:identityCardTypeSelectedValue] withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        _identityCardTypeView.userInteractionEnabled = NO;
                        _identityCardImage.userInteractionEnabled = NO;
                        [_uploadIdCardBtn setTitle:NSLocalizedString(VC_USERIDCARD_UPLOADED, nil) forState:UIControlStateNormal];
                        _uploadIdCardBtn.userInteractionEnabled = NO;
                        if(!_isUpgradeFlow){
                            [self performSelector:@selector(dismiss:) withObject:self afterDelay:1.0];
                        }
                        isIDCardUploaded = true;
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            } andImage:selectedIdentityCardImage];
        }];
    }*/
}


-(IBAction) setProfileUpgradeReminder:(id)sender {
    [[OnboardFlowHelper sharedInstance] setReminder:self];
}

-(IBAction) moveToNextStep:(id)sender {
    if(!isIDCardUploaded){
        self.errorMessage.text = NSLocalizedString(VC_USERIDCARD_IDCARDMOVENEXTVAL, nil);
        return;
    }
    [[OnboardFlowHelper sharedInstance] handleIDCardDetailsFlow:self];
}


-(IBAction) dismiss:(id)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isForPush = [prefs boolForKey:@"forPush"];
    if(isForPush){
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void) selectedPickerValue:(NSString *)value{
    identityCardTypeSelectedValue = value;
    _identityCardType.text = value;
}

-(void) userSelectedFinalImage:(UIImage *)image{
    selectedIdentityCardImage = image;
    _identityCardPlaceholder.hidden = YES;
    _identityCardImage.image = image;
}

-(BOOL) allowImageEditing{
    return YES;
}

-(BOOL) validateIdentityCardDetails{
    if(selectedIdentityCardImage == nil){
        self.errorMessage.text = NSLocalizedString(VC_USERIDCARD_IDCARDIMAGEVAL, nil);
        return false;
    }
    self.errorMessage.text = @"";
    return true;
}

+(UserIDCardDetailsController *)createUserIDCardDetailsController {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"UserOnboard" bundle:[NSBundle mainBundle]];
    UserIDCardDetailsController *userIDCardDetailsController = [storyBoard instantiateViewControllerWithIdentifier:@"userIDCardDetailsController"];
    return userIDCardDetailsController;
}

+(UINavigationController *)userIDCardDetailsNavCtrl {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"UserOnboard" bundle:[NSBundle mainBundle]];
    UINavigationController *userIDCardNavController = [storyBoard instantiateViewControllerWithIdentifier:@"userIDCardDetailsNavCtrl"];
    return userIDCardNavController;
}



@end





