//
//  AboutSupportController.h
//  zify
//
//  Created by Anurag S Rathor on 13/08/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollableContentViewController.h"
#import  "MessageHandler.h"
#import "ImageSelectionViewer.h"
#import  <MessageUI/MessageUI.h>

@interface AboutSupportController : ScrollableContentViewController<ImageSelectionViewerDelegate>
@property (nonatomic,weak) IBOutlet UITextView *issueTextView;
@property (nonatomic,strong) IBOutletCollection(UIView) NSArray *screenshotPlaceholders;
@property (nonatomic,strong) IBOutletCollection(UIImageView) NSArray *screenshotImages;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) id<MFMailComposeViewControllerDelegate> messageDelegate;
@property (weak, nonatomic) IBOutlet UIButton *btnFreshChat;
@property (weak, nonatomic) IBOutlet UILabel *lblPleaseDescYourPrblm;
@property (weak, nonatomic) IBOutlet UIButton *btnReadOurFAQ;
@property (weak, nonatomic) IBOutlet UILabel *lblAddScreenShots;
@property (weak, nonatomic) IBOutlet UILabel *lblReadOurPrivacyPolicy;
@property (weak, nonatomic) IBOutlet UIButton *btnPrivacyPolicy;
-(IBAction)showFreshChat:(id)sender;

@end
