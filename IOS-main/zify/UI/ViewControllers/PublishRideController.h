//
//  PublishRideController.h
//  zify
//
//  Created by Anurag S Rathor on 23/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleMapView.h"
#import  "PublishDriveRequest.h"
#import  "MessageHandler.h"
#import "CustomPickerView.h"
#import "DriveRoute.h"
#import "MapContainerView.h"
#import <NMAKit/NMAKit.h>

@protocol PublishRideDelgate <NSObject>
@optional
-(void) showActiveTripOnPublishRide;
-(void) selectedDriveRoute:(DriveRoute *)route;
@end

@interface PublishRideController : UIViewController<CustomPickerDelegate>
@property (nonatomic,weak) IBOutlet MapContainerView *mapContainerView;
@property (nonatomic,strong)  NMAMapView *heremapsView;
@property (nonatomic,weak) IBOutlet UIView *seatsNumView;
@property (nonatomic,weak) IBOutlet UILabel *seatsNumLabel;
@property (nonatomic,weak) IBOutlet UIButton *btnIncreaseNum;
@property (nonatomic,weak) IBOutlet UIButton *btnDecreaseNum;
@property (nonatomic,strong) IBOutlet UILabel *sourceLocality;
@property (nonatomic,strong) IBOutlet UILabel *destinationLocality;
@property (nonatomic,strong) IBOutlet UIButton *backward;
@property (nonatomic,strong) IBOutlet UILabel *distance;
@property (nonatomic,strong) IBOutlet UILabel *index;
@property (nonatomic,strong) IBOutlet UIButton *forward;
@property (nonatomic,strong) IBOutlet UIView *mapRoutesInfoView;
@property (nonatomic,strong) IBOutlet UIView *mapRoutesNavigationView;
@property (nonatomic,strong) IBOutlet UIButton *publish;
@property (nonatomic,strong) IBOutlet UIView *driveInfoView;
@property (nonatomic,weak) IBOutlet CustomPickerView *seatPickerView;
@property (nonatomic,strong) IBOutlet MessageHandler *messageHandler;
@property (nonatomic, weak) id<PublishRideDelgate> publishRideDelegate;
@property (nonatomic,strong) NSArray *routes;
@property (nonatomic,strong) PublishDriveRequest *publishDriveRequest;
@property (nonatomic) BOOL isRouteSelectOnly;
+(UINavigationController *)createPublishRideNavController;

//@property(nonatomic,strong) LocalityInfo *sourceAddLocInfo;
//@property(nonatomic,strong) LocalityInfo *destAddLocInfo;
//
//@property(nonatomic,strong) NSDictionary *sourceAddGeoInfo;
//@property(nonatomic,strong) NSDictionary *destAddGeoInfo;
//
//@property(nonatomic,strong) DriveRoute *userSelectedRoute;
//@property(nonatomic,strong) NSDictionary *polylineRouteObj;


//-(void)callApiForGeoDataBasedOnLocation:(LocalityInfo *)adddress isForSource:(BOOL)forSource;
//-(void)callAPIForRoutePolyline;

@end
