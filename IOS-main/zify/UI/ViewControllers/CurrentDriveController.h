//
//  CurrentDriveController.h
//  zify
//
//  Created by Anurag S Rathor on 29/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapContainerView.h"
#import "TripDrive.h"
#import "MessageHandler.h"
#import "CurrentRideLocationTracker.h"

@interface CurrentDriveController :UIViewController<CurrentRideLocationTrackerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,weak) IBOutlet MapContainerView *mapContainerView;
@property (nonatomic,weak) IBOutlet UILabel *driveStartTime;
@property (nonatomic,weak) IBOutlet UIView *riderListView;
@property (nonatomic,weak) IBOutlet UIView *riderListShadeView;
@property (nonatomic,weak) IBOutlet UIImageView *expandImageView;
@property (nonatomic,weak) IBOutlet UIView *tapToContactView;
@property (nonatomic,weak) IBOutlet UIView *taptoContactShadeView;
@property (nonatomic,strong) IBOutletCollection(UIImageView) NSArray *riderImages;
@property (nonatomic,strong) IBOutletCollection(UIView) NSArray *riderViews;

@property (nonatomic,weak) IBOutlet UIButton *driveStatusButton;
@property (nonatomic,weak) IBOutlet UITableView *contactTableView;
@property (nonatomic,strong) TripDrive *currentDrive;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (weak, nonatomic) IBOutlet UILabel *lblTapToConnect;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *shareDriveBtn;
@property (nonatomic,weak) IBOutlet UIButton *sosBtn;

+(UINavigationController *)createCurrentDriveNavController;
-(void)showCurrentDriveCoachMarks;
-(IBAction)shareDrive:(id)sender;

-(IBAction)sosBtnTapped:(id)sender;

-(void)startDriveFromSDL:(TripDrive *)driveINfo;
@end
