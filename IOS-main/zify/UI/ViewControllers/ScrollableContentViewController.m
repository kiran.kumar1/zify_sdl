//
//  ScrollableContentViewController.m
//  zify
//
//  Created by Anurag S Rathor on 10/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ScrollableContentViewController.h"

@interface ScrollableContentViewController ()

@end

@implementation ScrollableContentViewController

@synthesize scrollview,activeField;
// Call this method somewhere in your view controller setup code.

-(void) viewDidLoad{
    [super viewDidLoad];
    [self registerForKeyboardNotifications];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    /*float maxHeight = 0;
    
    scrollview.frame = CGRectMake(scrollview.frame.origin.x, scrollview.frame.origin.y, scrollview.frame.size.width, self.view.frame.size.height);
    for(UIView *v in [scrollview subviews]){
        if(v.frame.origin.y + v.frame.size.height > maxHeight)
            maxHeight = v.frame.origin.y + v.frame.size.height;
    }
    
    int deltaHeight = 5;
    if(_scrollDelta){
        deltaHeight = _scrollDelta.intValue;
    }
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, maxHeight + deltaHeight);*/
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollview.contentInset = contentInsets;
    scrollview.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.scrollview.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
       /* CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
        [scrollview setContentOffset:scrollPoint animated:YES];*/
        [scrollview scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollview.contentInset = contentInsets;
    scrollview.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void) dismissKeyboard{
    if(activeField) [activeField resignFirstResponder];
}
@end
