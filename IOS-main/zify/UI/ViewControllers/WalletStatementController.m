//
//  WalletStatementController.m
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "WalletStatementController.h"

#import "ServerInterface.h"
#import "GenericRequest.h"
#import "WalletRechargeStatement.h"
#import "WalletRechargeCell.h"
#import "WalletTransactionStatement.h"
#import "WalletTransactionCell.h"
#import "CurrentLocale.h"
#import "WalletStatementHeaderCell.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

enum StatementType{
    TRANSACTION=0,RECHARGE
};


@implementation WalletStatementController {
    NSInteger selectedStatementType;
    NSArray *rechargeStatementArray;
    NSArray *transactionStatementArray;
    NSMutableArray *statementDatesArray;
    NSMutableDictionary *statementDict;
    NSDateFormatter *dateFormatter;
    NSDateFormatter *dateTimeFormatter;
    CurrentLocale *currentLocale;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    rechargeStatementArray = nil;
    transactionStatementArray = nil;
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter setLocale:locale];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
    currentLocale = [CurrentLocale sharedInstance];
    if([currentLocale isGlobalLocale] && ![currentLocale isGlobalPayment]){
        self.segmentedControl.hidden = YES;
    }
    nodataMsgStr = NSLocalizedString(WS_STATEMENT_LBL_TRANSACTIONS_NOTRANSACTIONS, nil);
    [self initialiseView];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    selectedStatementType = TRANSACTION;
    [self.segmentedControl setSelectedSegmentIndex:TRANSACTION];
    
    [self fetchTransactionStatement];
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

-(void)initialiseView{
    [self.segmentedControl setTitle:NSLocalizedString(VC_WALLETSTATEMENT_TRANSACTION, nil) forSegmentAtIndex:0];
    [self.segmentedControl setTitle:NSLocalizedString(VC_WALLETSTATEMENT_RECHARGE, nil) forSegmentAtIndex:1];
    _tableView.estimatedRowHeight = 40.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    if([currentLocale isGlobalLocale] && ![currentLocale isGlobalPayment]){
        [self.view removeConstraint:_tableViewTopConstraint];
        [self.view removeConstraint:_noTransactionViewTopConstraint];
        _noTransactionView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        NSLayoutConstraint *newTableTopConstraint = [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
        [self.view addConstraint:newTableTopConstraint];
        NSLayoutConstraint *newNoTransactionViewConstraint = [NSLayoutConstraint constraintWithItem:_noTransactionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
        [self.view addConstraint:newNoTransactionViewConstraint];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [statementDatesArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *statementArray = [statementDict objectForKey:statementDatesArray[section]];
    return [statementArray count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        WalletStatementHeaderCell *statementHeaderCell = [tableView dequeueReusableCellWithIdentifier:@"statementHeaderCell"];
        statementHeaderCell.statementDate.text = statementDatesArray[indexPath.section];
        return statementHeaderCell;
    } else{
        NSArray *statementArray = [statementDict objectForKey:statementDatesArray[indexPath.section]];
        if(selectedStatementType == RECHARGE){
            WalletRechargeCell *rechargeCell = [tableView dequeueReusableCellWithIdentifier:@"rechargeCell"];
            WalletRechargeStatement *rechargeStatement = [statementArray objectAtIndex:indexPath.row - 1];
            rechargeCell.transactionId.text = [NSString stringWithFormat:NSLocalizedString(VC_WALLETSTATEMENT_TXNID, @"TXN ID : {RECHARGETXNID}"),rechargeStatement.transactionId];
            NSLog(@"rechargeStatement.transactionAmount is %@", rechargeStatement.transactionAmount);
            rechargeCell.transactionAmount.text = [NSString stringWithFormat:@"%@%@",[currentLocale getCurrencySymbol],rechargeStatement.transactionAmount.stringValue];
            
            if (IS_IPHONE_5) {
                rechargeCell.transactionId.fontSize = 10;
                rechargeCell.transactionAmount.fontSize = 17;
            } else if (IS_IPHONE_6) {
                rechargeCell.transactionId.fontSize = 12;
                rechargeCell.transactionAmount.fontSize = 18;
            } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
                rechargeCell.transactionId.fontSize = 12;
                rechargeCell.transactionAmount.fontSize = 19;
            }
            return rechargeCell;
            
        } else if(selectedStatementType == TRANSACTION){
            WalletTransactionCell *transactionCell = [tableView dequeueReusableCellWithIdentifier:@"transactionCell"];
            WalletTransactionStatement *transactionStatement = [statementArray objectAtIndex:indexPath.row - 1];
            transactionCell.transactionId.text =[NSString stringWithFormat:NSLocalizedString(VC_WALLETSTATEMENT_ID, @"{TRIPTYPE} ID: {TRIPID}"),transactionStatement.travelType.uppercaseString,transactionStatement.transactionId.stringValue];
            transactionCell.zifyPoints.text = [NSString stringWithFormat:@"%@%@",[currentLocale getCurrencySymbol],transactionStatement.zifyPoints];
            NSLog(@"transactionCell.zifyPoints.text is %@", transactionCell.zifyPoints.text);

            transactionCell.transactionType.text = transactionStatement.transactionType.uppercaseString;
            transactionCell.transactionType.layer.cornerRadius = 3.0;
            transactionCell.transactionType.layer.borderWidth = 1.0;
            transactionCell.transactionType.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            if([@"CREDIT" isEqualToString:transactionStatement.transactionType]){
                transactionCell.transactionType.textColor = [UIColor colorWithRed:0.0 green:150.0/255.0 blue:136.0/255.0 alpha:1.0];
            } else{
                transactionCell.transactionType.textColor =[UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
            }
            
            if (IS_IPHONE_5) {
                transactionCell.transactionId.fontSize = 11;
                transactionCell.transactionType.fontSize = 11;
                transactionCell.zifyPoints.fontSize = 18;
            } else if (IS_IPHONE_6) {
                transactionCell.transactionId.fontSize = 12;
                transactionCell.transactionType.fontSize = 11;
                transactionCell.zifyPoints.fontSize = 18;
            } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
                transactionCell.transactionId.fontSize = 13;
                transactionCell.transactionType.fontSize = 12;
                transactionCell.zifyPoints.fontSize = 19;
            }
            
            return transactionCell;
        }

    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

- (IBAction) statementTypeChanged:(id)sender{
    NSInteger selectedIndex = [self.segmentedControl selectedSegmentIndex];
    if(selectedIndex == TRANSACTION){
        _imgNoStmtRecharge.image = [UIImage imageNamed:@"NoStatement"];
        nodataMsgStr = NSLocalizedString(WS_STATEMENT_LBL_TRANSACTIONS_NOTRANSACTIONS, nil);
        [self fetchTransactionStatement];
    } else{
        _imgNoStmtRecharge.image = [UIImage imageNamed:@"NoRecharge"];
        nodataMsgStr = NSLocalizedString(WS_STATEMENT_LBL_RECHARGE_NOTRANSACTIONS, nil);
        [self fetchRechargeStatement];
    }
}

-(IBAction) dismiss:(id)sender{
    if([currentLocale isGlobalLocale]) [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    else [self.navigationController popViewControllerAnimated:NO];
}


-(void) fetchRechargeStatement{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[GenericRequest alloc] initWithRequestType:RECHARGESTATEMENTREQUEST] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    rechargeStatementArray = (NSArray *) response.responseObject;
                    if([rechargeStatementArray count] == 0){
                        [self resetStatementDict];
                        self.noDataLbl.text = nodataMsgStr;
                        self.tableView.hidden = true;
                        self.noTransactionView.hidden = false;
                    } else{
                        [self prepareStatementDictforRecharge];
                        selectedStatementType = RECHARGE;
                        self.tableView.hidden = false;
                        self.noTransactionView.hidden = true;
                    }
                } else{
                    rechargeStatementArray = nil;
                    [self resetStatementDict];
                    self.noTransactionView.hidden = true;
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
                [self.tableView reloadData];
            }];
        }];
    }];
}

-(void)prepareStatementDictforRecharge{
    statementDict = [[NSMutableDictionary alloc] init];
    statementDatesArray = [[NSMutableArray alloc] init];
    [rechargeStatementArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
          WalletRechargeStatement *rechargeStatement = (WalletRechargeStatement *)obj;
        NSString *rechargeDate = [dateFormatter stringFromDate:[dateTimeFormatter dateFromString:rechargeStatement.transactionDate]];
        NSMutableArray *rechargeArray = [statementDict objectForKey:rechargeDate];
        if(rechargeArray == nil){
            rechargeArray = [[NSMutableArray alloc] init];
            [statementDict setObject:rechargeArray forKey:rechargeDate];
        }
        [rechargeArray addObject:rechargeStatement];
        NSString *lastDate = [statementDatesArray lastObject];
        if(!lastDate || ![lastDate isEqualToString:rechargeDate]) [statementDatesArray addObject:rechargeDate];
    }];
}

-(void) fetchTransactionStatement{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[GenericRequest alloc] initWithRequestType:TRANSACTIONSTATEMENTREQUEST] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    transactionStatementArray = (NSArray *) response.responseObject;
                    if([transactionStatementArray count] == 0){
                        [self resetStatementDict];
                        self.noDataLbl.text = nodataMsgStr;
                        self.tableView.hidden = true;
                        self.noTransactionView.hidden = false;
                    } else{
                        [self prepareStatementDictForTransaction];
                        selectedStatementType = TRANSACTION;
                        self.tableView.hidden = false;
                        self.noTransactionView.hidden = true;
                    }
                } else{
                    transactionStatementArray = nil;
                    [self resetStatementDict];
                    self.noTransactionView.hidden = true;
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
                [self.tableView reloadData];
            }];
        }];
    }];
}

-(void)prepareStatementDictForTransaction{
    statementDict = [[NSMutableDictionary alloc] init];
    statementDatesArray = [[NSMutableArray alloc] init];
    [transactionStatementArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        WalletTransactionStatement *transactionStatement = (WalletTransactionStatement *)obj;
        NSString *rechargeDate = [dateFormatter stringFromDate:[dateTimeFormatter dateFromString:transactionStatement.departureTime]];
        NSMutableArray *statementArray = [statementDict objectForKey:rechargeDate];
        if(statementArray == nil){
            statementArray = [[NSMutableArray alloc] init];
            [statementDict setObject:statementArray forKey:rechargeDate];
        }
        [statementArray addObject:transactionStatement];
        NSLog(@"statementArray is %@", statementArray);
        NSString *lastDate = [statementDatesArray lastObject];
        if(!lastDate || ![lastDate isEqualToString:rechargeDate]) [statementDatesArray addObject:rechargeDate];
    }];
}

-(void)resetStatementDict{
    statementDict = nil;
    statementDatesArray = nil;
}

+(UINavigationController *)createWalletStatementNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Wallet" bundle:[NSBundle mainBundle]];
    UINavigationController *walletStatementNavContoller = [storyBoard instantiateViewControllerWithIdentifier:@"walletStatementNavContoller"];
    return walletStatementNavContoller;
}
@end
