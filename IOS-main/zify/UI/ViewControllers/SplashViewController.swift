//
//  SplashViewController.swift
//  zify
//
//  Created by Anurag on 18/02/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit
import Firebase


class SplashViewController: UIViewController {
    
    var imgSplashTemp : UIImage?
    var strTemp : String? = ""
    @IBOutlet weak var imgSplash: UIImageView!
    @IBOutlet weak var lblText: UILabel!
    var visualEffectView: UIVisualEffectView?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.view.alpha = 0.7
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        var blurEffect: UIVisualEffect?
//        blurEffect = UIBlurEffect(style: .dark)
//        visualEffectView = UIVisualEffectView(effect: blurEffect)
//        visualEffectView?.alpha = 0.7
//        visualEffectView?.frame = AppDelegate.getInstance().window.bounds
//        navigationController?.view.addSubview(visualEffectView!)
        
        imgSplash.image = imgSplashTemp
    }
    
    @IBAction func btnTappedClose(_ sender: Any) {
        //self.view.removeFromSuperview()
        //self.dismiss(animated: true, completion: nil)
        self.visualEffectView?.removeFromSuperview()
        self.view.removeFromSuperview()
    }
    
  
    class func showImage(strImageUrl : String) -> UIImage {
        
        let splashBackgroundImageUrl = strImageUrl
        let url = URL(string: splashBackgroundImageUrl)
        print("------url AVIALABLE----\(String(describing: url))--")
        SDWebImageDownloader.shared()?.downloadImage(with: URL(string: splashBackgroundImageUrl), options: SDWebImageDownloaderOptions.ignoreCachedResponse, progress: nil, completed: { (img, data, error, finished) in
            if img != nil {
                print("------IMAGE AVIALABLE------")
                
                DispatchQueue.main.sync {
                    return img
                }
            }
        })
        return UIImage()
    }

}


