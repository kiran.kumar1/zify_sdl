//
//  SearchRideViewController.h
//  zify
//
//  Created by Anurag S Rathor on 09/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserProfile.h"
#import "MenuViewController.h"
#import "LocalityAutoSuggestionController.h"
#import "MessageHandler.h"
#import "DateTimePickerView.h"
#import "CurrentLocationTracker.h"
#import "CurrentRideLocationTracker.h"
#import "MapContainerView.h"
#import "PublishRideController.h"
#import "AppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "EasyTipView.h"

@import GoogleMaps;

@interface SearchRideViewController : UIViewController<MenuDelegate,LocalitySelectionDelegate,DateTimePickerDelegate,CurrentLocationTrackerDelegate,CurrentRideLocationTrackerDelegate,MapViewDelegate,PublishRideDelgate,GMSMapViewDelegate,MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate>
{
    int notificationsCount;
    LocalityInfo *sourceLocalityInfo;
    LocalityInfo *destinationLocalityInfo;
    NSArray *fdjPointsArr;
    NSMutableArray *markersArr;
    NSDate *selectedDateFromPicker;
    NSArray *zenParksArray;
    BOOL isFDJServiceSuccess, isZenParkServiceSuccess;
    NSMutableArray *normalImagesArr,*selectedImagesArr;
    
    UIVisualEffectView *visualEffectView;
}
@property (nonatomic,weak) IBOutlet UIBarButtonItem *rideRequestBarButtonItem;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *btnNotification;
@property (nonatomic,weak) IBOutlet UILabel *sourceLocality;
@property (nonatomic,weak) IBOutlet UILabel *destinationLocality;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet DateTimePickerView *dateTimePickerView;
@property (nonatomic,weak) IBOutlet UIView *sourceInfoView;
@property (nonatomic,weak) IBOutlet UIView *sourceInfoShadowView;
@property (nonatomic,weak) IBOutlet UIView *destinationInfoView;
@property (nonatomic,weak) IBOutlet UIView *destinationInfoDetailsView;
@property (nonatomic,weak) IBOutlet UIView *rideDateAndLocationView;
@property (nonatomic,weak) IBOutlet UIView *rideDateTimeView;
@property (nonatomic,weak) IBOutlet UILabel *rideDateText;
@property (nonatomic,weak) IBOutlet UILabel *rideTimeText;
@property (nonatomic,weak) IBOutlet UIButton *rideDateTimeButton;
@property (nonatomic,weak) IBOutlet UIButton *searchButton;
@property (nonatomic,weak) IBOutlet UIButton *offerButton;
@property (nonatomic,weak) IBOutlet UIButton *heremapsButton;
@property (nonatomic,weak) IBOutlet MapContainerView *mapContainerView;

@property (nonatomic,weak) IBOutlet UIView *viewChooseMode;
@property (nonatomic,weak) IBOutlet UIView *viewDestnTimeDate;
@property (nonatomic,weak) IBOutlet UIView *viewDestnTimeDateTravelPref;
@property (nonatomic,weak) IBOutlet UIView *viewShade;
@property (nonatomic,weak) IBOutlet UIView *viewTravelPref;
@property (nonatomic,weak) IBOutlet UIView *viewFindOrShareRide;
@property (weak, nonatomic) IBOutlet UIButton *btnSwitchMode;
@property (weak, nonatomic) IBOutlet UIButton *btnFindOrShareRide;
@property (weak, nonatomic) IBOutlet UITextField *txtDestination;
@property (weak, nonatomic) IBOutlet UIView *viewTimeDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIButton *btnFirstTimeShowDateTimePicker;
@property (strong, nonatomic) IBOutlet UIView *viewHomeWork;

@property (weak, nonatomic) IBOutlet UIButton *btnCarOwner;
@property (weak, nonatomic) IBOutlet UIButton *btnRider;
@property (weak, nonatomic) IBOutlet UILabel *lblChooseYourMode;
@property (weak, nonatomic) IBOutlet UILabel *lblThisCanBeChangeLater;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnWork;
@property (weak, nonatomic) IBOutlet UIButton *btnBackOnMap;
@property (weak, nonatomic) IBOutlet UILabel *lblWhereAreYouGoing;
@property (nonatomic,weak) IBOutlet UIView *viewSwicthCOntrol;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnBarMenuIcon;

@property(nonatomic,strong) LocalityInfo *sourceLocalityInfo, *destinationLocalityInfo;
-(void)displayproperViewForLocationEnabledOrDisabled;
//- (BOOL)view_FindOrShareRide:(UIView *)view_FindOrShareRide view_TravelPref:(UIView *)view_TravelPref view_HomeWork:(UIView *)view_HomeWork;
-(void)showUpcomingRideWithLat:(NSNumber *)userLat andLong:(NSNumber *)userLong;
@end
