//
//  CreateRideController.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"
#import "GoogleMapView.h"
#import "UserSearchData.h"
#import "zify-Swift.h"

@interface CreateRideController : UIViewController<UITableViewDataSource,UITableViewDataSource, UIGestureRecognizerDelegate>{
    BOOL isLoadingForNextList;
    NSString *startTime;
    NSString *endTime;
    NSNumber *offsetValue;
    NSDictionary *responseDict;
    BOOL isNeedToDisplayLoadCell;
    Userprofile_New *uploadView;
}



@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *fetchingView;
@property (nonatomic,weak) IBOutlet LoadingSpinner *loader;
@property (nonatomic,weak) IBOutlet UIView *noDrivesView;
@property (nonatomic,weak) IBOutlet UILabel *noDrivesLbl;
@property (nonatomic,weak) IBOutlet UIView *tapToReferView;
@property (nonatomic,weak) IBOutlet UIButton *btnSearchAgain;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,strong) UserSearchData *userSearchData;
@property (nonatomic,assign) BOOL isFromDeepLinking;
@property (nonatomic,strong) NSDictionary *rideWithMeDict;
@property (nonatomic,weak) IBOutlet IDUploadCompulseryView *idUploadView;
@property (nonatomic,strong) SearchRideEntity *searchRide;




-(NSString *)getDateAndTime:(NSString *)tritDateTime strType:(NSString *)strType;
+(UINavigationController *)getcreateRideNavController;
-(IBAction)showFreshChat:(id)sender;
@end
