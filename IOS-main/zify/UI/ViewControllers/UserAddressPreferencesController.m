//
//  UserAddressPreferencesController.m
//  zify
//
//  Created by Anurag S Rathor on 12/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserAddressPreferencesController.h"
#import "LocalityInfo.h"
#import "ServerInterface.h"
#import "UniversalAlert.h"
#import "UserProfile.h"
#import "UserPreferences.h"
#import "OnboardFlowHelper.h"
#import "ServerInterface.h"
#import "UserPreferencesRequest.h"
#import "CurrentLocationTracker.h"
#import "UserRideRequest.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "UserOfferRideRequest.h"
#import "UserOfferRideRequest.h"
#import "zify-Swift.h"

enum PreferencePickMode {
    PickingHome,
    PickingOffice,
    PickingStartTime,
    PickingReturnTime
};

enum RouteSelectionType{
    HOMEOFFICE=0,OFFICEHOME
};

@interface UserAddressPreferencesController ()

@end

@implementation UserAddressPreferencesController{
    BOOL isUpdatePreferencesRequest;
    BOOL isViewInitialised;
    LocalityInfo *homeAddressInfo;
    LocalityInfo *officeAddressInfo;
    NSString *selectedStartTime;
    NSString *selectedReturnTime;
    DriveRoute *onwardRoute;
    DriveRoute *returnRoute;
    NSDateFormatter *timeFormatter1;
   // NSDateFormatter *timeFormatter2;
    NSDateFormatter *dateTimeFormatter;
    enum PreferencePickMode addressPickMode;
    enum PreferencePickMode timePickMode;
    NSInteger routeSelectionType;
    UserProfile *currentUser;
    BOOL isMapViewInitialised;
    MapLocationHelper *mapLocationHelper;
    NSArray *travelTypeValues;
    NSDictionary *travelTypeValuesMap;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    isViewInitialised = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self initilaiseView];
    
    if (IS_IPHONE_5) {
        _lblHomeAddTitle.fontSize = 13;
        _lblOfficeAddTitle.fontSize = 13;
        _lblTravelTypeTitle.fontSize = 13;
        _lblStartTimeTitle.fontSize = 13;
        _lblReturnTimeTitle.fontSize = 13;
        _lblSelectRouteTitle.fontSize = 16;
        _lblLetUsKnowYouTakeToTravelTitle.fontSize = 12;
        _lblSetTheTimeYouUsuallyLeaveWorkandHomeTitle.fontSize = 11;
        _btnSubmit.titleLabel.fontSize = 17;
        _homeAddress.fontSize = 13;
        _officeAddress.fontSize = 13;
        _startTime.fontSize = 13;
        _returnTime.fontSize = 13;
    } else if (IS_IPHONE_6 || IS_IPHONE_X) {
        
        _lblHomeAddTitle.fontSize = 14;
        _lblOfficeAddTitle.fontSize = 14;
        _lblTravelTypeTitle.fontSize = 14;
        _lblStartTimeTitle.fontSize = 14;
        _lblReturnTimeTitle.fontSize = 14;
        _lblSelectRouteTitle.fontSize = 17;
        _lblLetUsKnowYouTakeToTravelTitle.fontSize = 12;
        _lblSetTheTimeYouUsuallyLeaveWorkandHomeTitle.fontSize = 12;
        _btnSubmit.titleLabel.fontSize = 18;
        
        _homeAddress.fontSize = 14;
        _officeAddress.fontSize = 14;
        _startTime.fontSize = 14;
        _returnTime.fontSize = 14;
        
    } else if (IS_IPHONE_6_PLUS) {
        _lblHomeAddTitle.fontSize = 14;
        _lblOfficeAddTitle.fontSize = 14;
        _lblTravelTypeTitle.fontSize = 14;
        _lblStartTimeTitle.fontSize = 14;
        _lblReturnTimeTitle.fontSize = 14;
        _lblSelectRouteTitle.fontSize = 18;
        _lblLetUsKnowYouTakeToTravelTitle.fontSize = 13;
        _lblSetTheTimeYouUsuallyLeaveWorkandHomeTitle.fontSize = 13;
        _btnSubmit.titleLabel.fontSize = 20;
        
        _homeAddress.fontSize = 14;
        _officeAddress.fontSize = 14;
        _startTime.fontSize = 14;
        _returnTime.fontSize = 14;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    if(blurView){
        [blurView removeFromSuperview];
        blurView = nil;
    }
    if(displaySuccessView){
        [displaySuccessView removeFromSuperview];
        displaySuccessView = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initilaiseView{
    if(!isViewInitialised){
        isUpdatePreferencesRequest = NO;
        currentUser = [UserProfile getCurrentUser];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        timeFormatter1 = [[NSDateFormatter alloc] init];
        [timeFormatter1 setDateFormat:@"HH:mm:ss"];
        [timeFormatter1 setLocale:locale];
//        timeFormatter2 = [[NSDateFormatter alloc] init];
//        [timeFormatter2 setDateFormat:@"HH:mm"];
//        [timeFormatter2 setLocale:locale];
        dateTimeFormatter = [[NSDateFormatter alloc] init];
        [dateTimeFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
        routeSelectionType = 0;
        isMapViewInitialised = NO;
        mapLocationHelper = [[MapLocationHelper alloc] init];
        mapLocationHelper.mapLocationDelegate = self;
        travelTypeValues = [NSArray arrayWithObjects:NSLocalizedString(VC_USERPREFERENCES_RIDERVAL, nil),NSLocalizedString(VC_USERPREFERENCES_DRIVERVAL, nil),nil];
        travelTypeValuesMap = @{NSLocalizedString(VC_USERPREFERENCES_RIDERVAL,nil):@"RIDER",NSLocalizedString(VC_USERPREFERENCES_DRIVERVAL,nil):@"DRIVER"};
        
        UIImage *editAddressImage = [UIImage imageNamed:@"icn_preferences_route_editimage.png"];
        _homeAddressEditImage.image = [editAddressImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_homeAddressEditImage setTintColor:[UIColor lightGrayColor]];
        _officeAddressEditImage.image = [editAddressImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_officeAddressEditImage setTintColor:[UIColor lightGrayColor]];
        
         UIImage *routeSelectImage = [UIImage imageNamed:@"routeMap.png"];
        _routeSelectImage.image = routeSelectImage;//[routeSelectImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        //[_routeSelectImage setTintColor:[UIColor lightGrayColor]];
        _editRoute.layer.cornerRadius = _editRoute.frame.size.width/2;
        _editRoute.clipsToBounds = YES;
        if(_showPageIndicator){
            _pageControl.currentPage = 1;
        } else{
            _pageControl.hidden = YES;
        }
        _travelType.text = NSLocalizedString(VC_USERPREFERENCES_RIDERVAL, nil);
        [self showRouteSelectionViewForTravelType:NSLocalizedString(VC_USERPREFERENCES_RIDERVAL, nil)];
        [self showRouteSelectPlaceHolder];
        [self.segmentedControl setTitle:NSLocalizedString(VC_USERPREFERENCES_HOMEWORK, nil) forSegmentAtIndex:0];
        [self.segmentedControl setTitle:NSLocalizedString(VC_USERPREFERENCES_WORKHOME, nil) forSegmentAtIndex:1];
        if(!_isUpgradeFlow){
            UIImage *arrowImage = [UIImage imageNamed:@"left_arrow.png"];
            _closePageArrow.image = [arrowImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [_closePageArrow setTintColor:[UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0]];
            _pageTitle.text = NSLocalizedString(VC_USERPREFERENCES_UPDATEPREFERENCES, nil);
            //self.navBar.topItem.title = NSLocalizedString(VC_USERPREFERENCES_UPDATEPREFERENCES, nil);
            self.navigationItem.title = NSLocalizedString(VC_USERPREFERENCES_UPDATEPREFERENCES, nil);
            
            UserPreferences *preferences = currentUser.userPreferences;
            if(preferences){
                homeAddressInfo =[[LocalityInfo alloc] initWithName:preferences.sourceAddress  andAddress:preferences.sourceAddress andCity:preferences.city andState:@"" andCountry:@"" andLatLng:CLLocationCoordinate2DMake(preferences.sourceLatitude.doubleValue, preferences.sourceLongitude.doubleValue)];
                _homeAddress.text = homeAddressInfo.address;
                officeAddressInfo = [[LocalityInfo alloc] initWithName:preferences.destinationAddress andAddress:preferences.destinationAddress andCity:preferences.city andState:@"" andCountry:@"" andLatLng:CLLocationCoordinate2DMake(preferences.destinationLatitude.doubleValue, preferences.destinationLongitude.doubleValue)];
                _officeAddress.text = officeAddressInfo.address;
                
                
                selectedStartTime = preferences.startTime;
               // NSLog(@"start time is %@", selectedStartTime);
                if(selectedStartTime && selectedStartTime.length!=0){
                     _startTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:[timeFormatter1 dateFromString:selectedStartTime]];
                }
                selectedReturnTime =  preferences.returnTime;
                if(selectedReturnTime && selectedReturnTime.length!=0){
                   _returnTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:[timeFormatter1 dateFromString:selectedReturnTime]];
                }
                if([@"DRIVER" isEqualToString:preferences.userMode]){
                    _travelType.text = NSLocalizedString(VC_USERPREFERENCES_DRIVERVAL, nil);
                    [self showRouteSelectionViewForTravelType:NSLocalizedString(VC_USERPREFERENCES_DRIVERVAL, nil)];
                    onwardRoute = [[DriveRoute alloc] initWithSource:homeAddressInfo andDestination:officeAddressInfo andRouteId:preferences.onwardRouteId andPolyline:preferences.onwardPolyline];
                    returnRoute = [[DriveRoute alloc] initWithSource:officeAddressInfo andDestination:homeAddressInfo andRouteId:preferences.returnRouteId andPolyline:preferences.returnPolyline];
                    [self showRouteMap];
                }
                isUpdatePreferencesRequest = YES;
            }
            _remindLaterView.hidden = YES;
        } else{
              _closePageArrow.hidden = YES;
             [self fetchCurrentLocation];
        }
        
        _dateTimePickerView.delegate = self;
        _dateTimePickerView.isTimePicker = YES;
        isViewInitialised = YES;
    }
 }


-(void)fetchCurrentLocation {
    if(![[ServerInterface sharedInstance] isConnectedToInternet]){
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORTITLE, nil) WithMessage:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORMESSAGE, nil)];
        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY,nil) WithAction:^(void *action){
            [self fetchCurrentLocation];
        }];
        [alert showInViewController:self];
    } else if(![[CurrentLocationTracker sharedInstance] isLocationTrackingEnabled]){
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORTITLE, nil) WithMessage:NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORMESSAGE, nil)];
        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY,nil) WithAction:^(void *action){
            [self fetchCurrentLocation];
        }];
        [alert showInViewController:self];
    } else{
        [self updateUserCurrentLocation];
    }
}

-(IBAction) selectHomeAddress:(id)sender{
    addressPickMode = PickingHome;
    [self showAddressPicker];
}

-(IBAction) selectOfficeAddress:(id)sender{
     addressPickMode = PickingOffice;
    [self showAddressPicker];
}

-(IBAction)selectTravelType:(id)sender{
    self.travelTypePickerView.delegate = self;
    self.travelTypePickerView.pickervalues = travelTypeValues;
    self.travelTypePickerView.currentValue = self.travelType.text;
    [self.travelTypePickerView showInView:self.view];
}


-(IBAction) selectStartTime:(id)sender{
    timePickMode = PickingStartTime;
    if(selectedStartTime){
        self.dateTimePickerView.selectedDate = [timeFormatter1 dateFromString:selectedStartTime];
    }
    [self.dateTimePickerView showInView:self.view];
}

-(IBAction) selectEndTime:(id)sender{
    timePickMode = PickingReturnTime;
    if(selectedReturnTime){
        self.dateTimePickerView.selectedDate = [timeFormatter1 dateFromString:selectedReturnTime];
    }
    [self.dateTimePickerView showInView:self.view];
}

- (IBAction)routeSelectionTypeChanged:(id)sender{
    routeSelectionType = [self.segmentedControl selectedSegmentIndex];
    if(routeSelectionType == HOMEOFFICE){
        if(onwardRoute != nil) [self showRouteMap];
        else [self showRouteSelectPlaceHolder];
    } else{
        if(returnRoute != nil) [self showRouteMap];
        else [self showRouteSelectPlaceHolder];
    }
}
- (IBAction) selectRoute:(id)sender{
    if(homeAddressInfo == nil){
        self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_HOMEADDRESSVAL, nil);;
        return;
    }
    if(officeAddressInfo == nil){
        self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_OFFICEADDRESSVAL, nil);
        return;
    }
    self.errorMessage.text = @"";
    LocalityInfo *sourceLocalityInfo, *destinationLocalityInfo;
    if(routeSelectionType == HOMEOFFICE){
        sourceLocalityInfo = homeAddressInfo;
        destinationLocalityInfo = officeAddressInfo;
    } else{
        sourceLocalityInfo = officeAddressInfo;
        destinationLocalityInfo = homeAddressInfo;
    }
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[UserOfferRideRequest alloc] initWithSourceLocality:sourceLocalityInfo andDestinationLocality:destinationLocalityInfo andRideDate:[dateTimeFormatter stringFromDate:[NSDate date]] andSeats:@1 andMerchantId:nil] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    NSArray *driveRoutes = (NSArray *)response.responseObject;
                    if([driveRoutes count] == 0){
                        self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_NOROUTESVAL, nil);
                    } else{
                        UINavigationController *routeSelectNavController = [PublishRideController createPublishRideNavController];
                        PublishRideController *routeSelectController =  (PublishRideController *)[routeSelectNavController topViewController];
                        routeSelectController.publishRideDelegate = self;
                        routeSelectController.routes = driveRoutes;
                        routeSelectController.publishDriveRequest = nil;
                        routeSelectController.isRouteSelectOnly = YES;
                        [self presentViewController:routeSelectNavController animated:YES completion:nil];
                    }
                } else {
                    self.errorMessage.text = error.userInfo[@"message"];
                }
            }];
        }];
    }];
}

-(IBAction) setProfileUpgradeReminder:(id)sender {
    [[OnboardFlowHelper sharedInstance] setReminder:self];
}

-(IBAction) submitPreferences:(id)sender {
    if([self validateUserPreferences]) {
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            UserPreferencesRequest *request;
            if(isUpdatePreferencesRequest){
                request = [[UserPreferencesRequest alloc] initWithWithRequestType:UPDATEPREFERENCES andHomeAddress:homeAddressInfo andOfficeAddress:officeAddressInfo andStartTime:selectedStartTime andReturnTime:selectedReturnTime andOnwardRoute:onwardRoute andReturnRoute:returnRoute andUserMode:[travelTypeValuesMap objectForKey:_travelType.text]];
            } else {
                request = [[UserPreferencesRequest alloc] initWithWithRequestType:ADDPREFERENCES andHomeAddress:homeAddressInfo andOfficeAddress:officeAddressInfo andStartTime:selectedStartTime andReturnTime:selectedReturnTime andOnwardRoute:onwardRoute andReturnRoute:returnRoute andUserMode:[travelTypeValuesMap objectForKey:_travelType.text]];
            }
            [[ServerInterface sharedInstance] getResponse:request withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        if(isUpdatePreferencesRequest == NO) {
                            ///Refer and earn service call for process the referral amount to parent user and child user, where Tp is completed first time
                            [ReferAndEarnController serviceCall_ProcessReferralDataWithEventId:2 strUserType:[travelTypeValuesMap objectForKey:_travelType.text]];
                        }
                        NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
                        [prefs setBool:YES forKey:@"tpUpdated"];
                        [prefs synchronize];
                        [self createViewForSuccessOnServiceCall];
                    } else{
                        self.errorMessage.text = error.userInfo[@"message"];
                    }
                }];
            }];
        }];
    }
}


-(IBAction) dismiss:(id)sender {
    if(blurView){
        [blurView removeFromSuperview];
        blurView = nil;
    }
    if(displaySuccessView){
        [displaySuccessView removeFromSuperview];
        displaySuccessView = nil;
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isForPush = [prefs boolForKey:@"forPush"];
   /* if(isForPush){
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }*/
   // [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];

}

-(void) updateUserCurrentLocation{
    [self.messageHandler showTransparentBlockingLoadViewWithHandler:^{
        [CurrentLocationTracker sharedInstance].currentLocationTrackerDelegate = self;
        [[CurrentLocationTracker sharedInstance] updateCurrentLocation];
    }];
}

-(void)showAddressPicker{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [mapLocationHelper showPickOnMap:[self navigationController]];
    }];
}

#pragma mark - UserAddressPreferencesDelegate methods

-(void)completePreferenceSubmission{
    
    if(_preferencesDelegate) [_preferencesDelegate updateUserAddressPreferences];

    [self.navigationController popViewControllerAnimated:YES];
   /* [self dismissViewControllerAnimated:NO completion:^{
        if(_preferencesDelegate) [_preferencesDelegate updateUserAddressPreferences];
    }];*/
}

#pragma mark - MapLocationHelperDelegate methods

-(void) selectedLocationInfo:(LocalityInfo *)locationInfo andError:(NSError *)error{
    if(error != nil){
        [self.messageHandler dismissBlockingLoadViewWithHandler:^{
            [self.messageHandler showErrorMessage:NSLocalizedString(VC_USERPREFERENCES_LOCATIONMAPPINGERROR, nil)];
        }];
    } else {
        if(locationInfo != nil) {
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(addressPickMode == PickingOffice){
                    officeAddressInfo = locationInfo;
                    _officeAddress.text = locationInfo.address;
                } else{
                    homeAddressInfo = locationInfo;
                    _homeAddress.text = locationInfo.address;
                }
                 onwardRoute = nil;
                 returnRoute = nil;
                 [self showRouteSelectPlaceHolder];
            }];
        } else {
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                
            }];
        }
    }
}

#pragma mark - CustomPickerDelegate methods

-(void) selectedPickerValue:(NSString *)value{
    _travelType.text = value;
    [self showRouteSelectionViewForTravelType:value];
}

#pragma mark - DateTimePickerDelegate methods

-(void) selectedDate:(NSDate *)selectedDate{
    if(timePickMode == PickingStartTime){
         selectedStartTime = [timeFormatter1 stringFromDate:selectedDate];
         _startTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:selectedDate];
    } else{
        selectedReturnTime = [timeFormatter1 stringFromDate:selectedDate];
        _returnTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:selectedDate];
    }
}

-(void) selectedDriveRoute:(DriveRoute *)route{
    if(routeSelectionType == HOMEOFFICE){
        onwardRoute = route;
    } else{
        returnRoute = route;
    }
    [self showRouteMap];
}

-(void) showRouteSelectionViewForTravelType:(NSString *)travelType{
    if([NSLocalizedString(VC_USERPREFERENCES_RIDERVAL, nil) isEqualToString:travelType]){
        _routeSelectionView.hidden = YES;
    } else _routeSelectionView.hidden = NO;
}
 
-(void) showRouteSelectPlaceHolder{
    self.mapViewPlaceHolder.hidden = YES;
    self.routeSelectPlaceHolder.hidden = NO;
}

-(void)showRouteMap {
    self.mapViewPlaceHolder.hidden = NO;
    self.routeSelectPlaceHolder.hidden = YES;
    LocalityInfo *sourceLocalityInfo, *destinationLocalityInfo;
    NSString * overviewPolylinePoints;
    if(routeSelectionType == HOMEOFFICE) {
        sourceLocalityInfo = homeAddressInfo;
        destinationLocalityInfo = officeAddressInfo;
        overviewPolylinePoints = onwardRoute.overviewPolylinePoints;
    } else {
        sourceLocalityInfo = officeAddressInfo;
        destinationLocalityInfo = homeAddressInfo;
        overviewPolylinePoints = returnRoute.overviewPolylinePoints;
    }
    
    [AppDelegate getAppDelegateInstance].homeAddress = homeAddressInfo;
    [AppDelegate getAppDelegateInstance].officeAddress = officeAddressInfo;
    
    if(!isMapViewInitialised) [_mapContainerView createMapView];
    MapView *mapView = _mapContainerView.mapView;
    mapView.sourceCoordinate = sourceLocalityInfo.latLng;
    mapView.destCoordinate = destinationLocalityInfo.latLng;
    mapView.overviewPolylinePoints = overviewPolylinePoints;
    mapView.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0);
    if(!isMapViewInitialised) {
        [mapView drawMap];
        isMapViewInitialised = YES;
    } else {
        [mapView drawMarkers];
        [mapView modifyPathForIndex:0];
    }
}

#pragma mark - CurrentLocationTrackerDelegate methods

-(void) setCurrentLocation:(LocalityInfo *)currentLocation {
    [self.messageHandler dismissTransparentBlockingLoadViewWithHandler:^{
        
    }];
}

-(void) unableToFetchCurrentLocation:(NSError *)error {
    [self.messageHandler dismissTransparentBlockingLoadViewWithHandler:^{
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORTITLE, nil)  WithMessage:error.userInfo[@"message"]];
        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY, nil) WithAction:^(void *action){
            [self fetchCurrentLocation];
        }];
        [alert showInViewController:self];
    }];
}

-(BOOL)validateUserPreferences {
    if(homeAddressInfo == nil) {
        self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_HOMEADDRESSVAL, nil);
        return false;
    }
    if(officeAddressInfo == nil){
        self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_OFFICEADDRESSVAL, nil);
        return false;
    }
    if(selectedStartTime == nil || selectedStartTime.length == 0){
        self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_STARTTIMEVAL, nil);
        return false;
    }
    if(selectedReturnTime == nil || selectedReturnTime.length == 0){
        self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_RETURNTIMEVAL, nil);
        return false;
    }
    if(![NSLocalizedString(VC_USERPREFERENCES_RIDERVAL, nil) isEqualToString:_travelType.text]){
        if(onwardRoute == nil){
            self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_ONWARDROUTEVAL, nil);
            return false;
        }
        if(returnRoute == nil){
            self.errorMessage.text = NSLocalizedString(VC_USERPREFERENCES_RETURNROUTEVAL, nil);
            return false;
        }
    } else {
        onwardRoute = nil;
        returnRoute = nil;
        [self showRouteSelectPlaceHolder];
    }
    self.errorMessage.text = @"";
    return true;
}

+(UserAddressPreferencesController *)createUserAddressPreferencesController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"UserOnboard" bundle:[NSBundle mainBundle]];
    UserAddressPreferencesController *userAddressPreferencesController = [storyBoard instantiateViewControllerWithIdentifier:@"userAddressPreferencesController"];
    return userAddressPreferencesController;
}


-(void)createViewForSuccessOnServiceCall{
    blurView = [[UIView alloc] initWithFrame:[AppDelegate getAppDelegateInstance].window.frame];
    blurView.backgroundColor = [UIColor blackColor];
    blurView.alpha = 0.6;
    [[AppDelegate getAppDelegateInstance].window addSubview:blurView];
    
    
    CGFloat posX = 30;
    CGFloat viewHeight = 300;
    displaySuccessView = [[UIView alloc] initWithFrame:CGRectMake(posX,(blurView.frame.size.height - viewHeight)/2, blurView.frame.size.width - 2*posX, viewHeight)];
    displaySuccessView.backgroundColor = [UIColor whiteColor];
    [[AppDelegate getAppDelegateInstance].window addSubview:displaySuccessView];
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0,0, displaySuccessView.frameWidth, 90)];
    topView.backgroundColor = [UIColor colorWithRed:38.0/255.0 green:98.0/255.0 blue:255.0/255.0 alpha:1.0];
    [displaySuccessView addSubview:topView];
    
    CGFloat imageViewHeight = 50;
    UIImageView* tickImageView = [[UIImageView alloc] initWithFrame:CGRectMake((displaySuccessView.frameWidth - imageViewHeight)/2,-imageViewHeight/2,imageViewHeight,imageViewHeight)];
    tickImageView.image = [UIImage imageNamed:@"icn_account_withdrawl_verified@2x.png"];
    tickImageView.layer.cornerRadius = tickImageView.frameHeight/2;
    [displaySuccessView addSubview:tickImageView];
    
    CGFloat fontSize = 17;
    
    UILabel* label  = [[UILabel alloc] initWithFrame:CGRectMake(0, tickImageView.frameMaxY, displaySuccessView.frameWidth, topView.frameHeight - tickImageView.frameMaxY)];
  
    label.textColor = [UIColor whiteColor];
    label.text = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG, comment: "");
    label.font = [UIFont fontWithName:MediumFont size:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    [displaySuccessView addSubview:label];
    
    
    CGFloat fontSize1 = 14;
    
    UILabel* label1  = [[UILabel alloc] initWithFrame:CGRectMake(15, topView.frameMaxY, label.frameWidth - 30, 50)];
    label1.numberOfLines = 0;
    label1.text = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG1, comment: "");
    label1.font = [UIFont fontWithName:MediumFont size:fontSize1];
    label1.backgroundColor = [UIColor clearColor];
    label1.textAlignment = NSTextAlignmentCenter;
    [displaySuccessView addSubview:label1];

    UILabel* label2  = [[UILabel alloc] initWithFrame:label1.frame];
    label2.frameY = label1.frameMaxY;
    label2.textColor = [UIColor blackColor];
    label2.numberOfLines = 0;
    label2.text = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG2, comment: "");
    label2.font = [UIFont fontWithName:NormalFont size:fontSize1];
    label2.backgroundColor = [UIColor clearColor];
    [displaySuccessView addSubview:label2];
    
    UILabel* label3  = [[UILabel alloc] initWithFrame:label1.frame];
    label3.frameY = label2.frameMaxY;
    label3.textColor = [UIColor lightGrayColor];
    label3.numberOfLines = 0;
    label3.text = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG3, comment: "");
    label3.font = [UIFont fontWithName:NormalFont size:fontSize1];
    label3.backgroundColor = [UIColor clearColor];
    [displaySuccessView addSubview:label3];
    
    
    
    CGFloat btnfontSize = 16;
    CGFloat btnWidth = 130;
    CGFloat btnHeight = 40;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake((displaySuccessView.frameWidth - btnWidth)/2, label3.frameMaxY + 5, btnWidth, btnHeight);
    btn.backgroundColor = topView.backgroundColor;
    
    [btn setTitle:NSLocalizedString(CMON_GENERIC_OK, comment: "") forState:UIControlStateNormal];
    [btn setTitle:NSLocalizedString(CMON_GENERIC_OK, comment: "") forState:UIControlStateSelected];
    [displaySuccessView addSubview:btn];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [btn addTarget:self action:@selector(btnOkTapped:) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont fontWithName:MediumFont size:btnfontSize];
    btn.layer.cornerRadius = btn.frameHeight/2;
}


-(void)btnOkTapped:(UIButton *)sender {
    
    if(blurView){
        [blurView removeFromSuperview];
        blurView = nil;
    }
    if(displaySuccessView){
        [displaySuccessView removeFromSuperview];
        displaySuccessView = nil;
    }
    if(!_isUpgradeFlow){
        [self completePreferenceSubmission];
       // UIButton *submitButton = (UIButton *)sender;
       // [submitButton setTitle:NSLocalizedString(VC_USERPREFERENCES_UPDATED, nil) forState:UIControlStateNormal];
      //  [self performSelector:@selector(completePreferenceSubmission) withObject:self afterDelay:1.0];
    } else{
        [[OnboardFlowHelper sharedInstance] showUserHomeScreen:self];
    }
    
}

@end
