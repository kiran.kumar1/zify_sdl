//
//  CreateRideController.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "CreateRideController.h"
#import "CreateRideCell.h"
#import "SearchRide.h"
#import "DriverDetailEntity.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServerInterface.h"
#import "CreateRideRequest.h"
#import "UIImage+ProportionalFill.h"
#import "CurrentLocale.h"
#import "RouteInfoView.h"
#import "TripsTabController.h"
#import "AppUtilites.h"
#import "ChatManager.h"
#import "UniversalAlert.h"
//#import "UserWalletController.h"
#import "UserProfileDetailView.h"
#import "AppViralityHelper.h"
#import "WSCoachMarksView.h"
#import "UserFavouriteRequest.h"
#import "LocalisationConstants.h"
#import "UserRideRequest.h"
#import "UserOfferRideRequest.h"
#import "TutorialShownDB.h"
#import "AppDelegate.h"
#import "LoadingTableViewCell.h"
#import "SearchRideEntity.h"

#import "SearchResultsCell.h"
#import "zify-Bridging-Header.h"
#import "zify-Swift.h"
#import <FreshchatSDK.h>
#import "LocalisationConstants.h"



@interface CreateRideController ()

@end

@implementation CreateRideController{
    UIImage *profileImagePlaceHolder;
    UIImage *profileLargeImagePlaceHolder;
    UIImage *carImagePlaceHolder;
    NSDateFormatter *dateFormatter;
    //NSDateFormatter *timeFormatter;
    NSDateFormatter *dateTimeFormatter;
  //  BOOL isUserChatConnected;
    CurrentLocale *currentLocale;
    NSMutableArray *searchRides;
    BOOL isViewInitialised;
    NSMutableArray *indexPathsArray;
    BOOL isShownCoaches;
    BOOL isForUpcomingTrips;
    BOOL isForShowingCurrentRide;
    int selectedIndex;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    indexPathsArray = [[NSMutableArray alloc] init];
    isNeedToDisplayLoadCell = true;
    [self initialiseView];
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isFromRideRequest = [prefs boolForKey:@"isFromRideRequest"];
    if(isFromRideRequest){
        [prefs setBool:NO forKey:@"isFromRideRequest"];
        [prefs synchronize];
        UserProfile *userProfile = [UserProfile getCurrentUser];
        UserDocuments *userDocuments = userProfile.userDocuments;
        if([userDocuments.isIdCardDocUploaded intValue]){
            [self callApiForRideRequest:nil];
        }
    }
    
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _isFromDeepLinking = NO;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(_isFromDeepLinking){
        self.navigationItem.title = NSLocalizedString(MS_CREATERIDE_NAV_TITLE_FOR_RIDE_WITH_ME, "");
    }else{
        self.navigationItem.title = NSLocalizedString(MS_CREATERIDE_NAV_TITLE, "");
    }
    @try{
        if(!isViewInitialised){
            [_loader startAnimating];
            if(_isFromDeepLinking){
                
                [self callServiceForParticularDriverFromDeepLink];
            }else{
                [self callServiceForFetchingDataForFirstTime:YES];
            }
        }
    }@catch(NSException *ex){
        NSLog(@"excepton is %@", [ex description]);
    }
}

-(void)callServiceForFetchingDataForFirstTime:(BOOL)isForFirstTime
{
    
    /* if(![self isConnectedToInternet]){
     error =[NSError errorWithDomain:NOINTERNET_ERROR_DOMAIN code:NOINTERNET_ERROR_CODE userInfo:NOINTERNET_ERROR_USER_INFO];
     return;
     }*/
    @try{
        UserRideRequest *request;
        if(isForFirstTime) {
            _userSearchData.rideStartDate = [[AppDelegate getAppDelegateInstance] converDateToISOStandard:_userSearchData.rideStartDate];
            request = [[UserRideRequest alloc] initWithRequestType:SEARCHRIDE andSourceLocality:_userSearchData.sourceLocality andDestinationLocality:_userSearchData.destinationLocality andRideDate:_userSearchData.rideStartDate andSeats:@1 andMerchantId:_userSearchData.merchantId withOffsetValue:offsetValue withStartTime:startTime withEndTime:endTime];
        }else{
            int getNumOfMatches = [[responseDict objectForKey:@"numOfMatches"] intValue];
            if(getNumOfMatches !=  20){
                isNeedToDisplayLoadCell = NO;
                [_tableView reloadData];
                return;
            }else{
                int totalresultSetvalue = [[responseDict objectForKey:@"totalResultSet"] intValue];
                if([offsetValue intValue] >= totalresultSetvalue){
                    isNeedToDisplayLoadCell = NO;
                    [_tableView reloadData];
                    return;
                }
            }
            // startTime = [self increaseLastDepartureTimeByOneMinute:startTime];
            startTime = [[AppDelegate getAppDelegateInstance] converDateToISOStandardForSearch:startTime];
            endTime = [[AppDelegate getAppDelegateInstance] converDateToISOStandardForSearch:endTime];
            request = [[UserRideRequest alloc] initWithRequestType:SEARCHPAGINATEDRIDE andSourceLocality:_userSearchData.sourceLocality andDestinationLocality:_userSearchData.destinationLocality andRideDate:nil andSeats:@1 andMerchantId:_userSearchData.merchantId withOffsetValue:offsetValue withStartTime:startTime withEndTime:endTime];
        }
        [[ServerInterface sharedInstance] getResponse:request withHandler:^(ServerResponse *response, NSError *error){
            [_loader stopAnimating];
            _fetchingView.hidden = YES;
            if(response){
                startTime = [response.responseObject objectForKey:@"searchWindowStart"];
                endTime = [response.responseObject objectForKey:@"searchWindowEnd"];
                offsetValue = [response.responseObject objectForKey:@"offset"];
                responseDict = response.responseObject;
                NSArray *dataDictArr =  [response.responseObject objectForKey:@"drives"];
                NSMutableArray *searchRidesMutableArr = [[NSMutableArray alloc] init];
                for(NSDictionary *objDict in dataDictArr){
                    SearchRideEntity *searchObj = [[SearchRideEntity alloc] initWithDictionary:objDict];
                    [searchRidesMutableArr addObject:searchObj];
                }
                
                if(isForFirstTime){
                    searchRides = [[NSMutableArray alloc] initWithArray:searchRidesMutableArr];
                    NSDictionary *dataDictToMoengage = [[NSDictionary alloc] initWithObjectsAndKeys:_userSearchData.sourceLocality.address,@"source_address",_userSearchData.destinationLocality.address,@"destination_address",_userSearchData.rideStartDate,@"searchDateTime", nil];
                    
                    if(searchRides.count == 0){
                        [MoEngageEventsClass callRideSearchNotFoundEventWithDataDict:dataDictToMoengage];
                    }else{
                        [MoEngageEventsClass callRideSearchResultsFoundEventWithDataDict:dataDictToMoengage];
                    }
                }else{
                    isLoadingForNextList = NO;
                    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                    for(int i = (int)searchRides.count; i< searchRidesMutableArr.count; i++){
                        [indexPaths addObject:[NSString stringWithFormat:@"%d", i]];
                    }
                    [searchRides addObjectsFromArray:searchRidesMutableArr];
                    [self refreshData:indexPaths];
                }
                
                if(searchRides.count > 0){
                    [_tableView reloadData];
                    _tableView.hidden = NO;
                    
                } else{
                    _noDrivesView.hidden = NO;
                }
                //[self showCoachMarks];
            }
            else {
                if(searchRides.count == 0){
                    _noDrivesView.hidden = NO;
                }
                isLoadingForNextList = NO;
                offsetValue = @10000;   // static value given as more than 100 for removing the Loading cell
                [_tableView reloadData];
            }
        }];
        if(isForFirstTime){
            isViewInitialised = YES;
        }
    }@catch(NSException *ex){
        NSLog(@"ex is %@", [ex description]);
    }
    
}
-(void)callServiceForParticularDriverFromDeepLink{
    
    NSLog(@"params from deeplink is %@", self.rideWithMeDict);
  //  isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    NSNumber *driverId = [self.rideWithMeDict objectForKey:@"driverId"];
    NSNumber *driveId = [self.rideWithMeDict objectForKey:@"driveId"];
    
    SearchRideFromDeepLinkRequest *deeplinkRequest = [[SearchRideFromDeepLinkRequest alloc] initWithDriverId:driverId withDriveId:driveId];
    
    [[ServerInterface sharedInstance] getResponse:deeplinkRequest withHandler:^(ServerResponse *response, NSError *error){
        [_loader stopAnimating];
        _fetchingView.hidden = YES;
        if(response){
            responseDict = response.responseObject;
            NSDictionary *dataDict =  [response.responseObject objectForKey:@"drive"];
            NSMutableArray *searchRidesMutableArr = [[NSMutableArray alloc] init];
            SearchRideEntity *searchObj = [[SearchRideEntity alloc] initWithDictionary:dataDict];
            [searchRidesMutableArr addObject:searchObj];
            searchRides = [[NSMutableArray alloc] initWithArray:searchRidesMutableArr];
            if(searchRides.count > 0){
                _tableView.hidden = NO;
            } else{
                _noDrivesView.hidden = NO;
            }
            [_tableView reloadData];
        }
        else {
            NSLog(@"error is %d", (int)error.code);
            int statusCode = (int)error.code;
            _tableView.hidden = YES;
            _noDrivesView.hidden = NO;
            [self displayViewBasedOnTheStatusCode:statusCode];
        }
    }];
    isViewInitialised = YES;
}

-(void)displayViewBasedOnTheStatusCode:(int)statusCode{
    self.btnSearchAgain.hidden = YES;
    NSString *errorMsg = @"";
    if(statusCode == -2){
        errorMsg = NSLocalizedString(no_drive_exists, nil);
    }else if(statusCode == -300){
        errorMsg = NSLocalizedString(rwm_seat_full, nil);
    }else if(statusCode == -301){
        _btnSearchAgain.hidden = YES;
        errorMsg = NSLocalizedString(rwm_drive_has_expired, nil);
    }else if(statusCode == -302){
        // move to upcoming screen
        isForUpcomingTrips = YES;
        errorMsg = NSLocalizedString(rwm_already_requested, nil);
        _btnSearchAgain.hidden = NO;
        NSString *btnTitle = NSLocalizedString(SIDE_MENU_NEW_UPCOMING_TRIPS, nil);
        [_btnSearchAgain setTitle:btnTitle forState:UIControlStateNormal];
        [_btnSearchAgain setTitle:btnTitle forState:UIControlStateSelected];
        
    }else if(statusCode == -303){
        /// move to current ride screen
        _noDrivesView.hidden = YES;
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        [pref setBool:YES forKey:@"isNeedToShowCurrentRide"];
        [pref synchronize];
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }else if(statusCode == -304){
        errorMsg = NSLocalizedString(rwm_drive_started_but_rider_absent, nil);
    }
    
    _noDrivesLbl.text = errorMsg;
}

-(NSString *)increaseLastDepartureTimeByOneMinute:(NSString *)time{
    //NSString * lastDepTime = @"Feb 1, 2018 6:30:53 PM";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSDate *lastDeptDate = [dateFormatter dateFromString:time];
    NSDate *datePlusOneMinute = [lastDeptDate dateByAddingTimeInterval:60];
    NSString *updateTimeStr = [dateFormatter stringFromDate:datePlusOneMinute];
    if(updateTimeStr != nil){
        time = updateTimeStr;
    }
    return time;
}
-(void)initialiseView{
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    profileLargeImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_large_image.png"];
    carImagePlaceHolder = [UIImage imageNamed:@"placeholder_car_image.png"];
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter setLocale:locale];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yy"];
    //    timeFormatter = [[NSDateFormatter alloc] init];
    //    [timeFormatter setDateFormat:@"HH:mm"];
    //    [timeFormatter setLocale:locale];
 //   isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    currentLocale = [CurrentLocale sharedInstance];
    _tableView.estimatedRowHeight = 288;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.hidden = YES;
    _noDrivesView.hidden = YES;
    _fetchingView.hidden =  NO;
    isViewInitialised = NO;
    if(currentLocale.isGlobalLocale) _tapToReferView.hidden = true;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(_isFromDeepLinking){
        return searchRides.count;
    }
    if(searchRides.count == 0 || !isNeedToDisplayLoadCell)
        return searchRides.count;
    return searchRides.count + 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    @try{
        static NSString *cellLoadingIdentifier = @"Loading";
        
        if (indexPath.section < searchRides.count) {
            SearchRideEntity *ride = [searchRides objectAtIndex:indexPath.section];
            DriverDetailEntity *driverDetail = ride.driverDetail;
            
            
            
            NSString *cellidentifier = @"SearchResultsNewCell";
            SearchResultsNewCell *cellSearchResults = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
            if(cellSearchResults == nil){
                cellSearchResults = (SearchResultsNewCell *)[[SearchResultsNewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
            }
            
            //[cellSearchResults.contentBgView setShadow:[UIColor darkGrayColor] shadowRadius:2.0f cornerRadius:4];
            
            [cellSearchResults.contentBgView showShadow:cellSearchResults.contentBgView shadowColor:[UIColor darkGrayColor] offSet:CGSizeMake(1, 1) shadowRadius:2];
            
            //[self addDropShadowForView:cellSearchResults.contentBgView shadowRadius:4];
            
            cellSearchResults.lblTime.text = [self getDateAndTime:ride.departureTime strType:@"Time"];
            cellSearchResults.lblDate.text = [self getDateAndTime:ride.departureTime strType:@"Date"];
            
            cellSearchResults.lblSource.text = [NSString stringWithFormat:@"%@, %@",ride.srcAdd,ride.city];
            cellSearchResults.lblDestination.text = [NSString stringWithFormat:@"%@, %@",ride.destAdd,ride.destCity];
            
            cellSearchResults.lblPrice.text = [[AppDelegate getAppDelegateInstance] displayTripAmountInCommaSeparator:ride.amountPerSeat];
            
            
            [cellSearchResults.imgDriverProfile sd_setImageWithURL:[NSURL URLWithString:driverDetail.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL) {
            }];
            
            [cellSearchResults.btnProfilePic addTarget:self action:@selector(showDriverProfile:) forControlEvents:UIControlEventTouchUpInside];
            
            cellSearchResults.lblName.text = [[NSString stringWithFormat:@"%@ %@",driverDetail.firstName,driverDetail.lastName]capitalizedString];
            
            if (driverDetail.companyName == nil || [driverDetail.companyName isEqualToString:@""]) {
                cellSearchResults.lblWorks.text = [NSString stringWithFormat:NSLocalizedString(UPDX_USERPROFILE_LBL_WORKSAT, nil)];//@"Works at :"];
            } else {
                cellSearchResults.lblWorks.text = driverDetail.companyName;  // [NSString stringWithFormat:@"%@ %@", NSLocalizedString(UPDX_USERPROFILE_LBL_WORKSAT, nil), driverDetail.companyName];
            }
            
            if([@"VERIFIED" isEqualToString:driverDetail.driverStatus]){
                cellSearchResults.imgVerified.hidden = NO;
            } else {
                cellSearchResults.imgVerified.hidden = YES;
            }
            
//            if (driverDetail.chatProfile.chatUserId) {
//                cellSearchResults.btnRequest.hidden = YES;
//            } else {
//                cellSearchResults.btnRequest.hidden = NO;
//            }
            
            [cellSearchResults.btnRequest addTarget:self action:@selector(createRideRequest:)forControlEvents:UIControlEventTouchUpInside];
            [cellSearchResults.btnRequest setTitle:NSLocalizedString(MS_CREATERIDE_BTN_REQUEST, nil) forState:UIControlStateNormal];
            
            if(indexPath.section == 0 && indexPath.row == 0){
                if(!isShownCoaches){
                    isShownCoaches = YES;
                    [self showCoachMarks:cellSearchResults];
                }
            }
            cellSearchResults.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            
            
                cellSearchResults.lblTime.fontSize = 17;
                cellSearchResults.lblDate.fontSize = 12;
                cellSearchResults.lblSource.fontSize = 14;
                cellSearchResults.lblDestination.fontSize = 14;
                cellSearchResults.lblPrice.fontSize = 17;
                cellSearchResults.lblName.fontSize = 12;
                cellSearchResults.lblWorks.fontSize = 11;
                cellSearchResults.btnRequest.titleLabel.fontSize = 13;
            if (IS_IPHONE_6) {
                cellSearchResults.lblTime.fontSize = 20;
                cellSearchResults.lblDate.fontSize = 13;
                cellSearchResults.lblSource.fontSize = 14;
                cellSearchResults.lblDestination.fontSize = 14;
                cellSearchResults.lblPrice.fontSize = 20;
                cellSearchResults.lblName.fontSize = 14;
                cellSearchResults.lblWorks.fontSize = 12;
                cellSearchResults.btnRequest.titleLabel.fontSize = 14;
            } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
                cellSearchResults.lblTime.fontSize = 20;
                cellSearchResults.lblDate.fontSize = 13;
                cellSearchResults.lblSource.fontSize = 15;
                cellSearchResults.lblDestination.fontSize = 15;
                cellSearchResults.lblPrice.fontSize = 20;
                cellSearchResults.lblName.fontSize = 15;
                cellSearchResults.lblWorks.fontSize = 12;
                cellSearchResults.btnRequest.titleLabel.fontSize = 16;
            }
            
            return cellSearchResults;
            
            /*
             NSString *cellidentifier = @"SearchResultsCell";
             SearchResultsCell *cellSearchResults = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
             if(cellSearchResults == nil){
             cellSearchResults = (SearchResultsCell *)[[SearchResultsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
             }
             SearchRideEntity *ride = [searchRides objectAtIndex:indexPath.section];
             DriverDetailEntity *driverDetail = ride.driverDetail;
             
             [self addDropShadowForView:cellSearchResults.viewMain shadowRadius:2];
             [self addDropShadowForView:cellSearchResults.viewDetails shadowRadius:1];
             
             cellSearchResults.selectionStyle = UITableViewCellSelectionStyleNone;
             cellSearchResults.rideWithMeView.hidden = YES;
             
             if(_isFromDeepLinking){
             cellSearchResults.rideWithMeView.hidden = NO;
             }
             cellSearchResults.backgroundColor = [UIColor clearColor];
             cellSearchResults.backgroundView = nil;
             cellSearchResults.backgroundView.backgroundColor = [UIColor whiteColor];
             cellSearchResults.selectionStyle = UITableViewCellSelectionStyleNone;
             cellSearchResults.viewMain.backgroundColor = [UIColor whiteColor];
             cellSearchResults.viewDetails.backgroundColor = [UIColor whiteColor];
             
             cellSearchResults.lblDriverName.text = [[NSString stringWithFormat:@"%@ %@",driverDetail.firstName,driverDetail.lastName]capitalizedString];
             
             cellSearchResults.lblPriceNumber.text = [[AppDelegate getAppDelegateInstance] displayTripAmountInCommaSeparator:ride.amountPerSeat];
             
             cellSearchResults.lblSourceAddress.text = [NSString stringWithFormat:@"%@, %@",ride.srcAdd,ride.city];
             cellSearchResults.lblDestinationAddress.text = [NSString stringWithFormat:@"%@, %@",ride.destAdd,ride.destCity];
             
             [cellSearchResults.imgDriverProfile sd_setImageWithURL:[NSURL URLWithString:driverDetail.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL) {
             }];
             
             [cellSearchResults.btnProfileDetailView addTarget:self action:@selector(showDriverProfile:) forControlEvents:UIControlEventTouchUpInside];
             
             NSString* pickupDeltaString = [NSString stringWithFormat:@"%.02f", ride.pickupDelta.floatValue];
             
             float pickupDeltaInt =  [pickupDeltaString floatValue];
             if(pickupDeltaInt < 1) {
             pickupDeltaInt = pickupDeltaInt * 1000;
             cellSearchResults.lblPickupDelta.text = [NSString stringWithFormat:@"%d %@",(int)pickupDeltaInt, [currentLocale getDistanceSubUnit]];
             } else {
             cellSearchResults.lblPickupDelta.text = [NSString stringWithFormat:@"%@ %@",ride.pickupDelta, [currentLocale getDistanceUnit]];
             }
             
             NSString* dropDeltaIntString = [NSString stringWithFormat:@"%.02f", ride.dropDelta.floatValue];
             float dropDeltaInt = [dropDeltaIntString floatValue];
             if(dropDeltaInt < 1){
             dropDeltaInt = dropDeltaInt * 1000;
             cellSearchResults.lblDropDelta.text = [NSString stringWithFormat:@"%d %@",(int)dropDeltaInt, [currentLocale getDistanceSubUnit] ];
             } else {
             cellSearchResults.lblDropDelta.text = [NSString stringWithFormat:@"%@ %@",ride.dropDelta, [currentLocale getDistanceUnit]];
             }
             [cellSearchResults.btnRouteMap addTarget:self action:@selector(showRouteMap:) forControlEvents:UIControlEventTouchUpInside];
             [cellSearchResults.btnRequest addTarget:self action:@selector(createRideRequest:)forControlEvents:UIControlEventTouchUpInside];
             [cellSearchResults.btnRequest setTitle:NSLocalizedString(MS_CREATERIDE_BTN_REQUEST, nil) forState:UIControlStateNormal];
             
             [cellSearchResults.btnRequest1 addTarget:self action:@selector(createRideRequest:)forControlEvents:UIControlEventTouchUpInside];
             [cellSearchResults.btnRequest1 setTitle:NSLocalizedString(MS_CREATERIDE_BTN_REQUEST, nil) forState:UIControlStateNormal];
             
             [cellSearchResults.btnMessage addTarget:self action:@selector(showChat:) forControlEvents:UIControlEventTouchUpInside];
             [cellSearchResults.btnMessage setTitle:NSLocalizedString(CTS_CURRENTRIDE_LBL_MESSAGE, nil) forState:UIControlStateNormal];
             [cellSearchResults.btnCarDetailView addTarget:self action:@selector(showCarImage:) forControlEvents:UIControlEventTouchUpInside];
             NSString* distanceStr = [NSString stringWithFormat:@"%.02f", ride.distance.floatValue];
             
             cellSearchResults.lblTravelDistance.text = [NSString stringWithFormat:@"%@ %@", distanceStr, [currentLocale getDistanceUnit]];
             cellSearchResults.rideWithMeDistanceLbl.text = [NSString stringWithFormat:@"%@ %@", distanceStr, [currentLocale getDistanceUnit]];
             cellSearchResults.rideWithMeDistanceLbl.font = cellSearchResults.lblTravelDistance.font;
             
             if (driverDetail.companyName == nil || [driverDetail.companyName isEqualToString:@""]) {
             cellSearchResults.lblWorksAt.text = [NSString stringWithFormat:NSLocalizedString(UPDX_USERPROFILE_LBL_WORKSAT, nil)];//@"Works at :"];
             } else {
             cellSearchResults.lblWorksAt.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(UPDX_USERPROFILE_LBL_WORKSAT, nil), driverDetail.companyName];
             }
             
             cellSearchResults.lblTime.text = [self getDateAndTime:ride.departureTime strType:@"Time"];
             cellSearchResults.lblDate.text = [self getDateAndTime:ride.departureTime strType:@"Date"];
             
             if([@"VERIFIED" isEqualToString:driverDetail.driverStatus]){
             cellSearchResults.viewVerified.hidden = NO;
             cellSearchResults.lblVerified.text = NSLocalizedString(AS_WITHDRAW_LBL_VERIFIED, nil);
             } else {
             cellSearchResults.viewVerified.hidden = YES;
             }
             
             if (driverDetail.chatProfile.chatUserId) {
             cellSearchResults.btnRequest.hidden = YES;
             } else {
             cellSearchResults.btnRequest.hidden = NO;
             }
             if(indexPath.section == 0 && indexPath.row == 0){
             if(!isShownCoaches){
             isShownCoaches = YES;
             [self showCoachMarks:cellSearchResults];
             }
             }
             return cellSearchResults;
             */
            
            
            
        }else {
            LoadingTableViewCell *cell = (LoadingTableViewCell *)[[LoadingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellLoadingIdentifier];
            [cell.activityIndicatorView startAnimating];
            if(!isLoadingForNextList){
                isLoadingForNextList = YES;
                [self fetchMoreData];
            }
            
            return cell;
        }
    }@catch(NSException *ex){
        NSLog(@"exception is %@", [ex description]);
    }
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section >= searchRides.count){
        return 44;
    }/*else if(_isFromDeepLinking){
      return 280;
      }*/
    return 255;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //  if(section == 0) return 0.0f;
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    //let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as LoginViewController
    //self.navigationController?.pushViewController(secondViewController, animated: true)
    
//    CreateRideMapController *createRideMapCtrl = [[CreateRideMapController alloc]initWithNibName:@"CreateRideMapController" bundle:nil];
//    [self.navigationController pushViewController:createRideMapCtrl animated:TRUE];
    
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    //CreateRideMapController* createRideMapCtrl = [storyboard instantiateViewControllerWithIdentifier:@"CreateRideMapController"];
    //createRideMapCtrl.sharedInstance.searchRidesObjects = searchRides;
    
    selectedIndex = indexPath.section;
    [self performSegueWithIdentifier:@"showCreateRideMap" sender:self];

    
    //[createRideMapCtrl.sharedInstance].searchRidesObject = searchRides;
    //[CreateRideMapController sharedInstance].searchRidesObjects = searchRides;
    //UINavigationController *accountNavController = [storyboard instantiateViewControllerWithIdentifier:@"createRideMapNavController"];
    
//    controller.dictResponseObj = responseDict;
//    controller.iValueResutsObj = indexPath.row;
    //controller.searchRidesObjects = searchRides;
    //[self.navigationController presentViewController:[CreateRideMapController createRideMapNavController] animated:true completion:nil];
    
    
   
    
    //[self.navigationController pushViewController:[CreateRideMapController createRideMapNavController] animated:YES];
    
}

#pragma mark - Actions

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    CreateRideMapController *createRideMapCtrl =  (CreateRideMapController *)[(UINavigationController *)segue.destinationViewController topViewController];
    createRideMapCtrl.searchRidesObj = searchRides;
    createRideMapCtrl.selectedIndex = selectedIndex;
    createRideMapCtrl.userSearchData = self.userSearchData;
    
}

- (IBAction)btnTappedSearchAgain:(id)sender {
    if(isForUpcomingTrips){
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        [pref setBool:YES forKey:@"isNeedToShowUpcomingTrips"];
        [pref synchronize];
    }
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction) dismiss:(id)sender{
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

-(void) selectFavourite:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRideEntity *searchRide = [searchRides objectAtIndex:indexPath.section];
    NSNumber *subscribeFlag = searchRide.isFavDrive.intValue ? @0 : @1;
    
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[UserFavouriteRequest alloc] initWithRequestType:SUBSCRIBEFAVDRIVER andSearchRide:searchRide andSourceLocality:_userSearchData.sourceLocality andDestLocality:_userSearchData. destinationLocality andSubscibeFlag:subscribeFlag] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [self.messageHandler showSuccessMessage:response.message];
                    if(subscribeFlag.intValue){
                        searchRide.isFavDrive = @1;
                        [sender setSelected:YES];
                    } else{
                        searchRide.isFavDrive = @0;
                        [sender setSelected:NO];
                    }
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void) showDriverProfile:(UIButton *)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRideEntity *ride = [searchRides objectAtIndex:indexPath.section];
    DriverDetailEntity *driverDetail = ride.driverDetail;
    //[UserProfileDetailView showUserProfileOnView:self.navigationController.view WithDriverDetail:driverDetail];
    
    NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"IDCardViews" owner:self options:nil];
    
    for (NSArray *object in viewArray) {
        if ([object isKindOfClass:[Userprofile_New class]]) {
            uploadView = (Userprofile_New *)object;
            break;
        }
    }
    
    if (uploadView) {
        [[AppDelegate.getAppDelegateInstance window]addSubview:uploadView];
    }
    uploadView.translatesAutoresizingMaskIntoConstraints = false;
    [uploadView setDataForViewWithSearchEntity:ride];
    NSDictionary *viewsDictionary = @{@"uploadView":uploadView};
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[uploadView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[uploadView]|" options:0 metrics:nil views:viewsDictionary];
    [[AppDelegate.getAppDelegateInstance window]addConstraints:horizontalConsts];
    [[AppDelegate.getAppDelegateInstance window]addConstraints:verticalConsts];
    
}


-(void) showRouteMap:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRideEntity *searchRide = [searchRides objectAtIndex:indexPath.section];
    [RouteInfoView showRouteInfoOnView:self.navigationController.view WithSearchRide:searchRide];
}

-(void) showChat:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRideEntity *ride = [searchRides objectAtIndex:indexPath.section];
    DriverDetailEntity *driverDetail = ride.driverDetail;
    if(!driverDetail.chatProfile.chatUserId){
        return;
    }
    UserContactDetail *detail = [[UserContactDetail alloc] initWithChatIdentifier:driverDetail.chatProfile.chatUserId andCallIdentifier:nil andFirstName:driverDetail.firstName andLastName:driverDetail.lastName andImageUrl:driverDetail.profileImgUrl];
    UINavigationController *chatNavController = [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    chatUserListContoller.receivingUser = detail;
    [self presentViewController:chatNavController animated:YES completion:nil];
}

-(void) showCarImage:(UIButton *)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    SearchRideEntity *ride = [searchRides objectAtIndex:indexPath.section];
    DriverDetailEntity *driverDetail = ride.driverDetail;
    
    if([driverDetail.vehicleImgUrl isEqualToString:@""] || driverDetail.vehicleImgUrl == nil) {
        //[_messageHandler showErrorMessage:@"Car image not available"];
        [UIAlertController showErrorWithMessage:NSLocalizedString(CAR_IMAGE_NOT_AVAILABLE, nil) inViewController:self];
    } else {
        [ShowImageView setImageViewWithMainView:self.navigationController.view withCarDetails:driverDetail];
    }
    
    //    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:NSBundle.mainBundle];
    //    ShowImageController *showImageVC = [mainStoryBoard instantiateViewControllerWithIdentifier:@"ShowImageController"];
    //    showImageVC.strImageUrl = driverDetail.vehicleImgUrl;
    //    [self presentViewController:showImageVC animated:true completion:nil];
    
}

-(void) createRideRequest:(UIButton *)sender {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    self.searchRide = [searchRides objectAtIndex:indexPath.section];
    
    NSDictionary *dataDictToMoengage = [[NSDictionary alloc] initWithObjectsAndKeys:self.searchRide.driverDetail.firstName,@"CarOwner_Firstname",self.searchRide.driverDetail.lastName,@"CarOwner_Lastname",_userSearchData.sourceLocality.address,@"source_address",_userSearchData.destinationLocality.address ,@"destination_address",self.searchRide.departureTime,@"trip_Time", nil];
    [MoEngageEventsClass callPreRideRequestEventWithDataDict:dataDictToMoengage];
    UserProfile *userProfile = [UserProfile getCurrentUser];
    UserDocuments *userDocuments = userProfile.userDocuments;
    
    // modified on 6-Aug-19
    //    if(![userDocuments.isIdCardDocUploaded intValue]){
    //        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //        [prefs setBool:YES forKey:@"isFromRideRequest"];
    //        [prefs synchronize];
    //        [[AppDelegate getAppDelegateInstance] showViewForIdCardUpload];
    //        return;
    //    }
    sender.userInteractionEnabled = FALSE;
    [self callApiForRideRequest:sender];
}

-(void)callApiForRideRequest:(UIButton *)sender{
    
    if(self.searchRide == nil){
        return;
    }
    
    // [[ServerInterface sharedInstance] getResponse:[[CreateRideRequest alloc] initWithSearchRideEntity:self.searchRide] withHandler:^(ServerResponse *response, NSError *error){
    
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[CreateRideRequest alloc] initWithSearchRideEntity:self.searchRide withSearchData:self.userSearchData] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    if(response.messageCode.intValue== -30){
                        NSArray *extraObjDict = [[response.responseObject objectForKey:@"statusInfo"]  objectForKey:@"extraParams"];
                        NSString *currenyType = @"";
                        NSString *amountValue = @"";
                        if(extraObjDict.count >= 2){
                            currenyType = [extraObjDict objectAtIndex:0];
                            amountValue = [extraObjDict objectAtIndex:1];
                        }
                        NSString *message = [NSString stringWithFormat:NSLocalizedString(insufficient_recharge_error, nil),currenyType,amountValue];
                        
                        NSDictionary *dataDictToMoengage = [[NSDictionary alloc] initWithObjectsAndKeys:self.searchRide.driverDetail.firstName,@"CarOwner_Firstname",self.searchRide.driverDetail.lastName,@"CarOwner_Lastname",_userSearchData.sourceLocality.address,@"source_address", _userSearchData.destinationLocality.address ,@"destination_address",self.searchRide.departureTime,@"trip_Time", currenyType, @"currency", amountValue,@"amount", nil];
                        [MoEngageEventsClass callRideRequestLowBalanceEventWithDataDict:dataDictToMoengage];
                        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_CREATERIDE_RECHARGENOW, nil) WithMessage:message];
                        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"toAddMoney"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            UINavigationController *walletNavController = (UINavigationController *) [AddMoneyController createUserWalletNavController];
                            WalletController *walletController = (WalletController *)walletNavController.topViewController;
                            walletController.rechargeAmount = amountValue;
                            [self presentViewController:walletNavController animated:YES completion:nil];
                        }];
                        [alert showInViewController:self];
                        sender.userInteractionEnabled = TRUE;
                    } else{
                        NSDictionary *dataDictToMoengage = [[NSDictionary alloc] initWithObjectsAndKeys:self.searchRide.driverDetail.firstName,@"CarOwner_Firstname",self.searchRide.driverDetail.lastName,@"CarOwner_Lastname",_userSearchData.sourceLocality.address,@"source_address", _userSearchData.destinationLocality.address ,@"destination_address",self.searchRide.departureTime,@"trip_Time", nil];
                        [MoEngageEventsClass callRideRequestEventWithDataDict:dataDictToMoengage];
                        [UIAlertController showErrorWithMessage:response.message inViewController:self];
                        //  [self.messageHandler showSuccessMessage:response.message];
                        [self performSelector:@selector(showTrips) withObject:self afterDelay:1.0];
                    }
                } else{
                    NSLog(@"error.userInfo is %@",error.userInfo);
                    [UIAlertController showErrorWithMessage:error.userInfo[@"message"] inViewController:self];
                    // [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    sender.userInteractionEnabled = TRUE;
                }
            }];
        }];
    }];
    
}

-(IBAction) showReferAndEarn:(UIButton *)sender
{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [AppViralityHelper getCamapaignDetails:^(NSDictionary* campaignDetails){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                [self.navigationController dismissViewControllerAnimated:NO completion:^{
                    [AppViralityHelper showGrowthHackOnController:[AppUtilites applicationVisibleViewController] withDetails:campaignDetails];
                }];
            }];
        }];
    }];
}

-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}

-(void)showTrips{
    
    
    UIViewController *rootController = [AppDelegate getAppDelegateInstance].window.rootViewController;
    if( rootController.presentedViewController){
        
        [rootController dismissViewControllerAnimated:NO completion:^{
            [[AppUtilites applicationVisibleViewController] presentViewController:[TripsTabController createTripsTabNavController] animated:YES completion:nil];
        }];
    }
    
    
    //  [self.navigationController dismissViewControllerAnimated:NO completion:^{
    //  }];
}

-(void)showCoachMarks:(SearchResultsCell *)cellSearchResults {
    return;
    TutorialShownDB *obj = [[AppDelegate getAppDelegateInstance] getTutorialsDBObject];
    obj.isInCreateRideShown = @"0";
    if([obj.isInCreateRideShown isEqualToString:@"0"])
    {
        if(searchRides.count != 0) {
            //CreateRideCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            NSArray *coachMarks;
            if(!cellSearchResults.btnRequest.hidden) {
                coachMarks = @[
                               @{@"rect": [NSValue valueWithCGRect:(CGRect){[self getPointOfAViewFromItsSuperview:cellSearchResults.imgDriverProfile withCell:cellSearchResults],{cellSearchResults.imgDriverProfile.frame.size.width,cellSearchResults.imgDriverProfile.frame.size.height}}],
                                 @"caption": NSLocalizedString(VC_CREATERIDE_USERPROFILETUT, nil),
                                 @"shape": @"square"
                                 },
                               @{@"rect": [NSValue valueWithCGRect:(CGRect){[self getPointOfAViewFromItsSuperview:cellSearchResults.btnRouteMap withCell:cellSearchResults],{cellSearchResults.btnRouteMap.frame.size.width,cellSearchResults.btnRouteMap.frame.size.height}}],
                                 @"caption": NSLocalizedString(VC_CREATERIDE_VIEWROUTETUT, nil),
                                 @"shape": @"square"
                                 },
                               @{@"rect": [NSValue valueWithCGRect:(CGRect){[self getPointOfAViewFromItsSuperview:cellSearchResults.btnRequest withCell:cellSearchResults],{cellSearchResults.btnRequest.frame.size.width,cellSearchResults.btnRequest.frame.size.height}}],
                                 @"caption": NSLocalizedString(VC_CREATERIDE_REQUESTTUT, nil),
                                 @"shape": @"square"
                                 }
                               ];
            } else {
                coachMarks = @[
                               @{@"rect": [NSValue valueWithCGRect:(CGRect){[self getPointOfAViewFromItsSuperview:cellSearchResults.imgDriverProfile withCell:cellSearchResults],{cellSearchResults.imgDriverProfile.frame.size.width,cellSearchResults.imgDriverProfile.frame.size.height}}],
                                 @"caption": NSLocalizedString(VC_CREATERIDE_USERPROFILETUT, nil),
                                 @"shape": @"square"
                                 },
                               @{@"rect": [NSValue valueWithCGRect:(CGRect){[self getPointOfAViewFromItsSuperview:cellSearchResults.btnRouteMap withCell:cellSearchResults],{cellSearchResults.btnRouteMap.frame.size.width,cellSearchResults.btnRouteMap.frame.size.height}}],
                                 @"caption": NSLocalizedString(VC_CREATERIDE_VIEWROUTETUT, nil),
                                 @"shape": @"square"
                                 },
                               @{@"rect": [NSValue valueWithCGRect:(CGRect){[self getPointOfAViewFromItsSuperview:cellSearchResults.btnMessage withCell:cellSearchResults],{cellSearchResults.btnMessage.frame.size.width ,cellSearchResults.btnMessage.frame.size.height}}],
                                 @"caption": NSLocalizedString(VC_CREATERIDE_CHAT, nil),
                                 @"shape": @"square"
                                 },
                               @{@"rect": [NSValue valueWithCGRect:(CGRect){[self getPointOfAViewFromItsSuperview:cellSearchResults.btnRequest1 withCell:cellSearchResults],{cellSearchResults.btnRequest1.frame.size.width, cellSearchResults.btnRequest1.frame.size.height}}],
                                 @"caption": NSLocalizedString(VC_CREATERIDE_REQUESTTUT, nil),
                                 @"shape": @"square"
                                 }
                               ];
            }
            WSCoachMarksView *coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
            coachMarksView.animationDuration = 0.3f;
            coachMarksView.enableContinueLabel = YES;
            coachMarksView.enableSkipButton = NO;
            coachMarksView.maskColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
            [self.navigationController.view addSubview:coachMarksView];
            [coachMarksView start];
            obj.isInCreateRideShown = @"1";
            [[AppDelegate getAppDelegateInstance] updateTutorialDBObject:obj];
            //  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createRideTutorialShown"];
        }
    }
}

+(UINavigationController *)getcreateRideNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *createRideNavController = [storyBoard instantiateViewControllerWithIdentifier:@"createRideNavController"];
    return createRideNavController;
}
//-(NSString *)getDateAndTime:(NSString *)tritDateTime{
//    //NSLog(@"time is %@",tritDateTime);
//    //ride.departureTime
//    NSDateFormatter *dateTimeFormatter1 = [[NSDateFormatter alloc] init];
//    [dateTimeFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [dateTimeFormatter1 setLocale:locale];
//
//
//    NSDate *departureTime = [dateTimeFormatter1 dateFromString:tritDateTime];
//
//    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
//    [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
//
////    NSDateFormatter * timeFormatter1 = [[NSDateFormatter alloc] init];
////    [timeFormatter1 setDateFormat:@"HH:mm"];
////    [timeFormatter1 setLocale:locale];
//
//    NSString *timeStr = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureTime];
//    NSString *dateStr = [self getDateStringFromDepartureTime:departureTime];
//    return [NSString stringWithFormat:@"%@, %@", dateStr, timeStr];
//}

//phase - 1

-(NSString *)getDateAndTime:(NSString *)tritDateTime strType:(NSString *)strType {
    //NSLog(@"time is %@",tritDateTime);
    //ride.departureTime
    NSDateFormatter *dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    
    
    NSDate *departureTime = [dateTimeFormatter1 dateFromString:tritDateTime];
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    
    if ([strType  isEqual: @"Time"]) {
        NSString *timeStr = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureTime];
        return timeStr;
    } else if ([strType  isEqual: @"Date"]) {
        [dateFormatter1 setDateFormat:@"dd MMM"];  //"dd MMM yyyy"
        NSString *dateStr = [self getDateStringFromDepartureTime:departureTime];
        return dateStr;
    }
    
    NSString *timeStr = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureTime];
    NSString *dateStr = [self getDateStringFromDepartureTime:departureTime];
    return [NSString stringWithFormat:@"%@, %@", dateStr, timeStr];
}

-(NSString *)getDateStringFromDepartureTime:(NSDate *)date{
    NSDate *currentDayDate = [self getCurrentDayDate];
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:currentDayDate toDate:date options:0];
    NSInteger difference = [components day];
    if(difference == 0) return NSLocalizedString(CMON_GENERIC_TODAY, nil);
    else if(difference == 1) return NSLocalizedString(CMON_GENERIC_TOMORROW, nil);
    else return [dateFormatter1 stringFromDate:date];
}
-(NSDate *)getCurrentDayDate{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [calendar dateFromComponents:components];
}
-(CGPoint)getPointOfAViewFromItsSuperview:(UIView *)view withCell:(SearchResultsCell *)theCell{
    CGFloat padd = 133;
    if([AppDelegate SCREEN_HEIGHT] == 812){  // for Iphone X
        padd = 180;
    }
    CGRect frame = [view.superview convertRect:view.frame toView:_tableView];
    CGFloat Y1 = frame.origin.y + padd;
    return CGPointMake(frame.origin.x, Y1);
}

- (void)fetchMoreData {
    [self callServiceForFetchingDataForFirstTime:NO];
    // this simulates a background fetch; I'm just going to delay for a second
}
-(void)refreshData:(NSArray *)indexpathsArr{
    @try{
        static BOOL fetchInProgress = FALSE;
        if (fetchInProgress)
            return;
        typeof(self) __weak weakSelf = self;
        fetchInProgress = TRUE;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            typeof(self) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf.tableView beginUpdates];
                // [strongSelf.tableView insertRowsAtIndexPaths:indexpathsArr withRowAnimation:UITableViewRowAnimationTop];
                for (int i= 0; i<indexpathsArr.count; i++){
                    [strongSelf.tableView insertSections:[NSIndexSet indexSetWithIndex:[[indexpathsArr objectAtIndex:i] intValue]] withRowAnimation:UITableViewRowAnimationTop];
                }
                fetchInProgress = FALSE;
                [strongSelf.tableView endUpdates];
            }
        });
    }@catch(NSException *ex){
        
    }
}

-(void)addDropShadowForView:(UIView *)viewShadow shadowRadius:(int)shadaowradius {
    viewShadow.backgroundColor = [UIColor clearColor];
    viewShadow.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = CGSizeMake(1, 1);
    viewShadow.layer.shadowRadius = shadaowradius;
    viewShadow.layer.masksToBounds = NO;
}

-(IBAction)showFreshChat:(id)sender{
    [[Freshchat sharedInstance] showConversations:self];
}




@end
