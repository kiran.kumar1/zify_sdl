//
//  EmergencyController.swift
//  zify
//
//  Created by Anurag on 04/09/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class EmergencyController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var lblHeader1: UILabel!
    @IBOutlet var lblSubHeader1: UILabel!
    @IBOutlet var lblSixty: UILabel!
    @IBOutlet var lblSecondsTxt: UILabel!
    @IBOutlet var lblSubHeader2: UILabel!
    @IBOutlet var lblEmergencyContactTxt: UILabel!
    @IBOutlet var lblEmergencyNo: UILabel!
    @IBOutlet var lblLiveChatTxt: UILabel!
    @IBOutlet var viewSixtySec: UIView!
    @IBOutlet var btnCall: UIButton!

    var countdownTimer : Int?
    
    let profile = UserProfile.getCurrentUser()
    var locationManager = CLLocationManager()
    //var locationCoordinatesValue = CLLocationCoordinate2D()
    var currentLocation: CLLocation?
    
    
    var myValue : NSNumber?
    
    class var sharedInstance : EmergencyController {
        struct Static {
            static let instance : EmergencyController = EmergencyController()
        }
        return Static.instance
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countdownTimer = 60
        
        debugPrint(myValue, EmergencyController.sharedInstance.myValue, "---myValue----")
        
        
        lblHeader1.text = NSLocalizedString(dont_worry, comment: "")
        lblSubHeader1.text = NSLocalizedString(our_support_team_will, comment: "")
        lblSubHeader2.text = NSLocalizedString(if_call_doesnt_connect, comment: "")
        lblEmergencyContactTxt.text = NSLocalizedString(UPS_EDITPROFILE_LBL_EMERGENCTCONTACT, comment: "")
        lblLiveChatTxt.text = NSLocalizedString(SUPPORT_LIVE_CHAT_BTN_TITLE, comment: "")
        if profile?.emergencyContact == "" || profile?.emergencyContact == nil {
            lblEmergencyNo.text = "Not Available"
            btnCall.isHidden = true
        } else {
            lblEmergencyNo.text = "\(profile?.isdCode ?? "")" + "\(profile?.emergencyContact ?? "Not Available")"
            btnCall.isHidden = false
        }
        if UIScreen.main.sizeType == .iPhone5 {
            self.lblHeader1.fontSize = 30
            self.lblSubHeader1.fontSize = 16
            self.lblSixty.fontSize = 40
            self.lblSecondsTxt.fontSize = 17
            self.lblSubHeader2.fontSize = 13
            self.lblEmergencyContactTxt.fontSize = 15
            self.lblEmergencyNo.fontSize = 12
            self.lblLiveChatTxt.fontSize = 16
            
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            self.lblHeader1.fontSize = 32
            self.lblSubHeader1.fontSize = 15
            self.lblSixty.fontSize = 41
            self.lblSecondsTxt.fontSize = 19
            self.lblSubHeader2.fontSize = 14
            self.lblEmergencyContactTxt.fontSize = 16
            self.lblEmergencyNo.fontSize = 12
            self.lblLiveChatTxt.fontSize = 18
            
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            self.lblHeader1.fontSize = 34
            self.lblSubHeader1.fontSize = 17
            self.lblSixty.fontSize = 44
            self.lblSecondsTxt.fontSize = 21
            self.lblSubHeader2.fontSize = 16
            self.lblEmergencyContactTxt.fontSize = 18
            self.lblEmergencyNo.fontSize = 14
            self.lblLiveChatTxt.fontSize = 20
        }
        
        showShadow(view: viewSixtySec, color: UIColor.black, opacity: 0.5, offSet: CGSize.init(width: -1, height: 0), radius: 9, scale: true)
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        
        locationManager.requestWhenInUseAuthorization()
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locationManager.location
            self.serviceCall_SOS()
        } else {
            //if location service is not available
            serviceCall_SOS()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func updateTime() {
        if countdownTimer ?? 0 > 0 {
            countdownTimer? -= 1
            lblSixty.text = "\(countdownTimer ?? 0)"
        } else {
            return
        }
    }
    
    //MARK:- Actions
    
    @IBAction func btnTappedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnTappedEmergencyContact(_ sender: Any) {
        if let url = URL(string: "tel://\(lblEmergencyNo.text ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func btnTappedLiveChat(_ sender: Any) {
        //self.serviceCall_SOS()
        Freshchat.sharedInstance()?.showConversations(self)
    }
    
    //MARK:- Service Call
    
    func serviceCall_SOS() {
        let userProfile = UserProfile.getCurrentUser()
        let driveRideId : String = "\(EmergencyController.sharedInstance.myValue ?? 0)"
        
        let reqSOSMail = SendSOSEmailRequest.init(currentLat: "\(currentLocation?.coordinate.latitude ?? 0)", currentLong: "\(currentLocation?.coordinate.longitude ?? 0)", driveRideId: driveRideId)
        
        debugPrint("---reqSendMail  service -----\(String(describing: reqSOSMail))")
        
        ServerInterface.sharedInstance()?.getResponse(reqSOSMail, withHandler: { (response, error) in
            if error == nil {
                debugPrint("---success send mail service -----\(String(describing: response))")
            } else {
                debugPrint("---error in send mail request---\(String(describing: error?.localizedDescription))")
            }
        })
        
    }
    
    class func createEmergencyNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard.init(name: "Emergency", bundle: Bundle.main)
        let emergencyNavCtrl = storyBoard.instantiateViewController(withIdentifier: "EmergencyNavController") as? UINavigationController
        return emergencyNavCtrl
    }
    
}


class SendSOSEmailRequest : ServerRequest {
    let userProfileObj = UserProfile.getCurrentUser()
    
   
    var dictParamsTemp : [String : AnyObject] = [:]
    
    //https://maps.google.com/?q=<lat>,<lng>
    
    init(currentLat : String, currentLong : String, driveRideId : String) {
        
        dictParamsTemp = [
            "sos_address_link" : "https://maps.google.com/?q=\(currentLat),\(currentLong)",
            "drive_ride_id" : driveRideId
            ] as [String : AnyObject]
        debugPrint(dictParamsTemp, "-----dictParamsTemp---with lat long-------")
       
    }
    
    
    override func urlparams() -> [AnyHashable : Any]! {
        //let urlParams = [dictParam] as [String : Any]
        //let currentTripRide = TripRide.getObjectMapping()
        
        
        dictParamsTemp.updateValue((userProfileObj?.country ?? "") as AnyObject, forKey: "country_code")
        
        dictParamsTemp.updateValue((userProfileObj?.userPreferences.userMode ?? "") as AnyObject, forKey: "mode")
        
        

        debugPrint("---dict params------", dictParamsTemp)
        return dictParamsTemp
    }
    
    override func overrideBaseURL() -> Bool {
        return true
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
    override func isJSONRequestFormat() -> Bool {
        return true
    }
    
    override func serverURL() -> String! {

        //For Test Server
        //return "zifyemaildev/email/generateSOSMail"
        //For Live Server
        return "/zifyemail/email/generateSOSMail"
    }
    
    override func isForReferAndEarn() -> Bool {
        return true
    }
    
}
