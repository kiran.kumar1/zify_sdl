//
//  GuestMenuController.m
//  zify
//
//  Created by Anurag S Rathor on 19/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "GuestMenuController.h"
#import "GuestMenuProfileCell.h"
#import "GuestMenuOptionCell.h"
#import "LocalisationConstants.h"

#define PROFILE_SECTION 0
#define ABOUT_SECTION 1
#define HOWITWORKS_SECTION 2


@interface GuestMenuController ()

@end

@implementation GuestMenuController {
    NSLayoutConstraint *contentViewBeforeAnimateConstraint;
    NSLayoutConstraint *contentViewAfterAnimateConstraint;
    BOOL isViewAnimated;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    [self initialiseView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!isViewAnimated){
        [self.view removeConstraint:_contentViewLeftConstraint];
        [self.view removeConstraint:contentViewAfterAnimateConstraint];
        [self.view addConstraint:contentViewBeforeAnimateConstraint];
        [self.view layoutIfNeeded];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewAnimated){
        [self.view removeConstraint:contentViewBeforeAnimateConstraint];
        [self.view addConstraint:contentViewAfterAnimateConstraint];
        [UIView animateWithDuration:0.3  animations:^{
            self.contentView.alpha =1;
            [self.view layoutIfNeeded];
        }completion:^(BOOL finished){
            isViewAnimated = true;
        }];
    }
}

-(void) initialiseView{
    self.contentView.alpha =0;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [[UIScreen mainScreen] bounds].size.height <568.0){
        _tableView.scrollEnabled = YES;
    } else{
        _tableView.scrollEnabled = NO;
    }
    _tableView.estimatedRowHeight = 175.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    contentViewAfterAnimateConstraint = [NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.0f];
    contentViewBeforeAnimateConstraint = [NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.0f];
    isViewAnimated = false;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == PROFILE_SECTION) {
        GuestMenuProfileCell *profileCell = [tableView dequeueReusableCellWithIdentifier:@"profileCell"];
        profileCell.profilePlaceholder.layer.cornerRadius = profileCell.profilePlaceholder.frame.size.width / 2;
        profileCell.profilePlaceholder.clipsToBounds = YES;
        profileCell.loginButton.layer.borderWidth = 2.0;
        profileCell.loginButton.layer.borderColor =  profileCell.loginButton.titleLabel.textColor.CGColor;
        profileCell.loginButton.layer.cornerRadius = 15.0f;
        return profileCell;
    }
    GuestMenuOptionCell *optionCell = [tableView dequeueReusableCellWithIdentifier:@"optionCell"];
    if(indexPath.section == ABOUT_SECTION){
        optionCell.optionImage.image = [UIImage imageNamed:@"icn_menu_about.png"];
        optionCell.optionName.text = NSLocalizedString(VC_GUESTMENU_ABOUT, nil);
    }
    return optionCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissMenuWithCompletion:^{
        if(indexPath.section == PROFILE_SECTION){
            [self.menuDelegate selectedMenuOption:GUESTPROFILE];
        } else if(indexPath.section == ABOUT_SECTION){
            [self.menuDelegate selectedMenuOption:GUESTABOUT];
        }
    }];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

- (IBAction)dismissMenu:(id)sender {
    [self dismissMenuWithCompletion:nil];
}

-(void) dismissMenuWithCompletion:(void(^)())handler{
    [self.view removeConstraint:contentViewAfterAnimateConstraint];
    [self.view addConstraint:contentViewBeforeAnimateConstraint];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }completion:^(BOOL finished){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        if(handler) handler();
    }];
}

- (IBAction)showLogin:(id)sender {
    [self dismissMenuWithCompletion:^{
        [self.menuDelegate selectedMenuOption:GUESTPROFILE];
    }];
}

+(GuestMenuController *)createAndOpenGuestMenuWithSourceController:(UIViewController *)srcViewController andMenuDelegate:(id<GuestMenuDelegate>)menuDelegate;{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"GuestUser" bundle:[NSBundle mainBundle]];
    GuestMenuController *menuController = [storyBoard instantiateViewControllerWithIdentifier:@"menuView"];
    [srcViewController addChildViewController:menuController];
    [srcViewController.view addSubview:menuController.view];
    NSDictionary *viewsDictionary = @{@"menuView":menuController.view};
    menuController.view.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[menuView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[menuView]|" options:0 metrics:nil views:viewsDictionary];
    [srcViewController.view addConstraints:horizontalConsts];
    [srcViewController.view addConstraints:verticalConsts];
    menuController.menuDelegate = menuDelegate;
    return menuController;
}
@end
