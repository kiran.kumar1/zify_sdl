//
//  VerifyMobileController.m
//  zify
//
//  Created by Anurag S Rathor on 14/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "VerifyMobileController.h"
#import "UIImage+initWithColor.h"
#import "VerifyMobileRequest.h"
#import "ServerInterface.h"
#import "OnboardFlowHelper.h"
#import "UserProfile.h"
#import "AppDelegate.h"
#import "GenericRequest.h"
#import "UserProfile.h"
#import "FacebookLogin.h"
#import "ChatManager.h"
#import "PushNotificationManager.h"
#import "OnboardFlowHelper.h"
#import "UserDataManager.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

@interface VerifyMobileController ()

@end

@implementation VerifyMobileController{
    BOOL isViewInitialised;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isViewInitialised = false;
    [self.btnVerify addTarget:self action:@selector(btnVerifyTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLogout addTarget:self action:@selector(doLogout) forControlEvents:UIControlEventTouchUpInside];

    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (IS_IPHONE_5) {
        _lblWhenWeSentaVerifictionCodeToYourMobileNo.fontSize = 10;
        _mobileNumber.fontSize = 14;
        _btnVerify.titleLabel.fontSize = 14;
        _btnResend.titleLabel.fontSize = 11;
        _btnLogout.titleLabel.fontSize = 14;
        _lblDidnotGetTheSMS.fontSize = 12;
        _lblNotYou.fontSize = 11;
        _lblWeDonotUseMobNoAnyCommercialUse.fontSize = 10;
    } else if (IS_IPHONE_6) {
        _lblWhenWeSentaVerifictionCodeToYourMobileNo.fontSize = 10;
        _mobileNumber.fontSize = 14;
        _btnVerify.titleLabel.fontSize = 14;
        _btnResend.titleLabel.fontSize = 12;
        _btnLogout.titleLabel.fontSize = 14;
        _lblDidnotGetTheSMS.fontSize = 13;
        _lblNotYou.fontSize = 11;
        _lblWeDonotUseMobNoAnyCommercialUse.fontSize = 10;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _lblWhenWeSentaVerifictionCodeToYourMobileNo.fontSize = 11;
        _mobileNumber.fontSize = 15;
        _btnVerify.titleLabel.fontSize = 15;
        _btnResend.titleLabel.fontSize = 13;
        _btnLogout.titleLabel.fontSize = 15;
        _lblDidnotGetTheSMS.fontSize = 14;
        _lblNotYou.fontSize = 12;
        _lblWeDonotUseMobNoAnyCommercialUse.fontSize = 11;
    }
    

    
    [self initialiseView];
}

-(void)initialiseView{
    if(!isViewInitialised){
        UIImage *navigationBackgroundImage = [UIImage imageWithColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0]];
        [self.navigationController.navigationBar setBackgroundImage:navigationBackgroundImage forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
        UserProfile *currentUser = [UserProfile getCurrentUser];
        _mobileNumber.text = [NSString stringWithFormat:@"%@ %@",currentUser.isdCode,currentUser.mobile];
        [self getUserOTP];
        isViewInitialised = true;
    }
}

#pragma mark - SingleCharPasswordView Delegate methods
-(void)btnVerifyTapped:(id)sender{
    NSString *code = self.otpView.otpCode;
    [self callVerifyOTPService:code];
}
    -(void)callVerifyOTPService:(NSString *)code{
    if(code.length == 6){
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            VerifyMobileRequest *request = [[VerifyMobileRequest alloc] initWithOtp:code andRequestType:VERIFYMOBILE];
            [[ServerInterface sharedInstance]getResponse:request withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        _mobileVerifiedIcon.hidden = NO;
                        _btnVerify.hidden = YES;
                        [self completeUserFlow];
                        //[self performSelector:@selector(completeUserFlow) withObject:self afterDelay:1.0];
                    } else{
                        _mobileVerifiedIcon.hidden = YES;
                        NSString *errorMsg = NSLocalizedString(OTP_WRONG_ALERT_MSG, nil);
                        [self.messageHandler showErrorMessage:errorMsg];
                        [_otpView handleErrorCase];
                    }
                }];
            }];
        }];
    }
}
-(void)doLogout{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:[[GenericRequest alloc] initWithRequestType:LOGOUTREQUEST] withHandler:^(ServerResponse *response, NSError * error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [[PushNotificationManager sharedInstance] deregisterDeviceForPushOnLogout]; //Should be called before deleting user since userId is required here.
                    [UserProfile deleteCurrentUser];
                    [[ChatManager sharedInstance] disconnectChatOnLogout];
                    [[OnboardFlowHelper sharedInstance] clearReminder];
                    [[FacebookLogin sharedInstance] closeSessionAndClearSavedTokens];
                    [[UserDataManager sharedInstance] setDeepLinkFlagsOnLogout];
                    [(AppDelegate *)[UIApplication sharedApplication].delegate showLoginController];
                    [[FacebookLogin sharedInstance] closeSessionAndClearSavedTokens];
                } else {
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void) enteredPasswordCode:(NSString *)code{
   // [self callVerifyOTPService:code];
}

-(IBAction)resendOTP:(UIButton *)sender{
    [self getUserOTP];
}

-(void)getUserOTP{
    VerifyMobileRequest *request = [[VerifyMobileRequest alloc] initWithOtp:@"" andRequestType:VERIFYMOBILEOTP];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:request withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response) {
                    [self.otpView showKeyboard];
                    self.otpView.passwordDelegate = self;
                } else {
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void)completeUserFlow{
    [[AppDelegate getAppDelegateInstance] showTravelPrefAfterSignUpOnly];
    //[[OnboardFlowHelper sharedInstance] handleVerifyMobileFlow:self];
}

+(UINavigationController *)createVerifyMobileNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"UserOnboard" bundle:[NSBundle mainBundle]];
    UINavigationController *verifyMobileNavController = [storyBoard instantiateViewControllerWithIdentifier:@"verifyMobileNavController"];
    return verifyMobileNavController;
}@end
