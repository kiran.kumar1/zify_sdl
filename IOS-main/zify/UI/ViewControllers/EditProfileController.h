//
//  EditProfileController.h
//  zify
//
//  Created by Anurag S Rathor on 29/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ScrollableContentViewController.h"
#import "CustomPickerView.h"
#import "ImageSelectionViewer.h"
#import "MessageHandler.h"
#import "DateTimePickerView.h"
#import "zify-Swift.h"



@interface EditProfileController : ScrollableContentViewController<CustomPickerDelegate,ImageSelectionViewerDelegate,DateTimePickerDelegate,CropViewControllerDelegate>{
}
@property (nonatomic,weak) IBOutlet UIScrollView *objScroll;
@property (nonatomic,weak) IBOutlet UIImageView *displayProfileImage;
@property (nonatomic,weak) IBOutlet UIImageView *blurImageView;
@property (nonatomic,weak) IBOutlet UIButton *btnEditProfile;
@property (nonatomic,weak) IBOutlet UILabel *userName;
@property (nonatomic,weak) IBOutlet UIButton *gender;
@property (nonatomic,weak) IBOutlet UIButton *dob;
@property (nonatomic,weak) IBOutlet UITextField *companyName;
@property (nonatomic,weak) IBOutlet UITextField *companyEmail;
@property (nonatomic,weak) IBOutlet UIButton *bloodGroup;
@property (nonatomic,weak) IBOutlet UITextField *emergencyContact;
@property (nonatomic,weak) IBOutlet CustomPickerView *bloodGroupPickerView;
@property (nonatomic,weak) IBOutlet CustomPickerView *genderPickerView;
@property (nonatomic,weak) IBOutlet DateTimePickerView *dobPickerView;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;

@property (nonatomic,weak) IBOutlet UILabel *lblPersonalDetails;
@property (nonatomic,weak) IBOutlet UILabel *lblCompanyDetails;
@property (nonatomic,weak) IBOutlet UILabel *lblOtherDetails;
@property (nonatomic,weak) IBOutlet UIButton *btnSave;


@end
