//
//  UserBaseController.h
//  zify
//
//  Created by Anurag S Rathor on 30/08/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import  "MessageHandler.h"

@interface UserBaseController : UIViewController
@property(nonatomic,weak) IBOutlet UIImageView *bgImageView;
@property(nonatomic,strong)UIImageView *logoImageView;
@property(nonatomic,strong)UIImageView *pinImageView;
@property(nonatomic)CGPoint originalCenter;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@end
