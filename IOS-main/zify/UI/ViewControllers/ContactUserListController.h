//
//  ContactUserListController.h
//  zify
//
//  Created by Anurag S Rathor on 05/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"

@interface ContactUserListController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) UIViewController *srcController;
@property (nonatomic,strong) NSArray *contactUsers;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;
+(ContactUserListController *)createAndOpenContactListController:(UIViewController *)srcViewController WithUserContacts:(NSArray *)contacts;
@end
