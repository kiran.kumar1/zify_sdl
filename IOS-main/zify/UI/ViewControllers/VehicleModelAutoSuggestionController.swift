//
//  VehicleModelAutoSuggestionController.swift
//  zify
//
//  Created by Anurag Rathor on 07/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

protocol VehicleModelSelectionDelegate: NSObjectProtocol {
    func selectedModelName(_ model: String?)
}

class VehicleModelAutoSuggestionController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    weak var selectionDelegate: VehicleModelSelectionDelegate?
    var fetchResultsController: NSFetchedResultsController<NSFetchRequestResult>?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        searchBar.becomeFirstResponder()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if( (searchBar.text?.count)! > 1){
        let count: Int = (fetchResultsController?.sections![0].numberOfObjects)!
        return count
        }
        return 0;
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let CellIdentifier = "Cell"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
           // cell?.willTransition(to: UITableViewCellStateMask)
        }
        let vehicleModel = fetchResultsController?.object(at: indexPath) as! NSManagedObject
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        if let modelName = vehicleModel.value(forKey: "modelName") {
            cell?.textLabel?.text = "\(modelName)"
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    
    @IBAction func dismiss(_ sender: Any) {
        searchBar.resignFirstResponder()
        navigationController?.popViewController(animated: false)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      /*  let vehicleModel = fetchResultsController.object(at: indexPath) as? VehicleModel
        selectionDelegate.selectedModelName(vehicleModel?.modelName)
        navigationController?.popViewController(animated: false)*/
        
        let selectedObject = fetchResultsController?.object(at: indexPath) as! NSManagedObject
        let modelName = selectedObject.value(forKey: "modelName") as! String
        selectionDelegate?.selectedModelName(modelName)
        navigationController?.popViewController(animated: false)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
        if(searchText.count > 1){
        fetchResultsController = getFetchedResultsController(searchText)
        var error: Error?
        try? fetchResultsController?.performFetch()
        }
        tableView.reloadData()
        }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    
    func getFetchedResultsController(_ text: String?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: "VehicleModel", in: DBInterface.sharedInstance().sharedMainContext())
        fetchRequest.entity = entity
        let sort = NSSortDescriptor(key: "modelName", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        fetchRequest.fetchBatchSize = 20
        let predicate = NSPredicate(format: "modelName CONTAINS[cd] %@", text ?? "")
        fetchRequest.predicate = predicate
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DBInterface.sharedInstance().sharedMainContext(), sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultsController
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
