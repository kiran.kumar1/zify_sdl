//
//  TripsRidesController.h
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"

@interface TripsRidesController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *noRideView;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet UIView *rideButtonView;
@property(nonatomic,assign) BOOL isViewInitialised;
-(void)fetchTheServiceDetailsAndLoad;
@end

