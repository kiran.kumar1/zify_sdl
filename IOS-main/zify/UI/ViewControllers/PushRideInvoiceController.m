//
//  PushRideInvoiceController.m
//  zify
//
//  Created by Anurag S Rathor on 24/06/16.
//  Copyright © 2016 zify. All rights reserved.
//


#import "PushRideInvoiceController.h"
//#import "RideInvoiceController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CurrentLocale.h"
#import "ServerInterface.h"
#import "TripRatingsRequest.h"
#import "AppUtilites.h"
#import "PushTripDetails.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

#define COMMENTS_MAX_LENGTH 200
#define COMMENTS_TEXT_TAG 1234

@interface PushRideInvoiceController ()

@end

@implementation PushRideInvoiceController{
    UIImage *profileImagePlaceHolder;
    BOOL isViewInitialised;
    float selectedRating;
    PushTripDetails *tripDetails;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    isViewInitialised = false;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initialiseView];
    
    if (IS_IPHONE_5) {
        _userName.fontSize = 15;
        _amount.fontSize = 17;
        _source.fontSize = 13;
        _destination.fontSize = 13;
        _errorText.fontSize = 11;
        _lblHowWasYourTrip.fontSize = 12;
        _btnSubmit.titleLabel.fontSize = 15;
    } else if (IS_IPHONE_6) {
        _userName.fontSize = 15;
        _amount.fontSize = 17;
        _source.fontSize = 13;
        _destination.fontSize = 13;
        _errorText.fontSize = 12;
        _lblHowWasYourTrip.fontSize = 13;
        _btnSubmit.titleLabel.fontSize = 16;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _userName.fontSize = 16;
        _amount.fontSize = 18;
        _source.fontSize = 13;
        _destination.fontSize = 13;
        _errorText.fontSize = 12;
        _lblHowWasYourTrip.fontSize = 13;
        _btnSubmit.titleLabel.fontSize = 18;
    }
    

    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)initialiseView{
    if(!isViewInitialised){
       tripDetails = _tripCompleteNotification.tripDetails;
        _userImage.hidden = YES;
        [_userImage sd_setImageWithURL:[NSURL URLWithString:tripDetails.driverProfileImg] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
            self.userImage.hidden = NO;
        }];
        _userImage.layer.cornerRadius = self.userImage.frame.size.width / 2;
        _userImage.clipsToBounds = YES;
        _userImage.layer.borderColor = [UIColor whiteColor].CGColor;
        _userImage.layer.borderWidth = 3.0f;
        
       /* NSString *currencySymbol = [[CurrentLocale sharedInstance] getCurrencySymbol];
        if([currencySymbol isEqualToString:@"€"]){
            _amount.text = [[AppDelegate getAppDelegateInstance] displayTripAmountInCommaSeparator:tripDetails.amount];  // amountValue;
        }else{
            _amount.text = [NSString stringWithFormat:@"%@ %@",[[CurrentLocale sharedInstance] getCurrencySymbol],tripDetails.amount.stringValue];
        }*/
        
        _amount.text = [[AppDelegate getAppDelegateInstance] displayTripAmountInCommaSeparator:tripDetails.amount];  

        
        _userName.text = tripDetails.driverFirstName;
     //   UIImage *sourceIconImage = [UIImage imageNamed:@"icn_source_info.png"];
     //   _sourceIcon.image = [sourceIconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
     //   [_sourceIcon setTintColor:[UIColor colorWithRed:(255.0/255.0) green:(178.0/255.0) blue:(63.0/255.0) alpha:1.0]];
        _source.text = tripDetails.srcAddress;
     //   UIImage *destinationIconImage = [UIImage imageNamed:@"icn_destination_info.png"];
     //   _destinationIcon.image = [destinationIconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
     //   [_destinationIcon setTintColor:[UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0]];
        _destination.text = tripDetails.destAddress;
        _ratingView.rating = 0.0;
        _ratingView.editable = YES;
        _ratingView.delegate = self;
        isViewInitialised = true;
    }
}

- (void)rateView:(RatingView *)rateView ratingDidChange:(float)rating{
    selectedRating = rating;
}

-(IBAction) submitRating:(UIButton *)sender {
    if(selectedRating == 0){
        _errorText.text = NSLocalizedString(VC_PUSHRIDEINVOICE_RATINGVAL, nil);
        return;
    }
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[TripRatingsRequest alloc] initWithWithRequestType:RIDERATING andTripId:tripDetails.rideId andRatingTripIds:[NSArray arrayWithObject:tripDetails.rideId] andRatings:[NSArray arrayWithObject:[NSNumber numberWithInt:selectedRating]] andComments:[NSArray arrayWithObject:_feedbackText.text]] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [self willMoveToParentViewController:nil];
                    [self beginAppearanceTransition:NO animated:NO];
                    [self endAppearanceTransition];
                    [self.view removeFromSuperview];
                    [self removeFromParentViewController];
                } else{
                    _errorText.text = error.userInfo[@"message"];
                }
            }];
        }];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *replacementText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(textField.tag == COMMENTS_TEXT_TAG && replacementText.length > COMMENTS_MAX_LENGTH){
        return NO;
    }
    return YES;
}

+(void)showRideInvoiceControllerWithTripNotification:(PushTripCompleteNotification *)tripCompleteNotification{
    
   /* UIViewController *visibleController = [AppUtilites applicationVisibleViewController];
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UINavigationController *rideInvoiceNavController = (UINavigationController *) [RideInvoiceController createRideInvoiceNavController];
    RideInvoiceController *rideInvoiceController = (RideInvoiceController *)rideInvoiceNavController.topViewController;
    rideInvoiceController.ratingResponse = rideResponse;
    
    if(rootViewController.presentedViewController != nil){
        [rootViewController dismissViewControllerAnimated:NO completion:nil];
        visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [visibleController presentViewController:rideInvoiceNavController animated:YES completion:nil];
        
    }else{
        [visibleController presentViewController:rideInvoiceNavController animated:YES completion:nil];
    }
    return;*/
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Commons" bundle:[NSBundle mainBundle]];
    UIViewController *visibleController = [AppUtilites applicationVisibleViewController];
    if(visibleController.navigationController) visibleController = visibleController.navigationController;
    PushRideInvoiceController *rideInvoiceController = [storyBoard instantiateViewControllerWithIdentifier:@"pushRideInvoiceController"];
    rideInvoiceController.tripCompleteNotification = tripCompleteNotification;
    [visibleController addChildViewController:rideInvoiceController];
    [visibleController.view addSubview:rideInvoiceController.view];
    rideInvoiceController.view.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *viewsDictionary = @{@"invoiceView":rideInvoiceController.view};
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[invoiceView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[invoiceView]|" options:0 metrics:nil views:viewsDictionary];
    [visibleController.view addConstraints:horizontalConsts];
    [visibleController.view addConstraints:verticalConsts];
    
    rideInvoiceController.view.alpha = 0.0;
    [rideInvoiceController beginAppearanceTransition:YES animated:YES];
    
    rideInvoiceController.view.alpha = 1.0;
    [rideInvoiceController endAppearanceTransition];
    [rideInvoiceController didMoveToParentViewController:visibleController];
}
@end
