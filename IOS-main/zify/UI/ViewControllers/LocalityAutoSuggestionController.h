//
//  LocalityAutoSuggestionController.h
//  zify
//
//  Created by Anurag S Rathor on 21/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"
#import  "LocalityInfo.h"
#import "UserAddressPreferencesController.h"
#import "MapLocationHelper.h"

@protocol LocalitySelectionDelegate<NSObject>
-(void)selectedLocality:(LocalityInfo *)sourceLocation withDestnationLocation:(LocalityInfo *)destiLocation;
-(void)swapLocalityTapped;
-(void)selectedFromThePreviousHistory;
@end

@interface LocalityAutoSuggestionController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UserAddressPreferencesDelegate,MapLocationHelperDelegate,UITextFieldDelegate>{
    int textFieldtextLength;
    BOOL isOldSearchAvailable;
    int sectionsCount;
    NSArray *oldHistoryDataArr;
}
@property(nonatomic,weak) IBOutlet UIImageView *sourceImageView;
@property(nonatomic,weak) IBOutlet UIImageView *destImageView;
@property(nonatomic,weak) IBOutlet UITextField *sourceTextField;
@property(nonatomic,weak) IBOutlet UITextField *destinationTextField;
@property(nonatomic,weak) IBOutlet UIButton *btnSwapAddresses;
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property(nonatomic,weak) IBOutlet UISearchBar *searchBar;
@property(nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property(nonatomic)  BOOL showAutosuggestOnly;
@property (nonatomic, weak) id<LocalitySelectionDelegate> selectionDelegate;
+(UINavigationController *)createLocalityAutoSuggestNavController;
@property(nonatomic, assign) BOOL isForSourceAddress;
@property(nonatomic,strong) LocalityInfo *sourceLocality, *destinationLocality;
- (IBAction)btnSwapTapped:(UIButton *)sender;
@end
