//
//  CurrentRideController.swift
//  zify
//
//  Created by Anurag Rathor on 10/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class CurrentRideController: UIViewController, CurrentRideLocationTrackerDelegate {
    
    @IBOutlet weak var mapContainerView: MapContainerView!
    @IBOutlet weak var driverInfoShadeView: UIView!
    @IBOutlet weak var driverInfoView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var rideStartTime: UILabel!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleNumber: UILabel!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var etaValue: UILabel!
    @IBOutlet weak var boardButton: UIButton!
    @IBOutlet weak var sosBtn: UIButton!

    @IBOutlet weak var messageHandler: MessageHandler!
    var currentRide: TripRide?
    @IBOutlet weak var lblMessageText: UILabel!
    @IBOutlet weak var lblCallText: UILabel!
    @IBOutlet weak var lblBoardBtnSubText: UILabel!
    
    
    var isCurrentRideViewClosed = false
    var isCurrentRideStatusChanging = false
    var isViewInitialised = false
    var dateTimeFormatter: DateFormatter?
    var dateFormatter: DateFormatter?
    // NSDateFormatter *timeFormatter;
    var currentDayDate: Date?
    let profileObj = UserProfile.getCurrentUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isViewInitialised = false
        sosBtn.isHidden = true
        driverName.fontSize = 14
        rideStartTime.fontSize = 11
        vehicleName.fontSize = 13
        vehicleNumber.fontSize = 11
        etaValue.fontSize = 15
        boardButton.titleLabel?.fontSize = 15
        lblMessageText.fontSize = 10
        lblCallText.fontSize = 10
        if UIScreen.main.sizeType == .iPhone6 {
            driverName.fontSize = 14
            rideStartTime.fontSize = 11
            vehicleName.fontSize = 13
            vehicleNumber.fontSize = 11
            etaValue.fontSize = 15
            boardButton.titleLabel?.fontSize = 15
            lblMessageText.fontSize = 10
            lblCallText.fontSize = 10
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            driverName.fontSize = 15
            rideStartTime.fontSize = 12
            vehicleName.fontSize = 14
            vehicleNumber.fontSize = 12
            etaValue.fontSize = 16
            boardButton.titleLabel?.fontSize = 16
            lblMessageText.fontSize = 11
            lblCallText.fontSize = 11
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isViewInitialised {
            initialiseView()
        }
        let mapView:MapView = mapContainerView.mapView
        let srcNearLat = Double("\(currentRide?.srcNearLat ?? "0.0")")
        let srcNearLng = Double("\(currentRide?.srcNearLng ?? "0.0")")
        let destNearLat = Double("\(currentRide?.destNearLat ?? "0.0")")
        let destNearLng = Double("\(currentRide?.destNearLng ?? "0.0")")
        
        let srcActualLat = Double("\(currentRide?.srcActualLat ?? "0.0")")
        let srcActualLng = Double("\(currentRide?.srcActualLng ?? "0.0")")
        let destActualLat = Double("\(currentRide?.destActualLat ?? "0.0")")
        let destActualLng = Double("\(currentRide?.destActualLng ?? "0.0")")

        
        if srcNearLat == 0 && srcNearLng == 0 && destNearLat == 0 && destNearLng == 0 {
            return
        }
        let pickupImage = UIImage(named: "PickUp_Marker.png")
        let dropImage = UIImage(named: "Drop_Marker.png")
        let centerIcon = UIImage(named: "transparentoval.png")

        mapView.createMarkerWithhLat(NSNumber(value: srcNearLat ?? 0.0), andLong: NSNumber(value: srcNearLng ?? 0.0), andMarkerImage: pickupImage)
        mapView.createMarkerWithhLat(NSNumber(value: destNearLat ?? 0.0), andLong: NSNumber(value: destNearLng ?? 0.0), andMarkerImage: dropImage)
        
     /*   let pickupDeltaString = String(format: "%.02f", currentRide?.distance ?? 0)
        var pickupDeltaInt = Float(pickupDeltaString) ?? 0.0
        if pickupDeltaInt < 1 {
            pickupDeltaInt = pickupDeltaInt * 1000
            walkingDistance = "\(Int(pickupDeltaInt)) \(CurrentLocale.sharedInstance().getDistanceSubUnit() ?? "")"
        } else {
            walkingDistance = "\(currentRide?.distance ?? "0") \(CurrentLocale.sharedInstance().getDistanceUnit() ?? "")"
        }*/
        var walkingDistance = ""
        walkingDistance = "\(currentRide?.distance ?? "0") \(CurrentLocale.sharedInstance().getDistanceUnit() ?? "")"

        
        mapView.createMarker(forPickupWithhLat: NSNumber(value: srcNearLat ?? 0.0), andLong: NSNumber(value: srcNearLng ?? 0.0), andMarkerImage: pickupImage, withWalkingDistance: walkingDistance)
        
        let path = GMSPath(fromEncodedPath: (currentRide?.routeDetail.overviewPolylinePoints)!)
        let route = GMSPolyline(path: path)
        let count:Int = Int(route.path!.count())
        let center = route.path!.coordinate(at: UInt((count - 1) / 2))
        
        mapView.createMarker(forPickupWithhLat: NSNumber(value: center.latitude), andLong: NSNumber(value: center.longitude), andMarkerImage: centerIcon, withWalkingDistance: walkingDistance)

      /*  mapView.createMarkerWithhLat(NSNumber(value: srcNearLat ?? 0.0), andLong: NSNumber(value: srcNearLng ?? 0.0), andMarkerImage: pickupImage)

        mapView.createMarkerWithhLat(NSNumber(value: destNearLat ?? 0.0), andLong: NSNumber(value: destNearLng ?? 0.0), andMarkerImage: dropImage)
        */
        let pickupImage1 = UIImage(named: "icn_source_marker.png")
        mapView.createMarkerWithhLat(NSNumber(value: srcActualLat ?? 0.0), andLong: NSNumber(value: srcActualLng ?? 0.0), andMarkerImage: pickupImage1)
        
        let dropImage1 = UIImage(named: "icn_destination_marker.png")
        mapView.createMarkerWithhLat(NSNumber(value: destActualLat ?? 0.0), andLong: NSNumber(value: destActualLng ?? 0.0), andMarkerImage: dropImage1)
        
        let searchStart = CLLocation(latitude: CLLocationDegrees(NSNumber(value: srcActualLat ?? 0.0)), longitude: CLLocationDegrees(NSNumber(value: srcActualLng ?? 0.0)))
        let pickupStart = CLLocation(latitude: CLLocationDegrees(NSNumber(value: srcNearLat ?? 0.0)), longitude: CLLocationDegrees(NSNumber(value: srcNearLng ?? 0.0)))
        
        mapView.drawDashedLine(onMapBetweenOrigin: searchStart, destination: pickupStart)
        
        let dropStart = CLLocation(latitude: CLLocationDegrees(NSNumber(value: destNearLat ?? 0.0)), longitude: CLLocationDegrees(NSNumber(value: destNearLng ?? 0.0)))
        
        let searchEnd = CLLocation(latitude: CLLocationDegrees(NSNumber(value: destActualLat ?? 0.0)), longitude: CLLocationDegrees(NSNumber(value: destActualLng ?? 0.0)))

        mapView.drawDashedLine(onMapBetweenOrigin: dropStart, destination: searchEnd)
        
    }
    
    func initialiseView() {
        self.sosBtn.isHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        driverInfoView.frameWidth = AppDelegate.screen_WIDTH()
        driverInfoShadeView.frameWidth = AppDelegate.screen_WIDTH()
        isCurrentRideViewClosed = false
        drawCornerRadius(toAView: driverInfoView)
        drawShadow(toAView: driverInfoShadeView)
        let profileImagePlaceHolder = UIImage(named: "placeholder_profile_image")
        let vehicleImagePlaceHolder = UIImage(named: "placeholder_car_image.png")
        var driverInfoViewFrame: CGRect = driverInfoView.frame
        driverInfoViewFrame.origin.y = view.frame.height - driverInfoViewFrame.size.height
        driverInfoView.frame = driverInfoViewFrame
        var driverImageFrame: CGRect = driverImage.frame
        driverImageFrame.origin.y = driverInfoView.frame.minY - driverImageFrame.size.height / 2
        driverImage.frame = driverImageFrame
        var mapContainerFrame: CGRect = mapContainerView.frame
        mapContainerFrame.size.height = view.frame.height - driverInfoView.frame.height
        mapContainerView.frame = mapContainerFrame
        dateTimeFormatter = DateFormatter()
        dateTimeFormatter?.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let locale: NSLocale? = NSLocale(localeIdentifier: "en_US_POSIX")
        dateTimeFormatter?.locale = locale as Locale?
        dateFormatter = DateFormatter()
        dateFormatter?.dateFormat = "dd MMM yyyy"
        //    timeFormatter = [[NSDateFormatter alloc] init];
        //    [timeFormatter setDateFormat:@"HH:mm"];
        //    [timeFormatter setLocale:locale];
        currentDayDate = getCurrentDayDate()
        
        let driverDetail: DriverDetail? = currentRide?.driverDetail
        
        driverImage.sd_setImage(with: URL(string: driverDetail?.profileImgUrl ?? ""), placeholderImage: profileImagePlaceHolder, options: []) { (image, error, cacheType, newUrl) in
        }
        driverImage.layer.cornerRadius = driverImage.frame.size.width / 2
        driverImage.clipsToBounds = true
        driverImage.layer.borderColor = UIColor.white.cgColor
        driverImage.layer.borderWidth = 3.0
        driverName.text = driverDetail?.firstName
        let departureTime: Date? = dateTimeFormatter?.date(from: (currentRide?.tripTime)!)
        if departureTime != nil {
            rideStartTime.text = "\((getDateString(fromDepartureTime: departureTime))!), \(AppDelegate.getInstance().timeFormatter.string(from: departureTime!))"
        }
        let strLocalized = NSLocalizedString(YOUR_RIDE_STARTS_AT, comment: "")
        if departureTime != nil {
            lblBoardBtnSubText.text = "\(strLocalized) \((getDateString(fromDepartureTime: departureTime))!), \(AppDelegate.getInstance().timeFormatter.string(from: departureTime!))"
        }
        if driverDetail?.chatProfile?.chatUserId == nil {
            messageView.isHidden = true
        }
        //if(!driverDetail.callId)
        //   self.callView.hidden = true;
        print(" status is \(String(describing: currentRide?.driveDetail.status))")
        if (currentRide?.status == "PENDING") {
            callView.isHidden = true
        }
        
        let documents: DriverDetailDocuments? = driverDetail?.documents
        if documents?.isVehicleImgUploaded != nil {
            vehicleImage.sd_setImage(with: URL(string: documents?.vehicleImgUrl ?? ""), placeholderImage: vehicleImagePlaceHolder, options: []) { (image, error, cacheType, newUrl) in
            }
            vehicleImage.layer.borderColor = UIColor.white.cgColor
            vehicleImage.layer.borderWidth = 1.0
            vehicleName.text = documents?.vehicleModel
            vehicleName.isHidden = false
            vehicleNumber.text = documents?.vehicleRegistrationNum
            vehicleNumber.isHidden = false
        }
        if !(currentRide!.driveDetail.etaVal != nil) || ("N/A" == currentRide?.driveDetail.etaVal) {
            etaValue.text = "N/A"
        } else {
            etaValue.text = "\((currentRide?.driveDetail.etaVal)!) \((currentRide?.driveDetail.etaUnit.lowercased())!)"
        }
        //_boardButton.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        lblBoardBtnSubText.isHidden = false
        if !("RUNNING" == currentRide?.driveDetail.status) {
            boardButton.setTitle(NSLocalizedString(VC_CURRENTRIDE_DRIVENOTSTARTED, comment: ""), for: .normal)
            boardButton.isUserInteractionEnabled = false
        } else if currentRide?.hasBoarded != 0 {
            if profileObj?.country == "IN" {
                lblBoardBtnSubText.isHidden = false
                self.sosBtn.isHidden = false  // need to change to visible in next release
                lblBoardBtnSubText.text = NSLocalizedString(use_sos_emergency, comment: "")
            } else {
                //global users
                self.sosBtn.isHidden = true
                lblBoardBtnSubText.text = ""
                boardButton.contentHorizontalAlignment = .center
                boardButton.contentVerticalAlignment = .center
                lblBoardBtnSubText.isHidden = true
            }
            boardButton.setTitle(NSLocalizedString(VC_CURRENTRIDE_COMPLETERIDE, comment: ""), for: .normal)
            
            
            /*CurrentRideTracking *currentRide = [CurrentRideTracking getCurrentRide];
             [self startCurrentRideTrackingWithLat:currentRide.currentLat andLong:currentRide.currentLong];*/
            continueCurrentRideTracking()
        } else {
            if profileObj?.country == "IN" {
                self.sosBtn.isHidden = false  // need to change to visible in next release
            } else {
                //global users
                self.sosBtn.isHidden = true
            }
            boardButton.setTitle(NSLocalizedString(VC_CURRENTRIDE_BOARD, comment: ""), for: .normal)
        }
        
        mapContainerView.createMapView()
        let mapView:MapView = mapContainerView.mapView
        mapView.sourceCoordinate = CLLocationCoordinate2DMake(Double(currentRide!.srcLat)!, Double( currentRide!.srcLong)!)
        mapView.destCoordinate = CLLocationCoordinate2DMake(Double(currentRide!.destLat)!, Double(currentRide!.destLong)!)
        mapView.overviewPolylinePoints = currentRide?.overviewPolylinePoints
        mapView.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0)
        mapView.drawMap()
        isViewInitialised = true
        isCurrentRideStatusChanging = false
        /*self.mapView.refreshTrackingpath = true;
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(refreshTrackingPath:) name:UIApplicationWillEnterForegroundNotification
         object:nil];*/
        
        
    }
    
    @IBAction func dismiss(_ sender: Any) {
        isCurrentRideViewClosed = true
        dismiss(animated: true)
    }
    
    @IBAction func messageDriver(_ sender: Any) {
        let driverDetail: DriverDetail? = currentRide?.driverDetail
        let userContactDetail = UserContactDetail(chatIdentifier: driverDetail?.chatProfile.chatUserId, andCallIdentifier: driverDetail?.callId, andFirstName: driverDetail?.firstName, andLastName: driverDetail?.lastName, andImageUrl: driverDetail?.profileImgUrl)
        let chatNavController: UINavigationController? = ChatUserListController.createChatNavController()
        let chatUserListContoller = chatNavController?.topViewController as? ChatUserListController
        chatUserListContoller?.receivingUser = userContactDetail
        if let aController = chatNavController {
            present(aController, animated: true)
        }
    }
    
    @IBAction func callDriver(_ sender: Any) {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(UserCallRequest(requestType: INITIATECALL, andCalleeId: self.currentRide!.driverDetail.callId), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil && !(response?.responseObject is NSNull) {
                        AppUtilites.openCallApp(withNumber: response?.responseObject as! String)
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    @IBAction func changeRideStatus(_ sender: Any) {
        if CLLocationManager.authorizationStatus() == .denied {
            showAlertForEnablrLocServicesToCalFare()
        } else {
            if(currentRide?.status == "PENDING"){
                showAlertView(strMessage: NSLocalizedString(ride_request_not_yet_accepted, comment: ""), navigationCtrl: self.navigationController!)
                return
            }
            messageHandler.showBlockingLoadView(handler: {
                self.isCurrentRideStatusChanging = true
                CurrentRideLocationTracker.sharedInstance().currentRideLocationTrackerDelegate = self
                CurrentRideLocationTracker.sharedInstance().updateCurrentLocation()
            })
        }
    }
    
    func setCurrentRideLocation(_ currentLocation: CLLocationCoordinate2D) {
        let currentLat = currentLocation.latitude
        let currentLong = currentLocation.longitude
        handleLocationFetch(withLat: currentLat as NSNumber as NSNumber, andLong: currentLong as NSNumber)
    }
    func unable(toFetchCurrentRideLocation error: Error!) {
        handleLocationFetch(withLat: nil, andLong: nil)
    }
    
    func handleLocationFetch(withLat currentLat: NSNumber?, andLong currentLong: NSNumber?) {
        if isCurrentRideStatusChanging {
            var request: RideDriveActionRequest?
            if currentRide?.hasBoarded == 0 {
                request = RideDriveActionRequest(requestType: STARTRIDE, andDriveId: currentRide?.driveId, andRideId: currentRide?.rideId, andUserLat: currentLat, andUserLong: currentLong, andPathLine: nil)
        
                let eventDict = ["CarOwner_Firstname":currentRide?.driverDetail.firstName ?? "","CarOwner_Lastname":currentRide?.driverDetail.lastName ?? "", "source_address":currentRide?.srcAddress ?? "", "destination_address":currentRide?.destAddress ?? "", "trip_Time":currentRide?.tripTime ?? ""] as [String:Any]
                MoEngageEventsClass.callRideBoardEvent(withDataDict: eventDict)
                
                ServerInterface.sharedInstance().getResponse(request, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            self.currentRide?.hasBoarded = 1
                            self.boardButton.setTitle(NSLocalizedString(VC_CURRENTRIDE_COMPLETERIDE, comment: ""), for: .normal)
                            self.lblBoardBtnSubText.isHidden = true
                            self.self.boardButton.contentHorizontalAlignment = .center
                            self.boardButton.contentVerticalAlignment = .center
                            /*CurrentRideTracking *tracking = [CurrentRideTracking createCurrentRideTrackingObject:request withDuration:_currentRide.duration];
                             [CurrentRideUpdateTracker sharedInstance];
                             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                             [[CurrentRideUpdateTracker sharedInstance] updateCurrentRideInfoWithStartTime:tracking.createdTime andDuration:tracking.rideDuration];
                             });
                             [self startCurrentRideTrackingWithLat:currentLat andLong:currentLong];*/
                            if currentLat != nil {
                                self.updateCurrentRideTracking(withLat: currentLat, andLong: currentLong)
                            }
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        }
                    })
                })
            } else {
                /*[CurrentRideTracking completeWithLat:currentLat andLongitude:currentLong withCompletion:^(CurrentRideTracking *currentRide){*/
                // rajesh --- added andPathLine from "" to currentRide?.overviewPolylinePoints
                request = RideDriveActionRequest(requestType: COMPLETERIDE, andDriveId: currentRide?.driveId, andRideId: currentRide?.rideId, andUserLat: currentLat, andUserLong: currentLong, andPathLine: currentRide?.overviewPolylinePoints)
                
                let eventDict = ["CarOwner_Firstname":currentRide?.driverDetail.firstName ?? "","CarOwner_Lastname":currentRide?.driverDetail.lastName ?? "", "source_address":currentRide?.srcAddress ?? "", "destination_address":currentRide?.destAddress ?? "", "trip_Time":currentRide?.tripTime ?? ""] as [String:Any]
                MoEngageEventsClass.callRideCompletedEvent(withDataDict: eventDict)
                
                
                ServerInterface.sharedInstance().getResponse(request, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            self.boardButton.setTitle(NSLocalizedString(VC_CURRENTRIDE_RIDECOMPLETED, comment: ""), for: .normal)
                            self.boardButton.contentHorizontalAlignment = .center
                            self.boardButton.contentVerticalAlignment = .center
                            //[CurrentRideTracking deleteCurrentRide];
                            self.perform(#selector(self.dismiss(_:)), with: self, afterDelay: 1.0)
                            
                        }else{
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                            
                        }
                    });
                });
            }
            
            isCurrentRideStatusChanging = false;
        }else{
            updateCurrentRideTracking(withLat: currentLat, andLong: currentLong)
            
        }
    }
    
    func updateCurrentRideTracking(withLat latitude: NSNumber?, andLong longitude: NSNumber?) {
        if latitude != nil {
            mapContainerView.mapView?.updateTrackingMarker(withLat: latitude, andLong: longitude)
        }
        if !isCurrentRideViewClosed {
            perform(#selector(self.continueCurrentRideTracking), with: self, afterDelay: 60)
        }
    }
    
    
    
    func continueCurrentRideTracking() {
        CurrentRideLocationTracker.sharedInstance().currentRideLocationTrackerDelegate = self
        CurrentRideLocationTracker.sharedInstance().updateCurrentLocation()
    }
    
    
    func getCurrentDayDate() -> Date? {
        let calendar:NSCalendar = NSCalendar.init(identifier: .gregorian)!
        var components: DateComponents = calendar.components([.year, .month, .day, .hour, .minute], from: Date());        components.hour = 0
        components.minute = 0
        components.second = 0
        return calendar.date(from: components)
    }
    
    func getDateString(fromDepartureTime date: Date?) -> String? {
        
        let calendar:NSCalendar = NSCalendar.init(identifier: .gregorian)!
        var components: DateComponents = calendar.components(.day, from: currentDayDate!, to: date!, options: [])
        let difference: Int = components.day!
        if difference == 0 {
            return NSLocalizedString(CMON_GENERIC_TODAY, comment: "")
        } else if difference == 1 {
            return NSLocalizedString(CMON_GENERIC_TOMORROW, comment: "")
        } else {
            return dateFormatter?.string(from: date!)
        }
    }
    
    
    
    class func createCurrentRideNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "CurrentTrip", bundle: Bundle.main)
        let currentRideNavController = storyBoard.instantiateViewController(withIdentifier: "currentRideNavController") as? UINavigationController
        return currentRideNavController
    }
    
    func showAlertForEnablrLocServicesToCalFare() {
        let titleStr = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_MSG_TITLE, comment: "")
        let contentStr = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_CAlC_FARE_DRIVER_MSG, comment: "")
        let okStr = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_BTN_OK_ALERT_TITLE, comment: "")
        let cancelStr = NSLocalizedString(CMON_GENERIC_CANCEL, comment: "")
        
        let alertController = UIAlertController(title: titleStr, message: contentStr, preferredStyle: .alert)
        //We add buttons to the alert controller by creating UIAlertActions:
        let actionOk = UIAlertAction(title: okStr, style: .default, handler: { action in
            if let aString = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(aString)
            }
            
        })
        let actionCancel = UIAlertAction(title: cancelStr, style: .cancel, handler: { action in
            
        })
        alertController.addAction(actionOk)
        alertController.addAction(actionCancel)
        present(alertController, animated: true)
        
    }
    
    func drawCornerRadius(toAView cornerView: UIView?) {
        let maskPath = UIBezierPath(roundedRect: cornerView?.bounds ?? CGRect.zero, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 20.0, height: 20.0))
        //  CALayer *maskLayer = cornerView.layer;
        let maskLayer = CAShapeLayer()
        maskLayer.frame = cornerView?.bounds ?? CGRect.zero
        maskLayer.path = maskPath.cgPath
        cornerView?.layer.mask = maskLayer
        cornerView?.cornerRadius = 12
    }
    
    func drawShadow(toAView shadeView: UIView?) {
        let layer: CALayer? = shadeView?.layer
        layer?.masksToBounds = false
        layer?.shadowOffset = CGSize(width: 0, height: -3)
        layer?.shadowColor = (UIColor(red: 186.0 / 255.0, green: 184.0 / 255.0, blue: 184.0 / 255.0, alpha: 0.8)).cgColor
        layer?.shadowRadius = 5.0
        layer?.shadowOpacity = 1.0
        layer?.cornerRadius = 12
        layer?.shadowPath = (UIBezierPath(roundedRect: layer?.bounds ?? CGRect.zero, cornerRadius: layer?.cornerRadius ?? 0.0)).cgPath
    }
    
    @IBAction func sosBtnTapped(sender:UIButton){
        //emergencyCtrl.myValue = currentRide?.rideId ?? 0
        
        //let storyBoard = UIStoryboard.init(name: "Emergency", bundle: Bundle.main)
        //let emergencyNavCtrl = storyBoard.instantiateViewController(withIdentifier: "EmergencyNavController") as? UINavigationController
        //let emergencyCtrl = storyBoard.instantiateViewController(withIdentifier: "EmergencyController")
        //instantiateViewControllerWithIdentifier:@"EmergencyController"];
        
        //emergencyNavCtrl?.present(emergencyCtrl, animated: true, completion: nil)
        
        //self.navigationController?.present(EmergencyController.createEmergencyNavController()!, animated: true, completion: nil)
        
        
        EmergencyController.sharedInstance.myValue = (currentRide?.rideId ?? 0)
        self.navigationController?.present(EmergencyController.createEmergencyNavController()!, animated: true, completion: nil)
        
    }
    
    /*-(void) startCurrentRideTrackingWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
     [self.mapView createTrackingMarkerWithLat:latitude andLong:longitude];
     [self continueCurrentRideTracking];
     }
     
     -(void)continueCurrentRideTracking{
     [CurrentRideTracking getCurrentRideWithCompletion:^(CurrentRideTracking *currentRideTracking){
     [[NSOperationQueue mainQueue] addOperationWithBlock:^{
     if(currentRideTracking){
     [self.mapView addPolyLine:currentRideTracking.pathArray andLat:currentRideTracking.currentLat andLong:currentRideTracking.currentLong];
     NSString *rideStatus = currentRideTracking.status;
     NSNumber *isLocationTrackingCompleted = currentRideTracking.isLocationTrackingCompleted;
     if(![@"COMPLETED" isEqualToString:rideStatus] && ![isLocationTrackingCompleted intValue] && !isCurrentRideViewClosed){
     [self performSelector:@selector(continueCurrentRideTracking) withObject:self afterDelay:60.0];
     }
     }
     }];
     }];
     }
     - (void)refreshTrackingPath:(NSNotification *)notification {
     _mapView.refreshTrackingpath = true;
     }*/
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
