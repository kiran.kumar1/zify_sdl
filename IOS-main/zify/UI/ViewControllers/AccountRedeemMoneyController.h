//
//  AccountRedeemMoneyController.h
//  zify
//
//  Created by Anurag S Rathor on 28/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageHandler.h"

@interface AccountRedeemMoneyController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UIImageView *withdrawImageView;
@property (nonatomic,weak) IBOutlet UIView *unverifiedUserView;
@property (nonatomic,weak) IBOutlet UILabel *lblIdcardNotUploadValue;
@property (nonatomic,weak) IBOutlet UILabel *availableBalance;
@property (nonatomic,weak) IBOutlet UITextField *amountToRedeem;
@property (nonatomic,weak) IBOutlet UIView *redeemAmountView;
@property (nonatomic,weak) IBOutlet UIView *withdrawButtonView;
@property (nonatomic,weak) IBOutlet UIView *noAccountsView;
@property (nonatomic,weak) IBOutlet UIButton *addBankAccountButton;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet UITableView *tableView;

@property (nonatomic,weak) IBOutlet UILabel *lblZifyWillProcessBasedOnText;
@property (nonatomic,weak) IBOutlet UILabel *lblWithdrawPolicyDescribes;
@property (nonatomic,weak) IBOutlet UIButton *btnHere;
@property (nonatomic,weak) IBOutlet UIButton *btnWithDraw;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barBtnSupport;
@property (strong, nonatomic) IBOutlet UILabel *lblUnableToInitiate;


@end
