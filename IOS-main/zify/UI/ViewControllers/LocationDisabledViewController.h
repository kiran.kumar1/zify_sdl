//
//  LocationDisabledViewController.h
//  zify
//
//  Created by Anurag Rathor on 19/02/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationDisabledViewController : UIViewController{
}
@property(nonatomic,weak) IBOutlet UILabel *messageContentLbl;
- (IBAction)btnLocationServicesEnabledTapped:(UIButton *)sender;

@end
