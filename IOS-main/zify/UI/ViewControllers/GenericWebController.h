//
//  GenericWebController.h
//  zify
//
//  Created by Anurag S Rathor on 03/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GenericWebController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic)  NSString *webViewURL;
+(UINavigationController *)createGenericWebNavController;
@end
