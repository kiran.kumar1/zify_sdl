//
//  PushRideInvoiceController.h
//  zify
//
//  Created by Anurag S Rathor on 24/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RatingView.h"
#import "ScrollableContentViewController.h"
#import "PushTripCompleteNotification.h"
#import "MessageHandler.h"

@interface PushRideInvoiceController : ScrollableContentViewController<RateViewDelegate>
@property (nonatomic,weak) IBOutlet UIImageView *userImage;
@property (nonatomic,weak) IBOutlet UILabel *userName;
@property (nonatomic,weak) IBOutlet UILabel *amount;
@property (nonatomic,weak) IBOutlet UIImageView *sourceIcon;
@property (nonatomic,weak) IBOutlet UILabel *source;
@property (nonatomic,weak) IBOutlet UIImageView *destinationIcon;
@property (nonatomic,weak) IBOutlet UILabel *destination;
@property (nonatomic,weak) IBOutlet RatingView *ratingView;
@property (nonatomic,weak) IBOutlet UITextField *feedbackText;
@property (nonatomic,weak) IBOutlet UILabel *errorText;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,strong) PushTripCompleteNotification *tripCompleteNotification;
+(void)showRideInvoiceControllerWithTripNotification:(PushTripCompleteNotification *)tripCompleteNotification;
@property (weak, nonatomic) IBOutlet UILabel *lblHowWasYourTrip;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@end
