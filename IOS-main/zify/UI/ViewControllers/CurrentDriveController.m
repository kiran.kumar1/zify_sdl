//
//  CurrentDriveController.m
//  zify
//
//  Created by Anurag S Rathor on 29/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "CurrentDriveController.h"
#import "TripDriveRiderDetail.h"
#import "RiderProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RideDriveActionRequest.h"
#import "ServerInterface.h"
#import "LocalNotificationManager.h"
#import "ChatManager.h"
#import "UserContactDetail.h"
#import "UserCallRequest.h"
#import "CurrentDriveContactCell.h"
//#import "DriveInvoiceController.h"
#import "CurrentTripNPendingRatingResponse.h"
#import "AppUtilites.h"
#import "UIImage+initWithColor.h"
#import "WSCoachMarksView.h"
#import "ImageHelper.h"
#import "LocalisationConstants.h"
#import "UniversalAlert.h"
#import "AppDelegate.h"
#import "zify-Swift.h"

//#import "CurrentRideTracking.h"
//#import "CurrentRideUpdateTracker.h"

@implementation CurrentDriveController{
    BOOL isCurrentDriveViewClosed;
    BOOL isCurrentDriveStatusChanging;
    BOOL isViewInitialised;
    NSDateFormatter *dateTimeFormatter;
    NSDateFormatter *dateFormatter;
   // NSDateFormatter *timeFormatter;
    NSDate *currentDayDate;
    NSMutableArray *contactUsers;
    UIImage *profileImagePlaceHolder;
    CGFloat contactTableFooterHeight;
    NSMutableArray *riderMarkerColors;
    NSLayoutConstraint *beforeContactTableAnimateConstraint;
    NSLayoutConstraint *afterContactTableAnimateConstraint;
    
    UIButton *btnCompleteDrive;
    BOOL showTapToConnectView;
    BOOL isShadeApplied;
    UserProfile *profileObj;
    EmergencyController *emrgencyCtrl;
}


-(void) viewDidLoad{
    [super viewDidLoad];
    //emrgencyCtrl = [[EmergencyController alloc]init];
    profileObj = [UserProfile getCurrentUser];
    self.sosBtn.hidden = true;
    isViewInitialised = false;
    isShadeApplied = false;
    if (IS_IPHONE_5) {
        _driveStartTime.fontSize = 11;
        _driveStatusButton.titleLabel.fontSize = 16;
        _lblTapToConnect.fontSize = 10;
    } else if (IS_IPHONE_6) {
        _driveStartTime.fontSize = 12;
        _driveStatusButton.titleLabel.fontSize = 16;
        _lblTapToConnect.fontSize = 10;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _driveStartTime.fontSize = 13;
        _driveStatusButton.titleLabel.fontSize = 18;
        _lblTapToConnect.fontSize = 11;
    }

}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIImage *navigationBackgroundImage = [UIImage imageWithColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.2]];
    [self.navigationController.navigationBar setBackgroundImage:navigationBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:38.0/255.0 green:98.0/255.0 blue:255.0/255.0 alpha:1.0];//UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    
    if(!isViewInitialised) [self initialiseView];
    

    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self showCurrentDriveCoachMarks];
}

-(void)initialiseView{
    
    
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    isCurrentDriveViewClosed = false;
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter setLocale:locale];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
//    timeFormatter = [[NSDateFormatter alloc] init];
//    [timeFormatter setDateFormat:@"HH:mm"];
//    [timeFormatter setLocale:locale];
    currentDayDate = [self getCurrentDayDate];
    
    UIImage *arrowImage = [UIImage imageNamed:@"left_arrow.png"];
    _expandImageView.image = [arrowImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_expandImageView setTintColor:[UIColor colorWithRed:(158.0/255.0) green:(158.0/255.0) blue:(158.0/255.0) alpha:1.0]];
    _expandImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    
    riderMarkerColors = [[NSMutableArray alloc] init];
    [riderMarkerColors addObject:[UIColor colorWithRed:(63.0/255.0) green:(81.0/255.0) blue:(181.0/255.0) alpha:1.0]];
    [riderMarkerColors addObject:[UIColor colorWithRed:(231.0/255.0) green:(76.0/255.0) blue:(58.0/255.0) alpha:1.0]];
    [riderMarkerColors addObject:[UIColor colorWithRed:(33.0/255.0) green:(33.0/255.0) blue:(33.0/255.0) alpha:1.0]];
    [riderMarkerColors addObject:[UIColor colorWithRed:(255.0/255.0) green:(152.0/255.0) blue:(0.0/255.0) alpha:1.0]];
    
    UIImage *riderPlaceHolderImage = [UIImage imageNamed:@"current_ride_seat.png"];
      showTapToConnectView = false;
    
    
    [_riderViews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView *riderView = (UIView *)obj;
        CALayer *layer = riderView.layer;
         layer.cornerRadius = riderView.frame.size.width / 2;
         layer.masksToBounds = NO;
         layer.shadowOffset = CGSizeMake(0, 0);
         layer.shadowColor = [[UIColor colorWithRed:152.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.5] CGColor];
         layer.shadowRadius = 5.0f;
         layer.shadowOpacity = 0.8f;
         layer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:layer.bounds cornerRadius:layer.cornerRadius] CGPath];
         riderView.layer.cornerRadius = riderView.frame.size.width / 2;
    }];
    
    [_riderImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIImageView *riderImage = (UIImageView *)obj;
        [riderImage.layer setCornerRadius:riderImage.frame.size.width / 2];
        [riderImage.layer setBorderColor:[UIColor whiteColor].CGColor];
        [riderImage.layer setBorderWidth:3.0f];
        riderImage.clipsToBounds = YES;
     }];
    
    [_currentDrive.driveRiders enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TripDriveRiderDetail *riderDetail = (TripDriveRiderDetail *)obj;
        RiderProfile *riderProfile = riderDetail.riderProfile;
        if(riderProfile.chatProfile || riderProfile.callId)
            showTapToConnectView = YES;
    }];
    
    if(!showTapToConnectView){
         _tapToContactView.hidden = YES;
        _taptoContactShadeView.hidden = YES;
        _tapToContactView.userInteractionEnabled = NO;
        _riderListView.userInteractionEnabled = YES;
         [_tapToContactView addConstraint:[NSLayoutConstraint constraintWithItem:_tapToContactView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0f]];
    }
    _riderListView.frameWidth = [AppDelegate SCREEN_WIDTH];
    UIView *cornerView = _riderListView;
    UIView *shadeView = _riderListShadeView;
    if(!_tapToContactView.hidden){
        cornerView = _tapToContactView;
        shadeView = _taptoContactShadeView;
    }
    [self drawCornerRadiusToAView:cornerView];
    [self drawShadowToAView:shadeView];
   // cornerView.backgroundColor = [UIColor redColor];
    //[self drawCornerRadiusToAView:self.contactTableView];
   // [self drawShadowToAView:self.contactTableView];
    //self.contactTableView.cornerRadius = 10;
    //self.contactTableView.layer.masksToBounds = true;
    
    [_mapContainerView createMapView];
    MapView *mapView = _mapContainerView.mapView;
    mapView.sourceCoordinate = CLLocationCoordinate2DMake(_currentDrive.srcLat.doubleValue,_currentDrive.srcLong.doubleValue);
    mapView.destCoordinate = CLLocationCoordinate2DMake(_currentDrive.destLat.doubleValue,_currentDrive.destLong.doubleValue);
    mapView.overviewPolylinePoints = _currentDrive.overviewPolylinePoints;
    mapView.mapPadding = UIEdgeInsetsMake(70, 0, 0, 0);
    [mapView drawMap];
    NSDate *departureTime = [dateTimeFormatter dateFromString:_currentDrive.tripTime];
    _driveStartTime.text = [NSString stringWithFormat:@"%@, %@",[self getDateStringFromDepartureTime:departureTime],[[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureTime]];
    [_currentDrive.driveRiders enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TripDriveRiderDetail *riderDetail = (TripDriveRiderDetail *)obj;
        RiderProfile *riderProfile = riderDetail.riderProfile;
        UIImageView *riderImage = [_riderImages objectAtIndex:idx];
        riderImage.hidden = YES;
        [riderImage sd_setImageWithURL:[NSURL URLWithString:riderProfile.profileImageURL] placeholderImage:riderPlaceHolderImage completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
            riderImage.hidden = NO;
            UIImage *sourceMarkerBgImage = [ImageHelper imageWithImage:[UIImage imageNamed:@"trip_user_source_marker.png"] andTintColor:[riderMarkerColors objectAtIndex:idx]];
            UIImage *sourceMarkerFgImage = [ImageHelper imageWithRoundedCornersSize:22 usingImage:image scaledToSize:CGSizeMake(44,44)];
            UIImage *sourceMarkerImage = [ImageHelper drawImage:sourceMarkerFgImage inImage:sourceMarkerBgImage atPoint:CGPointMake(10,2)];
            [mapView createMarkerWithhLat:[NSNumber numberWithDouble:riderDetail.srcLat.doubleValue] andLong:[NSNumber numberWithDouble:riderDetail.srcLong.doubleValue] andMarkerImage:sourceMarkerImage];
            UIImage *destMakerImage =[ImageHelper imageWithImage:[UIImage imageNamed:@"trip_user_dest_marker.png"] andTintColor:[riderMarkerColors objectAtIndex:idx]];
            [mapView createMarkerWithhLat:[NSNumber numberWithDouble:riderDetail.destLat.doubleValue] andLong:[NSNumber numberWithDouble:riderDetail.destLong.doubleValue] andMarkerImage:destMakerImage];
        }];
    }];
    [_driveStartTime setHidden:FALSE];
    if([@"PENDING" isEqualToString:_currentDrive.status]) {
        [_driveStatusButton setTitle:NSLocalizedString(VC_CURRENTDRIVE_STARTDRIVE, nil) forState:UIControlStateNormal];
    } else if([@"RUNNING" isEqualToString:_currentDrive.status]){
        if ([profileObj.country isEqualToString:@"IN"]) {
            [_driveStartTime setHidden:FALSE];
            self.sosBtn.hidden = false;
            [self.driveStartTime setText:NSLocalizedString(use_sos_emergency, nil)];
        } else {
            //global users
            self.sosBtn.hidden = true;
            [self.driveStartTime setText:@""];
            [_driveStartTime setHidden:TRUE];
            _driveStatusButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            _driveStatusButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        }
        //self.sosBtn.hidden = false;  // need to change to visible in next release
        [_driveStatusButton setTitle:NSLocalizedString(VC_CURRENTDRIVE_COMPLETEDRIVE, nil) forState:UIControlStateNormal];
        
        if(self.navigationItem.rightBarButtonItem){
            self.navigationItem.rightBarButtonItem = nil;
        }
       /* CurrentRideTracking *currentRide = [CurrentRideTracking getCurrentRide];
        [self startCurrentDriveTrackingWithLat:currentRide.currentLat andLong:currentRide.currentLong];*/
       [self continueCurrentDriveTracking];
    }
    /*self.mapView.refreshTrackingpath = true;
    [[NSNotificationCenter defaultCenter] addObserver:self
        selector:@selector(refreshTrackingPath:) name:UIApplicationWillEnterForegroundNotification
                                               object:nil];*/
   
    isViewInitialised = true;
    isCurrentDriveStatusChanging = false;
}
-(void)drawCornerRadiusToAView:(UIView *)cornerView{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cornerView.bounds byRoundingCorners:( UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = cornerView.bounds;
    maskLayer.path  = maskPath.CGPath;
    cornerView.layer.mask = maskLayer;
}
-(void)drawShadowToAView:(UIView *)shadeView{
    CALayer *layer = shadeView.layer;
    layer.masksToBounds = NO;
    layer.shadowOffset = CGSizeMake(0, -3);
    layer.shadowColor = [[UIColor colorWithRed:186.0/255.0 green:184.0/255.0 blue:184.0/255.0 alpha:0.8] CGColor];
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 1.0f;
    layer.cornerRadius = 12;
    layer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:layer.bounds cornerRadius:layer.cornerRadius] CGPath];
}

#pragma mark -
#pragma mark Table view delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row != 0){
        CurrentDriveContactCell * contactCell = (CurrentDriveContactCell *)cell;
        CGRect userDetailsViewFrame = contactCell.userDetailsView.frame;
        userDetailsViewFrame.origin.x = 0;
        contactCell.userDetailsView.frame = userDetailsViewFrame;
        
       /* [UIView animateWithDuration:0.3f delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
            contactCell.userDetailsView.frame = userDetailsViewFrame;
        } completion:nil];*/
        
        CGRect contactUserViewFrame = contactCell.contactUserView.frame;
        contactUserViewFrame.origin.x = CGRectGetWidth(self.view.frame) - CGRectGetWidth(contactUserViewFrame);
        contactCell.contactUserView.frame = contactUserViewFrame;

     /*   [UIView animateWithDuration:0.3f delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
            contactCell.contactUserView.frame = contactUserViewFrame;
        } completion:nil];*/
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CurrentDriveContactCell *contactCell;
    if(indexPath.row == 0){
        contactCell = [tableView dequeueReusableCellWithIdentifier:@"contactHeaderCell"];
    } else{
        contactCell = [tableView dequeueReusableCellWithIdentifier:@"contactCell"];
        CGRect userDetailsViewFrame = contactCell.userDetailsView.frame;
        userDetailsViewFrame.origin.x = -CGRectGetWidth(contactCell.userDetailsView.frame);
        contactCell.userDetailsView.frame = userDetailsViewFrame;
        CGRect contactUserViewFrame = contactCell.contactUserView.frame;
        contactUserViewFrame.origin.x = CGRectGetWidth(self.view.frame);
        contactCell.contactUserView.frame = contactUserViewFrame;
        UserContactDetail *contactDetail = [contactUsers objectAtIndex:indexPath.row - 1];
        contactCell.userImage.hidden = YES;
        contactCell.userImage.layer.cornerRadius = contactCell.userImage.frame.size.width / 2;
        contactCell.userImage.clipsToBounds = YES;
        [contactCell.userImage sd_setImageWithURL:[NSURL URLWithString:contactDetail.userImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
            contactCell.userImage.hidden = NO;
        }];
        contactCell.userName.text = [NSString stringWithFormat:@"%@ %@",contactDetail.firstName,contactDetail.lastName];
        if(contactDetail.chatIdentifier) contactCell.chatButton.hidden = NO;
        else contactCell.chatButton.hidden = YES;
        if(contactDetail.callId) contactCell.callButton.hidden = NO;
        else contactCell.callButton.hidden = YES;
        if(indexPath.row == contactUsers.count) contactCell.separatorLine.hidden = YES;
        else contactCell.separatorLine.hidden = NO;
        
        [contactCell.chatButton addTarget:self action:@selector(chatBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        [contactCell.callButton addTarget:self action:@selector(callBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return contactCell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [contactUsers count] +  1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return contactTableFooterHeight;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
  //  view.frame = CGRectMake(0, 0, tableView.frameWidth, 1);
  //  [self drawShadowToAView:view];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(!showTapToConnectView)
        return;
    if(indexPath.row == 0){
        [self.view removeConstraint:afterContactTableAnimateConstraint];
        [self.view addConstraint:beforeContactTableAnimateConstraint];
        [UIView animateWithDuration:0.3f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
              [_contactTableView removeFromSuperview];
        }];
    }
}

-(IBAction) showChat:(id)sender{
   /* NSIndexPath *indexPath = [_contactTableView indexPathForCell:[self getTableViewCell:sender]];
    UserContactDetail *contactDetail = [contactUsers objectAtIndex:indexPath.row - 1];
    UINavigationController *chatNavController= [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    chatUserListContoller.receivingUser = contactDetail;
    [self presentViewController:chatNavController animated:YES completion:nil];*/
}
-(void)chatBtnTapped:(id)sender{
    NSIndexPath *indexPath = [_contactTableView indexPathForCell:[self getTableViewCell:sender]];
    UserContactDetail *contactDetail = [contactUsers objectAtIndex:indexPath.row - 1];
    if(contactDetail.chatIdentifier == nil){
        [UIAlertController showErrorWithMessage:@"Sorry!. Unable to chat right now. Please try again." inViewController:self];
        return;
    }
    UINavigationController *chatNavController= [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    chatUserListContoller.receivingUser = contactDetail;
    [self presentViewController:chatNavController animated:YES completion:nil];
}

-(IBAction) showCall:(id)sender{
   /* NSIndexPath *indexPath = [_contactTableView indexPathForCell:[self getTableViewCell:sender]];
    UserContactDetail *contactDetail = [contactUsers objectAtIndex:indexPath.row - 1];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[UserCallRequest alloc] initWithRequestType:INITIATECALL andCalleeId:contactDetail.callId] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response && ![response.responseObject isKindOfClass:[NSNull class]]){
                    [AppUtilites openCallAppWithNumber:response.responseObject];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];*/
}
-(void)callBtnTapped:(id)sender{
    NSIndexPath *indexPath = [_contactTableView indexPathForCell:[self getTableViewCell:sender]];
    UserContactDetail *contactDetail = [contactUsers objectAtIndex:indexPath.row - 1];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[UserCallRequest alloc] initWithRequestType:INITIATECALL andCalleeId:contactDetail.callId] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response && ![response.responseObject isKindOfClass:[NSNull class]]){
                    [AppUtilites openCallAppWithNumber:response.responseObject];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(IBAction) showUserContactDetails:(id)sender{
    if(!showTapToConnectView){
        return;
    }
    BOOL isInitialTableLoad = false;
    if(!contactUsers){
        contactUsers = [[NSMutableArray alloc] init];
       // BOOL isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
        [_currentDrive.driveRiders enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            TripDriveRiderDetail *riderDetail = (TripDriveRiderDetail *)obj;
            RiderProfile *riderProfile = riderDetail.riderProfile;
            if(riderProfile.chatProfile || riderProfile.callId){
                UserContactDetail *userContactDetail = [[UserContactDetail alloc] initWithChatIdentifier:riderProfile.chatProfile.chatUserId andCallIdentifier:riderProfile.callId andFirstName:riderProfile.firstName andLastName:riderProfile.lastName andImageUrl:riderProfile.profileImageURL];
                [contactUsers addObject:userContactDetail];
            }
        }];
        _contactTableView.estimatedRowHeight = 80.0f;
        _contactTableView.rowHeight = UITableViewAutomaticDimension;
        isInitialTableLoad = true;
    }
    CGFloat contactTableViewHeight = 30.0f + (contactUsers.count * 80.0f);
    CGFloat heightDiff = CGRectGetHeight(_riderListView.frame) + CGRectGetHeight(_tapToContactView.frame) - contactTableViewHeight;
    contactTableFooterHeight = heightDiff > 0 ? heightDiff : 0.0f;
    contactTableViewHeight += contactTableFooterHeight;
     [self.view insertSubview:_contactTableView belowSubview:_driveStatusButton];
    _contactTableView.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *viewsDictionary = @{@"contactTableView":_contactTableView};
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[contactTableView]|" options:0 metrics:nil views:viewsDictionary];
    beforeContactTableAnimateConstraint = [NSLayoutConstraint constraintWithItem:_contactTableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_driveStatusButton attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
    afterContactTableAnimateConstraint = [NSLayoutConstraint constraintWithItem:_contactTableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_driveStatusButton attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
    [self.view addConstraints:horizontalConsts];
    [self.view addConstraint:beforeContactTableAnimateConstraint];
    [self.view layoutIfNeeded];
    [self.view removeConstraint:beforeContactTableAnimateConstraint];
    [self.view addConstraint:afterContactTableAnimateConstraint];
    
    
    
    if(!isInitialTableLoad){
        [_contactTableView reloadData];

    } else{
        [_contactTableView addConstraint:[NSLayoutConstraint constraintWithItem:_contactTableView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:contactTableViewHeight]];
    }
    [UIView animateWithDuration:0.3f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {

    }];
    if(!isShadeApplied){
        isShadeApplied = YES;
    [self drawShadowToAView:self.contactTableView];
    }

    

}
-(IBAction) dismiss:(id)sender{
    isCurrentDriveViewClosed = true;
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction) changeDriveStatus:(id)sender{
    btnCompleteDrive = (UIButton*)sender;
    [btnCompleteDrive setUserInteractionEnabled:NO];
    [self doActionPerformBasedOnStatus];
}

-(void)doActionPerformBasedOnStatus{
    if([@"PENDING" isEqualToString:_currentDrive.status]){
        NSInteger minutes;
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components: NSCalendarUnitMinute fromDate:[NSDate date] toDate:[dateTimeFormatter dateFromString:_currentDrive.tripTime] options:0];
        minutes = [components minute];
        if(minutes > 30) {
            [btnCompleteDrive setUserInteractionEnabled:YES];
            [self.messageHandler showErrorMessage:NSLocalizedString(VC_CURRENTDRIVE_STARTDRIVEVAL, nil)];
            return;
        }
    }
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        [self showAlertForEnablrLocServicesToCalFare];
        [btnCompleteDrive setUserInteractionEnabled:YES];
    }else{
        
        
      //  dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            isCurrentDriveStatusChanging = true;
            [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = self;
            [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
        }];
            
      //  });
    }
}

-(void)startDriveFromSDL:(TripDrive *)driveINfo{
    _currentDrive = driveINfo;
    [self doActionPerformBasedOnStatus];
}

-(void) setCurrentRideLocation:(CLLocationCoordinate2D)currentLocation{
    NSNumber *currentLat = [NSNumber numberWithDouble:currentLocation.latitude];
    NSNumber *currentLong = [NSNumber numberWithDouble:currentLocation.longitude];
    [self handleLocationFetchWithLat:currentLat andLong:currentLong];
}

-(void) unableToFetchCurrentRideLocation:(NSError *)error{
   [self handleLocationFetchWithLat:nil andLong:nil];
}


-(void)handleLocationFetchWithLat:(NSNumber *)currentLat andLong:(NSNumber *)currentLong{
    if(isCurrentDriveStatusChanging){
        __block RideDriveActionRequest *request;
        if([@"PENDING" isEqualToString:_currentDrive.status]){
            request = [[RideDriveActionRequest alloc] initWithRequestType:STARTDRIVE andDriveId:_currentDrive.driveId andUserLat:currentLat andUserLong:currentLong andPathLine:nil];
            
            NSDictionary *eventDict = [NSDictionary dictionaryWithObjectsAndKeys: _currentDrive.srcAddress,@"source_address", _currentDrive.destAddress, @"destination_address", _currentDrive.tripTime,@"trip_Time",nil];
            [MoEngageEventsClass callDriveStartEventWithDataDict:eventDict];
            [[ServerInterface sharedInstance] getResponse:request withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        //self.sosBtn.hidden = false;  // need to change to visible in next release
                        if ([profileObj.country isEqualToString:@"IN"]) {
                            self.sosBtn.hidden = false;
                        } else {
                            //global users
                            self.sosBtn.hidden = true;
                        }
                        _currentDrive.status = @"RUNNING";
                        [_driveStatusButton setTitle:NSLocalizedString(VC_CURRENTDRIVE_COMPLETEDRIVE, nil) forState:UIControlStateNormal];
                        
                        if(_sosBtn.isHidden) {
                            [_driveStartTime setHidden:TRUE];
                            _driveStatusButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
                            _driveStatusButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                            [self.driveStartTime setText:@""];
                        } else {
                            [_driveStartTime setHidden:FALSE];
                            [self.driveStartTime setText:NSLocalizedString(use_sos_emergency, nil)];
                        }
                        
                        
                        if(self.navigationItem.rightBarButtonItem){
                            self.navigationItem.rightBarButtonItem = nil;
                        }
                        
                        
                        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_CURRENTDRIVE_CURRENTDRIVE, nil) WithMessage:NSLocalizedString(VC_CURRENTDRIVE_DRIVESTARTED,nil)];
                        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                            
                        }];
                        [alert showInViewController:self];
                        /* CurrentRideTracking *tracking = [CurrentRideTracking createCurrentRideTrackingObject:request withDuration:_currentDrive.duration];
                         [CurrentRideUpdateTracker sharedInstance];
                         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         [[CurrentRideUpdateTracker sharedInstance] updateCurrentRideInfoWithStartTime:tracking.createdTime andDuration:tracking.rideDuration];
                         });
                         [self startCurrentDriveTrackingWithLat:currentLat andLong:currentLong];*/
                       
                        if(currentLat)[self updateCurrentDriveTrackingWithLat:currentLat andLong:currentLong];
                        [[LocalNotificationManager sharedInstance] cancelNotificationWithIdentifier:_currentDrive.driveId.stringValue andType:DRIVEALARM];
                        [btnCompleteDrive setUserInteractionEnabled:YES];
                    } else{
                        [btnCompleteDrive setUserInteractionEnabled:YES];
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        } else if([@"RUNNING" isEqualToString:_currentDrive.status]){
            /*[CurrentRideTracking completeWithLat:currentLat andLongitude:currentLong withCompletion:^(CurrentRideTracking *currentRide){*/
            request = [[RideDriveActionRequest alloc] initWithRequestType:COMPLETEDRIVE andDriveId:_currentDrive.driveId andUserLat:currentLat andUserLong:currentLong andPathLine:@""];
            NSDictionary *eventDict = [NSDictionary dictionaryWithObjectsAndKeys: _currentDrive.srcAddress,@"source_address", _currentDrive.destAddress, @"destination_address", _currentDrive.tripTime,@"trip_Time",nil];
            [MoEngageEventsClass callDriveCompletedEventWithDataDict:eventDict];
            
            [[ServerInterface sharedInstance] getResponse:request withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        _currentDrive.status = @"COMPLETED";
                        [_driveStatusButton setTitle:NSLocalizedString(VC_CURRENTDRIVE_DRIVECOMPLETED, nil) forState:UIControlStateNormal];
                        [_driveStartTime setHidden:TRUE];
                        _driveStatusButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
                        _driveStatusButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                        
                        //[CurrentRideTracking deleteCurrentRide];
                        TripDrive *completedDrive = (TripDrive *)response.responseObject;
                        if(completedDrive.driveRiders.count >0){
                            UINavigationController *driveInvoiceNavController = (UINavigationController *) [DriveInvoiceController createDriveInvoiceNavController];
                            DriveInvoiceController *driveInvoiceController = (DriveInvoiceController *)driveInvoiceNavController.topViewController;
                            CurrentTripNPendingRatingResponse *ratingResponse = [[CurrentTripNPendingRatingResponse alloc] init];
                            ratingResponse.pendingDriveRating = completedDrive;
                            driveInvoiceController.ratingResponse =  ratingResponse;
                            driveInvoiceController.showEmptyDrive = false;
                            isCurrentDriveViewClosed = true;
                            [self dismissViewControllerAnimated:NO completion:^{
                                [[AppUtilites applicationVisibleViewController]  presentViewController:driveInvoiceNavController animated:YES completion:nil];
                            }];
                        } else{
                             [self performSelector:@selector(dismiss:) withObject:self afterDelay:1.0];
                        }
                    } else{
                        [btnCompleteDrive setUserInteractionEnabled:YES];
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
            //}];
        }
        isCurrentDriveStatusChanging = false;
    } else{
         [self updateCurrentDriveTrackingWithLat:currentLat andLong:currentLong];
    }
}

//New methods after removing user travelling path tracking
-(void) updateCurrentDriveTrackingWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
    if(latitude)[_mapContainerView.mapView updateTrackingMarkerWithLat:latitude andLong:longitude];
    if(!isCurrentDriveViewClosed){
        [self performSelector:@selector(continueCurrentDriveTracking) withObject:self afterDelay:60];
    }
}

-(void)continueCurrentDriveTracking{
    [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = self;
    [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
}

-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}


-(void)showCurrentDriveCoachMarks{
    
    BOOL isTutorialShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"currentDriveTutorialShown"];
    if(!isTutorialShown)
    {
        NSArray *coachMarks = @[
                                @{@"rect": [NSValue valueWithCGRect:(CGRect){[self.riderListShadeView convertPoint:_driveStatusButton.frame.origin toView:self.view],{_driveStatusButton.frame.size.width,_driveStatusButton.frame.size.height}}],
                                  @"caption": NSLocalizedString(VC_CURRENTDRIVE_STARTDRIVETUT, nil),
                                  @"shape": @"square"
                                  },
                                ];
        WSCoachMarksView *coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
        coachMarksView.animationDuration = 0.3f;
        coachMarksView.enableContinueLabel = YES;
        coachMarksView.enableSkipButton = NO;
        coachMarksView.maskColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
        [self.navigationController.view addSubview:coachMarksView];
        [coachMarksView start];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"currentDriveTutorialShown"];
    }
}


-(NSDate *)getCurrentDayDate{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [calendar dateFromComponents:components];
}

-(NSString *)getDateStringFromDepartureTime:(NSDate *)date{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:currentDayDate toDate:date options:0];
    NSInteger difference = [components day];
    if(difference == 0) return NSLocalizedString(CMON_GENERIC_TODAY, nil);
    else if(difference == 1) return NSLocalizedString(CMON_GENERIC_TOMORROW, nil);
    else return [dateFormatter stringFromDate:date];
}

+(UINavigationController *)createCurrentDriveNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"CurrentTrip" bundle:[NSBundle mainBundle]];
    UINavigationController *currentDriveNavController = [storyBoard instantiateViewControllerWithIdentifier:@"currentDriveNavController"];
    return currentDriveNavController;
}
-(void)showAlertForEnablrLocServicesToCalFare{
    NSString *titleStr = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_MSG_TITLE, nil);
    NSString *contentStr = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_CAlC_FARE_DRIVER_MSG, nil);
    NSString *okStr = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_BTN_OK_ALERT_TITLE, nil);
    NSString *cancelStr = NSLocalizedString(CMON_GENERIC_CANCEL, nil);
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titleStr
                                                                             message:contentStr preferredStyle:UIAlertControllerStyleAlert];
    //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:okStr style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        
    }];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:cancelStr style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:actionOk];
    [alertController addAction:actionCancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)addDropShadowForImageView:(UIImageView *)viewShadow shadowRadius:(int)shadaowradius {

    viewShadow.backgroundColor = [UIColor clearColor];
    viewShadow.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = CGSizeMake(0, 0);
    viewShadow.layer.shadowRadius = shadaowradius;
    viewShadow.layer.masksToBounds = NO;
}

-(IBAction)shareDrive:(id)sender{
    NSLog(@"clicked share drive To Friends");
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[UserGetDriveDeepLinkUrlRequest alloc] initWithDriveId:_currentDrive.driveId withDriverId:_currentDrive.driverId] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    if ([response.messageCode isEqualToString:@"1"] ){
                        NSDictionary *resultDict = response.responseObject;
                        NSString *deepLinkUrl = [resultDict objectForKey:@"responseText"];
                        [[AppDelegate getAppDelegateInstance] shareDriveDeepLinkIdWithFriends:deepLinkUrl fromScreen:self];
                    }else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}
    
    
    
    
-(IBAction)sosBtnTapped:(id)sender{
    //UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Emergency" bundle:[NSBundle mainBundle]];
    //EmergencyController *emergencyCtrl = [stb instantiateViewControllerWithIdentifier:@"EmergencyController"];
    //NSString *strNumber = [NSString stringWithFormat:@"%@", _currentDrive.driveId];
    
    //emergencyCtrl.myValue = strNumber;
    
    
   // [self.navigationController presentViewController:emergencyCtrl.createEmergencyNavController animated:TRUE completion:nil];
    [EmergencyController sharedInstance].myValue = _currentDrive.driveId;
    [self.navigationController presentViewController:[EmergencyController createEmergencyNavController] animated:TRUE completion:nil];
}





/*-(void) startCurrentDriveTrackingWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
    [self.mapView createTrackingMarkerWithLat:latitude andLong:longitude];
    [self continueCurrentDriveTracking];
}

-(void)continueCurrentDriveTracking{
    [CurrentRideTracking getCurrentRideWithCompletion:^(CurrentRideTracking *currentRideTracking){
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if(currentRideTracking){
                [self.mapView addPolyLine:currentRideTracking.pathArray andLat:currentRideTracking.currentLat andLong:currentRideTracking.currentLong];
                NSString *rideStatus = currentRideTracking.status;
                NSNumber *isLocationTrackingCompleted = currentRideTracking.isLocationTrackingCompleted;
                if(!isCurrentDriveViewClosed && ![@"COMPLETED" isEqualToString:rideStatus] && ![isLocationTrackingCompleted intValue]){
                    [self performSelector:@selector(continueCurrentDriveTracking) withObject:self afterDelay:60];
                }
             }
        }];
    }];
}
 
-(void)refreshTrackingPath:(NSNotification *)notification {
    _mapView.refreshTrackingpath = true;
}*/
@end
//Code used for testing for tracking user travelling path
/*CurrentRideTracking *temp = [CurrentRideTracking getCurrentRide];
 [self.mapView createTrackingMarkerWithLat:temp.currentLat andLong:temp.currentLong];
 [self.mapView addPolyLineString:temp.encodedPolyLine andLat:temp.currentLat andLong:temp.currentLong];*/
/* CurrentRideTracking *currentRideTracking = [CurrentRideTracking getCurrentRide];
 [self.mapView addPolyLine:currentRideTracking.encodedPolyLine andLat:currentRideTracking.currentLat andLong:currentRideTracking.currentLong];
 count ++;
 if(count <= 100){
 [self performSelector:@selector(continueCurrentDriveTracking) withObject:self afterDelay:5.0];
 }*/
