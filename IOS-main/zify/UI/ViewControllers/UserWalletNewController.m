//
//  UserWalletController.m
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserWalletNewController.h"
#import "ServerInterface.h"
#import "GenericRequest.h"
#import "UserWallet.h"
#import "CurrentLocale.h"
#import "UserProfile.h"
#import "RazorpayPaymentPersistRequest.h"
#import "StripePaymentPersistRequest.h"
#import "UniversalAlert.h"
#import "LocalisationConstants.h"
#import "IQKeyboardManager.h"
#import "IQUIView+Hierarchy.h"
#import "AppDelegate.h"
#import "zify-Swift.h"
#import "AddBankAccountController.h"
#import "AccountRedeemMoneyController.h"
#import "SettingsBankAccountsController.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@implementation UserWalletNewController{
    NSArray *amountArray;
    NSString *zifyPaymentRefId;
    BOOL isViewInitialised;
    NSString *currencySymbol;
    NSString *currencyCode;
    RazorpayPaymentController *razorpayController;
    
    NSArray *arrListImages;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialiseView];
    
    _arrList = [[NSArray alloc]initWithObjects: NSLocalizedString(WA_ADD_MONEY, nil), NSLocalizedString(WA_WITHDRAW, nil), NSLocalizedString(WA_BANK_ACCOUNT, nil), NSLocalizedString(WA_STATEMENT, nil), nil];
    arrListImages = [[NSArray alloc]initWithObjects:@"addmoney",@"withdraw",@"bankAccount",@"statement", nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised){
        [self fetchUserWallet];
        isViewInitialised = true;
        if(_rechargeAmount) self.amount.text = _rechargeAmount;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)selectAmount:(UIButton *)sender{
    for (int index = 0; index < [self.amountButtons count]; index++){
        UIButton *button = [self.amountButtons objectAtIndex:index];
        if(sender == button){
            self.amount.text = [amountArray objectAtIndex:index];
            button.backgroundColor = [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1.0];
        } else{
            button.backgroundColor = [UIColor whiteColor];
        }
    }
}

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)proceedToPayment:(UIButton *)sender{
    if([self validateAmount]){
        BOOL isRazorpayPaymentEnabled = [[CurrentLocale sharedInstance] isRazorpayPaymentEnabled];
        if(isRazorpayPaymentEnabled){
            [self.messageHandler showBlockingLoadViewWithHandler:^{
                [[ServerInterface sharedInstance] getResponse:[[RazorpayPaymentPersistRequest alloc] initWithRequestType:PAYMENTREQUEST andAmount:self.amount.text andCurrency:@"INR"] withHandler:^(ServerResponse *response, NSError *error){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        if(response && ![response.responseObject isKindOfClass:[NSNull class]] && ![@"" isEqualToString:response.responseObject]){
                            zifyPaymentRefId = response.responseObject;
                            [razorpayController initiatePaymentWithAmount:self.amount.text];
                        } else{
                            [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                        }
                    }];
                }];
            }];
        } else{
            [self.messageHandler showBlockingLoadViewWithHandler:^{
                [[ServerInterface sharedInstance] getResponse:[[StripePaymentPersistRequest alloc] initWithRequestType:STRIPEPAYMENTREQUEST andAmount:self.amount.text andCurrency:currencyCode] withHandler:^(ServerResponse *response, NSError *error){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        if(response && ![response.responseObject isKindOfClass:[NSNull class]] && ![@"" isEqualToString:response.responseObject]){
                            zifyPaymentRefId = response.responseObject;
                            UINavigationController *stripePaymentNavController = [StripePaymentController createStripePaymentNavController];
                            StripePaymentController *stripePaymentController = (StripePaymentController *)stripePaymentNavController.topViewController;
                            stripePaymentController.amount = self.amount.text;
                            stripePaymentController.paymentDelegate = self;
                            [self presentViewController:stripePaymentNavController animated:YES completion:nil];
                        } else{
                            [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                        }
                    }];
                }];
            }];
        }
    }
}

-(BOOL)validateAmount{

    if([@"" isEqualToString:self.amount.text]){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_WALLET_AMOUNTVAL, nil)];
        return false;
    }
    
    NSInteger iamount = [self.amount.text integerValue];
    if (iamount > 1000 || iamount == 1000) {
//        [self.messageHandler showErrorMessage:NSLocalizedString(VC_WALLET_AMOUNTVAL_LESSTHAN_THOUSAND, nil)];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AppName
                                                        message:NSLocalizedString(VC_WALLET_AMOUNTVAL_LESSTHAN_THOUSAND, nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Ok",nil];
        [alert show];
        
        return false;
    }
    int amountValue = [self.amount.text intValue];
    if(amountValue <= 0){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_WALLET_AMOUNTVAL_ZERO, nil)];
        return false;
    }
    return true;
   /* int minimumAmount = [[amountArray objectAtIndex:0] intValue];
    if(self.amount.text.intValue < minimumAmount) {
        NSString *alertMessage = [NSString stringWithFormat:@"Please recharge with amount >= %d",minimumAmount];
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:@"Insufficient Recharge Amount" WithMessage:alertMessage];
        [alert addButton:BUTTON_OK WithTitle:@"OK" WithAction:^(void *action){
           
        }];
        [alert showInViewController:self];
        return false;
    }
    return true;*/
}

-(void)initialiseView{
    self.scrollview.hidden = true;
    self.addMoney.hidden = true;
    isViewInitialised = false;
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    amountArray = [currentLocale getRechargeAmounts];
    currencySymbol = [currentLocale getCurrencySymbol];
    currencyCode = [currentLocale getCurrencyCode];
    //
    NSString *placeHolderStr = [NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(WS_WALLET_TXT_ENTERAMOUNT, nil),currencySymbol];
    self.amount.placeholder = placeHolderStr;
    self.amount.layer.borderColor =  [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1.0].CGColor;
    for (int index = 0; index < [self.amountButtons count]; index++){
        UIButton *button = [self.amountButtons objectAtIndex:index];
        NSString *title = [NSString stringWithFormat:@"+ %@%d",currencySymbol,[[amountArray objectAtIndex:index] intValue]];
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitle:title forState:UIControlStateSelected];
        button.layer.borderColor = [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1.0].CGColor;
    }
    razorpayController = [[RazorpayPaymentController alloc] init];
    razorpayController.paymentDelegate = self;
}

-(void)fetchUserWallet{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[GenericRequest alloc] initWithRequestType:RIDERWALLETREQUEST] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    UserWallet *wallet = (UserWallet *)response.responseObject;
                    self.availableCash.text =[NSString stringWithFormat:@"%@ %@",currencySymbol, wallet.zifyCash.stringValue];
                    self.availablePoints.text = [NSString stringWithFormat:NSLocalizedString(VC_WALLET_PROMOCREDITS, @"Credits : {UserPromotionalCredits}"),wallet.availablePoints.stringValue];
                    self.scrollview.hidden = false;
                    self.addMoney.hidden = false;
                    [self moveToAddMoneyScreenFromSearchResultToAdd];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}
-(void)moveToAddMoneyScreenFromSearchResultToAdd{
    BOOL isForAddMoney = [[NSUserDefaults standardUserDefaults] boolForKey:@"toAddMoney"];
    if(isForAddMoney){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"toAddMoney"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Wallet" bundle:[NSBundle mainBundle]];
        UserWalletController *userWalletController = [sb instantiateViewControllerWithIdentifier:@"UserWalletController"];
        [self.navigationController pushViewController:userWalletController animated:TRUE];
    }
}

-(void) razorpayPaymentSuccuessWithPaymentId:(NSString *)paymentId{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:[[RazorpayPaymentPersistRequest alloc] initWithRequestType:PAYMENTRESPONSE andPaymentId:paymentId andZifyPaymentRefId:zifyPaymentRefId andFailureCode:nil andFailureDescription:nil] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [self fetchUserWallet];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void) razorpayPaymentFailedWithCode:(int)code andMessage:(NSString *)message{
    [[ServerInterface sharedInstance]getResponse:[[RazorpayPaymentPersistRequest alloc] initWithRequestType:PAYMENTRESPONSE andPaymentId:nil andZifyPaymentRefId:zifyPaymentRefId andFailureCode:[NSNumber numberWithInt:code] andFailureDescription:message] withHandler:^(ServerResponse *response, NSError *error){
        
    }];
    [self.messageHandler showErrorMessage:NSLocalizedString(VC_WALLET_TRYAGAINERROR, nil)];
}

-(void) createStripePaymentWithToken:(STPToken *)token{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:[[StripePaymentPersistRequest alloc] initWithRequestType:STRIPEPAYMENTRESPONSE andStripeToken:token.stripeID andZifyPaymentRefId:zifyPaymentRefId andFailureCode:nil andFailureDescription:nil] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [self fetchUserWallet];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void) stripePaymentFailedWithError:(NSError *)error{
    [[ServerInterface sharedInstance]getResponse:[[StripePaymentPersistRequest alloc] initWithRequestType:STRIPEPAYMENTRESPONSE andStripeToken:nil andZifyPaymentRefId:zifyPaymentRefId andFailureCode:[NSNumber numberWithInt:(int)error.code] andFailureDescription:[error localizedDescription]] withHandler:^(ServerResponse *response, NSError *error){
    }];
    [self.messageHandler showErrorMessage:@"Please try again."];
}

+(UINavigationController *)createUserWalletNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Wallet" bundle:[NSBundle mainBundle]];
    UINavigationController *rechargeNavController = [storyBoard instantiateViewControllerWithIdentifier:@"userWalletNavController"];
    return rechargeNavController;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.amount && [AppDelegate SCREEN_HEIGHT] <= 568)
    {
        if  (self.view.frame.origin.y >= 0)
        {
            //[self setViewMovedUp:YES];
        }
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if([AppDelegate SCREEN_HEIGHT] <= 568){
       // [self setViewMovedUp:NO];
    }
}
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}



#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellidentifier = @"tableviewcell";
    WalletTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    cell.lblContent.text = [_arrList objectAtIndex:indexPath.section];
    cell.leftIconImageView.image = [UIImage imageNamed:[arrListImages objectAtIndex:indexPath.section]];
    cell.rightIconImageView.image = [UIImage imageNamed:@"publish_right_arrow.png"];
    [self addDropShadowForView:cell.contentBgView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
 }

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[_arrList objectAtIndex:indexPath.section]  isEqual: NSLocalizedString(WA_ADD_MONEY, "")]){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Wallet" bundle:[NSBundle mainBundle]];
        UserWalletController *userWalletController = [sb instantiateViewControllerWithIdentifier:@"UserWalletController"];
        [self.navigationController pushViewController:userWalletController animated:TRUE];
        
        //[self.navigationController pushViewController:[UserWalletController createUserWalletNavController] animated:TRUE];

    } else if([[_arrList objectAtIndex:indexPath.section]  isEqual: NSLocalizedString(WA_WITHDRAW, "")]){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Account" bundle:[NSBundle mainBundle]];
        AccountRedeemMoneyController *accountRedeemMonyVc = [sb instantiateViewControllerWithIdentifier:@"AccountRedeemMoneyController"];
        [self.navigationController pushViewController:accountRedeemMonyVc animated:TRUE];
        
        //[self performSegueWithIdentifier:@"showWithdrawMoney" sender:self];
    } else if([[_arrList objectAtIndex:indexPath.section]  isEqual: NSLocalizedString(WA_BANK_ACCOUNT, "")]){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"UserSettings" bundle:[NSBundle mainBundle]];
        SettingsBankAccountsController *settingBankAccVc = [sb instantiateViewControllerWithIdentifier:@"SettingsBankAccountsController"];
        [self.navigationController pushViewController:settingBankAccVc animated:TRUE];
        
        //[self performSegueWithIdentifier:@"showBankAccounts" sender:self];
    } else if([[_arrList objectAtIndex:indexPath.section]  isEqual: NSLocalizedString(WA_STATEMENT, "")]){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Wallet" bundle:[NSBundle mainBundle]];
        WalletStatementController *stmtVc = [sb instantiateViewControllerWithIdentifier:@"StmtVC"];
        [self.navigationController pushViewController:stmtVc animated:TRUE];
    }

    
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //if (section == 0) return 10;
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(void)addDropShadowForView:(UIView *)viewShadow {
    viewShadow.backgroundColor = [UIColor whiteColor];
    viewShadow.layer.shadowColor = [UIColor colorWithRed:117.0/255.0 green:117.0/255.0 blue:117.0/255.0 alpha:0.4].CGColor;
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = CGSizeMake(-1, 0);
    viewShadow.layer.shadowRadius = 4;
    viewShadow.layer.masksToBounds = NO;
}
@end
