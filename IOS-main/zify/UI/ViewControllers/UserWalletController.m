//
//  UserWalletController.m
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserWalletController.h"
#import "ServerInterface.h"
#import "GenericRequest.h"
#import "UserWallet.h"
#import "CurrentLocale.h"
#import "UserProfile.h"
#import "RazorpayPaymentPersistRequest.h"
#import "StripePaymentPersistRequest.h"
#import "UniversalAlert.h"
#import "LocalisationConstants.h"
#import "IQKeyboardManager.h"
#import "IQUIView+Hierarchy.h"
#import "AppDelegate.h"
#import "zify-Swift.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@implementation UserWalletController{
    NSArray *amountArray;
    NSString *zifyPaymentRefId;
    BOOL isViewInitialised;
    NSString *currencySymbol;
    NSString *currencyCode;
    RazorpayPaymentController *razorpayController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (IS_IPHONE_5) {
        _amount.font = [UIFont systemFontOfSize:15];
        _availableCash.fontSize = 33;
        _availablePoints.fontSize = 13;
        _addMoney.titleLabel.fontSize = 17;
        _lblAvailableBalanceText.fontSize = 13;
        _btnViewStatement.titleLabel.fontSize = 14;
        _lblAddMoney.fontSize = 14;
    } else if (IS_IPHONE_6) {
        _amount.font = [UIFont systemFontOfSize:16];
        _availableCash.fontSize = 35;
        _availablePoints.fontSize = 14;
        _addMoney.titleLabel.fontSize = 18;
        _lblAvailableBalanceText.fontSize = 14;
        _btnViewStatement.titleLabel.fontSize = 15;
        _lblAddMoney.fontSize = 15;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _amount.font = [UIFont systemFontOfSize:16];
        _availableCash.fontSize = 36;
        _availablePoints.fontSize = 15;
        _addMoney.titleLabel.fontSize = 20;
        _lblAvailableBalanceText.fontSize = 15;
        _btnViewStatement.titleLabel.fontSize = 16;
        _lblAddMoney.fontSize = 16;
    }
    [self initialiseView];

    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!isViewInitialised){
       // [self fetchUserWallet];
        isViewInitialised = true;
        self.scrollview.hidden = false;
        self.addMoney.hidden = false;
        if(_rechargeAmount) self.amount.text = _rechargeAmount;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)selectAmount:(UIButton *)sender{
    for (int index = 0; index < [self.amountButtons count]; index++){
        UIButton *button = [self.amountButtons objectAtIndex:index];
        if(sender == button){
            self.amount.text = [amountArray objectAtIndex:index];
            //button.backgroundColor = [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1.0];
            button.backgroundColor = [UIColor colorWithRed:(67.0/255.0) green:(91.0/255.0) blue:(108.0/255.0) alpha:1.0];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //435B6C
        } else{
            button.backgroundColor = [UIColor whiteColor];
            [button setTitleColor:[UIColor colorWithRed:(67.0/255.0) green:(91.0/255.0) blue:(108.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            [button.layer setBorderColor:[UIColor colorWithRed:(67.0/255.0) green:(91.0/255.0) blue:(108.0/255.0) alpha:1.0].CGColor];
        }
    }
}

-(IBAction)dismiss:(id)sender{
    [self.messageHandler dismissErrorView];
    [self.navigationController popViewControllerAnimated:true];
}

-(IBAction)proceedToPayment:(UIButton *)sender{
    if([self validateAmount]){
        BOOL isRazorpayPaymentEnabled = [[CurrentLocale sharedInstance] isRazorpayPaymentEnabled];
        if(isRazorpayPaymentEnabled){
            [self.messageHandler showBlockingLoadViewWithHandler:^{
                [[ServerInterface sharedInstance] getResponse:[[RazorpayPaymentPersistRequest alloc] initWithRequestType:PAYMENTREQUEST andAmount:self.amount.text andCurrency:@"INR"] withHandler:^(ServerResponse *response, NSError *error){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        if(response && ![response.responseObject isKindOfClass:[NSNull class]] && ![@"" isEqualToString:response.responseObject]){
                            zifyPaymentRefId = response.responseObject;
                            [razorpayController initiatePaymentWithAmount:self.amount.text];
                        } else{
                            [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                        }
                    }];
                }];
            }];
        } else {
            [self.messageHandler showBlockingLoadViewWithHandler:^{
                [[ServerInterface sharedInstance] getResponse:[[StripePaymentPersistRequest alloc] initWithRequestType:STRIPEPAYMENTREQUEST andAmount:self.amount.text andCurrency:currencyCode] withHandler:^(ServerResponse *response, NSError *error){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        if(response && ![response.responseObject isKindOfClass:[NSNull class]] && ![@"" isEqualToString:response.responseObject]){
                            zifyPaymentRefId = response.responseObject;
                            UINavigationController *stripePaymentNavController = [StripePaymentController createStripePaymentNavController];
                            StripePaymentController *stripePaymentController = (StripePaymentController *)stripePaymentNavController.topViewController;
                            stripePaymentController.amount = self.amount.text;
                            stripePaymentController.paymentDelegate = self;
                            [self presentViewController:stripePaymentNavController animated:YES completion:nil];
                        } else{
                            [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                        }
                    }];
                }];
            }];
        }
    }
}

-(BOOL)validateAmount{

    if([@"" isEqualToString:self.amount.text]){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_WALLET_AMOUNTVAL, nil)];
        return false;
    }
    
    NSInteger iamount = [self.amount.text integerValue];
    if (iamount > 1000 || iamount == 1000) {
//        [self.messageHandler showErrorMessage:NSLocalizedString(VC_WALLET_AMOUNTVAL_LESSTHAN_THOUSAND, nil)];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AppName
                                                        message:NSLocalizedString(VC_WALLET_AMOUNTVAL_LESSTHAN_THOUSAND, nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Ok",nil];
        [alert show];
        
        return false;
    }
    int amountValue = [self.amount.text intValue];
    if(amountValue <= 0){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_WALLET_AMOUNTVAL_ZERO, nil)];
        return false;
    }
    return true;
   /* int minimumAmount = [[amountArray objectAtIndex:0] intValue];
    if(self.amount.text.intValue < minimumAmount) {
        NSString *alertMessage = [NSString stringWithFormat:@"Please recharge with amount >= %d",minimumAmount];
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:@"Insufficient Recharge Amount" WithMessage:alertMessage];
        [alert addButton:BUTTON_OK WithTitle:@"OK" WithAction:^(void *action){
           
        }];
        [alert showInViewController:self];
        return false;
    }
    return true;*/
}

-(void)initialiseView{
    self.scrollview.hidden = true;
    self.addMoney.hidden = true;
    isViewInitialised = false;
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    amountArray = [currentLocale getRechargeAmounts];
    currencySymbol = [currentLocale getCurrencySymbol];
    currencyCode = [currentLocale getCurrencyCode];
    //
    NSString *placeHolderStr = [NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(WS_WALLET_TXT_ENTERAMOUNT, nil),currencySymbol];
    self.amount.placeholder = placeHolderStr;
    self.amount.layer.borderColor =  [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1.0].CGColor;
    for (int index = 0; index < [self.amountButtons count]; index++){
        UIButton *button = [self.amountButtons objectAtIndex:index];
        NSString *title = [NSString stringWithFormat:@"+ %@%d",currencySymbol,[[amountArray objectAtIndex:index] intValue]];
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitle:title forState:UIControlStateSelected];
        //button.layer.borderColor = [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1.0].CGColor;
    }
    razorpayController = [[RazorpayPaymentController alloc] init];
    razorpayController.paymentDelegate = self;
}

-(void)fetchUserWallet{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[GenericRequest alloc] initWithRequestType:RIDERWALLETREQUEST] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    UserWallet *wallet = (UserWallet *)response.responseObject;
                    self.availableCash.text =[NSString stringWithFormat:@"%@ %@",currencySymbol, wallet.zifyCash.stringValue];
                    self.availablePoints.text = [NSString stringWithFormat:NSLocalizedString(VC_WALLET_PROMOCREDITS, @"Promotional Credits : {UserPromotionalCredits}"),wallet.availablePoints.stringValue];
                    self.scrollview.hidden = false;
                    self.addMoney.hidden = false;
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void) razorpayPaymentSuccuessWithPaymentId:(NSString *)paymentId{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:[[RazorpayPaymentPersistRequest alloc] initWithRequestType:PAYMENTRESPONSE andPaymentId:paymentId andZifyPaymentRefId:zifyPaymentRefId andFailureCode:nil andFailureDescription:nil] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [self fetchUserWallet];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void) razorpayPaymentFailedWithCode:(int)code andMessage:(NSString *)message{
    [[ServerInterface sharedInstance]getResponse:[[RazorpayPaymentPersistRequest alloc] initWithRequestType:PAYMENTRESPONSE andPaymentId:nil andZifyPaymentRefId:zifyPaymentRefId andFailureCode:[NSNumber numberWithInt:code] andFailureDescription:message] withHandler:^(ServerResponse *response, NSError *error){
        
    }];
    [self.messageHandler showErrorMessage:NSLocalizedString(VC_WALLET_TRYAGAINERROR, nil)];
}

-(void) createStripePaymentWithToken:(STPToken *)token{
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:[[StripePaymentPersistRequest alloc] initWithRequestType:STRIPEPAYMENTRESPONSE andStripeToken:token.stripeID andZifyPaymentRefId:zifyPaymentRefId andFailureCode:nil andFailureDescription:nil] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [self fetchUserWallet];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void) stripePaymentFailedWithError:(NSError *)error{
    [[ServerInterface sharedInstance]getResponse:[[StripePaymentPersistRequest alloc] initWithRequestType:STRIPEPAYMENTRESPONSE andStripeToken:nil andZifyPaymentRefId:zifyPaymentRefId andFailureCode:[NSNumber numberWithInt:(int)error.code] andFailureDescription:[error localizedDescription]] withHandler:^(ServerResponse *response, NSError *error){
    }];
    [self.messageHandler showErrorMessage:@"Please try again."];
}

+(UINavigationController *)createUserWalletNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Wallet" bundle:[NSBundle mainBundle]];
    UINavigationController *rechargeNavController = [storyBoard instantiateViewControllerWithIdentifier:@"userWalletNavController"];
    return rechargeNavController;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.amount && [AppDelegate SCREEN_HEIGHT] <= 568)
    {
        if  (self.view.frame.origin.y >= 0)
        {
            //[self setViewMovedUp:YES];
        }
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if([AppDelegate SCREEN_HEIGHT] <= 568){
       // [self setViewMovedUp:NO];
    }
}
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
@end
