//
//  TripsTabController.h
//  zify
//
//  Created by Anurag S Rathor on 22/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripsTabController : UITabBarController
@property(nonatomic,weak) IBOutlet UITabBar *tabbar;
-(void)selectTripsDrives;
+(UINavigationController *)createTripsTabNavController;
@end
