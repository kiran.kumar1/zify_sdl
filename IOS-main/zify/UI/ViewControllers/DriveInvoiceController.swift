//
//  DriveInvoiceController.swift
//  zify
//
//  Created by Anurag on 28/06/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class DriveInvoiceController: UIViewController, UITableViewDataSource, UITableViewDelegate, RateViewDelegate, UITextViewDelegate {
    
    @IBOutlet var tblDriverFeedback: UITableView!
    
    /*
    @IBOutlet weak var sourceIcon: UIImageView!
    @IBOutlet weak var source: UILabel!
    @IBOutlet weak var destinationIcon: UIImageView!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var currency: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var feedbackView: UIView!
    @IBOutlet weak var feedbackTextView: UITextView!*/
    
    @IBOutlet weak var messageHandler: MessageHandler!
    var ratingResponse: CurrentTripNPendingRatingResponse?
    var showEmptyDrive = false
    
    let COMMENTS_MAX_LENGTH = 500
    let COMMENTS_TEXT_TAG = 1234
    
    var profileImagePlaceHolder: UIImage?
    var isViewInitialised = false
    var userRatings: [AnyObject] = []
    var driveTripIds: [AnyHashable] = []
    var userFeedback: [AnyHashable] = []
    var ratingDrive: TripDrive?
    var selectedFeedbackIndex: Int = 0
    var currentLocale: CurrentLocale?
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIScreen.main.sizeType == .iPhone5 {
            btnSubmit.titleLabel?.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            btnSubmit.titleLabel?.fontSize = 16
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            btnSubmit.titleLabel?.fontSize = 16
        } else {
            btnSubmit.titleLabel?.fontSize = 16
        }
        btnSubmit.setTitle(NSLocalizedString(CTS_DRIVEINVOICE_BTN_SUBMIT, comment: ""), for: .normal)
        self.navigationItem.title = NSLocalizedString(CTS_DRIVEINVOICE_NAV_TITLE, comment: "")
        profileImagePlaceHolder = UIImage(named: "placeholder_profile_image")
        isViewInitialised = false
        userRatings = [AnyObject]()
        userFeedback = [AnyHashable]()
        driveTripIds = [AnyHashable]()
        currentLocale = CurrentLocale.sharedInstance()
        
        ratingDrive = ratingResponse?.pendingDriveRating
        
        let iCount : Int = ratingDrive?.driveRiders.count ?? 0
        for counter in 0..<iCount {
        
          print("vvnhvhv\(ratingDrive?.driveRiders[counter])")
            let driverproile = ratingDrive?.driveRiders[counter] as? RiderProfile
            print("heheh\(driverproile?.regionDetail)")
        //for counter in 0..< Int(ratingDrive?.driveRiders.count) {
            userRatings.append(NSNumber(value: 0))
            userFeedback.append("")
            let riderDetail = ratingDrive?.driveRiders[counter] as? TripDriveRiderDetail
                driveTripIds.append(riderDetail?.rideId)
            
        }
        debugPrint("userRatings-----", userRatings)
    }
    
    @IBAction func tapOnSupport(_ sender: UIBarButtonItem) {
        Freshchat.sharedInstance().showConversations(self)
    }
    
    @IBAction func submitRating(_ sender: UIButton) {
        for index in 0..<userRatings.count {
            let rating : Int = userRatings[index] as! Int
            if rating == 0 {
                messageHandler.showErrorMessage(String(format: NSLocalizedString(VC_DRIVEINVOICE_RATINGVAL, comment: "Please select rating for passenger {PassengerNum}"), index + 1))
                return
            }
        }
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(TripRatingsRequest(with: DRIVERATING, andTripId: self.ratingDrive?.driveId, andRatingTripIds: self.driveTripIds, andRatings: self.userRatings, andComments: self.userFeedback), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.dismiss(animated: true) {
                            if ((self.ratingResponse?.currentDrive) != nil) {
                                if self.showEmptyDrive || self.ratingResponse?.currentDrive.numOfSeats.intValue ?? 0 > 0 {
                                    let currentDriveNavController = CurrentDriveController.createCurrentDriveNavController()
                                    let currentDriveController = currentDriveNavController?.topViewController as? CurrentDriveController
                                    currentDriveController?.currentDrive = self.ratingResponse?.currentDrive
                                    AppUtilites.applicationVisibleViewController().present((currentDriveNavController ?? nil)!, animated: true)
                                }
                            } else if ((self.ratingResponse?.currentRide) != nil) {
                                let currentRideNavController = CurrentRideController.createCurrentRideNavController()
                                let currentRideController = currentRideNavController?.topViewController as? CurrentRideController
                                currentRideController?.currentRide = self.ratingResponse?.currentRide
                                if let currentRideNavController = currentRideNavController {
                                    AppUtilites.applicationVisibleViewController().present(currentRideNavController, animated: true)
                                }
                            }
                        }
                        do {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as? String)
                        }
                    }
                })
            })
        })
    }
    
    // MARK: - RatingView Delegates
    func rateView(_ rateView: RatingView?, ratingDidChange rating: Float) {
        
        let indexPath: IndexPath? = tblDriverFeedback.indexPath(for: getTableViewCell(rateView!) ?? UITableViewCell())
        userRatings[indexPath?.row ?? 0] = NSNumber(value: rating)
        //(userRatings as NSArray).object(at: indexPath?.row ?? 0) = NSNumber(value: rating)
    }
    
    class func createDriveInvoiceNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Feedback", bundle: Bundle.main)
        let driveInvoiceNavController = storyBoard.instantiateViewController(withIdentifier: "DriveInvoiceNavigationController") as? UINavigationController
        return driveInvoiceNavController
    }
    
    func getTableViewCell(_ button: AnyObject) -> UITableViewCell? {
        var btn : AnyObject = button
        while !(btn is UITableViewCell) {
            //btn = (btn as AnyObject).superview()
            btn = btn.superview as AnyObject
        }
        return btn as? UITableViewCell
    }
    
    
    // MARK: - TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 || section == 4 {
            return ratingDrive?.driveRiders.count ?? 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let sectionLabel = UILabel(frame: CGRect(x: 10, y: 10, width:
            tableView.bounds.size.width, height: 40))
        
        switch(section) {
        case 0 :
            sectionLabel.text =   NSLocalizedString(ride_details, comment: "")
        case 1 :
            sectionLabel.text =   NSLocalizedString(money_received, comment: "")
        case 4 :
            sectionLabel.text =   NSLocalizedString(share_your_feedback, comment: "")
        default :sectionLabel.text =  ""
        }
        
        sectionLabel.font = setFontSizes(anyObj: sectionLabel, fontStyle: BoldFont, defaultSize: 17, iphone6Size: 17, iphoneXRSize: 17, iphone6PlusSize: 19, iphoneXSize: 19, iphoneXSMaxSize: 19)

        sectionLabel.font = UIFont(name: BoldFont, size: 18)
        sectionLabel.textColor = UIColor.black
        sectionLabel.sizeToFit()
        headerView.addSubview(sectionLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 || section == 4 {
            return 40.0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellDriveInvoiceRideDetails : DriveInvoiceDetailCell = (self.tblDriverFeedback.dequeueReusableCell(withIdentifier: "DriveInvoiceDetailCell") as! DriveInvoiceDetailCell)
            //cellRideDetails.lblDistance.text = "992.3847568923 kms"
            
            cellDriveInvoiceRideDetails.lblSourceAddress.text = ratingDrive?.srcAddress
            cellDriveInvoiceRideDetails.lblDestinationAddress.text = ratingDrive?.destAddress
            
            cellDriveInvoiceRideDetails.lblDriveId.text = "\(NSLocalizedString(drive_id, comment: "")) : \(ratingDrive?.driveId ?? 0)"
            
            let distanceValue = String(format: "%.02@", ratingDrive?.distance ?? 0.0)
            cellDriveInvoiceRideDetails.lblDistance.text = "\(distanceValue) " + (CurrentLocale.sharedInstance().getDistanceUnit() ?? "")
            
            
            //[[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:currentDate];
            
            //let timeFormatFromSetting = AppDelegate.getInstance().timeFormatter.date(from: "\(ratingDrive?.driveStartTime ?? "") ")
            //let setTime = getDateFormatToTimeForInvoice(strDate: "\(timeFormatFromSetting ?? Date()) ")
            cellDriveInvoiceRideDetails.lblStartTime.text = getDateFormatToTimeForInvoice(strDate: "\(ratingDrive?.driveStartTime ?? "") ") //"\(ratingDrive?.driveStartTime ?? "") "
            cellDriveInvoiceRideDetails.lblEndTime.text = getDateFormatToTimeForInvoice(strDate: "\(ratingDrive?.driveEndTime ?? "") ") //"\(ratingDrive?.driveEndTime ?? "") "
            
            cellDriveInvoiceRideDetails.lblDistanceTitle.text = NSLocalizedString(trip_distance, comment: "")
            cellDriveInvoiceRideDetails.lblStartTimeTitle.text = NSLocalizedString(start_time, comment: "")
            cellDriveInvoiceRideDetails.lblEndTimeTitle.text = NSLocalizedString(end_time, comment: "")
            
            cellDriveInvoiceRideDetails.lblSourceAddress.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblSourceAddress, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 14, iphoneXSMaxSize: 16)
            cellDriveInvoiceRideDetails.lblDestinationAddress.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblDestinationAddress, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 14, iphoneXSMaxSize: 16)
            cellDriveInvoiceRideDetails.lblDriveId.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblDriveId, fontStyle: NormalFont, defaultSize: 13, iphone6Size: 13, iphoneXRSize: 13, iphone6PlusSize: 14, iphoneXSize: 13, iphoneXSMaxSize: 14)
            cellDriveInvoiceRideDetails.lblDistance.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblDistance, fontStyle: BoldFont, defaultSize: 13, iphone6Size: 13, iphoneXRSize: 13, iphone6PlusSize: 15, iphoneXSize: 14, iphoneXSMaxSize: 15)
            cellDriveInvoiceRideDetails.lblStartTime.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblStartTime, fontStyle: BoldFont, defaultSize: 13, iphone6Size: 13, iphoneXRSize: 13, iphone6PlusSize: 15, iphoneXSize: 14, iphoneXSMaxSize: 15)
            cellDriveInvoiceRideDetails.lblEndTime.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblEndTime, fontStyle: BoldFont, defaultSize: 13, iphone6Size: 13, iphoneXRSize: 13, iphone6PlusSize: 15, iphoneXSize: 14, iphoneXSMaxSize: 15)
            cellDriveInvoiceRideDetails.lblDistanceTitle.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblDistanceTitle, fontStyle: NormalFont, defaultSize: 12, iphone6Size: 12, iphoneXRSize: 12, iphone6PlusSize: 14, iphoneXSize: 13, iphoneXSMaxSize: 14)
            cellDriveInvoiceRideDetails.lblStartTimeTitle.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblStartTimeTitle, fontStyle: NormalFont, defaultSize: 12, iphone6Size: 12, iphoneXRSize: 12, iphone6PlusSize: 14, iphoneXSize: 13, iphoneXSMaxSize: 14)
            cellDriveInvoiceRideDetails.lblEndTimeTitle.font = setFontSizes(anyObj: cellDriveInvoiceRideDetails.lblEndTimeTitle, fontStyle: NormalFont, defaultSize: 12, iphone6Size: 12, iphoneXRSize: 12, iphone6PlusSize: 14, iphoneXSize: 13, iphoneXSMaxSize: 14)
            
            
            return cellDriveInvoiceRideDetails
            
        } else if indexPath.section == 1 {
            let cellMoneyDebtdUser : DriveInvoiceMoneyDebitedUserCell = (self.tblDriverFeedback.dequeueReusableCell(withIdentifier: "DriveInvoiceMoneyDebitedUserCell") as? DriveInvoiceMoneyDebitedUserCell)!
            
            let riderDetail = ratingDrive?.driveRiders[indexPath.row] as? TripDriveRiderDetail
            let riderProfile = riderDetail?.riderProfile
            cellMoneyDebtdUser.lblUserName.text = (riderDetail?.riderProfile.firstName ?? "") //+ " " + (riderDetail?.riderProfile.lastName ?? "")
                //arrRiders[indexPath.row]
            
            
            let distanceValue = String(format: "%.2@", ratingDrive?.distance ?? 0.0)

            cellMoneyDebtdUser.lblKms.text = "\(distanceValue ) " + (CurrentLocale.sharedInstance().getDistanceUnit() ?? "") + " x " + (riderProfile?.regionDetail.perKmRate.stringValue ?? " ")
            
            //"\(ratingRide?.distance ?? "") " + CurrentLocale.sharedInstance().getDistanceUnit() + " x 3 "
            
            //let amountValue = String(format: "%.02@", ratingDrive?.zifyPoints ?? 0.0)//zifyPoints.floatValue)!)
            let amountValue = String(format: "%.02@", riderDetail?.points ?? 0.0)
            cellMoneyDebtdUser.lblPrice.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator: amountValue)
            
            
            cellMoneyDebtdUser.lblUserName.font = setFontSizes(anyObj: cellMoneyDebtdUser.lblUserName, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellMoneyDebtdUser.lblKms.font = setFontSizes(anyObj: cellMoneyDebtdUser.lblKms, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellMoneyDebtdUser.lblPrice.font = setFontSizes(anyObj: cellMoneyDebtdUser.lblPrice, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 15, iphoneXSize: 15, iphoneXSMaxSize: 15)
            
            
            return cellMoneyDebtdUser
            
        } else if indexPath.section == 2 {
            
            let cellDriveInvoiceMoneyDebtdTotal : DriveInvoiceMoneyDebitedTotalCell = (self.tblDriverFeedback.dequeueReusableCell(withIdentifier: "DriveInvoiceMoneyDebitedTotalCell") as? DriveInvoiceMoneyDebitedTotalCell)!
            cellDriveInvoiceMoneyDebtdTotal.lblTotalTitle.text = "\(NSLocalizedString(total, comment: "")) "
            let amountValue = String(format: "%.02@", ratingDrive?.zifyPoints ?? 0.0)//zifyPoints.floatValue)!)
            cellDriveInvoiceMoneyDebtdTotal.lblTotalPrice.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator: amountValue)
            
            cellDriveInvoiceMoneyDebtdTotal.lblTotalTitle.font = setFontSizes(anyObj: cellDriveInvoiceMoneyDebtdTotal.lblTotalTitle, fontStyle: BoldFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellDriveInvoiceMoneyDebtdTotal.lblTotalPrice.font = setFontSizes(anyObj: cellDriveInvoiceMoneyDebtdTotal.lblTotalPrice, fontStyle: BoldFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            
            return cellDriveInvoiceMoneyDebtdTotal
            
        } else if indexPath.section == 3 {
            let cellDriveInvoiceZifyCoinsEarned : DriveInvoiceZifyCoinsEarnedCell = (self.tblDriverFeedback.dequeueReusableCell(withIdentifier: "DriveInvoiceZifyCoinsEarnedCell") as? DriveInvoiceZifyCoinsEarnedCell)!
            cellDriveInvoiceZifyCoinsEarned.lblZifyCoinsEarnedTitle.text = NSLocalizedString(zify_coins_earned, comment: "")
            cellDriveInvoiceZifyCoinsEarned.lblZifyCoinsCount.text = "\(ratingDrive?.zifyCoinsEarned ?? 0)"
            cellDriveInvoiceZifyCoinsEarned.lblzifyCoinsDescription.text = NSLocalizedString(driver_coins_description, comment: "")
            
            cellDriveInvoiceZifyCoinsEarned.lblZifyCoinsEarnedTitle.font = setFontSizes(anyObj: cellDriveInvoiceZifyCoinsEarned.lblZifyCoinsEarnedTitle, fontStyle: BoldFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellDriveInvoiceZifyCoinsEarned.lblZifyCoinsCount.font = setFontSizes(anyObj: cellDriveInvoiceZifyCoinsEarned.lblZifyCoinsCount, fontStyle: BoldFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)
            cellDriveInvoiceZifyCoinsEarned.lblzifyCoinsDescription.font = setFontSizes(anyObj: cellDriveInvoiceZifyCoinsEarned.lblzifyCoinsDescription, fontStyle: NormalFont, defaultSize: 12, iphone6Size: 12, iphoneXRSize: 12, iphone6PlusSize: 13, iphoneXSize: 13, iphoneXSMaxSize: 13)
            
            
            return cellDriveInvoiceZifyCoinsEarned
            
        } else if indexPath.section == 4 {
            let cellDriveInvoiceRatingCell : DriveInvoiceRatingCell = (self.tblDriverFeedback.dequeueReusableCell(withIdentifier: "DriveInvoiceRatingCell") as? DriveInvoiceRatingCell)!
            
            let riderDetail = ratingDrive?.driveRiders[indexPath.row] as? TripDriveRiderDetail
            
            cellDriveInvoiceRatingCell.lblUserName?.text = riderDetail?.riderProfile.firstName ?? "" //arrRiders[indexPath.row]
            
            cellDriveInvoiceRatingCell.imgProfile.sd_setImage(with: URL(string: riderDetail?.riderProfile.profileImageURL ?? ""), placeholderImage: profileImagePlaceHolder, completed: { image, error, cacheType, imageURL in
            })
            
            cellDriveInvoiceRatingCell.ratingView.rating = userRatings[indexPath.row ?? 0] as! Float
            cellDriveInvoiceRatingCell.ratingView.editable = true
            cellDriveInvoiceRatingCell.ratingView.delegate = self
            cellDriveInvoiceRatingCell.ratingView.backgroundColor = UIColor.white

            cellDriveInvoiceRatingCell.lblUserName.font = setFontSizes(anyObj: cellDriveInvoiceRatingCell.lblUserName, fontStyle: NormalFont, defaultSize: 14, iphone6Size: 14, iphoneXRSize: 14, iphone6PlusSize: 16, iphoneXSize: 16, iphoneXSMaxSize: 16)

            return cellDriveInvoiceRatingCell
        }
        return UITableViewCell()
    }
    
    
    
}



class DriveInvoiceDetailCell : UITableViewCell {
    
    @IBOutlet weak var lblSourceAddress: UILabel!
    @IBOutlet weak var lblDestinationAddress: UILabel!
    @IBOutlet weak var lblDistanceTitle: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblStartTimeTitle: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTimeTitle: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblDriveId: UILabel!
}

class DriveInvoiceMoneyDebitedUserCell : UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblKms: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
}

class DriveInvoiceMoneyDebitedTotalCell : UITableViewCell {
    
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
}

class DriveInvoiceZifyCoinsEarnedCell : UITableViewCell {
    
    @IBOutlet weak var lblZifyCoinsEarnedTitle: UILabel!
    @IBOutlet weak var lblZifyCoinsCount: UILabel!
    @IBOutlet weak var lblzifyCoinsDescription: UILabel!
}

class DriveInvoiceRatingCell : UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var ratingView: RatingView!
}
