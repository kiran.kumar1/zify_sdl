//
//  ReferAndEarnController.swift
//  zify
//
//  Created by Anurag on 20/11/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class ReferAndEarnController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    @IBOutlet weak var lblReferLink: UILabel!
    @IBOutlet weak var tblReferEarnings: UITableView!
    @IBOutlet weak var btnReferNow: UIButton!

    @IBOutlet weak var viewShadowForLinkReferNowBtn: UIView!
    @IBOutlet weak var viewLinkReferNowBtn: UIView!
    
    let arrImg : [String] = ["wallet", "referYourOrg"]
    let arrTitle : [String] = [NSLocalizedString(my_referrals, comment: ""), NSLocalizedString(refer_your_organisation, comment: "")]
    let arrSubText : [String] = [NSLocalizedString(tap_to_check_your_referral_earnings, comment: ""), NSLocalizedString(refer_and_earn_exciting_coins, comment: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewShadowForLinkReferNowBtn.layer.cornerRadius = 5
        viewShadowForLinkReferNowBtn.layer.masksToBounds = true
        showShadow(view: viewLinkReferNowBtn, color: .darkGray, opacity: 1, offSet: CGSize(width: 0, height: 0), radius: 2, scale: true)
        
        var titleCoins = "100"
        var subHeaderCOCoins = "100"
        var subHeaderRiCoins = "70"
        var subHeaderFriendCoins = "50"
        if UserProfile.getCurrentUser()?.country == "FR" || UserProfile.getCurrentUser()?.country == "DE" || UserProfile.getCurrentUser()?.country == "CH" {
            titleCoins = "50"
            subHeaderCOCoins = "50"
            subHeaderRiCoins = "25"
            subHeaderFriendCoins = "10"
        } else if UserProfile.getCurrentUser()?.country == "DK" {
            titleCoins = "100"
            subHeaderCOCoins = "100"
            subHeaderRiCoins = "50"
            subHeaderFriendCoins = "10"
        }
        
        let strHeader = String.init(format: NSLocalizedString(refer_your_friends_and_get_upto, comment: ""), titleCoins)
        lblHeader.text = strHeader//NSLocalizedString(refer_your_friends_and_get_upto, comment: "")
        let strSubHeader = String.init(format: NSLocalizedString(if_car_owner_signups_you_get, comment: ""), subHeaderCOCoins, subHeaderRiCoins, subHeaderFriendCoins)
        lblSubHeader.text = strSubHeader//NSLocalizedString(if_car_owner_signups_you_get, comment: "")
        btnReferNow.setTitle(NSLocalizedString(refer_now, comment: ""), for: .normal)
        
        
        if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
//            lblHeader.font = UIFont.init(name: "OpenSans-SemiBold", size: 22)
//            lblSubHeader.font = UIFont.init(name: "OpenSans-SemiBold", size: 22)
//            lblReferLink.font = UIFont.init(name: "OpenSans-SemiBold", size: 22)
//            btnReferNow.titleLabel?.fontSize = 12
        } else if UIScreen.main.sizeType == .iPhone6Plus  || UIScreen.main.sizeType == .iPhoneXSMax {
            
        }
        
        if UserDefaults.standard.value(forKey: "ReferralLink") != nil {
            self.lblReferLink.text = UserDefaults.standard.value(forKey: "ReferralLink") as? String
        } else {
            self.serviceCall_GetReferAndEarnLink()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.title = NSLocalizedString(refer_and_earn, comment: "")
    }
    
    // MARK: - Actions
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionReferNow(_ sender: Any) {
//        let userObj = UserProfile.getCurrentUser()
//        if (userObj?.userStatus)! != "VERIFIED" {
//            showAlertView(strMessage: NSLocalizedString(for_trusted_carpooling_company_community_upload_id, comment: ""), navigationCtrl: self.navigationController!)
//            return
//        }
        
        var earnCoins = "50"
        if UserProfile.getCurrentUser()?.country == "FR" || UserProfile.getCurrentUser()?.country == "DE" || UserProfile.getCurrentUser()?.country == "CH" {
            earnCoins = "10"
        } else if UserProfile.getCurrentUser()?.country == "DK" {
            earnCoins = "10"
        }
        
        let strMailContent = String.init(format: NSLocalizedString(i_am_carpooling_with_zify, comment: ""), earnCoins)
        let strBodyText = NSLocalizedString(hi_there, comment: "") + "\n" + strMailContent + "\n"
        let referLink = NSURL(string: self.lblReferLink.text ?? "")
        
        guard let url = referLink else {
            print("nothing found")
            return
        }
        
        let shareItems:Array = [strBodyText, url] as [Any]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = []
        
        activityViewController.completionWithItemsHandler = { activity, success, items, error in
            if !success{
                print("cancelled")
                return
            }
            let eventDict = ["media_platform":activity ?? ""] as [String:Any]
            MoEngageEventsClass.callReferInviteEvent(withDataDict: eventDict)
        }
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK:- UITableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRefer : ReferCellEarnings = (self.tblReferEarnings.dequeueReusableCell(withIdentifier: "ReferCellEarnings") as? ReferCellEarnings)!
        cellRefer.lblCellTitle.text = arrTitle[indexPath.row]
        if indexPath.row == 0 {
            cellRefer.lblCellSubTitle.text = arrSubText[indexPath.row]
        }
        cellRefer.lblCellSubTitle.text = arrSubText[indexPath.row]
        cellRefer.imgCell.image = UIImage.init(named: arrImg[indexPath.row])
        cellRefer.selectionStyle = .none
        return cellRefer
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stb = UIStoryboard.init(name: "ReferAndEarn", bundle: nil)
        if indexPath.row == 0 {
            //ReferAndEarnController.serviceCall_SaveReferralData()
            let vcMyReferral =  stb.instantiateViewController(withIdentifier: "MyReferralController") as? MyReferralController
            self.navigationController?.pushViewController(vcMyReferral!, animated: true)
        } else if indexPath.row == 1 {
            //ReferAndEarnController.serviceCall_ProcessReferralData()
            let vcReferYourOrg =  stb.instantiateViewController(withIdentifier: "ReferYourOrgController") as? ReferYourOrgController
            self.navigationController?.pushViewController(vcReferYourOrg!, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    class func createReferAndEarnNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "ReferAndEarn", bundle: Bundle.main)
        let referAndEarnNavContoller = storyBoard.instantiateViewController(withIdentifier: "referAndEarnNavContoller") as? UINavigationController
        return referAndEarnNavContoller
    }
    
    
    // MARK: - Service call
    
    func serviceCall_GetReferAndEarnLink() {
        
        let reqGetReferLink = ReferAndEarnRequestUrlLink()
        debugPrint("---reqGetReferLink--------", reqGetReferLink as Any)
        ServerInterface.sharedInstance()?.getResponse(reqGetReferLink, withHandler: { (response, error) in
            if response != nil {
                debugPrint("respnse--------", response as Any)
                let referralLink = (response?.responseObject as? NSDictionary)?.value(forKey: "responseText") as? String
                self.lblReferLink.text = referralLink
                UserDefaults.standard.set(referralLink, forKey: "ReferralLink")
                UserDefaults.standard.synchronize()
            } else {
                let alert  = UniversalAlert.init(title: AppName, withMessage: error?.localizedDescription)
                alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_OK, comment: ""), withAction: { action in
                    self.dismiss(animated: true, completion: nil)
                })
                
                alert?.show(in: self)
            }
        })
        
    }
    
    class func serviceCall_SaveReferralData() {
        
        if UserDefaults.standard.value(forKey: "referralUserId") != nil && UserDefaults.standard.value(forKey: "referralCountryCode") != nil {
            
           /***/
            let reqSaveReferlData = SaveReferralData()
            debugPrint("---save referral data---", reqSaveReferlData as Any)
            
            ServerInterface.sharedInstance()?.getResponse(reqSaveReferlData, withHandler: { (response, error) in
                if response != nil {
                    debugPrint("----save refrl daata Response SUCCESSS--------", response as Any)
                } else {
                    debugPrint("----save refrl daata Response NILLL FAILUREE--------", error?.localizedDescription ?? "----Response NILLL FAILUREE----")
                }
            })
            /***/
        } else {
            debugPrint("----NIL Userdefaults Values--------")
            return
        }
    }
    
    
    class func serviceCall_ProcessReferralData(eventId : Int, strUserType : String) {
        let profileData = UserProfile.getCurrentUser()
        let reqProcessReferlData = ProcessReferralData.init(countryCode: profileData?.country ?? "", eventId: eventId, userId: profileData?.userId as! Int, userType: strUserType )
        debugPrint("---process referral data---", reqProcessReferlData as Any)
        if profileData?.userType == nil || profileData?.userType == "" {
            showAlertView(strMessage: "Please upload Travel preferences", navigationCtrl: ReferAndEarnController().navigationController!)
            return
        }
        ServerInterface.sharedInstance()?.getResponse(reqProcessReferlData, withHandler: { (response, error) in
            if response != nil {
                debugPrint("----process refrl daata Response SUCCESSS--------", response as Any)
            } else {
                debugPrint("----process refrl daata Response NILLL FAILUREE--------", error?.localizedDescription ?? "--process--Response NILLL FAILUREE----")
            }
        })
    }
    
    
}

class ReferCellEarnings : UITableViewCell {
    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var lblCellTitle: UILabel!
    @IBOutlet weak var lblCellSubTitle: UILabel!
}


// MARK:- Service Request for get Referral Link

class ReferAndEarnRequestUrlLink : ServerRequest {
    
    let myuserProfile : UserProfile = UserProfile.getCurrentUser()
    
    override func urlparams() -> [AnyHashable : Any]! {
        let dictRefer = [
            "user_id" : UserProfile.getCurrentUserId() as AnyObject,
            "country_code" : "\(myuserProfile.country ?? "")",
            "lang" : "\(CurrentLocale.sharedInstance()?.getString() ?? "")" //need to change this - get lang from userObject
            
            ] as [String : Any]
        debugPrint(dictRefer)
        return dictRefer
    }

    override func serverURL() -> String! {
        //SERVER-Base-url must be "http://test.zify.co"
        return "/zifyreferandearn/referandearn/getReferralLink"
    }
    override func isForZenParkService() -> Bool {
        return true
    }
    override func isJSONRequestFormat() -> Bool {
        return false
    }
    override func overrideBaseURL() -> Bool {
        return true
    }
    override func isForReferAndEarn() -> Bool {
        return true
    }
    
}

// MARK: - Server Request for Save Referral Data

class SaveReferralData : ServerRequest {
    
    override func urlparams() -> [AnyHashable : Any]! {
        
        let referralParentCountryCode = UserDefaults.standard.value(forKey: "referralCountryCode")
        let referralParentUserId : Int = UserDefaults.standard.value(forKey: "referralUserId") as! Int
        let profileReferral = UserProfile.getCurrentUser()
        
        
        let parameters: [String: AnyObject] = [
            "referee_child_country_code" : "\(profileReferral?.country! ?? "")" as AnyObject,
            "referee_child_user_id" : (profileReferral?.userId!) ?? 0,
            "referral_parent_country_code" : "\(referralParentCountryCode ?? "")" as AnyObject,
            "referral_parent_user_id" : referralParentUserId as AnyObject
        ]
        return parameters
    }
    
    override func serverURL() -> String! {
        return "/zifyreferandearn/referandearn/saveReferralData"
    }
    override func isForZenParkService() -> Bool {
        return true
    }
    override func isJSONRequestFormat() -> Bool {
        return true
    }
    override func overrideBaseURL() -> Bool {
        return true
    }
    override func isForReferAndEarn() -> Bool {
        return true
    }
}

// MARK: - Server Request for Process Referral Data

class ProcessReferralData : ServerRequest {
    let dictParams : [String : AnyObject]?
    init(countryCode : String, eventId : Int, userId : Int, userType : String) {
      dictParams = [
            "country_code" : countryCode as AnyObject,
            "event_id" : eventId as AnyObject,
            "user_id" : userId as AnyObject,
            "user_type" : userType as AnyObject
        ]
    }
    
    override func urlparams() -> [AnyHashable : Any]! {
        debugPrint("----Process referral data---", dictParams!)
        return dictParams
    }
    
    override func serverURL() -> String! {
        return "/zifyreferandearn/referandearn/processReferralData"
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
    override func isJSONRequestFormat() -> Bool {
        return true
    }
    
    override func overrideBaseURL() -> Bool {
        return true
    }
    override func isForReferAndEarn() -> Bool {
        return true
    }
    
}
