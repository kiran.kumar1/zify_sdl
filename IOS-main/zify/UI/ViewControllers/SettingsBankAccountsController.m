//
//  SettingsBankAccountsController.m
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "SettingsBankAccountsController.h"
#import "BankAccountDetailsCell.h"
#import "ServerInterface.h"
#import "BankAccountRequest.h"
#import "BankAccountDetails.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

@interface SettingsBankAccountsController ()


@end

@implementation SettingsBankAccountsController{
    NSMutableArray *bankAccounts;
    BOOL isGlobalLocale;
    
    BankAccountDetailsCell *cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (IS_IPHONE_5) {
        _lblAddedAccountsText.fontSize = 14;
        cell.bankName.fontSize = 12;
        cell.accountNumber.fontSize = 14;
        cell.ifscCode.fontSize = 12;
        cell.lblAccountNoText.fontSize = 14;
    } else if (IS_IPHONE_6) {
        _lblAddedAccountsText.fontSize = 14;
        cell.bankName.fontSize = 13;
        cell.accountNumber.fontSize = 15;
        cell.ifscCode.fontSize = 13;
        cell.lblAccountNoText.fontSize = 15;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _lblAddedAccountsText.fontSize = 16;
        cell.bankName.fontSize = 14;
        cell.accountNumber.fontSize = 16;
        cell.ifscCode.fontSize = 14;
        cell.lblAccountNoText.fontSize = 16;
    }
    
    bankAccounts = nil;
    self.bankAccountsView.hidden = true;
    self.noAccountsView.hidden = true;
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    isGlobalLocale = [currentLocale isGlobalLocale];
    _tableView.estimatedRowHeight = 140.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated {
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.bankAccountsView.hidden = true;
    self.noAccountsView.hidden = true;
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance]getResponse:[[BankAccountRequest alloc] initWithRequestType:SAVEDBANKDETAILS] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    bankAccounts = [NSMutableArray arrayWithArray:(NSArray *)response.responseObject];
                    [self handleBankAccountsResponse];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(bankAccounts.count == 0) return 0;
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(bankAccounts.count == 1 && indexPath.section == 1){
        UITableViewCell *addBankAccountCell = [tableView dequeueReusableCellWithIdentifier:@"addBankAccount"];
        return addBankAccountCell;
    }
    BankAccountDetails *bankAccountDetails = [bankAccounts objectAtIndex:indexPath.section];
    cell = [_tableView dequeueReusableCellWithIdentifier:@"bankAccountDetails"];
    cell.bankName.text = [bankAccountDetails.bankName uppercaseString];
    cell.accountNumber.text = bankAccountDetails.accountNumber;
    if(isGlobalLocale) cell.ifscCodeLabel.text = NSLocalizedString(VC_SETTINGSBANKACCOUNT_IBANNUMBER, nil);
    else cell.ifscCodeLabel.text = NSLocalizedString(VC_SETTINGSBANKACCOUNT_IFSCCODE, nil);
    cell.ifscCode.text = bankAccountDetails.ifscCode;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(bankAccounts.count == 1 && indexPath.section == 1) {
      [self performSegueWithIdentifier:@"addBankAccount" sender:self];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(IBAction) dismiss:(id)sender{
     [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction) addBankAccount:(UIButton *)sender{
    [self performSegueWithIdentifier:@"addBankAccount" sender:sender];
}

-(IBAction)deleteAccount:(UIButton *)sender{
  NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    BankAccountDetails *bankDetails = [bankAccounts objectAtIndex:indexPath.section];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[BankAccountRequest alloc] initWithRequestType:DELETEBANK andBankAccountDetaiId:bankDetails.bankDetailId andRedeemAmount:nil] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [bankAccounts removeObjectAtIndex:indexPath.section];
                    [self handleBankAccountsResponse];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(void)handleBankAccountsResponse{
    if([bankAccounts count] == 0){
        self.bankAccountsView.hidden = true;
        self.noAccountsView.hidden = false;
    } else{
        self.bankAccountsView.hidden = false;
        self.noAccountsView.hidden = true;
        [self.tableView reloadData];
    }
}

-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}
@end
