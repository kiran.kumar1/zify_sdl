//
//  UserAddressPreferencesController.h
//  zify
//
//  Created by Anurag S Rathor on 12/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateTimePickerView.h"
#import "CurrentLocationTracker.h"
#import "MessageHandler.h"
#import "PublishRideController.h"
#import "MapLocationHelper.h"
#import "MapContainerView.h"
#import "CustomPickerView.h"

@protocol  UserAddressPreferencesDelegate<NSObject>
-(void)updateUserAddressPreferences;
@end


@interface UserAddressPreferencesController : UIViewController<DateTimePickerDelegate,CurrentLocationTrackerDelegate,PublishRideDelgate,MapLocationHelperDelegate,CustomPickerDelegate>{
    UIView *blurView;
    UIView *displaySuccessView;
}
@property (nonatomic,weak) IBOutlet UIImageView *closePageArrow;
@property (nonatomic,weak) IBOutlet UILabel *pageTitle;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic,weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic,weak) IBOutlet UILabel *homeAddress;
@property (nonatomic,weak) IBOutlet UIImageView *homeAddressEditImage;
@property (nonatomic,weak) IBOutlet UILabel *officeAddress;
@property (nonatomic,weak) IBOutlet UIImageView *officeAddressEditImage;
@property (nonatomic,weak) IBOutlet UILabel *travelType;
@property (nonatomic,weak) IBOutlet UILabel *startTime;
@property (nonatomic,weak) IBOutlet UILabel *returnTime;
@property (nonatomic,weak) IBOutlet UIView *routeSelectionView;
@property (nonatomic,weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic,weak) IBOutlet UIImageView *routeSelectImage;
@property (nonatomic,weak) IBOutlet UIView *routeSelectPlaceHolder;
@property (nonatomic,weak) IBOutlet UIView *mapViewPlaceHolder;
@property (nonatomic,weak) IBOutlet MapContainerView *mapContainerView;
@property (nonatomic,weak) IBOutlet UIButton *editRoute;
@property (nonatomic,weak) IBOutlet UILabel *errorMessage;
@property (nonatomic,weak) IBOutlet UIView *preferencesActionView;
@property (nonatomic,weak) IBOutlet UIView *remindLaterView;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
@property (nonatomic,weak) IBOutlet DateTimePickerView *dateTimePickerView;
@property (nonatomic,weak) IBOutlet CustomPickerView *travelTypePickerView;
@property (nonatomic) BOOL isUpgradeFlow;
@property (nonatomic) BOOL showPageIndicator;
@property (nonatomic, weak) id<UserAddressPreferencesDelegate> preferencesDelegate;
+(UserAddressPreferencesController *)createUserAddressPreferencesController;

@property (nonatomic,weak) IBOutlet UILabel *lblHomeAddTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblOfficeAddTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblTravelTypeTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblStartTimeTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblReturnTimeTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblSelectRouteTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblLetUsKnowYouTakeToTravelTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblSetTheTimeYouUsuallyLeaveWorkandHomeTitle;

@property (nonatomic,weak) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;




@end



