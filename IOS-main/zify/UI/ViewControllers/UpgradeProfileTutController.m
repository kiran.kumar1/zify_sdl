//
//  UpgradeProfileTutController.m
//  zify
//
//  Created by Anurag S Rathor on 02/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UpgradeProfileTutController.h"
#import "UpgradeProfileTutCell.h"
#import "OnboardFlowHelper.h"
#import "CurrentLocale.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

@interface UpgradeProfileTutController ()

@end

@implementation UpgradeProfileTutController{
    BOOL isViewInitialised;
    BOOL isGlobalLocale;
    BOOL isGlobalPayment;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isViewInitialised = false;
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    isGlobalLocale = [currentLocale isGlobalLocale];
    isGlobalPayment = [currentLocale isGlobalPayment];
    if(isGlobalLocale && !isGlobalPayment) self.pageControl.numberOfPages = 2;
    _collectionView.hidden = true;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
    if (IS_IPHONE_5) {
        _lblPageTitle.fontSize = 15;
        _btnRemindLater.titleLabel.fontSize = 15;
        _btnGetStarted.titleLabel.fontSize = 15;
    } else if (IS_IPHONE_6) {
        _lblPageTitle.fontSize = 15;
        _btnRemindLater.titleLabel.fontSize = 15;
        _btnGetStarted.titleLabel.fontSize = 15;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _lblPageTitle.fontSize = 16;
        _btnRemindLater.titleLabel.fontSize = 16;
        _btnGetStarted.titleLabel.fontSize = 16;
    }
    

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(!isViewInitialised) [self initialiseView];
    // Do any additional setup after loading the view.
}

-(void)initialiseView{
    [_flowLayout setItemSize:CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetHeight(_collectionView.frame))];
    [_collectionView reloadData];
    _collectionView.hidden = false;
    isViewInitialised = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if(isGlobalLocale && !isGlobalPayment) return 2;
    return 3;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"tutorialCell";
    
    UpgradeProfileTutCell *tutorialCell = (UpgradeProfileTutCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if(indexPath.section == 0){
        tutorialCell.tutorialImage.image = [UIImage imageNamed:@"icn_onboard_upgrade_idcard.png"];
        tutorialCell.tutorialTitle.text = NSLocalizedString(VC_UPGRADETUT_IDTUTTITLE, nil);
        tutorialCell.tutorialDescription.text = NSLocalizedString(VC_UPGRADETUT_IDTUTDESC, nil);
    } else if(indexPath.section == 1) {
        tutorialCell.tutorialImage.image = [UIImage imageNamed:@"icn_onboard_upgrade_address.png"];
        tutorialCell.tutorialTitle.text = NSLocalizedString(VC_UPGRADETUT_ADDRESSTUTTITLE, nil);
        tutorialCell.tutorialDescription.text = NSLocalizedString(VC_UPGRADETUT_ADDRESSTUTDESC, nil);
    } else{
        tutorialCell.tutorialImage.image = [UIImage imageNamed:@"icn_onboard_upgrade_withdraw.png"];
        tutorialCell.tutorialTitle.text = NSLocalizedString(VC_UPGRADETUT_BANKACCOUNTTUTTITLE, nil);
        tutorialCell.tutorialDescription.text = @"";
    }
    
    return tutorialCell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.collectionView.frame.size.width;
    self.pageControl.currentPage = self.collectionView.contentOffset.x / pageWidth;
}

-(IBAction) setProfileUpgradeReminder:(id)sender {
    [[OnboardFlowHelper sharedInstance] setReminder:self];
}

-(IBAction) startProfileUpgrade:(id)sender {
     [[OnboardFlowHelper sharedInstance] handeUpgradeProfileTutFlow:self];
}


+(UIViewController *)createUpgradeProfileTutNavController{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"UserOnboard" bundle:[NSBundle mainBundle]];
    UIViewController *upgradeProfileTutController = [storyBoard instantiateViewControllerWithIdentifier:@"upgradeProfileTutController"];
    return upgradeProfileTutController;
}
@end
