//
//  VerifyMobileController.h
//  zify
//
//  Created by Anurag S Rathor on 14/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"
#import "SingleCharPasswordView.h"
#import "MessageHandler.h"

@interface VerifyMobileController : UIViewController<SingleCharPasswordViewDelegate>
@property (nonatomic,weak) IBOutlet UILabel *mobileNumber;
@property (nonatomic,weak) IBOutlet UIImageView *mobileVerifiedIcon;
@property (nonatomic,weak) IBOutlet SingleCharPasswordView *otpView;
@property (nonatomic,weak) IBOutlet UIButton *btnVerify;
@property (nonatomic,weak) IBOutlet UIButton *btnResend;
@property (nonatomic,weak) IBOutlet UIButton *btnLogout;

@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
+(UINavigationController *)createVerifyMobileNavController;
@property (weak, nonatomic) IBOutlet UILabel *lblWhenWeSentaVerifictionCodeToYourMobileNo;
@property (weak, nonatomic) IBOutlet UILabel *lblDidnotGetTheSMS;
@property (weak, nonatomic) IBOutlet UILabel *lblNotYou;
@property (weak, nonatomic) IBOutlet UILabel *lblWeDonotUseMobNoAnyCommercialUse;



@end
