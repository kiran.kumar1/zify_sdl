//
//  ChatMessagesController.swift
//  zify
//
//  Created by Anurag Rathor on 10/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import FirebaseAuth
import FirebaseDatabase

class ChatMessagesController: JSQMessagesViewController , JSQMessagesComposerTextViewPasteDelegate ,ChatMessageDelegate {
    
    
    var receivingUser: UserContactDetail?
    var chatDialog: QBChatDialog?
    var isNewUser = false
    
    
    var outgoingBubbleImageData: JSQMessagesBubbleImage?
    var incomingBubbleImageData: JSQMessagesBubbleImage?
    var outgoingUserImage: JSQMessagesAvatarImage?
    var incomingUserImage: JSQMessagesAvatarImage?
    var chatMessagesData:NSMutableArray!
    var isViewInitialised = false
    var recevierDeviceToken:String = ""
    var receiverLastName:String = ""
    
    
    var initialChatCount:Int = 0
    var userMessageRef:DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chatMessagesData = NSMutableArray.init()
        
        self.updateUnreadCountFromHistoryBasedOnSelectedUser(selectedUser: self.receivingUser?.chatIdentifier ?? "")
        initialiseView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.userMessageRef?.removeAllObservers()
        Database.database().reference().removeAllObservers()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getReceiverProfileFromFirebaseToGetDeviceToken()
        loadUserChatMessages()
    }
    
    func chatManager() -> ChatManager? {
        return ChatManager.sharedInstance()
    }
    
    func getReceiverProfileFromFirebaseToGetDeviceToken(){
        
        guard let receiverId = self.receivingUser?.chatIdentifier else {
            return
        }
        
        let usersScemaRef = Database.database().reference().child("Users").child(receiverId)
        usersScemaRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let dict = snapshot.value as? [String: AnyObject] else {
                return
            }
          //  let person = Person()
          //  person.setValuesForKeys(dict)
            self.recevierDeviceToken = dict["deviceToken"] as? String ?? "" //person.deviceToken ?? ""
            self.receiverLastName = dict["qbLastName"] as? String ?? ""
        })
    }
    
    func pushMainViewController() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let nc = sb.instantiateInitialViewController() as? UINavigationController
        if let aController = nc?.topViewController {
            navigationController?.pushViewController(aController, animated: true)
        }
    }
    
    override func didPressSend(_ button: UIButton?, withMessageText text: String?, senderId: String?, senderDisplayName: String?, date: Date?) {
        /**
         *  Sending a message. Your implementation of this method should do *at least* the following:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishSendingMessage`
         */
     /*   if ChatManager.sharedInstance().isUserChatConnected() {
            let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
            chatMessagesData.add(message)
            finishSendingMessage(animated: true)
            chatManager()?.sendChatMessage(text, toUser: receivingUser, andTime: date, andChatDialog: chatDialog)
        } else {
            let alert = UniversalAlert(title: NSLocalizedString(CMON_GENERIC_ERROR, comment: ""), withMessage: NSLocalizedString(VC_CHATMESSAGES_ERRORMESSAGE, comment: ""))
            alert?.addButton(BUTTON_OK, withTitle: "OK", withAction: { action in
                
            })
            alert?.show(in: self)
        }*/
        
        button?.isUserInteractionEnabled = false
        let count = text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count ?? 0
        if(count > 0){
            sendMessageWithProperty(["message":text as AnyObject])
        }
        button?.isUserInteractionEnabled = true


    }
    
    
    private func sendMessageWithProperty(_ property: [String: AnyObject]){
        let messageText = property["message"] as? String ?? ""
        let fromId = Auth.auth().currentUser!.uid
        let toId = self.receivingUser?.chatIdentifier ?? ""
        let date:Date = Date.init()
        let userProfile = UserProfile.getCurrentUser()

        let ref = Database.database().reference().child("Messages").child(fromId).child(toId)
        let childRef = ref.childByAutoId()
       // let timeStamp = NSNumber.init(value: Date().timeIntervalSince1970)
        let timeInMiliSec = Int64((Date().timeIntervalSince1970 * 1000.0).rounded())//Int64(Date().timeIntervalSince1970 * 1000)
        let timeStamp = NSNumber.init(value: timeInMiliSec)

        
        var values: [String : AnyObject] = ["to":toId as AnyObject, "from":fromId as AnyObject, "timeStamp":timeStamp as AnyObject]
        values = values.merged(with: property)
        childRef.updateChildValues(values)
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                print(error!)
                return
            }else{
                let message = JSQMessage(senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: date, text: messageText)
                self.chatMessagesData.add(message!)
                self.finishSendingMessage(animated: true)
                let recipentUserMessage = Database.database().reference().child("Messages").child(toId).child(fromId)
                let messageId = (childRef.key)!
                let recipentUserMessageRef = recipentUserMessage.child(messageId)
                recipentUserMessageRef.updateChildValues(values)
                recipentUserMessageRef.updateChildValues(values) { (error, ref) in
                    if error != nil {
                        print(error!)
                        return
                    }else{
                        let receiverID = fromId///self.receivingUser?.chatIdentifier ?? ""
                        let firstName = userProfile?.firstName ?? ""//self.receivingUser?.firstName ?? ""
                        let lastName = userProfile?.lastName ?? "" //self.receivingUser?.lastName ?? ""
                        let profileImgUrl = userProfile?.profileImgUrl ?? "" //self.receivingUser?.userImgUrl ?? ""

                        var userFullName = "Zify"
                        if let fname = userProfile?.firstName, let lname = userProfile?.lastName{
                            userFullName = "\(fname) \(lname)"
                        }
                        
                        let dataDict:[String : String] = ["dialog_id" :receiverID,"fName":firstName,"lName":lastName,"image":profileImgUrl,"message":messageText,"title":userFullName]
                        
                        FirebaseChatClass.sharedInstance.sendPushNotification(to: self.recevierDeviceToken, title: userFullName, body: messageText, dataDict:dataDict )
                    }
                }
            }
        }
        
        
     
        
        
        //get unread messages count
        
        
        var count:NSNumber = 0
        self.getUnreadCountFromHistoryBasedOnSelectedUser(selectedUser: toId) { lastMessageObj in
            let contactsSchemaRef = Database.database().reference().child("Contacts")
            count = lastMessageObj.unreadCount ?? 0
            let incrementValue:Int = count.intValue + 1
            count = NSNumber.init(value: incrementValue)
            let zeroCount = NSNumber.init(value: 0)
            //For Sender
            let lastActivityValues: [String : AnyObject] = ["dateStamp":timeStamp as AnyObject, "lastMessage":messageText as AnyObject,"unreadCount":zeroCount  as AnyObject,"profileImageurl":(self.receivingUser?.userImgUrl ?? "") as AnyObject,"userFirstname":(self.receivingUser?.firstName ?? "") as AnyObject , "userLastname":self.receiverLastName as AnyObject] //
            let lastActivity: [String : AnyObject] = ["LastActivity":lastActivityValues as AnyObject]
            let contactsRef = contactsSchemaRef.child(fromId).child(toId)
            contactsRef.updateChildValues(lastActivity)
            //For Receiver
            let lastActivityValues1 = ["dateStamp":timeStamp as AnyObject, "lastMessage":messageText as AnyObject,"unreadCount": count,"profileImageurl":(userProfile?.profileImgUrl ?? "") as AnyObject,"userFirstname":(userProfile?.firstName ?? "") as AnyObject , "userLastname":(userProfile?.lastName ?? "") as AnyObject]
             let lastActivity1 = ["LastActivity":lastActivityValues1 as AnyObject]
            let receiverRef = contactsSchemaRef.child(toId).child(fromId)
            receiverRef.updateChildValues(lastActivity1)
            
            
         
        }
    }
    
    
    
    
    func getUnreadCountFromHistoryBasedOnSelectedUser(selectedUser:String, completion: @escaping (LastMessage) -> Void) {
        let userID = Auth.auth().currentUser!.uid
        let contactsSchemaRef = Database.database().reference().child("Contacts").child(selectedUser).child(userID)
        var messages = [LastMessage]()
        contactsSchemaRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dict = snapshot.value as? [String: AnyObject] else {
                let message = LastMessage()  // Fresh Chat
                message.unreadCount = 0
                messages.append(message)
                completion(messages[0])
                return
            }
          //  print("messages are \(dict) and ID is \(snapshot.key)")
            let messageDict = dict["LastActivity"] as? [String : AnyObject]
            
            let message = LastMessage()
            message.id = snapshot.key
            message.setValuesForKeys(messageDict!)
            messages.append(message)
            completion(messages[0])
            
        })
    }
    
    
    
    func updateUnreadCountFromHistoryBasedOnSelectedUser(selectedUser:String) {
        let userID = Auth.auth().currentUser!.uid
        let contactsSchemaRef = Database.database().reference().child("Contacts").child(userID).child(selectedUser)
        contactsSchemaRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dict = snapshot.value as? [String: AnyObject] else {
                return
            }
            let messageDict = dict["LastActivity"] as? [String : AnyObject]
            let zeroCount = NSNumber.init(value: 0)

            let message = LastMessage()
            message.id = snapshot.key
            message.setValuesForKeys(messageDict!)
            let lastActivityValues : [String : AnyObject] = ["dateStamp":message.dateStamp ?? "" as AnyObject, "lastMessage":message.lastMessage as AnyObject,"unreadCount": zeroCount,"profileImageurl":message.profileImageurl as AnyObject,"userFirstname":message.userFirstname as AnyObject , "userLastname": message.userLastname as AnyObject]
            let lastActivity: [String : AnyObject] = ["LastActivity":lastActivityValues as AnyObject]
            contactsSchemaRef.updateChildValues(lastActivity)
            
        })
    }
    
    
    func observeMessage() {
        guard let uid = Auth.auth().currentUser?.uid, let toId = self.receivingUser?.chatIdentifier else {
            return
        }
        var count:Int = 0
        self.userMessageRef = Database.database().reference().child("Messages").child(uid).child(toId)
        self.userMessageRef.observe(.childAdded, with: { (snapshot) in
            guard let dict = snapshot.value as? [String: AnyObject] else {
                return
            }
            count = count + 1
            let message = Message()
            message.setValuesForKeys(dict)
            if(count > self.initialChatCount && message.from != uid){
                if (message.to == uid){
                    self.updateUnreadCountFromHistoryBasedOnSelectedUser(selectedUser: self.receivingUser?.chatIdentifier ?? "")
                }
                self.chatMessagesData.add(JSQMessage(senderId: message.from, senderDisplayName: self.receivingUser?.firstName, date: Date.init(), text: message.message))
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                    if self.automaticallyScrollsToMostRecentMessage {
                        self.scrollToBottom(animated: true)
                    }
                }
            }
        }, withCancel: nil)
    }
    
    override func didPressAccessoryButton(_ sender: UIButton?) {
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, messageDataForItemAt indexPath: IndexPath?) -> JSQMessageData? {
        return chatMessagesData[indexPath?.item ?? 0] as? JSQMessageData
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, didDeleteMessageAt indexPath: IndexPath?) {
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, messageBubbleImageDataForItemAt indexPath: IndexPath?) -> JSQMessageBubbleImageDataSource? {
        /**
         *  You may return nil here if you do not want bubbles.
         *  In this case, you should set the background color of your collection view cell's textView.
         *
         *  Otherwise, return your previously created bubble image data objects.
         */
        
        let message = chatMessagesData[indexPath?.item ?? 0] as? JSQMessage
        
        if (message?.senderId == senderId) {
            return outgoingBubbleImageData
        }
        
        return incomingBubbleImageData
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, avatarImageDataForItemAt indexPath: IndexPath?) -> JSQMessageAvatarImageDataSource? {
        /**
         *  Return `nil` here if you do not want avatars.
         *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
         *
         *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
         *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
         *
         *  It is possible to have only outgoing avatars or only incoming avatars, too.
         */
        
        /**
         *  Return your previously created avatar image data objects.
         *
         *  Note: these the avatars will be sized according to these values:
         *
         *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
         *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
         *
         *  Override the defaults in `viewDidLoad`
         */
        let message = chatMessagesData[indexPath?.item ?? 0] as? JSQMessage
        
        if (message?.senderId == senderId) {
            return outgoingUserImage
        }
        
        return incomingUserImage
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForCellTopLabelAt indexPath: IndexPath?) -> NSAttributedString? {
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        let message = chatMessagesData[indexPath?.item ?? 0] as? JSQMessage
        return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message?.date)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForCellBottomLabelAt indexPath: IndexPath?) -> NSAttributedString? {
        return nil
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chatMessagesData.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        /**
         *  Override point for customizing cells
         */
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as? JSQMessagesCollectionViewCell
        
        /**
         *  Configure almost *anything* on the cell
         *
         *  Text colors, label text, label colors, etc.
         *
         *
         *  DO NOT set `cell.textView.font` !
         *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
         *
         *
         *  DO NOT manipulate cell layout information!
         *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
         */
        
      //  let msg = chatMessagesData[indexPath.item] as? JSQMessage
        
        let msg = chatMessagesData[indexPath.item] as? JSQMessage
       // if msg?.isMediaMessage == nil {
        
          //  print("msg from == \(msg?.senderId) and sender id = \(senderId)")
            if (msg?.senderId == senderId) {
                cell?.textView.textColor = UIColor.black
            } else {
                cell?.textView.textColor = UIColor.white
            }
            
           
          /*  if let aColor = cell?.textView.textColor {
                 cell?.textView.linkTextAttributes = [NSAttributedStringKey.foregroundColor: aColor, NSAttributedStringKey.underlineStyle: [.styleSingle, .patternSolid].rawValue]
            }*/
       // }
        
        return cell!
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout?, heightForCellTopLabelAt indexPath: IndexPath?) -> CGFloat {
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every message
         */
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout?, heightForCellBottomLabelAt indexPath: IndexPath?) -> CGFloat {
        return 0.0
    }
    func composerTextView(_ textView: JSQMessagesComposerTextView?, shouldPasteWithSender sender: Any?) -> Bool {
        if UIPasteboard.general.image != nil {
            return false
        }
        return true
    }
    
    @IBAction func dismiss(_ sender: Any) {
        chatManager()?.chatMessageDelegate = nil
        navigationController?.popViewController(animated: false)
    }
    
    func initialiseView() {
        
        inputToolbar.contentView.leftBarButtonItem = nil
        title = receivingUser?.firstName?.capitalized
        
        /**
         *  You MUST set your senderId and display name
         */
        
        let loggedInUserId = Auth.auth().currentUser!.uid

        senderId = loggedInUserId //chatManager()?.currentUserChatIdentifier() == nil ? "" : chatManager()?.currentUserChatIdentifier()
        senderDisplayName = "You"
        inputToolbar.contentView.textView.pasteDelegate = self
        
        outgoingUserImage = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "YOU", backgroundColor: UIColor(white: 0.85, alpha: 1.0), textColor: UIColor(white: 0.60, alpha: 1.0), font: UIFont.systemFont(ofSize: 12.0), diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
        
        incomingUserImage = JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "placeholder_profile_image"), diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
        if receivingUser?.userImgUrl != nil {
        SDWebImageManager.shared().downloadImage(with: URL(string: receivingUser!.userImgUrl), options: SDWebImageOptions(rawValue: 0), progress: nil, completed: { image, error, cacheType, finished, imageURL in
            if image != nil {
                self.incomingUserImage = JSQMessagesAvatarImageFactory.avatarImage(with: image, diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
                self.collectionView?.reloadData()
                }
            })
        }
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        outgoingBubbleImageData = bubbleFactory?.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
        incomingBubbleImageData = bubbleFactory?.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleGreen())
        chatManager()?.chatMessageDelegate = self
        
        isViewInitialised = false
        
    }
    
    func loadUserChatMessages() {
        if !isViewInitialised {
           /* if let storedChatMessages = chatManager()?.getChatUserMessages(forChatIdentifier: receivingUser?.chatIdentifier) {
            chatMessagesData = NSMutableArray.init()
            if (storedChatMessages.count) > 0 {
                processUserChatMessages((storedChatMessages as? NSArray)! )
            } else {
                chatManager()?.fetchMessages(withConvId: chatDialog?.id, withHandler: { userChatMessages in
                    self.processUserChatMessages(userChatMessages as! NSArray)
                }, andReceivingUser: receivingUser, andIsNewUser: isNewUser)
            }
        }*/
            
            FirebaseChatClass.sharedInstance.getMessagesHistoryForOneToOne(otherPersonId: self.receivingUser?.chatIdentifier ?? "") { messagesArray in
             //   self.chatMessagesData = NSMutableArray.init()
                let storedChatMessages = NSArray.init(array: messagesArray)
                self.initialChatCount = storedChatMessages.count
                print("meessages count == \(String(describing: storedChatMessages.count))")
                DispatchQueue.main.async {
                        self.processUserChatMessages(storedChatMessages)
                        self.isViewInitialised = true
                        self.observeMessage()

                }
            }
            
            
            
            

        }
    }
    
    func processUserChatMessages(_ userChatMessages:NSArray) {
        
        chatMessagesData = NSMutableArray.init()
        
        let loggedInUserId = Auth.auth().currentUser!.uid

        
        
        for i in 0..<userChatMessages.count {
            
            
            let managedObject = userChatMessages.object(at: i) as? Message
            var isSelf:Int = 0
            let senderID = managedObject?.from ?? ""//(managedObject.value(forKey: "from") as? String)!
           // print("senderID is \(senderID)")
            if(senderID == loggedInUserId){
                isSelf = 1
            }
            var messageStr:String = ""
            var messageTime:Date = NSDate() as Date
            
            if let chatMessage = managedObject?.message{//managedObject.value(forKey: "message") {
                messageStr = chatMessage
            }
            
            if let seconds = managedObject?.timeStamp {
           // if let chatMessageTime = managedObject.value(forKey: "timeStamp") {
                let timesnapDate = Date.init(timeIntervalSince1970: TimeInterval(Int64(seconds)/1000))
                messageTime = timesnapDate
            }
            
            
            if(isSelf == 1){
                chatMessagesData.add(JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: messageTime, text: messageStr))
            }else{
                chatMessagesData.add(JSQMessage(senderId: receivingUser?.chatIdentifier, senderDisplayName: receivingUser?.firstName, date: messageTime, text: messageStr))
            }
            
            
            
          /*  let managedObject = (userChatMessages.object(at: i) as? NSManagedObject)!
            let isSelf:Int = (managedObject.value(forKey: "isSelfMessage") as? Int)!
            var messageStr:String = ""
            var messageTime:Date = NSDate() as Date
            
            if let chatMessage = managedObject.value(forKey: "message") {
                messageStr = chatMessage as! String
            }
            
            if let chatMessageTime = managedObject.value(forKey: "messageTime") {
                messageTime = chatMessageTime as! Date
            }

            
            if(isSelf == 1){
                chatMessagesData.add(JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: messageTime, text: messageStr))
            }else{
                chatMessagesData.add(JSQMessage(senderId: receivingUser?.chatIdentifier, senderDisplayName: receivingUser?.firstName, date: messageTime, text: messageStr))
            }*/
        }
       

        collectionView?.reloadData()
        if automaticallyScrollsToMostRecentMessage {
            scrollToBottom(animated: false)
        }
    }
    
    func newMessageReceived(_ message: String?, andDate date: Date?) {
        let receivedMessage = JSQMessage(senderId: receivingUser?.chatIdentifier, senderDisplayName: receivingUser?.firstName, date: date, text: message)
        chatMessagesData.add(receivedMessage)
        finishReceivingMessage(animated: true)
    }
    
    func messageReceiverChatIdentifier() -> String? {
        return receivingUser?.chatIdentifier
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

