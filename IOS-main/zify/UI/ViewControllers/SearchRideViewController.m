//
//  SearchRideViewController.m
//  zify
//
//  Created by Anurag S Rathor on 09/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "SearchRideViewController.h"
#import "MenuViewController.h"
#import "TripsTabController.h"
#import "AccountController.h"
//#import "NotificationsController.h"
#import "UserRideRequest.h"
#import "ServerInterface.h"
#import "PublishDriveRequest.h"

#import "CreateRideController.h"
#import "UserProfileController.h"
#import "GenericRequest.h"
#import "CurrentDriveController.h"
#import "AppViralityHelper.h"
#import "RideDriveActionRequest.h"
#import "UIImage+initWithColor.h"
#import "PushNotificationManager.h"
//#import "UserWalletController.h"
//#import "WalletStatementController.h"
#import "UniversalAlert.h"
#import "ChatManager.h"
#import "ChatNotificationView.h"
#import "UIBarButtonItem+Badge.h"
#import "RideRequestsController.h"
#import "LocalNotificationManager.h"
#import "UserFavouritesController.h"
#import "LocalisationConstants.h"
#import "CurrentLocale.h"
#import "UIView+iOS.h"
#import "CurrentTripNPendingRatingResponse.h"
//#import "DriveInvoiceController.h"
//#import "RideInvoiceController.h"
#import "TripRatingsRequest.h"
#import "UserDataManager.h"
#import "TutorialShownDB.h"
#import "DBUserDataInterface.h"
#import "GetFDJpointsRequest.h"
#import "FDJLocationObject.h"
#import "GetZenParkpointsRequest.h"
#import "ZenParkSignInRequest.h"
#import "UserOfferRideRequest.h"
#import "GoogleMapView.h"
#import "zify-Swift.h"
#import "GuestHomeController.h"
#import "Bottomsheet.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ProportionalFill.h"
#import "TripRide.h"

enum LocalityPickMode {
    PickingSourceLocality,
    PickingDestinationLocality
};

@interface SearchRideViewController () <EasyTipViewDelegate,SPSegmentControlCellStyleDelegate,SPSegmentControlDelegate>
@property(nonatomic,weak) IBOutlet SPSegmentedControl *switchControl;
// @property(nonatomic,weak) IBOutlet SPSegmentedControl *switchControl;
@end

@implementation SearchRideViewController{
    enum LocalityPickMode localityPickMode;
    
    NSArray *driveRoutes;
    NSArray *searchRides;
    NSString *rideStartDate;
    NSDateFormatter *dateTimeFormatter1;
    NSDateFormatter *dateFormatter1;
    // NSDateFormatter *timeFormatter1;
    BOOL isInitialLocationFetch;
    BOOL isMenuCurrentTripClicked;
    BOOL isGlobalLocale;
    UserDataManager *userDataManager;
    UserSearchData *userSearchData;
    BOOL isNotificationBtnTapped;
    
    LocalityInfo *homeAddressInfo;
    LocalityInfo *officeAddressInfo;
    UserProfile *currentUser;
    NSArray *cellsArray;
    NSString *strTravelType;
    BOOL isNeedToDisplayEasyTips;
    UIView *customViewProfileImage;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self addUserProfileImageViewAsLeftBarButton];
    [self setFreshChatUser];
    
    [self connectToSDL];

    _btnSwitchMode.hidden = YES;
    _switchControl.borderWidth = 1;
    _switchControl.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
    _switchControl.styleDelegate = self;
    _switchControl.delegate = self;
    [self addSwitchControlImages];
    _viewSwicthCOntrol.backgroundColor = [UIColor clearColor];
    //  UserProfile *user = [UserProfile getCurrentUser];
    // [OneSignal setEmail:user.emailId];
    
    if (IS_IPHONE_5) {
        _btnFindOrShareRide.titleLabel.fontSize = 18;
        _lblChooseYourMode.fontSize = 20;
        _btnCarOwner.titleLabel.fontSize = 16;
        _btnRider.titleLabel.fontSize = 16;
        _lblThisCanBeChangeLater.fontSize = 12;
        _btnHome.titleLabel.fontSize = 16;
        _btnWork.titleLabel.fontSize = 16;
    } else if (IS_IPHONE_6) {
        _btnFindOrShareRide.titleLabel.fontSize = 21;
        _lblChooseYourMode.fontSize = 21;
        _btnCarOwner.titleLabel.fontSize = 19;
        _btnRider.titleLabel.fontSize = 19;
        _lblThisCanBeChangeLater.fontSize = 13;
        _btnHome.titleLabel.fontSize = 17;
        _btnWork.titleLabel.fontSize = 17;
    } else if (IS_IPHONE_6_PLUS || IS_IPHONE_X) {
        _btnFindOrShareRide.titleLabel.fontSize = 23;
        _lblChooseYourMode.fontSize = 22;
        _btnCarOwner.titleLabel.fontSize = 20;
        _btnRider.titleLabel.fontSize = 20;
        _lblThisCanBeChangeLater.fontSize = 14;
        _btnHome.titleLabel.fontSize = 18;
        _btnWork.titleLabel.fontSize = 18;
    }
    
    _btnWork.layer.cornerRadius = _btnWork.frame.size.height/2;
    _btnHome.layer.cornerRadius = _btnWork.frame.size.height/2;
    self.destinationInfoView.hidden = YES;
    self.rideDateTimeView.hidden = true;
    
    [self.viewDestnTimeDateTravelPref addSubview:self.viewHomeWork];
    [self.viewDestnTimeDateTravelPref addSubview:self.viewFindOrShareRide];
    [self.viewDestnTimeDateTravelPref addSubview:self.viewTravelPref];
    
    
    //[self.btnFirstTimeShowDateTimePicker.layer setBorderColor:UIColor.lightGrayColor.CGColor];
    // [self.btnFirstTimeShowDateTimePicker.layer setBorderWidth:1];
    [self.btnFirstTimeShowDateTimePicker.layer setCornerRadius:5];
    [self.btnFirstTimeShowDateTimePicker.layer setMasksToBounds:NO];
    [self addDropShadowForView:self.btnFirstTimeShowDateTimePicker];
    
    [self addDropShadowForView:self.viewDestnTimeDate];
    
    [[AppDelegate getAppDelegateInstance] removeLocationAccessScreenIfEsists];
    [AppDelegate getAppDelegateInstance].topNavController = self;
    [self initialiseSearchRideScreen];
    
    self.viewTimeDate.hidden = false; //before 4/10/19 it was true
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnViewDateTime)];
    [self.viewTimeDate addGestureRecognizer:tapRecognizer];
    _btnBackOnMap.hidden = TRUE;
    [self drawShadowToAView:self.sourceInfoShadowView withColor:[UIColor colorWithRed:42.0/255.0 green:42.0/255.0 blue:42.0/255.0 alpha:0.5] withCornerRadius:self.sourceInfoShadowView.frame.size.height/2 withShadowRadius:4 withOffset:CGSizeMake(-1, 0)];
    [self drawShadowToAView:self.viewShade withColor:[UIColor colorWithRed:186.0/255.0 green:184.0/255.0 blue:184.0/255.0 alpha:0.8] withCornerRadius:12 withShadowRadius:5 withOffset:CGSizeMake(0, -3)];
    
    //[self showSplashOfferView];
    
    //[self performSelector:@selector(showSplashOfferView) withObject:self afterDelay:3.0];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self showSplashOfferView];
    });
    //[self performSelectorInBackground:@selector(showSplashOfferView) withObject:self];
    
    // UINavigationController *navController = [DummayViewController showDummyNavController];
    // [self.navigationController presentViewController:navController animated:NO completion:nil];
    
    
    /*  if([UserDataManager sharedInstance].deepLinkParams){
     [[UserDataManager sharedInstance] handleBranchDeepLinkParams:[UserDataManager sharedInstance].deepLinkParams];
     [UserDataManager sharedInstance].deepLinkParams = nil;
     }
     */
    
}

#pragma mark - Get Image On LeftbarButton item
- (void)getImageOnBarButtonItem {
    if(customViewProfileImage){
        [customViewProfileImage removeFromSuperview];
        customViewProfileImage = nil;
    }
    //Added Profile image to bar button
    UIImageView* imgProfile = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIImage *profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image.png"];
    NSURL *urlStr = [NSURL URLWithString:currentUser.profileImgUrl];
    //imgProfile.hidden =  YES;
    [imgProfile sd_setImageWithURL:urlStr placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        //  imgProfile.hidden =  NO;
    }];
    
    imgProfile.layer.masksToBounds = YES;
    imgProfile.layer.cornerRadius = 20;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imgProfile.layer.borderWidth = 2.0;
    
    CGSize itemSize = CGSizeMake(40, 40);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    
    [imgProfile.image drawInRect:imageRect];
    imgProfile.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    customViewProfileImage = [[UIView alloc]initWithFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, 40, 40)];
    [self drawShadowToAView:customViewProfileImage withColor:[UIColor grayColor] withCornerRadius:20 withShadowRadius:5 withOffset:CGSizeMake(0, 0)];
    
    customViewProfileImage.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    customViewProfileImage.layer.shadowRadius = 3.0f;
    customViewProfileImage.layer.shadowOpacity = 1.0f;
    
    
    [customViewProfileImage addSubview:imgProfile];
    
    UIButton* btnTemp = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTemp.frame = CGRectMake(0, 0, 40, 40);
    [btnTemp addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
    //  [btnTemp setImage:[imgProfile image] forState:UIControlStateNormal];
    
    btnTemp.layer.borderWidth = 2;
    btnTemp.layer.borderColor = [UIColor clearColor].CGColor;
    btnTemp.layer.masksToBounds = YES;
    btnTemp.layer.cornerRadius = 20;
    btnTemp.clipsToBounds = YES;
    
    btnTemp.layer.shadowOffset = CGSizeMake(0, 12); //CGSize(width: 0, height: 12);
    btnTemp.layer.shadowColor =  [UIColor lightGrayColor].CGColor; //UIColor(red: 0.06, green: 0.09, blue: 0.13, alpha: 0.8).cgColor;
    btnTemp.layer.shadowOpacity = 1;
    btnTemp.layer.shadowRadius = 14;
    
    btnTemp.backgroundColor = [UIColor clearColor];
    [customViewProfileImage addSubview:btnTemp];
    UIBarButtonItem* rightBtn = [[UIBarButtonItem alloc] initWithCustomView:customViewProfileImage];
    self.navigationItem.leftBarButtonItem = rightBtn;
    //  [self.view addSubview:viewShade];
    
}

- (void)tapOnViewDateTime {
    [self btnTappedShowDateTimePicker:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self removeEasyTipView];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:67.0/255.0 green:91.0/255.0 blue:108.0/255.0 alpha:1.0]];
    
    isNeedToDisplayEasyTips = NO;
    notificationsCount = 0;
    if(selectedDateFromPicker) {
        rideStartDate = [dateTimeFormatter1 stringFromDate:selectedDateFromPicker];
        self.rideDateText.text = [dateFormatter1 stringFromDate:selectedDateFromPicker];
        self.rideTimeText.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:selectedDateFromPicker];
        self.lblDate.text = [dateFormatter1 stringFromDate:selectedDateFromPicker];
        self.lblTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:selectedDateFromPicker];
    } else {
        // getting nil value need "_userSearchData.rideStartDate" value
        //userSearchData.rideStartDate = userDataManager.getUserSearchData.rideStartDate;
    }
    [self performSelector:@selector(getNotificationCountFromUserDetails) withObject:nil afterDelay:0.5];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isTpUpdated = [prefs boolForKey:@"tpUpdated"];
    if(isTpUpdated){
        [prefs setBool:NO forKey:@"tpUpdated"];
        [prefs synchronize];
        [self displayAppropriateViewsBasedOnTravelPreferences];
        return;
    }
    
    BOOL isNeedToSHowUpcomingTrips = [prefs boolForKey:@"isNeedToShowUpcomingTrips"];
    if(isNeedToSHowUpcomingTrips){
        [prefs setBool:NO forKey:@"isNeedToShowUpcomingTrips"];
        [self presentViewController:[TripsTabController createTripsTabNavController] animated:YES completion:nil];
        return;
    }
    
    BOOL isNeedToShowCurrentRide = [prefs boolForKey:@"isNeedToShowCurrentRide"];
    if(isNeedToShowCurrentRide){
        [prefs setBool:NO forKey:@"isNeedToShowCurrentRide"];
        [self showUpcomingRideWithLat:nil andLong:nil];
        return;
    }
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    isNotificationBtnTapped = YES;
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self displayproperViewForLocationEnabledOrDisabled];
    [self getImageOnBarButtonItem];
}

#pragma mark - Splash Offer View
- (void)showSplashOfferView {
    
    UserProfile *userObj = [UserProfile getCurrentUser];
    NSLog(@"=-----%@-----", [[[AppDelegate.getAppDelegateInstance remoteConfig] configValueForKey:@"app_launch_offerwalls"] stringValue]);
    NSString *strOfferWall = [[[AppDelegate.getAppDelegateInstance remoteConfig] configValueForKey:@"app_launch_offerwalls"] stringValue];
    
    NSData *data = [strOfferWall dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];//[strOfferWall dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableArray *arrOfferWall = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    
  //  NSMutableArray *arrOfferWall = ([@"country":@"IN",@"wall_url":@"https://zify-push-1.s3.ap-south-1.amazonaws.com/zify-offerwall-04102019-01.png",@"status":1],[@"country":@"DK",@"wall_url":@"https://zify-push-1.s3.ap-south-1.amazonaws.com/zify-push-offer-140219-01.png",@"status":0],[@"country":@"DE",@"wall_url":@"https://zify-push-1.s3.ap-south-1.amazonaws.com/zify-push-offer-150219-01.png",@"status":0] );
    
    
  /*  NSDictionary *o1 = @{@"country": @"IN",
                         @"wall_url": @"https://zify-push-1.s3.ap-south-1.amazonaws.com/zify-offerwall-04102019-01.png",
                         @"status": @1};
    
    NSDictionary *o2 = @{@"country": @"DK",
                         @"wall_url": @"https://zify-push-1.s3.ap-south-1.amazonaws.com/zify-push-offer-140219-01.png",
                         @"status": @0};
    NSDictionary *o3 = @{@"country": @"DE",
                         @"wall_url": @"https://zify-push-1.s3.ap-south-1.amazonaws.com/zify-push-offer-150219-01.png",
                         @"status": @0};
    
    NSMutableArray *arrOfferWall = @[o1, o2, o3];
    */
   // NSLog(@"=--joarrOfferWallsn---%@-----",arrOfferWall);
    for(id someObject in arrOfferWall)
    {
        NSLog(@"=--someObject---%@-----",someObject);
        if ([[someObject valueForKey:@"country"] isEqualToString:userObj.country]) {
            if ([[someObject valueForKey:@"status"] boolValue] == true) {
                NSLog(@"=--load--wall_url--Imahe---%@-----",[someObject valueForKey:@"wall_url"]);
                if ([[someObject valueForKey:@"wall_url"]isEqualToString:@""]) {
                    return;
                }
                [[AppDelegate getAppDelegateInstance]loadImageOfferSplashScreen:[someObject valueForKey:@"wall_url"]isShowUpdatedAlert:true];
            } else {
                NSLog(@"=--Do not load image status is false 0-----");
            }
        } else {
            NSLog(@"=--doe not mattch the countey----");
        }
    }
}

- (void)chooseModeView :(BOOL)isShowChooseModeView {
    //if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"IsCarOwnerOrRiderSeleted"]  isEqual: @"TRUE"]) {
    
    _viewDestnTimeDateTravelPref.frame = CGRectMake(0, AppDelegate.SCREEN_HEIGHT - _viewDestnTimeDateTravelPref.frame.size.height, AppDelegate.SCREEN_WIDTH, _viewDestnTimeDateTravelPref.frame.size.height);
    _lblWhereAreYouGoing.hidden = FALSE;
    [self.view addSubview:_viewDestnTimeDateTravelPref];
    
    if (isShowChooseModeView == TRUE) {
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.alpha = 0.7;
        visualEffectView.frame = self.view.bounds;
        [self.navigationController.view addSubview:visualEffectView];
        self.viewChooseMode.frame = CGRectMake(0, AppDelegate.SCREEN_HEIGHT, AppDelegate.SCREEN_WIDTH, self.viewChooseMode.frame.size.height);
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             self.viewChooseMode.frame = CGRectMake(0, AppDelegate.SCREEN_HEIGHT - self.viewChooseMode.frame.size.height, AppDelegate.SCREEN_WIDTH, self.viewChooseMode.frame.size.height);
                         }
                         completion:^(BOOL finished){
                         }];
        [self.navigationController.view addSubview:self.viewChooseMode];
    } else {
        //_viewDestnTimeDateTravelPref.frame = CGRectMake(0, AppDelegate.SCREEN_HEIGHT - _viewDestnTimeDateTravelPref.frame.size.height + 30, AppDelegate.SCREEN_WIDTH, _viewDestnTimeDateTravelPref.frame.size.height);
        //[self.view addSubview:_viewDestnTimeDateTravelPref];
    }
    
}

-(void)displayproperViewForLocationEnabledOrDisabled {
    [self chooseModeView:false];
    if([CLLocationManager locationServicesEnabled]) {
        if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
            [self checkLocationServicesEnabledOrNOt];
        }else if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways){
            [self checkLocationServicesEnabledOrNOt];
        }else if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            if(sourceLocalityInfo && destinationLocalityInfo){
                [[AppDelegate getAppDelegateInstance] removeLocationAccessScreenIfEsists];
                [self setCurrentLocation:sourceLocalityInfo];
            }else{
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                BOOL isFromLocationDeniedScreen = [prefs boolForKey:@"isFromDeniedScreen"];
                if(isFromLocationDeniedScreen){
                    [prefs setBool:NO forKey:@"isFromDeniedScreen"];
                    [prefs synchronize];
                    [self moveToLocationSearchFromLocationsServicesDeniedScreen];
                }else{
                    [self checkLocationServicesEnabledOrNOt];
                }
            }
        }
    }
}

-(void)checkLocationServicesEnabledOrNOt{
    if([CLLocationManager locationServicesEnabled]){
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                [[AppDelegate getAppDelegateInstance] showLocationAccessScreen:self];
            }];
        }else{
            [[AppDelegate getAppDelegateInstance] removeLocationAccessScreenIfEsists];
            if(sourceLocalityInfo == nil){
                isInitialLocationFetch = true;
            }
            [self fetchCurrentLocation];
        }
    }else{
        [self.messageHandler dismissBlockingLoadViewWithHandler:^{
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)initialiseSearchRideScreen {
    
    UIImage *navigationBackgroundImage = [UIImage imageWithColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4]];
    [self.navigationController.navigationBar setBackgroundImage:navigationBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    // NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSDate *currentDate = [NSDate date];
    dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yy"];
    
    
    
    //    timeFormatter1 = [[NSDateFormatter alloc] init];
    //    [timeFormatter1 setDateFormat:@"HH:mm"];
    //    [timeFormatter1 setLocale:locale];
    
    userDataManager = [UserDataManager sharedInstance];
    rideStartDate = [dateTimeFormatter1 stringFromDate:currentDate];
    self.rideDateText.text = [dateFormatter1 stringFromDate:currentDate];
    self.rideTimeText.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:currentDate];
    //self.lblDate.text = [dateFormatter1 stringFromDate:currentDate];
    //self.lblTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:currentDate];
    [self addDropShadowForView:self.viewTimeDate]; // added for shadow effect showing current date view
    selectedDateFromPicker = currentDate;
    [self selectedDate:currentDate];
    //  UIBarButtonItem *rideRequestBarButtonItem = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
    
    //  rideRequestBarButtonItem.badgeBGColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
    isInitialLocationFetch = true;
    isMenuCurrentTripClicked = false;
    _heremapsButton.hidden = true;
    isGlobalLocale = [[CurrentLocale sharedInstance] isGlobalLocale];
    // if(!isGlobalLocale) _heremapsButton.hidden = true;
    
    //rideStartDate = nil;
    self.rideDateText.numberOfLines = 0;
    self.rideDateText.text = NSLocalizedString(SELECT_DATE_TIME, nil);
    self.rideTimeText.text = @"";
    [self displayAppropriateViewsBasedOnTravelPreferences];
}
-(void)displayAppropriateViewsBasedOnTravelPreferences{
    currentUser = [UserProfile getCurrentUser];
    UserPreferences *preferences = currentUser.userPreferences;
    if(preferences){
        [self checkToShow_FindOrShareRideView];
        if(!preferences.startTime){
            preferences.startTime = @"";
        }
        if(!preferences.returnTime){
            preferences.returnTime = @"";
        }
        NSString *selectedStartTime = [[NSString alloc]initWithString: preferences.startTime];
        // NSLog(@"start time is %@", selectedStartTime);
        if(selectedStartTime && selectedStartTime.length!=0){
            //_startTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:[timeFormatter1 dateFromString:selectedStartTime]];
        }
        
        NSString *selectedReturnTime = [[NSString alloc]initWithString: preferences.returnTime];
        if(selectedReturnTime && selectedReturnTime.length!=0){
            //_returnTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:[timeFormatter1 dateFromString:selectedReturnTime]];
        }
        
        homeAddressInfo =[[LocalityInfo alloc] initWithName:preferences.sourceAddress  andAddress:preferences.sourceAddress andCity:preferences.city andState:@"" andCountry:@"" andLatLng:CLLocationCoordinate2DMake(preferences.sourceLatitude.doubleValue, preferences.sourceLongitude.doubleValue)];
        //_homeAddress.text = homeAddressInfo.address;
        officeAddressInfo = [[LocalityInfo alloc] initWithName:preferences.destinationAddress andAddress:preferences.destinationAddress andCity:preferences.city andState:@"" andCountry:@"" andLatLng:CLLocationCoordinate2DMake(preferences.destinationLatitude.doubleValue, preferences.destinationLongitude.doubleValue)];
        //_officeAddress.text = officeAddressInfo.address;
        
        if ((homeAddressInfo != nil || ![homeAddressInfo.address  isEqual: @""]) && (officeAddressInfo != nil || ![officeAddressInfo.address  isEqual: @""])) {
            //hide travel Preferences View then show HOME WORK view
            [self chooseModeView:FALSE];
            [self.viewTravelPref setHidden:TRUE];
            [self.viewFindOrShareRide setHidden:TRUE];
            self.viewHomeWork.frame = CGRectMake(self.viewDestnTimeDateTravelPref.frame.origin.x, self.viewTravelPref.frame.origin.y, AppDelegate.SCREEN_WIDTH, self.viewHomeWork.frame.size.height);
            //[self.viewDestnTimeDateTravelPref addSubview:self.viewHomeWork];
            [self.viewHomeWork setHidden:FALSE];
            
            
            
        } else {
            // show travel Preferences View
            [self.viewTravelPref setHidden:FALSE];
            [self.viewHomeWork setHidden:TRUE];
            [self.viewFindOrShareRide setHidden:TRUE];
            [self chooseModeView:TRUE];
            _lblWhereAreYouGoing.hidden = FALSE;//TRUE;
            //self.viewDestnTimeDateTravelPref.frame.size.height = 100;
        }
    } else {
        [self chooseModeView:TRUE];
    }
}

-(void)fetchCurrentLocation {
    if(isInitialLocationFetch) {
        if(![[ServerInterface sharedInstance] isConnectedToInternet]) {
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORTITLE, nil) WithMessage:NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORMESSAGE, nil)];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY,nil) WithAction:^(void *action){
                [self fetchCurrentLocation];
            }];
            [alert showInViewController:self];
        } else if(![[CurrentLocationTracker sharedInstance] isLocationTrackingEnabled]) {
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORTITLE, nil) WithMessage:NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORMESSAGE, nil)];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY,nil) WithAction:^(void *action){
                [self fetchCurrentLocation];
            }];
            [alert showInViewController:self];
        } else{
            [self updateUserCurrentLocation];
        }
    } else{
        [self fetchRideRequestsCount];
    }
}

#pragma mark - Actions
-(IBAction) btnTappedBackOnMap:(UIButton *)sender {
    _lblWhereAreYouGoing.hidden = FALSE;
    destinationLocalityInfo = nil;
    [self.viewDestnTimeDateTravelPref setNeedsLayout];
    [self.viewDestnTimeDateTravelPref layoutIfNeeded];
    _btnBackOnMap.hidden = TRUE;
    _txtDestination.text = @"";
    _txtDestination.delegate = self;
    [self checkToShow_FindOrShareRideView];
}


-(IBAction) btnTappedCarOwnerOrRider:(UIButton *)sender {
    isNeedToDisplayEasyTips = YES;
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.viewChooseMode.frame = CGRectMake(0, AppDelegate.SCREEN_HEIGHT, AppDelegate.SCREEN_WIDTH, 460);
                     }
                     completion:^(BOOL finished) {
                         if (finished)
                             
                             [self.viewChooseMode setHidden:YES];
                         
                         [visualEffectView removeFromSuperview];
                         
                         if(sender.tag == 0) {
                             strTravelType = @"DRIVER";//@"Car Owner";   //DRIVER
                         }else{
                             strTravelType = @"RIDER";//@"Rider";  //RIDER
                         }
                         [AppDelegate getAppDelegateInstance].travelTypeStr = strTravelType;
                         
                         _viewDestnTimeDateTravelPref.frame = CGRectMake(0, AppDelegate.SCREEN_HEIGHT - _viewDestnTimeDateTravelPref.frame.size.height + 30, AppDelegate.SCREEN_WIDTH, _viewDestnTimeDateTravelPref.frame.size.height);
                         [self.view addSubview:_viewDestnTimeDateTravelPref];
                         [self btnTappedTravelPreferences:nil];
                     }];
}

-(IBAction) btnTappedTravelPreferences:(id)sender {
    
    
    /*
     UINavigationController *navController = [NewTPScreenViewController showNewTPNavController];
     NewTPScreenViewController *obj = (NewTPScreenViewController *) [navController topViewController];
     obj.choosenModebyUser = strTravelType;
     [self.navigationController presentViewController:navController animated:YES completion:nil];
     */
    
    UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Home" bundle:NSBundle.mainBundle];
    SetAddressController *setAddressVC = [stb instantiateViewControllerWithIdentifier:@"SetAddressController"];
    setAddressVC.strTravelTypeInAddress = strTravelType;
    [self.navigationController pushViewController:setAddressVC animated:true];
    
    
    //    self.viewTravelPref.hidden = YES;
    //    self.viewFindOrShareRide.frame = CGRectMake(self.txtDestination.frame.origin.x, self.viewTravelPref.frame.origin.y, AppDelegate.SCREEN_WIDTH, self.viewFindOrShareRide.frame.size.height);
    //    [self.viewDestnTimeDateTravelPref addSubview:self.viewFindOrShareRide];
}

-(IBAction) btnTappedFindOrShareRide:(UIButton *)sender {
    if ([sender.titleLabel.text isEqual:NSLocalizedString(MS_TAKE_A_RIDE, "")]) {
        [self searchRide:sender];
    } else if ([sender.titleLabel.text isEqual:NSLocalizedString(MS_OFFER_A_DRIVE, "")]) {
        [self offerRide:sender];
    }
}

-(IBAction) btnTappedSwitchFindOrShareBtn:(UIButton *)sender {
    
    [UIView transitionWithView:self.btnFindOrShareRide duration:1.0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
        if (_btnSwitchMode.selected) {
            [self.btnFindOrShareRide setTitle:NSLocalizedString(MS_TAKE_A_RIDE, "") forState:UIControlStateNormal];
            _btnSwitchMode.selected = false;
            [_btnSwitchMode setImage:[UIImage imageNamed:@"riderSwitch"] forState:UIControlStateNormal];
        } else {
            _btnSwitchMode.selected = true;
            [self.btnFindOrShareRide setTitle:NSLocalizedString(MS_OFFER_A_DRIVE, "") forState:UIControlStateNormal];
            [_btnSwitchMode setImage:[UIImage imageNamed:@"carownerSwitch"] forState:UIControlStateNormal];
            
        }
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)btnTappedShowDateTimePicker:(id)sender {
    self.viewTimeDate.hidden = false;
    //[self addDropShadowForView:self.viewTimeDate]; comment here added in initialise screen bcz showing current time
    if(selectedDateFromPicker !=  nil){
        self.dateTimePickerView.selectedDate = selectedDateFromPicker;
    }
    self.dateTimePickerView.delegate = self;
    [self.dateTimePickerView showInView:self.navigationController.view];
}

- (IBAction)btnTappedHome:(id)sender {
    _lblWhereAreYouGoing.hidden = FALSE;//TRUE;
    
    [self.viewHomeWork setHidden:TRUE];
    [self.viewFindOrShareRide setHidden:FALSE];
    [self.viewTravelPref setHidden:TRUE];
    
    
    _btnBackOnMap.hidden = FALSE;
    destinationLocalityInfo = homeAddressInfo;
    self.destinationLocality.text = homeAddressInfo.address;
    
    self.txtDestination.text = homeAddressInfo.address;
    self.viewFindOrShareRide.frame = self.viewHomeWork.frame;
    
    [self.viewFindOrShareRide setNeedsLayout];
    [self.viewFindOrShareRide layoutIfNeeded];
}

- (IBAction)btnTappedWork:(id)sender {
    _lblWhereAreYouGoing.hidden = FALSE;//TRUE;
    //constTopViewDestnTravelPrefTimeDate.constant = 30;
    
    
    [self.viewFindOrShareRide setHidden:FALSE];
    [self.viewTravelPref setHidden:TRUE];
    [self.viewHomeWork setHidden:TRUE];
    
    _btnBackOnMap.hidden = FALSE;
    destinationLocalityInfo = officeAddressInfo;
    self.destinationLocality.text = officeAddressInfo.address;
    
    self.txtDestination.text = officeAddressInfo.address;
    //self.viewFindOrShareRide.frame = CGRectMake(self.viewDestnTimeDateTravelPref.frame.origin.x, self.viewTravelPref.frame.origin.y, AppDelegate.SCREEN_WIDTH, self.viewFindOrShareRide.frame.size.height);
    
    //    CGRect temp = self.viewDestnTimeDateTravelPref.frame;
    //    temp.origin.y = temp.origin.y + 30;
    //    self.viewDestnTimeDateTravelPref.frame = temp;
    
    
    //[self.viewDestnTimeDateTravelPref addSubview:self.viewFindOrShareRide];
    
}

-(IBAction) showMenu:(id)sender{
    @try{
        [self removeEasyTipView];
        [MenuViewController createAndOpenMenuWithSourceController:self.navigationController.parentViewController andMenuDelegate:self];
        [self.messageHandler dismissErrorView];
        [self.messageHandler dismissSuccessView];
    }@catch(NSException *ex){
        
    }
}


-(IBAction) viewRideRequests:(id)sender {
    [self removeEasyTipView];
    [self presentViewController:[RideRequestsController createRideRequestsNavController] animated:YES completion:nil];
}

-(IBAction) viewNotifications:(id)sender{
    [self removeEasyTipView];
    // [self getNotificationCountFromUserDetails];
    [self presentViewController:[NotificationController createNotificationsNavController] animated:YES completion:nil];
}

-(IBAction) viewFavourites:(id)sender{
    [self removeEasyTipView];
    [self presentViewController:[ChatUserListController createChatNavController] animated:YES completion:nil];
}

-(IBAction) selectRideDate:(UIButton *)sender {
    if(selectedDateFromPicker !=  nil){
        self.dateTimePickerView.selectedDate = selectedDateFromPicker;
    }
    self.dateTimePickerView.delegate = self;
    [self.dateTimePickerView showInView:self.navigationController.view];
    
    
    if ((_btnFirstTimeShowDateTimePicker.isHidden == false) && [_txtDestination.text isEqual:@""]) {
        //[self.viewTravelPref setHidden:FALSE];
        
        [self.viewFindOrShareRide setHidden:TRUE];
        [self.viewTravelPref setHidden:FALSE];
        [self.viewHomeWork setHidden:TRUE];
        
    } else {
        [self.viewTravelPref setHidden:TRUE];
        self.viewFindOrShareRide.frame = CGRectMake(self.viewDestnTimeDateTravelPref.frame.origin.x, self.viewTravelPref.frame.origin.y, AppDelegate.SCREEN_WIDTH, self.viewFindOrShareRide.frame.size.height);
        //[self.viewDestnTimeDateTravelPref addSubview:self.viewFindOrShareRide];
        
        [self.viewFindOrShareRide setHidden:TRUE];
        [self.viewTravelPref setHidden:FALSE];
        [self.viewHomeWork setHidden:FALSE];
        
    }
    
}

-(IBAction)openHereMapsApp:(UIButton *)sender{
    [AppUtilites openHereMapsApp];
}

-(IBAction) searchRide:(UIButton *)sender{
    // [FirebaseEventClass storeRideSearchEventFromSearchScreen];
    if([self validateSearchInfo]) {
        userSearchData = [[UserSearchData alloc] initWithSourceLocality:sourceLocalityInfo andDestLocality:destinationLocalityInfo andRideStartDate:rideStartDate andRideDateValue:nil andRideTimeValue:nil andMerchantId:nil];
        NSDictionary *dataDictToMoengage = [[NSDictionary alloc] initWithObjectsAndKeys:sourceLocalityInfo.address,@"source_address",destinationLocalityInfo.address,@"destination_address",[[AppDelegate getAppDelegateInstance] converDateToISOStandard:rideStartDate],@"searchDateTime", nil];
        [MoEngageEventsClass callRideSearchEventWithDataDict:dataDictToMoengage];
        [self performSegueWithIdentifier:@"showCreateRide" sender:self];
    }
}

-(IBAction) offerRide:(UIButton *)sender{
    if([self validateSearchInfo]){
        UserProfile *userProfile = [UserProfile getCurrentUser];
        if(userProfile.userDocuments.vehicleModel.length == 0 && userProfile.userDocuments.vehicleRegistrationNum.length == 0){
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_PUBLISHRIDE_VEHICLEDETAILSERRTITLE, nil) WithMessage:NSLocalizedString(VC_PUBLISHRIDE_VEHICLEDETAILSERRMESSAGE, nil)];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                UINavigationController *vehicleTextNavController = [VehicleTextController createVehcileTextNavControllerWithIsNavPresent:YES];
                [self presentViewController:vehicleTextNavController animated:YES completion:nil];
            }];
            [alert showInViewController:self];
        }else{
            NSLog(@"rideStartDate for offer is %@", rideStartDate);
            [self.messageHandler showBlockingLoadViewWithHandler:^{
                [[ServerInterface sharedInstance] getResponse:[[UserOfferRideRequest alloc] initWithSourceLocality:sourceLocalityInfo andDestinationLocality:destinationLocalityInfo andRideDate:rideStartDate andSeats:@1 andMerchantId:nil] withHandler:^(ServerResponse *response, NSError *error){
                    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                        if(response){
                            driveRoutes = (NSArray *)response.responseObject;
                            if([driveRoutes count] == 0){
                                [self.messageHandler showErrorMessage:NSLocalizedString(VC_SEARCHRIDE_NODRIVESMESSAGE, nil)];
                            } else{
                                [self performSegueWithIdentifier:@"showPublishRide" sender:self];
                            }
                        } else {
                            [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                        }
                    }];
                }];
            }];
        }
    }
}

-(void)dealloc {
    
    NSLog(@"Dealloc searchride is called");
}

-(IBAction)refreshUserCurrentLocation:(id)sender{
    [self updateUserCurrentLocation];
}

-(void) updateUserCurrentLocation{
    [self.messageHandler showTransparentBlockingLoadViewWithHandler:^{
        [CurrentLocationTracker sharedInstance].currentLocationTrackerDelegate = self;
        [[CurrentLocationTracker sharedInstance] updateCurrentLocation];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showPublishRide"]){
        PublishRideController *publishRideController =  (PublishRideController *)[(UINavigationController *)segue.destinationViewController topViewController];
        PublishDriveRequest *request = [[PublishDriveRequest alloc] initWithDepartureTime:[[AppDelegate getAppDelegateInstance] converDateToISOStandard:rideStartDate] andRoutine:@"" andReturnTime:@""];
        publishRideController.publishRideDelegate = self;
        //publishRideController.sourceAddLocInfo = sourceLocalityInfo;
        //publishRideController.destAddLocInfo =  destinationLocalityInfo;
        publishRideController.routes = driveRoutes;
        publishRideController.publishDriveRequest = request;
        publishRideController.isRouteSelectOnly = NO;
    } else if([segue.identifier isEqualToString:@"showCreateRide"]){
        CreateRideController *createRideController =  (CreateRideController *)[(UINavigationController *)segue.destinationViewController topViewController];
        createRideController.userSearchData = userSearchData;
        
        NSLog(@"***********choosen addresss issss*************");
        NSLog(@"***********Source addresss issss************* lat = %f, long = %f", userSearchData.sourceLocality.latLng.latitude, userSearchData.sourceLocality.latLng.longitude);
        NSLog(@"***********Destination addresss issss************* lat = %f, long = %f", userSearchData.destinationLocality.latLng.latitude, userSearchData.destinationLocality.latLng.longitude);

        
    } else if([segue.identifier isEqualToString:@"showDestinationLocality"]){
        localityPickMode = PickingDestinationLocality;
        LocalityAutoSuggestionController *autoSuggestionController = (LocalityAutoSuggestionController *)[(UINavigationController *)segue.destinationViewController topViewController];
        autoSuggestionController.isForSourceAddress = false;
        autoSuggestionController.sourceLocality = sourceLocalityInfo;
        autoSuggestionController.destinationLocality = destinationLocalityInfo;
        NSString *title = NSLocalizedString(MS_SERACHRIDE_BTN_SEARCH, nil);
        autoSuggestionController.title = [title capitalizedString];
        autoSuggestionController.selectionDelegate = self;
        autoSuggestionController.showAutosuggestOnly = NO;
    } else if([segue.identifier isEqualToString:@"showSourceLocality"]){
        localityPickMode = PickingSourceLocality;
        LocalityAutoSuggestionController *autoSuggestionController = (LocalityAutoSuggestionController *)[(UINavigationController *)segue.destinationViewController topViewController];
        autoSuggestionController.isForSourceAddress = YES;
        autoSuggestionController.sourceLocality = sourceLocalityInfo;
        autoSuggestionController.destinationLocality = destinationLocalityInfo;
        NSString *title = NSLocalizedString(MS_SERACHRIDE_BTN_SEARCH, nil);
        autoSuggestionController.title =[title capitalizedString];
        autoSuggestionController.selectionDelegate = self;
        autoSuggestionController.showAutosuggestOnly = NO;
    }
}

#pragma mark- MenuOptions Delegate
-(void) selectedMenuOption:(enum MenuOptions)option{
    if(option == SIDE_MENU_TRAVEL_PREFERENCE) {
        [[AppDelegate getAppDelegateInstance] moveTOTravelPrefenceScreen:self];
    }
    else if(option == SIDE_MENU_UPCOMING_TRIPS)
        [self presentViewController:[TripsTabController createTripsTabNavController] animated:YES completion:nil];
    else if(option == SIDE_MENU_HISTORY)
        [self presentViewController:[AccountController createAccountNavController] animated:YES completion:nil];
    else if(option == SIDE_MENU_WALLET) {
        UINavigationController *walletNavController = (UINavigationController *) [WalletController createUserWalletNavController];
        [self presentViewController:walletNavController animated:YES completion:nil];
    }
    else if(option == SIDE_MENU_PROFILE)
        [self presentViewController:[UserProfileController createUserProfileNavController] animated:YES completion:nil];
    else if(option == SIDE_MENU_ACTIVE_TRIPS) {
        isMenuCurrentTripClicked = true;
        [self updateCurrentRideLocation];
    }
    else if (option == SIDE_MENU_REWARDS) {
        [self presentViewController:[RewardsController createRewardsNavController]  animated:YES completion:nil];
    }
    else if (option == SIDE_MENU_SETTINGS)
        [self presentViewController:[SettingsViewController createSettingsNavController] animated:YES completion:nil];
    else if(option == SIDE_MENU_ABOUT)
        [self presentViewController:[AboutController createAboutNavController] animated:YES completion:nil];
    else if(option == SIDE_MENU_REWARDS) {
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [AppViralityHelper getCamapaignDetails:^(NSDictionary* campaignDetails){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    [AppViralityHelper showGrowthHackOnController:self withDetails:campaignDetails];
                }];
            }];
        }];
    }
    else if(option == SIDE_MENU_MESSAGE) [self presentViewController:[ChatUserListController createChatNavController] animated:YES completion:nil];
    else if(option == SIDE_MENU_WALLET) [self presentViewController:[WalletStatementController createWalletStatementNavController] animated:YES completion:nil];
    else if(option == SIDE_MENU_LEAVE_FEEDBACK){
        
        [Freshchat.sharedInstance showConversations:self];
        //disable from 6-Aug-19
       /* NSUserDefaults *prefs =  [NSUserDefaults standardUserDefaults];
        [prefs setBool:YES forKey:@"forDismissing"];
        [prefs synchronize];
        [self presentViewController:[AppDelegate createFeedbackNavController:self] animated:YES completion:nil];*/
    }
    else if(option == REFERANDEARN) {
        [self presentViewController:[ReferAndEarnController createReferAndEarnNavController] animated:YES completion:nil];
    }
}

-(void)updateCurrentRideLocation {
    if(isMenuCurrentTripClicked) {
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = self;
            [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
        }];
    } else{
        [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = self;
        [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
    }
}

//-(void)selectedLocality:(LocalityInfo *)location{
-(void)selectedLocality:(LocalityInfo *)sourceLocation withDestnationLocation:(LocalityInfo *)destiLocation{
    if(sourceLocation == nil){
        sourceLocation = sourceLocalityInfo;
    }
    [self updateSourceLocalityInfo:sourceLocation];
    [self updateDestinationLocalityInfo:destiLocation];
    double delayInSeconds = 0.3;
    if(sourceLocation != nil){
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [_mapContainerView.mapView moveToLocation:sourceLocation.latLng];
        });
    }
    /*if(sourceLocalityInfo){
     [self callServiceForGettingFDJPoints:sourceLocalityInfo];
     }*/
}
#pragma mark - UITextfield Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == _txtDestination) {
        
    }
}


#pragma mark - DateTime Picker Delegate

-(void)selectedDate:(NSDate *)selectedDate {
    selectedDateFromPicker = selectedDate;
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"en_GB"];
    dateTimeFormatter1.locale = locale;
    rideStartDate = [dateTimeFormatter1 stringFromDate:selectedDate];
    self.rideDateText.text = [dateFormatter1 stringFromDate:selectedDate];
    self.rideTimeText.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:selectedDate];
    
    //phase - 1
    [dateFormatter1 setDateFormat:@"MMM dd"];
    self.lblDate.text = [dateFormatter1 stringFromDate:selectedDate];
    self.lblTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:selectedDate];
    [self.btnFirstTimeShowDateTimePicker setHidden:TRUE];
    
    //[self checkToShow_FindOrShareRideView];
}



- (void)checkToShow_FindOrShareRideView {
    if ([_txtDestination.text isEqual:@""]){ //(_btnFirstTimeShowDateTimePicker.isHidden == false) || ) {
        _btnBackOnMap.hidden = TRUE;
        _lblWhereAreYouGoing.hidden = FALSE;
        UserProfile *profile = [UserProfile getCurrentUser];
        if(profile.userPreferences){
            [self.viewFindOrShareRide setHidden:TRUE];
            [self.viewTravelPref setHidden:TRUE];
            [self.viewHomeWork setHidden:FALSE];
        }else{
            [self.viewFindOrShareRide setHidden:TRUE];
            [self.viewTravelPref setHidden:FALSE];
            [self.viewHomeWork setHidden:TRUE];
        }
    } else {
        _btnBackOnMap.hidden = FALSE;
        _lblWhereAreYouGoing.hidden = FALSE;
        [self.viewFindOrShareRide setHidden:FALSE];
        [self.viewTravelPref setHidden:TRUE];
        [self.viewHomeWork setHidden:TRUE];
        
        //self.viewFindOrShareRide.frame = CGRectMake(self.viewDestnTimeDateTravelPref.frame.origin.x, self.viewTravelPref.frame.origin.y, AppDelegate.SCREEN_WIDTH, self.viewFindOrShareRide.frame.size.height);
    }
}

-(void) setCurrentRideLocation:(CLLocationCoordinate2D)currentLocation{
    NSNumber *currentLat = [NSNumber numberWithDouble:currentLocation.latitude];
    NSNumber *currentLong = [NSNumber numberWithDouble:currentLocation.longitude];
    [self showUpcomingRideWithLat:currentLat andLong:currentLong];
}

-(void) unableToFetchCurrentRideLocation:(NSError *)error{
    [self showUpcomingRideWithLat:nil andLong:nil];
}

-(void)showUpcomingRideWithLat:(NSNumber *)userLat andLong:(NSNumber *)userLong{
    [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:CURRENTTRIPNPENDINGRATING andUserLat:userLat andUserLong:userLong] withHandler:^(ServerResponse *response, NSError *error){
        if(isMenuCurrentTripClicked){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                [self handleCurrentRideResponse:response andError:error];
            }];
        } else{
            [self handleCurrentRideResponse:response andError:error];
        }
    }];
}


-(void)handleCurrentRideResponse:(ServerResponse *)response andError:(NSError *)error{
    if(response){
        
        [SDLConnection sharedManager].tripDepartureTime = nil;
        [SDLConnection sharedManager].tripInfo = nil;

        CurrentTripNPendingRatingResponse *rideResponse = (CurrentTripNPendingRatingResponse *)response.responseObject;
        UIViewController *visibleController = [AppUtilites applicationVisibleViewController];
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        if(rideResponse.pendingDriveRating){
            UINavigationController *driveInvoiceNavController = (UINavigationController *) [DriveInvoiceController createDriveInvoiceNavController];
            DriveInvoiceController *driveInvoiceController = (DriveInvoiceController *)driveInvoiceNavController.topViewController;
            driveInvoiceController.ratingResponse =  rideResponse;
            driveInvoiceController.showEmptyDrive = isMenuCurrentTripClicked;
            if(rootViewController.presentedViewController != nil){
                [rootViewController dismissViewControllerAnimated:NO completion:nil];
                visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
                [visibleController presentViewController:driveInvoiceNavController animated:YES completion:nil];
                
            }else{
                [visibleController presentViewController:driveInvoiceNavController animated:YES completion:nil];
            }
        } else if(rideResponse.pendingRideRating){
            //if(visibleController.navigationController) visibleController = visibleController.navigationController;
            //[RideInvoiceController showRideInvoiceController:visibleController andRatingResponse:rideResponse andShowEmptyDrive:isMenuCurrentTripClicked];
            
            
            // push to ride navigation controller
            UINavigationController *rideInvoiceNavController = (UINavigationController *) [RideInvoiceController createRideInvoiceNavController];
            RideInvoiceController *rideInvoiceController = (RideInvoiceController *)rideInvoiceNavController.topViewController;
            
            
            rideInvoiceController.ratingResponse = rideResponse;
            rideInvoiceController.showEmptyDrive = isMenuCurrentTripClicked;
            
            
            if(rootViewController.presentedViewController != nil){
                [rootViewController dismissViewControllerAnimated:NO completion:nil];
                visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
                [visibleController presentViewController:rideInvoiceNavController animated:YES completion:nil];
                
            }else{
                [visibleController presentViewController:rideInvoiceNavController animated:YES completion:nil];
            }
            
            
            
        }else if(rideResponse.currentDrive){
            NSLog(@"isMenuCurrentTripClicked is %d, and seats = %d",isMenuCurrentTripClicked,rideResponse.currentDrive.numOfSeats.intValue);
            //if(isMenuCurrentTripClicked || rideResponse.currentDrive.numOfSeats.intValue > 0)
            {
                UINavigationController *currentDriveNavController = (UINavigationController *) [CurrentDriveController createCurrentDriveNavController];
                CurrentDriveController *currentDriveController = (CurrentDriveController *)currentDriveNavController.topViewController;
                currentDriveController.currentDrive =  rideResponse.currentDrive;
                
                [SDLConnection sharedManager].tripDepartureTime = rideResponse.currentDrive.departureTime;
                [SDLConnection sharedManager].tripInfo = rideResponse.currentDrive;
                [[SDLConnection sharedManager] alertForStartTRip];
                
                if(rootViewController.presentedViewController != nil){
                    [rootViewController dismissViewControllerAnimated:NO completion:nil];
                    visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
                    [visibleController presentViewController:currentDriveNavController animated:YES completion:nil];
                }else{
                    [visibleController presentViewController:currentDriveNavController animated:YES completion:nil];
                }
            }
        } else if(rideResponse.currentRide){
            
            TripRide *currentRide =  rideResponse.currentRide;
            NSLog(@"status is %@", currentRide.status );
            if(![currentRide.status isEqualToString:@"PENDING"]){
                UINavigationController *currentRideNavController = (UINavigationController *) [CurrentRideController createCurrentRideNavController];
                CurrentRideController *currentRideController = (CurrentRideController *)currentRideNavController.topViewController;
                currentRideController.currentRide =  rideResponse.currentRide;
                if(rootViewController.presentedViewController != nil){
                    [rootViewController dismissViewControllerAnimated:NO completion:nil];
                    visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
                    [visibleController presentViewController:currentRideNavController animated:YES completion:nil];
                }else{
                    [visibleController presentViewController:currentRideNavController animated:YES completion:nil];
                }
            }else{
                [self.messageHandler showErrorMessage:NSLocalizedString(VC_SEARCHRIDE_NOACTIVEDRIVE, nil)];
            }
        } else if(isMenuCurrentTripClicked){
            [self.messageHandler showErrorMessage:NSLocalizedString(VC_SEARCHRIDE_NOACTIVEDRIVE, nil)];
        }
    } else if(isMenuCurrentTripClicked) {
        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
    }
    if(!isMenuCurrentTripClicked) [self performSelector:@selector(showPushNotificationData) withObject:self afterDelay:0.5];
    isMenuCurrentTripClicked = false;
    
    BOOL isShowIdScreen = [[NSUserDefaults standardUserDefaults] boolForKey:@"showIDScreenToUpload"];
    if(isShowIdScreen){
        [self presentViewController:[UserProfileController createUserProfileNavController] animated:NO completion:nil];
    }
}

-(void) showPushNotificationData{
    [[PushNotificationManager sharedInstance] showNotificationData];
}

-(void) setCurrentLocation:(LocalityInfo *)currentLocation{
    [self.messageHandler dismissTransparentBlockingLoadViewWithHandler:^{
        if(isInitialLocationFetch){
            [_mapContainerView createMapView];
            MapView *mapView = _mapContainerView.mapView;
            userSearchData = [userDataManager getUserSearchData]; //If guest user came by searching
            if(userSearchData && userSearchData.sourceLocality.address)  mapView.sourceCoordinate =  userSearchData.sourceLocality.latLng;
            else {
                [self updateSourceLocalityInfo:currentLocation];
                mapView.sourceCoordinate = currentLocation.latLng;
            }
            mapView.mapPadding = UIEdgeInsetsMake(CGRectGetHeight(self.destinationInfoView.frame), 0, CGRectGetHeight(self.destinationInfoView.frame), 0);
            [mapView initialiseMapWithSourceCoordinate];
            mapView.resolveMapMoveGesture = YES;
            mapView.mapViewDelegate = self;
            [self completeInitialLocationFetch];
        } else{
            [self updateSourceLocalityInfo:currentLocation];
            MapView *mapView = _mapContainerView.mapView;
            [mapView moveToLocation:currentLocation.latLng];
        }
        // [self callServiceForGettingFDJPoints:currentLocation];
    }];
}

-(void) unableToFetchCurrentLocation:(NSError *)error{
    [self.messageHandler dismissTransparentBlockingLoadViewWithHandler:^{
        if(isInitialLocationFetch){
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORTITLE, nil)  WithMessage:error.userInfo[@"message"]];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_RETRY, nil) WithAction:^(void *action){
                [self fetchCurrentLocation];
            }];
            [alert showInViewController:self];
        } else{
            [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
        }
    }];
}

-(void)completeInitialLocationFetch{
    isInitialLocationFetch = false;
    [[ChatManager sharedInstance] initialiseChat];
    
    [self fetchRideRequestsCount ];
    [userDataManager setDeepLinkFlagsOnLogin];
    if([userDataManager isDeepLinkDataPresent]){
        [userDataManager handleDeepLinkParams];
    } else if(userSearchData){
        if(userSearchData.sourceLocality.address){ //Deeplink flow will not have address.Hence no need to poulate the details
            [self updateSourceLocalityInfo:userSearchData.sourceLocality];
            [self updateDestinationLocalityInfo:userSearchData.destinationLocality];
            rideStartDate = userSearchData.rideStartDate;
            self.rideDateText.text = userSearchData.rideDateValue;
            self.rideTimeText.text = userSearchData.rideTimeValue;
        }
        [userDataManager resetUserSearchData];
        [self performSegueWithIdentifier:@"showCreateRide" sender:self];
    } else{
        [self updateCurrentRideLocation];
    }
    
    [[LocalNotificationManager sharedInstance] refreshLocalNotifications];
}

-(void)changedMapLocation:(LocalityInfo *)newLocation{
    
    [self updateSourceLocalityInfo:newLocation];
}

#pragma mark - Publish Ride Delegate
-(void) showActiveTripOnPublishRide{
    isMenuCurrentTripClicked = true;
    [self updateCurrentRideLocation];
}

-(void) updateSourceLocalityInfo:(LocalityInfo *)location{
    sourceLocalityInfo = location;
    self.sourceLocality.text = location.address;
    // if(localityPickMode == PickingSourceLocality)
    [self callServiceForGettingFDJPoints:location];
}

-(void) updateDestinationLocalityInfo:(LocalityInfo *)location{
    destinationLocalityInfo = location;
    self.destinationLocality.text = location.address;
    //phase - 1
    [self.txtDestination setText:location.address];
    [self checkToShow_FindOrShareRideView];
}

-(void)swapSourceAndDestinaion{
    NSLog(@"swap tapped....");
    if(sourceLocalityInfo && destinationLocalityInfo){
        LocalityInfo *tempLocality = sourceLocalityInfo;
        sourceLocalityInfo = destinationLocalityInfo;
        destinationLocalityInfo = tempLocality;
        self.sourceLocality.text = sourceLocalityInfo.address;
        self.destinationLocality.text = destinationLocalityInfo.address;
        //phase - 1
        self.txtDestination.text = destinationLocalityInfo.address;
    }
}

-(void)swapLocalityTapped{
    [self swapSourceAndDestinaion];
}
-(void)fetchRideRequestsCount{
    NSString *userToken = [UserProfile getCurrentUserToken];
    //NSLog(@"user token is %@", userToken);
    if(userToken){
        [[ServerInterface sharedInstance]getResponse:[[GenericRequest alloc] initWithRequestType:USERNOTIFICATIONSREQUEST] withHandler:^(ServerResponse *response, NSError *error){
            //  UIBarButtonItem *rideRequestBarButtonItem = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
            if(response){
                NSUInteger notificationCount  = [(NSArray *)response.responseObject count];
                self.rideRequestBarButtonItem.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)notificationCount];
                [self removeEasyTipView];
                if(notificationCount >0){
                    [self displayRideRequestsTipView];
                }
            } else{
                self.rideRequestBarButtonItem.badgeValue = @"0";
            }
        }];
    }
}


-(BOOL) validateSearchInfo{
    [self removeEasyTipView];
    if(sourceLocalityInfo == nil){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_SEARCHRIDE_SOURCEVAL, nil)];
        return false;
    }
    if(destinationLocalityInfo == nil){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_SEARCHRIDE_DESTVAL, nil) ];
        return false;
    }
    if(rideStartDate == nil){
        [self.messageHandler showErrorMessage:NSLocalizedString(VC_SEARCHRIDE_DEPDATEVAL, nil) ];
        return false;
    }
    return true;
}


-(void)getNotificationCountFromUserDetails{
    currentUser = [UserProfile getCurrentUser];
    NSString *isIdCardUploadedStr = [currentUser.userDocuments.isIdCardDocUploaded stringValue];
    NSString *isVehicleImgUploadedStr = [currentUser.userDocuments.isVehicleImgUploaded stringValue];
    if(![isIdCardUploadedStr isEqualToString:@"1"]){
        notificationsCount++;
    }
    if(!currentUser.userPreferences){
        notificationsCount++;
    }
    if(!currentUser.profileImgUrl || currentUser.profileImgUrl.length == 0){
        notificationsCount++;
    }
    if(currentUser.dob.length == 0 || currentUser.gender.length == 0 || currentUser.companyName.length == 0 || currentUser.companyEmail.length == 0 || currentUser.bloodGroup.length == 0 || currentUser.emergencyContact.length == 0){
        notificationsCount++;
    }
    if([currentUser.userPreferences.userMode isEqualToString:@"DRIVER"] && ![isVehicleImgUploadedStr isEqualToString:@"1"]){
        notificationsCount++;
    }
    // NSLog(@"notificationsCount is %d", notificationsCount);
    // notificationsCount = 1;
    self.btnNotification.badgeValue = [NSString stringWithFormat:@"%d",notificationsCount];
    if(notificationsCount > 0 && !isNotificationBtnTapped){
        [self displayNotificationsTipView];
    }
}
-(void)displayNotificationsTipView{
    [self removeEasyTipView];
    NSString * text =  NSLocalizedString(MS_SEARCHRIDE_LBL_NEW_NOTIFICATIONS, nil);
    self.btnNotification.customView.backgroundColor = [UIColor greenColor];
    NSLog(@"view is %@", self.btnNotification.view);
    if(!isNeedToDisplayEasyTips){
        @try{
            [EasyTipView showWithAnimated:YES forItem:self.btnNotification withinSuperview:self.navigationController.view text:text delegate:self];
        }@catch(NSException *ex){
            NSLog(@"ex is %@", [ex description]);
        }
    }
}
-(void)removeEasyTipView{
    for( UIView *subview in self.navigationController.view.subviews){
        if([subview isKindOfClass:[EasyTipView class]]){
            [subview removeFromSuperview];
        }
    }
}

-(void)displayRideRequestsTipView{
    NSString * text =  NSLocalizedString(MS_SEARCHRIDE_LBL_NEW_RIDE_REQUESTS, nil);
    self.rideRequestBarButtonItem.customView.backgroundColor = [UIColor greenColor];
    NSLog(@"view is %@", self.btnNotification.view);
    if(!isNeedToDisplayEasyTips){
        @try{
            [EasyTipView showWithAnimated:YES forItem:self.rideRequestBarButtonItem withinSuperview:self.navigationController.view text:text delegate:self];
        }@catch(NSException *ex){
            NSLog(@"ex is %@", [ex description]);
        }
    }
}

-(void)easyTipViewDidDismiss:(EasyTipView *)tipView{
    
}

#pragma mark - Call Services

-(void)callServiceForGettingFDJPoints:(LocalityInfo *)currentLocation{
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    isGlobalLocale = [currentLocale isGlobalLocale];
    // isGlobalLocale = YES;
    if(isGlobalLocale){
        isFDJServiceSuccess = NO;
        NSNumber *latNum = [NSNumber numberWithDouble:currentLocation.latLng.latitude];
        NSNumber *longNum = [NSNumber numberWithDouble:currentLocation.latLng.longitude];
        NSString *latValue = [latNum stringValue];
        NSString *longValue = [longNum stringValue];
        [[ServerInterface sharedInstance] getResponse:[[GetFDJpointsRequest alloc] initWithSrcLatitude:latValue andSrcLongitude:longValue] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    isFDJServiceSuccess = YES;
                    fdjPointsArr = nil;
                    fdjPointsArr = (NSArray *)response.responseObject;
                    [self addPickUpAndParkingPoints];
                } else {
                    // [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
        [self callServiceForZenparkSignIn:currentLocation];
    }else {
        //to get local MetroStation Points
        [self getMetroStationPoints];
    }
}

-(void)callServiceForGettingZenParkPoints:(LocalityInfo *)currentLocation{
    isZenParkServiceSuccess = NO;
    // GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] includingCoordinate:currentLocation.latLng];
    GMSMapView *googleMapObj = [AppDelegate getAppDelegateInstance].googleMapObject;
    NSLog(@"googleMapObj is %@", googleMapObj);
    GMSVisibleRegion region = googleMapObj.projection.visibleRegion;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    CLLocationCoordinate2D northEast = bounds.northEast;
    CLLocationCoordinate2D southWest = bounds.southWest;
    NSNumber *north =  [NSNumber numberWithDouble:northEast.latitude];
    NSNumber *south = [NSNumber numberWithDouble:southWest.latitude];
    NSNumber *west = [NSNumber numberWithDouble:southWest.longitude];
    NSNumber *east = [NSNumber numberWithDouble:northEast.longitude];
    
    /* north =@48.86560144954412;
     south =@48.85298064817098;
     west =@2.3607346631332575;
     east = @2.3833510495468317;*/
    [[ServerInterface sharedInstance] getResponse:[[GetZenParkpointsRequest alloc] initWithNorth:[north stringValue] andSouth:[south stringValue] andWest:[west stringValue] andEast:[east stringValue]] withHandler:^(ServerResponse *response, NSError *error){
        [self.messageHandler dismissBlockingLoadViewWithHandler:^{
            if(response){
                if([response.messageCode isEqualToString:@"1"]){
                    isZenParkServiceSuccess = YES;
                    zenParksArray = nil;
                    zenParksArray = [response.responseObject objectForKey:@"parkingLots"];
                    [self addPickUpAndParkingPoints];
                }
            } else {
                [AppDelegate getAppDelegateInstance].zenparkAuthToken = nil;
                [self callServiceForZenparkSignIn:currentLocation];
            }
        }];
    }];
}

-(void)addPickUpAndParkingPoints {
    if(isFDJServiceSuccess && isZenParkServiceSuccess) {
        [_mapContainerView.mapView removeMarkersFromMap];
        [self addFDJpointsToMap];
        [self addZenparkPointsToMap];
    }
}

-(void)callServiceForZenparkSignIn:(LocalityInfo *)currentLocation{
    
    [[ServerInterface sharedInstance] getResponse:[[ZenParkSignInRequest alloc] initWithSignInRequest] withHandler:^(ServerResponse *response, NSError *error){
        [self.messageHandler dismissBlockingLoadViewWithHandler:^{
            if(response){
                NSString *tokenStr = [response.responseObject objectForKey:@"token"];
                [AppDelegate getAppDelegateInstance].zenparkAuthToken = tokenStr;
                [self callServiceForGettingZenParkPoints:currentLocation];
            } else {
                [self callServiceForZenparkSignIn:currentLocation];
                // [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
            }
        }];
    }];
}
-(void)addFDJpointsToMap{
    for(NSDictionary *obj in fdjPointsArr){
        // NSString *idValue = [obj objectForKey:@"ID"];
        NSString *cxValue = [obj objectForKey:@"CX"];
        NSString *cyValue = [obj objectForKey:@"CY"];
        [_mapContainerView.mapView createMarkerWithhLat:[NSNumber numberWithDouble:cxValue.doubleValue] andLong:[NSNumber numberWithDouble:cyValue.doubleValue] andMarkerImage:[UIImage imageNamed:@"pin-point-final.png"]];
    }
}
-(void)addZenparkPointsToMap{
    for(NSDictionary *obj in zenParksArray){
        NSDictionary *locObj = [obj objectForKey:@"Location"];
        NSString  *latStr = [locObj objectForKey:@"Latitude" ];
        NSString  *longStr = [locObj objectForKey:@"Longitude"];
        NSNumber *latitude = [NSNumber numberWithDouble:[latStr doubleValue]];
        NSNumber *longitude = [NSNumber numberWithDouble:[longStr doubleValue]];
        [_mapContainerView.mapView createMarkerWithhLat:latitude andLong:longitude andMarkerImage:[UIImage imageNamed:@"zenpark.png"]];
    }
}
-(void)moveToLocationSearchFromLocationsServicesDeniedScreen{
    UINavigationController *autoSugeestionNavController = [LocalityAutoSuggestionController createLocalityAutoSuggestNavController];
    LocalityAutoSuggestionController *autoSuggestionController = (LocalityAutoSuggestionController *)[autoSugeestionNavController topViewController];
    autoSuggestionController.isForSourceAddress = YES;
    autoSuggestionController.sourceLocality = nil;
    autoSuggestionController.destinationLocality = nil;
    NSString *title = NSLocalizedString(MS_SERACHRIDE_BTN_SEARCH, nil);
    autoSuggestionController.title =  [title capitalizedString];
    autoSuggestionController.selectionDelegate = self;
    autoSuggestionController.showAutosuggestOnly = NO;
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:@"isFromManuaalySearch"];
    [prefs synchronize];
    [self presentViewController:autoSugeestionNavController animated:NO completion:nil];
}
-(void)methodForLocationServicesAccessDenied{
    [self.messageHandler dismissBlockingLoadViewWithHandler:^{
        if(self.messageHandler.loadingSpinner.superview){
            [self.messageHandler.loadingSpinner.superview removeFromSuperview];
        }
        [self checkLocationServicesEnabledOrNOt];
    }];
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)addDropShadowForView:(UIView *)viewShadow {
    viewShadow.backgroundColor = [UIColor whiteColor];
    viewShadow.layer.shadowColor = [UIColor colorWithRed:117.0/255.0 green:117.0/255.0 blue:117.0/255.0 alpha:0.4].CGColor;
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = CGSizeMake(-1, 0);
    viewShadow.layer.shadowRadius = 4;
    viewShadow.layer.masksToBounds = NO;
    if(viewShadow == self.btnFirstTimeShowDateTimePicker){
        viewShadow.layer.position = CGPointMake(-1, 0);
    }
    
}
-(void)drawShadowToAView:(UIView *)viewShadow withColor:(UIColor *)color withCornerRadius:(int)radius withShadowRadius:(int)shadowRadius withOffset:(CGSize)size{
    viewShadow.backgroundColor = [UIColor whiteColor];
    viewShadow.layer.shadowOpacity = 1;
    viewShadow.layer.shadowOffset = size;
    viewShadow.layer.shadowRadius = shadowRadius;
    viewShadow.layer.masksToBounds = NO;
    viewShadow.layer.cornerRadius = radius;
    viewShadow.layer.shadowColor = [color CGColor];
    
}

-(void)addSwitchControlImages{
    
    UIImage *img1 = [self createImageWithImage:@"Shape"];
    UIImage *img2 =[self createImageWithImage:@"relaxing-walk"];
    normalImagesArr = [[NSMutableArray alloc] initWithObjects:img1,img2, nil];
    
    UIImage *selectedimg1 = [self createImageWithImage:@"Shape_selected"];
    UIImage *selectedimg2 = [self createImageWithImage:@"relaxing-walk_selected"];
    selectedImagesArr = [[NSMutableArray alloc] initWithObjects:selectedimg1,selectedimg2, nil];
    
    SPSegmentedControlCell *cell1 = [self createCell:@"" wihImage:img1];
    SPSegmentedControlCell *cell2 = [self createCell:@"" wihImage:img2];
    self.switchControl.isRoundedFrame = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    [_viewSwicthCOntrol addGestureRecognizer:tapGesture];
    
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionUp | UISwipeGestureRecognizerDirectionDown)];
    [_viewSwicthCOntrol addGestureRecognizer:gestureRecognizer];
    
    cellsArray = [[NSArray alloc] initWithObjects:cell1,cell2, nil];
    
    currentUser = [UserProfile getCurrentUser];
    if(currentUser.userPreferences){
        NSString *userMode = currentUser.userPreferences.userMode;
        if([userMode isEqualToString:@"DRIVER"]){
            self.switchControl.defaultSelectedIndex = 0;
        }else{
            self.switchControl.defaultSelectedIndex = 1;
        }
    }
    [self.switchControl addWithCells:cellsArray];
    
}
- (void)viewTap:(UITapGestureRecognizer*)sender {
    if(self.switchControl.defaultSelectedIndex == 0){
        self.switchControl.defaultSelectedIndex = 1;
    }else{
        self.switchControl.defaultSelectedIndex = 0;
    }
    [self.switchControl addWithCells:cellsArray];
}
-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    if(self.switchControl.defaultSelectedIndex == 0){
        self.switchControl.defaultSelectedIndex = 1;
    }else{
        self.switchControl.defaultSelectedIndex = 0;
    }
    [self.switchControl addWithCells:cellsArray];
}

-(SPSegmentedControlCell *)createCell:(NSString *)text wihImage:(UIImage *)image
{
    SPSegmentedControlCell *cell = [[SPSegmentedControlCell alloc] init];
    cell.label.text = text;
    cell.imageView.image = image;
    return cell;
}

-(UIImage *)createImageWithImage:(NSString *)imageName{
    return [UIImage imageNamed:imageName];
    return [[UIImage imageNamed:imageName]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}
-(void)selectedStateWithSegmentControlCell:(SPSegmentedControlCell *)segmentControlCell forIndex:(NSInteger)index{
    NSLog(@"index is %d", index);
    BOOL isRider = 0;
    NSArray *cellsArray = _switchControl.cells;
    for (int i=0; i<cellsArray.count; i++){
        SPSegmentedControlCell *cell = [cellsArray objectAtIndex:i];
        cell.imageView.image = [normalImagesArr objectAtIndex:i];
    }
    segmentControlCell.imageView.image = [selectedImagesArr objectAtIndex:index];
    [UIView transitionWithView:self.btnFindOrShareRide duration:0.4 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
        if (index == 1) {
            __block isRider = 1;
            [self.btnFindOrShareRide setTitle:NSLocalizedString(MS_TAKE_A_RIDE, "") forState:UIControlStateNormal];
            [_btnSwitchMode setImage:[UIImage imageNamed:@"riderSwitch"] forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults] setBool:isRider forKey:@"choosenMode"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            __block isRider = 0;
            [self.btnFindOrShareRide setTitle:NSLocalizedString(MS_OFFER_A_DRIVE, "") forState:UIControlStateNormal];
            [_btnSwitchMode setImage:[UIImage imageNamed:@"carownerSwitch"] forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults] setBool:isRider forKey:@"choosenMode"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    } completion:^(BOOL finished) {
    }];
}
-(void)setFreshChatUser{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isFreshChatUserSet = [prefs boolForKey:@"isAddedFreshChat"];
    if(!isFreshChatUserSet){
        [prefs setBool:YES forKey:@"isAddedFreshChat"];
        [prefs synchronize];
        UserProfile *profile = [UserProfile getCurrentUser];
        // Create a user object
        FreshchatUser *user = [FreshchatUser sharedInstance];
        user.firstName = profile.firstName;
        user.lastName = profile.lastName;
        user.email = profile.emailId;
        user.phoneCountryCode = profile.isdCode;
        user.phoneNumber = profile.mobile;
        [[Freshchat sharedInstance] setUser:user];
        [[Freshchat sharedInstance]unreadCountWithCompletion:^(NSInteger count) {
            NSLog(@"your unread count : %d", (int)count);
        }];
        
        /* Managing Badge number for unread messages - Manual
         If you want to listen to a local notification and take care of updating the badge number yourself, listen for a notification with name "FRESHCHAT_UNREAD_MESSAGE_COUNT_CHANGED " as below
         */
        
        /*  [[NSNotificationCenter defaultCenter]addObserverForName:FRESHCHAT_UNREAD_MESSAGE_COUNT_CHANGED object:nil queue:nil usingBlock:^(NSNotification *note) {
         [[Freshchat sharedInstance]unreadCountWithCompletion:^(NSInteger count) {
         NSLog(@"your unread count : %d", (int)count);
         }];
         }];*/
    }
}

-(void)addUserProfileImageViewAsLeftBarButton{
    UIImage *image = [UIImage imageNamed:@"placeholder_profile_image.png"];
    
    UIButton *profileImageView = [UIButton buttonWithType:UIButtonTypeCustom];
    profileImageView.frame = CGRectMake(0, 0, 40, 40);
    profileImageView.cornerRadius = 20;
    [profileImageView setBackgroundImage:image forState:UIControlStateNormal];
    [profileImageView setBackgroundImage:image forState:UIControlStateSelected];
    [profileImageView addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftView = [[UIBarButtonItem alloc] initWithCustomView:profileImageView];
    self.navigationController.navigationItem.leftBarButtonItem = leftView;
}
- (void)getMetroStationPoints {
    NSString *path = [[NSBundle mainBundle]pathForResource:@"MetroStationsPoints" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    NSArray *jsonArr = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
    
    // NSLog(@"metro strations--%@", jsonArr);
    [_mapContainerView.mapView removeMarkersFromMap];
    for(NSDictionary *obj in jsonArr){
        // NSString  *locStationName = [obj objectForKey:@"Metro_Station_Name"];
        NSNumber *latitude = [NSNumber numberWithDouble:[[obj objectForKey:@"Lattitude" ] doubleValue]];
        NSNumber *longitude = [NSNumber numberWithDouble:[[obj objectForKey:@"Longitude"] doubleValue]];
        [_mapContainerView.mapView createMarkerWithhLat:latitude andLong:longitude andMarkerImage:[UIImage imageNamed:@"metro.png"]];
    }
    
}

-(void)connectToSDL{
    [[SDLConnection sharedManager] connectToSDL];
}

/*-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
 NSLog(@"zoom level position is %f", position.zoom);
 //cell.imageView.image = [historyImage imageScaledToFitSize:CGSizeMake(24,24)];
 }*/

@end
