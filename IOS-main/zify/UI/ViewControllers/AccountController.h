//
//  AccountController.h
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageHandler.h"
#import "ServerInterface.h"
#import  "GenericRequest.h"
#import "UserWallet.h"
#import "RateCardResponse.h"
#import "AppUtilites.h"
#import "ScrollableContentViewController.h"

@interface AccountController : ScrollableContentViewController
@property (nonatomic,weak) IBOutlet UIImageView *walletImage;
@property (nonatomic,weak) IBOutlet UILabel *availableCash;
@property (nonatomic,weak) IBOutlet UILabel *availablePoints;
@property (nonatomic,weak) IBOutlet UILabel *distanceTravelled;
@property (nonatomic,weak) IBOutlet UILabel *distanceOffered;
@property (nonatomic,weak) IBOutlet UILabel *totalRides;
@property (nonatomic,weak) IBOutlet UILabel *totalDrives;
@property (nonatomic,weak) IBOutlet UILabel *distanceTravelledText;
@property (nonatomic,weak) IBOutlet UILabel *distanceOfferedText;
@property (nonatomic,weak) IBOutlet UILabel *totalRidesText;
@property (nonatomic,weak) IBOutlet UILabel *totalDrivesText;
@property (nonatomic,weak) IBOutlet UIView *fareChartClickGlobalView;
@property (nonatomic,weak) IBOutlet UIView *fareChartClickView;
@property (nonatomic,weak) IBOutlet UIView *withdrawClickView;
@property (nonatomic,weak) IBOutlet UIView *fareMeterView;
@property (nonatomic,weak) IBOutlet UILabel *riderLocalCosting;
@property (nonatomic,weak) IBOutlet UILabel *riderIntercityCosting;
@property (nonatomic,weak) IBOutlet UILabel *driverLocalEarning;
@property (nonatomic,weak) IBOutlet UILabel *driverIntercityEarning;
@property (nonatomic,weak) IBOutlet UILabel *distanceUnit;
@property (nonatomic,weak) IBOutlet MessageHandler *messageHandler;
+(UINavigationController *)createAccountNavController;
@property (weak, nonatomic) IBOutlet UILabel *lblFareChartText;
@property (weak, nonatomic) IBOutlet UILabel *lblWithDrawText;

@property (weak, nonatomic) IBOutlet UILabel *lblFareDetailText;
@property (weak, nonatomic) IBOutlet UILabel *lblPassengerText;
@property (weak, nonatomic) IBOutlet UILabel *lblCarOwnerText;
@property (weak, nonatomic) IBOutlet UILabel *lblPaysText;
@property (weak, nonatomic) IBOutlet UILabel *lblEarnsText;

@property (weak, nonatomic) IBOutlet UILabel *lblLocalText;
@property (weak, nonatomic) IBOutlet UILabel *lblIntercityText;
@property (weak, nonatomic) IBOutlet UILabel *lblNoMinimumChargesText;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsAndConditionsApplyText;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;

@end
