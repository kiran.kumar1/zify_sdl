//
//  TripsRidesController.m
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripsRidesController.h"
#import  "UpcomingRideDetailCell.h"
#import "ServerInterface.h"
#import "TripRide.h"
#import "GenericRequest.h"
#import "RideDriveActionRequest.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDWebImageManager.h>
#import "ChatManager.h"
#import "UserContactDetail.h"
#import "ContactUserListController.h"
#import "LocalisationConstants.h"
#import "UniversalAlert.h"
#import "CurrentLocale.h"
#import "RouteInfoView.h"
#import "UserCallRequest.h"
#import "AppUtilites.h"
#import "ChatUserListController.h"

@interface TripsRidesController ()

@end
@implementation TripsRidesController{
    NSMutableArray *tripRides;
    UIImage *profileImagePlaceHolder;
    NSDateFormatter *dateTimeFormatter1;
    NSDateFormatter *dateFormatter1;
   // NSDateFormatter *timeFormatter1;
    NSDate *currentDayDate;
    BOOL isUserChatConnected;
    //BOOL isViewInitialised;
}
@synthesize isViewInitialised;

- (void)viewDidLoad {
    [super viewDidLoad];
    tripRides = nil;
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
//    timeFormatter1 = [[NSDateFormatter alloc] init];
//    [timeFormatter1 setDateFormat:@"HH:mm"];
//    [timeFormatter1 setLocale:locale];
    currentDayDate = [self getCurrentDayDate];
    isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    isViewInitialised = false;
    _tableView.estimatedRowHeight = 225.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self fetchTheServiceDetailsAndLoad];
    
}
-(void)fetchTheServiceDetailsAndLoad{
    if(!isViewInitialised){
        self.tableView.hidden = false;
        self.noRideView.hidden = true;
        [self.messageHandler showBlockingLoadViewWithHandler:^{
            [[ServerInterface sharedInstance]getResponse:[[GenericRequest alloc] initWithRequestType:UPCOMINGRIDESREQUEST] withHandler:^(ServerResponse *response, NSError *error){
                [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                    if(response){
                        tripRides = [NSMutableArray arrayWithArray:(NSArray *)response.responseObject];
                        [self handleTripRidesResponse];
                    } else{
                        [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                }];
            }];
        }];
        isViewInitialised = true;
    }
}
-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.messageHandler dismissErrorView];
    [self.messageHandler dismissSuccessView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tripRides.count == 0) return 0;
    return [tripRides count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellidentifier = @"cell";
    UpcomingRideDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell == nil){
        cell = (UpcomingRideDetailCell *)[[UpcomingRideDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    TripRide *ride = [tripRides objectAtIndex:indexPath.section];
    [cell setDataForCell:ride];
    [cell.cancelButton addTarget:self action:@selector(cancelRide:) forControlEvents:UIControlEventTouchUpInside];
    [cell.viewRouteButton addTarget:self action:@selector(showRouteInfo:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contactButton addTarget:self action:@selector(showCall:) forControlEvents:UIControlEventTouchUpInside];
    [cell.chatButton addTarget:self action:@selector(showChat:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
  /*  TripRide *ride = [tripRides objectAtIndex:indexPath.section];
    UpcomingRideDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"upcomingRideDetailCell"];
    
    cell.driverName.text = [NSString stringWithFormat:@"%@ %@",ride.driverDetail.firstName,ride.driverDetail.lastName];
    cell.driverImage.hidden = YES;
    [cell.driverImage sd_setImageWithURL:[NSURL URLWithString:ride.driverDetail.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        cell.driverImage.hidden = NO;
    }];
    if((isUserChatConnected && ride.driverDetail.chatProfile) || (ride.driverDetail.callId && ![@"PENDING" isEqualToString:ride.status]))
        cell.contactButton.hidden = NO;
    else cell.contactButton.hidden = YES;
    cell.source.text = ride.srcAddress;
    cell.destination.text = ride.destAddress;
    NSDate *departureTime = [dateTimeFormatter1 dateFromString:ride.departureTime];
    cell.tripTime.text = [timeFormatter1 stringFromDate:departureTime];
    cell.tripDate.text = [self getDateStringFromDepartureTime:departureTime];
    if([NSLocalizedString(CMON_GENERIC_TODAY, nil) isEqualToString:cell.tripDate.text]){
        cell.tripDate.textColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
        cell.tripTime.textColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
    } else{
        cell.tripDate.textColor = [UIColor lightGrayColor];
        cell.tripTime.textColor = [UIColor lightGrayColor];
    }
    cell.rideId.text = ride.rideId.stringValue;
    if([@"RUNNING" isEqualToString:ride.driveDetail.status]){
        cell.cancelButton.hidden = YES;
        cell.cancelButtonSeparator.hidden = YES;
        
    } else{
        cell.cancelButton.hidden = NO;
        cell.cancelButtonSeparator.hidden = NO;
    }
    if([@"PENDING" isEqualToString:ride.status]){
        cell.pendingStatus.hidden = false;
    } else{
        cell.pendingStatus.hidden = true;
    }
    cell.tripAmount.text = [NSString stringWithFormat:@"%@%@",[[CurrentLocale sharedInstance] getCurrencySymbol],ride.zifyPoints];
    return cell;*/
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BOOL isChatOprionAvailable = NO;
    BOOL isCallOptionAvailable = NO;
    TripRide *ride = [tripRides objectAtIndex:indexPath.section];
    BOOL isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    if(isUserChatConnected && ride.driverDetail.chatProfile){
        isChatOprionAvailable = YES;
    }
    if(ride.driverDetail.callId){
        isCallOptionAvailable = YES;
    }
    if(isChatOprionAvailable || isCallOptionAvailable){
        return 260.0f;
    }
    return 220.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    return view;
}

-(IBAction) dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction) showUserContactDetails:(id)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    TripRide *tripRide = [tripRides objectAtIndex:indexPath.section];
    NSMutableArray *contactUsersArray = [[NSMutableArray alloc] init];
    DriverDetail *driverDetail = tripRide.driverDetail;
    NSString *callId;
    if(![@"PENDING" isEqualToString:tripRide.status])
        callId = driverDetail.callId;
    UserContactDetail *userContactDetail = [[UserContactDetail alloc] initWithChatIdentifier:driverDetail.chatProfile && isUserChatConnected ? driverDetail.chatProfile.chatUserId : nil andCallIdentifier:callId andFirstName:driverDetail.firstName andLastName:driverDetail.lastName andImageUrl:driverDetail.profileImgUrl];
    [contactUsersArray addObject:userContactDetail];
    [ContactUserListController createAndOpenContactListController:self.navigationController WithUserContacts:contactUsersArray];
}
-(void) showChat:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    
    TripRide *tripRide = [tripRides objectAtIndex:indexPath.section];
    DriverDetail *driverDetail = tripRide.driverDetail;
    UserContactDetail *userContactDetail = [[UserContactDetail alloc] initWithChatIdentifier:driverDetail.chatProfile.chatUserId andCallIdentifier:driverDetail.callId andFirstName:driverDetail.firstName andLastName:driverDetail.lastName andImageUrl:driverDetail.profileImgUrl];
    UINavigationController *chatNavController = [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    chatUserListContoller.receivingUser = userContactDetail;
    [self presentViewController:chatNavController animated:YES completion:nil];
    
    
   /* BOOL isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
     TripRide *tripRide = [tripRides objectAtIndex:indexPath.section];
    [_currentDrive.driveRiders enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TripDriveRiderDetail *riderDetail = (TripDriveRiderDetail *)obj;
        RiderProfile *riderProfile = riderDetail.riderProfile;
        if((isUserChatConnected && riderProfile.chatProfile) || riderProfile.callId){
            UserContactDetail *userContactDetail = [[UserContactDetail alloc] initWithChatIdentifier:riderProfile.chatProfile.chatUserId andCallIdentifier:riderProfile.callId andFirstName:riderProfile.firstName andLastName:riderProfile.lastName andImageUrl:riderProfile.profileImageURL];
            [contactUsers addObject:userContactDetail];
        }
    }];
    UserContactDetail *contactDetail = [_contactUsers objectAtIndex:indexPath.row];
    UINavigationController *chatNavController = [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    chatUserListContoller.receivingUser = contactDetail;
    [_srcController presentViewController:chatNavController animated:YES completion:nil];
    */
}

-(void) showCall:(UIButton *)sender{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];

    TripRide *tripRide = [tripRides objectAtIndex:indexPath.section];
    DriverDetail *driverDetail = tripRide.driverDetail;
    NSString *callId;
  //  if(![@"PENDING" isEqualToString:tripRide.status])
        callId = driverDetail.callId;
    UserContactDetail *contactDetail = [[UserContactDetail alloc] initWithChatIdentifier:driverDetail.chatProfile && isUserChatConnected ? driverDetail.chatProfile.chatUserId : nil andCallIdentifier:callId andFirstName:driverDetail.firstName andLastName:driverDetail.lastName andImageUrl:driverDetail.profileImgUrl];
    
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[UserCallRequest alloc] initWithRequestType:INITIATECALL andCalleeId:contactDetail.callId] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response && ![response.responseObject isKindOfClass:[NSNull class]]){
                    [AppUtilites openCallAppWithNumber:response.responseObject];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}

-(IBAction)showRouteInfo:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    TripRide *tripRide = [tripRides objectAtIndex:indexPath.section];
    [RouteInfoView showRouteInfoOnView:self.navigationController.view WithTripRouteDetail:tripRide.routeDetail];
}

-(IBAction)cancelRide:(UIButton *)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:[self getTableViewCell:sender]];
    [self showAlertFortakingConfirmationForRide:indexPath];
    
   
}


-(void)handleTripRidesResponse{
    if([tripRides count] == 0){
        self.tableView.hidden = true;
        self.noRideView.hidden = false;
        self.noRideView.frame = CGRectMake(self.noRideView.frame.origin.x, self.noRideView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
        //self.rideButtonView.frame = CGRectMake(self.rideButtonView.frame.origin.x,self.noRideView.frame.size.height - self.rideButtonView.frame.size.height - 50,self.rideButtonView.frame.size.width, self.rideButtonView.frame.size.height);
    } else{
        self.tableView.hidden = false;
        self.noRideView.hidden = true;
    }
    [self.tableView reloadData];

}

-(UITableViewCell*)getTableViewCell:(id)button{
    while(![button isKindOfClass:[UITableViewCell class]]){
        button=[button superview];
    }
    return (UITableViewCell*)button;
}

-(NSDate *)getCurrentDayDate{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [calendar dateFromComponents:components];
}

-(NSString *)getDateStringFromDepartureTime:(NSDate *)date{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:currentDayDate toDate:date options:0];
    NSInteger difference = [components day];
    if(difference == 0) return NSLocalizedString(CMON_GENERIC_TODAY, nil);
    else if(difference == 1) return NSLocalizedString(CMON_GENERIC_TOMORROW, nil);
    else return [dateFormatter1 stringFromDate:date];
}
-(void)showAlertFortakingConfirmationForRide:(NSIndexPath *)indexPath{
    UniversalAlert *alert = [[UniversalAlert alloc]
        initWithTitle:NSLocalizedString(VC_TRIPRIDES_CANCELALERTTITLE, nil) WithMessage:NSLocalizedString(VC_TRIPRIDES_CANCELALERMESSAGE, nil)];
    [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
        [self callServiceForDeleteRide:indexPath];
    }];
    [alert addButton:BUTTON_CANCEL WithTitle:NSLocalizedString(CMON_GENERIC_CANCEL, nil) WithAction:^(void *action){
        
    }];
    [alert showInViewController:self];
}
-(void)callServiceForDeleteRide:(NSIndexPath *)indexPath{
    TripRide *tripRide = [tripRides objectAtIndex:indexPath.section];
    [self.messageHandler showBlockingLoadViewWithHandler:^{
        [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:CANCELRIDE andRideId:tripRide.rideId andDriveId:tripRide.driveId andDriverId:tripRide.driverId andRouteId:tripRide.routeId] withHandler:^(ServerResponse *response, NSError *error){
            [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                if(response){
                    [tripRides removeObjectAtIndex:indexPath.section];
                    [self handleTripRidesResponse];
                } else{
                    [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            }];
        }];
    }];
}
@end
