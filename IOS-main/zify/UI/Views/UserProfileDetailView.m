//
//  UserProfileDetailView.m
//  zify
//
//  Created by Anurag S Rathor on 25/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserProfileDetailView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CurrentLocale.h"
#import "LocalisationConstants.h"

@implementation UserProfileDetailView

+(void)showImageOnView:(UIView *)view WithDriverDetail:(DriverDetailEntity *)driverDetail{
    UserProfileDetailView *userProfileDetailView = [UserProfileDetailView getUserProfileDetailView];
    userProfileDetailView.userImage.hidden = NO;
    [userProfileDetailView.userImage sd_setImageWithURL:[NSURL URLWithString:driverDetail.vehicleImgUrl] placeholderImage:[UIImage imageNamed:@"placeholder_profile_large_image.png"] completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL) {
        userProfileDetailView.userImage.hidden = NO;
    }];
    
    
    [view addSubview:userProfileDetailView];
    NSDictionary *viewsDictionary = @{@"userProfileDetailView":userProfileDetailView};
    userProfileDetailView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[userProfileDetailView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[userProfileDetailView]|" options:0 metrics:nil views:viewsDictionary];
    [view addConstraints:horizontalConsts];
    [view addConstraints:verticalConsts];
}

+(void)showUserProfileOnView:(UIView *)view WithDriverDetail:(DriverDetailEntity *)driverDetail{
    UserProfileDetailView *userProfileDetailView = [UserProfileDetailView getUserProfileDetailView];
    userProfileDetailView.userImage.hidden = NO;
    [userProfileDetailView.userImage sd_setImageWithURL:[NSURL URLWithString:driverDetail.profileImgUrl] placeholderImage:[UIImage imageNamed:@"placeholder_profile_large_image.png"] completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        userProfileDetailView.userImage.hidden = NO;
    }];
    userProfileDetailView.userName.text = [NSString stringWithFormat:@"%@ %@",driverDetail.firstName,driverDetail.lastName];
    userProfileDetailView.maleRatingView.rating = [driverDetail.driverRatingMale floatValue];
    userProfileDetailView.maleRatingView.editable = NO;
    userProfileDetailView.femaleRatingView.rating = [driverDetail.driverRatingFemale floatValue];
    userProfileDetailView.femaleRatingView.editable = NO;
    userProfileDetailView.companyName.text = driverDetail.companyName;
    userProfileDetailView.sharedDistance.text = driverDetail.totalDistance.stringValue;
    NSString *distanceUnit = [[CurrentLocale sharedInstance] getDistanceUnit];
    userProfileDetailView.sharedDistanceUnit.text = [NSString stringWithFormat:@"%@ %@",distanceUnit,NSLocalizedString(VIEW_USERPROFILE_SHARED, nil)];
    if([@"VERIFIED" isEqualToString:driverDetail.driverStatus]){
          userProfileDetailView.userVerifiedImage.hidden = NO;
    } else{
        userProfileDetailView.userVerifiedImage.hidden = YES;
    }
    
    [view addSubview:userProfileDetailView];
    NSDictionary *viewsDictionary = @{@"userProfileDetailView":userProfileDetailView};
    userProfileDetailView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[userProfileDetailView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[userProfileDetailView]|" options:0 metrics:nil views:viewsDictionary];
    [view addConstraints:horizontalConsts];
    [view addConstraints:verticalConsts];
}

+(void)showUserProfileOnView:(UIView *)view WithRiderProfile:(RiderProfile *)riderProfile{
    UserProfileDetailView *userProfileDetailView = [UserProfileDetailView getUserProfileDetailView];
    userProfileDetailView.userImage.hidden = NO;
    [userProfileDetailView.userImage sd_setImageWithURL:[NSURL URLWithString:riderProfile.profileImageURL] placeholderImage:[UIImage imageNamed:@"placeholder_profile_large_image.png"] completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        userProfileDetailView.userImage.hidden = NO;
    }];
    userProfileDetailView.userName.text = [NSString stringWithFormat:@"%@ %@",riderProfile.firstName,riderProfile.lastName];
    userProfileDetailView.maleRatingView.rating = [riderProfile.userRatingMale floatValue];
    userProfileDetailView.maleRatingView.editable = NO;
    userProfileDetailView.femaleRatingView.rating = [riderProfile.userRatingFemale floatValue];
    userProfileDetailView.femaleRatingView.editable = NO;
    userProfileDetailView.companyName.text = riderProfile.companyName;
    userProfileDetailView.sharedDistance.text =riderProfile.totalDistance.stringValue;
    NSString *distanceUnit = [[CurrentLocale sharedInstance] getDistanceUnit];
    userProfileDetailView.sharedDistanceUnit.text = [NSString stringWithFormat:@"%@ %@",distanceUnit,NSLocalizedString(VIEW_USERPROFILE_SHARED, nil)];
    if([@"VERIFIED" isEqualToString:riderProfile.userStatus]){
        userProfileDetailView.userVerifiedImage.hidden = NO;
    } else{
        userProfileDetailView.userVerifiedImage.hidden = YES;
    }
    
    [view addSubview:userProfileDetailView];
    NSDictionary *viewsDictionary = @{@"userProfileDetailView":userProfileDetailView};
    userProfileDetailView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[userProfileDetailView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[userProfileDetailView]|" options:0 metrics:nil views:viewsDictionary];
    [view addConstraints:horizontalConsts];
    [view addConstraints:verticalConsts];
}


+(UserProfileDetailView *)getUserProfileDetailView{
    NSArray *viewsArray = [[NSBundle mainBundle] loadNibNamed:@"UserProfileDetail" owner:self options:nil];
    UserProfileDetailView *userProfileDetailView = (UserProfileDetailView *)[viewsArray objectAtIndex:0];
    return userProfileDetailView;
}

-(IBAction) dismissUserProfileView:(UIButton *)sender{
    [self removeFromSuperview];
}
@end
