//
//  PushNotificationView.h
//  zify
//
//  Created by Anurag S Rathor on 25/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PushNotificationInfo.h"




@interface PushNotificationView : UIView
@property (nonatomic,weak) IBOutlet UIView *notificationView;
@property (nonatomic,strong) PushNotificationInfo *notificationInfo;
+(void)showPushNotificationView:(NSDictionary *)notificationDict andIsApplaunch:(BOOL)isAppLaunch;
+(void)handlePushNotificationAction:(NSString *)actionIdentifier andNotificationDict:(NSDictionary *)notificationDict;
-(void) populateNotificationDetails;
-(CAGradientLayer*)failureGradient;
-(CAGradientLayer*)successGradient;
@end
