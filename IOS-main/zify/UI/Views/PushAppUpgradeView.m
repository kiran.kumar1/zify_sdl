//
//  PushAppUpgradeView.m
//  zify
//
//  Created by Anurag S Rathor on 08/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushAppUpgradeView.h"
#import "AppUtilites.h"
#import "PushAppUpgradeInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PushAppUpgradeView
+(PushAppUpgradeView *)createNotificationView{
    NSArray *viewsArray = [[NSBundle mainBundle] loadNibNamed:@"PushNotificationViews" owner:self options:nil];
    PushAppUpgradeView *upgradeView;
    for (id object in viewsArray) {
        if ([object isKindOfClass:[self class]]) {
            upgradeView = (PushAppUpgradeView *)object;
            break;
        }
    }
    return upgradeView;
}

-(void)populateNotificationDetails{
    PushAppUpgradeInfo *upgradeInfo = (PushAppUpgradeInfo *)self.notificationInfo;
    _upgradeText.text = upgradeInfo.message;
    _upgradeImage.hidden = YES;
    NSLog(@"forcedUpgrade is %d", (int)upgradeInfo.forcedUpgrade);
    int forceUpgrade = upgradeInfo.forcedUpgrade.intValue;
    if(forceUpgrade > 0){
        self.btnCancel.hidden = YES;
    }else{
        self.btnCancel.hidden = NO;
    }
    if([upgradeInfo.hasImage intValue]){
        _defaultImageView.hidden = YES;
        NSString* encodedURL = [upgradeInfo.imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [_upgradeImage sd_setImageWithURL:[NSURL URLWithString:encodedURL] completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
            _upgradeImage.hidden = NO;
        }];
    }
    
}

-(IBAction)openAppStore:(id)sender{
    [self removeFromSuperview];
    [self performSelector:@selector(moveToAppStore) withObject:self afterDelay:0.3];
}

-(IBAction) dismissUpgradeNotificationView:(id)sender{
    [self removeFromSuperview];
}


-(void)moveToAppStore{
    [AppUtilites openAppStore];
}
@end
