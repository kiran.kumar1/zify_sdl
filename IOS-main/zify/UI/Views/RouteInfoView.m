//
//  RouteInfoView.m
//  zify
//
//  Created by Anurag S Rathor on 05/02/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "RouteInfoView.h"
#import "GoogleMapView.h"
#import "UserSearchData.h"
#import "CurrentLocale.h"
#import "AppDelegate.h"
#import "zify-Swift.h"
@import GoogleMaps;
@implementation RouteInfoView

+(void)showRouteInfoOnViewFromUpcomingRides:(UIView *)view WithTripDetail:(TripRide *)trip{
    TripRouteDetail *routeDetail = trip.routeDetail;
    RouteInfoView *routeInfoView = [RouteInfoView getRouteInfoView];
    routeInfoView.routeSource.text = routeDetail.srcAdd;
    routeInfoView.routeDestination.text = routeDetail.destAdd;
    [routeInfoView.routeMapContainer createMapView];
    MapView *mapView = routeInfoView.routeMapContainer.mapView;
    mapView.sourceCoordinate = CLLocationCoordinate2DMake(routeDetail.srcLat.doubleValue,routeDetail.srcLong.doubleValue);
    mapView.destCoordinate = CLLocationCoordinate2DMake(routeDetail.destLat.doubleValue,routeDetail.destLong.doubleValue);
    mapView.overviewPolylinePoints = routeDetail.overviewPolylinePoints;
    mapView.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0);
    [view addSubview:routeInfoView];
    NSDictionary *viewsDictionary = @{@"routeInfoView":routeInfoView};
    routeInfoView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[routeInfoView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[routeInfoView]|" options:0 metrics:nil views:viewsDictionary];
    [view addConstraints:horizontalConsts];
    [view addConstraints:verticalConsts];
    [mapView drawMap];
    
    if(trip.srcNearLng.doubleValue == 0 && trip.srcNearLat.doubleValue == 0 && trip.destNearLat.doubleValue == 0 && trip.destNearLng.doubleValue == 0){
        return;
    }
    UIImage *pickupImage = [UIImage imageNamed:@"PickUp_Marker.png"];
    UIImage *dropImage = [UIImage imageNamed:@"Drop_Marker.png"];
    UIImage *centerIcon = [UIImage imageNamed:@"transparentoval.png"];

    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:trip.srcNearLat.doubleValue] andLong:[NSNumber numberWithDouble:trip.srcNearLng.doubleValue] andMarkerImage:pickupImage];
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:trip.destNearLat.doubleValue] andLong:[NSNumber numberWithDouble:trip.destNearLng.doubleValue] andMarkerImage:dropImage];
    
    
    
 
    GMSPath *path = [GMSPath pathFromEncodedPath:trip.routeDetail.overviewPolylinePoints];
    GMSPolyline* route = [GMSPolyline polylineWithPath:path];
    CLLocationCoordinate2D center = [route.path coordinateAtIndex: (route.path.count-1)/2];
    NSString *walkingDistance = @"";
    walkingDistance = [NSString stringWithFormat:@"%.02f %@",trip.distance.floatValue, [[CurrentLocale sharedInstance] getDistanceUnit]];
    
    [mapView createMarkerForPickupWithhLat:[NSNumber numberWithDouble:center.latitude] andLong:[NSNumber numberWithDouble:center.longitude] andMarkerImage:centerIcon withWalkingDistance:walkingDistance];


 //   [mapView createMarkerForPickupWithhLat:[NSNumber numberWithDouble:trip.srcNearLat.doubleValue] andLong:[NSNumber numberWithDouble:trip.srcNearLng.doubleValue] andMarkerImage:pickupImage withWalkingDistance:walkingDistance];
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:trip.srcNearLat.doubleValue] andLong:[NSNumber numberWithDouble:trip.srcNearLng.doubleValue] andMarkerImage:pickupImage];


    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:trip.destNearLat.doubleValue] andLong:[NSNumber numberWithDouble:trip.destNearLng.doubleValue] andMarkerImage:dropImage];
    
    UIImage *pickupImage1 = [UIImage imageNamed:@"icn_source_marker.png"];
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:trip.srcActualLat.doubleValue] andLong:[NSNumber numberWithDouble:trip.srcActualLng.doubleValue] andMarkerImage:pickupImage1];
    
    UIImage *dropImage1 = [UIImage imageNamed:@"icn_destination_marker.png"];
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:trip.destActualLat.doubleValue] andLong:[NSNumber numberWithDouble:trip.destActualLng.doubleValue] andMarkerImage:dropImage1];
    
    CLLocation *searchStart = [[CLLocation alloc] initWithLatitude:trip.srcActualLat.doubleValue longitude:trip.srcActualLng.doubleValue];
    CLLocation *pickupStart = [[CLLocation alloc] initWithLatitude:trip.srcNearLat.doubleValue longitude:trip.srcNearLng.doubleValue];
    // destiationMarker.icon = [UIImage imageNamed:@"icn_destination_marker.png"];
    
    [mapView drawDashedLineOnMapBetweenOrigin:searchStart destination:pickupStart];
    
    CLLocation *dropStart = [[CLLocation alloc] initWithLatitude:trip.destNearLat.doubleValue longitude:trip.destNearLng.doubleValue];
    
    CLLocation *searchEnd = [[CLLocation alloc] initWithLatitude:trip.destActualLat.doubleValue longitude:trip.destActualLng.doubleValue];
    [mapView drawDashedLineOnMapBetweenOrigin:dropStart destination:searchEnd];

    
    
}
+(void)showRouteInfoOnView:(UIView *)view WithTripRouteDetail:(TripRouteDetail *)routeDetail{
    RouteInfoView *routeInfoView = [RouteInfoView getRouteInfoView];
    routeInfoView.routeSource.text = routeDetail.srcAdd;
    routeInfoView.routeDestination.text = routeDetail.destAdd;
    [routeInfoView.routeMapContainer createMapView];
    MapView *mapView = routeInfoView.routeMapContainer.mapView;
    mapView.sourceCoordinate = CLLocationCoordinate2DMake(routeDetail.srcLat.doubleValue,routeDetail.srcLong.doubleValue);
    mapView.destCoordinate = CLLocationCoordinate2DMake(routeDetail.destLat.doubleValue,routeDetail.destLong.doubleValue);
    mapView.overviewPolylinePoints = routeDetail.overviewPolylinePoints;
    mapView.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0);
    [view addSubview:routeInfoView];
    NSDictionary *viewsDictionary = @{@"routeInfoView":routeInfoView};
    routeInfoView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[routeInfoView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[routeInfoView]|" options:0 metrics:nil views:viewsDictionary];
    [view addConstraints:horizontalConsts];
    [view addConstraints:verticalConsts];
    [mapView drawMap];
    
}

+(void)showRouteInfoOnView:(UIView *)view WithSearchRide:(SearchRideEntity *)searchRide{
    RouteInfoView *routeInfoView = [RouteInfoView getRouteInfoView];
    routeInfoView.routeSource.text = searchRide.srcAdd;
    routeInfoView.routeDestination.text = searchRide.destAdd;
    [routeInfoView.routeMapContainer createMapView];
    MapView *mapView = routeInfoView.routeMapContainer.mapView;
    mapView.sourceCoordinate = CLLocationCoordinate2DMake(searchRide.srcLat.doubleValue,searchRide.srcLong.doubleValue);
    mapView.destCoordinate = CLLocationCoordinate2DMake(searchRide.destLat.doubleValue,searchRide.destLong.doubleValue);
    mapView.overviewPolylinePoints = searchRide.overviewPolylinePoints;
    mapView.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0);
    [view addSubview:routeInfoView];
    NSDictionary *viewsDictionary = @{@"routeInfoView":routeInfoView};
    routeInfoView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[routeInfoView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[routeInfoView]|" options:0 metrics:nil views:viewsDictionary];
    [view addConstraints:horizontalConsts];
    [view addConstraints:verticalConsts];
    [mapView drawMap];
    
    if(searchRide.srcNearLong.doubleValue == 0 && searchRide.srcNearLat.doubleValue == 0 && searchRide.destNearLong.doubleValue == 0 && searchRide.destNearLat.doubleValue == 0){
        return;
    }
    UIImage *pickupImage = [UIImage imageNamed:@"PickUp_Marker.png"];
    UIImage *dropImage = [UIImage imageNamed:@"Drop_Marker.png"];
    
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:searchRide.srcNearLat.doubleValue] andLong:[NSNumber numberWithDouble:searchRide.srcNearLong.doubleValue] andMarkerImage:pickupImage];
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:searchRide.destNearLat.doubleValue] andLong:[NSNumber numberWithDouble:searchRide.destNearLong.doubleValue] andMarkerImage:dropImage];
}

+(void)showMapInfoOnView:(MapView *)mapView WithSearchRide:(SearchRideEntity *)searchRide withCollectionViewHeight:(CGFloat)height withUserSearchData:(UserSearchData *)searchData withIsforFirstTime:(BOOL)isForFirstTime{
    
    mapView.sourceCoordinate = CLLocationCoordinate2DMake(searchRide.srcLat.doubleValue,searchRide.srcLong.doubleValue);
    mapView.destCoordinate = CLLocationCoordinate2DMake(searchRide.destLat.doubleValue,searchRide.destLong.doubleValue);
    mapView.overviewPolylinePoints = searchRide.overviewPolylinePoints;
    mapView.mapPadding = UIEdgeInsetsMake(0, 0, height, 0);
    
    if(isForFirstTime){
        [mapView drawMap];
    }else{
        [mapView putMarkersOnSameMap];
    }
    if(searchRide.srcNearLong.doubleValue == 0 && searchRide.srcNearLat.doubleValue == 0 && searchRide.destNearLong.doubleValue == 0 && searchRide.destNearLat.doubleValue == 0){
        return;
    }
    UIImage *pickupImage = [UIImage imageNamed:@"PickUp_Marker.png"];
    UIImage *dropImage = [UIImage imageNamed:@"Drop_Marker.png"];
    UIImage *centerIcon = [UIImage imageNamed:@"transparentoval.png"];

    
    //  [mapView createMarkerWithhLat:[NSNumber numberWithDouble:searchRide.srcNearLat.doubleValue] andLong:[NSNumber numberWithDouble:searchRide.srcNearLong.doubleValue] andMarkerImage:pickupImage];
    
  
    
    
    GMSPath *path = [GMSPath pathFromEncodedPath:searchRide.overviewPolylinePoints];
    GMSPolyline* route = [GMSPolyline polylineWithPath:path];
    CLLocationCoordinate2D center = [route.path coordinateAtIndex: (route.path.count-1)/2];
    NSString *walkingDistance = @"";
    walkingDistance = [NSString stringWithFormat:@"%.02f %@",searchRide.distance.floatValue, [[CurrentLocale sharedInstance] getDistanceUnit]];


   // [mapView createMarkerForPickupWithhLat:[NSNumber numberWithDouble:searchRide.srcNearLat.doubleValue] andLong:[NSNumber numberWithDouble:searchRide.srcNearLong.doubleValue] andMarkerImage:pickupImage withWalkingDistance:walkingDistance];
    
    
    
    [mapView createMarkerForPickupWithhLat:[NSNumber numberWithDouble:center.latitude] andLong:[NSNumber numberWithDouble:center.longitude] andMarkerImage:centerIcon withWalkingDistance:walkingDistance];
    
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:searchRide.srcNearLat.doubleValue] andLong:[NSNumber numberWithDouble:searchRide.srcNearLong.doubleValue] andMarkerImage:pickupImage];

    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:searchRide.destNearLat.doubleValue] andLong:[NSNumber numberWithDouble:searchRide.destNearLong.doubleValue] andMarkerImage:dropImage];
    
    UIImage *pickupImage1 = [UIImage imageNamed:@"icn_source_marker.png"];
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:searchData.sourceLocality.latLng.latitude] andLong:[NSNumber numberWithDouble:searchData.sourceLocality.latLng.longitude] andMarkerImage:pickupImage1];
    
    UIImage *dropImage1 = [UIImage imageNamed:@"icn_destination_marker.png"];
    [mapView createMarkerWithhLat:[NSNumber numberWithDouble:searchData.destinationLocality.latLng.latitude] andLong:[NSNumber numberWithDouble:searchData.destinationLocality.latLng.longitude] andMarkerImage:dropImage1];
    
    CLLocation *searchStart = [[CLLocation alloc] initWithLatitude:searchData.sourceLocality.latLng.latitude longitude:searchData.sourceLocality.latLng.longitude];
    CLLocation *pickupStart = [[CLLocation alloc] initWithLatitude:searchRide.srcNearLat.doubleValue longitude:searchRide.srcNearLong.doubleValue];
    // destiationMarker.icon = [UIImage imageNamed:@"icn_destination_marker.png"];
    
    [mapView drawDashedLineOnMapBetweenOrigin:searchStart destination:pickupStart];
    
    CLLocation *dropStart = [[CLLocation alloc] initWithLatitude:searchRide.destNearLat.doubleValue longitude:searchRide.destNearLong.doubleValue];
    
    CLLocation *searchEnd = [[CLLocation alloc] initWithLatitude:searchData.destinationLocality.latLng.latitude longitude:searchData.destinationLocality.latLng.longitude];
    [mapView drawDashedLineOnMapBetweenOrigin:dropStart destination:searchEnd];
    
    
}

/*+(void)drawPolilinesBetweenorigin:(CLLocationCoordinate2D)origin withDestination:(CLLocationCoordinate2D)destination withmapViw:(MapView *)mapView{
    GMSMutablePath *path = [GMSMutablePath path];
    [path addCoordinate:origin];
    [path addCoordinate:destination];
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeWidth = 2;
    polyline.strokeColor = [UIColor orangeColor];
    polyline.map = mapView;
}*/

+(RouteInfoView *)getRouteInfoView{
    NSArray *viewsArray = [[NSBundle mainBundle] loadNibNamed:@"RouteDetail" owner:self options:nil];
    RouteInfoView *routeInfoView = (RouteInfoView *)[viewsArray objectAtIndex:0];
    return routeInfoView;
}

+(void)getMarkersOnMap:(MapView *)mapView  WithSearchRide:(SearchRideEntity *)searchRide {
    
    //MapView *mapView = routeInfoView.routeMapContainer.mapView;
    mapView.sourceCoordinate = CLLocationCoordinate2DMake(searchRide.srcLat.doubleValue,searchRide.srcLong.doubleValue);
    mapView.destCoordinate = CLLocationCoordinate2DMake(searchRide.destLat.doubleValue,searchRide.destLong.doubleValue);
    mapView.overviewPolylinePoints = searchRide.overviewPolylinePoints;
    mapView.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0);
   
}

-(IBAction) dismissRouteInfoView:(id)sender{
    [self removeFromSuperview];
}
@end
