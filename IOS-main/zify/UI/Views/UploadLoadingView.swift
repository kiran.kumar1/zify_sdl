//
//  UploadLoadingView.swift
//  zify
//
//  Created by Anurag Rathor on 13/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit
import Lottie

class UploadLoadingView: UIView {

    @IBOutlet var bgBlurView:UIView!
    @IBOutlet var infoView:UIView!
    @IBOutlet var animationView:LOTAnimationView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.showLoadingSymbol()
    }
    
    func showLoadingSymbol(){
        let lottieFileName = "loading"
        self.animationView.setAnimation(named: lottieFileName)
        self.animationView.loopAnimation = true
        self.animationView.play()
        let timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(showSuccessView), userInfo: nil, repeats: false)

        
    }
    func showSuccessView(){
        let lottieFileName = "tick"
        self.animationView.setAnimation(named: lottieFileName)
        self.animationView.loopAnimation = false
        self.animationView.play()
    }

}
