//
//  ShowImageView.swift
//  zify
//
//  Created by Anurag on 13/07/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit


class ShowImageView: UIView {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblCarNumber: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet var messageHandler: MessageHandler!
    
    class func setImageView(mainView: UIView?, withCarDetails: DriverDetailEntity?) {
        
        
        let imageInfoView = ShowImageView.getImageInfoView()
        
        let modeltext = NSLocalizedString(UP_VEHICLE_MODEL, comment: "")
        let registrationtext = NSLocalizedString(UP_VEHICLE_NUMBER, comment: "")
        
        imageInfoView?.lblCarName.text = modeltext + " : " + (withCarDetails?.vehicleModel)!
        imageInfoView?.lblCarNumber.text = registrationtext + " : "  + (withCarDetails?.vehicleRegistrationNum)!
        imageInfoView?.frame = (mainView?.frame)!
        imageInfoView?.alpha = 1
        
        let urlString = withCarDetails?.vehicleImgUrl
        if urlString == "" || urlString == nil {
            debugPrint("Car image not available")
            
        } else {
            let url = URL(string: urlString!)
            imageInfoView?.messageHandler.showBlockingLoadView(handler: {
                imageInfoView?.imgView.sd_setImage(with: url!, completed: { (img, err, cacheTypr, imgUrl) in
                    imageInfoView?.messageHandler.dismissBlockingLoadView(handler: {
                        imageInfoView?.imgView.image = img
                        mainView?.addSubview(imageInfoView!)
                    })
                })
            })
        }
       
        
    }
    
    
    class func getImageInfoView() -> ShowImageView? {
        let viewsArray = Bundle.main.loadNibNamed("ShowImage", owner: self, options: nil)
        let imageInfoView = viewsArray?[0] as? ShowImageView
        return imageInfoView
    }
    
    @IBAction func btnTappedClose(_ sender: Any) {
        removeFromSuperview()
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
