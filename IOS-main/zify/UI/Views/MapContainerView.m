//
//  MapContainerView.m
//  zify
//
//  Created by Anurag S Rathor on 21/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "MapContainerView.h"
#import "CurrentLocale.h"
#import "HereMapView.h"
#import "GoogleMapView.h"

@interface MapContainerView ()

@end

@implementation MapContainerView{
    BOOL isGlobalLocale;
}

- (void)createMapView {
    isGlobalLocale = [[CurrentLocale sharedInstance] isGlobalLocale];
   /* if(isGlobalLocale){
        _mapView =  [[HereMapView alloc] initWithFrame:self.frame];
    } else{
        _mapView =  [[GoogleMapView alloc] initWithFrame:self.frame];
    }*/
    _mapView =  [[GoogleMapView alloc] initWithFrame:self.frame];
    [self addSubview:_mapView];
    NSDictionary *viewsDictionary = @{@"mapView":_mapView};
    _mapView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[mapView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[mapView]|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:horizontalConsts];
    [self addConstraints:verticalConsts];
}
@end
