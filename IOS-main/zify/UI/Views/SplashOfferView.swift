//
//  SplashOfferView.swift
//  zify
//
//  Created by Anurag Rathor on 19/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class SplashOfferView: UIView {

    @IBOutlet var blurView:UIView!
    @IBOutlet var offerImageView:UIImageView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    
    
     func showImage(strImageUrl : String) -> UIImage {
        
        let splashBackgroundImageUrl = strImageUrl
        let url = URL(string: splashBackgroundImageUrl)
        print("------url AVIALABLE----\(String(describing: url))--")
        SDWebImageDownloader.shared()?.downloadImage(with: URL(string: splashBackgroundImageUrl), options: SDWebImageDownloaderOptions.ignoreCachedResponse, progress: nil, completed: { (img, data, error, finished) in
            if img != nil {
                print("------IMAGE AVIALABLE------")
                
                DispatchQueue.main.sync {
                   // return img
                    self.offerImageView.image = img
                }
            }
        })
        return UIImage()
    }
    
    @IBAction func btnCloseTapped(_ sener:UIButton){
        self.removeFromSuperview()
    }

}
