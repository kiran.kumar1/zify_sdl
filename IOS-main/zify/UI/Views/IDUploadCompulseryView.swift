//
//  IDUploadCompulseryView.swift
//  zify
//
//  Created by Anurag Rathor on 13/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

protocol moveToIDcardUploadScreenProtocal {
    func moveToIDcardUploadScreenMethod() -> Void
}


class IDUploadCompulseryView: UIView {
    
    @IBOutlet weak var bgBlurView:UIView!
    @IBOutlet weak var infoView:UIView!
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var descLbl:UILabel!
    @IBOutlet weak var setBtn:UIButton!
    
    var delegate: moveToIDcardUploadScreenProtocal?
    
    
    
    @IBAction func btnSetNowTapped(_ sender:UIButton){
        self.removeFromSuperview()
        AppDelegate.getInstance().showIDCardUploadScreen()
    }
    
    func setAttributesAndFontForControls() -> Void {
        
        self.titleLbl.fontSize = 12
        self.descLbl.fontSize = 12
        self.setBtn.titleLabel?.fontSize = 13;
        if UIScreen.main.sizeType == .iPhone6 {
            self.titleLbl.fontSize = 13
            self.descLbl.fontSize = 13
            self.setBtn.titleLabel?.fontSize = 14;
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            self.titleLbl.fontSize = 15
            self.descLbl.fontSize = 15
            self.setBtn.titleLabel?.fontSize = 16;
        }
        let idproofStr  = NSLocalizedString(id_proof_string, comment: "")
        self.descLbl.attributedText = self.attributedText(withString: self.descLbl.text ?? "", boldString: idproofStr, font: (self.descLbl.font)!)
        self.descLbl.textAlignment = .center
    }
    
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSFontAttributeName: font])
        let boldFontAttribute: [String: Any] = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
    
    func moveToIDUploadScreen() -> Void{
        let userProfile = UserProfile.getCurrentUser()!
        let sb = UIStoryboard(name: "IDCard", bundle: nil)
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        let topVc =  AppDelegate.getInstance().topMostController()
        
        if rootViewController?.presentedViewController != nil {
            rootViewController?.dismiss(animated: false)
        }
        
        if (userProfile.userPreferences.userMode == "DRIVER") {
            let vc: UIViewController = sb.instantiateViewController(withIdentifier: "UserIDDrivingLicenceController")
            rootViewController?.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc: UIViewController = sb.instantiateViewController(withIdentifier: "UserIDCardController")
            rootViewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
