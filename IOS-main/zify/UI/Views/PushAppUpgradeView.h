//
//  PushAppUpgradeView.h
//  zify
//
//  Created by Anurag S Rathor on 08/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PushNotificationView.h"

@interface PushAppUpgradeView : PushNotificationView
+(PushAppUpgradeView *)createNotificationView;
@property(nonatomic,weak) IBOutlet UILabel *title;
@property(nonatomic,weak) IBOutlet UILabel *upgradeText;
@property (nonatomic,weak) IBOutlet UIView *actionsView;
@property (nonatomic,weak) IBOutlet UIButton *btnCancel;
@property(nonatomic,weak) IBOutlet UIImageView *upgradeImage;
@property(nonatomic,weak) IBOutlet UIView *defaultImageView;
@end
