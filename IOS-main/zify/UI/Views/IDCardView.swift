//
//  IDCardView.swift
//  zify
//
//  Created by Anurag Rathor on 12/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class IDCardView: UIView {
    
    @IBOutlet var idCardBgView:UIView!
    
    var beforeAnimateConstraint: NSLayoutConstraint?
    var currentViewAnimateConstraint: NSLayoutConstraint?
    var afterAnimateConstraint: NSLayoutConstraint?
    
    
   /* override func willMove(toSuperview newSuperview: UIView?) {
        if newSuperview == nil {
            return
        }
        
        beforeAnimateConstraint = NSLayoutConstraint(item: idCardBgView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        currentViewAnimateConstraint = NSLayoutConstraint(item: idCardBgView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        afterAnimateConstraint = NSLayoutConstraint(item: idCardBgView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        addConstraint(self.beforeAnimateConstraint)
        layoutIfNeeded()
        super.willMove(toSuperview: newSuperview)
    }
    
   override func didMoveToSuperview() {
        idCardBgView.transform = CGAffineTransform(rotationAngle: -M_1_PI / 1.5)
        removeConstraint(beforeAnimateConstraint)
        addConstraint(currentViewAnimateConstraint)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
            self.idCardBgView.transform = CGAffineTransform(rotationAngle: 0)
            self.layoutIfNeeded()
        }) { finished in
        }
    }
    
    func removeFromSuperview1() {
        defer {
        }
        do {
            removeConstraint(currentViewAnimateConstraint)
            addConstraint(afterAnimateConstraint)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                notificationView.transform = CGAffineTransform(rotationAngle: M_1_PI / 1.5)
                self.layoutIfNeeded()
            }) { finished in
                if self.superview {
                    super.removeFromSuperview()
                }
            }
        } catch let ex {
            print("[Excep is \(ex.description())")
        }
    }*/
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
