//
//  ChatNotificationView.m
//  zify
//
//  Created by Anurag S Rathor on 15/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "ChatNotificationView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
#import "zify-Swift.h"

@implementation ChatNotificationView{
    NSLayoutConstraint *beforeVerticalConstraint;
    NSLayoutConstraint *afterVerticalConstraint;
}

+(ChatNotificationView *)getChatNotificationViewWithUserName:(NSString *)userName andMessage:(NSString *)message andImage:(NSString *)imageURL{
    NSArray *viewsArray = [[NSBundle mainBundle] loadNibNamed:@"ChatNotification" owner:self options:nil];
    ChatNotificationView *chatNotificationView = (ChatNotificationView *)[viewsArray objectAtIndex:0];
    chatNotificationView.userName.text = userName;
    chatNotificationView.message.text = message;
    UIImage *profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image.png"];
    chatNotificationView.userImage.hidden = YES;
    [chatNotificationView.userImage sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        chatNotificationView.userImage.hidden = NO;
    }];
    chatNotificationView.userImage.layer.cornerRadius = chatNotificationView.userImage.frame.size.width / 2;
   chatNotificationView.userImage.clipsToBounds = YES;
    
    
    
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGFloat width = 25;

    UIImageView *imgCancel = [[UIImageView alloc] init];
    imgCancel.frame = CGRectMake(bounds.size.width - width - 10, (chatNotificationView.frame.size.height - width)/2, width, width);
    imgCancel.image = [UIImage imageNamed:@"cancel_cross.png"];
    [chatNotificationView addSubview:imgCancel];
    
    return chatNotificationView;
}
-(void)handleSingleTap:(id)sender{
    NSLog(@"touch is happened");
}

-(void)showWithInterval:(NSTimeInterval)interval{
     UIWindow *window = [[UIApplication sharedApplication] keyWindow];
   // self.backgroundColor = [UIColor greenColor];
    for(UIView *view in window.subviews){
        if([view isKindOfClass:[ChatNotificationView class]]){
            [view removeFromSuperview];
        }
    }
    [window addSubview:self];
    [window bringSubviewToFront:self];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *viewsDictionary = @{@"displayView":self};
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[displayView]|" options:0 metrics:nil views:viewsDictionary];
    [window addConstraints:horizontalConsts];
    beforeVerticalConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:window attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    [window addConstraint:beforeVerticalConstraint];
    [window layoutIfNeeded];
    [window removeConstraint:beforeVerticalConstraint];
    afterVerticalConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:window attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    [window addConstraint:afterVerticalConstraint];
    
    //let widthConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 100)
    

    [window addConstraint:afterVerticalConstraint];

    __weak UIWindow *weakWindow = window;
    __weak NSLayoutConstraint *weakAfterVericalConstant = afterVerticalConstraint, *weakBeforeVerticalConstraint = beforeVerticalConstraint;
    [UIView animateWithDuration:0.3f delay:interval options:UIViewAnimationOptionCurveEaseIn animations:^{
        [weakWindow layoutIfNeeded];
    } completion:^(BOOL finished) {
      /*  [weakWindow removeConstraint:weakAfterVericalConstant];
         [weakWindow addConstraint:weakBeforeVerticalConstraint];
        [UIView animateWithDuration:0.3f delay:2.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [weakWindow layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];*/
    }];
    window.backgroundColor = [UIColor redColor];
    
    
 

}
-(void) test{
    NSArray *array = [[NSMutableArray alloc] init];
    for(int i=0;i<array.count;i++){
        
    }
}

@end
