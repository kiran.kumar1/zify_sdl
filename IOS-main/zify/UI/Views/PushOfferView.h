//
//  PushNotificationOfferView.h
//  zify
//
//  Created by Anurag S Rathor on 25/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PushNotificationView.h"


@interface PushOfferView : PushNotificationView
@property (nonatomic,weak) IBOutlet UIImageView *notificationImage;
@property (nonatomic,weak) IBOutlet UILabel *notificationTitle;
@property (nonatomic,weak) IBOutlet UILabel *notificationBody;
@property (nonatomic,weak) IBOutlet UIView *notificationActions;
+(PushOfferView *)createNotificationView;
@end
