//
//  PushNotificationRideRequestView.h
//  zify
//
//  Created by Anurag S Rathor on 07/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PushNotificationView.h"

@interface PushRideRequestView : PushNotificationView
+(PushRideRequestView *)createNotificationView;
@property (nonatomic,weak) IBOutlet UIView *superView;
@property(nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UIImageView *userImage;
@property (nonatomic,weak) IBOutlet UILabel *userName;
@property (nonatomic,weak) IBOutlet UIView *userInfoView;
@property(nonatomic,weak) IBOutlet UILabel *source;
@property(nonatomic,weak) IBOutlet UILabel *destination;
@property (nonatomic,weak) IBOutlet UILabel *departureDate;
@property (nonatomic,weak) IBOutlet UILabel *departureTime;
-(void) acceptRideRequest;
-(void) declineRideRequest;
-(IBAction) declineRideRequest:(id)sender;
-(IBAction) acceptRideRequest:(id)sender;
@end
