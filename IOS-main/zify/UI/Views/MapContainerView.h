//
//  MapContainerView.h
//  zify
//
//  Created by Anurag S Rathor on 21/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapView.h"

@interface MapContainerView : UIView
@property(nonatomic,strong) MapView *mapView;
-(void)createMapView;
@end
