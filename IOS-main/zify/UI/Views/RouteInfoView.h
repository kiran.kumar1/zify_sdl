//
//  RouteInfoView.h
//  zify
//
//  Created by Anurag S Rathor on 05/02/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripRouteDetail.h"
#import "SearchRideEntity.h"
#import "MapContainerView.h"
#import "TripRide.h"
#import "UserSearchData.h"

@interface RouteInfoView : UIView
@property (nonatomic,weak) IBOutlet MapContainerView *routeMapContainer;
@property(nonatomic,weak) IBOutlet UILabel *routeSource;
@property(nonatomic,weak) IBOutlet UILabel *routeDestination;
+(void)showRouteInfoOnView:(UIView *)view WithTripRouteDetail:(TripRouteDetail *)routeDetail;
+(void)showRouteInfoOnView:(UIView *)view WithSearchRide:(SearchRideEntity *)searchRide;
+(void)showRouteInfoOnViewFromUpcomingRides:(UIView *)view WithTripDetail:(TripRide *)trip;
+(void)showMapInfoOnView:(MapView *)mapView WithSearchRide:(SearchRideEntity *)searchRide withCollectionViewHeight:(CGFloat)height withUserSearchData:(UserSearchData *)searchData withIsforFirstTime:(BOOL)isForFirstTime;
+(void)getMarkersOnMap:(MapView *)mapView  WithSearchRide:(SearchRideEntity *)searchRide;

//+(void)drawPolilinesBetweenorigin:(CLLocationCoordinate2D)origin withDestination:(CLLocationCoordinate2D)destination withmapViw:(MapView *)mapView;

@end
