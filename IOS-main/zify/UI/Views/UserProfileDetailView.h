//
//  UserProfileDetailView.h
//  zify
//
//  Created by Anurag S Rathor on 25/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RatingView.h"
#import "DriverDetailEntity.h"
#import "RiderProfile.h"

@interface UserProfileDetailView : UIView
@property (nonatomic,weak) IBOutlet UIImageView *userImage;
@property (nonatomic,weak) IBOutlet UIImageView *userVerifiedImage;
@property (nonatomic,weak) IBOutlet UILabel *userName;
@property(nonatomic,weak) IBOutlet RatingView *maleRatingView;
@property(nonatomic,weak) IBOutlet RatingView *femaleRatingView;
@property(nonatomic,weak) IBOutlet UILabel *companyName;
@property(nonatomic,weak) IBOutlet UILabel *sharedDistance;
@property(nonatomic,weak) IBOutlet UILabel *sharedDistanceUnit;
+(void)showImageOnView:(UIView *)view WithDriverDetail:(DriverDetailEntity *)driverDetail;
+(void)showUserProfileOnView:(UIView *)view WithDriverDetail:(DriverDetailEntity *)driverDetail;
+(void)showUserProfileOnView:(UIView *)view WithRiderProfile:(RiderProfile *)riderProfile;
@end
