//
//  PushNotificationView.m
//  zify
//
//  Created by Anurag S Rathor on 25/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushNotificationView.h"
#import "PushOfferView.h"
#import "PushRideRequestView.h"
#import "PushTripNotificationView.h"
#import "PushAppUpgradeView.h"
#import <RestKit/RestKit.h>
#import "zify-Swift.h"
#import "PushOfferInfo.h"
#import "PushRideRequest.h"
#import "PushTripNotification.h"
#import "PushAppUpgradeInfo.h"
#import "PushTripCompleteNotification.h"
#import "PushRideInvoiceController.h"
#import "AppDelegate.h"
#import "RideRequestsController.h"
#import "AppUtilites.h"
#import "ChatManager.h"
#import "CurrentDriveController.h"
#import "SearchRideViewController.h"
#import "zify-Swift.h"

#define PAYLOAD_KEY @"payload"
#define NOTIFICATION_TYPE_KEY @"notification_type"

#define TYPE_OFFER 0
#define TYPE_RIDE_REQUEST 1
#define TYPE_TRIP_NOTIFICATION 2
#define TYPE_APP_UPGRADE 3
#define TYPE_TRIP_COMPLETE_NOTIFICATION 4
#define TYPE_CHAT_NOTIFICATION 15


@implementation PushNotificationView{
    NSLayoutConstraint *beforeAnimateConstraint,*currentViewAnimateConstraint,*afterAnimateConstraint;
}

+(void)showPushNotificationView:(NSDictionary *)notificationDict andIsApplaunch:(BOOL)isAppLaunch{
    
    
    if(!notificationDict[PAYLOAD_KEY]) return;
    int notificationType = [notificationDict[PAYLOAD_KEY][NOTIFICATION_TYPE_KEY] intValue];
    int category = [notificationDict[PAYLOAD_KEY][@"category"] intValue];

    PushNotificationView *notificationView;
    
    for (UIView *subview in [AppDelegate getAppDelegateInstance].window.subviews){ // for removing exised push notification view if any
        if([subview isKindOfClass:[PushNotificationView class]] || [subview isKindOfClass:[PushOfferView class]] || [subview isKindOfClass:[PushAppUpgradeView class]]){
            if(subview)
               [subview removeFromSuperview];
        }
    }
    UserProfile *currentUser = [UserProfile getCurrentUser];
    if(currentUser == nil){
        return;
    }
    if((currentUser.mobileVerified && currentUser.profileImgUrl) || notificationType == 3){
    switch (notificationType){
        case TYPE_RIDE_REQUEST:{
            [[SDLConnection sharedManager] showAlertForTheRideRequestWithTripInfo:notificationDict];
            UIViewController *topVc = [[AppDelegate getAppDelegateInstance] topMostController];
            if([topVc isKindOfClass:[RideRequestsController class]]){
                RideRequestsController *obcVc = (RideRequestsController *)topVc;
                obcVc.isViewInitialised = false;
                [obcVc callServiceAndLoadData];
                return;
            }
            notificationView = [PushRideRequestView createNotificationView];
            AppDelegate.getAppDelegateInstance.notificationViewFromAppdelegate = (PushRideRequestView *)notificationView;

            notificationView.notificationInfo = [PushNotificationView getPushNotificationInfoFromDict:notificationDict andMapping:[PushRideRequest getPushRideRequestObjectMapping]];
            
            break;
        }
        case TYPE_TRIP_NOTIFICATION:{
            UIViewController *topVc = [[AppDelegate getAppDelegateInstance] topMostController];
            if(category == 7){  /// drive started notification for rider
                if([topVc isKindOfClass:[CurrentRideController class]]){
                    NSLog(@"top view controller is itself need to update the uI");
                    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
                    if(rootViewController.presentedViewController != nil){
                        [rootViewController dismissViewControllerAnimated:NO completion:nil];
                    }
                    SearchRideViewController *obj = (SearchRideViewController *)[AppDelegate getAppDelegateInstance].topNavController;
                    [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = obj;
                    [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
                }
            }else if([topVc isKindOfClass:[TripsRidesController class]]){
                NSLog(@"top view controller is itself need to update the uI");
                TripsRidesController *obcVc = (TripsRidesController *)topVc;
                obcVc.isViewInitialised = false;
                [obcVc fetchTheServiceDetailsAndLoad];
                return;
            }
            notificationView = [PushTripNotificationView createNotificationView];
             notificationView.notificationInfo = [PushNotificationView getPushNotificationInfoFromDict:notificationDict andMapping:[PushTripNotification getPushTripNotificationObjectMapping]];
            break;
        }
        case TYPE_APP_UPGRADE:{
            notificationView = [PushAppUpgradeView createNotificationView];
             notificationView.notificationInfo = [PushNotificationView getPushNotificationInfoFromDict:notificationDict andMapping:[PushAppUpgradeInfo getPushAppUpgradeInfoObjectMapping]];
            break;
        }
        case TYPE_TRIP_COMPLETE_NOTIFICATION:{
            PushNotificationInfo *notificationInfo = [PushNotificationView getPushNotificationInfoFromDict:notificationDict andMapping:[PushTripCompleteNotification getPushTripCompleteNotificationObjectMapping]];
            //if([notificationInfo showNotification:isAppLaunch])
            UIApplication *application = [UIApplication sharedApplication];
            if (application.applicationState == UIApplicationStateActive)
            {
                UIViewController *visibleController = [[AppDelegate getAppDelegateInstance] topMostController];
                UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
                if(rootViewController.presentedViewController != nil){
                     [rootViewController dismissViewControllerAnimated:NO completion:nil];
                     visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
                }
                NSLog(@"array data is %@", visibleController.navigationController.viewControllers);

                if([visibleController isKindOfClass:[MainContentController class]]){
                    
                    SearchRideViewController *objVc = (SearchRideViewController *)[AppDelegate getAppDelegateInstance].topNavController;

                    [objVc showUpcomingRideWithLat:nil andLong:nil];
                }
                
            //    [PushRideInvoiceController showRideInvoiceControllerWithTripNotification:(PushTripCompleteNotification *)notificationInfo];

              /*  UIViewController *topVc = [[AppDelegate getAppDelegateInstance] topMostController];
                NSLog(@"topVc is hehhe %@", topVc);
                if(![topVc isKindOfClass:[RideInvoiceController class]]){
                    
                  [PushRideInvoiceController showRideInvoiceControllerWithTripNotification:(PushTripCompleteNotification *)notificationInfo];
                }else{
                    RideInvoiceController *obcVc = (RideInvoiceController *)topVc;
                    [obcVc callServiceAndLoadData];
                    return;
                }*/
            }
            return;
        }
        case TYPE_OFFER:{
            notificationView = [PushOfferView createNotificationView];
            notificationView.notificationInfo = [PushNotificationView getPushNotificationInfoFromDict:notificationDict andMapping:[PushOfferInfo getPushOfferInfoObjectMapping]];
            break;
        }
        default:{
            notificationView = [PushOfferView createNotificationView];
            notificationView.notificationInfo = [PushNotificationView getPushNotificationInfoFromDict:notificationDict andMapping:[PushOfferInfo getPushOfferInfoObjectMapping]];
            break;
        }
    }
    
    [notificationView populateNotificationDetails];
   // if(notificationView.title)
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:notificationView];
    
    NSDictionary *viewsDictionary = @{@"notificationView":notificationView};
    notificationView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[notificationView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[notificationView]|" options:0 metrics:nil views:viewsDictionary];
    [window addConstraints:horizontalConsts];
    [window addConstraints:verticalConsts];
    [PushNotificationView clearNotifications];
    }
}
+ (void) clearNotifications {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}


+ (PushNotificationInfo *)getPushNotificationInfoFromDict:(NSDictionary *)notificationDict andMapping:(RKObjectMapping *)mapping{
    NSDictionary *mappingDictionary= @{PAYLOAD_KEY:mapping};
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:notificationDict options:0 error:&error];
    id parsedData = [RKMIMETypeSerialization objectFromData:jsonData MIMEType:@"application/json" error:&error];
    RKMapperOperation *mappingOperation=[[RKMapperOperation alloc]initWithRepresentation:parsedData mappingsDictionary:mappingDictionary];
    [mappingOperation execute:&error];
    if(error) return nil;
    if(mappingOperation.mappingResult.array.count == 0)return nil;
    PushNotificationInfo *notificationInfo = (PushNotificationInfo *)[mappingOperation.mappingResult.array objectAtIndex:0];
    return notificationInfo;
}

+(void)handlePushNotificationAction:(NSString *)actionIdentifier andNotificationDict:(NSDictionary *)notificationDict{
    int notificationType = [notificationDict[PAYLOAD_KEY][NOTIFICATION_TYPE_KEY] intValue];
    
    UserProfile *currentUser = [UserProfile getCurrentUser];
    if(currentUser == nil){
        return;
    }
    if([actionIdentifier isEqualToString:@"CHAT"]){
        [[ChatManager sharedInstance] moveToChatScreen:nil];
        return;
    }
    if(notificationType == TYPE_RIDE_REQUEST){
        PushRideRequestView *rideRequestView = [PushRideRequestView createNotificationView];
        rideRequestView.notificationInfo = [PushNotificationView getPushNotificationInfoFromDict:notificationDict andMapping:[PushRideRequest getPushRideRequestObjectMapping]];
        if([@"RIDE_REQUEST_ACCEPT" isEqualToString:actionIdentifier]){
            [rideRequestView acceptRideRequest];
        } else if([@"RIDE_REQUEST_DECLINE" isEqualToString:actionIdentifier]){
            [rideRequestView declineRideRequest];
        }
    }
}
    
+(void)moveToChatScreen{
    UIViewController *visibleController = [AppUtilites applicationVisibleViewController];
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UINavigationController *chatNavController = [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    if(rootViewController.presentedViewController != nil){
        [rootViewController dismissViewControllerAnimated:NO completion:nil];
        visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [visibleController presentViewController:chatUserListContoller animated:YES completion:nil];
    }else{
        [visibleController presentViewController:chatUserListContoller animated:YES completion:nil];
    }
    
    
}
-(void) populateNotificationDetails{
    
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if (newSuperview == nil) {
        return;
    }
    beforeAnimateConstraint = [NSLayoutConstraint constraintWithItem:_notificationView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
    currentViewAnimateConstraint = [NSLayoutConstraint constraintWithItem:_notificationView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f];
     afterAnimateConstraint = [NSLayoutConstraint constraintWithItem:_notificationView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f];
    [self addConstraint:beforeAnimateConstraint];
    [self layoutIfNeeded];
    [super willMoveToSuperview:newSuperview];
}

-(void)didMoveToSuperview{
    
    @try {
        _notificationView.transform = CGAffineTransformMakeRotation(-M_1_PI / 1.5);
        [self removeConstraint:beforeAnimateConstraint];
        [self addConstraint:currentViewAnimateConstraint];
        [UIView animateWithDuration:0.3f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            _notificationView.transform = CGAffineTransformMakeRotation(0);
            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
   
}

- (void)removeFromSuperview1
{
    @try{
        if(self){
    [self removeConstraint:currentViewAnimateConstraint];
    [self addConstraint:afterAnimateConstraint];
    [UIView animateWithDuration:0.3f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _notificationView.transform = CGAffineTransformMakeRotation(M_1_PI / 1.5);
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        if(self.superview)
             [super removeFromSuperview];
    }];
        }
    }@catch(NSException *ex){
        NSLog(@"[Excep is %@", [ex description]);
    }
}

-(CAGradientLayer*)failureGradient{
    UIColor *colorOne = [UIColor colorWithRed:(255.0/255.0) green:(98.0/255.0) blue:(74.0/255.0) alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:(255.0/255.0) green:(98.0/255.0) blue:(74.0/255.0) alpha:1.0];
    UIColor *colorThree = [UIColor colorWithRed:(122.0/255.0) green:(47.0/255.0) blue:(35.0/255.0) alpha:1.0];
    
    NSArray *colors =  [NSArray arrayWithObjects:(id)colorThree.CGColor, colorTwo.CGColor, colorOne.CGColor, nil];
    
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:0.5];
    NSNumber *stopThree = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, stopThree, nil];
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    headerLayer.cornerRadius = 3.0;
    return headerLayer;
}

-(CAGradientLayer*)successGradient{
    UIColor *colorOne = [UIColor colorWithRed:(98.0/255.0) green:(196.0/255.0) blue:(183.0/255.0) alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:0 green:(128.0/255.0) blue:(128.0/255.0) alpha:1.0];
    
    NSArray *colors =  [NSArray arrayWithObjects:(id)colorTwo.CGColor, colorOne.CGColor, nil];
    
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    headerLayer.cornerRadius = 3.0;
    return headerLayer;
}


@end
