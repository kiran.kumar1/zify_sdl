//
//  PushTripNotificationView.m
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushTripNotificationView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PushTripNotification.h"
#import "AppDelegate.h"
#import "CurrentDriveController.h"
#import "SearchRideViewController.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"

#define CATEGORY_RIDE_ACCEPTED 4
#define CATEGORY_RIDE_DECLINED 5
#define CATEGORY_RIDE_CANCELLED 6
#define CATEGORY_DRIVE_CANCELLED 7
#define CATEGORY_DRIVE_STARTED 8

@implementation PushTripNotificationView{
    NSDateFormatter *dateTimeFormatter1;
    NSDateFormatter *dateFormatter1;
   // NSDateFormatter *timeFormatter1;
}

+(PushTripNotificationView *)createNotificationView{
    NSArray *viewsArray = [[NSBundle mainBundle] loadNibNamed:@"PushNotificationViews" owner:self options:nil];
    PushTripNotificationView *notificationView;
    for (id object in viewsArray) {
        if ([object isKindOfClass:[self class]]) {
            notificationView = (PushTripNotificationView *)object;
            break;
        }
    }
    return notificationView;
}

-(void) populateNotificationDetails{
    dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
//    timeFormatter1 = [[NSDateFormatter alloc] init];
//    [timeFormatter1 setDateFormat:@"HH:mm"];
//    [timeFormatter1 setLocale:locale];

    PushTripNotification *tripNotification = (PushTripNotification *)self.notificationInfo;
    int category = tripNotification.category.intValue;
    if(![self checkIfTopScreenIsActiveScreen]){
    CAGradientLayer *titleViewLayer;
    if(category == CATEGORY_RIDE_ACCEPTED || category == CATEGORY_DRIVE_STARTED){
       titleViewLayer = [self successGradient];
    } else{
        titleViewLayer = [self failureGradient];
        _notificationImage.transform = CGAffineTransformMakeRotation(M_PI);
    }
    _titleView.layer.masksToBounds = YES;
    titleViewLayer.frame = _titleView.bounds;
    titleViewLayer.frame = CGRectMake(titleViewLayer.frame.origin.x, titleViewLayer.frame.origin.y, [AppDelegate SCREEN_WIDTH] - 2* titleViewLayer.frame.origin.x , titleViewLayer.frame.size.height);
    [_titleView.layer insertSublayer:titleViewLayer atIndex:0];
    //_title.text = tripNotification.title;
    _title.text =  NSLocalizedString(tripNotification.title, "");
    _userName.text = [NSString stringWithFormat:@"%@ %@",tripNotification.userDetails.firstName,tripNotification.userDetails.lastName];
    NSDate *departureDateTime = [dateTimeFormatter1 dateFromString:tripNotification.tripDetails.tripTime];
    _departureTime.text = [NSString stringWithFormat:@"%@, %@",[dateFormatter1 stringFromDate:departureDateTime],[[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureDateTime]];
    }
}

-(IBAction) dismiss:(id)sender{
    [self removeFromSuperview];
}

-(BOOL)checkIfTopScreenIsActiveScreen{
    UIViewController *topVc = [[AppDelegate getAppDelegateInstance] topMostController];
    if([topVc isKindOfClass:[CurrentDriveController class]] || [topVc isKindOfClass:[CurrentRideController class]]){
        SearchRideViewController *obj = (SearchRideViewController *)[AppDelegate getAppDelegateInstance].topNavController;
        [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = obj;
        [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
        return NO;
    }
    return NO;
    
}

@end
