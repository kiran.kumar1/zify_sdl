//
//  PushNotificationRideRequestView.m
//  zify
//
//  Created by Anurag S Rathor on 07/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushRideRequestView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PushRideRequest.h"
#import "ServerInterface.h"
#import "RideDriveActionRequest.h"
#import "UniversalAlert.h"
#import "AppUtilites.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "CurrentDriveController.h"
#import "SearchRideViewController.h"
#import "zify-Swift.h"

@implementation PushRideRequestView{
    NSDateFormatter *dateTimeFormatter1;
    NSDateFormatter *dateFormatter1;
 //   NSDateFormatter *timeFormatter1;
    CAGradientLayer *userInfoViewLayer;
}
+(PushRideRequestView *)createNotificationView{
    NSArray *viewsArray = [[NSBundle mainBundle] loadNibNamed:@"PushNotificationViews" owner:self options:nil];
    PushRideRequestView *rideRequestView;
    for (id object in viewsArray) {
        if ([object isKindOfClass:[self class]]) {
            rideRequestView = (PushRideRequestView *)object;
            break;
        }
    }
    return rideRequestView;
}

-(void)populateNotificationDetails{
    dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yy"];
//    timeFormatter1 = [[NSDateFormatter alloc] init];
//    [timeFormatter1 setDateFormat:@"HH:mm"];
//    [timeFormatter1 setLocale:locale];
    _userInfoView.frameWidth = _superView.frameWidth;
    PushRideRequest *rideRequest = (PushRideRequest *)self.notificationInfo;
    _title.text = NSLocalizedString(rideRequest.title, ""); 
    UIImage *profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image.png"];
    _userImage.hidden = YES;
    [_userImage sd_setImageWithURL:[NSURL URLWithString:rideRequest.userDetails.profileImageURL] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        _userImage.hidden = NO;
    }];
    _userImage.layer.cornerRadius = _userImage.frame.size.width / 2;
    _userImage.clipsToBounds = YES;
    _userImage.layer.borderColor = [UIColor whiteColor].CGColor;
    _userImage.layer.borderWidth = 4.0f;
    _userName.text = [NSString stringWithFormat:@"%@ %@",rideRequest.userDetails.firstName,rideRequest.userDetails.lastName];
    _source.text = rideRequest.tripDetails.srcAddress;
    _destination.text = rideRequest.tripDetails.destAddress;
    NSDate *departureDateTime = [dateTimeFormatter1 dateFromString:rideRequest.tripDetails.tripTime];
    _departureDate.text = [dateFormatter1 stringFromDate:departureDateTime];
    _departureTime.text = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureDateTime];
    userInfoViewLayer = [self successGradient];
    _userInfoView.layer.masksToBounds = YES;
    userInfoViewLayer.frame = _userInfoView.bounds;
    userInfoViewLayer.frame = CGRectMake(userInfoViewLayer.frame.origin.x, userInfoViewLayer.frame.origin.y, [AppDelegate SCREEN_WIDTH] - 2*userInfoViewLayer.frame.origin.x, userInfoViewLayer.frame.size.height);
    [_userInfoView.layer insertSublayer:userInfoViewLayer atIndex:0];
}


-(IBAction) acceptRideRequest:(id)sender{
  
    [self closeAlertInSDLIExists];
    [self callApiForAcceptRideRequest];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
    });
}

-(IBAction) declineRideRequest:(id)sender{
    [self closeAlertInSDLIExists];
    [self callAPIForDeclineRideRequest];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
    });
}
-(void)closeAlertInSDLIExists{
    if(AppDelegate.getAppDelegateInstance.alertObj){
        AppDelegate.getAppDelegateInstance.alertObj.duration = @(0);
        [[ProxyManager sharedManager].sdlManager sendRequest:AppDelegate.getAppDelegateInstance.alertObj withResponseHandler:^(SDLRPCRequest *request, SDLRPCResponse *response, NSError *error) {
            NSLog(@"response is %@", response);
            if (![response.resultCode isEqualToEnum:SDLResultSuccess]) { return; }
            
        }];
    }
}

-(void) callAPIForDeclineRideRequest{
    PushRideRequest *rideRequest = (PushRideRequest *)self.notificationInfo;
    [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:DECLINERIDE andDriveId:rideRequest.tripDetails.driveId andRideId:rideRequest.tripDetails.rideId] withHandler:^(ServerResponse *response, NSError *error){
        
        if(response){
            UIViewController *topVc = [[AppDelegate getAppDelegateInstance] topMostController];
            if([topVc isKindOfClass:[CurrentDriveController class]]){
                NSLog(@"top view controller is itself need to update the uI");
                SearchRideViewController *searchRideObj;
                NSLog(@"vcs array is %@", [AppDelegate getAppDelegateInstance].window.rootViewController.navigationController.viewControllers);
                for (UIViewController *vc in [AppDelegate getAppDelegateInstance].window.rootViewController.navigationController.viewControllers){
                    if ([vc isKindOfClass:[SearchRideViewController class]]){
                        searchRideObj = (SearchRideViewController *)vc;
                    }
                }
                if(searchRideObj){
                    [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = searchRideObj;
                    [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
                }
            }else{
            
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_PUSH_RIDEREQUEST_RIDEREQUEST, nil) WithMessage:NSLocalizedString(VC_PUSH_RIDEREQUEST_REQUESTDECLINED,nil)];
        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
           
        }];
        [alert showInViewController:[AppUtilites applicationVisibleViewController]];
            }
        }
        
    }];
}

-(void) acceptRideRequest{
 /*   AppDelegate.getAppDelegateInstance.notificationInfoFromPush = (PushRideRequest *)self.notificationInfo;
    UserProfile *userProfile = [UserProfile getCurrentUser];
    UserDocuments *userDocuments = userProfile.userDocuments;
    if(![userDocuments.isIdCardDocUploaded intValue]){
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setBool:YES forKey:@"IdFromPush"];
        [prefs synchronize];
        [[AppDelegate getAppDelegateInstance] showViewForIdCardUpload];
        return;
    }*/
    [self callApiForAcceptRideRequest];
}
-(void)callApiForAcceptRideRequest{
    PushRideRequest *rideRequest = (PushRideRequest *)self.notificationInfo;
    [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:CONFIRMRIDE andRideId:rideRequest.tripDetails.rideId andDriveId:rideRequest.tripDetails.driveId andSeatsNum:@1] withHandler:^(ServerResponse *response, NSError *error){
        if(response){
            UIViewController *topVc = [[AppDelegate getAppDelegateInstance] topMostController];
            if([topVc isKindOfClass:[CurrentDriveController class]]){
                SearchRideViewController *obj = (SearchRideViewController *)[AppDelegate getAppDelegateInstance].topNavController;
                [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = obj;
                [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
                
            }else{
                UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_PUSH_RIDEREQUEST_RIDEREQUEST, nil) WithMessage:NSLocalizedString(VC_PUSH_RIDEREQUEST_REQUESTACCEPTED,nil)];
                [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                    
                }];
                [alert showInViewController:[AppUtilites applicationVisibleViewController]];
            }
        }
    }];
}


@end
