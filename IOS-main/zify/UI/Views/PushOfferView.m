//
//  PushNotificationOfferView.m
//  zify
//
//  Created by Anurag S Rathor on 25/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushOfferView.h"
#import "PushOfferInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PushOfferView
+(PushOfferView *)createNotificationView{
    NSArray *viewsArray = [[NSBundle mainBundle] loadNibNamed:@"PushNotificationViews" owner:self options:nil];
    PushOfferView *offerView;
    for (id object in viewsArray) {
        if ([object isKindOfClass:[self class]]) {
            offerView = (PushOfferView *)object;
            break;
        }
    }
    return offerView;
}

-(IBAction) dismissOfferNotificationView:(id)sender{
    [self removeFromSuperview];
}

-(void)populateNotificationDetails{
    PushOfferInfo *offerInfo = (PushOfferInfo *)self.notificationInfo;
    _notificationTitle.text = offerInfo.title;
    _notificationBody.text = offerInfo.message;
    _notificationImage.hidden = YES;
    if([offerInfo.hasImage intValue]){
        NSString* encodedURL = [offerInfo.imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [_notificationImage sd_setImageWithURL:[NSURL URLWithString:encodedURL] completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
          //  _notificationImage.image = [self imageWithImage:_notificationImage.image scaledToSize:_notificationImage.frame.size isAspectRation:NO];
            _notificationImage.hidden = NO;
            
        }];
    } else{
        _notificationImage.translatesAutoresizingMaskIntoConstraints = NO;
        [_notificationImage addConstraint:[NSLayoutConstraint constraintWithItem:_notificationImage attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:0.0f]];
    }
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize isAspectRation:(BOOL)aspect {
    if (!image) {
        return nil;
    }
    NSLog(@"resize is %f %f", newSize.width, newSize.height);
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    CGFloat originRatio = image.size.width / image.size.height;
    CGFloat newRatio = newSize.width / newSize.height;
    
    CGSize sz;
    
    if (!aspect) {
        sz = newSize;
    }else {
        if (originRatio < newRatio) {
            sz.height = newSize.height;
            sz.width = newSize.height * originRatio;
        }else {
            sz.width = newSize.width;
            sz.height = newSize.width / originRatio;
        }
    }
    CGFloat scale = 1.0;
    //    if([[UIScreen mainScreen]respondsToSelector:@selector(scale)]) {
    //        CGFloat tmp = [[UIScreen mainScreen]scale];
    //        if (tmp > 1.5) {
    //            scale = 2.0;
    //        }
    //    }
    sz.width /= scale;
    sz.height /= scale;
    UIGraphicsBeginImageContextWithOptions(sz, NO, scale);
    [image drawInRect:CGRectMake(0, 0, sz.width, sz.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
