//
//  PushTripNotificationView.h
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PushNotificationView.h"

@interface PushTripNotificationView : PushNotificationView
+(PushTripNotificationView *)createNotificationView;
@property(nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UIImageView *notificationImage;
@property(nonatomic,weak) IBOutlet UIView *titleView;
@property (nonatomic,weak) IBOutlet UILabel *departureTime;
@property (nonatomic,weak) IBOutlet UILabel *userName;
@end
