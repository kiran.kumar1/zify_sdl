//
//  ChatNotificationView.h
//  zify
//
//  Created by Anurag S Rathor on 15/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatNotificationView : UIView
@property(nonatomic,weak) IBOutlet UIImageView *userImage;
@property(nonatomic,weak) IBOutlet UILabel *userName;
@property(nonatomic,weak) IBOutlet UILabel *message;

+(ChatNotificationView *)getChatNotificationViewWithUserName:(NSString *)userName andMessage:(NSString *)message andImage:(NSString *)imageURL;
-(void)showWithInterval:(NSTimeInterval)interval;
@end
