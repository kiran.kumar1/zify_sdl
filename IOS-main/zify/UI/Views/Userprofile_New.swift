//
//  IDUploadFromTp.swift
//  zify
//
//  Created by Anurag Rathor on 12/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit
import Lottie

/*protocol moveToIDCardScreenFromTPProtocol {
    func skipBtnTapped() -> Void
    func setNowbtntapped() -> Void
}*/

class Userprofile_New: UIView {
    
    @IBOutlet var bgBlurView:UIView!
    @IBOutlet var infoView:UIView!
    @IBOutlet var tpSecureImageView:UIImageView!
    @IBOutlet var titleLbl:UILabel!
    @IBOutlet var titleDesclbl:UILabel!

    @IBOutlet var profileImageView:UIImageView!
    @IBOutlet var verifyImageView:UIImageView!

    @IBOutlet var nameLbl:UILabel!
    @IBOutlet var ratingLbl:UILabel!

    @IBOutlet var carmModelLbl:UILabel!
    @IBOutlet var carNumberLbl:UILabel!

    @IBOutlet var setBtn:UIButton!
    var profileImagePlaceHolder: UIImage?

   
    
   // var delegate: moveToIDCardScreenFromTPProtocol?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImagePlaceHolder = UIImage(named: "placeholder_profile_image")

       
        
    }
    
    func setDataForView(withSearchEntity:SearchRideEntity){
        self.titleLbl.text = NSLocalizedString(UPS_PROFILE_NAV_TITLE, comment: "")
        self.titleDesclbl.text = NSLocalizedString(check_vehicle_details_of_car_owner, comment: "")
        self.nameLbl.text = withSearchEntity.driverDetail.firstName
        self.carmModelLbl.text = withSearchEntity.driverDetail.vehicleModel
        self.carNumberLbl.text = withSearchEntity.driverDetail.vehicleRegistrationNum
        self.carNumberLbl.textColor = UIColor.red
        self.carNumberLbl.text = "XXXXXXXXXX"
        let maleRate: Double = Double(withSearchEntity.driverDetail.driverRatingMale)
        let femaleRate: Double = Double(withSearchEntity.driverDetail.driverRatingFemale)
        let numOfMaleRate: Double = Double(withSearchEntity.driverDetail.numOfMalesRated)
        let numOfFemaleRate: Double = Double(withSearchEntity.driverDetail.numOfFemalesRated)
        
        let val = ((maleRate * numOfMaleRate) + (femaleRate * numOfFemaleRate))/(numOfMaleRate + numOfFemaleRate)
        
        self.ratingLbl.text = String(format: "%.1f", val)
        
        if withSearchEntity.driverDetail?.driverStatus == "VERIFIED" {
            verifyImageView.isHidden = false
        } else {
            verifyImageView.isHidden = true
        }
        
        self.profileImageView.sd_setImage(with: URL(string: withSearchEntity.driverDetail.profileImgUrl ?? ""), placeholderImage: profileImagePlaceHolder, completed: { image, error, cacheType, imageURL in
        })
    }
    
  /*  func setAttributesAndFontForControls() -> Void {
        
        self.titleLbl.fontSize = 12
        self.descLbl.fontSize = 12
        self.setBtn.titleLabel?.fontSize = 13;
        if UIScreen.main.sizeType == .iPhone6 {
            self.titleLbl.fontSize = 13
            self.descLbl.fontSize = 13
            self.setBtn.titleLabel?.fontSize = 14;
        }else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS  || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            self.titleLbl.fontSize = 15
            self.descLbl.fontSize = 15
            self.setBtn.titleLabel?.fontSize = 16;
        }
        let idproofStr  = NSLocalizedString(id_proof_string, comment: "")
        self.descLbl.attributedText = self.attributedText(withString: self.descLbl.text ?? "", boldString: idproofStr, font: (self.descLbl.font)!)
        self.descLbl.textAlignment = .center
    }*/
    
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSFontAttributeName: font])
        let boldFontAttribute: [String: Any] = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
    
    @IBAction func btnSkipTapped(_ sender:UIButton){
        self.removeFromSuperview()
       // self.delegate?.skipBtnTapped()
        
    }
    @IBAction func btnSetNowTapped(_ sender:UIButton){
        self.removeFromSuperview()
       // self.delegate?.setNowbtntapped()
    }
    
  
    func showInfoView() -> Void {
    }
    
    func removeView() ->Void{
        self.removeFromSuperview()
    }
    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
