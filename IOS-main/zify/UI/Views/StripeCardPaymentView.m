//
//  StripeCardPaymentView.m
//  zify
//
//  Created by Anurag S Rathor on 03/10/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "StripeCardPaymentView.h"

@implementation StripeCardPaymentView

- (UIView *)inputAccessoryView {
    UIToolbar* toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlack;
    toolBar.tintColor = [UIColor colorWithRed:(239.0/255.0) green:(239.0/255.0) blue:(239.0/255.0) alpha:1.0];
    toolBar.items = [NSArray arrayWithObjects:
                     [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                     [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)],
                     nil];
    [toolBar sizeToFit];
    return toolBar;
}

-(void)dismiss{
    [self resignFirstResponder];
}
@end
