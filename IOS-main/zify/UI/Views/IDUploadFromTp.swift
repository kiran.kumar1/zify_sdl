//
//  IDUploadFromTp.swift
//  zify
//
//  Created by Anurag Rathor on 12/03/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit
import Lottie

protocol moveToIDCardScreenFromTPProtocol {
    func skipBtnTapped() -> Void
    func setNowbtntapped() -> Void
}

class IDUploadFromTp: UIView {
    
    @IBOutlet var bgBlurView:UIView!
    @IBOutlet var infoView:UIView!
    @IBOutlet var tpSecureImageView:UIImageView!
    @IBOutlet var titleLbl:UILabel!
    @IBOutlet var descLbl:UILabel!
    @IBOutlet var setBtn:UIButton!
    
    @IBOutlet var laterBtn:UIButton!
    @IBOutlet var animationBgView:UIView!
    @IBOutlet var animationView:LOTAnimationView!
    @IBOutlet var tpSavedLbl:UILabel!
    
    var delegate: moveToIDCardScreenFromTPProtocol?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        infoView.isHidden = true
        tpSecureImageView.isHidden = true
        animationBgView.isHidden = false
        tpSavedLbl.isHidden = true
        
        self.showLoadingSymbol()
    }
    
    func setAttributesAndFontForControls() -> Void {
        
        self.titleLbl.fontSize = 12
        self.descLbl.fontSize = 12
        self.setBtn.titleLabel?.fontSize = 13;
        if UIScreen.main.sizeType == .iPhone6 {
            self.titleLbl.fontSize = 13
            self.descLbl.fontSize = 13
            self.setBtn.titleLabel?.fontSize = 14;
        }else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS  || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            self.titleLbl.fontSize = 15
            self.descLbl.fontSize = 15
            self.setBtn.titleLabel?.fontSize = 16;
        }
        let idproofStr  = NSLocalizedString(id_proof_string, comment: "")
        self.descLbl.attributedText = self.attributedText(withString: self.descLbl.text ?? "", boldString: idproofStr, font: (self.descLbl.font)!)
        self.descLbl.textAlignment = .center
    }
    
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSFontAttributeName: font])
        let boldFontAttribute: [String: Any] = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
    
    @IBAction func btnSkipTapped(_ sender:UIButton){
        self.removeFromSuperview()
        self.delegate?.skipBtnTapped()
        
    }
    @IBAction func btnSetNowTapped(_ sender:UIButton){
        self.removeFromSuperview()
        self.delegate?.setNowbtntapped()
    }
    
    func showLoadingSymbol(){
        let lottieFileName = "loading"
        self.animationView.setAnimation(named: lottieFileName)
        self.animationView.loopAnimation = true
        self.animationView.play()
        // let timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(showSuccessView), userInfo: nil, repeats: false)
    }
    func showSuccessView(){
        let lottieFileName = "tick"
        self.animationView.setAnimation(named: lottieFileName)
        self.animationView.loopAnimation = false
        self.animationView.play {(true)  in
            self.tpSavedLbl.isHidden = false
            //let timer = Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(self.showInfoView), userInfo: nil, repeats: false)
            
        }
    }
    func showInfoView() -> Void {
        self.animationBgView.isHidden = true
        self.infoView.isHidden = false
        tpSecureImageView.isHidden = false
    }
    
    func removeView() ->Void{
        self.removeFromSuperview()
    }
    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
