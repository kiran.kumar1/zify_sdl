//
//  Message.swift
//  ChatApp-Swift-And-Firebase
//
//  Created by Surya on 9/30/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import FirebaseAuth

class Message: NSObject {
    var from: String?
    var message: String?
    var timeStamp: NSNumber?
    var to: String?
    var imageUrl: String?
    var imageWidth: NSNumber?
    var imageHeight: NSNumber?
    var videoUrl: String?
    var forUid_status: String?
    var snapShotKey: String?

    func chatPartnerId() -> String {
        return (from == Auth.auth().currentUser?.uid ? to : from)!
    }
 
}

