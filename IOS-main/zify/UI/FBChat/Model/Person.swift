//
//  User.swift
//  ChatApp-Swift-And-Firebase
//
//  Created by Surya on 9/28/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class Person: NSObject {
    var qbChatUserId: String?
    var name: String?
    var email: String?
    var profileImageUrl: String?
    var deviceToken: String?
    var isGlobal:Bool?
    var isGlobalPayment:Bool?
    var messageCode:NSNumber?
    var qbFirstName:String?
    var qbLastName:String?
    var qbUserEmail:String?
    var qbUserName:String?
    var qbUserPassword:String?
    var qbProfilePicUrl:String?
}

