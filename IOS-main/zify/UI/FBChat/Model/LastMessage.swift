//
//  User.swift
//  ChatApp-Swift-And-Firebase
//
//  Created by Surya on 9/28/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class LastMessage: NSObject {
    var id:String?
    var dateStamp: NSNumber?
    var lastMessage: String?
    var unreadCount: NSNumber?
    var profileImageurl:String?
    var userFirstname:String?
    var userLastname:String?

}
