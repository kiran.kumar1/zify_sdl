//
//  AboutSupportController.swift
//  zify
//
//  Created by Anurag Rathor on 06/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class AboutSupportController:ScrollableContentViewController, ImageSelectionViewerDelegate {

    let SCREENSHOT_PLACEHOLDER_DEFAULT_TAG = 1000
    
    @IBOutlet weak var issueTextView: UITextView!
    @IBOutlet var screenshotPlaceholders: [UIView]!
    @IBOutlet var screenshotImages: [UIImageView]!
    @IBOutlet weak var messageHandler: MessageHandler!
    var messageDelegate: MFMailComposeViewControllerDelegate?
    @IBOutlet weak var btnFreshChat: UIButton!
    @IBOutlet weak var lblPleaseDescYourPrblm: UILabel!
    @IBOutlet weak var btnReadOurFAQ: UIButton!
    @IBOutlet weak var lblAddScreenShots: UILabel!
    @IBOutlet weak var lblReadOurPrivacyPolicy: UILabel!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    
    
    var selectedImages: NSMutableArray = []
    var selectedImageIndex: Int = 0
    var currentUser: UserProfile?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblPleaseDescYourPrblm.fontSize = 11
        btnReadOurFAQ.titleLabel?.fontSize = 13
        lblAddScreenShots.fontSize = 12
        lblReadOurPrivacyPolicy.fontSize = 10
        btnPrivacyPolicy.titleLabel?.fontSize = 11
        btnFreshChat.titleLabel?.fontSize = 13
        if UIScreen.main.sizeType == .iPhone6 {
            lblPleaseDescYourPrblm.fontSize = 13
            btnReadOurFAQ.titleLabel?.fontSize = 14
            lblAddScreenShots.fontSize = 13
            lblReadOurPrivacyPolicy.fontSize = 11
            btnPrivacyPolicy.titleLabel?.fontSize = 12
            btnFreshChat.titleLabel?.fontSize = 15
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            lblPleaseDescYourPrblm.fontSize = 13
            btnReadOurFAQ.titleLabel?.fontSize = 15
            lblAddScreenShots.fontSize = 14
            lblReadOurPrivacyPolicy.fontSize = 12
            btnPrivacyPolicy.titleLabel?.fontSize = 13
            btnFreshChat.titleLabel?.fontSize = 15
        }
        addDropShadow(for: btnFreshChat)
        initialiseView()
        let readOurFAQStrLocalized = NSLocalizedString(USS_SUPPORT_BTN_READFAQ, comment: "")
        btnReadOurFAQ.titleLabel?.attributedText = NSAttributedString(string: readOurFAQStrLocalized, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        let privacyPolicyStrLocalized = NSLocalizedString(USS_SUPPORT_BTN_PRIVACYPOLICY, comment: "")
        btnPrivacyPolicy.titleLabel?.attributedText = NSAttributedString(string: privacyPolicyStrLocalized, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        // Do any additional setup after loading the view.
    }
    
    func initialiseView() {
        selectedImages = NSMutableArray.init()
        for counter in 0..<screenshotPlaceholders.count {
            selectedImages.add(NSNull())
        }
        //  [self.issueTextView becomeFirstResponder];
        currentUser = UserProfile.getCurrentUser()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        messageHandler.dismissErrorView()
        messageHandler.dismissSuccessView()
    }
    
    @IBAction func dismiss(_ sender: Any) {
        view.endEditing(true)
        let prefs = UserDefaults.standard
        let isDismiss: Bool = prefs.bool(forKey: "forDismissing")
        if isDismiss {
            prefs.set(false, forKey: "forDismissing")
            prefs.synchronize()
            dismiss(animated: true)
        } else {
            navigationController?.popViewController(animated: false)
        }
    }
    
    
    @IBAction func submitProblem(_ sender: Any) {
        if validateSupportMailInfo() {
            view.endEditing(true)
            var messageBody: String? = nil
            if let aKey = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") {
                messageBody = "\(issueTextView.text!)\n\n-----------------------\nEmail: \(currentUser?.emailId! ?? "")\nBuild Version: \(aKey) | iOS Version: \(UIDevice.current.systemVersion) | Device Model: \(UIDevice.current.model)"
            }
            let dictionary = ["subject": "Zify App Feedback [iOS]", "messageBody": messageBody, "recipients": "support@zify.co"]
            //support@zify.co
            var attachmentArray: [AnyHashable] = []
            for index in 0..<selectedImages.count {
                if !(selectedImages[index] is NSNull) {
                    attachmentArray.append(UIImagePNGRepresentation(selectedImages[index] as! UIImage as! UIImage)!)
                }
            }
            let prefs = UserDefaults.standard
            let isDismiss: Bool = prefs.bool(forKey: "forDismissing")
            if isDismiss {
                prefs.set(false, forKey: "forDismissing")
                prefs.synchronize()
                dismiss(animated: true)
            } else {
                navigationController?.popViewController(animated: false)
            }
            AppUtilites.shareEmail(withData: dictionary, with: messageDelegate, addAttachmentArray: attachmentArray, andFileType: "png", andMimeType: "image/png")
        }
    }
    
    @IBAction func showFAQ(_ sender: Any) {
        let genericWebNavController: UINavigationController? = GenericWebController.createGenericWebNavController()
        let webController = genericWebNavController?.topViewController as? GenericWebController
        webController?.webViewURL = "https://support.zify.co/portal/kb" //@"https://zify.zendesk.com/hc/en-us"
        webController?.title = NSLocalizedString(CMON_GENERIC_FAQ, comment: "")
        if let aController = genericWebNavController {
            present(aController, animated: true)
        }
    }
    
    @IBAction func selectImage(_ sender: UIButton) {
        selectedImageIndex = sender.tag - SCREENSHOT_PLACEHOLDER_DEFAULT_TAG
        ImageSelectionViewer.sharedInstance().imageSelectionDelegate = self
        ImageSelectionViewer.sharedInstance().openGallery()
    }
    
    @IBAction func showTerms(_ sender: Any) {
        let genericWebNavController: UINavigationController? = GenericWebController.createGenericWebNavController()
        let webController = genericWebNavController?.topViewController as? GenericWebController
        webController?.webViewURL = NSLocalizedString(CMON_GENERIC_TERMSURL, comment: "")
        webController?.title = NSLocalizedString(CMON_GENERIC_TNC, comment: "")
        if let aController = genericWebNavController {
            present(aController, animated: true)
        }
    }
    
    func userSelectedFinalImage(_ image: UIImage?) {
        
        let image1 = ImageHelper.image(with: image, scaledTo: CGSize.init(width: image!.size.width/3, height: image!.size.height/3))
        selectedImages.removeObject(at: selectedImageIndex)
        selectedImages.insert(image1, at: selectedImageIndex)
        let placeHolderView = screenshotPlaceholders[selectedImageIndex] as? UIView
        placeHolderView?.isHidden = true
        let selectedImageView = screenshotImages[selectedImageIndex] as? UIImageView
        selectedImageView?.image = image
    }
    
    
    func allowImageEditing() -> Bool {
        return false
    }
    
    func validateSupportMailInfo() -> Bool {
        if ("" == issueTextView.text) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_ABOUTSUPPORT_PROBLEMDESCRIBEVAL, comment: ""))
            return false
        }
        return true
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true) {
            
        }
    }
    
    @IBAction func showFreshChat(_ sender: Any) {
        Freshchat.sharedInstance().showConversations(self)
    }
    
    func addDropShadow(for viewShadow: UIView?) {
       // viewShadow?.backgroundColor = UIColor.white
        viewShadow?.layer.shadowColor = UIColor(red: 117.0 / 255.0, green: 117.0 / 255.0, blue: 117.0 / 255.0, alpha: 0.4).cgColor
        viewShadow?.layer.shadowOpacity = 1
        viewShadow?.layer.shadowOffset = CGSize(width: -1, height: 0)
        viewShadow?.layer.shadowRadius = 4
        viewShadow?.layer.masksToBounds = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
