//
//  Category.swift
//  zify
//
//  Created by Anurag on 20/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

import Foundation
import UIKit

 

extension UIScreen {
    
    enum SizeType: CGFloat {
        case Unknown = 0.0
        case iPhone4 = 960.0
        case iPhone5 = 1136.0
        case iPhone6 = 1334.0
        case iPhoneXR = 1792.0
        case iPhone6Plus = 1920.0
        case iPhoneXAndXS = 2436.0
        case iPhoneXSMax = 2688.0
    }
    
    var sizeType: SizeType {
        let height = nativeBounds.height
        guard let sizeType = SizeType(rawValue: height) else { return .Unknown }
        return sizeType
    }
}

func setFontSizes(anyObj : AnyObject, fontStyle : String, defaultSize : CGFloat, iphone6Size : CGFloat, iphoneXRSize : CGFloat, iphone6PlusSize : CGFloat, iphoneXSize : CGFloat, iphoneXSMaxSize : CGFloat) -> UIFont {
    var tempObj = UIFont.init(name: fontStyle, size: defaultSize)
    if UIScreen.main.sizeType == .iPhone6 {
        tempObj = UIFont.init(name: fontStyle, size: iphone6Size)
    } else if UIScreen.main.sizeType == .iPhoneXR {
        tempObj = UIFont.init(name: fontStyle, size: iphoneXRSize)
    }  else if UIScreen.main.sizeType == .iPhone6Plus {
        tempObj = UIFont.init(name: fontStyle, size: iphone6PlusSize)
    } else  if UIScreen.main.sizeType == .iPhoneXAndXS {
        tempObj = UIFont.init(name: fontStyle, size: iphoneXSize)
    } else  if UIScreen.main.sizeType == .iPhoneXSMax {
        tempObj = UIFont.init(name: fontStyle, size: iphoneXSMaxSize)
    }
    return tempObj ?? UIFont.init(name: fontStyle, size: defaultSize)!
}
func setBorderCornerRadius(anyOb: AnyObject, color: UIColor) {
    if( anyOb is UIButton ){
        let btn = anyOb as! UIButton
      btn.layer.cornerRadius = 4
      btn.layer.borderColor = color.cgColor
      btn.layer.borderWidth = 1
      btn.layer.masksToBounds = true
    }else if ( anyOb is UILabel){
        let lbl = anyOb as! UILabel
        lbl.layer.cornerRadius = 4
        lbl.layer.borderColor = color.cgColor
        lbl.layer.borderWidth = 1
        lbl.layer.masksToBounds = true
    }
 
}

func showAlertView(strMessage: String, navigationCtrl : UINavigationController) {
    let alertVc = UIAlertController.init(title: AppName, message: strMessage, preferredStyle: .alert)
    let okAction = UIAlertAction.init(title: "Ok", style: .default) { (UIAlertViewAction) in
        
    }
    alertVc.addAction(okAction)
    navigationCtrl.present(alertVc, animated: true, completion: nil)
}

func isValidEmail(emailStr: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: emailStr)
}

public func showShadow(view: UIView, color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 5, scale: Bool = true) {
    view.layer.masksToBounds = false
    view.layer.shadowColor = color.cgColor
    view.layer.shadowOpacity = opacity
    view.layer.shadowOffset = offSet
    view.layer.shadowRadius = radius
    
    //        view.layer.shadowPath = UIBezierPath(rect: self.view.bounds).cgPath
  //  view.layer.shouldRasterize = true
   // view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    
}

extension UIView {
    
    func dropShadow(isShowShadow: Bool) {
        if isShowShadow == true {
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowOffset = CGSize(width: 0, height: 1)
            
            self.layer.shadowRadius = 1
//            self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = UIScreen.main.scale
        } else {
            self.layer.masksToBounds = true
            
        }
    }
}


@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}

extension UIView{
    
    @IBInspectable var shadowOffset: CGSize{
        get{
            return self.layer.shadowOffset
        }
        set{
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowColor: UIColor{
        get{
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set{
            self.layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat{
        get{
            return self.layer.shadowRadius
        }
        set{
            self.layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float{
        get{
            return self.layer.shadowOpacity
        }
        set{
            self.layer.shadowOpacity = newValue
        }
    }
}

extension UILabel {
    var fontSize: CGFloat {
        get {
            return self.font.pointSize
        }
        set {
            self.font =  UIFont(name: self.font.fontName, size: newValue)!
            self.sizeToFit()
        }
    }
}

func getJsonObj(str : String) -> [AnyObject] {
    let data = str.data(using: .utf8)!
    do {
        if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>] {
            debugPrint(jsonArray, "-----jsonArray--") // use the json here
            return jsonArray as [AnyObject]
        } else {
            print("bad json")
            return []
        }
    } catch let error as NSError {
        print(error)
    }
    return []
}

func getDateFormatToTimeForInvoice(strDate : String) -> String {
    var strDateReturn = ""
    
    let dateTimeFormatterGet = DateFormatter()
    dateTimeFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    let locale = NSLocale(localeIdentifier: "en_US_POSIX")
    dateTimeFormatterGet.locale = locale as Locale?
    
    let settingFormat = AppDelegate.getInstance().timeFormatter
    if let date = dateTimeFormatterGet.date(from: strDate) {
        print(settingFormat.string(from: date))
        strDateReturn = settingFormat.string(from: date)
    } else {
        print("There was an error decoding the date string for invoice")
        //Jul 10, 2019 05:55:16 AM
    }
    
    return strDateReturn
}
