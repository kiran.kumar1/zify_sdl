//
//  TravelPrefStep2.swift
//  zify
//
//  Created by Anurag S Rathor on 17/11/17.
//  Copyright © 2017 zify. All rights reserved.
//

import UIKit

class TravelPrefStep4: BaseViewController,DateTimePickerDelegate {
    @IBOutlet var messageHandler:MessageHandler! = MessageHandler()
    var ownerView:UIView = UIView()
    var riderView:UIView = UIView()
    var startTimeBtn:UIButton = UIButton()
    var returnTimeBtn:UIButton = UIButton()
    
    var selectedStartTime:String = String()
    var selectedReturnTime:String = String()


    var isStartTimeTapped:Bool = Bool()
    var dateTimeFormatter1:DateFormatter = DateFormatter()
    var timeFormatter1:DateFormatter = DateFormatter()
    //var timeFormatter2:DateFormatter = DateFormatter()
    var blurView:UIView = UIView()
    var displaySuccessView:UIView = UIView()

    
    
    @IBOutlet  var dateTimePickerView:DateTimePickerView! = DateTimePickerView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDateFormatter()
        selectedStartTime = "08:00:00"
        selectedReturnTime = "18:00:00"
        self.progressView.setProgress(1.0, animated: true)

        self.btnNext.addTarget(self, action: #selector(btnNextTapped), for: .touchUpInside)
        self.btnNext.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        self.btnNext.setTitle("COMPLETE", for:.normal)
        self.btnNext.setTitle("COMPLETE", for:.selected)
        self.btnNext.setImage(nil, for: .normal)
        self.btnNext.setImage(nil, for: .selected)
        self.headerTitleLbl.text = NSLocalizedString(TP_SCREEN4_TITLE, comment: "");
        self.headerSubTitleLbl.text = NSLocalizedString(TP_SCREEN4_SUB_TITLE, comment: "");
        
        
        let remainingHeight:CGFloat = (self.bgView.frameHeight - self.headerSubTitleLbl.frameMaxY - self.bottomView.frameHeight)/2 - 1;
        ownerView = createViewForRiderOrOwner(imageName: "icn_onboard_pref_time@3x.png", withTitle: NSLocalizedString(TP_SCREEN4_FORWARD_TIME_TITLE, comment: ""), withFrame:CGRect(x: 0, y: self.headerSubTitleLbl.frameMaxY, width: AppDelegate.screen_WIDTH(), height: remainingHeight),withTag: 44)
        self.view.addSubview(ownerView);
        
        let lineSeparator:UILabel = UILabel();
        lineSeparator.frame = CGRect(x:0, y:ownerView.frameMaxY, width:AppDelegate.screen_WIDTH(), height:2);
        lineSeparator.backgroundColor = UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        self.view.addSubview(lineSeparator);
        
        riderView = createViewForRiderOrOwner(imageName: "icn_onboard_pref_time@3x.png", withTitle: NSLocalizedString(TP_SCREEN4_BACKWORD_TIME_TITLE, comment: ""), withFrame: ownerView.frame,withTag: 55)
        riderView.frameY = lineSeparator.frameMaxY
        self.view.addSubview(riderView);
        
       
        self.dateTimePickerView.delegate = self as DateTimePickerDelegate
        self.dateTimePickerView.isTimePicker = true;
        
        
        
        
        // Dasny additional setup after loading the view.
    }
    func createViewForRiderOrOwner(imageName:String, withTitle:String, withFrame:CGRect, withTag:Int) -> UIView {
        
        let mainview:UIView = UIView.init(frame: withFrame)
        mainview.backgroundColor = UIColor.clear;
        
        let padX:CGFloat = 30;
        let padY:CGFloat = 20;
        let view:UIView = UIView.init(frame: CGRect(x: padX, y: padY, width:(AppDelegate.screen_WIDTH() - 2*padX), height: mainview.frameHeight - 2*padY));
        view.backgroundColor = UIColor.white;
        mainview.addSubview(view);
        
        var fontSize:CGFloat = 14;
        if(AppDelegate.screen_HEIGHT() > 568){
            fontSize = 16;
        }
        let label: UILabel = UILabel.init(frame:CGRect(x: 0
            , y: 25, width: view.frameWidth, height: 30))
        label.textColor = UIColor.init(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0);
        label.text = withTitle;
        label.backgroundColor = UIColor.clear
        label.textAlignment = NSTextAlignment.center;
        view.addSubview(label);
        label.font = UIFont.init(name: NormalFont, size: fontSize)
        
        let clockImg:UIImage = UIImage.init(named: imageName)!
        var factor:CGFloat = 1;
        if(AppDelegate.screen_HEIGHT() == 568){
            factor = 1.3;
        }
        let imageLogo:UIImageView = UIImageView.init(frame: CGRect(x:0, y: 0, width: clockImg.size.width/factor, height: clockImg.size.height/factor))
        imageLogo.frameY = (view.frameHeight - imageLogo.frameHeight) - label.frameY;
        imageLogo.image = clockImg
        view.addSubview(imageLogo);
        imageLogo.center = CGPoint(x:view.frameWidth/2, y:view.frameHeight/2);
        imageLogo.frameY = imageLogo.frameY - label.frameHeight/2;
        
        
        let timeBtn: UIButton = UIButton.init(type: .custom)
        timeBtn.frame = CGRect(x: view.frameWidth/2
            , y: view.frameHeight - label.frameY - label.frameHeight, width: view.frameWidth/2, height: imageLogo.frameHeight)
        timeBtn.tag = withTag
        timeBtn.setTitleColor(UIColor.init(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0), for: .normal)
        timeBtn.setTitleColor(UIColor.init(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0), for: .selected)
        view.addSubview(timeBtn);
        timeBtn.addTarget(self, action: #selector(btnTimeTapped), for: .touchUpInside)
        timeBtn.contentHorizontalAlignment = .left
        timeBtn.titleLabel?.font = label.font
        if(withTag == 44){
            startTimeBtn = timeBtn;
            startTimeBtn.setTitle("08:00 AM", for: .normal)
            startTimeBtn.setTitle("08:00 AM", for: .selected)
            
        }else{
            returnTimeBtn = timeBtn;
            returnTimeBtn.setTitle("06:00 PM", for: .normal)
            returnTimeBtn.setTitle("06:00 PM", for: .selected)
        }
        imageLogo.frameX = timeBtn.frameX - imageLogo.frameWidth - 10
        imageLogo.frameY = timeBtn.frameY
     
        view.layer.borderColor = UIColor.red.cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 5;
        
        return mainview;
    }
 
    func btnNextTapped(sender:UIButton) -> Void {
       print("call service for updating")
       // self.createViewForSuccessOnServiceCall()
       // self.callServiceForAddingTravelPreference()
    }
    func btnTimeTapped(sender:UIButton!) -> Void {
        
        if(sender.tag == 44){isStartTimeTapped = true}
        else{isStartTimeTapped = false}
        self.selectTimeClicked(sender: sender)
    }
    func selectTimeClicked(sender:UIButton!) -> Void {
        if(isStartTimeTapped){
        self.dateTimePickerView.selectedDate = timeFormatter1.date(from: selectedStartTime)
        }else{
            self.dateTimePickerView.selectedDate = timeFormatter1.date(from: selectedReturnTime)
        }
        self.dateTimePickerView.show(in: self.view)
    }
    
    func selectedDate(_ selectedDate: Date!) {
        
        print("selecte dtime is \(selectedDate)")
        let timeStr:String = AppDelegate.getInstance().timeFormatter.string(from: selectedDate)
        if(isStartTimeTapped){
        selectedStartTime = timeFormatter1.string(from: selectedDate)
        startTimeBtn.setTitle(timeStr, for: .normal)
        startTimeBtn.setTitle(timeStr, for: .selected)
        }else{
            selectedReturnTime = timeFormatter1.string(from: selectedDate)
            returnTimeBtn.setTitle(timeStr, for: .normal)
            returnTimeBtn.setTitle(timeStr, for: .selected)
        }
    }

    func convertDateTimeToTime(dateWithTime:Date) -> String {
        
//        let locale:Locale  = Locale.init(identifier: "en_US_POSIX")
//        let timeFormatter:DateFormatter = DateFormatter();
//        timeFormatter.dateFormat = "HH:mm"
//        timeFormatter.locale = locale
        return AppDelegate.getInstance().timeFormatter.string(from:dateWithTime)
    }
    func setDateFormatter() -> Void {
        
        let locale:Locale  = Locale.init(identifier: "en_US_POSIX")
        dateTimeFormatter1 = DateFormatter();
        dateTimeFormatter1.dateFormat = "yyyy/MM/dd HH:mm"
        dateTimeFormatter1.locale = locale
        
        timeFormatter1 = DateFormatter();
        timeFormatter1.dateFormat = "HH:mm:ss"
        timeFormatter1.locale = locale
        
//        timeFormatter2 = DateFormatter();
//        timeFormatter2.dateFormat = "HH:mm"
//        timeFormatter2.locale = locale

    }
    
 /*   func callServiceForAddingTravelPreference() -> Void {
        self.messageHandler.showBlockingLoadView {
            
            let userMode:String =   AppDelegate.getInstance().paramsDict.object(forKey: "OwnerOrRider") as! String
            print("param are \(AppDelegate.getInstance().paramsDict)")
           // var userModeStr:String;
            if(userMode == "RIDER")
            {
                //userModeStr = NSLocalizedString(VC_USERPREFERENCES_RIDERVAL, comment: "")
                AppDelegate.getInstance().homeToOffDriveRoute = nil;
                AppDelegate.getInstance().oFFToHomeDriveRoute = nil;
            }else{
               // userModeStr = NSLocalizedString(VC_USERPREFERENCES_DRIVERVAL, comment: "")
            }
            
            let request:UserPreferencesRequest = UserPreferencesRequest.init(with:ADDPREFERENCES, andHomeAddress: AppDelegate.getInstance().homeAddress, andOfficeAddress: AppDelegate.getInstance().officeAddress, andStartTime: self.selectedStartTime, andReturnTime: self.selectedReturnTime, andOnwardRoute: AppDelegate.getInstance().homeToOffDriveRoute, andReturn: AppDelegate.getInstance().oFFToHomeDriveRoute, andUserMode: userMode)
            ServerInterface.sharedInstance().getResponse(request, withHandler: { (response:ServerResponse?, error) in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if(response != nil){
                        self.createViewForSuccessOnServiceCall();
                    }else{
                        print("Errrororororo");
                        self.messageHandler.showErrorMessage(NSLocalizedString(CMON_GENERIC_INTERNALERROR, comment: ""))
                    }
                })
            })
        }
    }*/
    func createViewForSuccessOnServiceCall() -> Void {
        
        blurView = UIView.init(frame: self.view.frame)
        blurView.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.7)
        self.view.addSubview(blurView)
        
        let posX:CGFloat = 30
        let viewHeight:CGFloat = 300;
        displaySuccessView = UIView.init(frame: CGRect(x: posX, y: (AppDelegate.screen_HEIGHT() - viewHeight)/2, width: AppDelegate.screen_WIDTH() - 2*posX, height: viewHeight))
        displaySuccessView.backgroundColor = UIColor.white
        self.view.addSubview(displaySuccessView);
        
        let topView:UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: displaySuccessView.frameWidth , height: 90))
        topView.backgroundColor = UIColor.red
        displaySuccessView.addSubview(topView);
        
        
        
        let imageViewHeight:CGFloat = 50;
        let tickImageView:UIImageView = UIImageView.init(frame: CGRect(x:(displaySuccessView.frameWidth - imageViewHeight)/2,y:-imageViewHeight/2,width: imageViewHeight,height:imageViewHeight))
        tickImageView.image = UIImage.init(named: "icn_account_withdrawl_verified@2x.png")
        tickImageView.layer.cornerRadius = tickImageView.frameHeight/2;
        displaySuccessView.addSubview(tickImageView)
        
        let fontSize:CGFloat = 17;
        
        let label: UILabel = UILabel.init(frame:CGRect(x: 0
            , y: tickImageView.frameMaxY, width: displaySuccessView.frameWidth, height: topView.frameHeight - tickImageView.frameMaxY))
        label.textColor = UIColor.white
        label.text = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG, comment: "");
        label.font = UIFont.init(name: MediumFont, size: fontSize)
        label.backgroundColor = UIColor.clear
        label.textAlignment = NSTextAlignment.center;
        displaySuccessView.addSubview(label);
        
        
        
        let fontSize1:CGFloat = 14;
        let label1: UILabel = UILabel.init(frame:CGRect(x: 15
            , y: topView.frameMaxY, width: label.frameWidth - 30, height: 50))
        label1.numberOfLines = 0;
        label1.text = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG1, comment: "");
        label1.font = UIFont.init(name: MediumFont, size: fontSize1)
        label1.backgroundColor = UIColor.clear
        label1.textAlignment = NSTextAlignment.center;
        displaySuccessView.addSubview(label1);
        
        let label2: UILabel = UILabel.init(frame:label1.frame)
        label2.frameY = label1.frameMaxY
        label2.textColor = UIColor.black
        label2.numberOfLines = 0;
        label2.text = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG2, comment: "");
        label2.font = UIFont.init(name: NormalFont, size: fontSize1)
        label2.backgroundColor = UIColor.clear
        displaySuccessView.addSubview(label2);
        
        let label3: UILabel = UILabel.init(frame:label1.frame)
        label3.frameY = label2.frameMaxY
        label3.textColor = UIColor.lightGray
        label3.numberOfLines = 0;
        label3.text = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG3, comment: "");
        label3.font = UIFont.init(name: NormalFont, size: fontSize1)
        label3.backgroundColor = UIColor.clear
        displaySuccessView.addSubview(label3);
        
        
        let btnfontSize:CGFloat = 16;
        let btnWidth:CGFloat = 130;
        let btnHeight:CGFloat = 40;
        let btn:UIButton = UIButton.init(type: UIButtonType.custom);
        btn.frame = CGRect(x: (displaySuccessView.frameWidth - btnWidth)/2
            , y: label3.frameMaxY + 5, width: btnWidth, height: btnHeight)
        btn.backgroundColor = topView.backgroundColor
        //
        btn.setTitle(NSLocalizedString(CMON_GENERIC_OK, comment: ""), for: .normal)
        btn.setTitle(NSLocalizedString(CMON_GENERIC_OK, comment: ""), for: .selected)
        displaySuccessView.addSubview(btn);
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitleColor(UIColor.white, for: .selected)
        btn.addTarget(self, action: #selector(btnOkTapped), for: .touchUpInside)
        btn.titleLabel?.font = UIFont.init(name: MediumFont, size: btnfontSize)
        btn.layer.cornerRadius = btn.frameHeight/2
    }
    func btnOkTapped(sender:UIButton!) -> Void {
        OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: false)
        PushNotificationManager.sharedInstance().handlePushRegisterWithUser()
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

