//
//  StatementController.swift
//  zify
//
//  Created by Anurag on 06/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class WalletStatementController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var imgNoStmtRecharge: UIImageView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noTransactionView: UIView!
    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var noTransactionViewTopConstraint: NSLayoutConstraint!
    
    var nodataMsgStr = ""
    
    var selectedStatementType: Int = 0
    var rechargeStatementArray = [AnyObject]()
    var transactionStatementArray = [AnyObject]()
    var statementDatesArray : NSMutableArray = []
    var statementDict: [String : AnyObject] = [:]
    var dateFormatter: DateFormatter?
    var dateTimeFormatter: DateFormatter?
    var currentLocale: CurrentLocale?
    
    enum StatementType : Int {
        case TRANSACTION = 0
        case RECHARGE
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        rechargeStatementArray = []
        transactionStatementArray = []
        dateTimeFormatter = DateFormatter()
        dateTimeFormatter?.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateTimeFormatter?.locale = locale as Locale?
        dateFormatter = DateFormatter()
        dateFormatter?.dateFormat = "MMM dd, yyyy"
        currentLocale = CurrentLocale.sharedInstance()
        if (currentLocale?.isGlobalLocale())! && !(currentLocale?.isGlobalPayment())! {
            segmentedControl.isHidden = true
        }
        nodataMsgStr = NSLocalizedString(WS_STATEMENT_LBL_TRANSACTIONS_NOTRANSACTIONS, comment: "")
        initialiseView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        selectedStatementType = StatementType.TRANSACTION.rawValue //TRANSACTION
        segmentedControl.selectedSegmentIndex = StatementType.TRANSACTION.rawValue
        
        fetchTransactionStatement()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        messageHandler.dismissErrorView()
        messageHandler.dismissSuccessView()
    }
    
    func initialiseView() {
        segmentedControl.setTitle(NSLocalizedString(VC_WALLETSTATEMENT_TRANSACTION, comment: ""), forSegmentAt: 0)
        segmentedControl.setTitle(NSLocalizedString(VC_WALLETSTATEMENT_RECHARGE, comment: ""), forSegmentAt: 1)
        tableView.estimatedRowHeight = 40.0
        tableView.rowHeight = UITableViewAutomaticDimension
        if (currentLocale?.isGlobalLocale())! && !(currentLocale?.isGlobalPayment())! {
            view.removeConstraint(tableViewTopConstraint)
            view.removeConstraint(noTransactionViewTopConstraint)
            noTransactionView.translatesAutoresizingMaskIntoConstraints = false
            tableView.translatesAutoresizingMaskIntoConstraints = false
            let newTableTopConstraint = NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0.0)
            view.addConstraint(newTableTopConstraint)
            let newNoTransactionViewConstraint = NSLayoutConstraint(item: noTransactionView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0.0)
            view.addConstraint(newNoTransactionViewConstraint)
        }
    }
    
    // MARK:- UITableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return statementDatesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if statementDatesArray.count != 0 {
            let statementArray = (statementDict as NSDictionary).object(forKey: statementDatesArray[section])
            return (statementArray as AnyObject).count + 1
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let statementHeaderCell = tableView.dequeueReusableCell(withIdentifier: "statementHeaderCell") as? WalletStatementHeaderCell
            statementHeaderCell?.statementDate.text = statementDatesArray[indexPath.section] as? String
            return statementHeaderCell!
        } else {
            let statementArray = (statementDict as NSDictionary).object(forKey: statementDatesArray.object(at: indexPath.section) as! String)
            let currencySymbol:String = (currentLocale?.getCurrencySymbol()!)!
            if selectedStatementType == StatementType.RECHARGE.rawValue {
                let rechargeCell = tableView.dequeueReusableCell(withIdentifier: "rechargeCell") as? WalletRechargeCell
                //var rechargeStatement = statementArray[indexPath.row - 1] as? WalletRechargeStatement
                let rechargeStatement = (statementArray as? NSArray)?.object(at: indexPath.row - 1) as? WalletRechargeStatement
                if let anId = rechargeStatement?.transactionId {
                    rechargeCell?.transactionId.text = String(format: NSLocalizedString(VC_WALLETSTATEMENT_TXNID, comment: "TXN ID : {RECHARGETXNID}"), anId)
                }
                if let anAmount = rechargeStatement?.transactionAmount {
                    print("rechargeStatement.transactionAmount is \(anAmount)")
                }
                
              /*  if(currencySymbol == "€"){
                    rechargeCell?.transactionAmount.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator: (rechargeStatement?.transactionAmount)!)
                }else{
                    rechargeCell?.transactionAmount.text = currencySymbol + " " + "\((rechargeStatement?.transactionAmount)!)"
                }*/
                rechargeCell?.transactionAmount.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator: (rechargeStatement?.transactionAmount)!)

                
                if UIScreen.main.sizeType == .iPhone5 {
                    rechargeCell?.transactionId.fontSize = 10
                    rechargeCell?.transactionAmount.fontSize = 17
                } else if UIScreen.main.sizeType == .iPhone6 {
                    rechargeCell?.transactionId.fontSize = 12
                    rechargeCell?.transactionAmount.fontSize = 18
                } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
                    rechargeCell?.transactionId.fontSize = 12
                    rechargeCell?.transactionAmount.fontSize = 19
                }
                return rechargeCell!
                
            } else if selectedStatementType == StatementType.TRANSACTION.rawValue {
                let transactionCell = tableView.dequeueReusableCell(withIdentifier: "transactionCell") as? WalletTransactionCell
                let transactionStatement = (statementArray as? NSArray)?.object(at: indexPath.row - 1) as? WalletTransactionStatement
                transactionCell?.transactionId.text = String(format: NSLocalizedString(VC_WALLETSTATEMENT_ID, comment: "{TRIPTYPE} ID: {TRIPID}"), (transactionStatement?.travelType.uppercased())!, "\((transactionStatement?.transactionId)!)")
              /*  if let aPoints = transactionStatement?.zifyPoints {
                    transactionCell?.zifyPoints.text = currencySymbol + aPoints //"\(String(describing: currentLocale?.getCurrencySymbol()))\(aPoints)"
                }*/
                
              /*  if(currencySymbol == "€"){
                    transactionCell?.zifyPoints.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator: (transactionStatement?.zifyPoints)!) 
                }else{
                    transactionCell?.zifyPoints.text = currencySymbol + " " + "\((transactionStatement?.zifyPoints)!)"
                }*/
                transactionCell?.zifyPoints.text = AppDelegate.getInstance().displayTripAmount(inCommaSeparator: (transactionStatement?.zifyPoints)!)

                
                if let aText = transactionCell?.zifyPoints.text {
                    print("transactionCell.zifyPoints.text is \(aText)")
                }
                print("type is \(String(describing: transactionStatement?.transactionType))")
                transactionCell?.transactionType.text = transactionStatement?.transactionType != nil ? transactionStatement?.transactionType.uppercased() : ""
                transactionCell?.transactionType.layer.cornerRadius = 3.0
                transactionCell?.transactionType.layer.borderWidth = 1.0
                transactionCell?.transactionType.layer.borderColor = UIColor.lightGray.cgColor
                if ("CREDIT" == transactionStatement?.transactionType) {
                    transactionCell?.transactionType.textColor = UIColor(red: 0.0, green: 150.0 / 255.0, blue: 136.0 / 255.0, alpha: 1.0)
                } else {
                    transactionCell?.transactionType.textColor = UIColor(red: 231.0 / 255.0, green: 76.0 / 255.0, blue: 58.0 / 255.0, alpha: 1.0)
                }
                
                if UIScreen.main.sizeType == .iPhone5 {
                    transactionCell?.transactionId.fontSize = 11
                    transactionCell?.transactionType.fontSize = 11
                    transactionCell?.zifyPoints.fontSize = 18
                } else if UIScreen.main.sizeType == .iPhone6 {
                    transactionCell?.transactionId.fontSize = 12
                    transactionCell?.transactionType.fontSize = 11
                    transactionCell?.zifyPoints.fontSize = 18
                } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
                    transactionCell?.transactionId.fontSize = 13
                    transactionCell?.transactionType.fontSize = 12
                    transactionCell?.zifyPoints.fontSize = 19
                }
                
                return transactionCell!
            }
           
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    // MARK: - Segment Controller
    @IBAction func statementTypeChanged(_ sender: Any) {
        if(statementDatesArray.count != 0){
            statementDatesArray.removeAllObjects()
        }
        tableView.reloadData()
        let selectedIndex: Int = segmentedControl.selectedSegmentIndex
        noTransactionView.isHidden = true
        if selectedIndex == StatementType.TRANSACTION.rawValue {
            imgNoStmtRecharge.image = UIImage(named: "NoStatement")
            nodataMsgStr = NSLocalizedString(WS_STATEMENT_LBL_TRANSACTIONS_NOTRANSACTIONS, comment: "")
            fetchTransactionStatement()
        } else {
            imgNoStmtRecharge.image = UIImage(named: "NoRecharge")
            nodataMsgStr = NSLocalizedString(WS_STATEMENT_LBL_RECHARGE_NOTRANSACTIONS, comment: "")
            fetchRechargeStatement()
        }
    }
    
    @IBAction func dismiss(_ sender: Any) {
        navigationController?.popViewController(animated: false)

       /* if (currentLocale?.isGlobalLocale())! {
            navigationController?.dismiss(animated: false)
        } else {
            navigationController?.popViewController(animated: false)
        }*/
    }
    
    func fetchRechargeStatement() {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: RECHARGESTATEMENTREQUEST), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.rechargeStatementArray = response?.responseObject as! [NSMutableArray]
                        if self.rechargeStatementArray.count == 0 {
                            self.resetStatementDict()
                            self.noTransactionView.isHidden = false
                            self.noDataLbl.text = self.nodataMsgStr
                            self.tableView.isHidden = true
                            self.noTransactionView.isHidden = false
                        } else {
                            self.noTransactionView.isHidden = true
                            self.prepareStatementDictforRecharge()
                            self.selectedStatementType = StatementType.RECHARGE.rawValue
                            self.tableView.isHidden = false
                            self.noTransactionView.isHidden = true
                        }
                    } else {
                        self.rechargeStatementArray = []
                        self.resetStatementDict()
                        self.noTransactionView.isHidden = true
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                    self.tableView.reloadData()
                })
            })
        })
    }
    
    func prepareStatementDictforRecharge() {
        
        statementDict = [String : AnyObject]()
        statementDatesArray = []
        
        for (idx, obj) in rechargeStatementArray.enumerated() {
            
            let rechargeStatement = obj as? WalletRechargeStatement
            var rechargeDate: String? = ""
            rechargeDate = dateFormatter?.string(from: (dateTimeFormatter?.date(from: (rechargeStatement?.rechargeTimestamp)!))!)
            var rechargeArray = statementDict[rechargeDate!] as? NSMutableArray
            if rechargeArray == nil {
                rechargeArray = []
                statementDict.updateValue(rechargeArray as AnyObject, forKey: rechargeDate!)
            }
            rechargeArray?.add(rechargeStatement!)
            let lastDate = statementDatesArray.lastObject as? String
            if lastDate == nil || !(lastDate == rechargeDate) {
                statementDatesArray.add(rechargeDate!)
            }
            
        }
        
    }
    
    func fetchTransactionStatement() {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: TRANSACTIONSTATEMENTREQUEST), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.transactionStatementArray = response?.responseObject as! [NSMutableArray]
                        if self.transactionStatementArray.count == 0 {
                            self.resetStatementDict()
                            self.noTransactionView.isHidden = false
                            self.noDataLbl.text = self.nodataMsgStr
                            self.tableView.isHidden = true
                            self.noTransactionView.isHidden = false
                        } else {
                            self.noTransactionView.isHidden = true
                            self.prepareStatementDictForTransaction()
                            self.selectedStatementType = StatementType.TRANSACTION.rawValue
                            self.tableView.isHidden = false
                            self.noTransactionView.isHidden = true
                        }
                    } else {
                        self.transactionStatementArray = []
                        self.resetStatementDict()
                        self.noTransactionView.isHidden = true
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                    self.tableView.reloadData()
                })
            })
        })
    }
    
    func prepareStatementDictForTransaction() {
        statementDict = [String : AnyObject]()
        statementDatesArray = []
        
        for (idx, obj) in transactionStatementArray.enumerated() {
            let transactionStatement = obj as? WalletTransactionStatement
            var rechargeDate: String? = ""
            
            let depdateStr = transactionStatement?.paymentTimestamp != nil ? (transactionStatement?.paymentTimestamp)!  : ""
            let deptDateReq:Date = dateTimeFormatter?.date(from:depdateStr) != nil ? (dateTimeFormatter?.date(from:depdateStr))! : NSDate() as Date
            rechargeDate = dateFormatter?.string(from: deptDateReq) != nil ?  dateFormatter?.string(from: deptDateReq) : ""
            var statementArray = statementDict[rechargeDate!] as? NSMutableArray
            if statementArray == nil {
                statementArray = []
                statementDict.updateValue(statementArray as AnyObject, forKey: rechargeDate!)
            }
            //statementArray?.append(transactionStatement!)
            statementArray?.add(transactionStatement!)
            
           // print("statementArray is \(statementArray!)")
            
            
            let lastDate = statementDatesArray.lastObject as? String
            if lastDate == nil || !(lastDate == rechargeDate) {
                statementDatesArray.add(rechargeDate!)
            }
        }
        
    }
    
    func resetStatementDict() {
        statementDict = [:]
        statementDatesArray = []
    }
    
    class func createWalletStatementNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Wallet", bundle: Bundle.main)
        let walletStatementNavContoller = storyBoard.instantiateViewController(withIdentifier: "walletStatementNavContoller") as? UINavigationController
        return walletStatementNavContoller
    }
    
}

class WalletStatementHeaderCell : UITableViewCell {
    @IBOutlet weak var statementDate: UILabel!
}

class WalletRechargeCell : UITableViewCell {
    @IBOutlet weak var transactionId: UILabel!
    @IBOutlet weak var transactionAmount: UILabel!
}

class WalletTransactionCell : UITableViewCell {
    @IBOutlet weak var transactionId: UILabel!
    @IBOutlet weak var zifyPoints: UILabel!
    @IBOutlet weak var transactionType: UILabel!
}
