//
//  GuestMenuOptionCell.h
//  zify
//
//  Created by Anurag S Rathor on 19/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestMenuOptionCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *optionName;
@property(nonatomic,weak) IBOutlet UIImageView *optionImage;
@end
