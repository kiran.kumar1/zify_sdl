//
//  ChatUserDetailCell.swift
//  zify
//
//  Created by Anurag Rathor on 07/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class ChatUserDetailCell: UITableViewCell {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var unreadCount: UILabel!
    @IBOutlet weak var lastMessage: UILabel!
    @IBOutlet weak var lastMessageDate: UILabel!
    @IBOutlet weak var userImage: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
