//
//  GuestSearchResultsCell.h
//  zify
//
//  Created by Anurag on 17/07/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestSearchResultsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIView *viewDetails;
@property (weak, nonatomic) IBOutlet UIImageView *imgDriverProfile;
@property (weak, nonatomic) IBOutlet UIView *viewVerified;
@property (weak, nonatomic) IBOutlet UILabel *lblWorksAt;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblRateName;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblVerified;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupDelta;
@property (weak, nonatomic) IBOutlet UILabel *lblDropDelta;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDestinationAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblTravelDistance;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnRequest;
@property (weak, nonatomic) IBOutlet UIButton *btnCarDetailView;
@property (weak, nonatomic) IBOutlet UIButton *btnRouteMap;
@property (weak, nonatomic) IBOutlet UIView *viewMessageRequest;
@property (weak, nonatomic) IBOutlet UIView *viewRequest;

@end
