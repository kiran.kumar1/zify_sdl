//
//  NewTPTableViewCell.swift
//  zify
//
//  Created by Anurag Rathor on 25/02/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit



class NewTPTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sectionImageView:UIImageView!
    @IBOutlet weak var displayTitleLabel:UILabel!
    @IBOutlet weak var displaySubTitle:UILabel!
    @IBOutlet weak var setButton:UIButton!
    @IBOutlet weak var rightIconImageView:UIImageView!
    @IBOutlet weak var viewRouteBtn:UIButton!
    
   // var moveToNewTpScreenProtocalDelegate: moveToNewTpScreenProtocal?
   // var modeCellsArray = [SPSegmentedControlCell]()


    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let viewRouteStr = NSLocalizedString(TS_DRIVES_LBL_VIEWROUTE, comment: "")
        viewRouteBtn.setAttributedTitle(NSAttributedString(string: viewRouteStr, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]), for: .normal)
        
        // Initialization code
       // self.adjustFontChangesForCell()
    //    self.modeChangeSwitch.borderWidth = 1;
  //      self.modeChangeSwitch.backgroundColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
  //      self.modeChangeSwitch.delegate = self
 //       self.modeChangeSwitch.styleDelegate = self
  //      self.addSwitchControlImages()
        
    }
    
    func adjustFontChangesForCell() -> Void {
            displayTitleLabel.fontSize = 11
            displaySubTitle.fontSize = 8
            setButton.titleLabel?.fontSize = 12
            viewRouteBtn.titleLabel?.fontSize = 12

         if UIScreen.main.sizeType == .iPhone6 {
            displayTitleLabel.fontSize = 14
            displaySubTitle.fontSize = 11
            setButton.titleLabel?.fontSize = 14
            viewRouteBtn.titleLabel?.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            displayTitleLabel.fontSize = 15
            displaySubTitle.fontSize = 12
            setButton.titleLabel?.fontSize = 16
            viewRouteBtn.titleLabel?.fontSize = 16
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
 /*   func addSwitchControlImages() {
        
        let img1: UIImage = createImage(withImage: "Shape")
        let img2: UIImage = createImage(withImage: "relaxing-walk")
        normalImagesArr = [img1, img2]
        
        let selectedimg1: UIImage = createImage(withImage: "Shape_selected")
        let selectedimg2: UIImage = createImage(withImage: "relaxing-walk_selected")
        selectedImagesArr = [selectedimg1 , selectedimg2]
        
        let cell1: SPSegmentedControlCell = createCell("", wihImage: img1)
        let cell2: SPSegmentedControlCell = createCell("", wihImage: img2)
        modeChangeSwitch.isRoundedFrame = true

       /* var tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTap(_:)))
        viewSwicthCOntrol.addGestureRecognizer(tapGesture)
        
        
        var gestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeHandler(_:)))
        gestureRecognizer.direction = [.right, .left, .up, .down]
        viewSwicthCOntrol.addGestureRecognizer(gestureRecognizer)
        
        
        currentUser = UserProfile.getCurrentUser()
        if currentUser.userPreferences {
            var userMode = currentUser.userPreferences.userMode
            if (userMode == "DRIVER") {
                modeChangeSwitch.defaultSelectedIndex = 0
            } else {
                modeChangeSwitch.defaultSelectedIndex = 1
            }
        }*/
        modeCellsArray = [cell1, cell2]
        modeChangeSwitch.add(cells: modeCellsArray)
    }
   
    func viewTap(_ sender: UITapGestureRecognizer?) {
        if modeChangeSwitch.defaultSelectedIndex == 0 {
            modeChangeSwitch.defaultSelectedIndex = 1
        } else {
            modeChangeSwitch.defaultSelectedIndex = 0
        }
        modeChangeSwitch.add(withCells: cellsArray)
    }
    
    func swipeHandler(_ recognizer: UISwipeGestureRecognizer?) {
        if modeChangeSwitch.defaultSelectedIndex == 0 {
            modeChangeSwitch.defaultSelectedIndex = 1
        } else {
            modeChangeSwitch.defaultSelectedIndex = 0
        }
        modeChangeSwitch.add(withCells: cellsArray)
    }
 
    func createCell(_ text: String?, wihImage image: UIImage?) -> SPSegmentedControlCell {
        let cell = SPSegmentedControlCell()
        cell.label.text = text
        cell.imageView.image = image
        return cell
    }
    
    func createImage(withImage imageName: String) -> UIImage {
        return   (UIImage.init(named: imageName))!   
    }

    
    func selectedState(segmentControlCell: SPSegmentedControlCell, forIndex index: Int) {
        print("****selected index is**** \(index)")
        let cellsArray = modeChangeSwitch.cells
        for i in 0..<cellsArray.count {
            let cell = cellsArray[i]
            cell.imageView.image = self.normalImagesArr[i] as? UIImage
        }
        segmentControlCell.imageView.image = self.selectedImagesArr[index] as? UIImage
        self.moveToNewTpScreenProtocalDelegate?.moveToNewTpMethod(withSelectedIndex: index)
    }
    
    func setAppropriateCellAsSelectedByUser(index:Int, withControl:SPSegmentedControl) -> Void {
        print("index is\(index)")
        let cellsArray = withControl.cells
        for i in 0..<cellsArray.count {
            let cell = cellsArray[i]
            cell.imageView.image = self.normalImagesArr[i] as? UIImage
        }
        let segmentControlCell = withControl.cells[index] as SPSegmentedControlCell
        segmentControlCell.imageView.image = self.selectedImagesArr[index] as? UIImage
    }
    func modeChossenBtnTapped() -> Void{
        
    }*/
}
