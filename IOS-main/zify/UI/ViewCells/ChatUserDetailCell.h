//
//  ChatUserDetailCell.h
//  zify
//
//  Created by Anurag S Rathor on 12/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatUserDetailCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *userName;
@property(nonatomic,weak) IBOutlet UILabel *unreadCount;
@property(nonatomic,weak) IBOutlet UILabel *lastMessage;
@property(nonatomic,weak) IBOutlet UILabel *lastMessageDate;
@property (nonatomic,weak) IBOutlet UIImageView *userImage;
@end
