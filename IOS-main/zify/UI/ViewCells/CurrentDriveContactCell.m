//
//  CurrentDriveContactCell.m
//  zify
//
//  Created by Anurag S Rathor on 30/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "CurrentDriveContactCell.h"


@implementation CurrentDriveContactCell
- (void)awakeFromNib {
    [super awakeFromNib];
    UIImage *arrowImage = [UIImage imageNamed:@"left_arrow.png"];
    _minimiseImage.image = [arrowImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_minimiseImage setTintColor:[UIColor colorWithRed:(158.0/255.0) green:(158.0/255.0) blue:(158.0/255.0) alpha:1.0]];
    _minimiseImage.transform = CGAffineTransformMakeRotation(-M_PI_2);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
@end
