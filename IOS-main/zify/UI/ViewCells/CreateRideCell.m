//
//  CreateRideCell.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "CreateRideCell.h"
#import "AppDelegate.h"
#import "LocalisationConstants.h"


@implementation CreateRideCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if( [super initWithStyle:style reuseIdentifier:reuseIdentifier])
        [self initializeControlls];
    
   return self;
 }
-(void)initializeControlls{
    self.frameWidth = [AppDelegate SCREEN_WIDTH];
    self.contentView.frameWidth = [AppDelegate SCREEN_WIDTH];

    _mainView = [[UIView alloc] init];
    _mainView.frame = CGRectMake(5,5,[AppDelegate SCREEN_WIDTH] - 10,395);
    [self addSubview:_mainView];
    _mainView.backgroundColor = [UIColor whiteColor];
    _mainView.layer.cornerRadius = 5;
    _mainView.layer.borderColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0].CGColor;
    _mainView.layer.borderWidth = 1.5;
    
    _vehicleImage = [[UIImageView alloc] init];
    _vehicleImage.frame = CGRectMake(0 ,0  ,_mainView.frameWidth, 200);
    _vehicleImage.backgroundColor = [UIColor whiteColor];
    [_mainView addSubview:_vehicleImage];
    _vehicleImage.layer.cornerRadius = 5;
    _vehicleImage.clipsToBounds = YES;
    _vehicleImage.userInteractionEnabled = YES;
    
    _favouriteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _favouriteButton.frame = CGRectMake(10, 10, 25, 25);
    UIImage *normalImage = [UIImage imageNamed:@"favourite_unselected@3x.png"];
    UIImage *selectedImage = [UIImage imageNamed:@"favourite_selected@3x.png"];
    [_favouriteButton setBackgroundImage:normalImage forState:UIControlStateNormal];
    [_favouriteButton setBackgroundImage:selectedImage forState:UIControlStateSelected];
    _favouriteButton.backgroundColor = [UIColor clearColor];
    [_vehicleImage addSubview:_favouriteButton];
    
    UIImage *verifiedImage = [UIImage imageNamed:@"icn_user_verified@3x.png"];
    CGFloat wid = verifiedImage.size.width;
    CGFloat hei = verifiedImage.size.height;
    _userVerifiedImage = [[UIImageView alloc] init];
    _userVerifiedImage.frame = CGRectMake(_vehicleImage.frameWidth - wid - _favouriteButton.frameX ,_favouriteButton.frameY  ,wid, hei);
    _userVerifiedImage.backgroundColor = [UIColor clearColor];
    _userVerifiedImage.image = verifiedImage;
    [_vehicleImage addSubview:_userVerifiedImage];
    
    _driverInfoView = [[UIView alloc] init];
    _driverInfoView.frame = CGRectMake(0 ,_vehicleImage.frameHeight - 70  ,_vehicleImage.frameWidth, 70);
    _driverInfoView.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:27.0/255.0 blue:26.0/255.0 alpha:1.0];
    [_mainView addSubview:_driverInfoView];
    
    _driverImage = [[UIImageView alloc] init];
    _driverImage.frame = CGRectMake(5, 5, _driverInfoView.frameHeight-10, _driverInfoView.frameHeight-10);
    _driverImage.image = [UIImage imageNamed:@"profilePlaceHolder.png"];
    _driverImage.backgroundColor = [UIColor clearColor];
    [_driverInfoView addSubview:_driverImage];
    
    _driverProfileBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
    _driverProfileBtn.frame = _driverImage.frame;
    [_driverInfoView addSubview:_driverProfileBtn];
    _driverProfileBtn.backgroundColor = [UIColor clearColor];
    
    
    
    //CGFloat width = 80;
    
    _amountView = [[UIView alloc] init];
    _amountView.frame = CGRectMake(_driverInfoView.frameMaxX - _driverImage.frameWidth -_driverImage.frameX - 10 ,_driverImage.frameY ,_driverImage.frameWidth+10, _driverImage.frameHeight);
    _amountView.backgroundColor = [UIColor clearColor];
    [_driverInfoView addSubview:_amountView];
    
    _amountviewSeparator = [[UILabel alloc] init];
    _amountviewSeparator.frame = CGRectMake(_amountView.frameX - 1.5, _amountView.frameY, 1.5, _amountView.frameHeight);
    _amountviewSeparator.backgroundColor = [UIColor colorWithRed:60.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:1.0];
    [_driverInfoView addSubview:_amountviewSeparator];
    
    _driverName = [[UILabel alloc] init];
    _driverName.frame = CGRectMake(_driverImage.frameMaxX+5, _driverImage.frameY, _amountView.frameX - (_driverImage.frameMaxX+5+5), 25);
    _driverName.textColor = [UIColor whiteColor];
    _driverName.text = @"jbjbjds";
    _driverName.backgroundColor = [UIColor clearColor];
    [_driverInfoView addSubview:_driverName];
    
    CGFloat ratingHeight = 16;
    CGRect  ratingFrame = CGRectMake(_driverName.frameX, _driverImage.frameMaxY-ratingHeight, 80, ratingHeight);
    _femaleRatingView = [[RatingView alloc] initWithFrame:ratingFrame];
    _femaleRatingView.backgroundColor = [UIColor clearColor];
    [_driverInfoView addSubview:_femaleRatingView];
    
    _femaleLbl = [[UILabel alloc] init];
    _femaleLbl.frame = CGRectMake(_femaleRatingView.frameMaxX, _femaleRatingView.frameY, _femaleRatingView.frameHeight, _femaleRatingView.frameHeight);
    _femaleLbl.text = @"(F)";
    //_femaleLbl.font = [UIFont systemFontOfSize:9];
    [_femaleLbl setFont:[UIFont fontWithName:@"OpenSans-Regular" size:9]];

    _femaleLbl.textColor = [UIColor whiteColor];
    [_driverInfoView addSubview:_femaleLbl];
    
    _maleRatingView = [[RatingView alloc] initWithFrame:_femaleRatingView.frame];
    _maleRatingView.frameY = _femaleRatingView.frameY-_maleRatingView.frameHeight;
    _maleRatingView.backgroundColor = [UIColor clearColor];
    [_driverInfoView addSubview:_maleRatingView];
  
    _maleLbl = [[UILabel alloc] init];
    _maleLbl.frame = CGRectMake(_maleRatingView.frameMaxX, _maleRatingView.frameY, _femaleLbl.frameHeight, _femaleLbl.frameHeight);
    _maleLbl.text = @"(M)";
    //_maleLbl.font = [UIFont systemFontOfSize:9];
    [_maleLbl setFont:[UIFont fontWithName:@"OpenSans-Regular" size:9]];
    _maleLbl.textColor = [UIColor whiteColor];
    [_driverInfoView addSubview:_maleLbl];
    
    _amount = [[UILabel alloc] init];
    _amount.frame = CGRectMake(0, 0, _amountView.frameWidth, _amountView.frameHeight/3);
    _amount.textAlignment = NSTextAlignmentRight;
    _amount.text = @"$54.00";
    _amount.textColor = [UIColor whiteColor];
    [_amountView addSubview:_amount];
    
    _availableSeats = [[UILabel alloc] init];
    _availableSeats.frame = _amount.frame;
    _availableSeats.frameY = _amount.frameMaxY;
    _availableSeats.textAlignment = NSTextAlignmentRight;
    _availableSeats.text = @"5";
    _availableSeats.textColor = [UIColor whiteColor];
    [_amountView addSubview:_availableSeats];
    
    _seatsTextLbl = [[UILabel alloc] init];
    _seatsTextLbl.frame = _amount.frame;
    _seatsTextLbl.frameY = _availableSeats.frameMaxY;
    _seatsTextLbl.textAlignment = NSTextAlignmentRight;
    _seatsTextLbl.text = @"Seats";
    _seatsTextLbl.textColor = [UIColor grayColor];
    [_amountView addSubview:_seatsTextLbl];
    _availableSeats.frameY = _seatsTextLbl.frameY - _availableSeats.frameHeight +3;
    
    _distanceView = [[UIView alloc] init];
    _distanceView.frame = CGRectMake(0, _vehicleImage.frameMaxY, _vehicleImage.frameWidth, 70);
    _distanceView.backgroundColor = [UIColor whiteColor];
    [_mainView addSubview:_distanceView];
    
    CGFloat distanceSubViewWidth = _distanceView.frameWidth/3;
    CGFloat padd = 20;
    
    _distanceViewSubView1 = [[UIView alloc] init];
    _distanceViewSubView1.frame = CGRectMake(0, 0, distanceSubViewWidth - padd, _distanceView.frameHeight);
    _distanceViewSubView1.backgroundColor = [UIColor clearColor];
    [_distanceView addSubview:_distanceViewSubView1];
    
    CGFloat imageWidth = 15;
    _leftJourneyImageView = [[UIImageView alloc] init];
    _leftJourneyImageView.image = [UIImage imageNamed:@"flag.png"];
    _leftJourneyImageView.frame = CGRectMake(_driverImage.frameX, _distanceViewSubView1.frameHeight - imageWidth, imageWidth, imageWidth);
    _leftJourneyImageView.backgroundColor = [UIColor clearColor];
    [_distanceViewSubView1 addSubview:_leftJourneyImageView];
    
    CGFloat padd1 = 5;
    _leftUnderlineLbl = [[UILabel alloc] init];
    _leftUnderlineLbl.frame = CGRectMake(_leftJourneyImageView.frameMaxX+padd1, _leftJourneyImageView.frameMaxY-1 , _distanceViewSubView1.frameWidth - (_leftJourneyImageView.frameMaxX+2*padd1), 1);
    _leftUnderlineLbl.text = @"-------------------";
    _leftUnderlineLbl.textColor = [UIColor blueColor];
    _leftUnderlineLbl.backgroundColor = [UIColor clearColor];
    [_distanceViewSubView1 addSubview:_leftUnderlineLbl];
    
    _pickupDeltaLbl = [[UILabel alloc] init];
    _pickupDeltaLbl.frame = _leftUnderlineLbl.frame;
    _pickupDeltaLbl.frameHeight = 25;
    _pickupDeltaLbl.frameY = 0;
    _pickupDeltaLbl.textAlignment = NSTextAlignmentCenter;
    [_distanceViewSubView1 addSubview:_pickupDeltaLbl];
    _pickupDeltaLbl.backgroundColor = [UIColor clearColor];
    
    _leftManImageView = [[UIImageView alloc] init];
    _leftManImageView.frame = _pickupDeltaLbl.frame;
    _leftManImageView.frameHeight = _leftUnderlineLbl.frameY - _pickupDeltaLbl.frameMaxY - 6;
    _leftManImageView.frameWidth = _leftManImageView.frameHeight;
    _leftManImageView.frameY = _pickupDeltaLbl.frameMaxY + 3;
    _leftManImageView.frameX = _pickupDeltaLbl.frameX + (_pickupDeltaLbl.frameWidth - _leftManImageView.frameWidth)/2;
    _leftManImageView.backgroundColor = [UIColor clearColor];
    [_distanceViewSubView1 addSubview:_leftManImageView];
    _leftManImageView.image = [UIImage imageNamed:@"pedestrian-walking.png"];
   
    
    _distanceViewSubView3 = [[UIView alloc] init];
    _distanceViewSubView3.frame = _distanceViewSubView1.frame;
    _distanceViewSubView3.backgroundColor = [UIColor clearColor];
    _distanceViewSubView3.frameX = _distanceView.frameWidth - _distanceViewSubView3.frameWidth;
    [_distanceView addSubview:_distanceViewSubView3];
    
    _rightJourneyImageView = [[UIImageView alloc] init];
    _rightJourneyImageView.frame = _leftJourneyImageView.frame;
    _rightJourneyImageView.frameX = _distanceViewSubView3.frameWidth - _rightJourneyImageView.frameWidth - _leftJourneyImageView.frameX;
    _rightJourneyImageView.backgroundColor = [UIColor clearColor];
    _rightJourneyImageView.image = [UIImage imageNamed:@"racing-flag.png"];

    [_distanceViewSubView3 addSubview:_rightJourneyImageView];
    
    _rightUnderlineLbl = [[UILabel alloc] init];
    _rightUnderlineLbl.frame = _leftUnderlineLbl.frame;
    _rightUnderlineLbl.frameX = _rightJourneyImageView.frameX - _rightUnderlineLbl.frameWidth -  padd1;
    _rightUnderlineLbl.text = @"-------------------";
    _rightUnderlineLbl.textColor = [UIColor blueColor];
    _rightUnderlineLbl.backgroundColor = [UIColor clearColor];
    [_distanceViewSubView3 addSubview:_rightUnderlineLbl];
    
    _dropDeltaLbl = [[UILabel alloc] init];
    _dropDeltaLbl.frame = _pickupDeltaLbl.frame;
    _dropDeltaLbl.frameX = _rightUnderlineLbl.frameX;
    _dropDeltaLbl.frameY = 0;
    _dropDeltaLbl.textAlignment = NSTextAlignmentCenter;
    [_distanceViewSubView3 addSubview:_dropDeltaLbl];
    _dropDeltaLbl.backgroundColor = [UIColor clearColor];
    
    _rightmanImageView = [[UIImageView alloc] init];
    _rightmanImageView.frame = _leftManImageView.frame;
    _rightmanImageView.frameY = _dropDeltaLbl.frameMaxY + 3;
    _rightmanImageView.frameX = _dropDeltaLbl.frameX +(_dropDeltaLbl.frameWidth - _rightmanImageView.frameWidth)/2;
    _rightmanImageView.backgroundColor = [UIColor whiteColor];
    [_distanceViewSubView3 addSubview:_rightmanImageView];
    _rightmanImageView.image = [UIImage imageNamed:@"pedestrian-walking.png"];

    _distanceViewSubView2 = [[UIView alloc] init];
    _distanceViewSubView2.frame = _distanceViewSubView1.frame;
    _distanceViewSubView2.backgroundColor = [UIColor clearColor];
    _distanceViewSubView2.frameX = _distanceViewSubView1.frameMaxX;
    _distanceViewSubView2.frameWidth = _distanceViewSubView3.frameX - _distanceViewSubView2.frameX;
    [_distanceView addSubview:_distanceViewSubView2];
    _distanceViewSubView2.backgroundColor = [UIColor clearColor];
    
    _distance = [[UILabel alloc] init];
    _distance.frame = CGRectMake(5, 0, _distanceViewSubView2.frameWidth-10, _distanceViewSubView2.frameHeight);
    _distance.backgroundColor = [UIColor clearColor];
    _distance.textAlignment = NSTextAlignmentCenter;
    _distance.text = @"100 Kms";
    [_distanceViewSubView2 addSubview:_distance];
    [self addLayerForDistanceView:_distance];
    
    CGFloat sourceIconImageHeight = 14;
    
    _journeyDetailsView = [[UIView alloc] init];
    _journeyDetailsView.frame = CGRectMake(0, _distanceView.frameMaxY, _distanceView.frameWidth, _distanceView.frameHeight);
    _journeyDetailsView.frameHeight = 75;
    _journeyDetailsView.backgroundColor = [UIColor whiteColor];
    [_mainView addSubview:_journeyDetailsView];
    
    
    
    _srcImage2 = [[UIImageView alloc] init];
    _srcImage2.frame = CGRectMake(_driverImage.frameX, 5, sourceIconImageHeight, sourceIconImageHeight);
    _srcImage2.backgroundColor = [UIColor whiteColor];
    _srcImage2.layer.borderColor = [UIColor orangeColor].CGColor;
    _srcImage2.layer.borderWidth =  2.0;
    _srcImage2.layer.cornerRadius = _srcImage2.frameHeight/2;
    _srcImage2.clipsToBounds = YES;
    [_journeyDetailsView addSubview:_srcImage2];
    
    
    
    _destImage2 = [[UIImageView alloc] init];
    _destImage2.frame = CGRectMake(_srcImage2.frameX, _journeyDetailsView.frameHeight - sourceIconImageHeight - _srcImage2.frameY, sourceIconImageHeight, sourceIconImageHeight);
    _destImage2.backgroundColor = [UIColor clearColor];
    _destImage2.image = [[AppDelegate getAppDelegateInstance] getNewImageWithOldImage:[UIImage imageNamed:@"loc_123.png"] withNewColor:[UIColor lightGrayColor]];
    [_journeyDetailsView addSubview:_destImage2];
    
    
    _viewMapDetails = [[UIView alloc] initWithFrame:_distanceViewSubView3.frame];
    _viewMapDetails.frameHeight = _journeyDetailsView.frameHeight;
    _viewMapDetails.backgroundColor = [UIColor clearColor];
    [_journeyDetailsView addSubview:_viewMapDetails];
    
    [self addleftSeparatorForViewMap:_viewMapDetails];
    
    
    _viewLbl = [[UILabel alloc] init];
    _viewLbl.frame = CGRectMake(0, _leftseparatorForMap.frameMaxY-20, _viewMapDetails.frameWidth, 20);
    _viewLbl.textAlignment = NSTextAlignmentCenter;
    _viewLbl.text =  NSLocalizedString(TS_DRIVES_LBL_VIEWROUTE, nil);
    _viewLbl.textColor = [UIColor colorWithRed:109.0/255.0 green:109.0/255.0 blue:109.0/255.0 alpha:1.0];
    _viewLbl.backgroundColor = [UIColor clearColor];
    [_viewMapDetails addSubview:_viewLbl];
    
    UIImageView *mapView = [[UIImageView alloc] init];
    mapView.frame = CGRectMake(0, 0, 100, _viewMapDetails.frameHeight - _viewLbl.frameHeight);
    mapView.frameY = _leftseparatorForMap.frameY+ 5;
    mapView.frameHeight = _leftseparatorForMap.frameHeight - _viewLbl.frameHeight - 10;
    mapView.frameWidth = mapView.frameHeight;
    mapView.frameX = (_viewLbl.frameWidth - mapView.frameWidth)/2;
    [_viewMapDetails addSubview:mapView];
    mapView.image = [UIImage imageNamed:@"map.png"];
    
    _viewRouteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _viewRouteButton.frame = CGRectMake(0, 0, _viewMapDetails.frameWidth, _viewMapDetails.frameHeight);
    _viewRouteButton.backgroundColor = [UIColor clearColor];
    [_viewMapDetails addSubview:_viewRouteButton];
    
    
    _srcAddress = [[UILabel alloc] init];
    _srcAddress.frame = CGRectMake(_srcImage2.frameMaxX + 5, 3, _viewMapDetails.frameX - _srcImage2.frameMaxX - 10, 25);
    _srcAddress.numberOfLines = 2;
    _srcAddress.backgroundColor = [UIColor clearColor];
    [_journeyDetailsView addSubview:_srcAddress];
    
    _destAddress = [[UILabel alloc] init];
    _destAddress.frame = _srcAddress.frame;
    _destAddress.frameY = _journeyDetailsView.frameHeight - _destAddress.frameHeight - _srcAddress.frameY;
    _destAddress.numberOfLines = 2;
    _destAddress.backgroundColor = [UIColor clearColor];
    [_journeyDetailsView addSubview:_destAddress];
    
    _srcImage2.frameY = _srcAddress.frameY + (_srcAddress.frameHeight - _srcImage2.frameHeight)/2;
    _destImage2.frameY = _destAddress.frameY + (_destAddress.frameHeight - _destImage2.frameHeight)/2;
    
    _lineSeparator = [[UILabel alloc] init];
    _lineSeparator.frame = CGRectMake(0, _srcImage2.frameMaxY+2, 1.5, _destImage2.frameY - (_srcImage2.frameMaxY+2+2));
    _lineSeparator.backgroundColor = [UIColor clearColor];
    _lineSeparator.frameX = _srcImage2.frameX +( _srcImage2.frameWidth - _lineSeparator.frameWidth)/2;
    [_journeyDetailsView addSubview:_lineSeparator];
    _lineSeparator.text = @"-------------------";
    _lineSeparator.textColor = [UIColor blueColor];
    _lineSeparator.numberOfLines = 0;
    _lineSeparator.lineBreakMode = NSLineBreakByCharWrapping;

    _bottomView = [[UIView alloc] init];
    _bottomView.frame = CGRectMake(0, _journeyDetailsView.frameMaxY, _vehicleImage.frameWidth, 50);
    _bottomView.backgroundColor = [UIColor whiteColor];
    [_mainView addSubview:_bottomView];
    
    _departureDate = [[UILabel alloc] init];
    _departureDate.frame = CGRectMake(_driverImage.frameX, 0, 100, _bottomView.frameHeight);
    _departureDate.backgroundColor = [UIColor clearColor];
    _departureDate.textColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    [_bottomView addSubview:_departureDate];
    
    CGFloat btnWidth = 100;
    _requestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _requestButton.frame = CGRectMake(_bottomView.frameWidth - btnWidth - _departureDate.frameX , 5, 100, _bottomView.frameHeight - 10);
    _requestButton.layer.cornerRadius = _requestButton.frameHeight/2;
    _requestButton.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:69.0/255.0 blue:59.0/255.0 alpha:1.0];
    NSString *reqStr = NSLocalizedString(MS_CREATERIDE_BTN_REQUEST, nil);
    [_requestButton setTitle:reqStr forState:UIControlStateNormal];
    [_requestButton setTitle:reqStr forState:UIControlStateSelected];
    [_requestButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_requestButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

    [_bottomView addSubview:_requestButton];
    
    
    
    CGFloat chatImgHeight = 25;
    _chatImage = [[UIImageView alloc] init];
    _chatImage.frame = CGRectMake(_requestButton.frameX - chatImgHeight - 10, _requestButton.frameY, chatImgHeight, chatImgHeight);
    _chatImage.frameY = (_bottomView.frameHeight - _chatImage.frameHeight)/2;
    _chatImage.backgroundColor = [UIColor clearColor];
    UIImage *chatImage = [UIImage imageNamed:@"chat22.png"];
    _chatImage.image = chatImage;
    [_bottomView addSubview:_chatImage];
    

    _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _chatButton.frame = CGRectMake(_chatImage.frameX, 0, _chatImage.frameWidth, _bottomView.frameHeight);
    _chatButton.backgroundColor = [UIColor clearColor];
    [_bottomView addSubview:_chatButton];
    _departureDate.frameWidth = _chatButton.frameX - _departureDate.frameX;
    
    [self addLayerForView:_distanceViewSubView2];
    
    [self addbottomLayerForJourneyView:_journeyDetailsView];
    _departureDate.numberOfLines = 2;
    //Default set for Iphone 6
    
    CGFloat driverNameFont = 15;
    CGFloat malelblFont = 11;
    CGFloat costFont = 16;
    CGFloat seatAvailFont = 17;
    CGFloat seatsLblFont = 14;
    CGFloat pickupLblFont = 12;
    CGFloat distanceLblFont = 14;
    CGFloat srcAddFont = 12.5;
    CGFloat viewLblFont = 12;
    CGFloat reqBtnFont = 15;
    CGFloat dropDateLblFont = 14;
    if(IS_IPHONE_5) {
        driverNameFont = 14;
        malelblFont = 10;
        costFont = 14;
        seatAvailFont = 15;
        seatsLblFont = 13;
         pickupLblFont = 11;
         distanceLblFont = 14;
         srcAddFont = 13;
         viewLblFont = 12;
         reqBtnFont = 14;
         dropDateLblFont = 12;
    }


    _driverName.font = [UIFont fontWithName:MediumFont size:driverNameFont];
    _maleLbl.font = [UIFont fontWithName:NormalFont size:malelblFont];
    _femaleLbl.font = [UIFont fontWithName:NormalFont size:malelblFont];
    _driverName.font = [UIFont fontWithName:MediumFont size:driverNameFont];
    _amount.font = [UIFont fontWithName:MediumFont size:costFont];
    _availableSeats.font = [UIFont fontWithName:MediumFont size:seatAvailFont];
    _seatsTextLbl.font = [UIFont fontWithName:NormalFont size:seatsLblFont];
    
    _pickupDeltaLbl.font = [UIFont fontWithName:NormalFont size:pickupLblFont];
    _dropDeltaLbl.font = [UIFont fontWithName:NormalFont size:pickupLblFont];
    _distance.font = [UIFont fontWithName:NormalFont size:distanceLblFont];
    _srcAddress.font = [UIFont fontWithName:NormalFont size:srcAddFont];
    _destAddress.font = [UIFont fontWithName:NormalFont size:srcAddFont];
    _viewLbl.font = [UIFont fontWithName:NormalFont size:viewLblFont];
    _requestButton.titleLabel.font = [UIFont fontWithName:MediumFont size:reqBtnFont];
    _departureDate.font = [UIFont fontWithName:NormalFont size:dropDateLblFont];
//    _leftUnderlineLbl.font = [UIFont systemFontOfSize:10];
//    _rightUnderlineLbl.font = [UIFont systemFontOfSize:10];
    [_leftUnderlineLbl setFont:[UIFont fontWithName:@"OpenSans-Regular" size:10]];
    [_rightUnderlineLbl setFont:[UIFont fontWithName:@"OpenSans-Regular" size:10]];
    [_lineSeparator setFont:[UIFont fontWithName:@"OpenSans-Regular" size:4]];
//    _lineSeparator.font = [UIFont systemFontOfSize:4];
    _leftUnderlineLbl.adjustsFontSizeToFitWidth = NO;
    _leftUnderlineLbl.lineBreakMode = NSLineBreakByTruncatingMiddle;
    
    _rightUnderlineLbl.adjustsFontSizeToFitWidth = NO;
    _rightUnderlineLbl.lineBreakMode = NSLineBreakByTruncatingMiddle;
}
-(void)addLayerForView:(UIView*)view{
    
    CGFloat width = 1;
    CGFloat posY = _pickupDeltaLbl.frameMaxY + 2;
    _leftseparatorDistance = [[UILabel alloc] init];
    _leftseparatorDistance.frame = CGRectMake(_distanceViewSubView1.frameWidth - width/2,posY,width,_distanceView.frameHeight - posY);
    _leftseparatorDistance.backgroundColor = [UIColor colorWithRed:60.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:1.0];
    [_distanceView addSubview:_leftseparatorDistance];

    _rightseparatorDistance = [[UILabel alloc] init];
    _rightseparatorDistance.frame = CGRectMake(_distanceViewSubView2.frameMaxX - width/2, _leftseparatorDistance.frameY,_leftseparatorDistance.frameWidth,_leftseparatorDistance.frameHeight);
    _rightseparatorDistance.backgroundColor = [UIColor colorWithRed:60.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:1.0];
    [_distanceView addSubview:_rightseparatorDistance];
    
    CGFloat paddy = 5;
    CGFloat width1  =  _pickupDeltaLbl.frameHeight - 2*paddy;

    _srcImage1 = [[UIImageView alloc] init];
    _srcImage1.frame = CGRectMake(_leftseparatorDistance.frameX-width1/2, _pickupDeltaLbl.frameY+paddy, width1, width1);
    _srcImage1.backgroundColor = [UIColor whiteColor];
    _srcImage1.layer.cornerRadius = _srcImage1.frameHeight/2;
    _srcImage1.layer.borderColor = [UIColor orangeColor].CGColor;
    _srcImage1.layer.borderWidth = 2.0;
    [_distanceView addSubview:_srcImage1];
    
    _srcImage1.frameY = _leftseparatorDistance.frameY - _srcImage1.frameHeight - 5;
    
    _destImage1 = [[UIImageView alloc] init];
    _destImage1.frame = _srcImage1.frame;
    _destImage1.frameX  = _rightseparatorDistance.frameX - width1/2;
    _destImage1.image = [[AppDelegate getAppDelegateInstance] getNewImageWithOldImage:[UIImage imageNamed:@"loc_123.png"] withNewColor:[UIColor lightGrayColor]];
    [_distanceView addSubview:_destImage1];

}
-(void)addleftSeparatorForViewMap:(UIView *)view{
    
    CGFloat width = 1;
    
    _leftseparatorForMap = [[UILabel alloc] init];
    _leftseparatorForMap.frame = CGRectMake(_viewMapDetails.frameX - width/2, _srcAddress.frameY + 5, width, _destAddress.frameMaxY - _srcAddress.frameY - 5-5);
    _leftseparatorForMap.frameY = _srcAddress.frameY + 5;
    _leftseparatorForMap.frameHeight = _viewMapDetails.frameHeight - _leftseparatorForMap.frameY - 10;
    _leftseparatorForMap.frameY = (_journeyDetailsView.frameHeight - _leftseparatorForMap.frameHeight)/2;
    
    _leftseparatorForMap.backgroundColor = [UIColor colorWithRed:60.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:1.0];
    [_journeyDetailsView addSubview:_leftseparatorForMap];
    
    
    
}
-(void)addLayerForDistanceView:(UIView*)view{
    
    UILabel *bottomSeparartor = [[UILabel alloc] init];
    bottomSeparartor.frame = CGRectMake(0,view.frameHeight - 1,view.frameWidth,1);
    bottomSeparartor.backgroundColor = [UIColor redColor];
    [view addSubview:bottomSeparartor];
}
-(void)addbottomLayerForJourneyView:(UIView*)view{
    CGFloat height = 1.5;
    CALayer *border = [CALayer layer];
    border.borderColor = [UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0].CGColor;
    border.frame = CGRectMake(0, view.frame.size.height - height, view.frame.size.width, height);
    border.borderWidth = height;
    [view.layer addSublayer:border];
    view.layer.masksToBounds = YES;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

}
-(void)drawDashedLine:(UILabel *)lbl{
    CAShapeLayer *dashedLine = [CAShapeLayer layer];
    [dashedLine setFrame:lbl.frame];
    
    // Setup the path
    CGMutablePathRef thePath = CGPathCreateMutable();
    CGPathMoveToPoint(thePath, NULL, 0, 10);
    CGPathAddLineToPoint(thePath, NULL, lbl.frameWidth,10);
    dashedLine.path = thePath;
    CGPathRelease(thePath);
    
    
    [dashedLine setLineDashPattern: [NSArray arrayWithObjects:[NSNumber numberWithFloat:15], nil]];
    dashedLine.lineWidth = 1.0f;
    dashedLine.strokeColor = [[UIColor greenColor] CGColor];
    
    [lbl.layer addSublayer:dashedLine];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
