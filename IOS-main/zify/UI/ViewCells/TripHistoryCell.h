//
//  TripHistoryCell.h
//  zify
//
//  Created by Anurag S Rathor on 24/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripRideDriveDetail.h"
#import "DriverDetail.h"

enum TripHistoryType{
    RIDE,DRIVE
};

@interface TripHistoryCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *referenceNumber;
@property (nonatomic,weak) IBOutlet UILabel *source;
@property (nonatomic,weak) IBOutlet UILabel *destination;
@property (nonatomic,weak) IBOutlet UILabel *tripDate;
@property (nonatomic,weak) IBOutlet UILabel *totalDistance;
@property (nonatomic,weak) IBOutlet UILabel *totalAmount;
@property (nonatomic,weak) TripRideDriveDetail *driveDetail;
@property (nonatomic,weak) DriverDetail *driverDetail;
@property (nonatomic,weak) NSArray *driveRiders;
@property (nonatomic) enum TripHistoryType tripType;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDestinationTitle;
@end
