//
//  MenuProfileCell.h
//  zify
//
//  Created by Anurag S Rathor on 09/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuProfileCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *userName;
@property(nonatomic,weak) IBOutlet UILabel *verificationMsg;
@property(nonatomic,weak) IBOutlet UILabel *completenessPercent;
@property(nonatomic,weak) IBOutlet UIImageView *verificationImage;
@property(nonatomic,weak) IBOutlet UIImageView *userImage;
@property(nonatomic,weak) IBOutlet UIProgressView *completenessProgress;
@property(nonatomic) float profileCompleteness;
-(void)startProgress;
@end
