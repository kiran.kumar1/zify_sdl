//
//  ContactUserDetailCell.m
//  zify
//
//  Created by Anurag S Rathor on 05/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "ContactUserDetailCell.h"

@implementation ContactUserDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIImage *chatImage = [UIImage imageNamed:@"icn_user_chat.png"];
    self.chatButtonImage.image = [chatImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.chatButtonImage setTintColor:[UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0]];
    UIImage *callImage = [UIImage imageNamed:@"icn_user_call.png"];
    self.callButtonImage.image = [callImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.callButtonImage setTintColor:[UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1.0]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
