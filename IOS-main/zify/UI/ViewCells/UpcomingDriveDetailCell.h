//
//  UpcomingDriveDetailCell.h
//  zify
//
//  Created by Anurag S Rathor on 05/02/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingDriveDetailCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UIView *srcImageView;
@property(nonatomic,weak) IBOutlet UIImageView *destImageView;
@property(nonatomic,weak) IBOutlet UILabel *source;
@property(nonatomic,weak) IBOutlet UILabel *destination;
@property(nonatomic,weak) IBOutlet UILabel *tripDate;
@property(nonatomic,weak) IBOutlet UILabel *tripTime;
@property(nonatomic,weak) IBOutlet UILabel *driveId;
@property(nonatomic,weak) IBOutlet UIButton *contactButton;
@property(nonatomic,weak) IBOutlet UIButton *cancelButton;
@property(nonatomic,weak) IBOutlet UIView *cancelButtonSeparator;
@property(nonatomic,weak) IBOutlet UILabel *onTripLabel;
@property(nonatomic,weak) IBOutlet UILabel *tapToConnentLbl;
@property (nonatomic,strong) IBOutletCollection(UIImageView) NSArray *riderImages;
@property (nonatomic,strong) IBOutletCollection(UIButton) NSArray *riderImageButtons;
@property (nonatomic,weak) NSArray *driveRiders;
@end
