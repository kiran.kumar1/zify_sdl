//
//  GuestMenuProfileCell.h
//  zify
//
//  Created by Anurag S Rathor on 19/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestMenuProfileCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UIImageView *profilePlaceholder;
@property(nonatomic,weak) IBOutlet UIButton *loginButton;
@property(nonatomic,weak) IBOutlet UILabel *welcomeText;
@end
