//
//  CreateRideCell.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"
#import "UIView+iOS.h"

@interface CreateRideCell : UITableViewCell


@property(nonatomic,strong)  UIView *mainView;
@property(nonatomic,strong)  UIButton *favouriteButton;
@property(nonatomic,strong)  UIImageView *vehicleImage;
@property(nonatomic,strong)  UIImageView *userVerifiedImage;
@property(nonatomic,strong)  UIView *driverInfoView;
@property(nonatomic,strong)  UIImageView *driverImage;
@property(nonatomic,strong)  UIButton *driverProfileBtn;

@property(nonatomic,strong)  UILabel *driverName;

@property(nonatomic,strong)UIView *amountView;
@property(nonatomic,strong) UILabel *amountviewSeparator;
@property(nonatomic,strong)  UILabel *amount;
@property(nonatomic,strong)  RatingView *maleRatingView;
@property(nonatomic,strong)  RatingView *femaleRatingView;
@property(nonatomic,strong)  UILabel *femaleLbl;
@property(nonatomic,strong)  UILabel *maleLbl;

@property(nonatomic,strong) UIView *distanceView;
@property(nonatomic,strong) UILabel *leftseparatorDistance;
@property(nonatomic,strong) UILabel *rightseparatorDistance;
@property(nonatomic,strong) UIView *distanceViewSubView1;
@property(nonatomic,strong) UIView *distanceViewSubView2;
@property(nonatomic,strong) UIView *distanceViewSubView3;

@property(nonatomic,strong)  UIImageView *leftJourneyImageView;
@property(nonatomic,strong)  UILabel *leftUnderlineLbl;
@property(nonatomic,strong)  UIImageView *leftManImageView;
@property(nonatomic,strong)  UIImageView *rightJourneyImageView;
@property(nonatomic,strong)  UILabel *rightUnderlineLbl;
@property(nonatomic,strong)  UIImageView *rightmanImageView;



@property(nonatomic,strong)  UIImageView *srcImage1;
@property(nonatomic,strong)  UIImageView *destImage1;
@property(nonatomic,strong)  UIImageView *srcImage2;
@property(nonatomic,strong)  UIImageView *destImage2;

@property(nonatomic,strong)  UILabel *availableSeats;
@property(nonatomic,strong)  UIView *journeyDetailsView;
@property(nonatomic,strong)  UIView *viewMapDetails;
@property(nonatomic,strong) UILabel *leftseparatorForMap;

@property(nonatomic,strong)  UILabel *lineSeparator;
@property(nonatomic,strong)  UILabel *srcAddress;
@property(nonatomic,strong)  UILabel *destAddress;
@property(nonatomic,strong)  UIButton *viewRouteButton;
@property(nonatomic,strong)  UILabel *departureDate;
@property(nonatomic,strong)  UILabel *departureTime;
@property(nonatomic,strong)  UILabel *distance;
@property(nonatomic,strong)  UILabel *distanceUnit;
@property(nonatomic,strong)  UIImageView *chatImage;
@property(nonatomic,strong)  UIButton *chatButton;
@property(nonatomic,strong)  UIButton *requestButton;
@property(nonatomic,strong)  UIView *bottomView;
@property(nonatomic,strong)  UILabel *pickupDeltaLbl;
@property(nonatomic,strong)  UILabel *dropDeltaLbl;
@property(nonatomic,strong)  UILabel *seatsTextLbl;
@property(nonatomic,strong)  UILabel *viewLbl;

 

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
