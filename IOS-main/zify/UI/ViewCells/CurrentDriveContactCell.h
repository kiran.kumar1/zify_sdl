//
//  CurrentDriveContactCell.h
//  zify
//
//  Created by Anurag S Rathor on 30/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentDriveContactCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIImageView *minimiseImage;
@property(nonatomic,weak) IBOutlet UILabel *userName;
@property (nonatomic,weak) IBOutlet UIImageView *userImage;
@property (nonatomic,weak) IBOutlet UIView *userDetailsView;
@property (nonatomic,weak) IBOutlet UIView *contactUserView;
@property (nonatomic,weak) IBOutlet UIButton *chatButton;
@property (nonatomic,weak) IBOutlet UIButton *callButton;
@property (nonatomic,weak) IBOutlet UIView *separatorLine;
@end
