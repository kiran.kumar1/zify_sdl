//
//  RideRequestCell.h
//  zify
//
//  Created by Anurag S Rathor on 26/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"

@interface RideRequestCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UIImageView *userImage;
@property(nonatomic,weak) IBOutlet UILabel *name;
@property(nonatomic,weak) IBOutlet UIImageView *userVerifiedImage;
@property(nonatomic,weak) IBOutlet UILabel *tripDate;
@property(nonatomic,weak) IBOutlet UILabel *tripTime;
@property(nonatomic,weak) IBOutlet UILabel *source;
@property(nonatomic,weak) IBOutlet UILabel *destination;
@property(nonatomic,weak) IBOutlet UIButton *declineButton;
@property(nonatomic,weak) IBOutlet UIButton *acceptButton;
@property(nonatomic,weak) IBOutlet RatingView *maleRatingView;
@property(nonatomic,weak) IBOutlet RatingView *femaleRatingView;
@property(nonatomic,weak) IBOutlet UIButton *chatButton;

@property(nonatomic,weak) IBOutlet UILabel *lblSourceText;
@property(nonatomic,weak) IBOutlet UILabel *lblDestinationText;

@end
