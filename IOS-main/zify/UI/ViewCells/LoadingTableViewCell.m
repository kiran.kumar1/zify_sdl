//
//  LoadingTableViewCell.m
//  BottomRefresh
//
//  Created by Robert Ryan on 5/26/15.
//  Copyright (c) 2015 Robert Ryan. All rights reserved.
//
//  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
//  http://creativecommons.org/licenses/by-sa/4.0/

#import "LoadingTableViewCell.h"
#import "UIView+iOS.h"

@implementation LoadingTableViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if( [super initWithStyle:style reuseIdentifier:reuseIdentifier])
        [self initializeControlls];
    
    return self;
}
-(void)initializeControlls{
    
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndicatorView.frame = CGRectMake(10, 0, 25, 25);
    _activityIndicatorView.frameY = (self.frameHeight - _activityIndicatorView.frameHeight)/2;
    // If you need custom color, use color property
    // activityIndicator.color = yourDesirableColor;
    [self addSubview:_activityIndicatorView];
    [_activityIndicatorView startAnimating];
    
    _lblLoading = [[UILabel alloc] init];
    _lblLoading.frame = CGRectMake(_activityIndicatorView.frameMaxX, 0, 150, self.frameHeight);
    _lblLoading.text = @"Loading";
    [_lblLoading setFont:[UIFont fontWithName:@"OpenSans-Regular" size:14]];
    [self addSubview:_lblLoading];
}
// this is intentionally blank

@end
