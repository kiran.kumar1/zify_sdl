//
//  NotificationViewCell.h
//  zify
//
//  Created by Anurag S Rathor on 06/12/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface NotificationViewCell : UITableViewCell
@property(nonatomic,strong)  UIView *mainView;
@property(nonatomic,strong)  UIImageView *notificationImageView;
@property(nonatomic,strong)  UILabel *titleLbl;
@property(nonatomic,strong)  UILabel *contentLbl;
@property(nonatomic,strong)  UIImageView *rightImageView;

-(void)setDataForCell:(NSDictionary *)infoDict;

@end
