//
//  WithdrawTableViewCell.swift
//  zify
//
//  Created by Anurag Rathor on 13/08/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class WalletTableViewCell: UITableViewCell {
    @IBOutlet weak var contentBgView:UIView!
    @IBOutlet weak var leftIconImageView:UIImageView!
    @IBOutlet weak var lblContent:UILabel!
    @IBOutlet weak var rightIconImageView:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
