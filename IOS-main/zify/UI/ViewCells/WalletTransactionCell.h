//
//  WalletTransactionCell.h
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletTransactionCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *transactionId;
@property(nonatomic,weak) IBOutlet UILabel *zifyPoints;
@property(nonatomic,weak) IBOutlet UILabel *transactionType;
@end
