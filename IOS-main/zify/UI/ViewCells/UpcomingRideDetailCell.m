//
//  UpcomingRideDetailCell.m
//  zify
//
//  Created by Anurag S Rathor on 04/02/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UpcomingRideDetailCell.h"
#import "AppDelegate.h"
#import "LocalisationConstants.h"
#import "LocalisationConstants.h"
#import "UIImage+ProportionalFill.h"
#import "TripRide.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDWebImageManager.h>
#import "ChatManager.h"
#import "CurrentLocale.h"
#import "zify-Swift.h"


@implementation UpcomingRideDetailCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if( [super initWithStyle:style reuseIdentifier:reuseIdentifier])
        [self initializeControlls];
    
    return self;
}
-(void)initializeControlls{
    self.frameWidth = [AppDelegate SCREEN_WIDTH];
    self.contentView.frameWidth = [AppDelegate SCREEN_WIDTH];
    CGFloat cellHeight = 250;
    self.contentView.frameHeight = cellHeight;
    
    self.backgroundColor = [UIColor clearColor];
    self.backgroundView = nil;
    self.backgroundView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _mainView = [[UIView alloc] init];
    _mainView.frame = CGRectMake(10,5,[AppDelegate SCREEN_WIDTH] - 20,cellHeight);
    [self addSubview:_mainView];
    _mainView.backgroundColor = [UIColor whiteColor];
    _mainView.layer.cornerRadius = 5;
    _mainView.layer.borderColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0].CGColor;
    _mainView.layer.borderWidth = 1.5;
    
    _coloredView = [[UILabel alloc] init];
    _coloredView.frame = CGRectMake(0, 0, 7, _mainView.frameHeight);
    _coloredView.backgroundColor = [UIColor purpleColor];
    _coloredView.layer.cornerRadius = 5;
    _coloredView.clipsToBounds = YES;
    
    [self addTopView];
    [self driverInfo];
    [self addJourneyDetailsView];
    [self rideDetailsInfoView];
    [self bottomView];
    [self setFontForAllControls];
    //  [self setDummayDataForAllControls];
    [self addShadowToView:_mainView];
}
-(void)addTopView{
    CGFloat topViewWidth = 150;
    _topView = [[UIView alloc] init];
    _topView.frame = CGRectMake(_mainView.frameWidth - topViewWidth ,0  ,topViewWidth, 30);
    _topView.backgroundColor = [UIColor clearColor];
    [_mainView addSubview:_topView];
    
    CGFloat cancelImgHeight = _topView.frameHeight - 10;
    _deleteView = [[UIView alloc] init];
    _deleteView.frame = CGRectMake(_topView.frameWidth - 35, 0, 35, _topView.frameHeight);
    _deleteView.backgroundColor = [UIColor clearColor];
    [_topView addSubview:_deleteView];
    
    
    _statusSeparator = [[UILabel alloc] init];
    _statusSeparator.frame = CGRectMake(2, _cancelImageView.frameY, 2, _cancelImageView.frameHeight);
    _statusSeparator.backgroundColor = [UIColor whiteColor];
    [_deleteView addSubview:_statusSeparator];
    
    UIImage *cancelImage = [UIImage imageNamed:@"icn_delete.png"];
    cancelImage = [[AppDelegate getAppDelegateInstance] getNewImageWithOldImage:cancelImage withNewColor:[UIColor whiteColor]];
    _cancelImageView = [[UIImageView alloc] init];
    _cancelImageView.frame = CGRectMake(_statusSeparator.frameMaxX+(30 - cancelImgHeight)/2, (_topView.frameHeight - cancelImgHeight)/2, cancelImgHeight, cancelImgHeight);
    _cancelImageView.backgroundColor = [UIColor clearColor];
    _cancelImageView.image = cancelImage;
    [_deleteView addSubview:_cancelImageView];
    
    
    _statusSeparator.frameY = _cancelImageView.frameY;
    _statusSeparator.frameHeight = _cancelImageView.frameHeight;
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _cancelButton.frame = CGRectMake(0, 0, _deleteView.frameWidth, _deleteView.frameHeight);
    [_deleteView addSubview:_cancelButton];
    
    _pendingStatus = [[UILabel alloc] init];
    _pendingStatus.frame = CGRectMake(0, 0, _deleteView.frameX, _topView.frameHeight);
    _pendingStatus.backgroundColor = [UIColor clearColor];
    _pendingStatus.textAlignment = NSTextAlignmentCenter;
    _pendingStatus.textColor = [UIColor whiteColor];
    [_topView addSubview:_pendingStatus];
}

-(void)driverInfo{
    _driverImage = [[UIImageView alloc] init];
    _driverImage.frame = CGRectMake(_coloredView.frameMaxX + 10, _topView.frameMaxY + 5, 50, 50);
    _driverImage.backgroundColor = [UIColor clearColor];
    profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
    _driverImage.image = profileImagePlaceHolder;
    _driverImage.layer.cornerRadius = _driverImage.frameHeight/2;
    _driverImage.clipsToBounds = YES;
    [_mainView addSubview:_driverImage];
    
    CGFloat amountWidth = 65;
    _tripAmount = [[UILabel alloc] init];
    _tripAmount.frame = CGRectMake(_mainView.frameWidth - amountWidth - _driverImage.frameX, _driverImage.frameY, amountWidth, _driverImage.frameHeight);
    _tripAmount.backgroundColor = [UIColor clearColor];
    _tripAmount.textAlignment = NSTextAlignmentRight;
    [_mainView addSubview:_tripAmount];
    
    _driverName = [[UILabel alloc] init];
    _driverName.frame = CGRectMake(_driverImage.frameMaxX+5, _driverImage.frameY, _tripAmount.frameX - (_driverImage.frameMaxX+5) - 5 , _driverImage.frameHeight);
    _driverName.numberOfLines = 2;
    [_mainView addSubview:_driverName];
    _driverName.backgroundColor = [UIColor clearColor];
}
-(void)addJourneyDetailsView{
    _journeyDetailsView = [[UIView alloc] init];
    _journeyDetailsView.frame = CGRectMake(0, _driverImage.frameMaxY, _mainView.frameWidth, 85);
    _journeyDetailsView.backgroundColor = [UIColor clearColor];
    [_mainView addSubview:_journeyDetailsView];
    
    CGFloat sourceIconImageHeight = 25;
    _srcImage = [[UIImageView alloc] init];
    _srcImage.frame = CGRectMake(_driverImage.frameX, 5, sourceIconImageHeight, sourceIconImageHeight);
    _srcImage.backgroundColor = [UIColor clearColor];
    //_srcImage.layer.borderColor = [UIColor orangeColor].CGColor;
    //_srcImage.layer.borderWidth = 2.0;
    _srcImage.layer.cornerRadius = _srcImage.frameHeight/2;
    _srcImage.clipsToBounds = YES;
    _srcImage.image = [UIImage imageNamed:@"entry_small.png"];//@"icn_source_marker.png"];
    [_journeyDetailsView addSubview:_srcImage];
    // _srcImage.frameX = _driverImage.frameX + (_driverName.frameWidth - _srcImage.frameWidth)/2;
    
    _destImage = [[UIImageView alloc] init];
    _destImage.frame = _srcImage.frame;
    _destImage.backgroundColor = [UIColor clearColor];
    UIImage *destinageImage = [UIImage imageNamed:@"exit_small.png"];//@"icn_destination_marker.png"];
    _destImage.image = destinageImage;//[[AppDelegate getAppDelegateInstance] getNewImageWithOldImage:destinageImage withNewColor:[UIColor grayColor]];
    [_journeyDetailsView addSubview:_destImage];
    
    
    CGFloat mapViewWidth = 70;
    _viewMapDetails = [[UIView alloc] initWithFrame:CGRectMake(_mainView.frameWidth - mapViewWidth - _driverImage.frameX, 0, mapViewWidth, _journeyDetailsView.frameHeight)];
    _viewMapDetails.backgroundColor = [UIColor clearColor];
    [_journeyDetailsView addSubview:_viewMapDetails];
    
    _viewRouteLbl = [[UILabel alloc] init];
    _viewRouteLbl.frame = CGRectMake(0, _viewMapDetails.frameHeight-20, _viewMapDetails.frameWidth, 20);
    _viewRouteLbl.textAlignment = NSTextAlignmentCenter;
    _viewRouteLbl.text =  NSLocalizedString(TS_DRIVES_LBL_VIEWROUTE, nil);
    _viewRouteLbl.textColor = [UIColor colorWithRed:109.0/255.0 green:109.0/255.0 blue:109.0/255.0 alpha:1.0];
    _viewRouteLbl.backgroundColor = [UIColor clearColor];
    [_viewMapDetails addSubview:_viewRouteLbl];
    
    _viewMapImageView = [[UIImageView alloc] init];
    _viewMapImageView.frame = CGRectMake(0, 0, 37,29);
    _viewMapImageView.frameX = (_viewMapDetails.frameWidth - _viewMapImageView.frameWidth)/2;
    CGFloat remainHeight = _viewMapDetails.frameHeight - _viewRouteLbl.frameHeight;
    _viewMapImageView.frameY = (remainHeight - _viewMapImageView.frameWidth)/2;
    
    [_viewMapDetails addSubview:_viewMapImageView];
    _viewMapImageView.image = [UIImage imageNamed:@"routeIcon"];
    _viewRouteLbl.frameY = _viewMapImageView.frameMaxY;
    
    
    _viewRouteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _viewRouteButton.frame = CGRectMake(0, 0, _viewMapDetails.frameWidth, _viewMapDetails.frameHeight);
    _viewRouteButton.backgroundColor = [UIColor clearColor];
  //  _viewRouteButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
  //  _viewRouteButton.layer.borderWidth = 1;
   // _viewRouteButton.layer.cornerRadius = 3;

    [_viewMapDetails addSubview:_viewRouteButton];
    
    _lineSeparator = [[UILabel alloc] init];
    _lineSeparator.frame = CGRectMake(0, _srcImage.frameMaxY+2, 1.5, _destImage.frameY - (_srcImage.frameMaxY+2+2));
    _lineSeparator.backgroundColor = [UIColor clearColor];
    _lineSeparator.frameX = _srcImage.frameX +( _srcImage.frameWidth - _lineSeparator.frameWidth)/2;
    [_journeyDetailsView addSubview:_lineSeparator];
    _lineSeparator.backgroundColor = [UIColor grayColor];
    
    addressLblTextHeight = 35;
    
    _srcAddressLbl = [[UILabel alloc] init];
    _srcAddressLbl.frame = CGRectMake(_srcImage.frameMaxX + 5, 3, _viewMapDetails.frameX - _srcImage.frameMaxX - 10, addressLblTextHeight);
    _srcAddressLbl.numberOfLines = 2;
    _srcAddressLbl.backgroundColor = [UIColor clearColor];
    [_journeyDetailsView addSubview:_srcAddressLbl];
    
    _destAddressLbl = [[UILabel alloc] init];
    _destAddressLbl.frame = _srcAddressLbl.frame;
    _destAddressLbl.frameY = _journeyDetailsView.frameHeight - _destAddressLbl.frameHeight - _srcAddressLbl.frameY;
    _destAddressLbl.numberOfLines = 2;
    _destAddressLbl.backgroundColor = [UIColor clearColor];
    [_journeyDetailsView addSubview:_destAddressLbl];
    
    _srcImage.frameY = _srcAddressLbl.frameY + (_srcAddressLbl.frameHeight - _srcImage.frameHeight)/2;
    _destImage.frameY = _destAddressLbl.frameY + (_destAddressLbl.frameHeight - _destImage.frameHeight)/2;
    _lineSeparator.frameY = _srcImage.frameMaxY + 3;
    _lineSeparator.frameHeight = _destImage.frameY - _lineSeparator.frameY - 3;
    
    
}
-(void)rideDetailsInfoView{
    _rideDetailsView = [[UIView alloc] init];
    _rideDetailsView.frame = CGRectMake(0, _journeyDetailsView.frameMaxY, _mainView.frameWidth, 40);
    _rideDetailsView.backgroundColor = [UIColor clearColor];
    [_mainView addSubview:_rideDetailsView];
    
    _rideId = [[UILabel alloc] init];
    _rideId.frame = CGRectMake(_driverImage.frameX, 0, _rideDetailsView.frameWidth/2 - _driverImage.frameX, _rideDetailsView.frameHeight);
    _rideId.backgroundColor = [UIColor clearColor];
    [_rideDetailsView addSubview:_rideId];
    
    UIImage *timeImage =[UIImage imageNamed:@"icn_search_ride_time.png"];
    _clockImageView = [[UIImageView alloc] init];
    _clockImageView.frame = CGRectMake(_rideDetailsView.frameWidth/2, 10, 20, 20);
    _clockImageView.frameY = (_rideDetailsView.frameHeight - _clockImageView.frameHeight)/2;
    _clockImageView.image = timeImage;
    [_rideDetailsView addSubview:_clockImageView];
    
    _tripDateAndTime = [[UILabel alloc] init];
    _tripDateAndTime.frame = CGRectMake(_clockImageView.frameMaxX, 0, _rideDetailsView.frameWidth/2 - _clockImageView.frameWidth , _rideDetailsView.frameHeight);
    _tripDateAndTime.textAlignment = NSTextAlignmentRight;
    _tripDateAndTime.backgroundColor = [UIColor clearColor];
    [_rideDetailsView addSubview:_tripDateAndTime];
    
    [self addBottomborder:_rideDetailsView];
    
}
-(void)addBottomborder:(UIView *)view{
    CALayer *border = [CALayer layer];
    CGFloat borderHeight = 0.5;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, view.frame.size.height - borderHeight, view.frameWidth, 0.5);
    border.borderWidth = borderHeight;
    [view.layer addSublayer:border];
    view.layer.masksToBounds = YES;
}
-(void)bottomView{
    _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _chatButton.frame = CGRectMake(0, _rideDetailsView.frameMaxY, _mainView.frameWidth/2 - 0.5, 40);
    _chatButton.backgroundColor = [UIColor clearColor];
    CGFloat spacing = 5; // the amount of spacing to appear between image and title
    NSString *chatTextStr = NSLocalizedString(CHATS_CONTACTLIST_LBL_CHAT, nil);
    UIImage *chatImage = [UIImage imageNamed:@"chat.png"];
    chatImage = [chatImage imageScaledToFitSize:CGSizeMake(20.0f,20.0f)];
    chatImage = [[AppDelegate getAppDelegateInstance] getNewImageWithOldImage:chatImage withNewColor:[UIColor grayColor]];
    [_chatButton setImage:chatImage forState:UIControlStateNormal];
    [_chatButton setImage:chatImage forState:UIControlStateSelected];
    [_chatButton setTitle:chatTextStr forState:UIControlStateNormal];
    [_chatButton setTitle:chatTextStr forState:UIControlStateSelected];
    [_mainView addSubview:_chatButton];
    _chatButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
    _chatButton.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
    
    
    _contactButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _contactButton.frame = _chatButton.frame;
    _contactButton.frameX = _chatButton.frameMaxX + 1;
    _contactButton.backgroundColor = [UIColor clearColor];
    [_mainView addSubview:_contactButton];
    UIImage *callImage = [UIImage imageNamed:@"icn_trans_user_call.png"];
    callImage = [callImage imageScaledToFitSize:CGSizeMake(20.0f,20.0f)];
    callImage = [[AppDelegate getAppDelegateInstance] getNewImageWithOldImage:callImage withNewColor:[UIColor grayColor]];
    
    NSString *callTextStr = NSLocalizedString(CHATS_CONTACTLIST_LBL_CALL, nil);
    [_contactButton setImage:callImage forState:UIControlStateNormal];
    [_contactButton setImage:callImage forState:UIControlStateSelected];
    [_contactButton setTitle:callTextStr forState:UIControlStateNormal];
    [_contactButton setTitle:callTextStr forState:UIControlStateSelected];
    _contactButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
    _contactButton.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
    
    
    _verticalSeparator = [[UILabel alloc] init];
    _verticalSeparator.frame = CGRectMake(_chatButton.frameMaxX, _chatButton.frameY + 3, 1, _chatButton.frameHeight - 2*3);
    _verticalSeparator.backgroundColor = [UIColor lightGrayColor];
    [_mainView addSubview:_verticalSeparator];
    [self setTextColorForControls];
}
-(void)setFontForAllControls{
    _pendingStatus.font = [UIFont fontWithName:NormalFont size:12];
    _driverName.font = [UIFont fontWithName:NormalFont size:13];
    _tripAmount.font = [UIFont fontWithName:NormalFont size:13];
    _srcAddressLbl.font = [UIFont fontWithName:NormalFont size:12];
    _destAddressLbl.font = [UIFont fontWithName:NormalFont size:12];
    _viewRouteLbl.font = [UIFont fontWithName:NormalFont size:12];
    _rideId.font = [UIFont fontWithName:NormalFont size:12];
    _tripDateAndTime.font = [UIFont fontWithName:NormalFont size:12];
    _chatButton.titleLabel.font = [UIFont fontWithName:NormalFont size:12];
    _contactButton.titleLabel.font = [UIFont fontWithName:NormalFont size:12];
}
-(void)setDummayDataForAllControls{
    _pendingStatus.text = @"Request Pending";
    _driverName.text = @"Kiran Kumar Kota";
    _tripAmount.text = @"$ 7.35";
    _srcAddressLbl.text = @"Gachibj";
    _destAddressLbl.text = @"hsfh yegfyjgt";
    _rideId.text = @"Ride Id:12345678";
    _tripDateAndTime.text = @"Today 12:34 AM";
    _pendingStatus.textColor = [UIColor blackColor];
    //  [self setFramesAfterSettingText];
}

-(void)setTextColorForControls{
    _topView.backgroundColor = [UIColor colorWithRed:67.0/255.0 green:91.0/255.0 blue:108.0/255.0 alpha:1.0];
    _tripAmount.textColor = [UIColor colorWithRed:122.0/255.0 green:71.0/255.0 blue:192.0/255.0 alpha:1.0];
    _rideId.textColor = [UIColor lightGrayColor];
    _viewRouteLbl.textColor = [UIColor lightGrayColor];
    [_chatButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_chatButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
    [_contactButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_contactButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
}
-(void)setFramesAfterSettingText{
    [_pendingStatus sizeToFit];
    _pendingStatus.frameHeight = _topView.frameHeight;
    _pendingStatus.frameX = _deleteView.frameX - _pendingStatus.frameWidth-2;
    _topView.frameWidth = _pendingStatus.frameMaxX + _deleteView.frameWidth+ 2;
    _topView.frameX = _mainView.frameWidth - _topView.frameWidth;
    _pendingStatus.frameX = 0;
    _pendingStatus.frameWidth = _deleteView.frameX;
    [_srcAddressLbl sizeToFit];
    [_destAddressLbl sizeToFit];
    if(_srcAddressLbl.frameHeight > addressLblTextHeight){
        _srcAddressLbl.frameHeight = addressLblTextHeight;
    }
    if(_srcAddressLbl.frameHeight < _srcImage.frameHeight){
        _srcAddressLbl.frameHeight = _srcImage.frameHeight;
    }
    if(_destAddressLbl.frameHeight > addressLblTextHeight){
        _destAddressLbl.frameHeight = addressLblTextHeight;
    }
    if(_destAddressLbl.frameHeight < _srcImage.frameHeight){
        _destAddressLbl.frameHeight = _srcImage.frameHeight;
    }
    if(_srcAddressLbl.frameHeight != addressLblTextHeight){
        _srcAddressLbl.frameY = 5;
        _srcImage.frameY = _srcAddressLbl.frameY + (_srcAddressLbl.frameHeight - _srcImage.frameHeight)/2;
    }
    if(_destAddressLbl.frameHeight != addressLblTextHeight){
        _destAddressLbl.frameY = _journeyDetailsView.frameHeight - _destAddressLbl.frameHeight - 5;
        _destImage.frameY = _destAddressLbl.frameY + (_destAddressLbl.frameHeight - _srcImage.frameHeight)/2;
    }
    
    _lineSeparator.frameY = _srcImage.frameMaxY + 3;
    _lineSeparator.frameHeight = _destImage.frameY - _lineSeparator.frameY -3 - 3;
    _lineSeparator.frameX = _srcImage.frameX + (_srcImage.frameWidth - _lineSeparator.frameWidth)/2;
    
    [_tripDateAndTime sizeToFit];
    _tripDateAndTime.frameHeight = _rideDetailsView.frameHeight;
    CGFloat dateTimeLblOriginalWidth = _mainView.frameWidth/2 - _clockImageView.frameWidth - _driverImage.frameX - 5;
    if(_tripDateAndTime.frameWidth > dateTimeLblOriginalWidth){
        _tripDateAndTime.frameWidth = dateTimeLblOriginalWidth;
    }
    _tripDateAndTime.frameX = _rideDetailsView.frameWidth - _tripDateAndTime.frameWidth - _driverImage.frameX;
    _clockImageView.frameX = _tripDateAndTime.frameX - _clockImageView.frameWidth - 5;
    [self applyToRightCornerRadiusToTopView];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)addShadowToView:(UIView *)v{
    [v.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [v.layer setShadowOpacity:0.8];
    [v.layer setShadowRadius:3.0];
    // [v.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    v.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
}
-(void)setDataForCell:(TripRide *)ride{
    
    self.driverName.text = [[NSString stringWithFormat:@"%@ %@",ride.driverDetail.firstName,ride.driverDetail.lastName] capitalizedString];
    [self.driverImage sd_setImageWithURL:[NSURL URLWithString:ride.driverDetail.profileImgUrl] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
    }];
  //  NSLog(@"sttaus is %@",ride.status);
    if([ride.status caseInsensitiveCompare:@"pending"] == NSOrderedSame){
        _pendingStatus.text = NSLocalizedString(TRIPS_RIDE_REQ_PENDONG_LBL, nil);
        _topView.backgroundColor = [UIColor colorWithRed:67.0/255.0 green:91.0/255.0 blue:108.0/255.0 alpha:1.0];
    }else if([ride.status caseInsensitiveCompare:@"CONFIRMED"] == NSOrderedSame){
        _pendingStatus.text = NSLocalizedString(TRIPS_RIDE_REQ_CONFIRMED_LBL, nil);
        _topView.backgroundColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
        
    }else if([ride.status caseInsensitiveCompare:@"Running"] == NSOrderedSame){
        _pendingStatus.text = NSLocalizedString(TRIPS_RIDE_REQ_RUNNING_LBL, nil);
        _topView.backgroundColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
        
    }
  //  BOOL isUserChatConnected = [[ChatManager sharedInstance] isUserChatConnected];
    self.chatButton.hidden = YES;
    self.contactButton.hidden = YES;
    if(ride.driverDetail.chatProfile){
        self.chatButton.hidden = NO;
    }
  /*  if(![ride.status isEqualToString:@"PENDING"]){
        self.contactButton.hidden = NO;
    }*/
    self.contactButton.hidden = NO;
    if(self.chatButton.hidden){
        self.contactButton.frameX = 0;
        self.contactButton.frameWidth = _mainView.frameWidth;
        self.verticalSeparator.hidden = YES;
    }
    if(self.contactButton.hidden){
        self.chatButton.frameX = 0;
        self.chatButton.frameWidth = _mainView.frameWidth;
        self.verticalSeparator.hidden = YES;
    }
    if(self.chatButton.hidden == YES && self.contactButton.hidden == YES){
        self.mainView.frameHeight = 210;
        _coloredView.frameHeight = self.mainView.frameHeight;
    }else{
        self.mainView.frameHeight = 250;
        _coloredView.frameHeight = self.mainView.frameHeight;
    }
    if(self.chatButton.hidden == NO && self.contactButton.hidden == NO){
        self.chatButton.frameX = 0;
        self.chatButton.frameWidth = _mainView.frameWidth/2;
        self.contactButton.frameX = self.chatButton.frameMaxX;
        self.contactButton.frameWidth = _mainView.frameWidth/2;
    }
    [_mainView addSubview:_coloredView];
    self.srcAddressLbl.text = ride.driveDetail.srcAdd;
    self.destAddressLbl.text = ride.driveDetail.destAdd;
    self.tripDateAndTime.text = [self getDateAndTime:ride.tripTime];
    
    self.pendingStatus.backgroundColor = [UIColor clearColor];
    self.rideId.text = [NSString stringWithFormat:NSLocalizedString(VC_TRIPSHISTORY_RIDEID, @"Ride Id: {User Ride Id}"), ride.rideId.stringValue];
    /* if([@"RUNNING" isEqualToString:ride.driveDetail.status]){
     self.cancelButton.hidden = YES;
     self.lineSeparator.hidden = YES;
     
     } else{
     self.cancelButton.hidden = NO;
     self.lineSeparator.hidden = NO;
     }
     */
    
    
   // NSString *currencySymbol = [[CurrentLocale sharedInstance] getCurrencySymbol];
   // NSString *amountValue = [NSString stringWithFormat:@"%.02f", ride.zifyPoints.floatValue];
   // self.tripAmount.text = [NSString stringWithFormat:@"%@ %@",currencySymbol,amountValue];

  /*  if([currencySymbol isEqualToString:@"€"]){
        amountValue = [[AppDelegate getAppDelegateInstance] displayTripAmountInCommaSeparator:ride.zifyPoints];
        self.tripAmount.text = amountValue;//[NSString stringWithFormat:@"%@ %@",amountValue,currencySymbol];

    }*/
    
    NSString* amountValue = [[AppDelegate getAppDelegateInstance] displayTripAmountInCommaSeparator:ride.zifyPoints];
    self.tripAmount.text = amountValue;
    [self setFramesAfterSettingText];
    
}
-(NSString *)getDateAndTime:(NSString *)tritDateTime{
    
    //ride.departureTime
  //  NSLog(@"date is %@", tritDateTime);
    NSDateFormatter *dateTimeFormatter1 = [[NSDateFormatter alloc] init];
    [dateTimeFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter1 setLocale:locale];
    
    
    NSDate *departureTime = [dateTimeFormatter1 dateFromString:tritDateTime];
    
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
    
    //   NSDateFormatter * timeFormatter1 = [[NSDateFormatter alloc] init];
    //    [timeFormatter1 setDateFormat:@"HH:mm"];
    //    [timeFormatter1 setLocale:locale];
    
    NSString *timeStr = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureTime];
    NSString *dateStr = [self getDateStringFromDepartureTime:departureTime];
    
    if([NSLocalizedString(CMON_GENERIC_TODAY, nil) isEqualToString:dateStr]){
        self.tripDateAndTime.textColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
    } else{
        self.tripDateAndTime.textColor = [UIColor lightGrayColor];
    }
    return [NSString stringWithFormat:@"%@, %@", dateStr, timeStr];
}
-(NSString *)getDateStringFromDepartureTime:(NSDate *)date{
    NSDate *currentDayDate = [self getCurrentDayDate];
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MMM dd, yyyy"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:currentDayDate toDate:date options:0];
    NSInteger difference = [components day];
    if(difference == 0) return NSLocalizedString(CMON_GENERIC_TODAY, nil);
    else if(difference == 1) return NSLocalizedString(CMON_GENERIC_TOMORROW, nil);
    else return [dateFormatter1 stringFromDate:date];
}
-(NSDate *)getCurrentDayDate{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [calendar dateFromComponents:components];
}
-(void)applyToRightCornerRadiusToTopView{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.topView.bounds byRoundingCorners:(UIRectCornerTopRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.topView.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.topView.layer.mask = maskLayer;
}
@end
