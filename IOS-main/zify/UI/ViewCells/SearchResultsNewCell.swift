//
//  SearchResultsNewCell.swift
//  zify
//
//  Created by Anurag on 10/09/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class SearchResultsNewCell: UITableViewCell {

    @IBOutlet weak var contentBgView:UIView!
    @IBOutlet weak var lblTime:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblSource:UILabel!
    @IBOutlet weak var lblDestination:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblWorks:UILabel!
    @IBOutlet weak var btnRequest:UIButton!

    @IBOutlet weak var imgVerified:UIImageView!
    @IBOutlet weak var btnProfilePic:UIButton!
    @IBOutlet weak var imgDriverProfile:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
