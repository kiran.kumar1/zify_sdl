//
//  UserFavouriteCell.m
//  zify
//
//  Created by Anurag S Rathor on 23/02/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "UserFavouriteCell.h"

@implementation UserFavouriteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
