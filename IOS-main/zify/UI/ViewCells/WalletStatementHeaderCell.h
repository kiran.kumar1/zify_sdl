//
//  WalletStatementHeaderCell.h
//  zify
//
//  Created by Anurag S Rathor on 01/09/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletStatementHeaderCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *statementDate;
@end
