//
//  MenuProfileCell.m
//  zify
//
//  Created by Anurag S Rathor on 09/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "MenuProfileCell.h"

@implementation MenuProfileCell
float progressValue = 0.0f;

-(void) startProgress{
    [self performSelector:@selector(increaseProgressValue) withObject:self afterDelay:0.4];

}

-(void)increaseProgressValue{
    if(self.completenessProgress.progress < self.profileCompleteness){
        progressValue = progressValue + 0.01;
        self.completenessPercent.text = [NSString stringWithFormat:@"%d%%",(int)(progressValue * 100)];
        [self.completenessPercent setFont:[UIFont fontWithName:@"OpenSans-Regular" size:14]];
        self.completenessProgress.progress = progressValue;
        [self performSelector:@selector(increaseProgressValue) withObject:self afterDelay:0.01];
    } else{
        progressValue = 0.0;
    }
}
@end
