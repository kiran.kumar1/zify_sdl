//
//  UpgradeProfileTutCell.h
//  zify
//
//  Created by Anurag S Rathor on 02/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpgradeProfileTutCell : UICollectionViewCell
@property (nonatomic,weak) IBOutlet UIImageView *tutorialImage;
@property(nonatomic,weak) IBOutlet UILabel *tutorialTitle;
@property(nonatomic,weak) IBOutlet UILabel *tutorialDescription;
@end
