//
//  GuestMenuOptionCell.m
//  zify
//
//  Created by Anurag S Rathor on 19/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "GuestMenuOptionCell.h"

@implementation GuestMenuOptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
