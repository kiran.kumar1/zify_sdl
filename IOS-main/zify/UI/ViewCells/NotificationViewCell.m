//
//  NotificationViewCell.m
//  zify
//
//  Created by Anurag S Rathor on 06/12/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "NotificationViewCell.h"

@implementation NotificationViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if( [super initWithStyle:style reuseIdentifier:reuseIdentifier])
        [self initializeControlls];
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)initializeControlls{
    self.frameWidth = [AppDelegate SCREEN_WIDTH];
    self.contentView.frameHeight = 95;
    self.contentView.frameWidth = [AppDelegate SCREEN_WIDTH];
    _mainView = [[UIView alloc] init];
    _mainView.backgroundColor = [UIColor whiteColor];
    _mainView.frame = CGRectMake(5,5,self.contentView.frameWidth - 10  , self.contentView.frameHeight - 10);
    [self.contentView addSubview:_mainView];
    
    CGFloat height = 35;
    CGFloat posX = 10;
    _notificationImageView = [[UIImageView alloc] init];
    _notificationImageView.frame = CGRectMake(posX ,(_mainView.frameHeight - height)/2  ,height, height);
    _notificationImageView.backgroundColor = [UIColor whiteColor];
    [_mainView addSubview:_notificationImageView];
    
    CGFloat arrowHeight = 18;
    _rightImageView = [[UIImageView alloc] init];
    _rightImageView.frame = CGRectMake( _mainView.frameWidth - arrowHeight - _notificationImageView.frameX ,(_mainView.frameHeight - arrowHeight)/2  ,arrowHeight, arrowHeight);
    _rightImageView.image = [UIImage imageNamed:@"av_arrow.png"];
    _rightImageView.backgroundColor = [UIColor clearColor];
    [_mainView addSubview:_rightImageView];
    
    
    _titleLbl = [[UILabel alloc] init];
    _titleLbl.frame = CGRectMake(_notificationImageView.frameMaxX + 5, 0, _rightImageView.frameX - (_notificationImageView.frameMaxX + 5) , 30);
    _titleLbl.numberOfLines = 0;
    _titleLbl.backgroundColor = [UIColor clearColor];
    [self.mainView addSubview:_titleLbl];
    
    _contentLbl = [[UILabel alloc] init];
    _contentLbl.frame = _titleLbl.frame;
    _contentLbl.frameY = _titleLbl.frameMaxY;
    _contentLbl.backgroundColor = [UIColor clearColor];
    _contentLbl.numberOfLines = 0;
    [self.mainView addSubview:_contentLbl];
    
    CGFloat font = 13;
    if([AppDelegate SCREEN_HEIGHT] > 568.0){
        font = 14;
    }
    _titleLbl.font = [UIFont fontWithName:MediumFont size:font];
    _contentLbl.font = [UIFont fontWithName:NormalFont size:font + 0.5];
    _titleLbl.textColor = [UIColor colorWithRed:185.0/255.0 green:81.0/255.0 blue:68.0/255.0 alpha:1.0];
    _contentLbl.textColor = [UIColor colorWithRed:152.0/255.0 green:152.0/255.0 blue:152.0/255.0 alpha:1.0];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setDataForCell:(NSDictionary *)infoDict{
    _mainView.layer.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:223.0/255.0 alpha:1.0].CGColor;
    _mainView.layer.borderWidth = 1.0;
    [self addShadowToView:_mainView];
    _notificationImageView.image  = [UIImage imageNamed:[infoDict objectForKey:@"imageName"]];
    _titleLbl.text = [infoDict objectForKey:@"title"];
    _contentLbl.text = [infoDict objectForKey:@"subTitle"];
    [_titleLbl sizeToFit];
    [_contentLbl sizeToFit];
    CGFloat totalHeight = _titleLbl.frameHeight + _contentLbl.frameHeight;
    CGFloat posY = (self.mainView.frameHeight - totalHeight)/2;
    _titleLbl.frameY = posY - 2;
    _contentLbl.frameY = _titleLbl.frameMaxY + 2;
}
-(void)addShadowToView:(UIView *)v{
    [v.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [v.layer setShadowOpacity:0.8];
    [v.layer setShadowRadius:3.0];
   // [v.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    v.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
}
@end
