//
//  ContactUserDetailCell.h
//  zify
//
//  Created by Anurag S Rathor on 05/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUserDetailCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *userName;
@property (nonatomic,weak) IBOutlet UIImageView *userImage;
@property (nonatomic,weak) IBOutlet UIView *chatUserView;
@property (nonatomic,weak) IBOutlet UIView *callUserView;
@property (nonatomic,weak) IBOutlet UIImageView *chatButtonImage;
@property (nonatomic,weak) IBOutlet UIImageView *callButtonImage;
@property (nonatomic,weak) IBOutlet UIView *separatorLine;
@end
