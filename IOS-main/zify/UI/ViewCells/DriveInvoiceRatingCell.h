//
//  DriveInvoiceRatingCell.h
//  zify
//
//  Created by Anurag S Rathor on 17/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"

@interface DriveInvoiceRatingCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *userName;
@property (nonatomic,weak) IBOutlet UIImageView *userImage;
@property(nonatomic,weak) IBOutlet RatingView *ratingView;
@end
