//
//  BankAccountDetailsCell.h
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankAccountDetailsCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UIButton *bankSelectButton;
@property(nonatomic,weak) IBOutlet UILabel *bankName;
@property(nonatomic,weak) IBOutlet UILabel *accountNumber;
@property(nonatomic,weak) IBOutlet UILabel *ifscCodeLabel;
@property(nonatomic,weak) IBOutlet UILabel *ifscCode;
@property (weak, nonatomic) IBOutlet UILabel *lblAccountNoText;

@end
