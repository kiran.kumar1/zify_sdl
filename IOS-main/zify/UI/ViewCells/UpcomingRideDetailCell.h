//
//  UpcomingRideDetailCell.h
//  zify
//
//  Created by Anurag S Rathor on 04/02/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripRide.h"

@interface UpcomingRideDetailCell : UITableViewCell{
    CGFloat addressLblTextHeight;
    UIImage* profileImagePlaceHolder;
}
@property(nonatomic,strong) UIView *mainView;
@property(nonatomic,strong) UILabel *coloredView;
@property(nonatomic,strong) UIView *topView;
@property(nonatomic,strong) UIView *deleteView;
@property(nonatomic,strong) UILabel *pendingStatus;
@property(nonatomic,strong) UILabel *statusSeparator;
@property(nonatomic,strong) UIImageView *cancelImageView;
@property(nonatomic,strong)  UIButton *cancelButton;

@property(nonatomic,strong) UIImageView *driverImage;
@property(nonatomic,strong) UILabel *driverName;
@property(nonatomic,strong) UILabel *tripAmount;

@property(nonatomic,strong) UIView *journeyDetailsView;
@property(nonatomic,strong) UIImageView *srcImage;
@property(nonatomic,strong) UIImageView *destImage;
@property(nonatomic,strong) UILabel *lineSeparator;
@property(nonatomic,strong) UIView *viewMapDetails;
@property(nonatomic,strong) UILabel *viewRouteLbl;
@property(nonatomic,strong) UIImageView *viewMapImageView;
@property(nonatomic,strong) UIButton *viewRouteButton;
@property(nonatomic,strong) UILabel *srcAddressLbl;
@property(nonatomic,strong) UILabel *destAddressLbl;
@property(nonatomic,strong) UIView *rideDetailsView;
@property(nonatomic,strong) UILabel *rideId;
@property(nonatomic,strong) UIImageView *clockImageView;
@property(nonatomic,strong) UILabel *tripDateAndTime;
@property(nonatomic,strong) UIButton *chatButton;
@property(nonatomic,strong) UIButton *contactButton;
@property(nonatomic,strong) UILabel *verticalSeparator;


-(void)setDataForCell:(TripRide *)ride;

/*
@property(nonatomic,weak) IBOutlet UIButton *contactButton;
@property(nonatomic,weak) IBOutlet UILabel *source;
@property(nonatomic,weak) IBOutlet UILabel *destination;
@property (nonatomic,weak) IBOutlet UILabel *tripDate;
@property (nonatomic,weak) IBOutlet UILabel *tripTime;
@property(nonatomic,weak) IBOutlet UILabel *rideId;
@property(nonatomic,weak) IBOutlet UILabel *tripAmount;
@property(nonatomic,weak) IBOutlet UIView *cancelButtonSeparator;
 */
@end
