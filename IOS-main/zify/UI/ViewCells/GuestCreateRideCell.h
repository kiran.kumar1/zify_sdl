//
//  GuestCreateRideCell.h
//  zify
//
//  Created by Anurag S Rathor on 20/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"

@interface GuestCreateRideCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UIImageView *vehicleImage;
@property(nonatomic,weak) IBOutlet UIImageView *vehicleLockImage;
@property(nonatomic,weak) IBOutlet UIView *driverInfoView;
@property(nonatomic,weak) IBOutlet UIImageView *driverImage;
@property(nonatomic,weak) IBOutlet UIImageView *driverLockImage;
@property(nonatomic,weak) IBOutlet UILabel *amount;
@property(nonatomic,weak) IBOutlet RatingView *maleRatingView;
@property(nonatomic,weak) IBOutlet RatingView *femaleRatingView;
@property(nonatomic,weak) IBOutlet UILabel *availableSeats;
@property(nonatomic,weak) IBOutlet UIImageView *srcIcon;
@property(nonatomic,weak) IBOutlet UILabel *srcAddress;
@property(nonatomic,weak) IBOutlet UIImageView *destIcon;
@property(nonatomic,weak) IBOutlet UILabel *destAddress;
@property(nonatomic,weak) IBOutlet UIButton *loginToRequest;
@end
