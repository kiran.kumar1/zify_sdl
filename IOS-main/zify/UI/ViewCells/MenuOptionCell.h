//
//  MenuOptionCell.h
//  zify
//
//  Created by Anurag S Rathor on 09/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuOptionCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *optionName;
@property(nonatomic,weak) IBOutlet UIImageView *optionImage;
@end
