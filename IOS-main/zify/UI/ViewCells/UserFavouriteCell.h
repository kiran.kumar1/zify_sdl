//
//  UserFavouriteCell.h
//  zify
//
//  Created by Anurag S Rathor on 23/02/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserFavouriteCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UIImageView *expiryImage;
@property(nonatomic,weak) IBOutlet UIImageView *driverImage;
@property(nonatomic,weak) IBOutlet UILabel *driverName;
@property(nonatomic,weak) IBOutlet UILabel *amount;
@property(nonatomic,weak) IBOutlet UIImageView *srcIcon;
@property(nonatomic,weak) IBOutlet UIImageView *destIcon;
@property(nonatomic,weak) IBOutlet UILabel *srcAddress;
@property(nonatomic,weak) IBOutlet UILabel *destAddress;
@property(nonatomic,weak) IBOutlet UILabel *departureTime;
@property(nonatomic,weak) IBOutlet UILabel *departureDate;
@property(nonatomic,weak) IBOutlet UIImageView *chatImage;
@property(nonatomic,weak) IBOutlet UIButton *chatButton;
@property(nonatomic,weak) IBOutlet UIButton *requestButton;
@end
