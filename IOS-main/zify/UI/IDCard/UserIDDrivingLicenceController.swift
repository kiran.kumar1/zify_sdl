//
//  UserIDDrivingLicenceController.swift
//  zify
//
//  Created by Anurag on 02/07/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class UserIDDrivingLicenceController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate {
 
    @IBOutlet var messageHandler: MessageHandler!
    
    @IBOutlet weak var lblHeader1: UILabel!
    @IBOutlet weak var lblHeader2: UILabel!
    @IBOutlet weak var lblHeader3: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    
    @IBOutlet weak var lblUploadDrivingLicenceText: UILabel!
    @IBOutlet weak var imgDrivingLicence: UIImageView!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var btnEditDLImage: UIButton!

    let picker = UIImagePickerController()
    var selectedIdentityCardImage: UIImage?
    var isIDCardUploaded = false
    var isUpgradeFlow = false
    var identityCardTypeSelectedValue = ""
    var identityCardPickerValues: [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapOnDrivingLicenceImage))
        gesture.numberOfTouchesRequired = 1
        gesture.delegate = self
        imgDrivingLicence.addGestureRecognizer(gesture)
        
        identityCardPickerValues = CurrentLocale.sharedInstance().getIdentityCardTyes()
        identityCardTypeSelectedValue = identityCardPickerValues[2] as! String // Driving License
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UIScreen.main.sizeType == .iPhone5 {
            self.lblHeader1.fontSize = 34
            self.lblHeader2.fontSize = 34
            self.lblHeader3.fontSize = 34
            self.lblSubHeader.fontSize = 16
            self.lblUploadDrivingLicenceText.fontSize = 18
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            self.lblHeader1.fontSize = 40
            self.lblHeader2.fontSize = 40
            self.lblHeader3.fontSize = 40
            self.lblSubHeader.fontSize = 20
            self.lblUploadDrivingLicenceText.fontSize = 19
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            self.lblHeader1.fontSize = 45
            self.lblHeader2.fontSize = 45
            self.lblHeader3.fontSize = 45
            self.lblSubHeader.fontSize = 23
            self.lblUploadDrivingLicenceText.fontSize = 20
        }
        
    }
    // MARK: - Actions
    
    @IBAction func btnTappedUpload(_ sender: Any) {
        self.btnUpload.isUserInteractionEnabled = false
        if validateIdentityCardDetails() {
            let imageUploadrequest = UserUploadMultiData(uploadType: ID_CARD_TYPE)
            messageHandler.showBlockingLoadView(handler: {
                ServerInterface.sharedInstance().getResponse(imageUploadrequest, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            self.uploadImagePath("")
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        }
                    })
                }, andImage: self.selectedIdentityCardImage)
            })
        }
    }
    func uploadImagePath(_ imagePath: String?) {

        messageHandler.showBlockingLoadView(handler: {
            let req = UserIdentityCardSaveRequest(identityCardType:self.identityCardTypeSelectedValue, withImagePath: imagePath)

            ServerInterface.sharedInstance().getResponse(req, withHandler: { response, error in
            self.messageHandler.dismissBlockingLoadView(handler: {
                if response != nil {
                    //identityCardTypeView.isUserInteractionEnabled = false
                    self.imgDrivingLicence.isUserInteractionEnabled = false
                    
                    self.btnUpload.setTitle(NSLocalizedString(VC_USERIDCARD_UPLOADED, comment: ""), for: .normal)
                    self.btnUpload.isUserInteractionEnabled = false
                    if !self.isUpgradeFlow {
                        self.perform(#selector(self.dismiss(_:)), with: self, afterDelay: 1.0)
                    }
                    self.isIDCardUploaded = true
                } else {
                    self.btnUpload.isUserInteractionEnabled = true
                    self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                }
            })
        }, andImage: self.selectedIdentityCardImage)
    })
}
    
    
    @IBAction func btnTappedEditDLImage(_ sender: Any) {
        openGalleryOrCamera()
    }
    
    func dismiss(_ sender: Any) {
        let prefs = UserDefaults.standard
        let isForPush: Bool = prefs.bool(forKey: "forPush")
        if isForPush {
            self.navigationController?.popViewController(animated: true)
            return
        }
        dismiss(animated: false)
    }
    
    
    // MARK:- Select Image
    
    func tapOnDrivingLicenceImage() {
        openGalleryOrCamera()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let imageSel = info[UIImagePickerControllerEditedImage] as? UIImage else {
            print("No image found")
            return
        }
        selectedIdentityCardImage = imageSel
        imgDrivingLicence.image = selectedIdentityCardImage
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    func openGalleryOrCamera() {
        
        let alert = UIAlertController.init(title: nil, message: "Any Govt ID", preferredStyle: .actionSheet)
        let galleryAction = UIAlertAction.init(title: "Choose from Gallery", style: .default) { (UIAlertAction) in
            self.fromGallery()
        }
        let cameraAction = UIAlertAction.init(title: "Open Camera", style: .default) { (UIAlertAction) in
            self.fromCamera()
        }
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (UIAlertAction) in
        }
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func fromGallery() {
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        self.present(picker, animated: true, completion: nil)
    }
    
    func fromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .camera
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.present(picker, animated: true, completion: nil)
        } else {
            showAlertView(strMessage: "Camera not available", navigationCtrl: self.navigationController!)
        }
    }
    
    class func createUserIdNavController() -> UINavigationController? {
        
        let storyBoard = UIStoryboard.init(name: "IDCard", bundle: Bundle.main)
        let userIdDrivingLicNavCtrl = storyBoard.instantiateViewController(withIdentifier: "createUserIDDrivingLicNavController") as? UINavigationController
        return userIdDrivingLicNavCtrl
    }
    
    func validateIdentityCardDetails() -> Bool {
        if selectedIdentityCardImage == nil {
            showAlertView(strMessage: NSLocalizedString(VC_USERIDCARD_IDCARDIMAGEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        return true
    }
    

}
