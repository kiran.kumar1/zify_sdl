//
//  UserIDCardController.swift
//  zify
//
//  Created by Anurag on 28/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import CropViewController

class UserIDCardController: UIViewController, UINavigationControllerDelegate, ImageSelectionViewerDelegate, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource,CropViewControllerDelegate {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblChooseAnyGovtID: UILabel!
    @IBOutlet weak var lblHeader1: UILabel!
    @IBOutlet weak var lblHeader2: UILabel!
    @IBOutlet weak var lblHeader3: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet var viewGovtID: UIView!
    @IBOutlet var viewChooseOptionsYourId: UIView!
    @IBOutlet var viewChooseOptionsYourIdLocal: UIView!
    @IBOutlet var viewOptionsTable: UIView!
    
    @IBOutlet weak var tblOptions: UITableView!
    
    @IBOutlet var imgAnyGovtID: UIImageView!
    @IBOutlet var btnChooseAnyID: [UIButton]!
    @IBOutlet var btnChooseAnyIDLocal: [UIButton]!
    
    @IBOutlet var lblNameGovtID: UILabel!
    
    @IBOutlet weak var uploadIdCardBtn: UIButton!
    @IBOutlet weak var btnEditImg: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var identityCardTypeSelectedValue = ""
    let picker = UIImagePickerController()
    var currentUser: UserProfile?
    var selectedIdentityCardImage: UIImage?
    
    var isIDCardUploaded = false
    var isUpgradeFlow = false
    var isGlobalLocale : Bool?
    var arrOptions : Array<String> = Array()
    var selectedIndex : Int?
    var langIdentify : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblOptions.isScrollEnabled = false
        currentUser = UserProfile.getCurrentUser()
        self.uploadIdCardBtn.backgroundColor = UIColor.gray
        self.uploadIdCardBtn.isEnabled = false;
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapOnDrivingLicenceImage))
        gesture.numberOfTouchesRequired = 1
        gesture.delegate = self
        imgAnyGovtID.addGestureRecognizer(gesture)
        viewGovtID.isHidden = true
        
        let currentLocale = CurrentLocale.sharedInstance()
        isGlobalLocale = currentLocale?.isGlobalLocale()
        
        if isGlobalLocale! {
            arrOptions = [NSLocalizedString(CMON_CURRENTLOCALE_GOVTID, comment: ""),NSLocalizedString(CMON_CURRENTLOCALE_PASSPORT, comment: ""),NSLocalizedString(CMON_CURRENTLOCALE_DRIVINGLICENSE, comment: "")]
        } else {
            arrOptions = [NSLocalizedString(ID_AADHAAR_CARD, comment: ""),NSLocalizedString(ID_VOTER_ID, comment: ""),NSLocalizedString(CMON_CURRENTLOCALE_PASSPORT, comment: ""),NSLocalizedString(CMON_CURRENTLOCALE_DRIVINGLICENSE, comment: "")]
        }
        
        let userProfile = UserProfile.getCurrentUser()
        // userProfile?.userType = "DRIVER"
        if(userProfile?.userType == "DRIVER"){
            self.imgAnyGovtID.image = UIImage.init(named: "idCardDL.png")
            self.identityCardTypeSelectedValue = NSLocalizedString(CMON_CURRENTLOCALE_DRIVINGLICENSE, comment: "")
            lblNameGovtID.text = NSLocalizedString(CMON_CURRENTLOCALE_DRIVINGLICENSE, comment: "")
            self.moveToSecondView(animated: false)
        }else{
            identityCardTypeSelectedValue = ""
            lblNameGovtID.text = ""//NSLocalizedString(CMON_CURRENTLOCALE_DRIVINGLICENSE, comment: "")
            self.tblOptions.isHidden = true
            self.viewGovtID.isHidden = false
            self.btnBack.isHidden = true
        }
        
        langIdentify = NSLocale.preferredLanguages[0] as String
        debugPrint(langIdentify!)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString(ID_CARD, comment: "")
      //  [nav.navigationBar setTranslucent:NO];
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        if UIScreen.main.sizeType == .iPhone5 {
            if (langIdentify?.contains("en"))! {
                lblsFontSizeBasedOnLang(lang: langIdentify!, headers: 34, subHeader: 16, nameGovtLbl: 18, chooseAnyGovtId: 20)
            } else  {
                lblsFontSizeBasedOnLang(lang: langIdentify!, headers: 25, subHeader: 14, nameGovtLbl: 14, chooseAnyGovtId: 16)
            }
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            
            if (langIdentify?.contains("en"))! {
                lblsFontSizeBasedOnLang(lang: langIdentify!, headers: 40, subHeader: 20, nameGovtLbl: 19, chooseAnyGovtId: 21)
            } else  {
                lblsFontSizeBasedOnLang(lang: langIdentify!, headers: 35, subHeader: 20, nameGovtLbl: 19, chooseAnyGovtId: 21)
            }
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            
            if (langIdentify?.contains("en"))! {
                lblsFontSizeBasedOnLang(lang: langIdentify!, headers: 45, subHeader: 23, nameGovtLbl: 20, chooseAnyGovtId: 22)
            } else  {
                lblsFontSizeBasedOnLang(lang: langIdentify!, headers: 35, subHeader: 23, nameGovtLbl: 20, chooseAnyGovtId: 22)
            }
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.messageHandler.dismissErrorView()
        self.messageHandler.dismissSuccessView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tblOptions.frame = viewGovtID.frame
    }
    
    func lblsFontSizeBasedOnLang(lang : String, headers : Int, subHeader : Int, nameGovtLbl : Int, chooseAnyGovtId : Int) {
        
        
        if lang.contains("fr") {
            self.lblHeader1.fontSize = CGFloat(headers)
            self.lblHeader2.fontSize = CGFloat(headers)
            self.lblHeader3.fontSize = CGFloat(headers)
            self.lblSubHeader.fontSize = CGFloat(subHeader)
            self.lblNameGovtID.fontSize = CGFloat(nameGovtLbl)
            self.lblChooseAnyGovtID.fontSize = CGFloat(chooseAnyGovtId)
        } else {
            self.lblHeader1.fontSize = CGFloat(headers)
            self.lblHeader2.fontSize = CGFloat(headers)
            self.lblHeader3.fontSize = CGFloat(headers)
            self.lblSubHeader.fontSize = CGFloat(subHeader)
            self.lblNameGovtID.fontSize = CGFloat(nameGovtLbl)
            self.lblChooseAnyGovtID.fontSize = CGFloat(chooseAnyGovtId)
        }
    }
    
    @IBAction func btnTappedEditImage(_ sender: Any) {
        displayPhotoPickingOptions(sender)
        
    }
    
    @IBAction func btnTappedUpload(_ sender: Any) {
        if validateIdentityCardDetails() {
            
            let imageUploadrequest = UserUploadMultiData(uploadType: ID_CARD_TYPE)
            
            messageHandler.showBlockingLoadView(handler: {
                ServerInterface.sharedInstance().getResponse(imageUploadrequest, withHandler: { response, error in
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            
                            var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                            let imagePath = resultDict["responseText"] as! String
                            //response?.responseObject["responseText"] as? String
                            self.uploadIdcardImage(imagePath: imagePath)
                        }
                    } else {
                        self.messageHandler.dismissBlockingLoadView(handler: {
                            
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        })
                    }
                }, andImage: self.selectedIdentityCardImage)
            })
        }
    }
    
    func uploadIdcardImage(imagePath:String)  {
        let req = UserIdentityCardSaveRequest(identityCardType:self.identityCardTypeSelectedValue, withImagePath: imagePath)
        ServerInterface.sharedInstance().getResponse(req, withHandler: { response, error in
            self.messageHandler.dismissBlockingLoadView(handler: {
                if response != nil {
                    //identityCardTypeView.isUserInteractionEnabled = false
                    MoEngageEventsClass.callIDCardUploadedEvent()
                    self.imgAnyGovtID.isUserInteractionEnabled = false
                    
                    self.uploadIdCardBtn.setTitle(NSLocalizedString(VC_USERIDCARD_UPLOADED, comment: ""), for: .normal)
                    self.uploadIdCardBtn.isUserInteractionEnabled = false
                    if !self.isUpgradeFlow {
                        self.perform(#selector(self.dismiss(_:)), with: self, afterDelay:0.5)
                    }
                    self.isIDCardUploaded = true
                } else {
                    self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                }
            })
        })
    }
    @IBAction func btnTappedNext(_ sender: Any) {
        if(self.identityCardTypeSelectedValue.count == 0){
            UIAlertController.showError(withMessage: NSLocalizedString(ID_PLEASE_CHOOSE_YOUR_GOVT_ID_TYPE, comment: ""), inViewController: self)
            return;
        }
        self.moveToSecondView(animated: true)
    }
    
    func moveToSecondView(animated:Bool)  {
        if(animated){
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.tblOptions.alpha = 0.0
            }, completion: {
                (finished: Bool) -> Void in
                if self.isGlobalLocale! {
                    self.tblOptions.isHidden = true
                    self.viewGovtID.isHidden = false
                } else {
                    self.tblOptions.isHidden = true
                    self.viewGovtID.isHidden = false
                }
                // Fade in
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                    self.viewGovtID.alpha = 1.0
                }, completion: nil)
            })
        }else{
            self.tblOptions.alpha = 0.0
            if self.isGlobalLocale! {
                self.tblOptions.isHidden = true
                self.viewGovtID.isHidden = false
            } else {
                self.tblOptions.isHidden = true
                self.viewGovtID.isHidden = false
            }
            self.viewGovtID.alpha = 1.0
        }
    }
    
    @IBAction func btnTappedBackViewGovtID(_ sender: Any) {
        imgAnyGovtID.image = UIImage.init(named: "idCardAny")
        selectedIdentityCardImage = nil
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.viewGovtID.alpha = 0.0
        }, completion: {
            (finished: Bool) -> Void in
            
            self.tblOptions.isHidden = false
            self.viewGovtID.isHidden = true
            // Fade in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.tblOptions.alpha = 1.0
            }, completion: nil)
        })
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.messageHandler.dismissErrorView()
        self.messageHandler.dismissSuccessView()
        let prefs = UserDefaults.standard
        let isForPush: Bool = prefs.bool(forKey: "forPush")
        let isForPresent: Bool = prefs.bool(forKey: "forPresent")
        let isFromID = prefs.bool(forKey: "IdFromPush")
        if(isFromID)
        {
            AppDelegate.getInstance().callApiForAcceptRideRequestFromPushNotification()
        }
        if isForPush {
            UserDefaults.standard.set(false, forKey: "forPush")
            UserDefaults.standard.synchronize()
            self.navigationController?.popViewController(animated: true)
            return
        }else if isForPresent {
            UserDefaults.standard.set(false, forKey: "forPresent")
            UserDefaults.standard.synchronize()
            self.navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        dismiss(animated: false)
    }
    
    // MARK:- Select Image
    func tapOnDrivingLicenceImage() {
        displayPhotoPickingOptions(self)
    }
    
    // MARK:- ImageSelectiponViewer Delegate
    func displayPhotoPickingOptions(_ sender: Any?) {
        ImageSelectionViewer.sharedInstance().imageSelectionDelegate = self
        ImageSelectionViewer.sharedInstance().finalImageSize = CGSize(width: 320.0, height: 320.0)
        ImageSelectionViewer.sharedInstance().selectImage()
    }
    
    func userSelectedFinalImage(_ image: UIImage?) {
        self.perform(#selector(openCropImgeVc), with: image, afterDelay: 0.0)

    }
    @objc func openCropImgeVc(withImage:UIImage) -> Void {
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default , image: withImage)
        cropController.delegate = self
        self.present(cropController, animated: false, completion: nil)
    }

    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        let finalImage:UIImage = ImageHelper.image(with: image, scaledTo: self.imgAnyGovtID.frame.size)
        selectedIdentityCardImage = finalImage
        self.imgAnyGovtID.image = finalImage
        self.uploadIdCardBtn.backgroundColor = UIColor.init(hex: "2662FF")
        self.uploadIdCardBtn.isEnabled = true
        self.dismiss(animated: true, completion:nil)
    }
    func allowImageEditing() -> Bool {
        return false
    }
    
    
    
    class func createUserIdNavController() -> UINavigationController? {
        
        let storyBoard = UIStoryboard.init(name: "IDCard", bundle: Bundle.main)
        let userIdNavCtrl = storyBoard.instantiateViewController(withIdentifier: "createUserIDNavController") as? UINavigationController
        return userIdNavCtrl
    }
    
    func validateIdentityCardDetails() -> Bool {
        if selectedIdentityCardImage == nil {
            showAlertView(strMessage: NSLocalizedString(VC_USERIDCARD_IDCARDIMAGEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        return true
    }
    
    // MARK:- UITableView Delegates
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        let headerCell = tblOptions.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        headerCell.lblChooseYourGovtIDText.numberOfLines = 2
        if UIScreen.main.sizeType == .iPhone5 {
            if (langIdentify?.contains("en"))! {
                headerCell.lblChooseYourGovtIDText.fontSize = 20
            } else {
                headerCell.lblChooseYourGovtIDText.fontSize = 18
            }
            
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS  {
            if (langIdentify?.contains("en"))! {
                headerCell.lblChooseYourGovtIDText.fontSize = 21
            } else {
                headerCell.lblChooseYourGovtIDText.fontSize = 19
            }
            
        } else if UIScreen.main.sizeType == .iPhone6Plus {
            if (langIdentify?.contains("en"))! {
                headerCell.lblChooseYourGovtIDText.fontSize = 22
            } else {
                headerCell.lblChooseYourGovtIDText.fontSize = 20
            }
            
        }
        
        headerView.addSubview(headerCell)
        headerView.backgroundColor = UIColor.clear
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44  // or whatever
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOptions.count + 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if( indexPath.row < arrOptions.count){ return 44}
        else {return 60};
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrOptions.count == indexPath.row {
            let cellNext = tableView.dequeueReusableCell(withIdentifier: "NextButtonCell") as! NextButtonCell
            cellNext.btnNext.addTarget(self, action: #selector(self.btnTappedNext(_:)), for: .touchUpInside)
            cellNext.selectionStyle = .none
            return cellNext
        }
        let cellOptions: ChooseOptionsCell = (tableView.dequeueReusableCell(withIdentifier: "ChooseOptionsCell") as! ChooseOptionsCell?)!
        cellOptions.lblOptions?.text = arrOptions[indexPath.row]
        cellOptions.selectionStyle = .none
        
        if(indexPath.row == selectedIndex) {
            cellOptions.imgRadio.image = UIImage.init(named: "bankaccount_radio_selected")
            
        } else {
            cellOptions.imgRadio.image = UIImage.init(named: "bankaccount_radio_unselected")
        }
        
        return cellOptions
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        if(indexPath.row < arrOptions.count){
        selectedIndex = indexPath.row
        identityCardTypeSelectedValue = arrOptions[indexPath.row]
        lblNameGovtID.text = arrOptions[indexPath.row]
        self.tblOptions.reloadData()
        }
    }
    
}

class ChooseOptionsCell : UITableViewCell {
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lblOptions: UILabel!
}

class NextButtonCell : UITableViewCell {
    @IBOutlet weak var btnNext: UIButton!
}

class HeaderCell : UITableViewCell {
    
    @IBOutlet weak var lblChooseYourGovtIDText: UILabel!
}
