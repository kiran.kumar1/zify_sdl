//
//  UserGetInController.swift
//  zify
//
//  Created by Anurag on 28/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class UserGetInController: UIViewController {
    
    @IBOutlet weak var lblHeader1: UILabel!
    @IBOutlet weak var lblHeader2: UILabel!
    @IBOutlet weak var lblHeader3: UILabel!
    @IBOutlet weak var lblHeader4: UILabel!
    @IBOutlet weak var lblHeader5: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    
    @IBOutlet var messageHandler: MessageHandler!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UIScreen.main.sizeType == .iPhone5 {
            self.lblHeader1.fontSize = 34
            self.lblHeader2.fontSize = 34
            self.lblHeader3.fontSize = 34
            self.lblHeader4.fontSize = 34
            self.lblHeader5.fontSize = 34
            self.lblSubHeader.fontSize = 14
         } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            self.lblHeader1.fontSize = 40
            self.lblHeader2.fontSize = 40
            self.lblHeader3.fontSize = 40
            self.lblHeader4.fontSize = 40
            self.lblHeader5.fontSize = 40
            self.lblSubHeader.fontSize = 17
         } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            self.lblHeader1.fontSize = 45
            self.lblHeader2.fontSize = 45
            self.lblHeader3.fontSize = 45
            self.lblHeader4.fontSize = 45
            self.lblHeader5.fontSize = 45
            self.lblSubHeader.fontSize = 20
         }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
