//
//  GenericWebController.swift
//  zify
//
//  Created by Anurag Rathor on 06/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class GenericWebController: UIViewController, UIWebViewDelegate{
    
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var webView: UIWebView!
    var webViewURL:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.webView.delegate = self
        if let anURL = URL(string: webViewURL) {
            self.messageHandler.showBlockingLoadView {
                self.webView.loadRequest(URLRequest(url: anURL))
            }
        }
    }

    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: false)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
       
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.messageHandler.dismissBlockingLoadView {
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.messageHandler.dismissBlockingLoadView {
        }
    }
    
    
    class func createGenericWebNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Commons", bundle: Bundle.main)
        let genericWebNavController = storyBoard.instantiateViewController(withIdentifier: "genericWebNavController") as? UINavigationController
        return genericWebNavController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
