//
//  TravelPrefStep2.swift
//  zify
//
//  Created by Anurag S Rathor on 17/11/17.
//  Copyright © 2017 zify. All rights reserved.
//

import UIKit

class TravelPrefStep2: BaseViewController,MapLocationHelperDelegate {

    
    var ownerView:UIView = UIView()
    var riderView:UIView = UIView()
    var btnSelected:UIButton  = UIButton()
    var isHomeAddressTapped:Bool = Bool()
    var isHomeAddressSelected:Bool = Bool()
    var isofficeAddressSelected:Bool = Bool()
    var btnHomeAddress:UIButton = UIButton()
    var btnOfficeAddress:UIButton = UIButton()

    @IBOutlet var messageHandler:MessageHandler! = MessageHandler()
    var mapLocationHelper:MapLocationHelper = MapLocationHelper()
    var homeAddressInfo:LocalityInfo = LocalityInfo()
    var officeAddressInfo:LocalityInfo = LocalityInfo()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapLocationHelper = MapLocationHelper.init(delegae: self)
        print(messageHandler.container)
        messageHandler.container.addSubview(self.bgView);
        self.btnNext.addTarget(self, action: #selector(btnNextTapped), for: .touchUpInside)
        self.headerTitleLbl.text = NSLocalizedString(TP_SCREEN2_TITLE, comment: "")
        self.headerSubTitleLbl.text = NSLocalizedString(TP_SCREEN2_SUB_TITLE, comment: "")
        self.progressView.setProgress(0.5, animated: true)
        
        let remainingHeight:CGFloat = (self.bgView.frameHeight - self.headerSubTitleLbl.frameMaxY - self.bottomView.frameHeight)/2 - 1;
        // CGRect(x: 0, y: headerSubTitleLbl.frameMaxY, width:AppDelegate.screen_WIDTH, height: remainingHeight)
        ownerView = createViewForRiderOrOwner(imageName: "icn_onboard_homeadress@3x.png", withTitle: NSLocalizedString(TP_SCREEN2_HOME_TITLE, comment: ""), withFrame:CGRect(x: 0, y: self.headerSubTitleLbl.frameMaxY, width: AppDelegate.screen_WIDTH(), height: remainingHeight),withTag:44)
        self.view.addSubview(ownerView);
        
        let lineSeparator:UILabel = UILabel();
        lineSeparator.frame = CGRect(x:0, y:ownerView.frameMaxY, width:AppDelegate.screen_WIDTH(), height:2);
        lineSeparator.backgroundColor = UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        self.view.addSubview(lineSeparator);
        
        riderView = createViewForRiderOrOwner(imageName: "icn_onboard_officeaddress@3x.png", withTitle: NSLocalizedString(TP_SCREEN2_OFFICE_TITLE, comment: ""), withFrame: ownerView.frame,withTag:55)
        riderView.frameY = lineSeparator.frameMaxY
        self.view.addSubview(riderView);
        
    }
    
    func createViewForRiderOrOwner(imageName:String, withTitle:String, withFrame:CGRect, withTag:Int) -> UIView {
        
        print(withTitle);
        let mainview:UIView = UIView.init(frame: withFrame)
        
        let padX:CGFloat = 30;
        let padY:CGFloat = 20;
        let view:UIView = UIView.init(frame: CGRect(x: padX, y: padY, width:(AppDelegate.screen_WIDTH() - 2*padX), height: mainview.frameHeight - 2*padY));
        view.backgroundColor = UIColor.white;
        mainview.addSubview(view);
        
        var posX:CGFloat = 25;
        var height:CGFloat = 55;
        var fontSize:CGFloat = 15;
        
        if(AppDelegate.screen_HEIGHT() <= 568){
            posX = 10;
            height = 40;
            fontSize = 14;
        }
        let imageLogo:UIImageView = UIImageView.init(frame: CGRect(x:posX, y: 0, width: height, height: height))
        imageLogo.frameY = (view.frameHeight - imageLogo.frameHeight)/2;
        imageLogo.image = UIImage.init(named: imageName)
        view.addSubview(imageLogo);
        let btn:UIButton = UIButton.init(type: UIButtonType.custom);
        btn.frame = imageLogo.frame;
        btn.frameX = imageLogo.frameMaxX + posX;
        btn.frameWidth = view.frameWidth - btn.frameX - posX;
        btn.backgroundColor = UIColor.clear
        btn.setTitle(withTitle, for: .normal)
        view.addSubview(btn);
        btn.tag = withTag
        btn.titleLabel?.lineBreakMode = .byWordWrapping;
        btn.titleLabel?.numberOfLines = 2;
        btn.setTitleColor(UIColor.init(red: 183.0/255.0, green: 183.0/255.0, blue: 183.0/255.0, alpha: 1.0), for: .normal)
        btn.setTitleColor(UIColor.init(red: 183.0/255.0, green: 183.0/255.0, blue: 183.0/255.0, alpha: 1.0), for: .selected)
        btn.addTarget(self, action: #selector(btnSelecttapped), for: .touchUpInside)
        btn.titleLabel?.font = UIFont.init(name: NormalFont, size: fontSize)
        btn.contentHorizontalAlignment = .left


        if(withTag == 44){
            btnHomeAddress = btn;
        }else{
            btnOfficeAddress = btn;
        }

        addBorder(button: btn)
        view.layer.borderColor = UIColor.red.cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 5;
        
        
        return mainview;
    }
    
    func addBorder(button:UIButton!) {
        let height:CGFloat = 1.5
        let border = CALayer()
        border.backgroundColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0, y: button.frame.size.height - height, width: button.frame.size.width, height: height)
        button.layer.addSublayer(border)
    }
    
    func btnNextTapped(sender:UIButton) -> Void {

        if(!isHomeAddressSelected){
            UIAlertController.showError(withMessage: NSLocalizedString(TP_SCREEN2_HOME_ADDRESS_ERROR, comment: ""), inViewController: self)
        }else if(!isofficeAddressSelected){
             UIAlertController.showError(withMessage: NSLocalizedString(TP_SCREEN2_OFFICE_ADDRESS_ERROR, comment: ""), inViewController: self)
        }else{
          let ownerOrRider = AppDelegate.getInstance().paramsDict.object(forKey: "OwnerOrRider") as! String
            if(ownerOrRider == "RIDER"){
                self.moveToLastScreen()
            }else{
                self.moveToNextScreen()
               // self.getRoutesFromStartAndDestination()
            }
        }
    }
    func moveToNextScreen() -> Void{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let obj:TravelPrefStep3 = storyboard.instantiateViewController(withIdentifier: "TpScreen3") as! TravelPrefStep3
        AppDelegate.getInstance().homeAddress = homeAddressInfo
        AppDelegate.getInstance().officeAddress = officeAddressInfo
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func moveToLastScreen() -> Void {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let obj:TravelPrefStep4 = storyboard.instantiateViewController(withIdentifier: "TpScreen4") as! TravelPrefStep4;        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func btnSelecttapped(sender:UIButton) -> Void {
        print("selected tappped")
        btnSelected = sender
        if(sender.tag == 44){isHomeAddressTapped = true}
        else{isHomeAddressTapped = false}
        self.showAddressPicker()
        
        
    }
    func showAddressPicker() -> Void {
        messageHandler.showBlockingLoadView(handler: {
            self.mapLocationHelper.showPick(onMap: self.navigationController)
        })
    }
    
    // MARK : MapLocationHelperDelegate methods
    func selectedLocationInfo(_ locationInfo: LocalityInfo!, andError error: Error!) {
        if(error != nil){
            self.messageHandler.dismissBlockingLoadView(handler: {
                self.messageHandler.showErrorMessage(NSLocalizedString(VC_USERPREFERENCES_LOCATIONMAPPINGERROR, comment: ""))
            })
        }else{
            if(locationInfo != nil){
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if(self.isHomeAddressTapped){
                        self.homeAddressInfo = locationInfo;
                        AppDelegate.getInstance().homeAddress = locationInfo
                        self.isHomeAddressSelected  = true;
                        self.btnHomeAddress.setTitle(locationInfo.address, for: .normal)
                        self.btnHomeAddress.setTitle(locationInfo.address, for: .selected)

                    }else{
                        self.officeAddressInfo = locationInfo
                        AppDelegate.getInstance().officeAddress = locationInfo
                        self.isofficeAddressSelected  = true;
                        self.btnOfficeAddress.setTitle(locationInfo.address, for: .normal)
                        self.btnOfficeAddress.setTitle(locationInfo.address, for: .selected)
                    }
                    print("address is \(locationInfo.address)")
                })
            }else{
                self.messageHandler.dismissBlockingLoadView(handler: {
                    
                })
            }
        }
    }
  
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
