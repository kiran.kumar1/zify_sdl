//
//  RedemptionController.swift
//  zify
//
//  Created by Anurag on 17/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

var strAddress: String! = ""
var dictForSrvc : Dictionary = Dictionary<String, AnyObject>()

class RedemptionController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var tblRedeemOptions: UITableView!
    @IBOutlet weak var txtRedeemableAmt: UITextField!
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var viewRedeemable: UIView!
    @IBOutlet weak var btnRedeem: UIButton!
    @IBOutlet weak var viewRedeemptionOptions: UIView!
    @IBOutlet weak var btn500Tapped: UIButton!
    @IBOutlet weak var btn1000Tapped: UIButton!
    @IBOutlet weak var btn1500Tapped: UIButton!
    @IBOutlet weak var btn2000Tapped: UIButton!
    @IBOutlet weak var const_ContentView_Height: NSLayoutConstraint!
    @IBOutlet weak var viewContent: UIView!
    
    //var arrMainCell = NSArray.init(objects: "Paytm", "Bank Account", "Petro Card", "Zify Cash")
   // var arrMainCell = NSArray.init(objects: NSLocalizedString(LR_PAYTM, comment: ""), NSLocalizedString(LR_BANK_ACCOUNT, comment: ""), NSLocalizedString(LR_PETRO_CARD, comment: ""), NSLocalizedString(LR_ZIFY_CASH, comment: ""))
    
    var arrMainCell = NSArray.init(objects: NSLocalizedString(LR_PAYTM, comment: ""), NSLocalizedString(LR_BANK_ACCOUNT, comment: ""), NSLocalizedString(LR_ZIFY_CASH, comment: ""))

    //var arrBankName = NSArray.init(objects: "State Bank of India", "IDBI Bank", "Co-operative Bank", "ICICI Bank", "Axis Bank")
    var arrAccountNo = NSArray.init(objects: "452309878923","09548739875634", "2903857235409", "02953847345", "239845798235")
    
    
    let kHeaderSectionTag: Int = 6900;
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: NSMutableArray = []
    
    var multipleButtons : Array<AnyObject> = []
    var arrBankAcccounts : Array<AnyObject> = []
    var iBankCount : Int! = 1
    var cellPayTm: PaytmCell!
    var petroAddressCell : PetroCardAddressCell!
    var mainCell : MainCell!
    var addBankAndAddressCell : AddBankAndAddressCell!
    var selectedIndex : Int?
    var bankAccountDetails : BankAccountDetails!

    var strAmountFromRewards : String! = ""
    var isGetBankServiceCall : Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //viewContent.isHidden = true
        lblAmount.text = strAmountFromRewards
        txtRedeemableAmt.layer.borderColor = UIColor.lightGray.cgColor
        txtRedeemableAmt.layer.borderWidth = 1;
        txtRedeemableAmt.layer.cornerRadius = 5;
        
        cellPayTm = (self.tblRedeemOptions.dequeueReusableCell(withIdentifier: "PaytmCell") as? PaytmCell)!
        mainCell = tblRedeemOptions.dequeueReusableCell(withIdentifier: "MainCell") as? MainCell
        petroAddressCell = (tblRedeemOptions.dequeueReusableCell(withIdentifier: "PetroCardAddressCell") as? PetroCardAddressCell)!
        addBankAndAddressCell = (tblRedeemOptions.dequeueReusableCell(withIdentifier: "AddBankAndAddressCell") as? AddBankAndAddressCell)!


        cellPayTm.txtRegisteredMobNo.layer.borderColor = UIColor.lightGray.cgColor
        cellPayTm.txtRegisteredMobNo.layer.borderWidth = 1;
        cellPayTm.txtRegisteredMobNo.layer.cornerRadius = 5;
        let leftview = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 5))
        cellPayTm.txtRegisteredMobNo.leftView = leftview
        cellPayTm.txtRegisteredMobNo.leftViewMode = .always
        
        viewRedeemable.dropShadow(isShowShadow: true)
        isTableView(isEnable: false)
        
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString(LR_REDEMPTION, comment: "")//"Redemption"
        //self.navigationItem.title = "Redemption"
        
        sectionItems = [ 1 , iBankCount, 1, 1]
        self.tblRedeemOptions!.tableFooterView = UIView()
        
        setBorderCornerRadius(anyOb: btn500Tapped, color: UIColor.init(red: 87/255, green: 99/255, blue: 237/255, alpha: 1))
        setBorderCornerRadius(anyOb: btn1000Tapped, color: UIColor.init(red: 87/255, green: 99/255, blue: 237/255, alpha: 1))
        setBorderCornerRadius(anyOb: btn1500Tapped, color: UIColor.init(red: 87/255, green: 99/255, blue: 237/255, alpha: 1))
        setBorderCornerRadius(anyOb: btn2000Tapped, color: UIColor.init(red: 87/255, green: 99/255, blue: 237/255, alpha: 1))
        
        multipleButtons = [btn500Tapped, btn1000Tapped, btn1500Tapped, btn2000Tapped]
        
        showMultipleBtns(FromLblAmt: multipleButtons)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showMultipleBtns(FromLblAmt: Array<AnyObject>) {
        
        var clickBtn = UIButton()
        for clickBtn2 in multipleButtons {
            clickBtn = (clickBtn2 as! UIButton)
            let CheckValue = Double((clickBtn.titleLabel!.text!))!
            let CheckValue22 = Double((lblAmount.text!))!
            
            if CheckValue <= CheckValue22 {
                clickBtn.isEnabled = true
                clickBtn.alpha = 1
            } else {
                clickBtn.isEnabled = false
                clickBtn.alpha = 0.5
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        const_ContentView_Height.constant = AppDelegate.screen_HEIGHT() - 50
        print("addddddd is \(strAddress)")
        
        addBankAndAddressCell.btnAddBankAndAddress.isUserInteractionEnabled = true
        
        if strAddress != "" {
            let indexPath = IndexPath(item: 0, section: 2)
            tblRedeemOptions.reloadRows(at: [indexPath], with: .top)
        } else {
//            let indexPath = IndexPath(item: 0, section: 2)
//            tblRedeemOptions.reloadRows(at: [indexPath], with: .top)
//            tblRedeemOptions.reloadSections(IndexSet(integersIn: 0...2), with: UITableViewRowAnimation.top)
        }

        if(isGetBankServiceCall == false){
            //self.callService_GetBankDetailLists()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }

    // MARK: - Actions
    
    @IBAction func btnAmountTapped(_ sender: UIButton) {
        
        for button in multipleButtons {
            guard button.isEnabled else {
                return
            }
            
            if button as? UIButton == sender {
                (button as? UIButton)!.isSelected = true
                (button as? UIButton)!.setTitleColor(UIColor.white, for: UIControlState.normal)
                (button as? UIButton)!.backgroundColor = UIColor.init(red: 87/255, green: 99/255, blue: 237/255, alpha: 1)
                btnRedeem.alpha = 1
                btnRedeem.isEnabled = true
                txtRedeemableAmt.text = sender.titleLabel?.text
                
            }
            else if (button as? UIButton)!.titleLabel?.text! == sender.titleLabel?.text ?? "0" {
                // For Auto Selecting the Button from Textfield Text
                (button as? UIButton)!.isSelected = true
                (button as? UIButton)!.setTitleColor(UIColor.white, for: UIControlState.normal)
                (button as? UIButton)!.backgroundColor = UIColor.init(red: 87/255, green: 99/255, blue: 237/255, alpha: 1)
                btnRedeem.alpha = 1
                btnRedeem.isEnabled = true
                
            } else if button as? UIButton != sender {
                (button as? UIButton)!.isSelected = false
                (button as? UIButton)?.setTitleColor(UIColor.init(red: 87/255, green: 99/255, blue: 237/255, alpha: 1), for: .normal)
                (button as? UIButton)?.backgroundColor = UIColor.white
                
            }
        }
        
    }
    
    @IBAction func btnRedeemTapped(_ sender: Any) {
        viewRedeemptionOptions.alpha = 1
        tblRedeemOptions.alpha = 1
        tblRedeemOptions.isUserInteractionEnabled = true
        
        viewRedeemable.dropShadow(isShowShadow: true)
    }
    
    func btnDoneBankTapped(sender: UIButton) {
        if sender.tag == 20 {
            
            guard dictForSrvc["bankDetailId"] != nil else {
                showAlertView(strMessage: NSLocalizedString(LR_PLEASE_SELECT_BANK_TO_SEND_MONEY_TO_YOUR_ACC, comment: ""), navigationCtrl: self.navigationController!)
                //showAlertView(strMessage: "Please Select bank to send money to your account", navigationCtrl: self.navigationController!)
                return
            }
            
            dictForSrvc["userToken"] = UserProfile.getCurrentUserToken() as AnyObject
            dictForSrvc["type"] = "BANK_ACC" as AnyObject
            dictForSrvc["amount"] = Int(txtRedeemableAmt.text!) as AnyObject
            callService_RedeemZifyCoins(strRedeemFrom: "FromBankAccount")
        }
    }
    
    func btnAddAddressOrAddBankTapped(sender: UIButton) {
        
        sender.isUserInteractionEnabled = false
        
        if sender.tag == 1 { // ADD BANK ACCOUNT
            isGetBankServiceCall = false
           /* let objAddbankAccScreen = self.storyboard?.instantiateViewController(withIdentifier: "addBankController")
            objAddbankAccScreen?.navigationController?.isNavigationBarHidden = false
            self.navigationController?.pushViewController(objAddbankAccScreen!, animated: true)
            */
            let sb = UIStoryboard(name: "UserSettings", bundle: Bundle.main)
            let settingBankAccVc = sb.instantiateViewController(withIdentifier: "SettingsBankAccountsController") as? SettingsBankAccountsController
            if let aVc = settingBankAccVc {
                navigationController?.pushViewController(aVc, animated: true)
            }
        } else if sender.tag == 2 { // ADD ADDRESS FOR PETRO CARD
            
            let presentAddAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "addressNav") as? UINavigationController
            self.navigationController?.present(presentAddAddressVC!, animated: true, completion: nil)
        }
    }
    
    
    func btnPayTmDoneTapped(sender: UIButton) {
        self.cellPayTm.txtRegisteredMobNo.resignFirstResponder()
        self.view.endEditing(true)
        
        guard cellPayTm.txtRegisteredMobNo.text! != "" else {
            showAlertView(strMessage: NSLocalizedString(LR_PLEASE_ENTER_REG_MOBILE_NUMBR_TO_SNED_MONEY_TO_PAYTM_ACC, comment: ""), navigationCtrl: self.navigationController!)
                //showAlertView(strMessage: "Please enter Registered mobile number to send money to your paytm account", navigationCtrl: self.navigationController!)
            return
        }
        
        guard cellPayTm.txtRegisteredMobNo.text!.count >= 10 else {
            showAlertView(strMessage: NSLocalizedString(LR_PLEASE_ENTER_VALID_MOB_NUMBER, comment: ""), navigationCtrl: self.navigationController!)
            //showAlertView(strMessage: "Please enter valid mobile number", navigationCtrl: self.navigationController!)
            return
        }
        
        dictForSrvc["userToken"] = UserProfile.getCurrentUserToken() as AnyObject
        dictForSrvc["type"] = "PAYTM" as AnyObject
        dictForSrvc["amount"] = Int(txtRedeemableAmt.text!) as AnyObject
        dictForSrvc["mobile"] = Int(cellPayTm.txtRegisteredMobNo.text!) as AnyObject

        callService_RedeemZifyCoins(strRedeemFrom: "FromPaytm")
        
    }
    
    func btnZifyCashDoneTapped(sender: UIButton) {
        
        dictForSrvc["userToken"] = UserProfile.getCurrentUserToken() as AnyObject
        dictForSrvc["amount"] = Int(txtRedeemableAmt.text!) as AnyObject
        dictForSrvc["type"] = "ZIFY_CASH" as AnyObject
        callService_RedeemZifyCoins(strRedeemFrom: "FromZifyCash")
    }
    
    func btnPetroCardAddressDoneTapped(sender: UIButton) {
        
        dictForSrvc["userToken"] = UserProfile.getCurrentUserToken() as AnyObject
        dictForSrvc["amount"] = Int(txtRedeemableAmt.text!) as AnyObject
        dictForSrvc["type"] = "PETRO_CARD" as AnyObject
        dictForSrvc["address"] = strAddress as AnyObject
        callService_RedeemZifyCoins(strRedeemFrom: "FromPetroCard")
    }
    
    
    func tapOnLabelAddress(gesture : UITapGestureRecognizer) {
        let presentAddAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "addressNav") as? UINavigationController
        strAddress = (gesture.view as? UILabel)?.text!
        self.navigationController?.present(presentAddAddressVC!, animated: true, completion: nil)
    }
    
    // MARK: - TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
      /*  if arrMainCell.count > 0 {
            tblRedeemOptions.backgroundView = nil
            return arrMainCell.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = NSLocalizedString(LR_RETRIEVING_DATA, comment: "") + "\n" + NSLocalizedString(LR_PLEASE_WAIT, comment: "") //"Retrieving data.\nPlease wait."
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = .center;
            messageLabel.font = UIFont(name: "OpenSans-Regular", size: 20.0)!
            messageLabel.sizeToFit()
            self.tblRedeemOptions.backgroundView = messageLabel;
        }
        return 0*/
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            if section == 1 {
                if iBankCount > 0 {
                    return iBankCount
                } else {
                    return 1
                }
            } else {
                return 1
            }
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.arrMainCell.count != 0) {
            return self.arrMainCell[section] as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.white
        
        let headerCell = tblRedeemOptions.dequeueReusableCell(withIdentifier: "MainCell") as! MainCell
        headerCell.lblRedemtionOptionName.text = arrMainCell.object(at: section) as? String
        header.addSubview(headerCell)
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 32, y: 13, width: 18, height: 18));
        theImageView.image = UIImage(named: "av_down_arrow")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        if let viewWithTag1 = header.viewWithTag(section + 1) {
            viewWithTag1.removeFromSuperview()
        }
        let imgRadio = UIImageView.init(frame: CGRect(x: 18, y: 15, width: 14, height: 14));
        imgRadio.image = UIImage(named: "circle_gray.png")
        //selectedIndex != nil &&
        if(  section == self.expandedSectionHeaderNumber ) {//&& self.expandedSectionHeaderNumber == 1){ //if( self.expandedSectionHeaderNumber == 1 && section == 1) {
            imgRadio.image = UIImage(named: "circle_violet.png")
        }
        else {
            imgRadio.image = UIImage(named: "circle_gray.png")
        }
        imgRadio.tag = section + 1
        header.addSubview(imgRadio)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(RedemptionController.sectionHeaderWasTouched))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            cellPayTm.lblMsgWeWillSendMoneyToPaytm.text = NSLocalizedString(LR_REDEMPTION_WE_WILL_SEND_MONEY_TO_PAYTM, comment: "")//"We will send money to your Paytm account"
            cellPayTm.selectionStyle = .none
            cellPayTm.btnDone.addTarget(self, action: #selector(RedemptionController.btnPayTmDoneTapped(sender:)), for: .touchUpInside)
            cellPayTm.txtRegisteredMobNo.delegate = self
            cellPayTm.txtRegisteredMobNo.text = ""
            
            return cellPayTm
            
        } else if indexPath.section == 1 {
            
            return tableSectionTapped_BankDetails(indexPath: indexPath)
            
        } /*else if indexPath.section == 2 {
            
            if strAddress != "" {
                
                setBorderCornerRadius(anyOb: petroAddressCell.lblAddress, color: .gray)
                petroAddressCell.lblAddress.text = strAddress
                petroAddressCell.btnDone.addTarget(self, action: #selector(self.btnPetroCardAddressDoneTapped(sender:)), for: .touchUpInside)
                
                let gestureTapAddressLbl : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(RedemptionController.tapOnLabelAddress(gesture:)))
                gestureTapAddressLbl.delegate = self
                gestureTapAddressLbl.numberOfTapsRequired = 1
                petroAddressCell.lblAddress.isUserInteractionEnabled = true
                petroAddressCell.lblAddress.addGestureRecognizer(gestureTapAddressLbl)
                
                petroAddressCell.selectionStyle = .none
                return petroAddressCell
                
            } else {
                
//                let addAddressCell : AddBankAndAddressCell = (tblRedeemOptions.dequeueReusableCell(withIdentifier: "AddBankAndAddressCell") as? AddBankAndAddressCell)!
                addBankAndAddressCell.lblMsgWeWillSendMoneyTo.text = NSLocalizedString(LR_AN_IOCL_PETRO_CARD_WILL_SENT_TO_ADDRESS, comment: "")//"An IOCL petro card will be sent to address provided"
                let addaddressStrLocali = NSLocalizedString(LR_ADD_ADDRESS, comment: "")
                addBankAndAddressCell.btnAddBankAndAddress.setTitle(addaddressStrLocali, for: .normal)
                addBankAndAddressCell.btnAddBankAndAddress.tag = indexPath.section
                addBankAndAddressCell.btnAddBankAndAddress.addTarget(self, action: #selector(RedemptionController.btnAddAddressOrAddBankTapped(sender:)), for: .touchUpInside)
                addBankAndAddressCell.selectionStyle = .none
                return addBankAndAddressCell
                
            }
        }*/ else if indexPath.section == 2 {
            let zifyCashCell : ZifyCashCell = (tblRedeemOptions.dequeueReusableCell(withIdentifier: "ZifyCashCell") as? ZifyCashCell)!
            zifyCashCell.selectionStyle = .none
            zifyCashCell.btnDone.addTarget(self, action: #selector(RedemptionController.btnZifyCashDoneTapped(sender:)), for: .touchUpInside)

            return zifyCashCell
        }
        
        return UITableViewCell()

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            selectedIndex = indexPath.row
            
            for i in 0 ..< arrBankAcccounts.count {
                let path:IndexPath = IndexPath.init(row: i, section: 1)
                let bankAccountDetailCell : BankAccountDetailCell = tblRedeemOptions.cellForRow(at: path) as! BankAccountDetailCell
                bankAccountDetailCell.imgSelectRadio.image = UIImage.init(named: "circle_gray.png")
            }
            
            let bankAccountDetailCell1 : BankAccountDetailCell = tblRedeemOptions.cellForRow(at: indexPath) as! BankAccountDetailCell
            bankAccountDetailCell1.imgSelectRadio.image = UIImage.init(named: "circle_violet.png")
            
            bankAccountDetails = arrBankAcccounts[indexPath.row] as? BankAccountDetails
            dictForSrvc["bankDetailId"] = bankAccountDetails.bankDetailId
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: - Expand / Collapse Methods
    
    func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        
        selectedIndex = nil
        for i in 0 ..< 4 {
            let secHeaderView = tblRedeemOptions.headerView(forSection: i)
            let imgViewRadioSelected = secHeaderView?.viewWithTag(i + 1) as? UIImageView
            imgViewRadioSelected?.image = UIImage.init(named: "circle_gray.png")
        }
        
        dictForSrvc = [:]
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        let imgSelectedRadio = headerView.viewWithTag(section + 1) as? UIImageView
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!, isectionItems: sectionItems[section] as! Int , imgRadio: imgSelectedRadio!)
           /* if section == 2 {
                let pathToLastIndex = IndexPath.init(row: 0, section: section)
                self.tblRedeemOptions.scrollToRow(at: pathToLastIndex, at: .top, animated: true)
            }*/
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!, isectionItems: sectionItems[self.expandedSectionHeaderNumber] as! Int, imgRadio: imgSelectedRadio!)
                
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!, isectionItems: sectionItems[self.expandedSectionHeaderNumber] as! Int, imgRadio: imgSelectedRadio!)
                tableViewExpandSection(section, imageView: eImageView!, isectionItems: sectionItems[section] as! Int , imgRadio: imgSelectedRadio!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView, isectionItems : Int, imgRadio: UIImageView) {
        self.expandedSectionHeaderNumber = -1;
        
        if isectionItems == 0 {
            return;
        } else {
            /*if section == 2 {
                strAddress = ""
                //self.expandedSectionHeaderNumber = section
            }*/
            
            imgRadio.image = UIImage.init(named: "circle_gray.png")
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< isectionItems {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tblRedeemOptions!.beginUpdates()
            self.tblRedeemOptions!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tblRedeemOptions!.endUpdates()
            
            self.tblRedeemOptions.reloadData()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView, isectionItems : Int, imgRadio: UIImageView) {
        
        if isectionItems == 0 {
            self.expandedSectionHeaderNumber = -1
            return
        } else {
            
            
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            imgRadio.image = UIImage.init(named: "circle_violet.png")
            var indexesPath = [IndexPath]()
            for i in 0 ..< isectionItems {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tblRedeemOptions!.beginUpdates()
            self.tblRedeemOptions!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tblRedeemOptions!.endUpdates()
            
            if section == 1 {
                if arrBankAcccounts.count == 0 {
                    if isGetBankServiceCall == false {
                        isGetBankServiceCall = true
                        //callService_GetBankDetailLists()
                    }
                }
            }
        }
        
    }
    
    func tableSectionTapped_BankDetails(indexPath : IndexPath) -> UITableViewCell {
        if self.arrBankAcccounts.count == 2 {
            return showBankDetailCell(indexPath: indexPath)
        } else if self.arrBankAcccounts.count == 1 {
            if (indexPath.row == 0) {
                return showBankDetailCell(indexPath: indexPath)
            } else {
                return showAddBankCell(indexPath: indexPath)
            }
        } else {
            if (indexPath.row == 0) {
                return showAddBankCell(indexPath: indexPath)
            }
            return UITableViewCell()
        }
        
    }
    
    
    func showAddBankCell(indexPath : IndexPath) -> UITableViewCell {
        //let addBankCell : AddBankAndAddressCell = (tblRedeemOptions.dequeueReusableCell(withIdentifier: "AddBankAndAddressCell") as? AddBankAndAddressCell)!
        addBankAndAddressCell.lblMsgWeWillSendMoneyTo.text = NSLocalizedString(LR_REDEMPTION_YOUR_MONEY_WILL_ADDED_TO_YOUR_BANK_AC, comment: "")//"Your Money will add to your Bank Account"
        let addBankStrLocali = NSLocalizedString(LR_ADD_BANK, comment: "")
        addBankAndAddressCell.btnAddBankAndAddress.setTitle(addBankStrLocali, for: .normal)
        addBankAndAddressCell.btnAddBankAndAddress.tag = indexPath.section
        addBankAndAddressCell.btnAddBankAndAddress.addTarget(self, action: #selector(RedemptionController.btnAddAddressOrAddBankTapped(sender:)), for: .touchUpInside)
        addBankAndAddressCell.selectionStyle = .none
        return addBankAndAddressCell
    }
    
    func showBankDetailCell(indexPath : IndexPath) -> UITableViewCell {
        bankAccountDetails = arrBankAcccounts[indexPath.row] as? BankAccountDetails
        let bankAccountDetailCell : BankAccountDetailCell = (tblRedeemOptions.dequeueReusableCell(withIdentifier: "BankAccountDetailCell") as? BankAccountDetailCell)!
        bankAccountDetailCell.lblBankName.text = bankAccountDetails.bankName
        bankAccountDetailCell.btnDone.addTarget(self, action: #selector(self.btnDoneBankTapped(sender:)), for: .touchUpInside)
        bankAccountDetailCell.btnDone.tag = 20
        let bankAccountNoStr = String.init(format: "Acc No:%@", (bankAccountDetails.accountNumber)!)
        let attributedString = NSMutableAttributedString(string: bankAccountNoStr)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: NSRange.init(location: 7, length: bankAccountNoStr.count - 7))
        bankAccountDetailCell.lblBankAccountNo.attributedText = attributedString
        
        if indexPath.row == arrBankAcccounts.count - 1 {
            bankAccountDetailCell.btnDone.isHidden = false
        } else {
            bankAccountDetailCell.btnDone.isHidden = true
        }
        bankAccountDetailCell.selectionStyle = .none
        
        
        if selectedIndex != nil &&  indexPath.row == selectedIndex {
            bankAccountDetails = arrBankAcccounts[indexPath.row] as? BankAccountDetails
            dictForSrvc["bankDetailId"] = bankAccountDetails.bankDetailId
            bankAccountDetailCell.imgSelectRadio.image = UIImage.init(named: "circle_violet.png")
        } else {
            bankAccountDetailCell.imgSelectRadio.image = UIImage.init(named: "circle_gray.png")
        }
        
        return bankAccountDetailCell
    }
    
    // MARK:- TextField Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == cellPayTm.txtRegisteredMobNo {
            let pointInTable: CGPoint? = textField.superview?.convert(textField.frame.origin, to: tblRedeemOptions)
            var contentOffset: CGPoint = tblRedeemOptions.contentOffset
            contentOffset.y = (pointInTable?.y ?? 0.0) - (textField.inputAccessoryView?.frame.size.height ?? 0.0)
            print("contentOffset is: \(NSStringFromCGPoint(contentOffset))")
            tblRedeemOptions.setContentOffset(contentOffset, animated: true)
            return true
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == cellPayTm.txtRegisteredMobNo {
            if (textField.superview?.superview is UITableViewCell) {
                let cell = textField.superview?.superview as? UITableViewCell
                var indexPath: IndexPath? = nil
                if let aCell = cell {
                    indexPath = tblRedeemOptions.indexPath(for: aCell)
                }
                if let aPath = indexPath {
                    tblRedeemOptions.scrollToRow(at: aPath, at: .middle, animated: true)
                }
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == cellPayTm.txtRegisteredMobNo {
            tblRedeemOptions.isScrollEnabled = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        tblRedeemOptions.isScrollEnabled = true
        if textField == txtRedeemableAmt {
            if (txtRedeemableAmt.text?.isEmpty)! {
                selectDeSelectMultipleButtonFromText(txtField: txtRedeemableAmt)
                
            } else {
                if Float(txtRedeemableAmt.text!)! > Float(lblAmount.text!)! {
                    showAlertView(strMessage: NSLocalizedString(LR_THE_VALUE_MUST_BE_LESSTHAN_OR_EQUAL_TO_REDEEMABLE_ACC, comment: ""), navigationCtrl: self.navigationController!)
                    txtRedeemableAmt.text = ""
                    selectDeSelectMultipleButtonFromText(txtField: txtRedeemableAmt)
                }
                
                else if Int(txtRedeemableAmt.text!)! % 500 == 0 {
                    debugPrint("Redeemable amount is acceptable value")
                    selectDeSelectMultipleButtonFromText(txtField: txtRedeemableAmt)
                    
                }  else {
                    showAlertView(strMessage: NSLocalizedString(LR_THE_VALUE_MUST_BE_IN_MULTIPLES_OF_ONLY, comment: ""), navigationCtrl: self.navigationController!)
                    txtRedeemableAmt.text = ""
                    selectDeSelectMultipleButtonFromText(txtField: txtRedeemableAmt)   
                }
            }
        }
        
        if textField == (cellPayTm.txtRegisteredMobNo)! {
            if textField.text == "" {
                dictForSrvc["mobile"] = nil
            } else {
                dictForSrvc["mobile"] = Int(cellPayTm.txtRegisteredMobNo.text!) as AnyObject
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == cellPayTm.txtRegisteredMobNo {
            guard let text = textField.text else {
                return true
            }
            let lengthOfChars = text.count + string.count - range.length
            return lengthOfChars <= 10
        }
        return true
    }
    
    func selectDeSelectMultipleButtonFromText(txtField : UITextField) {
        if (txtField.text?.isEmpty)! {
            let btnTemp = UIButton.init()
            btnTemp.setTitle(txtField.text!, for: .normal)
            btnAmountTapped(btnTemp)
            
            isTableView(isEnable: false)
        } else {
            //Amount Buttons Auto Selecting from Textfield Text
            let btnTemp = UIButton.init()
            btnTemp.setTitle(txtField.text!, for: .normal)
            btnAmountTapped(btnTemp)
        }
    }
    
    func isTableView(isEnable: Bool) {
        if isEnable == false {
            
            btnRedeem.isEnabled = false
            btnRedeem.alpha = 0.5
            viewRedeemptionOptions.alpha = 0.3
            tblRedeemOptions.alpha = 0.3
            tblRedeemOptions.isUserInteractionEnabled = false
            
        } else {
            viewRedeemptionOptions.alpha = 1
            tblRedeemOptions.alpha = 1
            tblRedeemOptions.isUserInteractionEnabled = true
            btnRedeem.isEnabled = true
            btnRedeem.alpha = 1
        }
    }
    
  
    // MARK:- Service Call
    
    func callService_GetBankDetailLists() {
        
        let reqGetBankAccount : BankAccountRequest = BankAccountRequest.init(requestType: SAVEDBANKDETAILS)
        
        self.messageHandler.showBlockingLoadView {
            ServerInterface.sharedInstance().getResponse(reqGetBankAccount) { (response:ServerResponse?, error) in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    self.viewContent.isHidden = false
                    
                    if response != nil {
                        self.arrBankAcccounts = response?.responseObject as! Array<AnyObject>
                        debugPrint(self.arrBankAcccounts)
                        
                        self.iBankCount = self.arrBankAcccounts.count
                        if self.arrBankAcccounts.count != 2 {
                            self.iBankCount = self.arrBankAcccounts.count + 1
                        }
                        //self.isGetBankServiceCall = true
                        self.sectionItems.replaceObject(at: 1, with: self.iBankCount)
                        
                        self.tblRedeemOptions.reloadData()
                        
                    } else {
                        self.isGetBankServiceCall = false
                    }
                    self.tblRedeemOptions.reloadData()

                })
            }
        }
        
        
        
    }
    
    
    func callService_RedeemZifyCoins(strRedeemFrom : String) {
        let redemptionReq = RedemptionRequest()
        self.messageHandler.showBlockingLoadView {
            ServerInterface.sharedInstance().getResponse(redemptionReq) { (response: ServerResponse?, error)  in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.navigationController?.popViewController(animated: true)

                        if strRedeemFrom == "FromPaytm" {
                            MoEngageEventsClass.callLRRedeemRequestEvent(withPoints:         dictForSrvc["amount"] as? Int ?? 0)
                            showAlertView(strMessage: NSLocalizedString(LR_REDEMPTION_ZIFY_COINS_TRANSFERED_TO_PAYTM_ACCOUNT, comment: ""), navigationCtrl: self.navigationController!)
                            
                            EmailClass.sendMail(strLRCoins: "\(dictForSrvc["amount"] ?? "0" as AnyObject)", iRequestType: 0, strWithdrawlAmount: "")
                            
                            //let emailClass = EmailClass()
                            //emailClass.sendMail(strLRCoins: "\(dictForSrvc["amount"] ?? "0" as AnyObject)", strRequestType: EmailClass.EmailRequestType.REDEEM.rawValue, strWithdrawlAmount: "")
                            
                            
                        } else if strRedeemFrom == "FromBankAccount" {
                            showAlertView(strMessage: NSLocalizedString(LR_YOUR_ZIFY_COINS_TRANSFRED_TO_YOUR_BANK_ACC_WITH_THREE_WRKNG_DAYS, comment: ""), navigationCtrl: self.navigationController!)
                        } else if strRedeemFrom == "FromPetroCard" {
                            showAlertView(strMessage: NSLocalizedString(LR_YOUR_PETRO_CARD_WILL_BE_SENT_WITHIN_SEVEN_WORKNG_DAYS_TO_THE_ADD, comment: ""), navigationCtrl: self.navigationController!)
                        } else if strRedeemFrom == "FromZifyCash" {
                            showAlertView(strMessage: NSLocalizedString(LR_YOUR_ZIFY_COINS_WILL_CONVERTED_TO_ZIFY_CASH_WITHIN_ONE_WRKNG_DAY, comment: ""), navigationCtrl: self.navigationController!)
                        }
                        self.navigationController?.popViewController(animated: true)
                        //debugPrint(response ?? "nil values are printing")
                        
                    } else {
                       // debugPrint("\(String(describing: error))")
                       // debugPrint((error! as NSError).userInfo)
                        let strErr = (error as NSError?)?.userInfo["message"]
                        self.serviceFailure(strError: strErr as! String)
                        
                    }
                    
                })
            }
        }
        
    }
    
    func serviceFailure(strError: String) {
        if strError == "insufficient_recharge_error" {
            showAlertView(strMessage: NSLocalizedString(LR_INSUFFICIENT_BALANCE, comment: ""), navigationCtrl: self.navigationController!)
        }
        
    }
    
    @IBAction func btnBackTapped(sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}




class MainCell: UITableViewCell {
    @IBOutlet weak var lblRedemtionOptionName: UILabel!
}

class PaytmCell: UITableViewCell {
    @IBOutlet weak var txtRegisteredMobNo: UITextField!
    @IBOutlet weak var lblMsgWeWillSendMoneyToPaytm: UILabel!
    @IBOutlet weak var btnDone: UIButton!
}

class AddBankAndAddressCell: UITableViewCell {
    @IBOutlet weak var lblMsgWeWillSendMoneyTo: UILabel!
    @IBOutlet weak var btnAddBankAndAddress: UIButton!
}

class ZifyCashCell: UITableViewCell {
    @IBOutlet weak var lblMsgZifyCoins: UILabel!
    @IBOutlet weak var btnDone: UIButton!
}

class BankAccountDetailCell : UITableViewCell {
    @IBOutlet weak var imgSelectRadio: UIImageView!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblBankAccountNo: UILabel!
    @IBOutlet weak var btnDone: UIButton!
}

class PetroCardAddressCell: UITableViewCell {
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMsgCardWillSendToYourAddress: UILabel!
    @IBOutlet weak var btnDone: UIButton!

}

// MARK:- Service Request

class RedemptionRequest : ServerRequest {
    
    override func urlparams() -> [AnyHashable : Any]! {
        return dictForSrvc
    }
    
    override func serverURL() -> String! {
        return "/extras/redeemZifyCoins.htm"
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
}






