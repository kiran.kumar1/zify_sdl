//
//  RewardsController.swift
//  zify
//
//  Created by Anurag on 16/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class RewardsController: UIViewController {
    
    @IBOutlet weak var viewRedeem: UIView!
    @IBOutlet weak var viewOffered: UIView!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewIdCardUploadTillProfileVerified: UIView!
    
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var btnRedeem: UIButton!
    @IBOutlet weak var btnIdCardUpload: UIButton!
    
    @IBOutlet weak var objStepSlider: StepSlider!
    @IBOutlet weak var lblZifyCoins: UILabel!
    @IBOutlet weak var lblZifyCoinsText: UILabel!
    @IBOutlet weak var lblRedeemableAmountVal: UILabel!
    @IBOutlet weak var lblZifyCoinsAreRedeemableIn: UILabel!
    @IBOutlet weak var lblCurrentRunningVal: UILabel!
    @IBOutlet weak var lblKmsOfferedVal: UILabel!
    @IBOutlet weak var lblKmsSharedVal: UILabel!
    
    @IBOutlet weak var lblRedeemableAmountText: UILabel!
    @IBOutlet weak var lblCurrentRunningText: UILabel!
    @IBOutlet weak var lblKmsOfferedText: UILabel!
    @IBOutlet weak var lblKmsSharedText: UILabel!
    @IBOutlet weak var lblPleaseTapHereToUploadIdCard: UILabel!
    @IBOutlet weak var btnLRTerms: UIButton!
    
    
    @IBOutlet var messageHandler: MessageHandler!
    var currentUser: UserProfile?
    
    
    var drivenVal:Double = 0
    var pointsArr:NSMutableArray = NSMutableArray()
    
    var wsCoachMarkView = MPCoachMarks()
    var coachMarks = NSArray()
    var langIdentify : String?
    var isGlobalLocale : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        langIdentify = NSLocale.preferredLanguages[0] as String
        debugPrint(langIdentify!)
        
        self.viewRedeem.isHidden = true
        self.viewOffered.isHidden = true
        viewRedeem.dropShadow(isShowShadow: true)
        btnInfo.layer.cornerRadius = btnInfo.frame.size.width / 2
        btnInfo.layer.borderColor = UIColor.init(red: 5/255.0, green: 124/255.0, blue: 238/155.0, alpha: 1).cgColor
        btnInfo.layer.borderWidth = 1
        btnInfo.layer.masksToBounds = true
        
        if UIScreen.main.sizeType == .iPhone5 {
            lblZifyCoins.fontSize = 15
            lblZifyCoinsText.fontSize = 15
            btnRedeem.titleLabel?.fontSize = 16
            lblZifyCoinsAreRedeemableIn.fontSize = 15
            lblRedeemableAmountText.fontSize = 13
            lblCurrentRunningText.fontSize = 13
            lblKmsOfferedText.fontSize = 13
            lblKmsSharedText.fontSize = 13
            lblKmsSharedVal.fontSize = 17
            lblRedeemableAmountVal.fontSize = 17
            lblCurrentRunningVal.fontSize = 17
            lblKmsOfferedVal.fontSize = 17
            lblPleaseTapHereToUploadIdCard.fontSize = 11
            btnIdCardUpload.titleLabel?.fontSize = 16
            
            lblsFontSizeBasedOnLang(lang: langIdentify!, lblTaphereUpld: 9, iBtnIdCard: 15)
            
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblZifyCoins.fontSize = 16
            lblZifyCoinsText.fontSize = 16
            btnRedeem.titleLabel?.fontSize = 17
            lblZifyCoinsAreRedeemableIn.fontSize = 16
            lblRedeemableAmountText.fontSize = 14
            lblCurrentRunningText.fontSize = 14
            lblKmsOfferedText.fontSize = 14
            lblKmsSharedText.fontSize = 14
            lblKmsSharedVal.fontSize = 18
            lblRedeemableAmountVal.fontSize = 18
            lblCurrentRunningVal.fontSize = 18
            lblKmsOfferedVal.fontSize = 18
            lblPleaseTapHereToUploadIdCard.fontSize = 12
            btnIdCardUpload.titleLabel?.fontSize = 17
            
            lblsFontSizeBasedOnLang(lang: langIdentify!, lblTaphereUpld: 10, iBtnIdCard: 16)
            
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblZifyCoins.fontSize = 17
            lblZifyCoinsText.fontSize = 17
            btnRedeem.titleLabel?.fontSize = 18
            lblZifyCoinsAreRedeemableIn.fontSize = 17
            lblRedeemableAmountText.fontSize = 14
            lblCurrentRunningText.fontSize = 14
            lblKmsOfferedText.fontSize = 14
            lblKmsSharedText.fontSize = 14
            lblKmsSharedVal.fontSize = 18
            lblRedeemableAmountVal.fontSize = 18
            lblCurrentRunningVal.fontSize = 18
            lblKmsOfferedVal.fontSize = 18
            lblPleaseTapHereToUploadIdCard.fontSize = 13
            btnIdCardUpload.titleLabel?.fontSize = 18
            
            lblsFontSizeBasedOnLang(lang: langIdentify!, lblTaphereUpld: 11, iBtnIdCard: 17)
        }
        
        let lrTermsStrLocalized = NSLocalizedString(LR_TERMS_AND_CONDITIONS, comment: "")
        btnLRTerms.titleLabel?.attributedText = NSAttributedString(string: lrTermsStrLocalized, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        //self.view.bringSubview(toFront: btnLRTerms)
        self.messageHandler.container.bringSubview(toFront: btnLRTerms)
        
        let currentLocale = CurrentLocale.sharedInstance()
        isGlobalLocale = currentLocale?.isGlobalLocale()
        if isGlobalLocale! {
            // global account
            btnLRTerms.isHidden = true
        } else {
            // local account
            btnLRTerms.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationItem.title = NSLocalizedString(LR_REWARDS_NAV_TITLE, comment: "")
        strAddress = ""
        objStepSlider.labels = ["0 km","100","200","300","400","500 km"]
        self.objStepSlider.labelOffset = 5
        self.objStepSlider.labelOrientation = .down
        self.objStepSlider.labelFont = UIFont.init(name: NormalFont, size: 10)
        self.objStepSlider.labelColor = UIColor.black
        
        
        currentUser = UserProfile.getCurrentUser()!
        //    print("currentUser?.userStatus is\((currentUser?.userStatus)!)")
        if (currentUser?.userStatus)! == "VERIFIED" {
            viewIdCardUploadTillProfileVerified.isHidden = true
            let currentLocale = CurrentLocale.sharedInstance()
            isGlobalLocale = currentLocale?.isGlobalLocale()
            if isGlobalLocale! {
                // global account
                callService_ForGetGlobalRewards()
            } else {
                // local account
                callService_ForGetRewards()
            }
            //callService_ForGetRewards()
        } else {
            
            if((currentUser?.userDocuments) == nil){
            }else if (currentUser?.userDocuments.idCardImgUrl != "") && currentUser?.userDocuments.idCardImgUrl != nil {
                self.btnIdCardUpload.isHidden = true
            }
            viewIdCardUploadTillProfileVerified.isHidden = false
            self.messageHandler.container.bringSubview(toFront: btnLRTerms)
        }
        // for test
        //viewIdCardUploadTillProfileVerified.isHidden = true
        //callService_ForGetRewards()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func lblsFontSizeBasedOnLang(lang : String, lblTaphereUpld : Int, iBtnIdCard : Int) {
        if lang.contains("fr") {
            lblPleaseTapHereToUploadIdCard.fontSize = CGFloat(lblTaphereUpld)
            btnIdCardUpload.titleLabel?.fontSize = CGFloat(iBtnIdCard)
        }
    }
    
    
    func setSliderIndexWithAnimation(valuesKms: Double) {
        
        UIView.animate(withDuration: 1.0, delay: 1.0, options: .curveLinear, animations: {
            
        }, completion: { (finish) in
            self.objStepSlider.setIndex(valuesKms/100, animated: true)
            ///*** Car image animate according to the Step Slider value ***///
            self.setCarImgAnimateToKmsPointval(valuesKms: valuesKms)
            
        })
        
    }
    
    func setCarImgAnimateToKmsPointval(valuesKms: Double) {
        
        let pointValueForProgress:Double = Double(objStepSlider.frame.size.width/5)
        print("pointValueForProgress is \(pointValueForProgress)")
        let progressBarWidth:Double = Double((valuesKms * pointValueForProgress)/100);
        print("progressBarWidth is \(progressBarWidth)")
        let noOfPointsCrossed:Int = Int(progressBarWidth/pointValueForProgress);
        print("noOfPointsCrossed is \(noOfPointsCrossed)")
        
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.imgCar.frame.origin = CGPoint.init(x: progressBarWidth, y: Double(self.imgCar.frame.origin.y))//CGPoint.init(x: progressBarWidth, y: self.imgCar.frame.origin.y)//CGRect.init(x: 0, y: 0, width: progressBarWidth, height: self.lblProgress.frame.size.height);
            
            print("progressBarWidth is hiii)")
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
            self.coloringTheExceedPoints(noOfPointsCrossed: noOfPointsCrossed)
        })
        
    }
    
    func setMaskScreen() {
        
        UserDefaults.standard.set(true, forKey: "MaskScreen")
        UserDefaults.standard.synchronize()
        
        var padY:CGFloat = 64;
        if(UIScreen.main.sizeType == .iPhoneXAndXS){padY = 88;}
        if(UIScreen.main.sizeType == .iPhoneXSMax){padY = 88;}

        
        let rectFrameBtnRedeem = CGRect.init(x: btnRedeem.frame.origin.x, y: btnRedeem.frame.origin.y+padY, width: btnRedeem.frame.size.width, height: btnRedeem.frame.size.height)
        let nsValue = NSValue.init(cgRect: rectFrameBtnRedeem)
        let myDict = ["rect" : nsValue,
                      "shape": NSNumber.init(value: 0),
                      "caption": NSLocalizedString(LR_REWARDS_TUTORIAL, comment: ""),//"1 KM = 1 ZIFY COIN \n You can redeem your zify coins here \n Zify coins are redeemable in multiples of 500 only",
            "position":NSNumber.init(value: 0),
            "showArrow":NSNumber.init(value: true)
            ] as [String : Any]
        
        coachMarks = [myDict]
        wsCoachMarkView = MPCoachMarks.init(frame: AppDelegate.getInstance().window.bounds, coachMarks: coachMarks as! [Any])
        wsCoachMarkView.animationDuration = 0.5
        wsCoachMarkView.enableContinueLabel = true
        wsCoachMarkView.continueLocation = ContinueLocation(rawValue: 0)!
        
        wsCoachMarkView.enableSkipButton = false
        wsCoachMarkView.maskColor = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
        
        self.navigationController?.view.addSubview(wsCoachMarkView)
        wsCoachMarkView.start()
        
    }
    
    
    
    /* func addPointsOnTheLabel() -> Void {
     debugPrint(lblProgress.frame)
     print("i is \(AppDelegate.screen_WIDTH())")
     
     let progressBar: UIProgressView = UIProgressView.init(frame: CGRect.init(x: 0, y: 0, width: lblProgress.frame.size.width, height: lblProgress.frame.size.height));
     progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 20)
     
     
     progressBar.backgroundColor = UIColor.red
     progressBar.progressTintColor = UIColor.red
     let lblProgressbar:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: lblProgress.frame.size.height));
     lblProgressbar.backgroundColor = UIColor.init(red: 209.0/255.0, green: 122.0/255.0, blue: 61.0/255.0, alpha: 1.0)
     lblProgress.addSubview(progressBar)
     
     
     let pointValue:CGFloat = (AppDelegate.screen_WIDTH() - 2*15)/5
     print("pointValue is \(pointValue)")
     
     var xPoint:CGFloat = 15;
     for i:Int in 0...5 {
     makeCircle(xFrame: xPoint, iValue: i)
     xPoint = xPoint + pointValue
     }
     
     drivenVal = 350;
     let pointValueForProgress:CGFloat = lblProgress.frame.size.width/5
     print("pointValueForProgress is \(pointValueForProgress)")
     
     let progressBarWidth:CGFloat = (drivenVal*pointValueForProgress)/100;
     print("progressBarWidth is \(progressBarWidth)")
     let noOfPointsCrossed:Int = Int(progressBarWidth/pointValueForProgress);
     print("noOfPointsCrossed is \(noOfPointsCrossed)")
     
     
     UIView.animate(withDuration: 8.0, animations: {() -> Void in
     lblProgressbar.frame = CGRect.init(x: 0, y: 0, width: progressBarWidth, height: self.lblProgress.frame.size.height)
     self.imgCar.frame.origin = CGPoint.init(x: progressBarWidth, y: self.imgCar.frame.origin.y ) //CGRect.init(x: progressBarWidth, y: 0, width: 100, height: 100);
     
     print("progressBarWidth is hiii)")
     
     }, completion: {(_ finished: Bool) -> Void in
     print("Completed")
     self.coloringTheExceedPoints(noOfPointsCrossed: noOfPointsCrossed)
     })
     }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeCircle(xFrame : CGFloat, iValue: Int) {
        
        print("x is \(xFrame)")
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let pad:CGFloat = 10;
        let width:CGFloat = lblProgress.frame.size.height + pad;
        shapeLayer.frame = CGRect.init(x:(xFrame - width/2), y:  (lblProgress.frame.origin.y - pad/2), width: width, height: width)
        shapeLayer.cornerRadius = width/2;
        shapeLayer.fillRule = kCAFillRuleEvenOdd;
        
        
        let lblAmount: UILabel = UILabel()
        lblAmount.text = "\((iValue * 100))"
        lblAmount.font = UIFont.init(name: "OpenSans-Regular", size: 10)
        lblAmount.sizeToFit()
        lblAmount.frame.origin.x = xFrame - 5
        lblAmount.frame.origin.y = lblProgress.frame.origin.y + 20
        viewRedeem.addSubview(lblAmount)
        
        
        pointsArr.add(shapeLayer)
        shapeLayer.backgroundColor = UIColor.init(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1.0).cgColor
        viewRedeem.layer.addSublayer(shapeLayer)
        
        
    }
    
    func coloringTheExceedPoints(noOfPointsCrossed:Int) -> Void {
        for i in 0..<pointsArr.count {
            if i < noOfPointsCrossed+1 {
                let  obj:CAShapeLayer = pointsArr[i] as! CAShapeLayer
                obj.backgroundColor = UIColor.init(red: 126.0/255.0, green: 204.0/255.0, blue: 52.0/255.0, alpha: 1.0).cgColor
            }
        }
    }
    
    class func createRewardsNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "LoyaltyAndRewardProgram", bundle: Bundle.main)
        let moreOptionsNavController = storyBoard.instantiateViewController(withIdentifier: "createRewardsNavController") as? UINavigationController
        return moreOptionsNavController
    }
    
    // MARK:- Actions
    
    @IBAction func btnLRTerms(_ sender: Any) {
        self.present(LRTermsController.createLRTermsNavController()!, animated: true, completion: nil)
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnRedeemTapped(_ sender: Any) {
        let redemptionVc = self.storyboard?.instantiateViewController(withIdentifier: "RedemptionController") as? RedemptionController
        
        redemptionVc?.strAmountFromRewards = lblZifyCoins.text!
        self.navigationController?.pushViewController(redemptionVc!, animated: true)
    }
    
    @IBAction func btnInfoTapped(_ sender: Any) {
        self.setMaskScreen()
    }
    @IBAction func btnTappedIdCardUpload(_ sender: Any) {
        
        if((currentUser?.userDocuments) == nil){
            self.moveToIdCardScreen()
        }else if (currentUser?.userDocuments.idCardImgUrl == "") || currentUser?.userDocuments.idCardImgUrl == nil {
            let defs = UserDefaults.standard
            defs.set(true, forKey: "forPush")
            defs.synchronize()
            self.moveToIdCardScreen()
        }
    }
    func moveToIdCardScreen()  {
        
        if(currentUser?.userPreferences == nil){
            self.moveToRiderIdCardScreen()
            return;
        }else if (currentUser?.userPreferences.userMode == "RIDER") {
            self.moveToRiderIdCardScreen()
        }else {
            //self.moveToDriverIdCardScreen()
            self.moveToRiderIdCardScreen()
        }
        
        
        
    }
    func moveToRiderIdCardScreen()  {
        let sb = UIStoryboard(name: "IDCard", bundle: nil)
        let vc: UIViewController = sb.instantiateViewController(withIdentifier: "UserIDCardController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToDriverIdCardScreen()  {
        let sb = UIStoryboard(name: "IDCard", bundle: nil)
        let vc: UIViewController = sb.instantiateViewController(withIdentifier: "UserIDDrivingLicenceController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK:- Service Call
    
    func callService_ForGetRewards() {
        let rewardReq = RewardsRequest()
        
        self.messageHandler.showBlockingLoadView {
            ServerInterface.sharedInstance().getResponse(rewardReq) { (response: ServerResponse?, error) in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    
                    print(response ?? "nil values printing")
                    if response?.messageCode == "1" {
                        
                        self.parsingResponse(dictResponse: response?.responseObject as! Dictionary<String, AnyObject>)
                        
                        self.viewRedeem.isHidden = false
                        self.viewOffered.isHidden = false
                        
                        let isShow = UserDefaults.standard.value(forKey: "MaskScreen") as? Bool
                        if isShow == nil || isShow == false {
                            self.setMaskScreen()
                        }
                        
                    } else {
                        let strErr = (error as NSError?)?.userInfo["message"]
                        self.serviceFailure(strError: strErr as! String)
                    }
                })
            }
        }
        
    }
    
    func callService_ForGetGlobalRewards() {
        let rewardReqGlobal = RewardsRequestForGlobal()
        
        self.messageHandler.showBlockingLoadView {
            ServerInterface.sharedInstance().getResponse(rewardReqGlobal) { (response: ServerResponse?, error) in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    
                    print(response ?? "nil values printing")
                    if response?.messageCode == "1" {
                        
                        self.parsingResponse(dictResponse: response?.responseObject as! Dictionary<String, AnyObject>)
                        
                        self.viewRedeem.isHidden = false
                        self.viewOffered.isHidden = false
                        
                        let isShow = UserDefaults.standard.value(forKey: "MaskScreen") as? Bool
                        if isShow == nil || isShow == false {
                            self.setMaskScreen()
                        }
                        
                    } else {
                        let strErr = (error as NSError?)?.userInfo["message"]
                        self.serviceFailure(strError: strErr as! String)
                    }
                })
            }
        }
        
    }
    
    // MARK:- Service Response
    
    func parsingResponse(dictResponse : Dictionary<String, AnyObject>) {
        debugPrint(dictResponse)
        
        let lrDetailsResObj = dictResponse["lrDetails"]! as? NSDictionary
        lblZifyCoins.text = "\(lrDetailsResObj!.value(forKey: "zifyCoins")!)" //+ "600"
        lblRedeemableAmountVal.text = "\(lrDetailsResObj!.value(forKey: "redeemableAmt")!)"
        
        let currentRunning = lrDetailsResObj!.value(forKey: "currentRunningKm")! as? Double
        //currentRunning = currentRunning.rounded()
        lblCurrentRunningVal.text = "\(currentRunning ?? 0.0)"
        
        let kmsOffered = lrDetailsResObj!.value(forKey: "kmsOffered")! as? Double
        //kmsOffered = kmsOffered.rounded()
        lblKmsOfferedVal.text = "\(kmsOffered ?? 0.0)"
        
        //let kmsShared = lrDetailsResObj!.value(forKey: "kmsShared")! as? Double
        //kmsShared = kmsShared.rounded()
        let kmsSharedValue = String(format: "%.02f", lrDetailsResObj!.value(forKey: "kmsShared")! as? Double ?? "0.0" )
        lblKmsSharedVal.text = "\(kmsSharedValue)"
        
        drivenVal = lrDetailsResObj!.value(forKey: "currentRunningKm")! as? Double ?? 0.0
        if drivenVal > 500 {
            drivenVal = 500
        }
        self.setSliderIndexWithAnimation(valuesKms: drivenVal)
        btnRedeem.isEnabled = false
        btnRedeem.alpha = 0.7
        if lrDetailsResObj!.value(forKey: "zifyCoins") as! Int >= 500 {
            let currentLocale : CurrentLocale = CurrentLocale.sharedInstance()
            let isGlobal:Bool = currentLocale.isGlobalLocale() as Bool
            if(!isGlobal){
            btnRedeem.isEnabled = true
            btnRedeem.alpha = 1.0
            }
        } else {
            btnRedeem.isEnabled = false
            btnRedeem.alpha = 0.7
        }
        
        // to test
        //btnRedeem.isEnabled = true
        //btnRedeem.alpha = 1.0
       
    }
    
   
    func serviceFailure(strError: String) {
        showAlertView(strMessage: strError, navigationCtrl: self.navigationController!)
    }
    
}


class RewardsRequest : ServerRequest {
    
    override func urlparams() -> [AnyHashable : Any]! {
        var params : [String : AnyObject] = [:]
        
        params["userToken"] = UserProfile.getCurrentUserToken() as AnyObject
        let currentLocale : CurrentLocale = CurrentLocale.sharedInstance()
        params["isGlobal"] = currentLocale.isGlobalLocale() as AnyObject
        return params
    }
    
    override func serverURL() -> String! {
        return "/extras/getZifyCoinsDetails"
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
}

class RewardsRequestForGlobal : ServerRequest {
    // "http://test.zify.co/zifyreferandearn/referandearn/getLrDetails?user_id=998458244&country_code=DE"
    override func urlparams() -> [AnyHashable : Any]! {
        let param : [String : AnyObject] = [
            "user_id" : UserProfile.getCurrentUserId()! as AnyObject,
            "country_code" : UserProfile.getCurrentUser()?.country as AnyObject
        ]
        return param
    }
    override func serverURL() -> String! {
        return "/zifyreferandearn/referandearn/getLrDetails"
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
    override func isJSONRequestFormat() -> Bool {
        return false
    }
    
    override func overrideBaseURL() -> Bool {
        return true
    }
    override func isForReferAndEarn() -> Bool {
        return true
    }
    
    
}

