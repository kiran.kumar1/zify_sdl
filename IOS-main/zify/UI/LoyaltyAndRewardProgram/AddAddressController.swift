//
//  AddAddressController.swift
//  zify
//
//  Created by Anurag on 18/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class AddAddressController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var txtViewAddAddress: UITextView!
    
    let placeholderLabel = UILabel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtViewAddAddress.delegate = self
        placeholderLabel.text = NSLocalizedString(LR_ADDRESS_DETAILS, comment: "")//"House No / Street / Area / Land Mark / City / State / Pincode"

        //placeholderLabel.text = "House No / Street / Area / Land Mark / City / State / Pincode"
        placeholderLabel.font = UIFont.init(name: "OpenSans-Regular", size: 15) //UIFont.italicSystemFont(ofSize: (txtViewAddAddress.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        placeholderLabel.numberOfLines = 2
        //txtViewAddAddress.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (txtViewAddAddress.font?.pointSize)! / 2)
        placeholderLabel.frame.size.height = 35

        placeholderLabel.textColor = UIColor.lightGray
//        placeholderLabel.isHidden = !txtViewAddAddress.text.isEmpty
        placeholderLabel.frame.size.width = txtViewAddAddress.frame.size.width - 60
        
        
        txtViewAddAddress.layer.borderWidth = 1
        txtViewAddAddress.layer.borderColor = UIColor.lightGray.cgColor
        txtViewAddAddress.layer.cornerRadius = 5
        txtViewAddAddress.layer.masksToBounds = true
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txtViewAddAddress.text = strAddress
        if strAddress == "" {
            txtViewAddAddress.addSubview(placeholderLabel)
        } else {
            placeholderLabel.removeFromSuperview()//isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        txtViewAddAddress.resignFirstResponder()
        if txtViewAddAddress.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            showAlertView(strMessage: NSLocalizedString(LR_PLEASE_ENTER_ADD, comment: ""), navigationCtrl: self.navigationController!)

            //showAlertView(strMessage: "Please enter Address", navigationCtrl: self.navigationController!)
            return
        }
        strAddress = txtViewAddAddress.text
        print(strAddress)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnCancelTapped(_ sender: Any) {
        txtViewAddAddress.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TextView Delegates
    
    func textViewDidChange(_ textView: UITextView) {
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if range.location == 0 {
            if text == " " {
                return false
            }
            if text == "" {
                txtViewAddAddress.addSubview(placeholderLabel)
                return true
            } else {
                placeholderLabel.removeFromSuperview()
                return true
            }
        } else if range.location >= 300 {
            return false
        }
        
        return true
    }
    
    func textViewShouldReturn(textView: UITextView!) -> Bool {
        self.view.endEditing(true)
        return true
    }
   

}



