//
//  LRTermsController.swift
//  zify
//
//  Created by Anurag on 10/10/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class LRTermsController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblLRTerms: UITableView!
    
    var arrTerms : [String] = [NSLocalizedString(LR_OFFER_APPLICABLE_IN_HYD_ONLY, comment: ""),
                    NSLocalizedString(LR_YOU_CAN_AVAIL_REWARDS_ONLY_MAX_OF_TWO_TRIPS_A_DAY, comment: ""),
                    NSLocalizedString(LR_NOT_APPLICABLE_FOR_OUTSTATION_TRAVEL, comment: ""),
                    NSLocalizedString(LR_MAX_COINS_CAN_EARNED_PER_TRIP, comment: ""),
                    NSLocalizedString(LR_CAR_OWNER_HAS_TO_START_AND_COMPLETE_DRIVE_TO_ELIGIBLE_LR, comment: ""),
                    NSLocalizedString(LR_RIDER_HAS_TO_BOARD_AND_COMPLETE_RIDE_TO_BE_ELIGIBLE_LR, comment: ""),
                    NSLocalizedString(LR_PROFILE_SHOULD_VERIFIED_FOR_A_USER_TO_ELIGIBLE_LR, comment: ""),
                    NSLocalizedString(LR_IT_TAKES_WORKING_DAYS_TO_REDEEM_REQ, comment: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
         Offer Terms and Condition
         Offer Applicable in Hyderabad ONLY.
         You can avail Rewards only on a maximum of two trips in a day.
         Loyalty and Rewards Program is NOT applicable for outstation travel.
         Maximum 25 coins can be earned per trip.
         Car Owner has to “Start Drive” and “Complete Drive” to be eligible for Loyalty and Reward Program.
         Rider has to “Board” and “Complete Ride” to be eligible for Loyalty and Reward Program.
         Profile should be verified for a user to be eligible for Loyalty and Reward Program.
         It takes 3-4 working days for us to process all kinds of Redeem Requests.
         
         LR_OFFER_TERMS_CONDITIONS
         LR_OFFER_APPLICABLE_IN_HYD_ONLY
         LR_YOU_CAN_AVAIL_REWARDS_ONLY_MAX_OF_TWO_TRIPS_A_DAY
         LR_NOT_APPLICABLE_FOR_OUTSTATION_TRAVEL
         LR_MAX_COINS_CAN_EARNED_PER_TRIP
         LR_CAR_OWNER_HAS_TO_START_AND_COMPLETE_DRIVE_TO_ELIGIBLE_LR
         LR_RIDER_HAS_TO_BOARD_AND_COMPLETE_RIDE_TO_BE_ELIGIBLE_LR
         LR_PROFILE_SHOULD_VERIFIED_FOR_A_USER_TO_ELIGIBLE_LR
         LR_IT_TAKES_WORKING_DAYS_TO_REDEEM_REQ
         
         
         */
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnTappedCancel(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - UITableView Delegates
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 64
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView:UIView =  UIView()
        headerView.backgroundColor = UIColor.white
        let lblHeader = UILabel.init(frame: CGRect.init(x: 5, y: 0, width: AppDelegate.screen_WIDTH() - 5, height: 64))
        lblHeader.text = NSLocalizedString(LR_OFFER_TERMS_CONDITIONS, comment: "")
        
        lblHeader.font = UIFont.init(name: "OpenSans-SemiBold", size: 18)
        if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblHeader.font = UIFont.init(name: "OpenSans-SemiBold", size: 19)
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblHeader.font = UIFont.init(name: "OpenSans-SemiBold", size: 20)
        }
        
        
        lblHeader.textColor = UIColor.init(hex: "435B6C")
        headerView.addSubview(lblHeader)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTerms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellLRTerms = self.tblLRTerms.dequeueReusableCell(withIdentifier: "LRTermsCell") as? LRTermsCell
        cellLRTerms?.lblTerms.text = arrTerms[indexPath.row]
        return cellLRTerms!
    }
    
    class func createLRTermsNavController() -> UINavigationController? {
        //LRTermsNavController
        let storyBoard = UIStoryboard.init(name: "LoyaltyAndRewardProgram", bundle: Bundle.main)
        let profilePicNavCtrl = storyBoard.instantiateViewController(withIdentifier: "LRTermsNavController") as? UINavigationController
        return profilePicNavCtrl
    }
    
}


class LRTermsCell : UITableViewCell {
    
    @IBOutlet weak var lblTerms: UILabel!
}
