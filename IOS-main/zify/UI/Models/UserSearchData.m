//
//  UserSearchData.m
//  zify
//
//  Created by Anurag S Rathor on 01/05/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "UserSearchData.h"

@implementation UserSearchData
-(id)initWithSourceLocality:(LocalityInfo *)sourceLocality andDestLocality:(LocalityInfo *)destinationLocality andRideStartDate:(NSString *)rideStartDate andRideDateValue:(NSString *)rideDateValue andRideTimeValue:(NSString *)rideTimeValue andMerchantId:(NSString *)merchantId{
    self = [super init];
    if (self) {
        _sourceLocality = sourceLocality;
        _destinationLocality = destinationLocality;
        _rideStartDate = rideStartDate;
        _rideDateValue = rideDateValue;
        _rideTimeValue = rideTimeValue;
        _merchantId = merchantId;
    }
    return self;
}
@end
