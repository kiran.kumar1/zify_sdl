//
//  UserSearchData.h
//  zify
//
//  Created by Anurag S Rathor on 01/05/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalityInfo.h"
@interface UserSearchData : NSObject
@property (nonatomic,strong) LocalityInfo *sourceLocality;
@property (nonatomic,strong) LocalityInfo *destinationLocality;
@property (nonatomic,strong) NSString *rideStartDate;
@property (nonatomic,strong) NSString *rideDateValue;
@property (nonatomic,strong) NSString *rideTimeValue;
@property (nonatomic,strong) NSString *merchantId;
-(id)initWithSourceLocality:(LocalityInfo *)sourceLocality andDestLocality:(LocalityInfo *)destinationLocality andRideStartDate:(NSString *)rideStartDate andRideDateValue:(NSString *)rideDateValue andRideTimeValue:(NSString *)rideTimeValue andMerchantId:(NSString *)merchantId;
@end
