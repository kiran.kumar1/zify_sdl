//
//  LocalityInfo.h
//  zify
//
//  Created by Anurag S Rathor on 22/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocalityInfo : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *city;
@property(nonatomic,strong) NSString *state;
@property(nonatomic,strong) NSString *country;
@property(nonatomic) CLLocationCoordinate2D latLng;
-(id)initWithName:(NSString *)name andAddress:(NSString *)address andCity:(NSString *)city andState:(NSString *)state andCountry:(NSString *)country andLatLng:(CLLocationCoordinate2D)latLng;
- (void)encodeWithCoder:(NSCoder *)aCoder;
-(id)initWithCoder:(NSCoder *)aDecoder;

@end
