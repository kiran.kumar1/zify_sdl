//
//  LocalityInfo.m
//  zify
//
//  Created by Anurag S Rathor on 22/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "LocalityInfo.h"

@implementation LocalityInfo
-(id)initWithName:(NSString *)name andAddress:(NSString *)address andCity:(NSString *)city andState:(NSString *)state andCountry:(NSString *)country andLatLng:(CLLocationCoordinate2D)latLng{
    self = [super init];
    if (self) {
        _name = name;
        _address = address;
        _city = city;
        _state = state;
        _country = country;
        _latLng = latLng;
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.address forKey:@"address"];
    [aCoder encodeObject:self.city forKey:@"city"];
    [aCoder encodeObject:self.country forKey:@"country"];

    NSNumber *lat = [NSNumber numberWithDouble:_latLng.latitude];
    NSNumber *lon = [NSNumber numberWithDouble:_latLng.longitude];
    NSDictionary *userLocation=@{@"lat":lat,@"long":lon};
    [aCoder encodeObject:userLocation forKey:@"coordinate"];
    NSLog(@"address is %@, lat = %f, long = %f",self.address,[lat stringValue],[lon stringValue]);


}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.address = [aDecoder decodeObjectForKey:@"address"];
        self.city = [aDecoder decodeObjectForKey:@"city"];
        self.country = [aDecoder decodeObjectForKey:@"country"];
        NSDictionary *userLoc = [aDecoder decodeObjectForKey:@"coordinate"];
        self.latLng = CLLocationCoordinate2DMake([[userLoc objectForKey:@"lat"] doubleValue], [[userLoc objectForKey:@"long"] doubleValue]);
        NSLog(@"address is %@, lat = %f, long = %f",self.address,self.latLng.latitude,self.latLng.longitude);


        
    }
    return self;
}
@end
