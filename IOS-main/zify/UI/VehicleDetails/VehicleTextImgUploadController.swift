//
//  VehicleTextImgUploadController.swift
//  zify
//
//  Created by Anurag on 21/08/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import CropViewController

class VehicleTextImgUploadController: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate, ImageSelectionViewerDelegate, CropViewControllerDelegate {
    
    @IBOutlet weak var lblVehicleModel: UILabel!
    @IBOutlet weak var lblVehicleRegistrationNumber: UILabel!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var imgCar: UIImageView!
    
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var lblVehicleDetailsDocEncryptedSecurlyStored: UILabel!
    
    let picker = UIImagePickerController()
    var userProfileImage : UIImage? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let txt1 = NSLocalizedString(UOS_VEHICLEDETAILS_LBL_SECURETEXTLINE1 , comment: "")
        let txt2 = NSLocalizedString(UOS_IDCARD_LBL_SECURETEXTLINE2 , comment: "")
        self.lblVehicleDetailsDocEncryptedSecurlyStored.text = txt1 + "\n" + txt2
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapOnCarImage))
        gesture.numberOfTouchesRequired = 1
        gesture.delegate = self
        imgCar.addGestureRecognizer(gesture)
        let user = UserProfile.getCurrentUser()
        lblVehicleModel.text = user?.userDocuments.vehicleModel
        lblVehicleRegistrationNumber.text = user?.userDocuments.vehicleRegistrationNum
        
        if UIScreen.main.sizeType == .iPhone5 {
            lblVehicleDetailsDocEncryptedSecurlyStored.fontSize = 11
            btnUpload.titleLabel?.fontSize = 18
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblVehicleDetailsDocEncryptedSecurlyStored.fontSize = 13
            btnUpload.titleLabel?.fontSize = 18
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblVehicleDetailsDocEncryptedSecurlyStored.fontSize = 13
            btnUpload.titleLabel?.fontSize = 19
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func dismiss(_ sender: Any) {
        self.messageHandler.dismissErrorView()
        self.messageHandler.dismissSuccessView()
        let prefs = UserDefaults.standard
        let isForPush: Bool = prefs.bool(forKey: "forPush")
        if isForPush {
            self.navigationController?.popViewController(animated: true)
            return
        }
        dismiss(animated: false)
    }
    
    @IBAction func btnTappedUpload(_ sender: Any) {
        let isSuccess = self.validateUserInfo()
        if(isSuccess){
            self.uploadVehicleImage()
        }
    }
    @IBAction func btnTappedEditImage(_ sender: Any) {
        displayPhotoPickingOptions(sender)
    }
    
    // MARK: - Service call
    func getCarDetails() {
        
    }
    
    func tapOnCarImage() {
        displayPhotoPickingOptions(self)
    }
    
    
    
    
    func validateUserInfo() -> Bool {
        if(self.userProfileImage == nil){
            messageHandler.showErrorMessage(NSLocalizedString(VC_VEHICLEDETAILS_IMAGEVAL, comment: ""))
            return false
        }
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let imageSel = info[UIImagePickerControllerEditedImage] as? UIImage else {
            print("No image found")
            return
        }
        imgCar.image = imageSel
        self.dismiss(animated: true, completion: nil)
        //btnSetAndUpload.setTitle(NSLocalizedString(SS_UPLOAD_BTN, comment: ""), for: .normal)
        //btnEditProfilePic.isHidden = false
        imgCar.image = imageSel
        userProfileImage = imageSel
    }
    
    func uploadVehicleImage(){
        let imageUploadrequest = UserUploadMultiData(uploadType: VEHICLE_TYPE)
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(imageUploadrequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            
                            var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                            let imagePath = resultDict["responseText"] as! String
                            //response?.responseObject["responseText"] as? String
                            self.updateImagePathToServer(imagePth: imagePath)
                        }
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            }, andImage: self.userProfileImage)
        })
    }
    
    
    func updateImagePathToServer(imagePth:String) {
        messageHandler.showBlockingLoadView(handler: {
            let profile = UserProfile.getCurrentUser()
            let model = profile?.userDocuments.vehicleModel
            let regNo = profile?.userDocuments.vehicleRegistrationNum
            
            let updateRequest = VehicleDetailUpdateRequest(vehicleModel: model, andVehicleRegistrationNumber: regNo, andInsuranceNumber: "", andDrivingLicenseNumber: "", andWithVehicleImagePah:imagePth, withUpdate: VEHICLE_IMAGE_TYPE, withMobileNumber: "", withIsoCode: "", withCountryCode:"")
            ServerInterface.sharedInstance().getResponse(updateRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            self.messageHandler.dismissSuccessView()
                            self.messageHandler.dismissErrorView()
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    
    // MARK:- ImageSelectiponViewer Delegate
    func displayPhotoPickingOptions(_ sender: Any?) {
        ImageSelectionViewer.sharedInstance().imageSelectionDelegate = self
      //  ImageSelectionViewer.sharedInstance().finalImageSize = CGSize(width: 320.0, height: 320.0)
        ImageSelectionViewer.sharedInstance().selectImage()
    }
    
   /* func userSelectedFinalImage(_ image: UIImage?) {
        userProfileImage = image
        self.imgCar.image = ImageHelper.image(with: image, scaledTo: self.imgCar.frame.size)
    }*/
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        let finalImage:UIImage = ImageHelper.image(with: image, scaledTo: self.imgCar.frame.size)
        userProfileImage = finalImage
        self.imgCar.image = finalImage
        self.dismiss(animated: true, completion:nil)
        
    }
    @objc func openCropImgeVc(withImage:UIImage) -> Void {
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default, image: withImage)
        cropController.delegate = self
        self.present(cropController, animated: false, completion: nil)
    }
    
    
    func userSelectedFinalImage(_ image: UIImage?) {
        self.perform(#selector(openCropImgeVc), with: image, afterDelay: 0.0)
    }
    
    func allowImageEditing() -> Bool {
        return false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
