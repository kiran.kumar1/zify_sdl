//
//  VehicleDetailsImageController.swift
//  zify
//
//  Created by Anurag on 21/08/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import CropViewController
class VehicleImageController: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate, ImageSelectionViewerDelegate,CropViewControllerDelegate {

    @IBOutlet weak var lblHeader1: UILabel!
    @IBOutlet weak var lblHeader2: UILabel!
    @IBOutlet weak var lblHeader3: UILabel!
    @IBOutlet weak var lblVehicleDetailsCannotBeEditOnceStored: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var imgCar: UIImageView!
    

    @IBOutlet var messageHandler: MessageHandler!

    var userProfileImage : UIImage? = nil
    let picker = UIImagePickerController()
    var isNavDirectly:Bool = Bool()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.sizeType == .iPhone5 {
            self.lblHeader1.fontSize = 23
            self.lblHeader2.fontSize = 23
            self.lblHeader3.fontSize = 23
            btnUpload.titleLabel?.fontSize = 18
            btnSkip.titleLabel?.fontSize = 18
            lblVehicleDetailsCannotBeEditOnceStored.fontSize = 12
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            self.lblHeader1.fontSize = 25
            self.lblHeader2.fontSize = 25
            self.lblHeader3.fontSize = 25
            btnUpload.titleLabel?.fontSize = 19
            btnSkip.titleLabel?.fontSize = 19
            lblVehicleDetailsCannotBeEditOnceStored.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            self.lblHeader1.fontSize = 26
            self.lblHeader2.fontSize = 26
            self.lblHeader3.fontSize = 26
            btnUpload.titleLabel?.fontSize = 20
            btnSkip.titleLabel?.fontSize = 20
            lblVehicleDetailsCannotBeEditOnceStored.fontSize = 14
        }
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapOnCarImage))
        gesture.numberOfTouchesRequired = 1
        gesture.delegate = self
        imgCar.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func btnTappedSkip(_ sender: Any) {
        
        self.dismiss();
        
    }
    func dismiss() {
        self.messageHandler.dismissSuccessView()
        self.messageHandler.dismissErrorView()
        if(self.isNavDirectly){
            self.navigationController?.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func btnTappedUpload(_ sender: Any) {
        let isSuccess = self.validateUserInfo()
        if(isSuccess){
            self.uploadVehicleImage()
        }
        
    }
    
    func validateUserInfo() -> Bool {
        if(self.userProfileImage == nil){
            messageHandler.showErrorMessage(NSLocalizedString(VC_VEHICLEDETAILS_IMAGEVAL, comment: ""))
            return false
        }
        return true
    }
    @IBAction func btnTappedEditImage(_ sender: Any) {
        displayPhotoPickingOptions(sender)
    }
    
    
    
    func tapOnCarImage() {
        displayPhotoPickingOptions(self)
    }
    
    // MARK:- ImageSelectiponViewer Delegate
    func displayPhotoPickingOptions(_ sender: Any?) {
        ImageSelectionViewer.sharedInstance().imageSelectionDelegate = self
        //ImageSelectionViewer.sharedInstance().finalImageSize = self.imgCar.frame.size//CGSize(width: 320.0, height: 320.0)
        ImageSelectionViewer.sharedInstance().selectImage()
    }
    
    @objc func openCropImgeVc(withImage:UIImage) -> Void {
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default , image: withImage)
        cropController.delegate = self
        self.present(cropController, animated: false, completion: nil)
        
        
        
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        let finalImage:UIImage = ImageHelper.image(with: image, scaledTo: self.imgCar.frame.size)
        userProfileImage = finalImage
        self.imgCar.image = finalImage
        self.dismiss(animated: true, completion:nil)
    }
    
    func userSelectedFinalImage(_ image: UIImage?) {
        self.perform(#selector(openCropImgeVc), with: image, afterDelay: 0.0)
    }
    
    func allowImageEditing() -> Bool {
        return false;
    }
    
    func uploadVehicleImage(){
        let imageUploadrequest = UserUploadMultiData(uploadType: VEHICLE_TYPE)
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(imageUploadrequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            
                            var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                            let imagePath = resultDict["responseText"] as! String
                            //response?.responseObject["responseText"] as? String
                            self.updateImagePathToServer(imagePth: imagePath)
                        }
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            }, andImage: self.userProfileImage)
        })
    }
    
    
    func updateImagePathToServer(imagePth:String) {
        messageHandler.showBlockingLoadView(handler: {
            let profile = UserProfile.getCurrentUser()
            let model = profile?.userDocuments.vehicleModel
            let regNo = profile?.userDocuments.vehicleRegistrationNum

            let updateRequest = VehicleDetailUpdateRequest(vehicleModel: model, andVehicleRegistrationNumber: regNo, andInsuranceNumber: "", andDrivingLicenseNumber: "", andWithVehicleImagePah:imagePth, withUpdate: VEHICLE_IMAGE_TYPE, withMobileNumber: "", withIsoCode: "", withCountryCode:"")
            ServerInterface.sharedInstance().getResponse(updateRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            self.dismiss()
                        }
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
