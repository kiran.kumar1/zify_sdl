//
//  VehicleDetailsAvailableController.swift
//  zify
//
//  Created by Anurag on 21/08/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import SDWebImage

class VehicleDetailsAvailableController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var lblVehicleModelText: UILabel!
    @IBOutlet weak var lblVehicleModel: UILabel!
    @IBOutlet weak var lblVehicleNumberText: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var lblVehicleDetailsCanBeEditedContSupport: UILabel!
    @IBOutlet weak var lblYourVehicleDetailsAreSecurlyStored: UILabel!
    @IBOutlet weak var btnSupportTeam: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateUserVehicleDetails()
        let supportStringLocalized = NSLocalizedString(UP_SUPPORT_TEAM, comment: "")
        lblVehicleDetailsCanBeEditedContSupport.text = NSLocalizedString(UP_VEHICLE_DETAILS_CANNOTBE_EDITED_CONTACT , comment: "")//"Vehicle details cannot be edited, contact"
        btnSupportTeam.titleLabel?.attributedText = NSAttributedString(string: supportStringLocalized, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        let txt1 = NSLocalizedString(UOS_VEHICLEDETAILS_LBL_SECURETEXTLINE1 , comment: "")
        let txt2 = NSLocalizedString(UOS_IDCARD_LBL_SECURETEXTLINE2 , comment: "")
        self.lblYourVehicleDetailsAreSecurlyStored.text = txt1 + "\n" + txt2
        
        if UIScreen.main.sizeType == .iPhone5 {
            self.lblVehicleDetailsCanBeEditedContSupport.fontSize = 12
            self.btnSupportTeam.titleLabel?.fontSize = 12
            self.lblYourVehicleDetailsAreSecurlyStored.fontSize = 11
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            self.lblVehicleDetailsCanBeEditedContSupport.fontSize = 13
            self.btnSupportTeam.titleLabel?.fontSize = 13
            self.lblYourVehicleDetailsAreSecurlyStored.fontSize = 13
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            self.lblVehicleDetailsCanBeEditedContSupport.fontSize = 13
            self.btnSupportTeam.titleLabel?.fontSize = 14
            self.lblYourVehicleDetailsAreSecurlyStored.fontSize = 13
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateUserVehicleDetails() {
        let currentUser = UserProfile.getCurrentUser()
        let userDocuments: UserDocuments? = currentUser?.userDocuments
        //let vehicleImagePlaceHolder = UIImage(named: "Car_ImageTemp")
        imgCar.isHidden = true
        
        imgCar.sd_setImage(with: URL(string: userDocuments?.vehicleImgUrl ?? "")) { (img, err, cacheTypr, imgUrl) in
            if img != nil {
            self.imgCar.image = ImageHelper.image(with: img, scaledTo: self.imgCar.frame.size)
            self.imgCar.isHidden = false
            }
            //self.imgCar.image = CGImage?.toFitSize(imgCar.frame.size, method: MGImageResizeCrop)
        }
        
        //        self.imgCar.sd_setImage(with: URL(string: userDocuments?.vehicleImgUrl ?? ""), placeholderImage: vehicleImagePlaceHolder) { (image, error, cacheType, imageURL) in
        //            self.imgCar.hidden = false
        //            self.imgCar.image = image?.toFitSize(imgCar.frame.size, method: MGImageResizeCrop)
        //        }
        
        
        //        imgCar.sd_setImage(with: URL(string: userDocuments?.vehicleImgUrl ?? ""), placeholderImage: vehicleImagePlaceHolder, completed: { image, error, cacheType, imageURL in
        //            imgCar.hidden = false
        //            imgCar.image = image?.toFitSize(imgCar.frame.size, method: MGImageResizeCrop)
        //        })
        lblVehicleModel.text = userDocuments?.vehicleModel
        lblVehicleNumber.text = userDocuments?.vehicleRegistrationNum
        //drivingLicenseNumber.text = userDocuments?.drivingLicenceNum
        //insuranceNumber.text = userDocuments?.vehicleInsuranceNum
    }
    
    
    // MARK: - Actions
    @IBAction func dismiss(_ sender: Any) {
        
        let prefs = UserDefaults.standard
        let isForPush: Bool = prefs.bool(forKey: "forPush")
        if isForPush {
            self.navigationController?.popViewController(animated: true)
            return
        }
        dismiss(animated: false)
    }
    
    @IBAction func btnTappedContactSupport(_ sender: Any) {
        
        let userProfile = UserProfile.getCurrentUser()
        let subjectStr = NSLocalizedString(VEHICLE_RO_MESSAGE_SUBJECT, comment: "")
        let emailStr = "\(String(describing: userProfile!.emailId!))"
        let nameStr = "\(String(describing: userProfile!.firstName!)) \(String(describing: userProfile!.lastName!))".capitalized
        let messageStr = String(format: NSLocalizedString(VEHICLE_RO_MESSAGE_MESSAGE, comment: ""), emailStr, nameStr)
        let dictionary = ["subject": subjectStr, "messageBody": messageStr, "recipients": "support@zify.co"]
        AppUtilites.shareEmail(withData: dictionary, with: self as MFMailComposeViewControllerDelegate)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true) {
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
