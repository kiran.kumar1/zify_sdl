//
//  VehicleTextController.swift
//
//
//  Created by Anurag on 21/08/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit



class VehicleTextController: UIViewController, UITextFieldDelegate, VehicleModelSelectionDelegate {

    let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

    @IBOutlet weak var lblHeader1: UILabel!
    @IBOutlet weak var lblHeader2: UILabel!
    @IBOutlet weak var lblHeader3: UILabel!
    @IBOutlet weak var lblVehicleDetailsCannotBeEditedOnceSaved: UILabel!

    
    @IBOutlet weak var txtVehicleBrand: UITextField!
    @IBOutlet weak var txtVehicleModel: UITextField!
    @IBOutlet weak var txtVehicleRegistrationNumber: UITextField!
    @IBOutlet weak var btnSaveAndNext: UIButton!

    @IBOutlet var messageHandler: MessageHandler!
    
    var  brandName = ""
    var  isNavigatedDirectly:Bool = Bool()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addLeftView(textFielld: txtVehicleBrand)
        self.addLeftView(textFielld: txtVehicleModel)
        self.addLeftView(textFielld: txtVehicleRegistrationNumber)
        if UIScreen.main.sizeType == .iPhone5 {
            self.lblHeader1.fontSize = 24
            self.lblHeader2.fontSize = 24
            self.lblHeader3.fontSize = 24
            btnSaveAndNext.titleLabel?.fontSize = 18
            lblVehicleDetailsCannotBeEditedOnceSaved.fontSize = 12
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            self.lblHeader1.fontSize = 27
            self.lblHeader2.fontSize = 27
            self.lblHeader3.fontSize = 27
            btnSaveAndNext.titleLabel?.fontSize = 19
            lblVehicleDetailsCannotBeEditedOnceSaved.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            self.lblHeader1.fontSize = 32
            self.lblHeader2.fontSize = 32
            self.lblHeader3.fontSize = 32
            btnSaveAndNext.titleLabel?.fontSize = 20
            lblVehicleDetailsCannotBeEditedOnceSaved.fontSize = 14
        }
    }
    func addLeftView(textFielld:UITextField)  {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: 10))
        textFielld.leftView = view;
        textFielld.leftViewMode = .always
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func btnTappedSaveAndNext(_ sender: Any) {
        let isSuccess = self.validateUserInfo()
        if(isSuccess){
            self.updateUserProfileWithImagePath()
        }
    }
    func validateUserInfo() -> Bool {
     if(brandName.count == 0){
        messageHandler.showErrorMessage(NSLocalizedString(profile_vehicleDetails_brand_val, comment: ""))
        return false
        }
        if(txtVehicleModel.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0){
            messageHandler.showErrorMessage(NSLocalizedString(VC_VEHICLEDETAILS_MODELVAL, comment: ""))
            return false
        }
        if(txtVehicleRegistrationNumber.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0){
          messageHandler.showErrorMessage(NSLocalizedString(VC_VEHICLEDETAILS_REGNUMVAL, comment: ""))
            return false
        }
        return true;
    }
    
    func selectedModelName(_ model: String?) {
        brandName = model!
        txtVehicleBrand.text = brandName
        txtVehicleBrand.textColor = UIColor.black
    }

    
    func updateUserProfileWithImagePath() {
        messageHandler.showBlockingLoadView(handler: {
            let vehicleInfoStr  = self.txtVehicleBrand.text! + " - " + self.txtVehicleModel.text!
            let updateRequest = VehicleDetailUpdateRequest(vehicleModel: vehicleInfoStr, andVehicleRegistrationNumber: self.txtVehicleRegistrationNumber.text, andInsuranceNumber: "", andDrivingLicenseNumber: "", andWithVehicleImagePah:"", withUpdate: VEHICLE_IMAGE_TYPE, withMobileNumber: "", withIsoCode: "", withCountryCode:"")
            ServerInterface.sharedInstance().getResponse(updateRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            self.moveToUploadVehicleImageScreen()
                        }
                        
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    // MARK: - UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtVehicleBrand {
            textField.resignFirstResponder()
            
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == txtVehicleRegistrationNumber {
//            let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//            let filtered = string.components(separatedBy: cs).joined(separator: "")
//            return string == filtered
//        }
//        return true
//    }
    
        
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if (segue.identifier == "showVehicleModel") {
                let autoSuggestionController = segue.destination as? VehicleModelAutoSuggestionController
                autoSuggestionController?.selectionDelegate = self 
            }
        }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func moveToUploadVehicleImageScreen()  {
        let vehicleImgControoler = self.storyboard?.instantiateViewController(withIdentifier: "VehicleImageController") as? VehicleImageController
        vehicleImgControoler?.isNavDirectly = isNavigatedDirectly
        self.navigationController?.pushViewController(vehicleImgControoler!, animated: true)
    }
    
    class func createVehcileTextNavController(isNavPresent:Bool) -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "UserProfile", bundle: Bundle.main)
        let vehicleDetailsNavController = storyBoard.instantiateViewController(withIdentifier: "vehicleTextNavController") as? UINavigationController
        let vehicleTextController = vehicleDetailsNavController?.topViewController as? VehicleTextController
        vehicleTextController?.isNavigatedDirectly = true;
        return vehicleDetailsNavController
    }
    
    class func createVehcileTextController() -> UIViewController? {
        let storyBoard = UIStoryboard(name: "UserProfile", bundle: Bundle.main)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VehicleTextController") as? VehicleTextController
        viewController?.isNavigatedDirectly = false
        return viewController;
    }
    
    
    @IBAction func dismissScreen(_ sender: Any) {
        
        self.messageHandler.dismissSuccessView()
        self.messageHandler.dismissErrorView()
        if isNavigatedDirectly {
            navigationController?.dismiss(animated: false)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}
