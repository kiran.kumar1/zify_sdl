//
//  AddMoneyController.swift
//  zify
//
//  Created by Anurag on 06/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class AddMoneyController: ScrollableContentViewController, UIAlertViewDelegate, RazorpayPaymentDelegate, StripePaymentDelegate {
   
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet var amount: UITextField!
    @IBOutlet var amountButtons: [UIButton]!
    @IBOutlet var addMoney: UIButton!
    @IBOutlet var lblAddMoney: UILabel!
    
    var amountArray: [AnyObject] = []
    var zifyPaymentRefId = ""
    var isViewInitialised = false
    var currencySymbol = ""
    var currencyCode = ""
    var razorpayController: RazorpayPaymentController?
    var rechargeAmount = ""
    var arrList: [Any] = []
    
    let kOFFSET_FOR_KEYBOARD = 80.0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIScreen.main.sizeType == .iPhone5 {
            amount.font = UIFont.systemFont(ofSize: 15)
            addMoney.titleLabel?.fontSize = 17
            lblAddMoney.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6 {
            amount.font = UIFont.systemFont(ofSize: 16)
            addMoney.titleLabel?.fontSize = 18
            lblAddMoney.fontSize = 15
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            amount.font = UIFont.systemFont(ofSize: 16)
            addMoney.titleLabel?.fontSize = 20
            lblAddMoney.fontSize = 16
        }
        initialiseView()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("recharge amount is \(rechargeAmount)")
        if !isViewInitialised {
            // [self fetchUserWallet];
            isViewInitialised = true
            scrollview.isHidden = false
            addMoney.isHidden = false
            if rechargeAmount != "" {
                amount.text = rechargeAmount
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    // MARK:- Actions
    @IBAction func selectAmount(_ sender: UIButton) {
        for index in 0..<amountButtons.count {
            let button = amountButtons[index] as? UIButton
            if sender == button {
                amount.text = amountArray[index] as? String
                //button.backgroundColor = [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1.0];
                button?.backgroundColor = UIColor(red: 67.0 / 255.0, green: 91.0 / 255.0, blue: 108.0 / 255.0, alpha: 1.0)
                button?.setTitleColor(UIColor.white, for: .normal)
                //435B6C
            } else {
                button?.backgroundColor = UIColor.white
                button?.setTitleColor(UIColor(red: 67.0 / 255.0, green: 91.0 / 255.0, blue: 108.0 / 255.0, alpha: 1.0), for: .normal)
                button?.layer.borderColor = UIColor(red: 67.0 / 255.0, green: 91.0 / 255.0, blue: 108.0 / 255.0, alpha: 1.0).cgColor
            }
        }
    }
    
    @IBAction func dismiss(_ sender: Any) {
      self.dismissVc()
        
    }
    func dismissVc()  {
        self.messageHandler.dismissErrorView()
        self.messageHandler.dismissSuccessView()
        let storedValue: Bool = UserDefaults.standard.bool(forKey: "toAddMoney")
        if storedValue == true {
            UserDefaults.standard.set(false, forKey: "toAddMoney")
            UserDefaults.standard.synchronize()
            dismiss(animated: true)
            
            //storedValue.set(false, forKey: "WalletPush")
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func proceed(toPayment sender: UIButton) {
        self.view.endEditing(true)
        if validateAmount() {
            let isRazorpayPaymentEnabled = CurrentLocale.sharedInstance().isRazorpayPaymentEnabled()
            if isRazorpayPaymentEnabled {
                messageHandler.showBlockingLoadView(handler: {
                    ServerInterface.sharedInstance().getResponse(RazorpayPaymentPersistRequest(requestType: PAYMENTREQUEST, andAmount: self.amount.text, andCurrency: "INR"), withHandler: { response, error in
                        self.messageHandler.dismissBlockingLoadView(handler: {
                            if((response) != nil){
                            let messageCode:Int = Int((response?.messageCode)!)!
                            if messageCode == 1 {
                                var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                                print("result dict is \(resultDict)")
                                let refId = resultDict["responseText"] as! String
                                if( refId.count > 0){
                                    self.zifyPaymentRefId = refId
                                    self.razorpayController?.initiatePayment(withAmount: self.amount.text)
                                }else{
                                    
                                }
                                }
                                
                            }
                            /*if response != nil && !(response?.responseObject is NSNull) && !((response?.responseObject as? String) == "") {
                                self.zifyPaymentRefId = response?.responseObject as! String
                                self.razorpayController?.initiatePayment(withAmount: self.amount.text)
                            }*/ else {
                                self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as? String)
                            }
                             })
                    })
                })
            } else {
                messageHandler.showBlockingLoadView(handler: {
                    ServerInterface.sharedInstance().getResponse(StripePaymentPersistRequest(requestType: STRIPEPAYMENTREQUEST, andAmount: self.amount.text, andCurrency: self.currencyCode), withHandler: { response, error in
                        self.messageHandler.dismissBlockingLoadView(handler: {
                            if((response) != nil){
                            let messageCode:Int = Int((response?.messageCode)!)!
                            if messageCode == 1 {
                                var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                                print("result dict is \(resultDict)")
                                let refId = resultDict["responseText"] as! String
                                if( refId.count > 0){
                                    self.zifyPaymentRefId = refId ;
                                    let stripePaymentNavController: UINavigationController? = StripePaymentController.createStripePaymentNavController()
                                    let stripePaymentController = stripePaymentNavController?.topViewController as? StripePaymentController
                                    stripePaymentController?.amount = self.amount.text
                                    stripePaymentController?.paymentDelegate = self
                                    if let aController = stripePaymentNavController {
                                        self.present(aController, animated: true)
                                    }
                                }else{
                                    
                                }
                                }
                            } else {
                                self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                            }
                            } )
                    })
                })
            }
        }
    }
    
    func validateAmount() -> Bool {
        
        if (amount.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count == 0) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_WALLET_AMOUNTVAL, comment: ""))
            return false
        }
        
        let iamount: Float = Float(amount.text!)!
        if iamount >= 1000  {
        messageHandler.showErrorMessage(NSLocalizedString(VC_WALLET_AMOUNTVAL_LESSTHAN_THOUSAND, comment: ""))
           // UIAlertController .showError(withMessage: NSLocalizedString(VC_WALLET_AMOUNTVAL_LESSTHAN_THOUSAND, comment: ""), inViewController: self)
            return false
        }
        let amountValue: Float = Float(amount.text!)!
        if amountValue <= 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_WALLET_AMOUNTVAL_ZERO, comment: ""))
            return false
        }
        return true
    }
    
    func initialiseView() {
        scrollview.isHidden = true
        addMoney.isHidden = true
        isViewInitialised = false
        let currentLocale = CurrentLocale.sharedInstance()
        amountArray = (currentLocale?.getRechargeAmounts()! as [AnyObject]?)!
        currencySymbol = (currentLocale?.getCurrencySymbol())!
        currencyCode = (currentLocale?.getCurrencyCode())!
        //
        let placeHolderStr = "\(NSLocalizedString(WS_WALLET_TXT_ENTERAMOUNT, comment: "")) (\(currencySymbol))"
        amount.placeholder = placeHolderStr
        amount.layer.borderColor = UIColor(red: 210.0 / 255.0, green: 210.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0).cgColor
        for index in 0..<amountButtons.count {
            let button = amountButtons[index] as? UIButton
            let title = "+ \(currencySymbol)\(amountArray[index])"
            button?.setTitle(title, for: .normal)
            button?.setTitle(title, for: .selected)
            //button.layer.borderColor = [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1.0].CGColor;
        }
        razorpayController = RazorpayPaymentController()
        razorpayController?.paymentDelegate = self
    }
    
    // MARK:- RazorPayment Delegate Method
    func razorpayPaymentSuccuess(withPaymentId paymentId: String?) {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(RazorpayPaymentPersistRequest(requestType: PAYMENTRESPONSE, andPaymentId: paymentId, andZifyPaymentRefId: self.zifyPaymentRefId, andFailureCode: nil, andFailureDescription: nil), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if((response) != nil){
                    let messageCode:Int = Int((response?.messageCode)!)!
                    if messageCode == 1 {
                        //self.fetchUserWallet()
                        self.callMoengageEventForRecharge(withZifyRefId: self.zifyPaymentRefId, andWithPaymentId: paymentId!,andWithStatus: true)
                        self.dismissVc()
                     }
                    }
                    else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                    
                })
            })
        })
    }
    func razorpayPaymentFailed(withCode code: Int32, andMessage message: String!) {
        print("razor pay error code is\(Int32(code))")
        ServerInterface.sharedInstance().getResponse(RazorpayPaymentPersistRequest(requestType: PAYMENTRESPONSE, andPaymentId: nil, andZifyPaymentRefId: zifyPaymentRefId, andFailureCode: Int32(code) as NSNumber, andFailureDescription: message), withHandler: { response, error in
        })
        self.callMoengageEventForRecharge(withZifyRefId: self.zifyPaymentRefId, andWithPaymentId: "",andWithStatus: false)

        var msg:String = message
        if(code == 2){
            msg = NSLocalizedString(payment_cancelled_user, comment: "")
        }
        if( msg.count == 0){
            msg = NSLocalizedString(VC_WALLET_TRYAGAINERROR, comment: "")
        }
        messageHandler.showErrorMessage(msg)
    }
    
    func createStripePayment(with token: STPToken?) {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(StripePaymentPersistRequest(requestType: STRIPEPAYMENTRESPONSE, andStripeToken: token?.stripeID, andZifyPaymentRefId: self.zifyPaymentRefId, andFailureCode: nil, andFailureDescription: nil), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        //self.fetchUserWallet()
                        self.callMoengageEventForRecharge(withZifyRefId: self.zifyPaymentRefId, andWithPaymentId: token?.stripeID ?? "", andWithStatus: true)
                        self.dismissVc()

                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    func stripePaymentFailedWithError(_ error: Error!) {
        print("error code is\(Int32(Int(error._code )))")
        ServerInterface.sharedInstance().getResponse(StripePaymentPersistRequest(requestType: STRIPEPAYMENTRESPONSE, andStripeToken: nil, andZifyPaymentRefId: zifyPaymentRefId, andFailureCode: Int32(Int(error._code )) as NSNumber, andFailureDescription: error.localizedDescription), withHandler: { response, error in
        })
        self.callMoengageEventForRecharge(withZifyRefId: self.zifyPaymentRefId, andWithPaymentId:"", andWithStatus: false)

        messageHandler.showErrorMessage(error.localizedDescription)
    }
    
    func fetchUserWallet() {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: RIDERWALLETREQUEST), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let wallet = response?.responseObject as? UserWallet
                        //self.availableCash.text = String.init(format: "%@ %@", self.currencySymbol, (wallet?.zifyCash.stringValue)!)
                        
                        //self.availablePoints.text = String(format: NSLocalizedString(VC_WALLET_PROMOCREDITS, comment: "Credits : {UserPromotionalCredits}"), "\(String(describing: wallet?.availablePoints!))")
                        
                        //self.availablePoints.text = String(format: NSLocalizedString(VC_WALLET_PROMOCREDITS, comment: "Credits : {UserPromotionalCredits}"), "\(wallet?.availablePoints ?? "")")
                        self.scrollview.isHidden = false
                        self.addMoney.isHidden = false
                        self.moveToAddMoneyScreenFromSearchResultToAdd()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    func moveToAddMoneyScreenFromSearchResultToAdd() {
        let isForAddMoney: Bool = UserDefaults.standard.bool(forKey: "toAddMoney")
        if isForAddMoney {
            UserDefaults.standard.set(false, forKey: "toAddMoney")
            UserDefaults.standard.synchronize()
            let sb = UIStoryboard(name: "Wallet", bundle: Bundle.main)
            let userWalletController = sb.instantiateViewController(withIdentifier: "AddMoneyController") as? AddMoneyController
            if let aController = userWalletController {
                navigationController?.pushViewController(aController, animated: true)
            }
        }
    }
    
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == amount && AppDelegate.screen_HEIGHT() <= 568 {
            if view.frame.origin.y >= 0 {
                //[self setViewMovedUp:YES];
            }
        }
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        if AppDelegate.screen_HEIGHT() <= 568 {
            // [self setViewMovedUp:NO];
        }
    }
    
    func setViewMovedUp(_ movedUp: Bool) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3) // if you want to slide up the view
        
        var rect: CGRect = view.frame
        if movedUp {
            rect.origin.y -= CGFloat(kOFFSET_FOR_KEYBOARD)
            rect.size.height += CGFloat(kOFFSET_FOR_KEYBOARD)
        } else {
            // revert back to the normal state.
            rect.origin.y += CGFloat(kOFFSET_FOR_KEYBOARD)
            rect.size.height -= CGFloat(kOFFSET_FOR_KEYBOARD)
        }
        view.frame = rect
        
        UIView.commitAnimations()
    }
    func callMoengageEventForRecharge(withZifyRefId:String, andWithPaymentId:String, andWithStatus:Bool)  {
        let dataDict = ["currency":self.currencyCode,"amount":self.amount.text!, "reference_id":withZifyRefId,"transaction_Id":andWithPaymentId] as [String:Any]

        if(andWithStatus){
            MoEngageEventsClass.callRechargeSuccessEvent(withDataDict: dataDict)
        }else{
            MoEngageEventsClass.callRechargeFailEvent(withDataDict: dataDict)
        }
    }
    
    class func createUserWalletNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Wallet", bundle: Bundle.main)
        let rechargeNavController = storyBoard.instantiateViewController(withIdentifier: "userWalletNavController") as? UINavigationController
        return rechargeNavController
    }
}


