//
//  SetAddressController.swift
//  zify
//
//  Created by Anurag on 26/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class SetAddressController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var viewHomeAddress: UIView!
    @IBOutlet weak var viewWorkAddress: UIView!
    
    @IBOutlet weak var viewIconLblHomeAddress: UIView!
    @IBOutlet weak var viewIconLblWorkAddress: UIView!
    
    @IBOutlet var viewSetHomeDetailAddress: UIView!
    @IBOutlet var viewSetWorkDetailAddress: UIView!
    
    @IBOutlet weak var lblHomeAddress: UILabel!
    @IBOutlet weak var lblWorkAddress: UILabel!
    
    @IBOutlet weak var lblHomeAddressTemp: UILabel!
    @IBOutlet weak var lblWorkAddressTemp: UILabel!
    
    @IBOutlet weak var btnHomeAddress: UIButton!
    @IBOutlet weak var btnWorkAddress: UIButton!
    @IBOutlet weak var btnNext: UIButton!

    var btnSelected:UIButton  = UIButton()
    var isHomeAddressTapped:Bool = Bool()
    var isHomeAddressSelected:Bool = Bool()
    var isofficeAddressSelected:Bool = Bool()
    
    @objc var strTravelTypeInAddress : String? = ""

    
    @IBOutlet var messageHandler:MessageHandler! = MessageHandler()
    var mapLocationHelper:MapLocationHelper = MapLocationHelper()
    var homeAddressInfo:LocalityInfo = LocalityInfo()
    var officeAddressInfo:LocalityInfo = LocalityInfo()
    var currentUser : UserProfile?
    var isViewInitialised:Bool = Bool();


    
    override func viewDidLoad() {
        super.viewDidLoad()
        isViewInitialised = false
        
        self.btnHomeAddress.isHidden = true
        self.btnWorkAddress.isHidden = true

        lblHomeAddressTemp.translatesAutoresizingMaskIntoConstraints = false
        lblWorkAddressTemp.translatesAutoresizingMaskIntoConstraints = false
        
        if UIScreen.main.sizeType == .iPhone5 {
            lblHomeAddressTemp.fontSize = 16
            lblWorkAddressTemp.fontSize = 16
            lblHomeAddress.fontSize = 14
            lblWorkAddress.fontSize = 14
            btnNext.titleLabel?.fontSize = 18
            lblHomeAddressTemp.numberOfLines = 4
            lblWorkAddressTemp.numberOfLines = 4
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR  {
            lblHomeAddressTemp.fontSize = 18
            lblWorkAddressTemp.fontSize = 18
            lblHomeAddress.fontSize = 15
            lblWorkAddress.fontSize = 15
            btnNext.titleLabel?.fontSize = 20
            lblHomeAddressTemp.numberOfLines = 5
            lblWorkAddressTemp.numberOfLines = 5
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblHomeAddressTemp.fontSize = 20
            lblWorkAddressTemp.fontSize = 20
            lblHomeAddress.fontSize = 16
            lblWorkAddress.fontSize = 16
            btnNext.titleLabel?.fontSize = 20
            lblHomeAddressTemp.numberOfLines = 5
            lblWorkAddressTemp.numberOfLines = 5
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.navigationBar.topItem?.title = " "
        
        navigationItem.title = NSLocalizedString(HS_SET_YOUR_ADDRESS, comment: "");
        //navigationController?.navigationBar.barStyle = UIBarStyle.default
        navigationController?.navigationBar.tintColor = UIColor.init(hex: "2662FF")
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(hex: "2662FF")]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(!isViewInitialised){
        
        mapLocationHelper = MapLocationHelper.init(delegae: self)
        showShadow(view: viewHomeAddress, color: .darkGray, opacity: 1, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
        showShadow(view: viewWorkAddress, color: .darkGray, opacity: 1, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
        
        let tapHomeAdd = UITapGestureRecognizer.init(target: self, action: #selector(self.viewTappedHomeAddress))
        tapHomeAdd.delegate = self
        viewHomeAddress.addGestureRecognizer(tapHomeAdd)
        
        let tapWorkAdd = UITapGestureRecognizer.init(target: self, action: #selector(self.viewTappedWorkAddress))
        tapWorkAdd.delegate = self
        viewWorkAddress.addGestureRecognizer(tapWorkAdd)
        
        
        
        self.viewSetHomeDetailAddress.frame.origin = self.viewIconLblHomeAddress.frame.origin
        self.viewSetHomeDetailAddress.frame.size = self.viewIconLblHomeAddress.frame.size
        self.viewSetWorkDetailAddress.frame.origin = self.viewIconLblWorkAddress.frame.origin
        self.viewSetWorkDetailAddress.frame.size = self.viewIconLblWorkAddress.frame.size
            isViewInitialised = true;
        }
        
    }
    
    
    // MARK: - Actions
    
    func viewTappedHomeAddress() {
        isHomeAddressTapped = true
        self.showAddressPicker()
        //        btn.addTarget(self, action: #selector(btnSelecttapped), for: .touchUpInside)
        
    }
    
    func viewTappedWorkAddress() {
        isHomeAddressTapped = false
        self.showAddressPicker()
    }
    
    @IBAction func btnTappedEditHomeAddress(_ sender: Any) {
        isHomeAddressTapped = true
        self.showAddressPicker()
    }
    
    @IBAction func btnTappedEditWorkAddress(_ sender: Any) {
        isHomeAddressTapped = false
        self.showAddressPicker()
    }
    
    // MARK: - Navigation
    
    @IBAction func btnTappedClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTappedNext(_ sender: Any) {
        if(!isHomeAddressSelected){
            UIAlertController.showError(withMessage: NSLocalizedString(TP_SCREEN2_HOME_ADDRESS_ERROR, comment: ""), inViewController: self)
        }else if(!isofficeAddressSelected){
            UIAlertController.showError(withMessage: NSLocalizedString(TP_SCREEN2_OFFICE_ADDRESS_ERROR, comment: ""), inViewController: self)
        }else{
            self.moveToNextScreen()
            
            /* let ownerOrRider = AppDelegate.getInstance().paramsDict.object(forKey: "OwnerOrRider") as! String
             if(ownerOrRider == "RIDER"){
             self.moveToLastScreen()
             }else{
             self.moveToNextScreen()
             // self.getRoutesFromStartAndDestination()
             }*/
        }
    }
    
    func moveToNextScreen() -> Void {
        
        let vcSetTime = self.storyboard?.instantiateViewController(withIdentifier: "SetTimeController") as? SetTimeController
        //vcSetTime?.currentUser = currentUser
        vcSetTime?.strTravelTypeInTime = strTravelTypeInAddress
        self.navigationController?.pushViewController(vcSetTime!, animated: true)
    }
    
    func moveToLastScreen() -> Void {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let obj:TravelPrefStep4 = storyboard.instantiateViewController(withIdentifier: "TpScreen4") as! TravelPrefStep4;        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func btnSelecttapped(sender:UIButton) -> Void {
        print("selected tappped")
        btnSelected = sender
        if(sender.tag == 44){isHomeAddressTapped = true}
        else{isHomeAddressTapped = false}
        self.showAddressPicker()
        
    }
    
    func showAddressPicker() -> Void {
        messageHandler.showBlockingLoadView(handler: {
            self.mapLocationHelper.showPick(onMap: self.navigationController)
        })
    }
    
    // MARK : MapLocationHelperDelegate methods
    func selectedLocationInfo(_ locationInfo: LocalityInfo!, andError error: Error!) {
        if(error != nil){
            self.messageHandler.dismissBlockingLoadView(handler: {
                print("Addess fetch failed")
            })
        }else{
            if(locationInfo != nil){
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if(self.isHomeAddressTapped){
                        if(!self.viewIconLblHomeAddress.isHidden) {
                            self.viewIconLblHomeAddress.isHidden = true
                            self.viewSetHomeDetailAddress.isHidden = false;
                            self.viewSetHomeDetailAddress.frame = self.viewIconLblHomeAddress.frame
                            self.viewHomeAddress.addSubview(self.viewSetHomeDetailAddress)
                            self.btnHomeAddress.isHidden = false
                            
                        }
                        self.homeAddressInfo = locationInfo;
                        AppDelegate.getInstance().homeAddress = locationInfo
                        self.isHomeAddressSelected  = true;
                        self.lblHomeAddress.text = locationInfo.address
                        
                    } else {
                        if(!self.viewIconLblWorkAddress.isHidden){
                            self.viewIconLblWorkAddress.isHidden = true
                            self.viewSetWorkDetailAddress.frame = self.viewIconLblWorkAddress.frame
                            self.viewSetWorkDetailAddress.isHidden = false;
                            self.viewWorkAddress.addSubview(self.viewSetWorkDetailAddress)
                            self.btnWorkAddress.isHidden = false

                        }
                        self.officeAddressInfo = locationInfo
                        AppDelegate.getInstance().officeAddress = locationInfo
                        self.isofficeAddressSelected  = true;
                        self.lblWorkAddress.text = locationInfo.address
                    }
                    print("address is \(locationInfo.address)")
                    
                })
            } else {
                self.messageHandler.dismissBlockingLoadView(handler: {
                    
                })
            }
        }
    }
}
