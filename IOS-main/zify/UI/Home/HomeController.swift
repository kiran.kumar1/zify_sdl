//
//  HomeController.swift
//  zify
//
//  Created by Anurag on 21/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import SideMenu
import MessageUI

enum LocalityPickMode : Int {
    case pickingSourceLocality
    case pickingDestinationLocality
}


class HomeController: UIViewController, DateTimePickerDelegate, MapViewDelegate, MFMailComposeViewControllerDelegate, EasyTipViewDelegate, CurrentLocationTrackerDelegate, GMSMapViewDelegate, CurrentRideLocationTrackerDelegate, LocalitySelectionDelegate {
    
    
    
  
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet var mapContainerView: MapContainerView!
    @IBOutlet var viewChooseMode: UIView!
    
    @IBOutlet var viewDestnTimeDateTrvelPref: UIView!
    @IBOutlet weak var viewTravelPreferences: UIView!
    @IBOutlet weak var viewSetDestinationTimeDate: UIView!
    @IBOutlet var viewFindOrShareRide: UIView!
    @IBOutlet var containerView: UIView!
    
    ///viewSetDestinationTimeDate -- Inside Elements
    @IBOutlet weak var viewDateTime: UIView!
    @IBOutlet weak var txtDestination: UITextField!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet var dateTimePickerView: DateTimePickerView!
    @IBOutlet weak var btnTappedShowDateTime: UIButton!
    @IBOutlet weak var btnFindOrShareRide: UIButton!
    
    @IBOutlet weak var sourceLocality: UILabel!
    @IBOutlet weak var rideRequestBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var btnNotification: UIBarButtonItem!
   
    var controller = BottomsheetController()
    var selectedDateFromPicker: Date?
    
    var sourceLocalityInfo: LocalityInfo? = nil
    var destinationLocalityInfo: LocalityInfo? = nil
    
    var userDataManager: UserDataManager? = nil
    var userSearchData: UserSearchData? = nil
    var notificationsCount: Int = 0
    var isInitialLocationFetch: Bool?
    var isGlobalLocale : Bool = false
    var rideStartDate : String = ""
    var isFDJServiceSuccess = false
    var isZenParkServiceSuccess = false
    var isNotificationBtnTapped = false
    var isMenuCurrentTripClicked = false
    var fdjPointsArr = [] as NSArray
    var zenParksArray = [] as NSArray
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        viewSetDestinationTimeDate.dropShadow(isShowShadow: true)
        viewDateTime.dropShadow(isShowShadow: true)
        viewDateTime.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapOnViewDateTime))
        tap.delegate = self as? UIGestureRecognizerDelegate
        viewDateTime.addGestureRecognizer(tap)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        controller = BottomsheetController()
        controller.overlayBackgroundColor = UIColor.black.withAlphaComponent(0.6)
        controller.initializeHeight = 300
        viewChooseMode.frame = controller.overlayView.frame
        
        containerView.addSubview(viewChooseMode)
        controller.addContentsView(containerView)
        
        DispatchQueue.main.async {
            self.present(self.controller, animated: true, completion: nil)
            self.controller.viewDidLayoutSubviews()
        }
        
        self.displayproperViewForLocationEnabledOrDisabled()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isNotificationBtnTapped = true
        messageHandler.dismissErrorView()
        messageHandler.dismissSuccessView()
    }
   
    
    func displayproperViewForLocationEnabledOrDisabled() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .notDetermined {
                checkLocationServicesEnabledOrNOt()
            } else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
                checkLocationServicesEnabledOrNOt()
            } else if CLLocationManager.authorizationStatus() == .denied {
                if (sourceLocalityInfo != nil) && (destinationLocalityInfo != nil) {
                    AppDelegate.getInstance().removeLocationAccessScreenIfEsists()
                    setCurrentLocation(sourceLocalityInfo)
                } else {
                    let prefs = UserDefaults.standard
                    let isFromLocationDeniedScreen: Bool = prefs.bool(forKey: "isFromDeniedScreen")
                    if isFromLocationDeniedScreen {
                        prefs.set(false, forKey: "isFromDeniedScreen")
                        prefs.synchronize()
                        moveToLocationSearchFromLocationsServicesDeniedScreen()
                    } else {
                        checkLocationServicesEnabledOrNOt()
                    }
                }
            }
        }
    }
    
    func checkLocationServicesEnabledOrNOt() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .denied {
                messageHandler.dismissBlockingLoadView(handler: {
                    AppDelegate.getInstance().showLocationAccessScreen(self)
                })
            } else {
                AppDelegate.getInstance().removeLocationAccessScreenIfEsists()
                if sourceLocalityInfo == nil {
                    isInitialLocationFetch = true
                }
                fetchCurrentLocation()
            }
        }
    }
    
    
    func setCurrentLocation(_ currentLocation: LocalityInfo?) {
        messageHandler.dismissTransparentBlockingLoadView(handler: {
            if self.isInitialLocationFetch == true {
                self.mapContainerView.createMapView()
                let mapView = self.mapContainerView.mapView
                self.userSearchData = self.userDataManager?.getUserSearchData()
                //If guest user came by searching
                if (self.userSearchData != nil) && (self.userSearchData!.sourceLocality.address != nil) {
                    mapView?.sourceCoordinate = (self.userSearchData?.sourceLocality.latLng)!
                } else {
                    self.updateSourceLocalityInfo(currentLocation)
                    mapView?.sourceCoordinate = (currentLocation?.latLng)!
                }
                //rajesh==mapView?.mapPadding = UIEdgeInsetsMake(self.destinationInfoView.frame.height, 0, self.destinationInfoView.frame.height, 0)
                mapView?.initialiseMapWithSourceCoordinate()
                mapView?.resolveMapMoveGesture = true
                mapView?.mapViewDelegate = self
                self.completeInitialLocationFetch()
            } else {
                self.updateSourceLocalityInfo(currentLocation)
                let mapView = self.mapContainerView.mapView
                mapView?.move(toLocation: (currentLocation?.latLng)!)
            }
            
        })
    }
    func completeInitialLocationFetch() {
        isInitialLocationFetch = false
        ChatManager.sharedInstance().initialiseChat()
        fetchRideRequestsCount()
        userDataManager?.setDeepLinkFlagsOnLogin()
        if (userDataManager?.isDeepLinkDataPresent()) == true {
            userDataManager?.handleDeepLinkParams()
        } else if (userSearchData != nil) {
            if ((userSearchData?.sourceLocality.address) != nil) {
                //Deeplink flow will not have address.Hence no need to poulate the details
                updateSourceLocalityInfo(userSearchData?.sourceLocality)
                updateDestinationLocalityInfo(userSearchData?.destinationLocality)
                rideStartDate = (userSearchData?.rideStartDate)!
                //rajesh==rideDateText.text = userSearchData.rideDateValue
                //rajesh==rideTimeText.text = userSearchData.rideTimeValue
            }
            userDataManager?.resetUserSearchData()
            performSegue(withIdentifier: "showCreateRide", sender: self)
        } else {
            updateCurrentRideLocation()
        }
        LocalNotificationManager.sharedInstance().refreshLocalNotifications()
    }

    func updateCurrentRideLocation() {
        if isMenuCurrentTripClicked {
            messageHandler.showBlockingLoadView(handler: {
                CurrentRideLocationTracker.sharedInstance().currentRideLocationTrackerDelegate = self
                CurrentRideLocationTracker.sharedInstance().updateCurrentLocation()
            })
        } else {
            CurrentRideLocationTracker.sharedInstance().currentRideLocationTrackerDelegate = self
            CurrentRideLocationTracker.sharedInstance().updateCurrentLocation()
        }
    }
 
    func fetchCurrentLocation() {
        if isInitialLocationFetch! {
            if !ServerInterface.sharedInstance().isConnectedToInternet() {
                let alert = UniversalAlert(title: NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORTITLE, comment: ""), withMessage: NSLocalizedString(CMON_GENERIC_CONNECTIVITYERRORMESSAGE, comment: ""))
                alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_RETRY, comment: ""), withAction: { action in
                    self.fetchCurrentLocation()
                })
                alert?.show(in: self)
            } else if !CurrentLocationTracker.sharedInstance().isLocationTrackingEnabled() {
                let alert = UniversalAlert(title: NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORTITLE, comment: ""), withMessage: NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORMESSAGE, comment: ""))
                alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_RETRY, comment: ""), withAction: { action in
                    self.fetchCurrentLocation()
                })
                alert?.show(in: self)
            } else {
                updateUserCurrentLocation()
            }
        } else {
            fetchRideRequestsCount()
        }
    }
    
    func updateUserCurrentLocation() {
        messageHandler.showTransparentBlockingLoadView(handler: {
            CurrentLocationTracker.sharedInstance().currentLocationTrackerDelegate = self
            CurrentLocationTracker.sharedInstance().updateCurrentLocation()
        })
    }

    func fetchRideRequestsCount() {
        if (UserProfile.getCurrentUserToken() != nil) {
            ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: USERNOTIFICATIONSREQUEST), withHandler: { response, error in
                //  UIBarButtonItem *rideRequestBarButtonItem = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
                if response != nil {
                    let notificationCount: Int? = (response?.responseObject as? [Any])?.count
                    self.rideRequestBarButtonItem.badgeValue = String.init(format: "%lu", notificationCount!)
                    // Int("\(UInt(notificationCount ?? 0))")
                    self.removeEasyTipView()
                    if (notificationCount ?? 0) > 0 {
                        self.displayRideRequestsTipView()
                    }
                } else {
                    self.rideRequestBarButtonItem.badgeValue = "0"
                }
            })
        }
    }
    
    func validateSearchInfo() -> Bool {
        removeEasyTipView()
        if sourceLocalityInfo == nil {
            messageHandler.showErrorMessage(NSLocalizedString(VC_SEARCHRIDE_SOURCEVAL, comment: ""))
            return false
        }
        if destinationLocalityInfo == nil {
            messageHandler.showErrorMessage(NSLocalizedString(VC_SEARCHRIDE_DESTVAL, comment: ""))
            return false
        }
        if rideStartDate == nil {
            messageHandler.showErrorMessage(NSLocalizedString(VC_SEARCHRIDE_DEPDATEVAL, comment: ""))
            return false
        }
        return true
    }
    
    func getNotificationCountFromUserDetails() {
        let currentUser = UserProfile.getCurrentUser()
        let isIdCardUploadedStr = "\(String(describing: currentUser?.userDocuments.isIdCardDocUploaded))"
        let isVehicleImgUploadedStr = "\(String(describing: currentUser?.userDocuments.isVehicleImgUploaded))"
        if !(isIdCardUploadedStr == "1") {
            notificationsCount += 1
        }
        if !(currentUser!.userPreferences != nil) {
            notificationsCount += 1
        }
        if !(currentUser!.profileImgUrl != nil) || currentUser?.profileImgUrl.count == 0 {
            notificationsCount += 1
        }
        if currentUser?.dob.count == 0 || currentUser?.gender.count == 0 || currentUser?.companyName.count == 0 || currentUser?.companyEmail.count == 0 || currentUser?.bloodGroup.count == 0 || currentUser?.emergencyContact.count == 0 {
            notificationsCount += 1
        }
        if (currentUser?.userPreferences.userMode == "DRIVER") && !(isVehicleImgUploadedStr == "1") {
            notificationsCount += 1
        }
        // NSLog(@"notificationsCount is %d", notificationsCount);
        //  notificationsCount = 1;
        
        
        
        btnNotification.badgeValue = String.init(format: "%d", notificationsCount) //Int("\(notificationsCount)")
        if notificationsCount > 0 && !isNotificationBtnTapped {
            displayNotificationsTipView()
        }
    }
    
    func displayNotificationsTipView() {
        removeEasyTipView()
        let text = NSLocalizedString(MS_SEARCHRIDE_LBL_NEW_NOTIFICATIONS, comment: "")
        btnNotification.customView?.backgroundColor = UIColor.green
        print("view is \(String(describing: btnNotification.view))")
        
        EasyTipView.show(animated: true, forItem: btnNotification, withinSuperview: navigationController?.view, text: text, delegate: self)
        
//        EasyTipView.showWith(animated: true, forItem: btnNotification, withinSuperview: navigationController?.view, text: text, delegate: self)
    }
    
    
    func displayRideRequestsTipView() {
        let text = NSLocalizedString(MS_SEARCHRIDE_LBL_NEW_RIDE_REQUESTS, comment: "")
        rideRequestBarButtonItem.customView?.backgroundColor = UIColor.green
        print("view is \(String(describing: btnNotification.view))")
        
        EasyTipView.show(animated: true, forItem: rideRequestBarButtonItem, withinSuperview: navigationController?.view, text: text, delegate: self)
    }
    
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        
    }
    
    // MARK:- Service Request
    
    func callService(forGettingFDJPoints currentLocation: LocalityInfo?) {
        let currentLocale = CurrentLocale.sharedInstance()
        isGlobalLocale = (currentLocale?.isGlobalLocale())!
        // isGlobalLocale = YES;
        if isGlobalLocale {
            isFDJServiceSuccess = false
            let latNum = currentLocation?.latLng.latitude ?? 0.0
            let longNum = currentLocation?.latLng.longitude ?? 0.0
            let latValue = "\(latNum)"
            let longValue = "\(longNum)"
            
 
            ServerInterface.sharedInstance().getResponse(GetFDJpointsRequest(srcLatitude: latValue, andSrcLongitude: longValue), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.isFDJServiceSuccess = true
                        self.fdjPointsArr = []
                        self.fdjPointsArr = (response?.responseObject as? [AnyObject])! as NSArray
                        self.addPickUpAndParkingPoints()
                    } else {
                        // [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                    }
                })
            })
            callServiceForZenparkSign(in: currentLocation)
        }
    }
    
    func callService(forGettingZenParkPoints currentLocation: LocalityInfo?) {
        isZenParkServiceSuccess = false
        // GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] includingCoordinate:currentLocation.latLng];
        let googleMapObj: GMSMapView? = AppDelegate.getInstance().googleMapObject
        if let anObj = googleMapObj {
            print("googleMapObj is \(anObj)")
        }
        let region: GMSVisibleRegion? = googleMapObj?.projection.visibleRegion()
        var bounds: GMSCoordinateBounds? = nil
        if let aRegion = region {
            bounds = GMSCoordinateBounds(region: aRegion)
        }
        let northEast: CLLocationCoordinate2D? = bounds?.northEast
        let southWest: CLLocationCoordinate2D? = bounds?.southWest
        let north = northEast?.latitude ?? 0.0
        let south = southWest?.latitude ?? 0.0
        let west = southWest?.longitude ?? 0.0
        let east = northEast?.longitude ?? 0.0
        /* north =@48.86560144954412;
         south =@48.85298064817098;
         west =@2.3607346631332575;
         east = @2.3833510495468317;*/
        ServerInterface.sharedInstance().getResponse(GetZenParkpointsRequest(north: "\(north)", andSouth: "\(south)", andWest: "\(west)", andEast: "\(east)"), withHandler: { response, error in
            self.messageHandler.dismissBlockingLoadView(handler: {
                if response != nil {
                    if (response?.messageCode == "1") {
                        self.isZenParkServiceSuccess = true
                        self.zenParksArray = []
                        //rajesh=self.zenParksArray = response?.responseObject["parkingLots"]
                        self.addPickUpAndParkingPoints()
                    }
                } else {
                    AppDelegate.getInstance().zenparkAuthToken = nil
                    self.callServiceForZenparkSign(in: currentLocation)
                }
            })
        })
    }
    
    func addPickUpAndParkingPoints() {
        if isFDJServiceSuccess && isZenParkServiceSuccess {
            mapContainerView.mapView?.removeMarkersFromMap()
            addFDJpointsToMap()
            addZenparkPointsToMap()
        }
    }
    
    func callServiceForZenparkSign(in currentLocation: LocalityInfo?) {
        ServerInterface.sharedInstance().getResponse(ZenParkSignInRequest(), withHandler: { response, error in
            self.messageHandler.dismissBlockingLoadView(handler: {
                if response != nil {
                    let tokenStr = ""//rajesh==response?.responseObject["token"] as? String
                    AppDelegate.getInstance().zenparkAuthToken = tokenStr
                    self.callService(forGettingZenParkPoints: currentLocation)
                } else {
                    self.callServiceForZenparkSign(in: currentLocation)
                    // [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                }
            })
        })
    }

    func addFDJpointsToMap() {
        for obj: [String : Any]? in fdjPointsArr as! [[String : Any]] {
            // NSString *idValue = [obj objectForKey:@"ID"];
            let cxValue = obj?["CX"] as? String
            let cyValue = obj?["CY"] as? String
            mapContainerView.mapView?.createMarkerWithhLat(Double(cxValue ?? "")! as NSNumber , andLong: Double(cyValue ?? "")! as NSNumber , andMarkerImage: UIImage(named: "pin-point-final.png"))
            //mapContainerView.mapView?.createMarkerWithhLat(Double(cxValue ?? "") ?? 0.0, andLong: Double(cyValue ?? "") ?? 0.0, andMarkerImage: UIImage(named: "pin-point-final.png"))
        }
    }
    
    
    
    func addZenparkPointsToMap() {
        for obj: [String : Any]? in zenParksArray as! [[String : Any]] {
            let locObj = obj?["Location"] as? [AnyHashable : Any]
            let latStr = locObj?["Latitude"] as? String
            let longStr = locObj?["Longitude"] as? String
            let latitude = Double(latStr ?? "") ?? 0.0
            let longitude = Double(longStr ?? "") ?? 0.0
            mapContainerView.mapView?.createMarkerWithhLat(latitude as NSNumber, andLong: longitude as NSNumber, andMarkerImage: UIImage(named: "zenpark.png"))
        }
    }
    
    func moveToLocationSearchFromLocationsServicesDeniedScreen() {
        let autoSugeestionNavController: UINavigationController? = LocalityAutoSuggestionController.createLocalityAutoSuggestNavController()
        let autoSuggestionController = autoSugeestionNavController?.topViewController as? LocalityAutoSuggestionController
        autoSuggestionController?.isForSourceAddress = true
        autoSuggestionController?.sourceLocality = nil
        autoSuggestionController?.destinationLocality = nil
        let title = NSLocalizedString(MS_SERACHRIDE_BTN_SEARCH, comment: "")
        autoSuggestionController?.title = title.capitalized
        autoSuggestionController?.selectionDelegate = self as! LocalitySelectionDelegate
        autoSuggestionController?.showAutosuggestOnly = false
        let prefs = UserDefaults.standard
        prefs.set(true, forKey: "isFromManuaalySearch")
        prefs.synchronize()
        if let aController = autoSugeestionNavController {
            present(aController, animated: false)
        }
    }
    
    func methodForLocationServicesAccessDenied() {
        checkLocationServicesEnabledOrNOt()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
        }
    }
    
 
    // MARk:- MapView Delegate
    
    func changedMapLocation(_ newLocation: LocalityInfo?) {
        updateSourceLocalityInfo(newLocation)
    }
    func updateSourceLocalityInfo(_ location: LocalityInfo?) {
        sourceLocalityInfo = location
        sourceLocality.text = location?.address
        //callService(forGettingFDJPoints: location)
    }
    
    func updateDestinationLocalityInfo(_ location: LocalityInfo?) {
        destinationLocalityInfo = location
        txtDestination.text = location?.address

        //rajesh==destinationLocality.text = location?.address
    }
    
    func swapSourceAndDestinaion() {
        print("swap tapped....")
        if (sourceLocalityInfo != nil) && (destinationLocalityInfo != nil) {
            let tempLocality: LocalityInfo? = sourceLocalityInfo
            sourceLocalityInfo = destinationLocalityInfo
            destinationLocalityInfo = tempLocality
            sourceLocality.text = sourceLocalityInfo?.address
            txtDestination.text = destinationLocalityInfo?.address
            //rajesh==destinationLocality.text = destinationLocalityInfo.address
        }
    }
    
    func swapLocalityTapped() {
        swapSourceAndDestinaion()
    }

    
    // MARK:- Actions
    
    @IBAction func showMenu(_ sender: Any) {
        
//        defer {
//        }
//        do {
//            removeEasyTipView()
//            MenuViewController.createAndOpenMenu(withSourceController: navigationController?.parent, andMenuDelegate: self)
//            messageHandler.dismissErrorView()
//            messageHandler.dismissSuccessView()
//        } catch let ex {
//        }

        
//        setupSideMenu()
//
//        let newView = (self.storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController)!
//        self.present(newView, animated: true, completion: nil)
        
    }
    
    @IBAction func showMessages(_ sender: Any) {
        
        
    }
    
    @IBAction func showUserRideRequest(_ sender: Any) {
        removeEasyTipView()
        self.present(RideRequestsController.createRideRequestsNavController(), animated: true, completion: nil)
    }
    
    @IBAction func showNotifications(_ sender: Any) {
        removeEasyTipView()
        //self.present(NotificationsController.createNotificationsNavController(), animated: true, completion: nil)
    }
    
    func tapOnViewDateTime() {
        if selectedDateFromPicker != nil {
            dateTimePickerView.selectedDate = selectedDateFromPicker
        }
        dateTimePickerView.delegate = self
        dateTimePickerView.show(in: self.controller.view)
    }
    
    @IBAction func btnTappedShowDateTimePicker(_ sender: Any) {
        tapOnViewDateTime()
    }
    
    
    @IBAction func btnTappedCarOwner(_ sender: Any) {
        
        viewChooseMode.removeFromSuperview()
        controller.initializeHeight = 200
        viewDestnTimeDateTrvelPref.frame = controller.overlayView.frame
        containerView.addSubview(viewDestnTimeDateTrvelPref)
        controller.viewDidLayoutSubviews()
    }
    
    @IBAction func btnTappedRider(_ sender: Any) {
        
        let sb = UIStoryboard(name: "IDCard", bundle: nil)
        let vc: UIViewController? = sb.instantiateViewController(withIdentifier: "UserIDCardController")
        if let aVc = vc {
            navigationController?.pushViewController(aVc, animated: true)
        }
        
    }
    
    @IBAction func btnTappedOpenMode(_ sender: Any) {
        
        controller = BottomsheetController()
        controller.overlayBackgroundColor = UIColor.black.withAlphaComponent(0.6)
        controller.initializeHeight = 300
        containerView.addSubview(viewChooseMode)
        controller.addContentsView(containerView)
        
        DispatchQueue.main.async {
            self.present(self.controller, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnTappedTravelPreferences(_ sender: Any) {
       
//        ///******** TO SHOW FIND or SHARE RIDE *********///
//        self.viewDestnTimeDateTrvelPref.addSubview(self.viewFindOrShareRide)
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
//            self.viewTravelPreferences.alpha = 0.0
//        }, completion: {
//            (finished: Bool) -> Void in
//                //self.viewTravelPreferences.isHidden = true
//            /// Fade in
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
//                self.viewFindOrShareRide.frame.origin.y = self.viewTravelPreferences.frame.origin.y
//                
//            }, completion: nil)
//        })
        
        
        ///******** TO SHOW TRAVEL PREFERENCE *********///
        
        let parent = self.controller
        parent.dismiss(animated: true, completion: {
            let presentSetAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "setAddressNav") as? UINavigationController
            self.navigationController?.present(presentSetAddressVC!, animated: true, completion: nil)
        })
        
    }
    
    
    @IBAction func btnTappedFindOrShareRide(_ sender: Any) {
        
        
    }
    
    @IBAction func btnTappedSwitchRiderOrCarowner(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            sender.setImage(UIImage.init(named: "carownerSwitch"), for: .normal)
            btnFindOrShareRide.setTitle("Share a Ride", for: .normal)
            
        } else {
            sender.isSelected = true
            sender.setImage(UIImage.init(named: "riderSwitch"), for: .normal)
            btnFindOrShareRide.setTitle("Find a Ride", for: .normal)
        }
        
    }
    
    
    @IBAction func btnTappedGetSourceLocality(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let autoSuggestionController22 = storyBoard.instantiateViewController(withIdentifier: "localityAutosuggestNavController") as? UINavigationController
        
        
        
        //localityPickMode = PickingSourceLocality
//        var autoSuggestionController = (segue.destination as? UINavigationController)?.topViewController as? LocalityAutoSuggestionController
//        autoSuggestionController?.isForSourceAddress = true
//        autoSuggestionController?.sourceLocality = sourceLocalityInfo
//        autoSuggestionController?.destinationLocality = destinationLocalityInfo
//        var title = NSLocalizedString(MS_SERACHRIDE_BTN_SEARCH, comment: "")
//        autoSuggestionController?.title = title.capitalized
//        autoSuggestionController?.selectionDelegate = self
//        autoSuggestionController?.showAutosuggestOnly = false
//
        self.present(autoSuggestionController22!, animated: true, completion: nil)
        
        //localityAutosuggestNavController

    }
    
    // MARK:- Date Time Picker Delegate
    
    func selectedDate(_ selectedDate: Date!) {

        btnTappedShowDateTime.isHidden = true
        viewDateTime.isHidden = false

        debugPrint(selectedDate)
        let dateTimeFrmt = DateFormatter.init()
        selectedDateFromPicker = selectedDate

        dateTimeFrmt.dateFormat = "hh:mm a"
        lblTime.text = dateTimeFrmt.string(from: selectedDate)

        dateTimeFrmt.dateFormat = "MMM dd"
        lblDate.text = dateTimeFrmt.string(from: selectedDate)

    }
    
    func removeEasyTipView() {
        for subview: UIView in (navigationController?.view.subviews)! {
            if (subview is EasyTipView) {
                subview.removeFromSuperview()
            }
        }
    }
    
    // MARK:- Textfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtDestination {
            txtDestination.resignFirstResponder()
            
            let parent = self.controller
            parent.dismiss(animated: true, completion: {
                let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                let localityNavCtrl = storyBoard.instantiateViewController(withIdentifier: "localityAutosuggestNavController") as? UINavigationController
                self.present(localityNavCtrl!, animated: true, completion: nil)
                
            })
            
            
//            UINavigationController *autoSugeestionNavController = [LocalityAutoSuggestionController createLocalityAutoSuggestNavController];
            
            
            
        }
    }
    
    // MARK:- SideMenu
    
    /// SetupSideMenu
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.menuFadeStatusBar = false
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        // SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        // SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // Set up a cool background image for demo purposes
        SideMenuManager.menuAnimationBackgroundColor = UIColor.lightGray
        
    }
    
    class func homeNavigationController() -> UINavigationController {
        //HomeNavigationController
        let storyBoard = UIStoryboard.init(name: "Home", bundle: Bundle.main)
        let homeNavigationCtrl = storyBoard.instantiateViewController(withIdentifier: "HomeNavigationController") as? UINavigationController
        return homeNavigationCtrl!
        
    }

    // MARK:- LocalitySelectionDelegate
    
    
    func selectedLocality(_ sourceLocation: LocalityInfo?, withDestnationLocation destiLocation: LocalityInfo?) {
        var sourceLocation = sourceLocation
        if sourceLocation == nil {
            sourceLocation = sourceLocalityInfo
        }
        updateSourceLocalityInfo(sourceLocation)
        updateDestinationLocalityInfo(destiLocation)
        let delayInSeconds: Double = 0.3
        if sourceLocation != nil {
            var popTime = DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC)))
            
            //let deadLineTime  = popTime / Double(NSEC_PER_SEC)
            
            DispatchQueue.main.asyncAfter(deadline: popTime, execute: {
                self.mapContainerView.mapView?.move(toLocation: (sourceLocation?.latLng)!)
            })
        }
    }
    
    func selectedFromThePreviousHistory() {
        
    }
    
    
    
    
    // MARK:- CurrentLocationTrackerDelegate
    func unable(toFetchCurrentLocation error: Error!) {
        messageHandler.dismissTransparentBlockingLoadView(handler: {
            if self.isInitialLocationFetch! {
                let alert = UniversalAlert(title: NSLocalizedString(CMON_GENERIC_LOCATIONACCESSERRORTITLE, comment: ""), withMessage: (error as NSError?)?.userInfo["message"] as! String)
                alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_RETRY, comment: ""), withAction: { action in
                    self.fetchCurrentLocation()
                })
                alert?.show(in: self)
            } else {
                self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
            }
        })
    }
    
    // MARK:- CurrentRideLocationTrackerDelegate
    func setCurrentRideLocation(_ currentLocation: CLLocationCoordinate2D) {
        let currentLat = currentLocation.latitude
        let currentLong = currentLocation.longitude
        showUpcomingRide(withLat: currentLat as NSNumber, andLong: currentLong as NSNumber)
    }
    
    func unable(toFetchCurrentRideLocation error: Error!) {
        showUpcomingRide(withLat: nil, andLong: nil)
    }
    
    func showUpcomingRide(withLat userLat: NSNumber?, andLong userLong: NSNumber?) {
        ServerInterface.sharedInstance().getResponse(RideDriveActionRequest(requestType: CURRENTTRIPNPENDINGRATING, andUserLat: userLat, andUserLong: userLong), withHandler: { response, error in
            if self.isMenuCurrentTripClicked {
                self.messageHandler.dismissBlockingLoadView(handler: {
                    self.handleCurrentRide(response, error: error)
                })
            } else {
                self.handleCurrentRide(response, error: error)
            }
        })
    }
    
    
    func handleCurrentRide(_ response: ServerResponse?, error: Error!) {
        if response != nil {
            let rideResponse = response?.responseObject as? CurrentTripNPendingRatingResponse
            var visibleController: UIViewController? = AppUtilites.applicationVisibleViewController()
            let rootViewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
            if rideResponse?.pendingDriveRating != nil {
                let driveInvoiceNavController = DriveInvoiceController.createDriveInvoiceNavController()
                let driveInvoiceController = driveInvoiceNavController?.topViewController as? DriveInvoiceController
                driveInvoiceController?.ratingResponse = rideResponse
                driveInvoiceController?.showEmptyDrive = isMenuCurrentTripClicked
                if rootViewController?.presentedViewController != nil {
                    rootViewController?.dismiss(animated: false)
                    visibleController = UIApplication.shared.keyWindow?.rootViewController
                    if let aController = driveInvoiceNavController {
                        visibleController?.present(aController, animated: true)
                    }
                } else {
                    if let aController = driveInvoiceNavController {
                        visibleController?.present(aController, animated: true)
                    }
                }
            } else if rideResponse?.pendingRideRating != nil {
                if visibleController?.navigationController != nil {
                    visibleController = visibleController?.navigationController
                }
                //RideInvoiceController.show(visibleController, andRatingResponse: rideResponse, andShowEmptyDrive: isMenuCurrentTripClicked)
                //RideInvoiceController.showRideInvoiceController(visibleController, andRatingResponse: rideResponse, andShowEmptyDrive: isMenuCurrentTripClicked)
            } else if rideResponse?.currentDrive != nil {
                print("isMenuCurrentTripClicked is \(isMenuCurrentTripClicked), and seats = \(rideResponse?.currentDrive.numOfSeats ?? 0)")
                //if(isMenuCurrentTripClicked || rideResponse.currentDrive.numOfSeats.intValue > 0)
                do {
                    let currentDriveNavController = CurrentDriveController.createCurrentDriveNavController()
                    let currentDriveController = currentDriveNavController?.topViewController as? CurrentDriveController
                    currentDriveController?.currentDrive = rideResponse?.currentDrive
                    if rootViewController?.presentedViewController != nil {
                        rootViewController?.dismiss(animated: false)
                        visibleController = UIApplication.shared.keyWindow?.rootViewController
                        if let aController = currentDriveNavController {
                            visibleController?.present(aController, animated: true)
                        }
                    } else {
                        if let aController = currentDriveNavController {
                            visibleController?.present(aController, animated: true)
                        }
                    }
                }
            } else if rideResponse?.currentRide != nil {
                let currentRideNavController = CurrentRideController.createCurrentRideNavController()
                let currentRideController = currentRideNavController?.topViewController as? CurrentRideController
                currentRideController?.currentRide = rideResponse?.currentRide
                if rootViewController?.presentedViewController != nil {
                    rootViewController?.dismiss(animated: false)
                    visibleController = UIApplication.shared.keyWindow?.rootViewController
                    if let aController = currentRideNavController {
                        visibleController?.present(aController, animated: true)
                    }
                } else {
                    if let aController = currentRideNavController {
                        visibleController?.present(aController, animated: true)
                    }
                }
            } else if isMenuCurrentTripClicked {
                messageHandler.showErrorMessage(NSLocalizedString(VC_SEARCHRIDE_NOACTIVEDRIVE, comment: ""))
            }
        } else if isMenuCurrentTripClicked {
            
            messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
        }
        if !isMenuCurrentTripClicked {
            self.perform(#selector(self.showPushNotificationData), with: self, afterDelay: 0.5)
            //self.perform(#selector(self.showPushNotificationData), with: self, afterDelay: 0.5)
        }
        isMenuCurrentTripClicked = false
    }
    
    func showPushNotificationData() {
        PushNotificationManager.sharedInstance().showNotificationData()
    }
    
    
    // MARK:- Menu Delegate
    
//    func selectedMenuOption(_ option: MenuOptions) {
//        if option == SIDE_MENU_TRAVEl_PREFERENCE {
//            AppDelegate.getInstance().move(toTravelPrefenceScreen: self)
//        } else if option == SIDE_MENU_ACTIVE_TRIPS {
//            self.present(TripsTabController.createTripsTabNavController(), animated: true)
//        } else if option == SIDE_MENU_ACCOUNT {
//            self.present(AccountController.createAccountNavController(), animated: true)
//        } else if option == SIDE_MENU_WALLET {
//            let walletNavController = UserWalletController.createUserWalletNavController()
//            if let aController = walletNavController {
//                present(aController, animated: true)
//            }
//        }
////        else if option == SIDE_MENU_ {
////            self.present(UserProfileController.createUserProfileNavController(), animated: true)
////        }
//        else if option == SIDE_MENU_UPCOMING_TRIPS {
//
//        } else if option == SIDE_MENU_REWARDS {
//            self.present(RewardsController.createRewardsNavController()!, animated: true)
//        } else if option == SIDE_MENU_SETTINGS {
//            self.present(SettingsController.createSettingsNavController(), animated: true)
//        } else if option == SIDE_MENU_ABOUT {
//            self.present(AboutController.createAboutNavController(), animated: true)
//        } else if option == SIDE_MENU_REFERANDEARN {
//            messageHandler.showBlockingLoadView(handler: {
//                AppViralityHelper.getCamapaignDetails({ campaignDetails in
//                    self.messageHandler.dismissBlockingLoadView(handler: {
//                        AppViralityHelper.showGrowthHack(on: self, withDetails: campaignDetails)
//                    })
//                })
//            })
//        } else if option == MESSAGES {
//            self.present(ChatUserListController.createChatNavController(), animated: true)
//        } else if option == WALLETSTATEMENT {
//            self.present(WalletStatementController.createWalletStatementNavController(), animated: true)
//        } else if option == LEAVEFEEDBACK {
//            let prefs = UserDefaults.standard
//            prefs.set(true, forKey: "forDismissing")
//            prefs.synchronize()
//            self.present(AppDelegate.createFeedbackNavController(self), animated: true)
//        }
//    }
    
}





