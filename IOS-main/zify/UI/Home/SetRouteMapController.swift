//
//  SetRouteMapController.swift
//  zify
//
//  Created by Anurag on 26/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class SetRouteMapController: UIViewController, UIGestureRecognizerDelegate, MapViewDelegate {
    
    
    @IBOutlet weak var viewHomeToOfficeRoute: UIView!
    @IBOutlet weak var viewOfficeToHomeRoute: UIView!
    
    @IBOutlet weak var viewTempHomeToOfficeRoute: UIView!
    @IBOutlet weak var viewTempOfficeToHomeRoute: UIView!
    
    @IBOutlet weak var homeToWorkMapContainerView: MapContainerView!
    @IBOutlet weak var workToHomeMapContainerView: MapContainerView!
    @IBOutlet var messageHandler:MessageHandler! = MessageHandler()

    
    @IBOutlet weak var lblHomeToOfficeRouteTemp: UILabel!
    @IBOutlet weak var lblOfficeToHomeRouteTemp: UILabel!
    
    @IBOutlet weak var btnHomeRoute: UIButton!
    @IBOutlet weak var btnWorkRoute: UIButton!

    
    @IBOutlet weak var btnSave: UIButton!
    
    var isForwardAddressTapped:Bool = Bool()
    var currentUser : UserProfile?
    var homeAddressInfo: LocalityInfo?
    var officeAddressInfo: LocalityInfo?
    var isUpdatePreferencesRequest = false
    var onwardRoute: DriveRoute?
    var returnRoute: DriveRoute?
    var isMapViewInitialised : Bool = Bool()
    
    var selectedStartTime: String = ""
    var selectedReturnTime: String = ""
    var travelType: String? = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnHomeRoute.isHidden = true;
        self.btnWorkRoute.isHidden = true;

        showShadow(view: viewHomeToOfficeRoute, color: .darkGray, opacity: 1, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
        showShadow(view: viewOfficeToHomeRoute, color: .darkGray, opacity: 1, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
        
        let tapHomeRoute = UITapGestureRecognizer.init(target: self, action: #selector(self.viewTappedHomeToOfficeRoute))
        tapHomeRoute.delegate = self;
        viewHomeToOfficeRoute.addGestureRecognizer(tapHomeRoute)
        
        let tapWorkRoute = UITapGestureRecognizer.init(target: self, action: #selector(self.viewTappedOfficeToHomeRoute))
        tapWorkRoute.delegate = self
        viewOfficeToHomeRoute.addGestureRecognizer(tapWorkRoute)
        
        if UIScreen.main.sizeType == .iPhone5 {
            lblHomeToOfficeRouteTemp.fontSize = 18
            lblOfficeToHomeRouteTemp.fontSize = 18
            btnSave.titleLabel?.fontSize = 18
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblHomeToOfficeRouteTemp.fontSize = 19
            lblOfficeToHomeRouteTemp.fontSize = 19
            btnSave.titleLabel?.fontSize = 20
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblHomeToOfficeRouteTemp.fontSize = 20
            lblOfficeToHomeRouteTemp.fontSize = 20
            btnSave.titleLabel?.fontSize = 20
        }
          homeAddressInfo = AppDelegate.getInstance().homeAddress
         officeAddressInfo = AppDelegate.getInstance().officeAddress
        AppDelegate.getInstance().homeToOffDriveRoute = nil
        AppDelegate.getInstance().oFFToHomeDriveRoute = nil
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBar.topItem?.title = " "
        navigationItem.title = NSLocalizedString(HS_SET_YOUR_ROUTE_MAP, comment: "")

        travelType =  AppDelegate.getInstance().travelTypeStr
        currentUser = UserProfile.getCurrentUser()
        //let preferences: UserPreferences? = currentUser?.userPreferences
        
     
        
        self.showRouteOntheView()
    }
    
    func viewTappedHomeToOfficeRoute()  {
        self.actionForShowingRoute(isFrom: true)
    }
    
    func viewTappedOfficeToHomeRoute() {
        self.actionForShowingRoute(isFrom: false)
    }
    
    func actionForShowingRoute(isFrom:Bool) -> Void {
        if homeAddressInfo != nil || officeAddressInfo != nil {
            if isFrom == true {
              
            } else {
                // office address tapped
//                AppDelegate.getInstance().homeAddress =  officeAddressInfo
//                AppDelegate.getInstance().officeAddress = homeAddressInfo
            }
        }
        
        
        print("selected tappped")
        isForwardAddressTapped = isFrom
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let obj:RouteDisplayForStrp3 =  storyboard.instantiateViewController(withIdentifier: "routeDisplay") as! RouteDisplayForStrp3;
        obj.isFromHomeToOffice = isForwardAddressTapped
        let navController:UINavigationController = UINavigationController.init(rootViewController: obj)
        self.navigationController?.present(navController, animated: true, completion:nil)
        
    }
    
    func showRouteOntheView() -> Void {
        if(AppDelegate.getInstance().homeToOffDriveRoute != nil){
            if isMapViewInitialised == true {
                isMapViewInitialised = false
            }
            createMapView(route: AppDelegate.getInstance().homeToOffDriveRoute, withMapContainerView: homeToWorkMapContainerView)
            self.btnHomeRoute.isHidden = false
        }
        if(AppDelegate.getInstance().oFFToHomeDriveRoute != nil){
            isMapViewInitialised = false
            createMapView(route: AppDelegate.getInstance().oFFToHomeDriveRoute, withMapContainerView: workToHomeMapContainerView)
            self.btnWorkRoute.isHidden = false
        }
    }
    
    func createMapView(route:DriveRoute, withMapContainerView:MapContainerView) -> Void {
        
        if(AppDelegate.getInstance().homeToOffDriveRoute != nil) {
            onwardRoute = AppDelegate.getInstance().homeToOffDriveRoute
        }
        if(AppDelegate.getInstance().oFFToHomeDriveRoute != nil) {
            returnRoute = AppDelegate.getInstance().oFFToHomeDriveRoute
        }
        
        if(isForwardAddressTapped) {
            if(AppDelegate.getInstance().homeToOffDriveRoute != nil) {
                viewTempHomeToOfficeRoute.isHidden = true
            } else {
                viewTempHomeToOfficeRoute.isHidden = false
            }
        } else {
            if(AppDelegate.getInstance().oFFToHomeDriveRoute != nil) {
                viewTempOfficeToHomeRoute.isHidden = true
            } else {
                viewTempOfficeToHomeRoute.isHidden = false
            }
        }
        
        if !isMapViewInitialised {
            withMapContainerView.createMapView()
        }
        
        let mapView = withMapContainerView.mapView
        mapView?.sourceCoordinate = CLLocationCoordinate2DMake(Double(route.srcLat)!, Double(route.srcLong)!)
        mapView?.destCoordinate = CLLocationCoordinate2DMake(Double(route.destLat)!, Double(route.destLong)!)
        mapView?.overviewPolylinePoints = route.overviewPolylinePoints
        //mapView?.drawMap()
         
        mapView?.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0)
        mapView?.mapViewDelegate = self
        
        if !isMapViewInitialised {
            mapView?.drawMap()
            isMapViewInitialised = true
        } else {
            mapView?.drawMarkers()
            mapView?.modifyPath(for: 0)
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func btnTappedEditHomeToOffMap(_ sender: Any) {
        viewTappedHomeToOfficeRoute()
    }
    @IBAction func btnTappedEditOffToHomeMap(_ sender: Any) {
        viewTappedOfficeToHomeRoute()
    }
    @IBAction func btnTappedClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTappedNext(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        
      //  self.callServices()
    }
    
    func showRouteMap() {
        
        var sourceLocalityInfo: LocalityInfo?
        var destinationLocalityInfo: LocalityInfo?
        //var overviewPolylinePoints: String?
        
        sourceLocalityInfo = homeAddressInfo
        destinationLocalityInfo = officeAddressInfo
        //overviewPolylinePoints = (onwardRoute?.overviewPolylinePoints)!
        
        guard sourceLocalityInfo != nil || destinationLocalityInfo != nil else {
            return
        }
        
        mapViewShowRoute(showMapContainerView: homeToWorkMapContainerView, strOverViewPolyLinePoints: (onwardRoute?.overviewPolylinePoints)!, sourceLocalityInfo: sourceLocalityInfo!, destinationLocalityInfo: destinationLocalityInfo!, isMapInitialised : isMapViewInitialised)
        
        isMapViewInitialised = false
        mapViewShowRoute(showMapContainerView: workToHomeMapContainerView, strOverViewPolyLinePoints: (returnRoute?.overviewPolylinePoints)!, sourceLocalityInfo: destinationLocalityInfo!, destinationLocalityInfo: sourceLocalityInfo!, isMapInitialised : isMapViewInitialised)
        
    }
    
    func mapViewShowRoute(showMapContainerView: MapContainerView, strOverViewPolyLinePoints : String, sourceLocalityInfo : LocalityInfo, destinationLocalityInfo : LocalityInfo, isMapInitialised : Bool) {
        
        if showMapContainerView == homeToWorkMapContainerView {
            if(!viewTempHomeToOfficeRoute.isHidden){
                viewTempHomeToOfficeRoute.isHidden = true
            }
        } else if showMapContainerView == workToHomeMapContainerView {
            if(!viewTempOfficeToHomeRoute.isHidden){
                viewTempOfficeToHomeRoute.isHidden = true
            }
        }
        
        if !isMapInitialised {
            showMapContainerView.createMapView()
        }
        
        let mapViewRoute = showMapContainerView.mapView
        mapViewRoute?.sourceCoordinate = (sourceLocalityInfo.latLng)
        mapViewRoute?.destCoordinate = (destinationLocalityInfo.latLng)
        mapViewRoute?.overviewPolylinePoints = strOverViewPolyLinePoints
        mapViewRoute?.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0)
        mapViewRoute?.mapViewDelegate = self
        
        if !isMapViewInitialised {
            mapViewRoute?.drawMap()
            isMapViewInitialised = true
        } else {
            mapViewRoute?.drawMarkers()
            mapViewRoute?.modifyPath(for: 0)
        }
        
    }
  
    
    // MARK: - Service Call
    
 /*   func callServices() {
        if validateUserPreferences() {
            messageHandler.showBlockingLoadView(handler: {
                var request: UserPreferencesRequest?
                if self.isUpdatePreferencesRequest {
                    request = UserPreferencesRequest(with: UPDATEPREFERENCES, andHomeAddress: self.homeAddressInfo, andOfficeAddress: self.officeAddressInfo, andStartTime: self.selectedStartTime, andReturnTime: self.selectedReturnTime, andOnwardRoute: self.onwardRoute, andReturn: self.returnRoute, andUserMode: self.travelType)
                } else {
                    request = UserPreferencesRequest(with: ADDPREFERENCES, andHomeAddress: self.homeAddressInfo, andOfficeAddress: self.officeAddressInfo, andStartTime: self.selectedStartTime, andReturnTime: self.selectedReturnTime, andOnwardRoute: self.onwardRoute, andReturn: self.returnRoute, andUserMode: self.travelType)
                }
                ServerInterface.sharedInstance().getResponse(request, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            ///Refer and earn service call for process the referral amount to parent user and child user
                            ReferAndEarnController.serviceCall_ProcessReferralData(eventId: 2, strUserType: "DRIVER")
                            let pref = UserDefaults.standard;
                            pref.set(true, forKey: "tpUpdated")
                            pref.synchronize()
                            let strSuccessMsg = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG, comment: "") + "\n" + NSLocalizedString(TP_SCREEN4_SUCCESS_MSG1, comment: "") + "\n" + NSLocalizedString(TP_SCREEN4_SUCCESS_MSG2, comment: "") + "\n" + NSLocalizedString(TP_SCREEN4_SUCCESS_MSG3, comment: "")
                            
                            let alertCtrl = UIAlertController.init(title: AppName, message: strSuccessMsg, preferredStyle: .alert)
                            let okAction = UIAlertAction.init(title: NSLocalizedString(CMON_GENERIC_OK, comment: ""), style: .default, handler: { (alertView) in
                                self.navigationController?.popToRootViewController(animated: true)
                            })
                            alertCtrl.addAction(okAction)
                            self.present(alertCtrl, animated: true, completion: nil)
                            
                           // showAlertView(strMessage: strSuccessMsg, navigationCtrl: self.navigationController!)
                            //self.createViewForSuccessOnServiceCall()
                        } else {
                            //self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                            showAlertView(strMessage: (error as NSError?)?.userInfo["message"] as! String, navigationCtrl: self.navigationController!)
                            //self.errorMessage.text = (error as NSError?)?.userInfo["message"]
                        }
                    })
                })
            })
        }
    }*/
    
    func validateUserPreferences() -> Bool {
        if homeAddressInfo == nil {
            showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_HOMEADDRESSVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if officeAddressInfo == nil {
            showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_OFFICEADDRESSVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if selectedStartTime == "" || selectedStartTime.count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_STARTTIMEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if selectedReturnTime == "" || selectedReturnTime.count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_RETURNTIMEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if !(NSLocalizedString(VC_USERPREFERENCES_RIDERVAL, comment: "") == travelType) {
            if onwardRoute == nil {
                showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_ONWARDROUTEVAL, comment: ""), navigationCtrl: self.navigationController!)
                return false
            }
            if returnRoute == nil {
                showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_RETURNROUTEVAL, comment: ""), navigationCtrl: self.navigationController!)
                return false
            }
        } else {
            onwardRoute = nil
            returnRoute = nil
        }
        return true
    }
    
    
}



