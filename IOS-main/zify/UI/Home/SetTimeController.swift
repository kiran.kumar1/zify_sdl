//
//  SetTimeController.swift
//  zify
//
//  Created by Anurag on 26/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class SetTimeController: UIViewController, DateTimePickerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var viewHomeToOfficeTime: UIView!
    @IBOutlet weak var viewOfficeToHomeTime: UIView!
    
    @IBOutlet weak var viewTempHomeToOfficeTime: UIView!
    @IBOutlet weak var viewTempOfficeToHomeTime: UIView!
    
    @IBOutlet weak var lblHomeToOfficeTimeTemp: UILabel!
    @IBOutlet weak var lblOfficeToHomeTimeTemp: UILabel!
    
    
    @IBOutlet weak var lblHomeToOfficeTime: UILabel!
    @IBOutlet weak var lblOfficeToHomeTime: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnHomeTime: UIButton!
    @IBOutlet weak var btnWorkTime: UIButton!

    
    
    var selectedStartTime: String = ""
    var selectedReturnTime: String = ""
    var travelType: String = String()
    
    var isStartTimeTapped:Bool = Bool()
    var dateTimeFormatter1:DateFormatter = DateFormatter()
    var timeFormatter1:DateFormatter = DateFormatter()
    var currentUser : UserProfile?
    
    var homeAddressInfo: LocalityInfo?
    var officeAddressInfo: LocalityInfo?
    var isUpdatePreferencesRequest = false
    var onwardRoute: DriveRoute? = nil
    var returnRoute: DriveRoute? = nil
    
    var strTravelTypeInTime : String? = ""
    
    @IBOutlet  var dateTimePickerView:DateTimePickerView! = DateTimePickerView()
    @IBOutlet var messageHandler:MessageHandler! = MessageHandler()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDateFormatter()
        isUpdatePreferencesRequest = false
        self.btnHomeTime.isHidden = true
        self.btnWorkTime.isHidden = true
        self.dateTimePickerView.delegate = self as DateTimePickerDelegate
        self.dateTimePickerView.isTimePicker = true;
        
        lblHomeToOfficeTime.isHidden = true
        lblOfficeToHomeTime.isHidden = true
        
        showShadow(view: viewHomeToOfficeTime, color: .darkGray, opacity: 1, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
        showShadow(view: viewOfficeToHomeTime, color: .darkGray, opacity: 1, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
        
        let tapHomeTime = UITapGestureRecognizer.init(target: self, action: #selector(self.viewTappedHomeToOfficeTime))
        tapHomeTime.delegate = self;
        viewHomeToOfficeTime.addGestureRecognizer(tapHomeTime)
        
        let tapWorkTime = UITapGestureRecognizer.init(target: self, action: #selector(self.viewTappedOfficeToHomeTime))
        tapWorkTime.delegate = self
        viewOfficeToHomeTime.addGestureRecognizer(tapWorkTime)
        
        if UIScreen.main.sizeType == .iPhone5 {
            lblHomeToOfficeTimeTemp.fontSize = 17
            lblOfficeToHomeTimeTemp.fontSize = 17
            lblHomeToOfficeTime.fontSize = 33
            lblOfficeToHomeTime.fontSize = 33
            btnNext.titleLabel?.fontSize = 18
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblHomeToOfficeTimeTemp.fontSize = 18
            lblOfficeToHomeTimeTemp.fontSize = 18
            lblHomeToOfficeTime.fontSize = 35
            lblOfficeToHomeTime.fontSize = 35
            btnNext.titleLabel?.fontSize = 20
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblHomeToOfficeTimeTemp.fontSize = 20
            lblOfficeToHomeTimeTemp.fontSize = 20
            lblHomeToOfficeTime.fontSize = 36
            lblOfficeToHomeTime.fontSize = 36
            btnNext.titleLabel?.fontSize = 20
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = NSLocalizedString(HS_SET_YOUR_TIME, comment: "")
        //self.navigationItem.title =  NSLocalizedString(SIDE_MENU_NEW_TRAVEl_PREFERENCE, comment: "")//"Set your time"
        
      
        navigationItem.title = NSLocalizedString(HS_SET_YOUR_TIME, comment: "");
        currentUser = UserProfile.getCurrentUser()
        travelType =  AppDelegate.getInstance().travelTypeStr;
        if travelType == "RIDER" {
                let saveTxt = NSLocalizedString(HS_SAVE, comment: "")
                btnNext.setTitle(saveTxt, for: UIControlState.normal)
        } else if travelType == "DRIVER" {
                let saveTxt = NSLocalizedString(HS_NEXT, comment: "")
                btnNext.setTitle(saveTxt, for: UIControlState.normal)
        }
    }
    
    func viewTappedHomeToOfficeTime() {
        isStartTimeTapped = true
        self.selectTimeClicked()
    }
    
    func viewTappedOfficeToHomeTime() {
        isStartTimeTapped = false
        self.selectTimeClicked()
    }
    
    func selectTimeClicked() -> Void {
        if(isStartTimeTapped) {
            self.dateTimePickerView.selectedDate = timeFormatter1.date(from: selectedStartTime)
        } else {
            self.dateTimePickerView.selectedDate = timeFormatter1.date(from: selectedReturnTime)
        }
        self.dateTimePickerView.show(in: self.view)
    }
    
    func selectedDate(_ selectedDate: Date!) {
        
        print("selecte dtime is \(selectedDate)")
        let timeStr:String = AppDelegate.getInstance().timeFormatter.string(from: selectedDate)
        if(isStartTimeTapped) {
            selectedStartTime = timeFormatter1.string(from: selectedDate)
            if(!self.viewTempHomeToOfficeTime.isHidden) {
                self.viewTempHomeToOfficeTime.isHidden = true;
                self.lblHomeToOfficeTime.isHidden = false;
                self.btnHomeTime.isHidden = false
            }
            self.lblHomeToOfficeTime.text = timeStr;
            
        } else {
            selectedReturnTime = timeFormatter1.string(from: selectedDate)
            if(!self.viewTempOfficeToHomeTime.isHidden) {
                self.viewTempOfficeToHomeTime.isHidden = true;
            }
            self.lblOfficeToHomeTime.isHidden = false;
            self.lblOfficeToHomeTime.text = timeStr;
            self.btnWorkTime.isHidden = false
        }
    }
    
    func convertDateTimeToTime(dateWithTime:Date) -> String {
        return AppDelegate.getInstance().timeFormatter.string(from:dateWithTime)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Actions
    @IBAction func btnTappedEditHomeToOffTime(_ sender: Any) {
        viewTappedHomeToOfficeTime()
    }
    @IBAction func btnTappedEditOffToHomeTime(_ sender: Any) {
        viewTappedOfficeToHomeTime()
    }
    @IBAction func btnTappedClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTappedNext(_ sender: Any) {
        
        guard validateUserPreferences() else {
            return
        }
        
        if(selectedStartTime.count == 0){
            print("Please select Start time")
        }else if(selectedReturnTime.count == 0){
            print("Please select return time")
        } else {
            
            if currentUser?.userPreferences == nil {
                if strTravelTypeInTime == "RIDER" {
                   // self.callServices()
                } else if strTravelTypeInTime == "DRIVER" {
                    let vcSetRouteMap = self.storyboard?.instantiateViewController(withIdentifier: "SetRouteMapController") as? SetRouteMapController
                    vcSetRouteMap?.selectedStartTime = selectedStartTime
                    vcSetRouteMap?.selectedReturnTime = selectedReturnTime
                    self.navigationController?.pushViewController(vcSetRouteMap!, animated: true)
                }
            } else {
                if currentUser?.userPreferences.userMode == "RIDER" {
                  //  self.callServices()
                } else {
                    travelType = NSLocalizedString(VC_USERPREFERENCES_DRIVERVAL, comment: "")
                    let vcSetRouteMap = self.storyboard?.instantiateViewController(withIdentifier: "SetRouteMapController") as? SetRouteMapController
                    vcSetRouteMap?.selectedStartTime = selectedStartTime
                    vcSetRouteMap?.selectedReturnTime = selectedReturnTime
                    self.navigationController?.pushViewController(vcSetRouteMap!, animated: true)
                }
            }
            
        }
    }
    
    func setDateFormatter() -> Void {
        let locale:Locale  = Locale.init(identifier: "en_US_POSIX")
        dateTimeFormatter1 = DateFormatter();
        dateTimeFormatter1.dateFormat = "yyyy/MM/dd HH:mm"
        dateTimeFormatter1.locale = locale
        
        timeFormatter1 = DateFormatter();
        timeFormatter1.dateFormat = "HH:mm:ss"
        timeFormatter1.locale = locale
    }
    
    
    // MARK: - Service Call
    
/*    func callServices() {
        if validateUserPreferences() {
            messageHandler.showBlockingLoadView(handler: {
                var request: UserPreferencesRequest?
                self.homeAddressInfo = AppDelegate.getInstance().homeAddress
                self.officeAddressInfo = AppDelegate.getInstance().officeAddress
                self.onwardRoute = nil
                self.returnRoute = nil
                if self.isUpdatePreferencesRequest {
                    request = UserPreferencesRequest(with: UPDATEPREFERENCES, andHomeAddress: self.homeAddressInfo, andOfficeAddress: self.officeAddressInfo, andStartTime: self.selectedStartTime, andReturnTime: self.selectedReturnTime, andOnwardRoute: self.onwardRoute, andReturn: self.returnRoute, andUserMode: self.travelType)
                } else {
                    request = UserPreferencesRequest(with: ADDPREFERENCES, andHomeAddress: self.homeAddressInfo, andOfficeAddress: self.officeAddressInfo, andStartTime: self.selectedStartTime, andReturnTime: self.selectedReturnTime, andOnwardRoute: self.onwardRoute, andReturn: self.returnRoute, andUserMode: self.travelType)
                }
                ServerInterface.sharedInstance().getResponse(request, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            ///Refer and earn service call for process the referral amount to parent user and child user
                            ReferAndEarnController.serviceCall_ProcessReferralData(eventId: 2, strUserType: "RIDER")
                            let pref = UserDefaults.standard;
                            pref.set(true, forKey: "tpUpdated")
                            pref.synchronize()
                            let strSuccessMsg = NSLocalizedString(TP_SCREEN4_SUCCESS_MSG, comment: "") + "\n" + NSLocalizedString(TP_SCREEN4_SUCCESS_MSG1, comment: "") + "\n" + NSLocalizedString(TP_SCREEN4_SUCCESS_MSG2, comment: "") + "\n" + NSLocalizedString(TP_SCREEN4_SUCCESS_MSG3, comment: "")
                            
                            let alertCtrl = UIAlertController.init(title: AppName, message: strSuccessMsg, preferredStyle: .alert)
                            let okAction = UIAlertAction.init(title: NSLocalizedString(CMON_GENERIC_OK, comment: ""), style: .default, handler: { (alertView) in
                                self.navigationController?.popToRootViewController(animated: true)
                            })
                            alertCtrl.addAction(okAction)
                            self.present(alertCtrl, animated: true, completion: nil)
                            //self.messageHandler.showSuccessMessage(strSuccessMsg)
                            //self.createViewForSuccessOnServiceCall()
                        } else {
                            showAlertView(strMessage: (error as NSError?)?.userInfo["message"] as! String, navigationCtrl: self.navigationController!)
                            //self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                            //self.errorMessage.text = (error as NSError?)?.userInfo["message"]
                        }
                    })
                })
            })
        }
    }*/
    
    func validateUserPreferences() -> Bool {
        if AppDelegate.getInstance().homeAddress == nil {
            showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_HOMEADDRESSVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if AppDelegate.getInstance().officeAddress == nil {
            showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_OFFICEADDRESSVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if selectedStartTime == "" || selectedStartTime.count == 0 {
            
            showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_STARTTIMEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if selectedReturnTime == "" || selectedReturnTime.count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_USERPREFERENCES_RETURNTIMEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
