//
//  SideMenuController.swift
//  zify
//
//  Created by Anurag on 05/07/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class SideMenuController: UIViewController, UITableViewDelegate, UITableViewDataSource, MenuDelegate {
    
    

    @IBOutlet weak var tblSideMenu: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnUserName: UIButton!
    @IBOutlet weak var lblVerification: UILabel!
    @IBOutlet weak var lblProfileCompletenessPercentage: UILabel!
    @IBOutlet weak var progressViewProfileCompleteness: UIProgressView!
    
    var arrOptionsData : [String] = []
    var arrImagesNames : [String] = []
    var menuDelegate: MenuDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrOptionsData =  [SIDE_MENU_NEW_TRAVEl_PREFERENCE,
                           SIDE_MENU_NEW_ACTIVE_TRIPS,
                           SIDE_MENU_NEW_MESSAGE,
                           SIDE_MENU_NEW_WALLET,
                           SIDE_MENU_NEW_UPCOMING_TRIPS,
                           SIDE_MENU_NEW_HISTORY,
                           /*SIDE_MENU_NEW_REFER_AND_EARN,*/
                            SIDE_MENU_NEW_REWARDS,
                            SIDE_MENU_NEW_SETTINGS,
                            SIDE_MENU_NEW_ABOUT,
                            SIDE_MENU_NEW_LEAVE_FEEDBACK]
        
        arrImagesNames = ["skyline.png", "location-on-road.png", "chat1.png", "wallet1.png", "briefcase.png", "key.png", "Rewards.png", "settings1.png", "ic_about.png", "call-service.png"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func btnTappedUserProfile(_ sender: Any) {
        
        self.present(UserProfileController.createUserProfileNavController(), animated: true)
    }
    
    
    // MARK: - TableView Delegates
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOptionsData.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellSideMenu: SideMenuCell = (self.tblSideMenu.dequeueReusableCell(withIdentifier: "SideMenuCell") as? SideMenuCell)!
        cellSideMenu.lblNamesOptions?.text = self.arrOptionsData[indexPath.row]
        cellSideMenu.imgIcons.image = UIImage.init(contentsOfFile: self.arrImagesNames[indexPath.row])
        
        return cellSideMenu
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_TRAVEl_PREFERENCE {
            
            AppDelegate.getInstance().move(toTravelPrefenceScreen: self)
            
        } else if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_ACTIVE_TRIPS {

            self.present(TripsTabController.createTripsTabNavController(), animated: true)
        } else if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_HISTORY {
            
            self.present(AccountController.createAccountNavController(), animated: true)
        } else if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_WALLET {
            
            let walletNavController = AddMoneyController.createUserWalletNavController()//UserWalletController.createUserWalletNavController()
            if let aController = walletNavController {
                present(aController, animated: true)
            }
        } else if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_UPCOMING_TRIPS {
            
            self.present(TripsTabController.createTripsTabNavController(), animated: true)
        } else if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_REWARDS {
            
            self.present(RewardsController.createRewardsNavController()!, animated: true)
        } else if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_SETTINGS {
            
            self.present(SettingsViewController.createSettingsNavController()!, animated: true)
        } else if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_ABOUT {
            
            self.present(AboutController.createAboutNavController()!, animated: true)
        } else if (tblSideMenu.cellForRow(at: indexPath)! as? SideMenuCell)?.lblNamesOptions.text == SIDE_MENU_NEW_LEAVE_FEEDBACK {
            
            Freshchat.sharedInstance()?.showConversations(self)
            //disable on 6-Aug-19
            /*
            let prefs = UserDefaults.standard
            prefs.set(true, forKey: "forDismissing")
            prefs.synchronize()
            self.present(AppDelegate.createFeedbackNavController(self), animated: true)
             */
        }
        
        
        
        
        
        
        
        
        /*
        if option == TRAVELPREFERENCE {
            AppDelegate.getInstance().move(toTravelPrefenceScreen: self)
        } else if option == TRIPS {
            self.present(TripsTabController.createTripsTabNavController(), animated: true)
        } else if option == ACCOUNT {
            self.present(AccountController.createAccountNavController(), animated: true)
        } else if option == WALLET {
            let walletNavController = UserWalletController.createUserWalletNavController() as? UINavigationController
            if let aController = walletNavController {
                present(aController, animated: true)
            }
        } else if option == PROFILE {
            self.present(UserProfileController.createUserProfileNavController(), animated: true)
        } else if option == CURRENTTRIP {
            
        } else if option == REWARDS {
            self.present(RewardsController.createRewardsNav(), animated: true)
        } else if option == SETTINGS {
            self.present(SettingsController.createSettingsNavController(), animated: true)
        } else if option == ABOUT {
            self.present(AboutController.createAboutNavController(), animated: true)
        } else if option == REFERANDEARN {
            messageHandler.showBlockingLoadView(withHandler: {
                AppViralityHelper.getCamapaignDetails({ campaignDetails in
                    self.messageHandler.dismissBlockingLoadView(withHandler: {
                        AppViralityHelper.showGrowthHack(onController: self, withDetails: campaignDetails)
                    })
                })
            })
        } else if option == MESSAGES {
            self.present(ChatUserListController.createChatNavController(), animated: true)
        } else if option == WALLETSTATEMENT {
            self.present(WalletStatementController.createWalletStatementNavController(), animated: true)
        } else if option == LEAVEFEEDBACK {
            let prefs = UserDefaults.standard
            prefs.set(true, forKey: "forDismissing")
            prefs.synchronize()
            self.present(AppDelegate.createFeedbackNavController(self), animated: true)
        }
 */
        
        
        
        
    }
    
    // MARK:- Funtions
    
    
    
    // MARK:- MenuDelegate
    
    func selectedMenuOption(_ option: MenuOptions) {
        
    }
    
}

class SideMenuCell : UITableViewCell {
    @IBOutlet weak var imgIcons: UIImageView!
    @IBOutlet weak var lblNamesOptions: UILabel!
}



