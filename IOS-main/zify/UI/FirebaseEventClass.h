//
//  FirebaseEventClass.h
//  zify
//
//  Created by Anurag S Rathor on 06/01/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FirebaseEventClass : NSObject

+(void)storeRideSearchEventFromSearchScreen;

@end
