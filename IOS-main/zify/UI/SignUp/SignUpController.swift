//
//  SignUpController.swift
//  SignUp
//
//  Created by Anurag on 05/06/18.
//  Copyright © 2018 Anurag. All rights reserved.
//
/*
 side menu name
 Yoir travel pref Top Bototm
 Reset password tip on serach results
 chat new messages and send buton
 color
 navigation bars
 icons
 switch btn
 
 */
import UIKit
import FBSDKLoginKit

var dictSignUpObj : Dictionary? = [:]


@available(iOS 9.0, *)
class SignUpController: UIViewController, FaceBookLoginDelgate {
    
    @IBOutlet weak var btnSkip: UIBarButtonItem!
    @IBOutlet weak var lblWelcometoZift: UILabel!
   // @IBOutlet weak var lblAlreadyHaveAnAccount: UILabel!
    @IBOutlet weak var btnContinueWithFb: UIButton!
    @IBOutlet weak var btnJoinNow: UIButton!
   @IBOutlet weak var btnSignIn: UIButton!

    
    var emailStr = ""
    var fbLoginResponse: FBLoginResponse?
    @IBOutlet var messageHandler: MessageHandler!
    var blurView:UIView = UIView()
    var alertBgView:UIView = UIView()
    
    var isInitialLocationFetch = false
    var sourceLocalityInfo: LocalityInfo?
    var destinationLocalityInfo: LocalityInfo?
    var userSearchData: UserSearchData?
    var isGlobalLocale = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true;
        UserDefaults.standard.removeObject(forKey: "fbURL")
        if UIScreen.main.sizeType == .iPhone5 {
            lblWelcometoZift.fontSize = 14
            btnSignIn.titleLabel?.fontSize = 16
            btnJoinNow.titleLabel?.fontSize = 16
            btnContinueWithFb.titleLabel?.fontSize = 16;
        } else if UIScreen.main.sizeType == .iPhone6 {
            lblWelcometoZift.fontSize = 15
            btnSignIn.titleLabel?.fontSize = 18
            btnJoinNow.titleLabel?.fontSize = 18
            btnContinueWithFb.titleLabel?.fontSize = 18;
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR  || UIScreen.main.sizeType == .iPhoneXSMax {
            lblWelcometoZift.fontSize = 16
            btnSignIn.titleLabel?.fontSize = 19
            btnJoinNow.titleLabel?.fontSize = 19
            btnContinueWithFb.titleLabel?.fontSize = 19;
        }
       // CurrentLocationTracker.sharedInstance()?.updateCurrentLocation()
        AppDelegate.getInstance().homeAddress = nil
        AppDelegate.getInstance().officeAddress = nil
        AppDelegate.getInstance().homeToOffDriveRoute = nil
        AppDelegate.getInstance().oFFToHomeDriveRoute = nil
        AppDelegate.getInstance().userRegion = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(dictSignUpObj?.count != 0){
            dictSignUpObj?.removeAll()
        }
        self.navigationController?.isNavigationBarHidden = true
       
        AppDelegate.getInstance().isShowUpdatedAlert()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    
    class func createSignUpNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "SignUp", bundle: Bundle.main)
        let signUpNavController = storyBoard.instantiateViewController(withIdentifier: "signUpNavController")
        return signUpNavController as? UINavigationController
    }
    
    // MARK:- Actions
    
    @IBAction func btnTappedSkip(_ sender: Any) {
        
    }
    
    @IBAction func btnTappedSignIn(_ sender: Any) {
        let signInWithEmail = NSLocalizedString(SS_SIGN_IN_WITH_EMAIL_BTN, comment: "")
        let continueWithFaceBook = NSLocalizedString(MS_EMAILLOGIN_LBL_LOGINWITHFB, comment: "")
        let cancel = NSLocalizedString(SS_CANCEL, comment: "")

        let actionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        let actionSignUpEmail = UIAlertAction.init(title: signInWithEmail, style: .default) { (UIAlertAction) in
            self.moveToEmailLoginScreen()
        }
        let continueWithFacebook = UIAlertAction.init(title: continueWithFaceBook, style: .default) { (UIAlertAction) in
            self.loginWithFaceBook()
        }
        let cancelAction = UIAlertAction.init(title: cancel, style: .cancel) { (UIAlertAction) in
        }
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        actionSheet.addAction(actionSignUpEmail)
        actionSheet.addAction(continueWithFacebook)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func moveToEmailLoginScreen()  {
        //loginUserSegue
        AppDelegate.getInstance().emailStrFromFB = self.emailStr
        let stb = UIStoryboard(name: "SignUp", bundle: Bundle.main)
        let loginViewCtrl = stb.instantiateViewController(withIdentifier: "LoginController") as? LoginController
        self.navigationController?.pushViewController(loginViewCtrl!, animated: true)
    }
    @IBAction func btnTappedJoinNow(_ sender: Any) {
        dictSignUpObj?.updateValue("", forKey: "loginMode")
        let signUpWithEmail = NSLocalizedString(SS_SIGN_UP_WITH_EMAIL, comment: "")
        let continueWithFaceBook = NSLocalizedString(MS_EMAILLOGIN_LBL_LOGINWITHFB, comment: "")
        let cancel = NSLocalizedString(SS_CANCEL, comment: "")
        
        let actionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        let actionSignUpEmail = UIAlertAction.init(title: signUpWithEmail, style: .default) { (UIAlertAction) in
            let vcSignUpName = self.storyboard?.instantiateViewController(withIdentifier: "SignUpNameController") as? SignUpNameController
            self.navigationController?.pushViewController(vcSignUpName!, animated: true)
        }
        let continueWithFacebook = UIAlertAction.init(title: continueWithFaceBook, style: .default) { (UIAlertAction) in
            self.loginWithFaceBook()
        }
        let cancelAction = UIAlertAction.init(title: cancel, style: .cancel) { (UIAlertAction) in
        }
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        actionSheet.addAction(actionSignUpEmail)
        actionSheet.addAction(continueWithFacebook)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func loginWithFaceBook() {
        dictSignUpObj?.updateValue("fb", forKey: "loginMode")
        FacebookLogin.sharedInstance().closeSessionAndClearSavedTokens()
        let sharedInstance = FacebookLogin.sharedInstance()
        sharedInstance?.faceBookLoginDelgate = self
        sharedInstance?.loginWithFacebook()
    }
    @IBAction func btnContinueWithFBTapped(){
        self.loginWithFaceBook();
    }

    func loggedIn(withFaceBook loginResponse: FBLoginResponse?) {
            if loginResponse?.email == nil || loginResponse?.email.count == 0 {
                UIAlertController.showError(withMessage: NSLocalizedString(VC_LOGIN_FB_NO_EMAIL, comment: ""), inViewController: self)
                return
            }
        self.callServiceForFBCheckUser(loginResponse: loginResponse!)

       // self.callServiceForLoginWithFB(loginResponse: loginResponse!)
    }
    func callServiceForFBCheckUser(loginResponse:FBLoginResponse) {
        var fbAccessToken = FacebookLogin.sharedInstance().currentAccessToken();
        if(fbAccessToken == nil){
            fbAccessToken = ""
        }
        
        dictSignUpObj?.updateValue(fbAccessToken!, forKey: "userPswd")
        
        let checkRequest = FBCheckUserRequest(email: loginResponse.email, andFbId: loginResponse.facebookId, andFbToken: fbAccessToken)
        messageHandler.showBlockingLoadView{
            ServerInterface.sharedInstance().getResponse(checkRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView{
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            self.completeUserLogin()
                            PushNotificationManager.sharedInstance().handlePushRegisterWithUser()
                            
                        } else if (messageCode == -53){
                            UIAlertController.showError(withMessage: FB_INVALID_TOKEN, inViewController: self)
                            return;
                            // Invalid Token
                        }else if (messageCode == -54 || messageCode == -6){
                            // already exists with email sign in
                            self.showAlertView(toEmailLogin: loginResponse.email)
                            
                        }else if (messageCode == -55){
                            // email does not exixis.. move to SignUp flow..
                            UserProfile.deleteCurrentUser()
                            self.fbLoginResponse = loginResponse
                            self.moveToMobileNumberScreen(fbResponse: self.fbLoginResponse!)
                        }else if (messageCode == -56){
                            // logout
                            UserProfile.deleteCurrentUser()
                            //UIAlertController.showError(withMessage: "Please Login again.", inViewController: self)
                            UIAlertController.showError(withMessage: NSLocalizedString(SS_LOGIN_AGAIN, comment: ""), inViewController: self)
                        }
                    } else {
                        guard let locerror = error as NSError? else {return}
                        let errorCode:Int = locerror.code
                        if errorCode == -6 {
                            self.showAlertView(toEmailLogin: loginResponse.email)
                        } else if (errorCode == -53){
                            UIAlertController.showError(withMessage: FB_INVALID_TOKEN, inViewController: self)
                            return;
                            // Invalid Token
                        } else if (errorCode == -54 || errorCode == -6){
                            // already exists with email sign in
                            self.showAlertView(toEmailLogin: loginResponse.email)
                            
                        } else if (errorCode == -55){
                            // email does not exixis.. move to SignUp flow..
                            UserProfile.deleteCurrentUser()
                            self.fbLoginResponse = loginResponse
                            self.moveToMobileNumberScreen(fbResponse: self.fbLoginResponse!)
                        } else if (errorCode == -56){
                            // logout
                            UserProfile.deleteCurrentUser()
                            //UIAlertController.showError(withMessage: "Please Login again.", inViewController: self)
                            UIAlertController.showError(withMessage: NSLocalizedString(SS_LOGIN_AGAIN, comment: ""), inViewController: self)
                        } else {
                            let errorStr:String = ((locerror).userInfo["message"] as? String)!
                            UIAlertController.showError(withMessage: errorStr, inViewController: self)
                        }
                    }
                }
            })
        }
    }
    func callServiceForLoginWithFB(loginResponse:FBLoginResponse){

        let loginRequest = FBUserLoginRequest(firstName: loginResponse.firstName, andLastName: loginResponse.lastName, andUserEmail: loginResponse.email, andisdCode: "00", andMobile: "0000000000", andGender: loginResponse.gender, andFbProfileId: loginResponse.facebookId)
        messageHandler.showBlockingLoadView{
            ServerInterface.sharedInstance().getResponse(loginRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView{
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            self.completeUserLogin()
                            PushNotificationManager.sharedInstance().handlePushRegisterWithUser()
                        } else {
                            UserProfile.deleteCurrentUser()
                            self.fbLoginResponse = loginResponse
                            self.moveToMobileNumberScreen(fbResponse: self.fbLoginResponse!)
                            //self.performSegue(withIdentifier: "registerUserSegue", sender: self)
                        }
                    } else {
                        guard let locerror = error as NSError? else {return}
                        let errorCode:Int = locerror.code
                        if errorCode == -6 {
                            self.showAlertView(toEmailLogin: loginResponse.email)
                        } else {
                            let errorStr:String = ((locerror).userInfo["message"] as? String)!
                            UIAlertController.showError(withMessage: errorStr, inViewController: self)
                        }
                    }
                }
            })
        }
    }
    func moveToMobileNumberScreen(fbResponse:FBLoginResponse){
        debugPrint(fbResponse)
        if(fbResponse.email == nil || fbResponse.email.count == 0) {
            UIAlertController.showError(withMessage: NSLocalizedString(fb_login_no_email_error, comment: ""), inViewController: self)
            return;
        }
        dictSignUpObj?.updateValue(fbResponse.firstName!, forKey: "fname")
        dictSignUpObj?.updateValue(fbResponse.lastName!, forKey: "lname")
        dictSignUpObj?.updateValue("", forKey: "gender")
        dictSignUpObj?.updateValue(fbResponse.email!, forKey: "userEmail")
        dictSignUpObj?.updateValue(fbResponse.facebookId!, forKey: "fbProfileId")
        dictSignUpObj?.updateValue("fb", forKey: "loginMode")

       /* let stb = UIStoryboard(name: "SignUp", bundle: Bundle.main)
        let loginViewCtrl = stb.instantiateViewController(withIdentifier: "SignUpPhoneNumberController") as? SignUpPhoneNumberController
        self.navigationController?.pushViewController(loginViewCtrl!, animated: true)*/
        
        let vcEmail = self.storyboard?.instantiateViewController(withIdentifier: "SignUpEmailController") as? SignUpEmailController
        vcEmail?.isfromFacebook = true
        //vcEmail?.strUserName = txtFirstName.text!.capitalized + " " + txtLastName.text!
        self.navigationController?.pushViewController(vcEmail!, animated: true)
    }
    
    func completeUserLogin() {
        OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)
    }

    func unableToLoginFBWithError(_ error: Error!) {
       // if let anError = error {
         //   print("error is \(anError)")
            UIAlertController.showError(withMessage: NSLocalizedString(VC_LOGIN_FBAUTHERROR, comment: ""), inViewController: self)
       // }
    }
   
    func showAlertView(toEmailLogin email: String?) {
        emailStr = email ?? ""
        blurView = UIView()
        blurView.frame = CGRect(x: 0, y: 0, width: AppDelegate.screen_WIDTH(), height: AppDelegate.screen_HEIGHT())
        blurView.backgroundColor = UIColor.black
        blurView.alpha = 0.8
        self.navigationController?.view.addSubview(blurView)
        let posX: CGFloat = 20
        alertBgView = UIAlertView()
        alertBgView.frame = CGRect(x: posX, y: AppDelegate.screen_HEIGHT(), width: AppDelegate.screen_WIDTH() - 2 * posX, height: 100)
        alertBgView.backgroundColor = UIColor.white
        alertBgView.layer.cornerRadius = 5.0
        self.navigationController?.view.addSubview(alertBgView)
        let alertView: UIView? = UIAlertView()
        alertView?.frame = CGRect(x: 0, y: 0, width: alertBgView.frameWidth, height: 100)
        alertView?.backgroundColor = UIColor.clear
        if let aView = alertView {
            alertBgView.addSubview(aView)
        }
        let alertLbl = UILabel()
        alertLbl.textAlignment = .center
        alertLbl.frame = CGRect(x: 5, y: 10, width: (alertView?.frameWidth)!  - 10, height: 45)
        alertLbl.numberOfLines = 2
        alertLbl.text = NSLocalizedString(MS_EMAILLOGIN_LOGIN_WITH_EMAIL_ERROR, comment: "")
        //
        if let aSize = UIFont(name: NormalFont, size: 16) {
            alertLbl.font = aSize
        }
        alertLbl.backgroundColor = UIColor.clear
        alertLbl.textColor = UIColor.gray
        alertView?.addSubview(alertLbl)
        let mailLbl = UILabel()
        mailLbl.frame = CGRect(x: 5, y: alertLbl.frameMaxY + 10, width: (alertView?.frameWidth)!  - 10, height: 30)
        mailLbl.textAlignment = .center
        if let aSize = UIFont(name: MediumFont, size: 15) {
            mailLbl.font = aSize
        }
        mailLbl.text = email
        mailLbl.numberOfLines = 2
        alertView?.addSubview(mailLbl)
        let lineLbl1 = UILabel()
        lineLbl1.frame = CGRect(x: 0, y: mailLbl.frameMaxY + 10, width: (alertView?.frameWidth)!, height: 1)
        lineLbl1.backgroundColor = UIColor.lightGray
        alertView?.addSubview(lineLbl1)
        let btnEmilLogin = UIButton(type: .custom)
        btnEmilLogin.frame = CGRect(x: 5, y: lineLbl1.frameMaxY + 10, width: (alertView?.frameWidth)! - 10, height: 50)
        let title = NSLocalizedString(MS_EMAIL_LOGIN_FROM_ERROR, comment: "")
        btnEmilLogin.setTitle(title, for: .normal)
        btnEmilLogin.setTitle(title, for: .selected)
        if let aSize = UIFont(name: NormalFont, size: 15) {
            btnEmilLogin.titleLabel?.font = aSize
        }
        btnEmilLogin.layer.cornerRadius = 5.0
        btnEmilLogin.backgroundColor = UIColor(red: 231.0 / 255.0, green: 76.0 / 255.0, blue: 58.0 / 255.0, alpha: 1.0)
        btnEmilLogin.addTarget(self, action: #selector(self.btnEmilLoginTapped(_:)), for: .touchUpInside)
        alertView?.addSubview(btnEmilLogin)
        let lineLbl2 = UILabel()
        lineLbl2.frame = CGRect(x: 0, y: btnEmilLogin.frameMaxY + 10, width: alertView?.frameWidth ?? 0.0, height: 1)
        lineLbl2.backgroundColor = UIColor.lightGray
        alertView?.addSubview(lineLbl2)
        alertView?.frameHeight = lineLbl2.frameMaxY + 2
        let cancelImage1 = AppDelegate.getInstance().getNewImage(withOldImage: UIImage(named: "cancel_cross.png"), withNewColor: UIColor.lightGray)
        let cancelImage = UIImageView()
        cancelImage.frame = CGRect(x: ((alertView?.frameWidth)!  - 25) / 2, y: (alertView?.frameMaxY)! + 10, width: 25, height: 25)
        cancelImage.image = cancelImage1
        alertBgView.addSubview(cancelImage)
        let btnCancel = UIButton(type: .custom)
        btnCancel.frame = CGRect(x: ((alertView?.frameWidth)!  - 40) / 2, y: alertView?.frameMaxY ?? 0.0, width: 40, height: 40)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.addTarget(self, action: #selector(self.btnCancelTappped(_:)), for: .touchUpInside)
        alertBgView.addSubview(btnCancel)
        alertBgView.frameHeight = btnCancel.frameMaxY + 10
        
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseIn, animations: {
            self.alertBgView.frameY = (self.view.frameHeight - self.alertBgView.frameHeight) / 2
        }) { finished in
        }
    }
    
    func btnCancelTappped(_ sender: Any?) {
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseIn, animations: {
            self.alertBgView.frameY = AppDelegate.screen_HEIGHT()
        }) { finished in
            if finished {
                self.alertBgView.removeFromSuperview()
            }
            self.blurView.removeFromSuperview()
        }
    }
    
    func btnEmilLoginTapped(_ sender: Any?) {
        self.btnCancelTappped(nil)
        AppDelegate.getInstance().emailStrFromFB = emailStr
        self.moveToEmailLoginScreen()
    }
    
}

