//
//  SignUpPhoneNumberController.swift
//  SignUp
//
//  Created by Anurag on 05/06/18.
//  Copyright © 2018 Anurag. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift


class SignUpPhoneNumberController: UIViewController, UITextFieldDelegate, CountryCodeSelectionDelegate {
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var btnWhyMyMobNo: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblEnterVerificationCode: UILabel!

    
    var selectedCountryCode : CountryCode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedCountryCode = CountryCode.getCountryObject(forISOCode: CurrentLocale.sharedInstance().getISOCode(), in: DBInterface.sharedInstance().sharedContext)
        self.addLeftView(txtField: txtCountryCode)
        self.addLeftView(txtField: txtMobileNumber)

        
        if (selectedCountryCode != nil) {
            txtCountryCode.text = selectedCountryCode?.isdCode
            dictSignUpObj?.updateValue(selectedCountryCode!.isoCode, forKey: "countrycode")
            dictSignUpObj?.updateValue(selectedCountryCode!.isdCode, forKey: "isdCode")
        }
        if UIScreen.main.sizeType == .iPhone5 {
            lblHeader.fontSize = 18
            btnWhyMyMobNo.titleLabel?.fontSize = 14
            btnVerify.titleLabel?.fontSize = 18
            lblEnterVerificationCode.fontSize = 12
        } else if UIScreen.main.sizeType == .iPhone6 {
            lblHeader.fontSize = 19
            btnWhyMyMobNo.titleLabel?.fontSize = 15
            btnVerify.titleLabel?.fontSize = 19
            lblEnterVerificationCode.fontSize = 13
        } else if UIScreen.main.sizeType == .iPhone6Plus {
            lblHeader.fontSize = 20
            btnWhyMyMobNo.titleLabel?.fontSize = 16
            btnVerify.titleLabel?.fontSize = 20
            lblEnterVerificationCode.fontSize = 14
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (txtMobileNumber as UITextField).becomeFirstResponder()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK:- Action
    
    @IBAction func btnTappedVerify(_ sender: Any) {
        self.view.endEditing(true)
        dictSignUpObj?.updateValue("\(txtMobileNumber.text!)", forKey: "mobile")
        serviceCall_SignUp()
    }
    
    @IBAction func btnTappedWhyMyMobNumber(_ sender: Any) {
        
    }
    
    // MARK:- Textfield Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCountryCode {
            self.view.endEditing(true)
            let countryCodeNavCtrl = CountryCodeSuggestionController.createCountryCodeNavController()
            let countryCodeController = countryCodeNavCtrl?.topViewController as? CountryCodeSuggestionController
            countryCodeController?.selectionDelegate = self
            self.present(countryCodeNavCtrl!, animated: true, completion: nil)
        }
    }
    
    // MARK:- Country Code Delegates
    func selectedCountryCode(_ countryCode: CountryCode!) {
        selectedCountryCode = countryCode
        txtCountryCode.text = "\(String(describing: selectedCountryCode.isdCode!))"
        dictSignUpObj?.updateValue(txtCountryCode.text!, forKey: "isdCode")
        dictSignUpObj?.updateValue("\(selectedCountryCode.isoCode!)", forKey: "countrycode")
    }
    
    func addLeftView(txtField:UITextField) {
        let customLeftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
        txtField.leftView = customLeftView;
        txtField.leftViewMode = .always
    }
    
    // MARK:- Service Call
    func serviceCall_SignUp() {
       /* var errorMessage = ""
        
        if validateUserInfo() {
            
            let registerReq = self.getRegisterRequest()
            
            messageHandler.showBlockingLoadView(handler: {
                
                ServerInterface.sharedInstance().getResponseWithoutImage(registerReq, withHandler: { (response, error) in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            let messageCode:Int = Int((response?.messageCode)!)!
                            if messageCode == 1 {
                            self.setBoolForAKeyForSignUpFlow()
                            self.completeUserLogin()
                            }
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                            errorMessage = (error as NSError?)?.userInfo["message"] as! String
                        }
                    })
                })
            })
        } else {
            showAlertView(strMessage: errorMessage, navigationCtrl: self.navigationController!)
        }*/
    }
    
    
    
    func setBoolForAKeyForSignUpFlow() {
        let prefs = UserDefaults.standard
        prefs.set(true, forKey: "isFromSignUp")
        prefs.synchronize()
    }
    
    func completeUserLogin() {
        dismiss(animated: false)
        //let vcVerifyCodeCtrl = self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeController") as? VerificationCodeController
        //self.navigationController?.pushViewController(vcVerifyCodeCtrl!, animated: true)
        OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)
    }
    
    func validateUserInfo() -> Bool {
        if (dictSignUpObj!["fname"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_FIRSTNAMEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if (dictSignUpObj!["lname"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_LASTNAMEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if (dictSignUpObj!["userEmail"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_EMAILVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if !isValidEmail(emailStr: (dictSignUpObj!["userEmail"] as! String)) { //!validateEmail(self.txtEmail.text) {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_EMAILVALIDVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if (dictSignUpObj!["mobile"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_MOBILEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        return true
    }
    
    func dismiss()  {
        self.view.endEditing(true)
    }
    
    
    
    
   /* func getRegisterRequest() -> UserRegisterRequest {
        let fname = dictSignUpObj!["fname"] as AnyObject
        let lanme = dictSignUpObj!["lname"] as AnyObject
        let userEmail = dictSignUpObj!["userEmail"] as AnyObject
        let userPswd = dictSignUpObj!["userPswd"] as AnyObject
        let isdCode = dictSignUpObj!["isdCode"] as AnyObject
        let mobile = dictSignUpObj!["mobile"] as AnyObject
        let gender = dictSignUpObj!["gender"] as AnyObject
        let countrycode = dictSignUpObj!["countrycode"] as AnyObject
        let loginMode = "fb" as AnyObject
        let promoCode = "" as AnyObject
        let registerRequest = UserRegisterRequest(firstName: fname as! String, andLastName: lanme as! String, andUserEmail: userEmail as! String, andUserPswd: userPswd as! String, andisdCode:isdCode as! String, andMobile: mobile as! String, andGender: gender as! String, andLoginMode: loginMode as! String, andPromoCode: promoCode as! String, andCountryCode:countrycode as! String)
        return registerRequest!
        
    }*/
    
    
}
