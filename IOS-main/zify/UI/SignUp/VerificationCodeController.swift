//
//  VerificationCodeController.swift
//  zify
//
//  Created by Anurag on 13/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit


class VerificationCodeController: UIViewController, SingleCharPasswordViewDelegate, CountryCodeSelectionDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var lblSubHeader: UILabel!
    @IBOutlet weak var lblYourMobileNoGetsVerified: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    
    
    @IBOutlet weak var viewOTP: SingleCharPasswordView!
    
    var selectedCountryCode: CountryCode!
    var alertCtrlNumber = UIAlertController()
    var isCountryCodeSelected : Bool?
    var updatedMobileNumber:String = ""
    var txtMobileNum:TextFieldWithDone = TextFieldWithDone()
    var isSameNumberToUpdate:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        if UIScreen.main.sizeType == .iPhone5 {
            lblHeader.fontSize = 18
            lblSubHeader.fontSize = 16
            btnResend.titleLabel?.fontSize = 14
            btnVerify.titleLabel?.fontSize = 18
            lblYourMobileNoGetsVerified.fontSize = 12
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblHeader.fontSize = 20
            lblSubHeader.fontSize = 13
            btnResend.titleLabel?.fontSize = 15
            btnVerify.titleLabel?.fontSize = 19
            lblYourMobileNoGetsVerified.fontSize = 12
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblHeader.fontSize = 20
            lblSubHeader.fontSize = 14
            btnResend.titleLabel?.fontSize = 16
            btnVerify.titleLabel?.fontSize = 20
            lblYourMobileNoGetsVerified.fontSize = 13
        }
       
        self.setSubHeader()
        if(self.updatedMobileNumber.count > 0 ){
            
            self.getUserOTP()
        }else{
            self.taponEditNumber()
        }
        self.viewOTP.passwordDelegate = self
    }
    
    func  setSubHeader()  {
        let strVerificationCodeHasBeenSentTo = NSLocalizedString(SS_VERIFICATION_CODE_HAS_BEEN_SENT_TO_LBL, comment: "")
        let userProfile = UserProfile.getCurrentUser()
       // userProfile?.mobile = nil
        self.updatedMobileNumber = (userProfile?.mobile) ??  ""
        let isdCode = (userProfile?.isdCode)  ??  ""
        let  numberStr = strVerificationCodeHasBeenSentTo + " " + isdCode + self.updatedMobileNumber
        selectedCountryCode = CountryCode.getCountryObject(forISDCode: userProfile?.isdCode, in: DBInterface.sharedInstance().sharedContext)
        let fullString = NSMutableAttributedString(string: numberStr)
        
        
        let iconImage = UIImage(named: "editPencil.png")!
        let icon = NSTextAttachment()
        icon.bounds = CGRect(x: 0, y: (lblSubHeader.font.capHeight - iconImage.size.height).rounded() / 2, width: iconImage.size.width, height: iconImage.size.height)
        icon.image = iconImage
        let image1String = NSAttributedString(attachment: icon)
        fullString.append(image1String)
  
        if(self.updatedMobileNumber.count > 0){
            lblSubHeader.attributedText = fullString
            lblSubHeader.isUserInteractionEnabled = true
            let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.taponEditNumber))
            lblSubHeader.addGestureRecognizer(gesture)
        }else{
            lblSubHeader.text = ""
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isCountryCodeSelected == true {
            taponEditNumber()
            isCountryCodeSelected = false
            //self.present(alertCtrlNumber, animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func taponEditNumber() {
        self.isSameNumberToUpdate = false
        var messageStr = NSLocalizedString(SS_DO_YOU_WANT_CHANGE_NUMBER, comment: "")
        if(self.updatedMobileNumber.count == 0){
            messageStr = "Add Mobile Number"
        }
        alertCtrlNumber = UIAlertController.init(title: AppName, message: messageStr, preferredStyle: .alert)

        alertCtrlNumber.addTextField(configurationHandler: { (txtMobileNum) in
            txtMobileNum.placeholder = NSLocalizedString(VC_SIGNUP_MOBILEVAL, comment: "")
            txtMobileNum.delegate = self as UITextFieldDelegate;
            txtMobileNum.keyboardType = .numberPad
            let button = UIButton(type: .custom)
            let userProfile = UserProfile.getCurrentUser()
            button.setTitle(userProfile?.isdCode ?? "", for: .normal)
            txtMobileNum.text = self.updatedMobileNumber
            if self.isCountryCodeSelected == true {
                button.setTitle(self.selectedCountryCode.isdCode!, for: .normal)
            }
            button.titleLabel?.font = UIFont.init(name: NormalFont, size: 14)
            button.frame = CGRect(x: CGFloat(txtMobileNum.frame.size.width - 5), y: CGFloat(0), width: CGFloat(45), height: CGFloat(25))
            button.addTarget(self, action: #selector(self.btnTappedCountryCode), for: .touchUpInside)
            button.setTitleColor(UIColor.black, for: .normal)
            txtMobileNum.leftView = button
            txtMobileNum.leftViewMode = .always
        })
        let okAction = UIAlertAction.init(title: NSLocalizedString(CMON_GENERIC_OK, comment: ""), style: .default) { (alertView) in
            self.view.endEditing(true)
            if(self.isSameNumberToUpdate){
                return
            }
            self.updateMobile_Service()
        }
        let cancelAction = UIAlertAction.init(title: NSLocalizedString(CMON_GENERIC_CANCEL, comment: ""), style: .default) { (alertView) in
            self.isSameNumberToUpdate = false
            let userProfile = UserProfile.getCurrentUser()
            self.updatedMobileNumber = (userProfile?.mobile) != nil ? (userProfile?.mobile)!  :  ""
        }
        if(self.updatedMobileNumber.count > 0){
            alertCtrlNumber.addAction(cancelAction)
        }
        alertCtrlNumber.addAction(okAction)
        self.present(alertCtrlNumber, animated: true, completion: nil)
    }
    
    
    // MARk: - Service Call Get - OTP
    func getUserOTP() {
        
        let request = VerifyMobileRequest(otp: "", andRequestType: VERIFYMOBILEOTP)
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(request, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.viewOTP.becomeFirstResponder()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    //SetProfilePictureController
    func completeUserFlow() {
        OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)

    }
    
    // MARK:- SingleCharPasswordViewDelegate
    func enteredPasswordCode(_ code: String!) {
        self.callAPIForVerifyMobileNumber(code: code)
    }
    
    func displayErrorMessageToSelectValidOTP(){
        if (!(navigationController?.visibleViewController is UIAlertController)) {
            UIAlertController.showError(withMessage: NSLocalizedString(verify_Otp_Screen_copy_error, comment: ""), inViewController: self)
        }
    }
    
    @available(iOS 9.0, *)
    func navigateToProfilePic() {
        let vcSetProfile = self.storyboard?.instantiateViewController(withIdentifier: "SetProfilePictureController") as? SetProfilePictureController
        self.navigationController?.pushViewController(vcSetProfile!, animated: true)
    }
    
    // MARk: - Actions
    @IBAction func btnTappedLogOut(_ sender: Any) {
        self.showAlertFortakingConfirmationForLogout()
    }
    
    func showAlertFortakingConfirmationForLogout() {
        let alert = UniversalAlert(title: "Zify", withMessage: NSLocalizedString(USS_LOGOUT_ALERT_MESAGE, comment: ""))
        alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_OK, comment: ""), withAction: { action in
            self.callServiceFroLogout()
        })
        alert?.addButton(BUTTON_CANCEL, withTitle: NSLocalizedString(CMON_GENERIC_CANCEL, comment: ""), withAction: { action in
            
        })
        alert?.show(in: self)
    }
    
    func callServiceFroLogout() -> Void {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: LOGOUTREQUEST), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        PushNotificationManager.sharedInstance().deregisterDeviceForPushOnLogout()
                        //Should be called before deleting user since userId is required here.
                        UserProfile.deleteCurrentUser()
                        ChatManager.sharedInstance().disconnectChatOnLogout()
                        OnboardFlowHelper.sharedInstance().clearReminder()
                        FacebookLogin.sharedInstance().closeSessionAndClearSavedTokens()
                        UserDataManager.sharedInstance().setDeepLinkFlagsOnLogout()
                        MoEngage.sharedInstance().resetUser()
                        (UIApplication.shared.delegate as? AppDelegate)?.deleteNsUserDefaults()
                        (UIApplication.shared.delegate as? AppDelegate)?.addMoEngageMethodInitializers(with: UIApplication.shared, andWithLaunchOptions: nil)
                        (UIApplication.shared.delegate as? AppDelegate)?.showLoginController()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as? String)
                    }
                })
            })
        })
    }
    
    
    @available(iOS 9.0, *)
    @IBAction func btnTappedVerify(_ sender: Any) {
        
        let code:String = viewOTP.otpCode
        self.callAPIForVerifyMobileNumber(code: code)
    }
    
    func callAPIForVerifyMobileNumber(code:String){
        if code.count == 6 {
            self.view.endEditing(true)
            messageHandler.showBlockingLoadView(handler: {
                let request = VerifyMobileRequest(otp: code, andRequestType: VERIFYMOBILE)
                ServerInterface.sharedInstance().getResponse(request, withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            MoEngageEventsClass.callMobileVerifiedEvent()
                            self.moveToProfilePicture()
                            // self.completeUserFlow()
                        } else {
                            let errorMsg = NSLocalizedString(OTP_WRONG_ALERT_MSG, comment: "")
                            self.messageHandler.showErrorMessage(errorMsg)
                            self.viewOTP.handleErrorCase()
                        }
                    })
                })
            })
        }
    }
    func moveToProfilePicture() {
        //self.navigationController?.pushViewController(SetProfilePictureController.createSetProfilePicNavController()!, animated: true)
        self.navigationController?.present(SetProfilePictureController.createSetProfilePicNavController()!, animated: false, completion: {
        })
    }
    @IBAction func btnTappedResend(_ sender: Any) {
        self.view.endEditing(true)
        getUserOTP()
    }
    
    func btnTappedCountryCode(_ sender : UIButton) {
        return;
        let countryCodeNavCtrl = CountryCodeSuggestionController.createCountryCodeNavController()
        let countryCodeController = countryCodeNavCtrl?.topViewController as? CountryCodeSuggestionController
        countryCodeController?.selectionDelegate = self
        //self.present(countryCodeNavCtrl!, animated: true, completion: nil)        
        if presentedViewController == nil {
            self.present(countryCodeNavCtrl!, animated: true, completion: nil)
        } else{
            self.dismiss(animated: false) { () -> Void in
                self.present(countryCodeNavCtrl!, animated: true, completion: nil)
            }
        }
        
    }
    
    // MARK:- Country Code Delegates
    func selectedCountryCode(_ countryCode: CountryCode!) {
        selectedCountryCode = countryCode
        isCountryCodeSelected = true
        
    }
    
    class func createVerificationCodeNavController() -> UINavigationController? {
        
        let storyBoard = UIStoryboard.init(name: "SignUp", bundle: Bundle.main)
        let verificationCodeNavCtrl = storyBoard.instantiateViewController(withIdentifier: "createVerificationCodeNavCtrl") as? UINavigationController
        return verificationCodeNavCtrl
    }
    
    // MARK:- Service Call
    
    func updateMobile_Service() {
        self.view.endEditing(true)
        if(self.selectedCountryCode?.isdCode.count == 0){
            UIAlertController.showError(withMessage: NSLocalizedString(MS_SIGNUP_BTN_COUNTRYCODE, comment: ""), inViewController: self)
            return
        }
        if(updatedMobileNumber.count == 0){
            let userProfile = UserProfile.getCurrentUser()
            //userProfile?.mobile = nil
            self.updatedMobileNumber = (userProfile?.mobile) != nil ? (userProfile?.mobile)!  :  ""
            //  UIAlertController.showError(withMessage: NSLocalizedString(VC_SIGNUP_MOBILEVAL, comment: ""), inViewController: self)
            UIAlertController.showAlert(in: self, withTitle:AppName, message: NSLocalizedString(VC_SIGNUP_MOBILEVAL, comment: ""), cancelButtonTitle: "Ok", destructiveButtonTitle: nil, otherButtonTitles: nil) { (UIAlertController, UIAlertAction, Int) in
                self.taponEditNumber()
            }
            return
        }
        self.updateMobileNumber()
    }
    
    func updateMobileNumber() {
        messageHandler.showBlockingLoadView(handler: {
            let updateRequest = VehicleDetailUpdateRequest(vehicleModel: "", andVehicleRegistrationNumber: "", andInsuranceNumber: "", andDrivingLicenseNumber: "", andWithVehicleImagePah: "", withUpdate: MOBILE_NUMBER_UPDATE, withMobileNumber: self.updatedMobileNumber, withIsoCode: self.selectedCountryCode?.isdCode, withCountryCode:self.selectedCountryCode?.isoCode)
            ServerInterface.sharedInstance().getResponse(updateRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.setSubHeader()
                        self.getUserOTP()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as? String)
                    }
                })
            })
        })
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let newNumber = textField.text!
        if( newNumber == updatedMobileNumber && updatedMobileNumber.count > 0){
            isSameNumberToUpdate = true
        }
        updatedMobileNumber = textField.text!;
    }
}








