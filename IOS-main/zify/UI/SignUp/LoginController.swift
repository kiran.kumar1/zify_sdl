//
//  LoginController.swift
//  zify
//
//  Created by Anurag on 21/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class LoginController: UIViewController,UITextFieldDelegate,FaceBookLoginDelgate {

    
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var btnShowHidePassword: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLogin: UIButton!

    
    var emailStr = ""
    var blurView:UIView!
    var alertBgView:UIView!
    
    var messageForValidation:String!
    var fbLoginResponse:FBLoginResponse!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.init(red: 67.0/255.0, green: 91.0/255.0, blue: 108.0/255.0, alpha: 0.2)
        let forgotPwdStrLocalized = NSLocalizedString(MS_EMAILLOGIN_LBL_FORGOTPASSWORD, comment: "")
        btnForgotPassword.titleLabel?.attributedText = NSAttributedString(string: forgotPwdStrLocalized, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        self.addLeftAndRightViews(txtField: self.txtEmail)
        self.addLeftAndRightViews(txtField: self.txtPassword)
        if UIScreen.main.sizeType == .iPhone5 {
            lblHeader.fontSize = 20
            btnForgotPassword.titleLabel?.fontSize = 14
            btnLogin.titleLabel?.fontSize = 20
        } else if UIScreen.main.sizeType == .iPhone6 {
            lblHeader.fontSize = 21
            btnForgotPassword.titleLabel?.fontSize = 15
            btnLogin.titleLabel?.fontSize = 21
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            lblHeader.fontSize = 22
            btnForgotPassword.titleLabel?.fontSize = 16
            btnLogin.titleLabel?.fontSize = 22
        }
        
       /* if(AppDelegate.getInstance().emailStrFromFB.count != 0){
          self.txtEmail.text = AppDelegate.getInstance().emailStrFromFB
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationController?.navigationItem.backBarButtonItem?.title = " "
        self.navigationController?.isNavigationBarHidden = false;

    }
    func addLeftAndRightViews(txtField:UITextField) {
        let customLeftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
        txtField.leftView = customLeftView;
        txtField.leftViewMode = .always
        if(txtField == self.txtPassword){
            txtField.rightView = self.btnShowHidePassword;
            txtField.rightViewMode = .always
        }
    }
    
    // MARK:- Actions
    
    @IBAction func btnTappedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTappedLogin(_ sender: Any) {
        messageForValidation = ""
        if validateUserInfo() {
            messageHandler.showBlockingLoadView(handler: {
                ServerInterface.sharedInstance().getResponse(LoginRequest(email: self.txtEmail.text, andPassword: self.txtPassword.text), withHandler: { response, error  in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        print("messageCode  is \(String(describing: response?.messageCode))")
                        if response != nil {
                            PushNotificationManager.sharedInstance().handlePushRegisterWithUser()
                            MoEngageEventsClass.callLoginNormalEvent()
                            self.completeUserLogin()
                        } else {
                            guard let locerror = error as NSError? else {return}
                            let errorCode:Int = locerror.code
                            print("error is \(locerror.localizedDescription ) code = \(locerror.code)");
                            if (errorCode == -5 || errorCode == -51) {
                                self.showAlertViewToFBLogin()
                            } else {
                                self.self.messageForValidation = (locerror).userInfo["message"] as? String
                                UIAlertController.showError(withMessage: self.messageForValidation, inViewController: self)
                            }
                        }
                    })
                })
            })
        } else {
            
            UIAlertController.showError(withMessage: messageForValidation, inViewController: self)
        }
        
    }
    
    func moveToProfilePicture() {
        let profileVc = self.storyboard?.instantiateViewController(withIdentifier: "SetProfilePictureController") as? SetProfilePictureController
        //vcEmail?.strUserName = txtFirstName.text!.capitalized + " " + txtLastName.text!
        self.navigationController?.pushViewController(profileVc!, animated: true)
    }
    
    @IBAction func btnTappedForgotPassword(_ sender: Any) {
        if((txtEmail.text?.count)! > 0){
            AppDelegate.getInstance().emailStr = txtEmail.text
        }
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vcForgotPassword = storyBoard.instantiateViewController(withIdentifier: "navigation") as? UINavigationController
        self.present(vcForgotPassword!, animated: true, completion: nil)
    }
    
    @IBAction func btnTappedShowHidePassword(_ sender: UIButton) {
        
        if btnShowHidePassword.isSelected {
            txtPassword.isSecureTextEntry = false
            btnShowHidePassword.isSelected = false
            btnShowHidePassword.setImage(UIImage.init(named: "showEye"), for: .normal)
        } else {
            btnShowHidePassword.isSelected = true
            txtPassword.isSecureTextEntry = true
            btnShowHidePassword.setImage(UIImage.init(named: "hideEye"), for: .normal)
        }
    }
    
   
    
    // MARK: - Textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
       /* if(!isValidEmail(emailStr: txtEmail.text!)){
            txtEmail.showErrorWithText(errorText: NSLocalizedString(VC_FORGOTPASSWORD_EMAILVALIDVAL, comment: ""))
            return false
        }*/
        
        if textField == txtEmail {
            (self.txtPassword as UITextField).becomeFirstResponder()
        }
        return true
    }
    
    func completeUserLogin() {
        OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)
    }
    
    func validateUserInfo() -> Bool {
        self.view.endEditing(true)
        
        if ("" == txtEmail.text) {
            messageForValidation = NSLocalizedString(VC_EMAILLOGIN_EMAILVAL, comment: "") //NSLocalizedString(VC_EMAILLOGIN_EMAILVAL, comment: "")
            return false
        }
        if(!isValidEmail(emailStr: txtEmail.text!)){
            //txtEmail.showErrorWithText(errorText: "Invalid Email")
            messageForValidation = NSLocalizedString(VC_EMAILLOGIN_EMAILVAL, comment: "") //NSLocalizedString(VC_EMAILLOGIN_EMAILVAL, comment: "")
            return false
        }
        
        if !isValidEmail(emailStr: txtEmail.text!)
        {
            messageForValidation = NSLocalizedString(VC_EMAILLOGIN_EMAILVALIDVAL, comment: "")
            return false
        }
        if ("" == txtPassword.text) {
            messageForValidation = NSLocalizedString(VC_EMAILLOGIN_PASSWORDVAL, comment: "")
            return false
        }
        return true
    }
    
    class func createLoginNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "SignUp", bundle: Bundle.main)
        let signUpNavController = storyBoard.instantiateViewController(withIdentifier: "LoginNavigationController")
        return signUpNavController as? UINavigationController
    }
    
   
    func showAlertViewToFBLogin() {
        blurView = UIView()
        blurView.frame = CGRect(x: 0, y: 0, width: AppDelegate.screen_WIDTH(), height: AppDelegate.screen_HEIGHT())
        blurView.backgroundColor = UIColor.black
        blurView.alpha = 0.8
        self.navigationController?.view.addSubview(blurView)
        let posX: CGFloat = 20
        alertBgView = UIView()
        alertBgView.frame = CGRect(x: posX, y: AppDelegate.screen_HEIGHT(), width: AppDelegate.screen_WIDTH() - 2 * posX, height: 100)
        alertBgView.backgroundColor = UIColor.white
        alertBgView.layer.cornerRadius = 5.0
        self.navigationController?.view.addSubview(alertBgView)
        let alertView: UIView? = UIView()
        alertView?.frame = CGRect(x: 0, y: 0, width: alertBgView.frameWidth, height: 100)
        alertView?.backgroundColor = UIColor.clear
        if let aView = alertView {
            alertBgView.addSubview(aView)
        }
        let alertLbl = UILabel()
        alertLbl.textAlignment = .center
        alertLbl.frame = CGRect(x: 5, y: 10, width: (alertView?.frameWidth)! - 10, height: 45)
        alertLbl.numberOfLines = 2
        alertLbl.text = NSLocalizedString(MS_EMAILLOGIN_LOGIN_WITH_FB_ERROR, comment: "")
        //
        if let aSize = UIFont(name: NormalFont, size: 16) {
            alertLbl.font = aSize
        }
        alertLbl.backgroundColor = UIColor.clear
        alertLbl.textColor = UIColor.gray
        alertView?.addSubview(alertLbl)
        let mailLbl = UILabel()
        mailLbl.frame = CGRect(x: 5, y: alertLbl.frameMaxY + 10, width: (alertView?.frameWidth)!  - 10, height: 30)
        mailLbl.textAlignment = .center
        if let aSize = UIFont(name: MediumFont, size: 15) {
            mailLbl.font = aSize
        }
        mailLbl.text = self.txtEmail.text
        mailLbl.numberOfLines = 2
        alertView?.addSubview(mailLbl)
        let lineLbl1 = UILabel()
        lineLbl1.frame = CGRect(x: 0, y: mailLbl.frameMaxY + 10, width: (alertView?.frameWidth)!, height: 1)
        lineLbl1.backgroundColor = UIColor.lightGray
        alertView?.addSubview(lineLbl1)
        let btnFb = UIButton(type: .custom)
        btnFb.frame = CGRect(x: 5, y: lineLbl1.frameMaxY + 10, width: (alertView?.frameWidth)! - 10, height: 50)
        let title = NSLocalizedString(MS_EMAILLOGIN_LBL_LOGINWITHFB, comment: "")
        // [btnFb setImage:[UIImage imageNamed:@"fbIconBg.png"] forState:UIControlStateNormal];
        // [btnFb setImage:[UIImage imageNamed:@"fbIconBg.png"] forState:UIControlStateSelected];
        btnFb.setTitle(title, for: .normal)
        btnFb.setTitle(title, for: .selected)
        if let aSize = UIFont(name: NormalFont, size: 15) {
            btnFb.titleLabel?.font = aSize
        }
        btnFb.layer.cornerRadius = 5.0
        btnFb.addTarget(self, action: #selector(self.btnFBTappped(_:)), for: .touchUpInside)
        btnFb.backgroundColor = UIColor(red: 59.0 / 255.0, green: 85.0 / 255.0, blue: 151.0 / 255.0, alpha: 1.0)
        alertView?.addSubview(btnFb)
        let lineLbl2 = UILabel()
        lineLbl2.frame = CGRect(x: 0, y: btnFb.frameMaxY + 10, width: (alertView?.frameWidth)!, height: 1)
        lineLbl2.backgroundColor = UIColor.lightGray
        alertView?.addSubview(lineLbl2)
        alertView?.frameHeight = lineLbl2.frameMaxY + 2
        let cancelImage1 = AppDelegate.getInstance().getNewImage(withOldImage: UIImage(named: "cancel_cross.png"), withNewColor: UIColor.lightGray)
        let cancelImage = UIImageView()
        cancelImage.frame = CGRect(x: ((alertView?.frameWidth)!  - 25) / 2, y: (alertView?.frameMaxY)!  + 10, width: 25, height: 25)
        cancelImage.image = cancelImage1
        alertBgView.addSubview(cancelImage)
        let btnCancel = UIButton(type: .custom)
        btnCancel.frame = CGRect(x: ((alertView?.frameWidth)! - 40) / 2, y: (alertView?.frameMaxY)!, width: 40, height: 40)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.addTarget(self, action: #selector(self.btnCancelTappped(_:)), for: .touchUpInside)
        alertBgView.addSubview(btnCancel)
        alertBgView.frameHeight = btnCancel.frameMaxY + 10
        
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseIn, animations: {
            self.alertBgView.frameY = (self.view.frameHeight - self.alertBgView.frameHeight) / 2
        }) { finished in
        }
    }
    
    func btnFBTappped(_ sender: Any?) {
        btnCancelTappped(nil)
        FacebookLogin.sharedInstance().closeSessionAndClearSavedTokens()
        let sharedInstance = FacebookLogin.sharedInstance()
        sharedInstance?.faceBookLoginDelgate = self
        sharedInstance?.loginWithFacebook()
    }
    
    func btnCancelTappped(_ sender: Any?) {
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseIn, animations: {
            self.alertBgView.frameY = AppDelegate.screen_HEIGHT()
        }) { finished in
            if finished {
                self.alertBgView.removeFromSuperview()
            }
            self.blurView.removeFromSuperview()
        }
    }
    
    func loggedIn(withFaceBook loginResponse: FBLoginResponse?) {
        if loginResponse?.email == nil || loginResponse?.email.count == 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_LOGIN_FB_NO_EMAIL, comment: ""))
            // [UIAlertController showErrorWithMessage:@"Sorry! No Email Id is associated with ths account" inViewController:self];
            return
        }
        let loginRequest = FBUserLoginRequest(firstName: loginResponse?.firstName, andLastName: loginResponse?.lastName, andUserEmail: loginResponse?.email, andisdCode: "00", andMobile: "0000000000", andGender: loginResponse?.gender, andFbProfileId: loginResponse?.facebookId)
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(loginRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        // NSLog(@"response is %@", response);
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            MoEngageEventsClass.callLoginNormalEvent()
                            self.completeUserLogin()
                            PushNotificationManager.sharedInstance().handlePushRegisterWithUser()
                        } else {
                            UserProfile.deleteCurrentUser()
                            //Deleting which is stored after getting the response in case user is not registered.
                            self.fbLoginResponse = loginResponse!
                            self.performSegue(withIdentifier: "registerUserSegue", sender: self)
                        }
                    } else {
                        if let anError = error {
                            print("error is \(anError)")
                        }
                        guard let locerror = error as NSError? else {return}
                        let errorCode:Int = locerror.code
                        if errorCode == -6 {
                            self.showAlertView(toEmailLogin: loginResponse?.email)
                        } else {
                            self.messageHandler.showErrorMessage(locerror.userInfo["message"] as! String)
                        }
                    }
                })
            })
        })
    }
    
    func showAlertView(toEmailLogin email: String?) {
        emailStr = email ?? ""
        blurView = UIView()
        blurView.frame = CGRect(x: 0, y: 0, width: AppDelegate.screen_WIDTH(), height: AppDelegate.screen_HEIGHT())
        blurView.backgroundColor = UIColor.black
        blurView.alpha = 0.8
        self.navigationController?.view.addSubview(blurView)
        let posX: CGFloat = 20
        alertBgView = UIAlertView()
        alertBgView.frame = CGRect(x: posX, y: AppDelegate.screen_HEIGHT(), width: AppDelegate.screen_WIDTH() - 2 * posX, height: 100)
        alertBgView.backgroundColor = UIColor.white
        alertBgView.layer.cornerRadius = 5.0
        self.navigationController?.view.addSubview(alertBgView)
        let alertView: UIView? = UIAlertView()
        alertView?.frame = CGRect(x: 0, y: 0, width: alertBgView.frameWidth, height: 100)
        alertView?.backgroundColor = UIColor.clear
        if let aView = alertView {
            alertBgView.addSubview(aView)
        }
        let alertLbl = UILabel()
        alertLbl.textAlignment = .center
        alertLbl.frame = CGRect(x: 5, y: 10, width: (alertView?.frameWidth)!  - 10, height: 45)
        alertLbl.numberOfLines = 2
        alertLbl.text = NSLocalizedString(MS_EMAILLOGIN_LOGIN_WITH_EMAIL_ERROR, comment: "")
        //
        if let aSize = UIFont(name: NormalFont, size: 16) {
            alertLbl.font = aSize
        }
        alertLbl.backgroundColor = UIColor.clear
        alertLbl.textColor = UIColor.gray
        alertView?.addSubview(alertLbl)
        let mailLbl = UILabel()
        mailLbl.frame = CGRect(x: 5, y: alertLbl.frameMaxY + 10, width: (alertView?.frameWidth)!  - 10, height: 30)
        mailLbl.textAlignment = .center
        if let aSize = UIFont(name: MediumFont, size: 15) {
            mailLbl.font = aSize
        }
        mailLbl.text = email
        mailLbl.numberOfLines = 2
        alertView?.addSubview(mailLbl)
        let lineLbl1 = UILabel()
        lineLbl1.frame = CGRect(x: 0, y: mailLbl.frameMaxY + 10, width: (alertView?.frameWidth)!, height: 1)
        lineLbl1.backgroundColor = UIColor.lightGray
        alertView?.addSubview(lineLbl1)
        let btnEmilLogin = UIButton(type: .custom)
        btnEmilLogin.frame = CGRect(x: 5, y: lineLbl1.frameMaxY + 10, width: (alertView?.frameWidth)! - 10, height: 50)
        let title = NSLocalizedString(MS_EMAIL_LOGIN_FROM_ERROR, comment: "")
        btnEmilLogin.setTitle(title, for: .normal)
        btnEmilLogin.setTitle(title, for: .selected)
        if let aSize = UIFont(name: NormalFont, size: 15) {
            btnEmilLogin.titleLabel?.font = aSize
        }
        btnEmilLogin.layer.cornerRadius = 5.0
        btnEmilLogin.backgroundColor = UIColor(red: 231.0 / 255.0, green: 76.0 / 255.0, blue: 58.0 / 255.0, alpha: 1.0)
        btnEmilLogin.addTarget(self, action: #selector(self.btnEmilLoginTapped(_:)), for: .touchUpInside)
        alertView?.addSubview(btnEmilLogin)
        let lineLbl2 = UILabel()
        lineLbl2.frame = CGRect(x: 0, y: btnEmilLogin.frameMaxY + 10, width: alertView?.frameWidth ?? 0.0, height: 1)
        lineLbl2.backgroundColor = UIColor.lightGray
        alertView?.addSubview(lineLbl2)
        alertView?.frameHeight = lineLbl2.frameMaxY + 2
        let cancelImage1 = AppDelegate.getInstance().getNewImage(withOldImage: UIImage(named: "cancel_cross.png"), withNewColor: UIColor.lightGray)
        let cancelImage = UIImageView()
        cancelImage.frame = CGRect(x: ((alertView?.frameWidth)!  - 25) / 2, y: (alertView?.frameMaxY)! + 10, width: 25, height: 25)
        cancelImage.image = cancelImage1
        alertBgView.addSubview(cancelImage)
        let btnCancel = UIButton(type: .custom)
        btnCancel.frame = CGRect(x: ((alertView?.frameWidth)!  - 40) / 2, y: alertView?.frameMaxY ?? 0.0, width: 40, height: 40)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.addTarget(self, action: #selector(self.btnCancelTappped(_:)), for: .touchUpInside)
        alertBgView.addSubview(btnCancel)
        alertBgView.frameHeight = btnCancel.frameMaxY + 10
        
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseIn, animations: {
            self.alertBgView.frameY = (self.view.frameHeight - self.alertBgView.frameHeight) / 2
        }) { finished in
        }
    }
    func btnEmilLoginTapped(_ sender: Any?) {
        AppDelegate.getInstance().emailStrFromFB = emailStr
        btnCancelTappped(nil)
        self.txtEmail.text = emailStr
        self.txtPassword.text = "";
        (self.txtPassword as UITextField).becomeFirstResponder()
      /*  let stb = UIStoryboard(name: "SignUp", bundle: Bundle.main)
        let loginViewCtrl = stb.instantiateViewController(withIdentifier: "LoginController") as? LoginController
        if let aCtrl = loginViewCtrl {
            present(aCtrl, animated: true)
        }*/
    }
    
    func unableToLoginFBWithError(_ error: Error!) {
       // if let anError = error {
          //  print("error is \(anError)")
            UIAlertController.showError(withMessage: NSLocalizedString(VC_LOGIN_FBAUTHERROR, comment: ""), inViewController: self)
       //}
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
