//
//  SignUpEmailController.swift
//  SignUp
//
//  Created by Anurag on 05/06/18.
//  Copyright © 2018 Anurag. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import TrueSDK



class SignUpEmailController: UIViewController, UITextFieldDelegate, CountryCodeSelectionDelegate {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnTermsAndCond: UIButton!
    @IBOutlet weak var lblThisCreatesYourAcc: UILabel!
    @IBOutlet weak var lblBySigningUpIAccept: UILabel!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var btnWhyMyMobNo: UIButton!
    @IBOutlet weak var btnShowHidePassword: UIButton!
    @IBOutlet weak var btnCountryCode: UIButton!
    
    @IBOutlet weak var btnPromoApplied: UIButton!
    @IBOutlet weak var btnApplyCodeOnView: UIButton!
    
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var btnCloseViewPromo: UIButton!
    @IBOutlet var viewPromoCode: UIView!
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var messageHandler: MessageHandler!
    
    var visualEffectView: UIVisualEffectView?
    
    var loginMode : String! = ""
    var selectedCountryCode: CountryCode!
    var isfromFacebook:Bool!
    var trueCallerProfile:TCTrueProfile!
    let strLocApplyPromoCode = NSLocalizedString(SS_APPLY_PROMO_CODE, comment: "")
    let strLocApplied = NSLocalizedString(SS_APPLIED, comment: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.doRunTimeDesign();
        addDoneButtonOnKeyboard()
        (txtEmail as UITextField).becomeFirstResponder()
        loginMode = dictSignUpObj!["loginMode"] as! String
        if(isfromFacebook){
            (txtPassword as UITextField).removeFromSuperview()
            btnShowHidePassword.isHidden = true
            let emailStr = dictSignUpObj!["userEmail"] as! String
            txtEmail.text = emailStr;
            txtEmail.isUserInteractionEnabled = false
        }
        if UIScreen.main.sizeType == .iPhone5 {
            lblBySigningUpIAccept.fontSize = 14
            btnTermsAndCond.titleLabel?.fontSize = 14
            btnSignUp.titleLabel?.fontSize = 18
            lblThisCreatesYourAcc.fontSize = 12
            lblHeader.fontSize = 18
            btnWhyMyMobNo.titleLabel?.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6{
            lblBySigningUpIAccept.fontSize = 15
            btnTermsAndCond.titleLabel?.fontSize = 15
            btnSignUp.titleLabel?.fontSize = 19
            lblThisCreatesYourAcc.fontSize = 13
            lblHeader.fontSize = 19
            btnWhyMyMobNo.titleLabel?.fontSize = 15
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            lblBySigningUpIAccept.fontSize = 16
            btnTermsAndCond.titleLabel?.fontSize = 16
            btnSignUp.titleLabel?.fontSize = 20
            lblThisCreatesYourAcc.fontSize = 14
            lblHeader.fontSize = 20
            btnWhyMyMobNo.titleLabel?.fontSize = 16
        }
        
        
        let txtBinding = NSLocalizedString(SS_HELP_US_TO_CREATE_AN_ACCOUNT_FOR_YOU, comment: "")
        
        var namesCharacters = (dictSignUpObj!["fname"] as! String)
        if namesCharacters.count > 25 {
            namesCharacters = String(namesCharacters.dropLast(namesCharacters.count - 25))
        }
        
        lblHeader.text = namesCharacters + txtBinding
        btnTermsAndCond.addTarget(self, action: #selector(self.btnTappedTermsAndCond(sender:)), for: .touchUpInside)
        
        let termsAndCondStringLocalized = NSLocalizedString(SS_TERMS_AND_CONDITIONS_BTN, comment: "")
        lblBySigningUpIAccept.text = NSLocalizedString(SS_BY_SIGNING_UP_I_ACCEPT_LBL , comment: "")//"Vehicle details cannot be edited, contact"
        btnTermsAndCond.titleLabel?.attributedText = NSAttributedString(string: termsAndCondStringLocalized, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        let whyMyMobStringLocalized = NSLocalizedString(SS_WHY_MY_MOBILE_NUMBER_BTN, comment: "")
        btnWhyMyMobNo.titleLabel?.attributedText = NSAttributedString(string: whyMyMobStringLocalized, attributes:[NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        
        selectedCountryCode = CountryCode.getCountryObject(forISOCode: CurrentLocale.sharedInstance().getISOCode(), in: DBInterface.sharedInstance().sharedContext)
        
        
        if (selectedCountryCode != nil) {
            txtCountryCode.text = selectedCountryCode?.isdCode
            dictSignUpObj?.updateValue(selectedCountryCode!.isoCode, forKey: "countrycode")
            dictSignUpObj?.updateValue(selectedCountryCode!.isdCode, forKey: "isdCode")
            let countryStr = selectedCountryCode!.isoCode
            let arrayOFObje = AppDelegate.getInstance().servicesCountriesArray as? Array<String>  ?? []
            if(arrayOFObje .count > 0){
                if(arrayOFObje.contains(countryStr ?? "")){
                    print("heheh")
                }else{
                    print("not available")
                    self.btnCountryCodeTapped(self.btnCountryCode)
                }
            }

            
         //   !AppDelegate.getInstance().servicesCountriesArray.contains(where: { (String) -> Bool in}
          //      return false
          //  )
            
        
            
        
            
            
        }
        dictSignUpObj?.updateValue("", forKey: "promoCode")
        self.prepapulateUserDetailsFromTrueCaller()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func doRunTimeDesign() {
        self.addLeftView(txtField: self.txtEmail)
        self.addLeftView(txtField: self.txtPassword)
        self.addLeftView(txtField: self.txtMobileNumber)
        self.addLeftView(txtField: self.txtPromoCode)
        self.txtPassword.rightView = self.btnShowHidePassword;
        self.txtPassword.rightViewMode = .always
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func btnTappedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // To Close Promo Code view
    @IBAction func btnTappedCloseViewPromo(_ sender: Any) {
        self.view.endEditing(true)
        SPAnimation.animate(0.3, animations: {
            self.viewPromoCode.frame = CGRect(x: 0, y: AppDelegate.screen_HEIGHT(), width: AppDelegate.screen_WIDTH(), height: self.viewPromoCode.frame.size.height);
        }, delay: 0, options: UIViewAnimationOptions.curveEaseOut) {
            self.visualEffectView?.removeFromSuperview()
        }
    }
    
    // To open Promo Code view
    @IBAction func btnTappedApplyPromoCode(_ sender: UIButton) {
        self.view.endEditing(true)
        if (btnPromoApplied.titleLabel?.text?.contains(strLocApplyPromoCode))! {
            self.txtPromoCode.text = ""
            var blurEffect: UIVisualEffect?
            blurEffect = UIBlurEffect(style: .dark)
            visualEffectView = UIVisualEffectView(effect: blurEffect)
            visualEffectView?.alpha = 0.7
            visualEffectView?.frame = AppDelegate.getInstance().window.bounds
            navigationController?.view.addSubview(visualEffectView!)
            
            self.viewPromoCode.frame = CGRect(x: 0, y: AppDelegate.screen_HEIGHT() - self.viewPromoCode.frame.size.height, width: AppDelegate.screen_WIDTH(), height: self.viewPromoCode.frame.size.height);
            
            self.navigationController?.view.addSubview(self.viewPromoCode)
            SPAnimationUpward.show(0.3, view: viewPromoCode)
        } else {
            dictSignUpObj?.updateValue("", forKey: "promoCode")
            self.txtPromoCode.text = ""
            self.btnPromoApplied.setImage(UIImage(named:""), for: .normal)
            self.btnPromoApplied.setTitle(strLocApplyPromoCode, for: .normal)
        }
        
    }
    
    @IBAction func btnTappedApplyCodeOnView(_ sender: Any) {
        self.view.endEditing(true)
        if txtPromoCode.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count == 0 {
            UIAlertController.showError(withMessage: NSLocalizedString(SS_PROMO_CODE_ERROR, comment: ""), inViewController: self)
            return;
            btnPromoApplied.setImage(UIImage(named:""), for: .normal)
            btnPromoApplied.setTitle(strLocApplyPromoCode, for: .normal)
        } else {
            let strPromo = " " + txtPromoCode.text! +  strLocApplied
            btnPromoApplied.setImage(UIImage(named:"promo_cancel"), for: .normal)
            btnPromoApplied.setTitle(strPromo, for: .normal)
            btnPromoApplied.titleEdgeInsets = UIEdgeInsetsMake(4, 0, 4, 0)
            btnPromoApplied.imageEdgeInsets = UIEdgeInsetsMake(4, -4, 4, 4)
        }
        
        debugPrint("--promo  code applied -- \(txtPromoCode.text!)")
        
        dictSignUpObj?.updateValue(txtPromoCode.text!, forKey: "promoCode")
        
        
        SPAnimation.animate(0.3, animations: {
            self.viewPromoCode.frame = CGRect(x: 0, y: AppDelegate.screen_HEIGHT(), width: AppDelegate.screen_WIDTH(), height: self.viewPromoCode.frame.size.height);
        }, delay: 0, options: UIViewAnimationOptions.curveEaseOut) {
            self.visualEffectView?.removeFromSuperview()
        }
    }
    
    func btnTappedTermsAndCond(sender : UIButton) {
        let genericWebNavController: UINavigationController? =  GenericWebController.createGenericWebNavController()
        let webController = genericWebNavController?.topViewController as? GenericWebController
        webController?.webViewURL = NSLocalizedString(CMON_GENERIC_TERMSURL, comment: "")
        webController?.title = NSLocalizedString(CMON_GENERIC_TNC, comment: "")
        if let aController = genericWebNavController {
            present(aController, animated: true)
        }
    }
    
    @available(iOS 9.0, *)
    @IBAction func btnTappedSignUp(_ sender: Any) {
        
        self.view.endEditing(true)
        guard txtEmail.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count != 0 else {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_EMAILVAL, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        
        if(!isValidEmail(emailStr: txtEmail.text!)){
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_EMAILVALIDVAL, comment: ""), navigationCtrl: self.navigationController!)
            //txtEmail.showErrorWithText(errorText: NSLocalizedString(VC_SIGNUP_EMAILVALIDVAL, comment: ""))
            return
        }
        if(!isfromFacebook){
            guard txtPassword.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count != 0 else {
                showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_PASSWORDVAL, comment: ""), navigationCtrl: self.navigationController!)
                return
            }
            
            if((txtPassword.text?.count)! < 5 || (txtPassword.text?.count)! > 25){
                showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_PASSWARD_MIN_LENGHT_VAL, comment: ""), navigationCtrl: self.navigationController!)
                return
            }
        }
        if(selectedCountryCode == nil){
            showAlertView(strMessage: NSLocalizedString(country_code_mismatch_error, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        guard txtMobileNumber.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count != 0 else {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_MOBILEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        if(Int(txtMobileNumber.text!) == 0){
            showAlertView(strMessage: NSLocalizedString(LR_PLEASE_ENTER_VALID_MOB_NUMBER, comment: ""), navigationCtrl: self.navigationController!)
            return

        }
        
        print("phone num is \(Int(txtMobileNumber.text!))")
        dictSignUpObj?.updateValue(txtEmail.text!, forKey: "userEmail")
        dictSignUpObj?.updateValue(txtMobileNumber.text!, forKey: "mobile")
        dictSignUpObj?.updateValue(selectedCountryCode!.isoCode, forKey: "countrycode")
        dictSignUpObj?.updateValue(selectedCountryCode!.isdCode, forKey: "isdCode")
        if(!isfromFacebook){
            dictSignUpObj?.updateValue(txtPassword.text!, forKey: "userPswd")
        }
        serviceCall_SignUp()
    }
    
    @IBAction func btnTappedWhyMyMobNumber(_ sender: Any) {
        
        
    }
    
    @IBAction func btnTappedSHowHideEye(_ sender: UIButton) {
        if btnShowHidePassword.isSelected {
            txtPassword.isSecureTextEntry = false
            btnShowHidePassword.isSelected = false
            btnShowHidePassword.setImage(UIImage.init(named: "showEye"), for: .normal)
        } else {
            btnShowHidePassword.isSelected = true
            txtPassword.isSecureTextEntry = true
            btnShowHidePassword.setImage(UIImage.init(named: "hideEye"), for: .normal)
        }
        
    }
    
    @IBAction func btnCountryCodeTapped(_ sender:Any){
        self.view.endEditing(true)
        let countryCodeNavCtrl = CountryCodeSuggestionController.createCountryCodeNavController()
        let countryCodeController:CountryCodeSuggestionController = (countryCodeNavCtrl?.topViewController as? CountryCodeSuggestionController)!
        countryCodeController.selectionDelegate = self
        //countryCodeController.selectedUserRegion = sele
        self.present(countryCodeNavCtrl!, animated: true, completion: nil)
        
    }
    
  
    
    // MARK: - Textfield Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == txtEmail) {
            if isValidEmail(emailStr: txtEmail.text!) {
                textField.resignFirstResponder()
                if textField == txtEmail {
                    (self.txtPassword as UITextField).becomeFirstResponder()
                }
                return true
            } else {
                showAlertView(strMessage: NSLocalizedString(VC_FORGOTPASSWORD_EMAILVALIDVAL, comment: ""), navigationCtrl: self.navigationController!)
                return false
            }
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMobileNumber {
           /* guard let text = textField.text else {
                return true
            }*/
            if(range.location==0)
            {
                if (string.hasPrefix("0") ) //[string hasPrefix:@"1"])
                {
                    return false;
                }
            }
            if(string == ""){
                return true
            }
            
            let lengthOfChars = textField.text?.count  ?? 0 + string.count - range.length
            return lengthOfChars <= 14
        }
        return true
    }
    
    // MARK:- Country Code Delegates
    
    func selectedCountryCode(_ countryCode: CountryCode!) {
        selectedCountryCode = countryCode
        txtCountryCode.text = "\(String(describing: selectedCountryCode.isdCode!))"
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: AppDelegate.getInstance().window.bounds.width, height: 44))
        //UIToolbar(frame: CGRectMake(0, 0, AppDelegate.getInstance().window.bounds.width, 50))
        doneToolbar.barStyle = UIBarStyle.black
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString(CMON_GENERIC_DONE, comment: ""), style: UIBarButtonItemStyle.done, target: self, action: #selector(SignUpEmailController.doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtMobileNumber.inputAccessoryView = doneToolbar
        
    }
    
    func addLeftView(txtField:UITextField) {
        let customLeftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
        txtField.leftView = customLeftView;
        txtField.leftViewMode = .always
    }
    
    func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    
    // MARK:- Service Call
    func serviceCall_SignUp() {
        var errorMessage = ""
        if validateUserInfo() {
            messageHandler.showBlockingLoadView(handler: {
                let registerReq = self.getRegisterRequest()
                ServerInterface.sharedInstance().getResponse(registerReq, withHandler: { (response, error) in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            let messageCode:Int = Int((response?.messageCode)!)!
                            if messageCode == 1 {
                                ///Referandearn service call for save Referral data
                                ReferAndEarnController.serviceCall_SaveReferralData()
                                self.setBoolForAKeyForSignUpFlow()
                                MoEngageEventsClass.callSignUPCompleteEvent()
                                self.completeUserLogin()
                                self.trueCallerProfile = nil;
                                PushNotificationManager.sharedInstance().handlePushRegisterWithUser()
                                
                                
                            }
                        }else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as? String)
                            errorMessage = (error as NSError?)?.userInfo["message"] as! String
                        }
                    })
                })
                
            })
        } else {
            //UIAlertController.showError(withMessage: errorMessage.text, inViewController: self)
            showAlertView(strMessage: errorMessage, navigationCtrl: self.navigationController!)
        }
        
    }
    
    
    
    func setBoolForAKeyForSignUpFlow() {
        let prefs = UserDefaults.standard
        prefs.set(true, forKey: "isFromSignUp")
        prefs.synchronize()
    }
    
    func completeUserLogin() {
        dismiss(animated: false)
        //print("delegate is \(registerDelegate)")
        //registerDelegate.showUserHome()
        
        //        let vcVerifyCodeCtrl = self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeController") as? VerificationCodeController
        //        self.navigationController?.pushViewController(vcVerifyCodeCtrl!, animated: true)
        
        OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)
    }
    
    func validateUserInfo() -> Bool {
        
        if (dictSignUpObj!["userEmail"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_EMAILVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if !isValidEmail(emailStr: (dictSignUpObj!["userEmail"] as! String)) {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_EMAILVALIDVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        if !(loginMode == "fb") {
            if (dictSignUpObj!["userPswd"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
                showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_PASSWORDVAL, comment: ""), navigationCtrl: self.navigationController!)
                return false
            }
            if ((dictSignUpObj!["userPswd"] as! String).count) < 5 || ((dictSignUpObj!["userPswd"] as! String).count) > 25 {
                showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_PASSWARD_MIN_LENGHT_VAL, comment: ""), navigationCtrl: self.navigationController!)
                return false
            }
        }
        if (dictSignUpObj!["mobile"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_MOBILEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return false
        }
        
   /*     let arrayOfCountries = NSArray.init(object: "")
        if( !arrayOfCountries.contains(selectedCountryCode.isoCode)){
            showAlertView(strMessage:"Sorry!. We are not providing serviceshere", navigationCtrl: self.navigationController!)

            return false
        }
        */
        
        
        
        
        
        
        return true
    }
    
    func dismiss()  {
        self.view.endEditing(true)
    }
    
    func getRegisterRequest() -> UserRegisterRequest {
        let fname = dictSignUpObj!["fname"] as AnyObject
        let lanme = dictSignUpObj!["lname"] as AnyObject
        let userEmail = dictSignUpObj!["userEmail"] as AnyObject
        let userPswd = dictSignUpObj!["userPswd"] as AnyObject
        let isdCode = dictSignUpObj!["isdCode"] as AnyObject
        let mobile = dictSignUpObj!["mobile"] as AnyObject
        let gender = dictSignUpObj!["gender"] as AnyObject
        let countrycode = dictSignUpObj!["countrycode"] as AnyObject
        let loginMode = dictSignUpObj!["loginMode"] as AnyObject
        let promoCode = dictSignUpObj!["promoCode"] as AnyObject //promoCode
        var fbProfileId = ""
        if(isfromFacebook){
            fbProfileId = (dictSignUpObj!["fbProfileId"] as AnyObject) as! String
        }
        let userMobilenUmber = "\(isdCode)"+"\(mobile)"
        var isTrueCallerMobile = "0"
        if(self.trueCallerProfile != nil){
            if(userMobilenUmber == self.trueCallerProfile.phoneNumber){
                isTrueCallerMobile = "1"
            }
        }
        let registerRequest = UserRegisterRequest(firstName: fname as! String, andLastName: lanme as! String, andUserEmail: userEmail as! String, andUserPswd: userPswd as! String, andisdCode:isdCode as! String, andMobile: mobile as! String, andGender: gender as! String, andLoginMode: loginMode as! String, andPromoCode: promoCode as! String, andCountryCode:countrycode as! String, andFbProfileId:fbProfileId, andisTrueCaller:isTrueCallerMobile)
        return registerRequest!
    }
    
    
    func prepapulateUserDetailsFromTrueCaller(){
        if(self.trueCallerProfile == nil){
            return;
        }
        self.txtEmail.text = self.trueCallerProfile?.email ?? nil;
        let countryCodeStr = self.trueCallerProfile.countryCode?.uppercased() ?? ""
        if( countryCodeStr.count > 0 ){
          selectedCountryCode = CountryCode.getCountryObject(forISOCode: countryCodeStr, in: DBInterface.sharedInstance().sharedContext)
        } else{
            selectedCountryCode = CountryCode.getCountryObject(forISOCode: CurrentLocale.sharedInstance().getISOCode(), in: DBInterface.sharedInstance().sharedContext)
        }
        if (selectedCountryCode != nil) {
            txtCountryCode.text = (selectedCountryCode?.isdCode)!
            dictSignUpObj?.updateValue(selectedCountryCode!.isoCode, forKey: "countrycode")
            dictSignUpObj?.updateValue(selectedCountryCode!.isdCode, forKey: "isdCode")
        }
        let mobileNumberStr = self.trueCallerProfile.phoneNumber!
        let originalMobileNumer = mobileNumberStr.replacingOccurrences(of: txtCountryCode.text!, with: "")
        self.txtMobileNumber.text = originalMobileNumer
        
        //print("data dict is \(dictSignUpObj)")
    }
}

