//
//  SetProfilePictureController.swift
//  zify
//
//  Created by Anurag on 15/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import CropViewController


@available(iOS 9.0, *)
class SetProfilePictureController: UIViewController, UINavigationControllerDelegate, UIGestureRecognizerDelegate , ImageSelectionViewerDelegate,CropViewControllerDelegate{
    
    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var btnSetAndUpload: UIButton!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    @IBOutlet weak var btnEditProfilePic: UIButton!
    
    
    let picker = UIImagePickerController()
    var loginMode = ""
    var userProfileImage : UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        if UIScreen.main.sizeType == .iPhone5 {
            lblHeader.fontSize = 18
            lblSubHeader.fontSize = 13
            btnSetAndUpload.titleLabel?.fontSize = 18
        } else if UIScreen.main.sizeType == .iPhone6 {
            lblHeader.fontSize = 20
            lblSubHeader.fontSize = 14
            btnSetAndUpload.titleLabel?.fontSize = 19
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            lblHeader.fontSize = 20
            lblSubHeader.fontSize = 15
            btnSetAndUpload.titleLabel?.fontSize = 20
        }
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapOnProfileImage))
        gesture.numberOfTouchesRequired = 1
        gesture.delegate = self
        imgProfilePic.addGestureRecognizer(gesture)
        
        self.btnSetAndUpload.backgroundColor = UIColor.gray
        self.btnSetAndUpload.isEnabled = false;
        self.btnSetAndUpload.titleLabel?.text! = NSLocalizedString(UOS_IDCARD_BTN_UPLOAD, comment: "")
        btnEditProfilePic.setImage(UIImage.init(named: "Camera_Edit"), for: .normal)
        self.btnSetAndUpload.isHidden = true;
        
        let prefs = UserDefaults.standard
        let fbUrl = prefs.object(forKey: "fbURL") as? String ?? ""
        if(fbUrl.count > 0){
            self.btnSetAndUpload.isHidden = false;
            self.btnSetAndUpload.backgroundColor = UIColor.init(hex: "2662FF");

           // self.imgProfilePic.image = finalImage

            self.imgProfilePic.sd_setImage(with: URL(string: fbUrl)) { (img, err, cacheTypr, imgUrl) in
                if img != nil {
                    let finalImage = ImageHelper.image(with: img, scaledTo: self.imgProfilePic.frame.size)
                    self.btnSetAndUpload.isEnabled = true
                    self.imgProfilePic.image = finalImage
                    self.userProfileImage = finalImage
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        
    }
    
    // MARk: - Actions
    
    @IBAction func btnTappedSetAndUploadImage(_ sender: Any) {

        self.uploadProfileImage()
       /*
        if btnSetAndUpload.titleLabel?.text! == NSLocalizedString(SS_UPLOAD_BTN, comment: "") {
            self.uploadProfileImage()
        }*/
    }
    
    @IBAction func btnTappedEditProfilePic(_ sender: Any) {
        displayPhotoPickingOptions(sender)
    }
    
    func tapOnProfileImage() {
        displayPhotoPickingOptions(self)
    }
    
    func displayPhotoPickingOptions(_ sender: Any?) {
        ImageSelectionViewer.sharedInstance().imageSelectionDelegate = self
        //ImageSelectionViewer.sharedInstance().finalImageSize =  //CGSize(width: 320.0, height: 320.0)
        ImageSelectionViewer.sharedInstance().selectImage()
    }
    
    func userSelectedFinalImage(_ image: UIImage?) {
        self.perform(#selector(openCropImgeVc), with: image, afterDelay: 0.0)
       // self.openCropImgeVc(withImage: image!)
    }
    
    func allowImageEditing() -> Bool {
        return false
    }
    
    @objc func openCropImgeVc(withImage:UIImage) -> Void {
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.circular , image: withImage)
        cropController.delegate = self
        self.present(cropController, animated: false, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        let finalImage:UIImage = ImageHelper.image(with: image, scaledTo: self.imgProfilePic.frame.size)
        btnEditProfilePic.setImage(UIImage.init(named: "edit"), for: .normal)
        userProfileImage = finalImage
        self.imgProfilePic.image = finalImage
        self.btnSetAndUpload.backgroundColor = UIColor.init(hex: "2662FF");
        self.btnSetAndUpload.isEnabled = true
        self.dismiss(animated: true, completion:nil)
        self.uploadProfileImage()
    }
    
    func uploadProfileImage(){
        if(self.userProfileImage == nil){
            UIAlertController.showError(withMessage: "Please choose your Profile picture.", inViewController: self)
            return
        }
        let imageUploadrequest = UserUploadMultiData(uploadType: PROFILE_TYPE)
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(imageUploadrequest, withHandler: { response, error in
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            
                            var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                            let imagePath = resultDict["responseText"] as! String
                            //response?.responseObject["responseText"] as? String
                            self.updateUserProfileWithImagePath(imagePath: imagePath)
                        }
                    } else {
                        self.messageHandler.dismissBlockingLoadView(handler: {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                             })
                    }
            }, andImage: self.userProfileImage)
        })
    }
    
    func updateUserProfileWithImagePath(imagePath:String) {
            let updateRequest = VehicleDetailUpdateRequest(vehicleModel: "", andVehicleRegistrationNumber: "", andInsuranceNumber: "", andDrivingLicenseNumber: "", andWithVehicleImagePah: imagePath, withUpdate: PROFILE_IMAGE_TYPE, withMobileNumber: "", withIsoCode: "", withCountryCode:"")
            ServerInterface.sharedInstance().getResponse(updateRequest, withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.messageHandler.showSuccessMessage(response?.message)
                        MoEngageEventsClass.callProfilePICUploadedEvent()
                        
                        let prefs = UserDefaults.standard
                        prefs.removeObject(forKey: "fbURL")
                        prefs.synchronize()
                        self.completeUserLogin()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
    }
    func completeUserLogin() {
        
        let currentUser = UserProfile.getCurrentUser()
        if(currentUser?.userPreferences == nil){
        
        let navController: UINavigationController? = NewTPScreenViewController.showNewTPNavController()
        if let navController = navController {
            navigationController?.present(navController, animated: false)
        }
        }else{
            AppDelegate.getInstance().showUserHomeController()
            //    [(AppDelegate *)[UIApplication sharedApplication].delegate showUserHomeController];
        }
        return;
        OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)
    }
    func setBoolForAKeyForSignUpFlow() {
        let prefs = UserDefaults.standard
        prefs.set(true, forKey: "isFromSignUp")
        prefs.synchronize()
    }
    
    
    func validateUserInfo() -> Bool {
        if userProfileImage == nil {
            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_PROFILEPICVAL, comment: ""))
            //errorMessage.text = NSLocalizedString(VC_SIGNUP_PROFILEPICVAL, comment: "")
            return false
        }
        if (dictSignUpObj!["firstName"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            //errorMessage.text = NSLocalizedString(VC_SIGNUP_FIRSTNAMEVAL, comment: "")
            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_FIRSTNAMEVAL, comment: ""))

            return false
        }
        if (dictSignUpObj!["lastName"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            //errorMessage.text = NSLocalizedString(VC_SIGNUP_LASTNAMEVAL, comment: "")
            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_LASTNAMEVAL, comment: ""))

            return false
        }
        if (dictSignUpObj!["email"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            //errorMessage.text = NSLocalizedString(VC_SIGNUP_EMAILVAL, comment: "")
            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_EMAILVAL, comment: ""))

            return false
        }
        if !isValidEmail(emailStr: (dictSignUpObj!["email"] as! String)) { //!validateEmail(self.txtEmail.text) {
            //errorMessage.text = NSLocalizedString(VC_SIGNUP_EMAILVALIDVAL, comment: "")
            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_EMAILVALIDVAL, comment: ""))

            return false
        }
        if !(loginMode == "fb") {
            if (dictSignUpObj!["password"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
                //errorMessage.text = NSLocalizedString(VC_SIGNUP_PASSWORDVAL, comment: "")
                self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_PASSWORDVAL, comment: ""))

                return false
            }
            if ((dictSignUpObj!["password"] as! String).count) < 5 || ((dictSignUpObj!["password"] as! String).count) > 25 {
                //errorMessage.text = NSLocalizedString(VC_SIGNUP_PASSWARD_MIN_LENGHT_VAL, comment: "")
                self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_PASSWARD_MIN_LENGHT_VAL, comment: ""))

                return false
            }
        }
        
        if (dictSignUpObj!["gender"] as! String) == "" {
            //errorMessage.text = NSLocalizedString(VC_SIGNUP_GENDERVAL, comment: "")
            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_GENDERVAL, comment: ""))
            return false
        }
        if (dictSignUpObj!["mobileNo"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            //errorMessage.text = NSLocalizedString(VC_SIGNUP_MOBILEVAL, comment: "")
            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SIGNUP_MOBILEVAL, comment: ""))

            return false
        }
        return true
    }
    
    class func createSetProfilePicNavController() -> UINavigationController? {
        
        let storyBoard = UIStoryboard.init(name: "SignUp", bundle: Bundle.main)
        let profilePicNavCtrl = storyBoard.instantiateViewController(withIdentifier: "createSetProfilePicNavController") as? UINavigationController
        return profilePicNavCtrl
    }
    

}
