//
//  WhyMyMobileController.swift
//  zify
//
//  Created by Anurag on 20/06/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class WhyMyMobileController: UIViewController {

    @IBOutlet var messageHandler: MessageHandler!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var lblTitle2: UILabel!
    @IBOutlet weak var lblTitle3: UILabel!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIScreen.main.sizeType == .iPhone5 {
            lblHeader.fontSize = 21
            lblSubHeader.fontSize = 15
            lblTitle1.fontSize = 14
            lblTitle2.fontSize = 14
            lblTitle3.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR {
            lblHeader.fontSize = 22
            lblSubHeader.fontSize = 16
            lblTitle1.fontSize = 15
            lblTitle2.fontSize = 15
            lblTitle3.fontSize = 15
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXSMax {
            lblHeader.fontSize = 23
            lblSubHeader.fontSize = 17
            lblTitle1.fontSize = 16
            lblTitle2.fontSize = 16
            lblTitle3.fontSize = 16
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       

    }

    @IBAction func btnTappedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
