//
//  SignUpNameController.swift
//  SignUp
//
//  Created by Anurag on 05/06/18.
//  Copyright © 2018 Anurag. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import TrueSDK


class SignUpNameController: UIViewController, UITextFieldDelegate , TCTrueSDKDelegate {
    
    @IBOutlet weak var txtFirstName: ACFloatingTextfield!
    @IBOutlet weak var txtLastName: ACFloatingTextfield!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblGiveYourEmail: UILabel!
    @IBOutlet var btnGender: [UIButton]!
    @IBOutlet var messageHandler: MessageHandler!
    
    
    var strGender : String? = ""
    var userTrueCallerProfile:TCTrueProfile!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.getInstance().isForTrueCall = true
        TCTrueSDK.sharedManager().delegate = self
        TCTrueSDK.sharedManager().requestTrueProfile()

        self.setNeedsStatusBarAppearanceUpdate()
        self.addLeftView(txtField: self.txtFirstName)
        self.addLeftView(txtField: self.txtLastName)
        if UIScreen.main.sizeType == .iPhone5 {
            btnNext.titleLabel?.fontSize = 18
            lblHeader.fontSize = 18
            lblGiveYourEmail.fontSize = 12
        } else if UIScreen.main.sizeType == .iPhone6 {
            btnNext.titleLabel?.fontSize = 19
            lblHeader.fontSize = 20
            lblGiveYourEmail.fontSize = 13
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            btnNext.titleLabel?.fontSize = 20
            lblHeader.fontSize = 20
            lblGiveYourEmail.fontSize = 14
        }
        UIApplication.shared.statusBarStyle = .default
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (txtFirstName as UITextField).becomeFirstResponder()
    }
  
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Actions
    
    @IBAction func btnTappedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTappedNext(_ sender: Any) {
        
        guard txtFirstName.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count != 0 else {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_FIRSTNAMEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        guard txtLastName.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count != 0 else {
            showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_LASTNAMEVAL, comment: ""), navigationCtrl: self.navigationController!)
            return
        }
        /* guard strGender?.count != 0 else {
         showAlertView(strMessage: NSLocalizedString(VC_SIGNUP_GENDERVAL, comment: ""), navigationCtrl: self.navigationController!)
         return
         }*/
        MoEngageEventsClass.callSignUPInitiateEvent(withFirstname: txtFirstName.text!, andLastName: txtLastName.text!)
        dictSignUpObj?.updateValue(txtFirstName.text!.capitalized, forKey: "fname")
        dictSignUpObj?.updateValue(txtLastName.text!.capitalized, forKey: "lname")
        // dictSignUpObj?.updateValue(strGender!, forKey: "gender")
        dictSignUpObj?.updateValue("", forKey: "gender")
        dictSignUpObj?.updateValue("", forKey: "loginMode")

        
        let vcEmail = self.storyboard?.instantiateViewController(withIdentifier: "SignUpEmailController") as? SignUpEmailController
        vcEmail?.isfromFacebook = false
        if(self.userTrueCallerProfile != nil){
        vcEmail?.trueCallerProfile = self.userTrueCallerProfile
        }
        //vcEmail?.strUserName = txtFirstName.text!.capitalized + " " + txtLastName.text!
        self.navigationController?.pushViewController(vcEmail!, animated: true)
        
    }
    
    @IBAction func btnTappedGender(_ sender: UIButton) {
        
        for btnTemp in btnGender {
            if btnTemp.tag == sender.tag {
                if btnTemp.tag == 1 {
                    strGender = "Male"
                } else if btnTemp.tag == 2 {
                    strGender = "Female"
                } else if btnTemp.tag == 3 {
                    strGender = "Others"
                }
                btnTemp.setImage(UIImage.init(named: "radio_selected"), for: .normal)
            } else {
                btnTemp.setImage(UIImage.init(named: "radio_unselected"), for: .normal)
            }
        }
        
    }
    func addLeftView(txtField:UITextField) {
        let customLeftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
        txtField.leftView = customLeftView;
        txtField.leftViewMode = .always
    }
    // MARK: - Textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        if textField == txtFirstName {
            (self.txtLastName as UITextField).becomeFirstResponder()
        }
        return true
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
//            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
//            let typedCharacterSet = CharacterSet(charactersIn: string)
//            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
//            return alphabet
//    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    func didFailToReceiveTrueProfileWithError(_ error: TCError) {
        //Custom code here
        print("error message is \(error.getCode())")
    }
    func didReceive(_ profile: TCTrueProfile) {
        //Custom code here
       // print("profile is\(profile.firstName), \(profile.lastName), \(profile.email), \(profile.countryCode), \(profile.phoneNumber), \(profile.isVerified)")
        self.userTrueCallerProfile = profile
        self.txtFirstName.text = self.userTrueCallerProfile?.firstName ?? nil;
        self.txtLastName.text = self.userTrueCallerProfile?.lastName ?? nil;
    }

}

