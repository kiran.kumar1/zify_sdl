//
//  AboutController1.swift
//  zify
//
//  Created by Anurag Rathor on 06/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class AboutController: UIViewController,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate {
    
    let LIKE_FACEBOOK_ROW = 0
    let TNC_ROW = 1
    //#define SUPPORT_ROW 2
    //#define RATEUS_ROW 3
    let FAQ_ROW = 2


    @IBOutlet var versionNumber: UILabel!
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialiseView()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "Cell"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.accessoryType = .disclosureIndicator
        cell?.textLabel?.textColor = UIColor(red: 51.0 / 255.0, green: 51.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
        if let aSize = UIFont(name: "Helvetica Neue", size: 16.0) {
            cell?.textLabel?.font = aSize
        }
        if indexPath.row == LIKE_FACEBOOK_ROW {
            cell?.textLabel?.text = NSLocalizedString(VC_ABOUT_LIKEFB, comment: "")
            cell?.imageView?.image = UIImage(named: "facebook-logo.png")
        } else if indexPath.row == TNC_ROW {
            cell?.textLabel?.text = NSLocalizedString(CMON_GENERIC_TNC, comment: "")
            cell?.imageView?.image = UIImage(named: "termsAndCond.png")
        } else {
            cell?.textLabel?.text = NSLocalizedString(CMON_GENERIC_FAQ, comment: "")
            cell?.imageView?.image = UIImage(named: "FAQ.png")
        }
        
        if let aSize = UIFont(name: NormalFont, size: 16) {
            cell?.textLabel?.font = aSize
        }
        if UIScreen.main.sizeType == .iPhone6 {
            if let aSize = UIFont(name: NormalFont, size: 17) {
                cell?.textLabel?.font = aSize
            }
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            if let aSize = UIFont(name: NormalFont, size: 18) {
                cell?.textLabel?.font = aSize
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == LIKE_FACEBOOK_ROW {
            let facebookURL = URL(string: "fb://profile/1430144977216466")
            if let anURL = facebookURL {
                if UIApplication.shared.canOpenURL(anURL) {
                    UIApplication.shared.openURL(anURL)
                } else {
                    if let aString = URL(string: "https://www.facebook.com/zify.co") {
                        UIApplication.shared.openURL(aString)
                    }
                }
            }
        } else if indexPath.row == TNC_ROW {
            let genericWebNavController: UINavigationController? = GenericWebController.createGenericWebNavController()
            let webController = genericWebNavController?.topViewController as? GenericWebController
            webController?.webViewURL = NSLocalizedString(CMON_GENERIC_TERMSURL, comment: "")
            webController?.title = NSLocalizedString(CMON_GENERIC_TNC, comment: "")
            if let aController = genericWebNavController {
                present(aController, animated: true)
            }
        } else {
            let genericWebNavController: UINavigationController? = GenericWebController.createGenericWebNavController()
            let webController = genericWebNavController?.topViewController as? GenericWebController
            webController?.webViewURL = "https://support.zify.co/portal/kb"
            webController?.title = NSLocalizedString(CMON_GENERIC_FAQ, comment: "")
            if let aController = genericWebNavController {
                present(aController, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func initialiseView() {
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.01))
        let infoDictionary = Bundle.main.infoDictionary
        let majorVersion = infoDictionary?["CFBundleShortVersionString"] as? String
        versionNumber.text = String(format: NSLocalizedString(VC_ABOUT_VERSION, comment: "Version : {App Version}"), majorVersion ?? "")
    }
    
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }
    
    class func createAboutNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "UserSettings", bundle: Bundle.main)
        let aboutNavController = storyBoard.instantiateViewController(withIdentifier: "aboutNavController") as? UINavigationController
        return aboutNavController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
