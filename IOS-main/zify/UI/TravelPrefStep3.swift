//
//  TravelPrefStep2.swift
//  zify
//
//  Created by Anurag S Rathor on 17/11/17.
//  Copyright © 2017 zify. All rights reserved.
//

import UIKit

class TravelPrefStep3: BaseViewController {
    
    var ownerView:UIView = UIView()
    var riderView:UIView = UIView()
    
    @IBOutlet var messageHandler:MessageHandler! = MessageHandler()
    
    var isForwardAddressTapped:Bool = Bool()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showRouteOntheView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.messageHandler.container)
        self.messageHandler.container.addSubview(self.bgView);
        self.btnNext.addTarget(self, action: #selector(btnNextTapped), for: .touchUpInside)
        self.progressView.setProgress(0.75, animated: true)

        self.headerTitleLbl.text = NSLocalizedString(TP_SCREEN3_TITLE, comment: "");
        self.headerSubTitleLbl.text = NSLocalizedString(TP_SCREEN3_SUB_TITLE, comment: "");
        
        let remainingHeight:CGFloat = (self.bgView.frameHeight - self.headerSubTitleLbl.frameMaxY - self.bottomView.frameHeight)/2 - 1;
        // CGRect(x: 0, y: headerSubTitleLbl.frameMaxY, width:AppDelegate.screen_WIDTH, height: remainingHeight)
        ownerView = createViewForRiderOrOwner(imageName: "map.png", withTitle: NSLocalizedString(TP_SCREEN3_FORWARD_TITLE, comment: ""), withFrame:CGRect(x: 0, y: self.headerSubTitleLbl.frameMaxY, width: AppDelegate.screen_WIDTH(), height: remainingHeight),withTag: 44)
        self.bgView.addSubview(ownerView);
        
        let lineSeparator:UILabel = UILabel();
        lineSeparator.frame = CGRect(x:0, y:ownerView.frameMaxY, width:AppDelegate.screen_WIDTH(), height:2);
        lineSeparator.backgroundColor = UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        self.bgView.addSubview(lineSeparator);
        
        riderView = createViewForRiderOrOwner(imageName: "map.png", withTitle: NSLocalizedString(TP_SCREEN3_BACKWORD_TITLE, comment: ""), withFrame: ownerView.frame,withTag: 55)
        riderView.frameY = lineSeparator.frameMaxY
        self.bgView.addSubview(riderView);
        
        
      //  self.getRoutesFromStartAndDestination()
        
        // Do any additional setup after loading the view.
    }
    
    func createViewForRiderOrOwner(imageName:String, withTitle:String, withFrame:CGRect,withTag:Int) -> UIView {
        
        let mainview:UIView = UIView.init(frame: withFrame)
        mainview.backgroundColor = UIColor.clear;
        
        let padX:CGFloat = 30;
        let padY:CGFloat = 20;
        let view:UIView = UIView.init(frame: CGRect(x: padX, y: padY, width:(AppDelegate.screen_WIDTH() - 2*padX), height: mainview.frameHeight - 2*padY));
        view.backgroundColor = UIColor.clear;
        mainview.addSubview(view);
        
        let mapContainerView:MapContainerView = MapContainerView.init()
        mapContainerView.frame = view.frame;
        mapContainerView.frameX = 2;
        mapContainerView.frameY = 2;
        mapContainerView.frameWidth = mapContainerView.frameWidth - 4
        mapContainerView.frameHeight = mapContainerView.frameHeight - 4
        mapContainerView.backgroundColor = UIColor .green
        view.addSubview(mapContainerView)
        mapContainerView.tag = withTag + 30
        mapContainerView.createMapView()
        mapContainerView.backgroundColor = UIColor.clear
        
        
        var fontSize:CGFloat = 12;
        if(AppDelegate.screen_HEIGHT() >= 568){
            fontSize = 14;
        }
        let label: UILabel = UILabel.init(frame:CGRect(x: 0
            , y: view.frameHeight-30, width: view.frameWidth, height: 30))
        label.textColor = UIColor.init(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0);
        label.text = withTitle;
        label.backgroundColor = UIColor.white
        label.textAlignment = NSTextAlignment.center;
        view.addSubview(label);
        label.font = UIFont.init(name: NormalFont, size: fontSize)

        

        let mapImage:UIImage = UIImage.init(named: imageName)!
        var factor:CGFloat = 2.5;
        if(AppDelegate.screen_HEIGHT() <= 568){
            factor = 3;
        }
        let imageLogo:UIImageView = UIImageView.init(frame: CGRect(x:0, y: 0, width: mapImage.size.width/factor, height: mapImage.size.height/factor))
        imageLogo.frameY = (view.frameHeight - imageLogo.frameHeight)/2;
        imageLogo.image = mapImage
        view.addSubview(imageLogo);
        imageLogo.tag = withTag - 30

        imageLogo.center = CGPoint(x:view.frameWidth/2, y:view.frameHeight/2);
        imageLogo.frameY = imageLogo.frameY - label.frameHeight/2;
      
        let btn:UIButton = UIButton.init(type: UIButtonType.custom);
        btn.frame = view.frame;
        btn.frameX = 0;
        btn.frameY = 0
        view.addSubview(btn);
        btn.tag = withTag
        btn.addTarget(self, action: #selector(btnSelecttapped), for: .touchUpInside)
        btn.backgroundColor = UIColor.clear

        
        view.layer.borderColor = UIColor.red.cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 5;
        
        
        return mainview;
    }
    
    
    func btnSelecttapped(sender:UIButton) -> Void {
        print("selected tappped")
        var isFrom:Bool = Bool()
        if(sender.tag == 44){
            isForwardAddressTapped = true
            isFrom = true
        } else {
            isForwardAddressTapped = false
            isFrom = false
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let obj:RouteDisplayForStrp3 =  storyboard.instantiateViewController(withIdentifier: "routeDisplay") as! RouteDisplayForStrp3;
        obj.isFromHomeToOffice = isFrom
        let navController:UINavigationController = UINavigationController.init(rootViewController: obj)
        self.navigationController?.present(navController, animated: true, completion:nil)
        
    }
    
    func btnNextTapped(sender:UIButton) -> Void {
        if(AppDelegate.getInstance().homeToOffDriveRoute == nil){
            UIAlertController.showError(withMessage: NSLocalizedString(TP_SCREEN3_HOME_ROUTE_ERROR, comment: ""), inViewController: self)
        }else if(AppDelegate.getInstance().oFFToHomeDriveRoute == nil){
            UIAlertController.showError(withMessage: NSLocalizedString(TP_SCREEN3_OFFICE_ROUTE_ERROR, comment: ""), inViewController: self)
        }else{
        //"Please  choose onward  and return routes"
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let obj:TravelPrefStep4 = storyboard.instantiateViewController(withIdentifier: "TpScreen4") as! TravelPrefStep4;        self.navigationController?.pushViewController(obj, animated: true)
        }
    }
   
    func showRouteOntheView() -> Void{
        if(AppDelegate.getInstance().homeToOffDriveRoute != nil){
            let btn:UIButton = (self.bgView.viewWithTag(44) as? UIButton)!
            createMapView(route: AppDelegate.getInstance().homeToOffDriveRoute, withBtn: btn)
        }
        if(AppDelegate.getInstance().oFFToHomeDriveRoute != nil){
            let btn:UIButton = self.bgView.viewWithTag(55) as! UIButton
            createMapView(route: AppDelegate.getInstance().oFFToHomeDriveRoute, withBtn: btn)
        }
    }
    func createMapView(route:DriveRoute, withBtn:UIButton) -> Void {
        let tag:Int = withBtn.tag;
        let mapContainerView = self.bgView.viewWithTag(tag+30) as! MapContainerView
        let imageLogo = self.bgView.viewWithTag(tag - 30) as! UIImageView
        imageLogo.isHidden = true
        let mapView = mapContainerView.mapView
        mapView?.sourceCoordinate = CLLocationCoordinate2DMake(Double(route.srcLat)!, Double(route.srcLong)!)
        mapView?.destCoordinate = CLLocationCoordinate2DMake(Double(route.destLat)!, Double(route.destLong)!)
        mapView?.overviewPolylinePoints = route.overviewPolylinePoints
        mapView?.drawMap()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

