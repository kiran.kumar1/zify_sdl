//
//  NewTPScreenViewController.swift
//  zify
//
//  Created by Anurag Rathor on 25/02/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class NewTPScreenViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, MapLocationHelperDelegate, moveToNewTpScreenProtocal, moveToIDCardScreenFromTPProtocol {
    
    
    
    @IBOutlet var scrollView:UIScrollView!
    
    @IBOutlet var optionsTblView:UITableView!
    @IBOutlet var saveBtnView:UIView!
    @IBOutlet var saveBtn:UIButton!
    @IBOutlet var messageHandler:MessageHandler!
    @IBOutlet var mapBgScreen:UIView!
    @IBOutlet var mapBGView:UIView!
    @IBOutlet var mapBGViewShadow:UIView!
    @IBOutlet var mapContainerView:MapContainerView!
    @IBOutlet var mapTilteLable:UILabel!
    @IBOutlet var closeRouteBtn:UIButton!
    @IBOutlet var editRouteBtn:UIButton!
    
    @IBOutlet weak var btnCarOwner: UIButton!
    @IBOutlet weak var btnRider: UIButton!
    @IBOutlet weak var lblChooseYourMode: UILabel!
    @IBOutlet weak var lblThisCanBeChangeLater: UILabel!
    
    @IBOutlet weak var backbutton:UIBarButtonItem!
    @IBOutlet weak var  viewChooseMode:UIView!
    
    var isMapViewInitialised:Bool = false
    
    var modeSwitchCell:NewTpSwitchModeTableViewCell?
    
    var uploadView: IDUploadFromTp!
    
    var homeGeoDataInfoDict:Dictionary<String, AnyObject>?
    var workGeoDataInfoDict:Dictionary<String, AnyObject>?

    var onwardRouteInfoDict:Dictionary<String, AnyObject>?
    var returnRouteInfoDict:Dictionary<String, AnyObject>?
    
    var choosenModebyUser = ""
    var selectedStartTime = ""
    var selectedReturnTime = ""
    var selectedStartTimeStr = ""
    var selectedReturnTimeStr = ""
    var errorMsg = ""
    
    //MS_CAR_OWNER
    var modesArray = [NSLocalizedString(MS_CAR_OWNER, comment: ""), NSLocalizedString(MS_RIDER, comment: "")]
    
    var currentUser:UserProfile = UserProfile.getCurrentUser()!
    var isForEdit:Bool = false
    
    var timeFormatter1: DateFormatter?
    var dateTimeFormatter: DateFormatter?
    
    var expandedSectionHeaderNumber: Int = -1
    
    var expandHeight: CGFloat = 200.0
    var normalHeight: CGFloat  = 70.0
    
    var optionsArray:NSArray = NSArray()
    var mapLocationHelper:MapLocationHelper = MapLocationHelper.init()
    var selectedIndexPath:IndexPath?
    var isForRouteSelection:Bool = false
    var timeStrFromPicker:String = ""
    
    var visualEffectView:UIVisualEffectView = UIVisualEffectView.init()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentUser = UserProfile.getCurrentUser()
        if(currentUser.userPreferences != nil){
            // self.backbutton?.tintColor = UIColor.init(red: 42.0/255.0, green: 42.0/255.0, blue: 42.0/255.0, alpha: 1.0)
            self.backbutton?.tintColor = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            self.backbutton?.isEnabled = true
        }else{
            self.backbutton?.isEnabled = false
            self.backbutton?.tintColor = UIColor.clear
            self.navigationController?.navigationBar.barTintColor = UIColor.white
        }
        if(isForRouteSelection){
            isForRouteSelection = false
            if(self.selectedIndexPath!.row == 0 && AppDelegate.getInstance().homeToOffDriveRoute != nil){
                self.updateoptionsArrayBasedOnRouteSelection(indexpAth: self.selectedIndexPath!, withRouteSelected: true)
                //self.callApiForGettingGeoPolyline(route: AppDelegate.getInstance().homeToOffDriveRoute, isForOnwardRoute: true)
            }
            if(self.selectedIndexPath!.row == 1 && AppDelegate.getInstance().oFFToHomeDriveRoute != nil){
                self.updateoptionsArrayBasedOnRouteSelection(indexpAth: self.selectedIndexPath!, withRouteSelected: true)
                //self.callApiForGettingGeoPolyline(route: AppDelegate.getInstance().oFFToHomeDriveRoute, isForOnwardRoute: false)
            }
        }
       // currentUser.userDocuments.vehicleModel = "tata NExon"
       // currentUser.userDocuments.vehicleRegistrationNum = "TS 12345"
        var vehicleDetailsStr = ""
        if let vehicleModel = currentUser.userDocuments.vehicleModel {
            vehicleDetailsStr = vehicleModel
        }
        if(vehicleDetailsStr.count > 0 && !isForEdit){
            let vehicleDetails = "\(currentUser.userDocuments.vehicleModel ?? "") | \(currentUser.userDocuments.vehicleRegistrationNum ?? "")"
            let objectsArray:NSMutableArray = NSMutableArray.init(array:  self.optionsArray[4] as! [Any])
            let obj:NSMutableDictionary = NSMutableDictionary.init(dictionary:objectsArray[0] as! [String : String])
            obj.setValue(vehicleDetails.uppercased(), forKey: "userValue")
            objectsArray.replaceObject(at: 0, with: obj)
            let menusArray:NSMutableArray = NSMutableArray.init(array: self.optionsArray)
            menusArray.replaceObject(at: 4, with: objectsArray)
            self.optionsArray = NSArray.init(array: menusArray)
            self.optionsTblView.reloadData()
            self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CurrentLocationTracker.sharedInstance().updateCurrentLocation()
        saveBtn.setTitle(NSLocalizedString(UPS_VEHICLEDETAILS_BTN_SAVE, comment: ""), for: .normal)
        guard #available(iOS 10.0, *) else {
            NotificationCenter.default.addObserver(self,selector:#selector(contentSizeDidChange(notification:)),name: NSNotification.Name.UIContentSizeCategoryDidChange,object: nil)
            return
        }
        self.setFontForControls()
        self.saveBtn.isEnabled = false
        self.mapBgScreen.isHidden = true
        // self.viewChooseMode.isHidden = true
        if(currentUser.userPreferences == nil){
            isForEdit = false
            self.displayChooseModeView()
        }else{
            isForEdit = true
            let title = NSLocalizedString(MS_TRAVEL_PREFERENCES, comment: "")
            self.title = title
            //  self.tpDescLbl.text = ""  //removing the TP desc label
            self.choosenModebyUser = currentUser.userPreferences.userMode ?? modesArray[0]
            if(self.choosenModebyUser == "DRIVER"){
                self.choosenModebyUser = modesArray[0]
            }else if(self.choosenModebyUser == modesArray[1]){
                self.choosenModebyUser = modesArray[1]
            }
        }
        self.saveBtn.backgroundColor = UIColor.init(red: 0/255.0, green: 122/255.0, blue: 255.0/255.0, alpha: 0.4)
        self.addDropShadow(for: mapBGView)
        self.mapContainerView.layer.cornerRadius = 12
        self.mapContainerView.backgroundColor = UIColor.white
        self.drawShadow(toAView: self.mapBGViewShadow, with: UIColor(red: 186.0 / 255.0, green: 184.0 / 255.0, blue: 184.0 / 255.0, alpha: 0.8), withCornerRadius: 12, withShadowRadius: 5, withOffset: CGSize(width: 0, height: -3))
        self.addDropShadowForSaveBtnView(for: self.saveBtnView)
        self.saveBtnView.backgroundColor = UIColor.white
        
        self.selectedIndexPath = IndexPath.init(row: 0, section: 0)
        mapLocationHelper.mapLocationDelegate = self
        let locale = NSLocale(localeIdentifier: "en_US_POSIX")
        timeFormatter1 = DateFormatter()
        timeFormatter1?.dateFormat = "HH:mm:ss"
        timeFormatter1?.locale = locale as Locale
        dateTimeFormatter = DateFormatter()
        dateTimeFormatter?.dateFormat = "yyyy/MM/dd HH:mm"
        
        
        self.setOptionsArrayForTableView()
        _ = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.addArrayItemForTPOnUpdate), userInfo: nil, repeats: false)
        
        
    }
    func setFontForControls(){
        
        
        lblChooseYourMode.fontSize = 20;
        btnCarOwner.titleLabel?.fontSize = 16;
        btnRider.titleLabel?.fontSize = 16;
        lblThisCanBeChangeLater.fontSize = 12;
        saveBtn.titleLabel?.fontSize = 14
        
        if UIScreen.main.sizeType == .iPhone6 {
            lblChooseYourMode.fontSize = 21;
            btnCarOwner.titleLabel?.fontSize = 19;
            btnRider.titleLabel?.fontSize = 19;
            lblThisCanBeChangeLater.fontSize = 13;
            saveBtn.titleLabel?.fontSize = 20
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS {
            lblChooseYourMode.fontSize = 22;
            btnCarOwner.titleLabel?.fontSize = 20;
            btnRider.titleLabel?.fontSize = 20;
            lblThisCanBeChangeLater.fontSize = 14;
            saveBtn.titleLabel?.fontSize = 17
        }
    }
    
    @objc private func contentSizeDidChange(notification: NSNotification) {
        optionsTblView.reloadData()
    }
    
    func setOptionsArrayForTableView(){
        /*  if(currentUser.userPreferences == nil){
         self.addArrayItemForTPOnFirstTime()
         }else{
         self.addArrayItemForTPOnUpdate()
         }*/
        self.addArrayItemForTPOnFirstTime()
    }
    
    func addArrayItemForTPOnFirstTime() -> Void {
        
        
        
        let modeStr =  NSLocalizedString(new_tp_user_mode_str, comment: "")
        let homeLocStr =  NSLocalizedString(new_tp_user_home_location, comment: "")
        let homeLocDescStr =  NSLocalizedString(new_tp_user_home_location_desc, comment: "")
        let officeLocStr =  NSLocalizedString(new_tp_user_office_location, comment: "")
        let officeLocDescStr =  NSLocalizedString(new_tp_user_office_location_desc, comment: "")
        let startTimeStr =  NSLocalizedString(UOS_PREFERENCES_LBL_STARTTIME, comment: "")
        let startTimeDescStr =  NSLocalizedString(new_tp_user_start_time_desc, comment: "")
        let returnTimeStr =  NSLocalizedString(UOS_PREFERENCES_LBL_RETURNTIME, comment: "")
        let returnTimeDescStr =  NSLocalizedString(new_tp_user_return_time_desc, comment: "")
        let hometoOffRouteStr =  NSLocalizedString(new_tp_user_home_to_office_route, comment: "")
        let hometoOffRouteDescStr =  NSLocalizedString(new_tp_user_home_to_office_route_desc, comment: "")
        let offToHomeRouteStr =  NSLocalizedString(new_tp_user_office_to_home_route, comment: "")
        let offToHomeRouteDescStr =  NSLocalizedString(new_tp_user_office_to_home_route_desc, comment: "")
        let vehicleDetailsStr =  NSLocalizedString(UPS_VEHCILEDETAILS_NAV_TITLE, comment: "")
        let vehicleDetailsDescStr =  NSLocalizedString(new_tp_user_enter_vehicle_details, comment: "")
        
        
        
        let userChoosenModeDict:Dictionary = ["image":"Mode_change.png", "title":modeStr, "defaultSubTitle":self.choosenModebyUser,"userValue": self.choosenModebyUser, "isRightArrowShow":"1", "rightArrowImagename":"locality_right_arrow.png"]
        let modeArray:Array = [userChoosenModeDict];
        
        
        let homeLocDict:Dictionary = ["image":"Home_New.png", "title":homeLocStr, "defaultSubTitle":homeLocDescStr,"userValue":"", "isRightArrowShow":"1","rightArrowImagename":"locality_right_arrow.png"]
        let workLocDict:Dictionary = ["image":"Work_new.png", "title":officeLocStr, "defaultSubTitle":officeLocDescStr, "userValue":"", "isRightArrowShow":"1","rightArrowImagename":"locality_right_arrow.png"]
        
        let locArray:Array = [homeLocDict,workLocDict];
        
        let startTimeDict:Dictionary = ["image":"Time_new.png", "title":startTimeStr, "defaultSubTitle":startTimeDescStr,"userValue":"", "isRightArrowShow":"0","rightArrowImagename":"locality_right_arrow.png"]
        let returnTimeDict:Dictionary = ["image":"Time_new.png", "title":returnTimeStr, "defaultSubTitle":returnTimeDescStr, "userValue":"","isRightArrowShow":"0","rightArrowImagename":"locality_right_arrow.png"]
        
        
        let timeArray:Array = [startTimeDict,returnTimeDict];
        
        let homeToOfcRouteDict:Dictionary = ["image":"Route_new.png", "title":hometoOffRouteStr, "defaultSubTitle":hometoOffRouteDescStr, "userValue":"", "isRightArrowShow":"1","rightArrowImagename":"locality_right_arrow.png"]
        let OfcToHomeRouteDict:Dictionary = ["image":"Route_new.png", "title":offToHomeRouteStr, "defaultSubTitle":offToHomeRouteDescStr,"userValue":"", "isRightArrowShow":"1","rightArrowImagename":"locality_right_arrow.png"]
        
        let routesArray:Array = [homeToOfcRouteDict,OfcToHomeRouteDict];
        
        let vehiclesInfoDict:Dictionary = ["image":"vehicle_new.png", "title":vehicleDetailsStr, "defaultSubTitle":vehicleDetailsDescStr,"userValue":"", "isRightArrowShow":"1","rightArrowImagename":"locality_right_arrow.png"]
        
        let vehicleInfoArray:Array = [vehiclesInfoDict];
        
        optionsArray = [modeArray, locArray, timeArray,routesArray,vehicleInfoArray];
        optionsTblView.reloadData()
        let indexPath = IndexPath.init(row: 0, section: 0)
        modeSwitchCell = self.optionsTblView.cellForRow(at: indexPath) as? NewTpSwitchModeTableViewCell
        
    }
    
    
    func addArrayItemForTPOnUpdate() -> Void {
        if(!isForEdit){
            return
        }
        let userPreferences = currentUser.userPreferences!
        
        let sourceAddress = LocalityInfo.init(name: userPreferences.sourceAddress, andAddress: userPreferences.sourceAddress, andCity: userPreferences.city, andState: "", andCountry: "", andLatLng: CLLocationCoordinate2DMake(userPreferences.sourceLatitude.doubleValue, userPreferences.sourceLongitude.doubleValue))
        
        AppDelegate.getInstance().homeAddress = sourceAddress
        
        
        let destAddress = LocalityInfo.init(name: userPreferences.destinationAddress, andAddress: userPreferences.destinationAddress, andCity: userPreferences.city, andState: "", andCountry: "", andLatLng: CLLocationCoordinate2DMake(userPreferences.destinationLatitude.doubleValue, userPreferences.destinationLongitude.doubleValue))
        
        AppDelegate.getInstance().officeAddress = destAddress
        
        selectedStartTime = (userPreferences.startTime)!
        selectedReturnTime = (userPreferences.returnTime)!
        
        let startDate = (timeFormatter1?.date(from: selectedStartTime))!
        selectedStartTimeStr = AppDelegate.getInstance().timeFormatter.string(from: startDate)
        
        let returnDate = (timeFormatter1?.date(from: selectedReturnTime))!
        selectedReturnTimeStr = AppDelegate.getInstance().timeFormatter.string(from: returnDate)
        
        
        
        
        let modeStr =  NSLocalizedString(new_tp_user_mode_str, comment: "")
        let homeLocStr =  NSLocalizedString(new_tp_user_home_location, comment: "")
        let officeLocStr =  NSLocalizedString(new_tp_user_office_location, comment: "")
        let startTimeStr =  NSLocalizedString(UOS_PREFERENCES_LBL_STARTTIME, comment: "")
        let returnTimeStr =  NSLocalizedString(UOS_PREFERENCES_LBL_RETURNTIME, comment: "")
        let hometoOffRouteStr =  NSLocalizedString(new_tp_user_home_to_office_route, comment: "")
        let hometoOffRouteDescStr =  NSLocalizedString(new_tp_user_home_to_office_route_desc, comment: "")
        let offToHomeRouteStr =  NSLocalizedString(new_tp_user_office_to_home_route, comment: "")
        let offToHomeRouteDescStr =  NSLocalizedString(new_tp_user_office_to_home_route_desc, comment: "")
        let viewRouteStr =  NSLocalizedString(TS_DRIVES_LBL_VIEWROUTE, comment: "")
        
        
        let timeresetStr =  NSLocalizedString(new_tp_user_time_reset, comment: "")
        
        
        let userChoosenModeDict:Dictionary = ["image":"Mode_change.png", "title":modeStr, "defaultSubTitle":self.choosenModebyUser,"userValue": self.choosenModebyUser, "isRightArrowShow":"1", "rightArrowImagename":"locality_right_arrow.png"]
        let modeArray:Array = [userChoosenModeDict];
        
        
        let homeLocDict:Dictionary = ["image":"Home_New.png", "title":homeLocStr, "defaultSubTitle":userPreferences.sourceAddress,"userValue":currentUser.userPreferences.sourceAddress, "isRightArrowShow":"1","rightArrowImagename":"Edit_newTp.png"]
        let workLocDict:Dictionary = ["image":"Work_new.png", "title":officeLocStr, "defaultSubTitle":userPreferences.destinationAddress, "userValue":currentUser.userPreferences.destinationAddress, "isRightArrowShow":"1","rightArrowImagename":"Edit_newTp.png"]
        
        let locArray:Array = [homeLocDict,workLocDict];
        
        let startTimeDict:Dictionary = ["image":"Time_new.png", "title":startTimeStr, "defaultSubTitle":timeresetStr,"userValue":selectedStartTimeStr, "isRightArrowShow":"0","rightArrowImagename":"locality_right_arrow.png"]
        let returnTimeDict:Dictionary = ["image":"Time_new.png", "title":returnTimeStr, "defaultSubTitle":timeresetStr, "userValue":selectedReturnTimeStr,"isRightArrowShow":"0","rightArrowImagename":"locality_right_arrow.png"]
        
        
        let timeArray:Array = [startTimeDict,returnTimeDict];
        
        
        var homeToOfcRouteDict: [AnyHashable : Any] = [:]
        var OfcToHomeRouteDict: [AnyHashable : Any] = [:]
        
        if(currentUser.userPreferences.userMode != "RIDER"){
            self.choosenModebyUser = modesArray[0]
            
            let onwardRoute = DriveRoute.init(source: sourceAddress, andDestination: destAddress, andRouteId: userPreferences.onwardRouteId, andPolyline: userPreferences.onwardPolyline)
            AppDelegate.getInstance().homeToOffDriveRoute = onwardRoute
            
            let returnRoute = DriveRoute.init(source: destAddress, andDestination: sourceAddress, andRouteId: userPreferences.returnRouteId, andPolyline: userPreferences.returnPolyline)
            AppDelegate.getInstance().oFFToHomeDriveRoute = returnRoute
            
            homeToOfcRouteDict = ["image":"Route_new.png", "title":hometoOffRouteStr, "defaultSubTitle":hometoOffRouteDescStr, "userValue":viewRouteStr, "isRightArrowShow":"1","rightArrowImagename":"Edit_newTp.png"]
            OfcToHomeRouteDict = ["image":"Route_new.png", "title":offToHomeRouteStr, "defaultSubTitle":offToHomeRouteDescStr,"userValue":viewRouteStr, "isRightArrowShow":"1","rightArrowImagename":"Edit_newTp.png"]
        }else{
            self.choosenModebyUser = modesArray[1]
            
            homeToOfcRouteDict = ["image":"Route_new.png", "title":hometoOffRouteStr, "defaultSubTitle":hometoOffRouteDescStr, "userValue":"", "isRightArrowShow":"1","rightArrowImagename":"locality_right_arrow.png"]
            OfcToHomeRouteDict = ["image":"Route_new.png", "title":offToHomeRouteStr, "defaultSubTitle":offToHomeRouteDescStr,"userValue":"", "isRightArrowShow":"1","rightArrowImagename":"locality_right_arrow.png"]
        }
        
        let routesArray:Array = [homeToOfcRouteDict,OfcToHomeRouteDict];
        
        //   let vehiclesInfoDict:Dictionary = ["image":"vehicle_new.png", "title":"Vehicle Details", "defaultSubTitle":"Enter your Vehicle Details","userValue":"", "isRightArrowShow":"1","rightArrowImagename":"locality_right_arrow.png"]
        
        //   let vehicleInfoArray:Array = [vehiclesInfoDict];
        optionsArray = [modeArray, locArray, timeArray,routesArray];
        
        print("choosen is \(self.choosenModebyUser)")
        optionsTblView.reloadData()
        let indexPath = IndexPath.init(row: 0, section: 0)
        modeSwitchCell = self.optionsTblView.cellForRow(at: indexPath) as? NewTpSwitchModeTableViewCell
        
        var selectedIndex:Int = 1
        if( currentUser.userPreferences.userMode == "DRIVER"){
            selectedIndex = 0
        }
        modeSwitchCell?.modeChangeSwitch.selectedIndex = selectedIndex
    }
    
    func changeTheTableViewHeight() -> Void {
        
        /*  let height =  optionsTblView.contentSize.height + 20
         dynamicTVHeight.constant = height
         print("scroll view content size is \(scrollView.contentSize.height)")
         let screenHeight:CGFloat = UIScreen.main.bounds.height
         if(scrollView.contentSize.height >= screenHeight){
         topConstraint.constant = 2;
         }else{
         let remainingSpace:CGFloat = screenHeight - optionsTblView.frameMaxY
         let padding = remainingSpace - saveBtnView.frameHeight;
         topConstraint.constant = padding
         }
         topConstraint.constant = 2;
         */
        /*  if(height > self.view.frame.size.height){
         self.saveBtn.removeConstraint(bottomConstraint)
         }else{
         self.saveBtn.removeConstraint(topConstraint)
         }*/
        self.view.layoutIfNeeded()
    }
    class func showNewTPNavController() -> UINavigationController {
        let storyBoard = UIStoryboard.init(name: "UserProfile", bundle: Bundle.main)
        let homeNavigationCtrl = storyBoard.instantiateViewController(withIdentifier: "NewTpNavController") as? UINavigationController
        return homeNavigationCtrl!
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // return 2
        if(self.choosenModebyUser == modesArray[1]){
            return 3
        }else{
            return self.optionsArray.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objectsArray = optionsArray[section] as! [Any]
        if(section == 2 && self.expandedSectionHeaderNumber != -1){
            return objectsArray.count + 1
        }
        return (objectsArray.count);
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 2 && self.expandedSectionHeaderNumber != -1 && indexPath.row == self.expandedSectionHeaderNumber+1){
            return expandHeight
        }
        
        return normalHeight
    }
    func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0.0
        }
        return 5.0
    }
    func  tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0.0
        }
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        let color:UIColor = UIColor.init(red: 246.0/255.0, green: 247.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        header.contentView.backgroundColor = color
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let footer: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        let color:UIColor = UIColor.init(red: 246.0/255.0, green: 247.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        footer.contentView.backgroundColor = color
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 2 && self.expandedSectionHeaderNumber != -1 && indexPath.row == self.expandedSectionHeaderNumber+1){
            
            let cell:DatePickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "datePickerViewCell") as! DatePickerViewTableViewCell
            cell.dateTimePickerView.tag = indexPath.row
            cell.dateTimePickerView.addTarget(self, action: #selector(handleDatePicker), for:.valueChanged)
            let isTimein24: Bool = UserDefaults.standard.bool(forKey: "24hoursFormatEnabled")
            if(!isTimein24){
                cell.dateTimePickerView.locale = Locale.init(identifier: "en_GB")
            }
            if(self.expandedSectionHeaderNumber == 0){
                if selectedStartTime != "" {
                    cell.dateTimePickerView.setDate(timeFormatter1!.date(from: selectedStartTime)!, animated: true)
                }else{
                    //set the default time
                    let timeStr = self.setCurrentTimeForDateTimePickerByDefault()
                    selectedStartTime = timeStr//(timeFormatter1?.string(from: timeStr))!
                    selectedStartTimeStr = AppDelegate.getInstance().timeFormatter.string(from: Date())
                    cell.dateTimePickerView.setDate(timeFormatter1!.date(from:timeStr)!, animated: true)
                }
            }else{
                if selectedReturnTime != "" {
                    cell.dateTimePickerView.setDate(timeFormatter1!.date(from: selectedReturnTime)!, animated: true)
                }else{
                    //set the default time
                    let timeStr = self.setCurrentTimeForDateTimePickerByDefault()
                    selectedReturnTime = timeStr//(timeFormatter1?.string(from: timeStr))!
                    selectedReturnTimeStr = AppDelegate.getInstance().timeFormatter.string(from: Date())
                    cell.dateTimePickerView.setDate(timeFormatter1!.date(from:timeStr)!, animated: true)
                }
            }
            return cell
        }else{
            
            let objectsArray:Array = optionsArray[indexPath.section] as! [Any]
            var index:Int = indexPath.row
            if(index >= objectsArray.count){
                index = objectsArray.count - 1
            }
            var obj:Dictionary = objectsArray[index] as! Dictionary<String,String>
            let userValue = obj["userValue"]
            
            if( indexPath.section == 0){
                let cell:NewTpSwitchModeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "newTpswitchCell") as! NewTpSwitchModeTableViewCell
                cell.moveToNewTpScreenProtocalDelegate = self
                cell.adjustFontChangesForCell()
                let imageName:String = obj["image"]!
                cell.sectionImageView.image = UIImage.init(named: imageName)
                if( userValue != nil && userValue!.count > 0){
                    cell.displaySubTitle.text = userValue
                    cell.displaySubTitle.textColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                    
                    if( userValue == modesArray[1]){
                        print("choosen Rider")
                        cell.modeChangeSwitch.defaultSelectedIndex = 1
                    }else{
                        print("choosen Driver")
                        cell.modeChangeSwitch.defaultSelectedIndex = 0
                    }
                    self.modeSwitchCell = cell
                    
                }else{
                    cell.displaySubTitle.text = obj["defaultSubTitle"]
                    cell.displaySubTitle.textColor = UIColor.gray
                }
                cell.displayTitleLabel.text = obj["title"]
                cell.displayTitleLabel.font = setFontSizes(anyObj: cell.displayTitleLabel, fontStyle: MediumFont, defaultSize: 14.0, iphone6Size: 15.0, iphoneXRSize: 15.0, iphone6PlusSize: 16.0, iphoneXSize: 16.0, iphoneXSMaxSize: 16.0)
                cell.displaySubTitle.font = setFontSizes(anyObj: cell.displaySubTitle, fontStyle: NormalFont, defaultSize: 10.0, iphone6Size: 12.0, iphoneXRSize: 12.0, iphone6PlusSize: 13.0, iphoneXSize: 13.0, iphoneXSMaxSize: 13.0)
                return cell;
                
            }else{
                let cell:NewTPTableViewCell = tableView.dequeueReusableCell(withIdentifier: "newTpCell") as! NewTPTableViewCell
                cell.adjustFontChangesForCell()
                cell.displaySubTitle.isHidden = false
                cell.displayTitleLabel.text = obj["title"]
                
                cell.viewRouteBtn.tag = indexPath.row
                cell.setButton.tag = indexPath.row
                cell.viewRouteBtn.addTarget(self, action: #selector(viewRouteBtnTapped), for: .touchUpInside)
                cell.setButton.addTarget(self, action: #selector(setTimeBtnTapped), for: .touchUpInside)
                
                cell.viewRouteBtn.isHidden = true
                
                if(indexPath.section == 1){
                    if( userValue != nil && userValue!.count > 0){
                        cell.displaySubTitle.text = userValue
                        cell.displaySubTitle.textColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                    }else{
                        cell.displaySubTitle.text = obj["defaultSubTitle"]
                        cell.displaySubTitle.textColor = UIColor.gray
                    }
                }else if( indexPath.section == 2){
                   // print("hehehe uservalue is \(userValue)")
                    if(userValue!.count > 0){
                        if(indexPath.row == 0){
                            //selectedStartTimeStr
                            cell.setButton.setTitle(selectedStartTimeStr, for: .normal)
                            cell.setButton.setTitle(selectedStartTimeStr, for: .selected)
                        }else{
                            //selectedReturnTimeStr
                            cell.setButton.setTitle(selectedReturnTimeStr, for: .normal)
                            cell.setButton.setTitle(selectedReturnTimeStr, for: .selected)
                        }
                        cell.displaySubTitle.textColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                    }else{
                        cell.displaySubTitle.textColor = UIColor.gray
                    }
                    cell.displaySubTitle.text = obj["defaultSubTitle"]
                }else if(indexPath.section == 3){
                    if(indexPath.row == 0){
                        if(AppDelegate.getInstance().homeToOffDriveRoute != nil){
                            cell.displaySubTitle.isHidden = true
                            cell.viewRouteBtn.isHidden = false
                        }else{
                            cell.displaySubTitle.text = obj["defaultSubTitle"]
                            cell.displaySubTitle.isHidden = false
                            cell.displaySubTitle.textColor = UIColor.gray
                        }
                    }else{
                        if(AppDelegate.getInstance().oFFToHomeDriveRoute != nil){
                            cell.displaySubTitle.isHidden = true
                            cell.viewRouteBtn.isHidden = false
                        }else{
                            cell.displaySubTitle.text = obj["defaultSubTitle"]
                            cell.displaySubTitle.isHidden = false
                            cell.viewRouteBtn.isHidden = true
                            cell.displaySubTitle.textColor = UIColor.gray
                        }
                    }
                }else{
                    cell.viewRouteBtn.isHidden = true
                    if(userValue!.count > 0){
                        cell.displaySubTitle.text = userValue
                        cell.displaySubTitle.textColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                        //cell.viewRouteBtn.isHidden = false
                    }else{
                        cell.displaySubTitle.text = obj["defaultSubTitle"]
                        cell.displaySubTitle.textColor = UIColor.gray
                        cell.viewRouteBtn.isHidden = true
                        
                    }
                }
                let rightArrowimageName = obj["rightArrowImagename"]
                cell.rightIconImageView.image = UIImage.init(named: rightArrowimageName!)
                let imageName:String = obj["image"]!
                cell.sectionImageView.image = UIImage.init(named: imageName)
                let isDisplayRightArraow:Int = Int(obj["isRightArrowShow"]!)!
                if(isDisplayRightArraow == 1){
                    cell.rightIconImageView.isHidden = false
                    cell.setButton.isHidden = true
                }else{
                    cell.rightIconImageView.isHidden = true
                    cell.setButton.isHidden = false
                }
                cell.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                if(self.expandedSectionHeaderNumber != -1 && indexPath.row == self.expandedSectionHeaderNumber && indexPath.section == 2){
                    cell.separatorInset = UIEdgeInsets.init(top: 0, left: cell.bounds.size.width, bottom: 0, right: 0)
                }
                self.changeTheTableViewHeight()
                cell.displayTitleLabel.font = setFontSizes(anyObj: cell.displayTitleLabel, fontStyle: MediumFont, defaultSize: 14.0, iphone6Size: 15.0, iphoneXRSize: 15.0, iphone6PlusSize: 16.0, iphoneXSize: 16.0, iphoneXSMaxSize: 16.0)
                cell.displaySubTitle.font = setFontSizes(anyObj: cell.displaySubTitle, fontStyle: NormalFont, defaultSize: 10.0, iphone6Size: 12.0, iphoneXRSize: 12.0, iphone6PlusSize: 13.0, iphoneXSize: 13.0, iphoneXSMaxSize: 13.0)
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        if(indexPath.section == 1){
            self.showAddressPickerForSelectedRow(selectedRow: indexPath.row)
        }else if(indexPath.section == 2){
            self.showTimePickerForSelectedRow(selectedRow: indexPath.row)
        }else if(indexPath.section == 3){
            self.showRouteForSelectedRow(selectedRow: indexPath.row)
            /* if(indexPath.row == 0){
             if(AppDelegate.getInstance().homeToOffDriveRoute == nil){
             self.showRouteForSelectedRow(selectedRow: indexPath.row)
             }
             }else if(indexPath.row == 1){
             if(AppDelegate.getInstance().oFFToHomeDriveRoute == nil){
             self.showRouteForSelectedRow(selectedRow: indexPath.row)
             }
             }*/
        }else if(indexPath.section == 4){
            self.showVehicleDetailsForSelectedRow(selectedRow: indexPath.row)
        }
    }
    
    func moveToNewTpMethod(withSelectedIndex: Int) {
        self.collapseTableViewBeforeLoadingOther()
        if(withSelectedIndex == 0){
            self.choosenModebyUser = modesArray[0]
        }else{
            self.choosenModebyUser = modesArray[1]
        }
        self.selectedIndexPath = IndexPath.init(row: 0, section: 0)
        let objectsArray:NSMutableArray = NSMutableArray.init(array:  self.optionsArray[self.selectedIndexPath!.section] as! [Any])
        let index:Int = self.selectedIndexPath!.row
        let obj:NSMutableDictionary = NSMutableDictionary.init(dictionary:objectsArray[index] as! [String : String])
        obj.setValue(self.choosenModebyUser, forKey: "userValue")
        
        objectsArray.replaceObject(at: index , with: obj)
        let menusArray:NSMutableArray = NSMutableArray.init(array: self.optionsArray)
        menusArray.replaceObject(at: self.selectedIndexPath!.section, with: objectsArray)
        self.optionsArray = NSArray.init(array: menusArray)
        self.optionsTblView.reloadData()
        self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
    }
    
    
    func showAddressPickerForSelectedRow(selectedRow:Int) -> Void {
        self.collapseTableViewBeforeLoadingOther()
        
        messageHandler.showBlockingLoadView {
            self.mapLocationHelper.showPick(onMap: self.navigationController)
            
        }
    }
    
    func collapseTableViewBeforeLoadingOther() -> Void {
        if(self.expandedSectionHeaderNumber != -1){
            let indexPath = IndexPath.init(row: self.expandedSectionHeaderNumber, section: 2)
            self.setTimeToArrayOptions(indexPath: indexPath, isForUpdate: true )
            self.expandedSectionHeaderNumber = -1
            optionsTblView.reloadData()
        }
        self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
        
    }
    
    func showTimePickerForSelectedRow(selectedRow:Int) -> Void {
        
        //self.collapseTableViewBeforeLoadingOther()
        if(self.expandedSectionHeaderNumber != -1){
            let indexPth = IndexPath.init(row: self.expandedSectionHeaderNumber, section: 2)
            self.setTimeToArrayOptions(indexPath: indexPth, isForUpdate: true)
            if(self.expandedSectionHeaderNumber != selectedRow){
                let indexPth = IndexPath.init(row: selectedRow, section: 2)
                self.setTimeToArrayOptions(indexPath: indexPth, isForUpdate: false)
            }
        }
        
        if(self.expandedSectionHeaderNumber == selectedRow){
            self.expandedSectionHeaderNumber = -1
        }else{
            self.expandedSectionHeaderNumber = selectedRow
            let indexPth = IndexPath.init(row: selectedRow, section: 2)
            self.setTimeToArrayOptions(indexPath: indexPth, isForUpdate: false)
        }
        
        if(selectedRow >= 2){
            self.expandedSectionHeaderNumber = self.expandedSectionHeaderNumber - 1
        }
        optionsTblView.reloadData()
        self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
    }
    
    func showRouteForSelectedRow(selectedRow:Int) -> Void {
        self.collapseTableViewBeforeLoadingOther()
        if(AppDelegate.getInstance().homeAddress != nil && AppDelegate.getInstance().officeAddress != nil){
            isForRouteSelection = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let obj:RouteDisplayForStrp3 =  storyboard.instantiateViewController(withIdentifier: "routeDisplay") as! RouteDisplayForStrp3;
            if( selectedRow == 0){
                obj.isFromHomeToOffice = true
            }else{
                obj.isFromHomeToOffice = false
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            UIAlertController.showError(withMessage: "Add your Addresses first", inViewController: self)
        }
        
    }
    func updateoptionsArrayBasedOnRouteSelection(indexpAth:IndexPath, withRouteSelected:Bool) {
        let objectsArray:NSMutableArray = NSMutableArray.init(array:  self.optionsArray[indexpAth.section] as! [Any])
        
        let index:Int = indexpAth.row
        let obj:NSMutableDictionary = NSMutableDictionary.init(dictionary:objectsArray[index] as! [String : String])
        if(withRouteSelected){
           let routeStr =  NSLocalizedString(TS_DRIVES_LBL_VIEWROUTE, comment: "")
            obj.setValue(routeStr, forKey: "userValue")
            obj.setValue("Edit_newTp.png", forKey: "rightArrowImagename")
        }else{
            obj.setValue("", forKey: "userValue")
            obj.setValue("locality_right_arrow.png", forKey: "rightArrowImagename")
        }
        objectsArray.replaceObject(at: index, with: obj)
        let menusArray:NSMutableArray = NSMutableArray.init(array: self.optionsArray)
        menusArray.replaceObject(at: indexpAth.section, with: objectsArray)
        self.optionsArray = NSArray.init(array: menusArray)
        self.optionsTblView.reloadData()
        self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
    }
    
    
    func showVehicleDetailsForSelectedRow(selectedRow:Int) -> Void {
        self.collapseTableViewBeforeLoadingOther()
        let userProfile:UserProfile = UserProfile.getCurrentUser()
        if(userProfile.userDocuments.vehicleImgUrl != nil &&  userProfile.userDocuments.vehicleImgUrl.count > 0 ){
            let prefs = UserDefaults.standard
            prefs.set(true, forKey: "forPush")
            prefs.synchronize()
            performSegue(withIdentifier: "showVehicleDetailsAvailable1", sender: self)
            
        }else if( userProfile.userDocuments.vehicleModel != nil && userProfile.userDocuments.vehicleModel.count != 0 && userProfile.userDocuments.vehicleRegistrationNum != nil && userProfile.userDocuments.vehicleRegistrationNum.count != 0 ){
            let prefs = UserDefaults.standard
            prefs.set(true, forKey: "forPush")
            prefs.synchronize()
            performSegue(withIdentifier: "showTextImgUploadController1", sender: self)
        } else {
            performSegue(withIdentifier: "showVehicleTextDetails1", sender: self)
        }
        
    }
    func selectedLocationInfo(_ locationInfo: LocalityInfo!, andError error: Error!) {
        if error != nil {
            messageHandler.dismissBlockingLoadView {
            self.messageHandler.showErrorMessage(NSLocalizedString(VC_USERPREFERENCES_LOCATIONMAPPINGERROR, comment: ""))
            }
        } else {
            if locationInfo != nil {
                messageHandler.dismissBlockingLoadView {
                    
                    if(locationInfo?.address == nil){
                        UIAlertController.showError(withMessage: NSLocalizedString(address_error_string, comment: ""), inViewController: self)
                        return
                    }
                    let cell:NewTPTableViewCell = self.optionsTblView.cellForRow(at: self.selectedIndexPath!) as! NewTPTableViewCell
                    cell.displaySubTitle.text = locationInfo?.address
                    cell.rightIconImageView.image = UIImage.init(named: "Edit_newTp.png")
                    cell.displaySubTitle.textColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                    if(self.selectedIndexPath?.row == 0){
                        AppDelegate.getInstance().homeAddress = locationInfo
                        //self.callApiForGettingNearestPointsToALocation(location: locationInfo, forHomeAddress: true)
                    }else{
                        AppDelegate.getInstance().officeAddress = locationInfo
                        //self.callApiForGettingNearestPointsToALocation(location: locationInfo, forHomeAddress: false)

                    }
                    let objectsArray:NSMutableArray = NSMutableArray.init(array:  self.optionsArray[self.selectedIndexPath!.section] as! [Any])
                    let index:Int = self.selectedIndexPath!.row
                    let obj:NSMutableDictionary = NSMutableDictionary.init(dictionary:objectsArray[index] as! [String : String])
                    obj.setValue(locationInfo?.address, forKey: "userValue")
                    obj.setValue("Edit_newTp.png", forKey: "rightArrowImagename")
                    objectsArray.replaceObject(at: index, with: obj)
                    let menusArray:NSMutableArray = NSMutableArray.init(array: self.optionsArray)
                    menusArray.replaceObject(at: self.selectedIndexPath!.section, with: objectsArray)
                    self.optionsArray = NSArray.init(array: menusArray)
                    
                    ////// Need to call get nearest points api here..
                    
                    if(AppDelegate.getInstance().homeToOffDriveRoute != nil){
                        let indexPath = IndexPath.init(row: 0, section: 3)
                        self.updateoptionsArrayBasedOnRouteSelection(indexpAth: indexPath, withRouteSelected: false)
                        AppDelegate.getInstance().homeToOffDriveRoute = nil
                    }
                    
                    if(AppDelegate.getInstance().oFFToHomeDriveRoute != nil){
                        let indexPath = IndexPath.init(row: 1, section: 3)
                        self.updateoptionsArrayBasedOnRouteSelection(indexpAth: indexPath, withRouteSelected: false)
                        AppDelegate.getInstance().oFFToHomeDriveRoute = nil
                    }
                    self.optionsTblView.reloadData()
                    self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
                }
            }else{
                messageHandler.dismissBlockingLoadView {
                }
            }
        }
    }
    
    /*
    func callApiForGettingNearestPointsToALocation(location:LocalityInfo, forHomeAddress:Bool){
        let nearestPointsRequest = GetNearestPointsToALocationRequest.init(location: location)
        ServerInterface.sharedInstance().getResponse(nearestPointsRequest, withHandler: { response, error in
            if response != nil {
                var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                let userGeoDataArray = resultDict["userGeoData"] as! Array<Any>
                print("userGeoDataArray is \(userGeoDataArray)")
                if(userGeoDataArray.count > 0){
                    let userGeoData = userGeoDataArray[0] as? Dictionary<String, AnyObject>
                    print("response is \(userGeoData)")
                    if(forHomeAddress){
                        self.homeGeoDataInfoDict = userGeoData
                    }else{
                        self.workGeoDataInfoDict = userGeoData
                    }
                }else{
                    if(forHomeAddress){
                        self.homeGeoDataInfoDict = ["address":location.address as AnyObject,"city":location.city as AnyObject,"exists":NSNumber.init(value: false),"geoDataId":NSNumber.init(value: 0),"lat":location.latLng.latitude as AnyObject,"lng":location.latLng.longitude as AnyObject]
                    }else{
                        self.workGeoDataInfoDict = ["address":location.address as AnyObject,"city":location.city as AnyObject,"exists":NSNumber.init(value: false),"geoDataId":NSNumber.init(value: 0),"lat":location.latLng.latitude as AnyObject,"lng":location.latLng.longitude as AnyObject]
                    }
                }
            }else {
                //// Need to implement to show the error msg to user
                //Must and Should
            }
        })
    }
    
    
    func callApiForGettingGeoPolyline(route:DriveRoute,isForOnwardRoute:Bool){
        let polyline = GetGEOPolylineForRouteRequest.init(route: route)
        ServerInterface.sharedInstance().getResponse(polyline, withHandler: { response, error in
            if response != nil {
                var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                let userRoute = resultDict["userRoute"] as? Dictionary<String, AnyObject>
                print("userRoute is \(userRoute)")
                if(isForOnwardRoute){
                    self.onwardRouteInfoDict = userRoute
                }else{
                    self.returnRouteInfoDict = userRoute
                }
            } else {
                
            }
        })
    }
    */
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) -> Void{
        timeStrFromPicker = AppDelegate.getInstance().timeFormatter.string(from: datePicker.date)
        // self.setTimeToArrayOptions(indexPath: self.selectedIndexPath!)
        let indexPath = IndexPath.init(row: datePicker.tag - 1, section: 2)
        let cell:NewTPTableViewCell = self.optionsTblView.cellForRow(at: indexPath) as! NewTPTableViewCell
        cell.setButton.setTitle(timeStrFromPicker, for: .normal)
        cell.setButton.setTitle(timeStrFromPicker, for: .selected)
        
        let index = datePicker.tag - 1
        if(index == 0){
            selectedStartTimeStr = timeStrFromPicker;
            selectedStartTime = (timeFormatter1?.string(from: datePicker.date))!
        }else{
            selectedReturnTimeStr = timeStrFromPicker
            selectedReturnTime = (timeFormatter1?.string(from: datePicker.date))!
        }
        self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
    }
    
    func setTimeToArrayOptions(indexPath:IndexPath, isForUpdate:Bool){
        let objectsArray:NSMutableArray = NSMutableArray.init(array:  self.optionsArray[indexPath.section] as! [Any])
        var index:Int = indexPath.row
        if(index >= objectsArray.count){
            index = objectsArray.count - 1
        }
        let obj:NSMutableDictionary = NSMutableDictionary.init(dictionary:objectsArray[index] as! [String : String])
        if(isForUpdate){
            if(timeStrFromPicker.count == 0){
                timeStrFromPicker = AppDelegate.getInstance().timeFormatter.string(from: Date())
                // self.setTimeToArrayOptions(indexPath: self.selectedIndexPath!)
                if(index == 0){
                    selectedStartTimeStr = timeStrFromPicker;
                    selectedStartTime = (timeFormatter1?.string(from: Date()))!
                }else{
                    selectedReturnTimeStr = timeStrFromPicker
                    selectedReturnTime = (timeFormatter1?.string(from: Date()))!
                }
            }
            obj.setValue(timeStrFromPicker, forKey: "userValue")
            let timeresetStr =  NSLocalizedString(new_tp_user_time_reset, comment: "")
            obj.setValue(timeresetStr, forKey: "defaultSubTitle")
        }else{
            obj.setValue("", forKey: "userValue")
            if(indexPath.row == 0) {
                obj.setValue("Set time when you start from Home", forKey: "defaultSubTitle")
            }else{
                obj.setValue("Set time when you return from Office", forKey: "defaultSubTitle")
            }
            
        }
        objectsArray.replaceObject(at: index , with: obj)
        let menusArray:NSMutableArray = NSMutableArray.init(array: self.optionsArray)
        menusArray.replaceObject(at: indexPath.section, with: objectsArray)
        self.optionsArray = NSArray.init(array: menusArray)
        self.optionsTblView.reloadData()
        
        self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
        
    }
    
    func viewRouteBtnTapped(sender:UIButton) -> Void{
        self.collapseTableViewBeforeLoadingOther()
        var isForOnwardRoute = true
        self.editRouteBtn.tag = 0
        if(sender.tag == 1){
            isForOnwardRoute = false
            self.editRouteBtn.tag = 1
        }
        showRouteMap(isForOnwardRoute: isForOnwardRoute)
    }
    
    
    func setTimeBtnTapped(sender:UIButton) -> Void{
        // self.collapseTableViewBeforeLoadingOther()
        self.selectedIndexPath = IndexPath.init(row: sender.tag, section: 2)
        self.showTimePickerForSelectedRow(selectedRow: sender.tag)
    }
    
    func showRouteMap(isForOnwardRoute:Bool) {
        self.mapBgScreen.isHidden = false
        var sourceLocalityInfo: LocalityInfo?
        var destinationLocalityInfo: LocalityInfo?
        var overviewPolylinePoints: String?
        if (isForOnwardRoute) //HOMEOFFICE
        {
            let routeStr = NSLocalizedString(new_tp_user_home_to_office_route, comment: "")
            let str  = " \(routeStr) "
            mapTilteLable.text =  str
            sourceLocalityInfo = AppDelegate.getInstance().homeAddress
            destinationLocalityInfo = AppDelegate.getInstance().officeAddress
            overviewPolylinePoints = (AppDelegate.getInstance().homeToOffDriveRoute.overviewPolylinePoints) ?? ""
        } else {
            
            let routeStr = NSLocalizedString(new_tp_user_office_to_home_route, comment: "")
            let str  = " \(routeStr) "
            mapTilteLable.text = str
            sourceLocalityInfo = AppDelegate.getInstance().officeAddress
            destinationLocalityInfo = AppDelegate.getInstance().homeAddress
            overviewPolylinePoints = (AppDelegate.getInstance().oFFToHomeDriveRoute.overviewPolylinePoints) ?? ""
        }
        if !isMapViewInitialised {
            mapContainerView.createMapView()
        }
        let mapView = mapContainerView.mapView
        mapView?.layer.cornerRadius = 12
        mapView?.sourceCoordinate = (sourceLocalityInfo?.latLng)!
        mapView?.destCoordinate = (destinationLocalityInfo?.latLng)!
        mapView?.overviewPolylinePoints = overviewPolylinePoints
        mapView?.mapPadding = UIEdgeInsetsMake(5, 0, 0, 0)
        if !isMapViewInitialised {
            mapView?.drawMap()
            isMapViewInitialised = true
        } else {
            mapView?.drawMarkers()
            mapView?.modifyPath(for: 0)
        }
    }
    
    @IBAction func closeBtntapped(sender:UIButton){
        self.mapBgScreen.isHidden = true
    }
    
    
    @IBAction func editRoutebtnTapped(sender:UIButton){
        self.mapBgScreen.isHidden = true
        self.selectedIndexPath = IndexPath.init(row: sender.tag, section: 3)
        self.showRouteForSelectedRow(selectedRow: self.selectedIndexPath!.row)
    }
    func addDropShadow(for viewShadow: UIView?) {
        viewShadow?.backgroundColor = UIColor.white
        viewShadow?.layer.shadowColor = UIColor(red: 117.0 / 255.0, green: 117.0 / 255.0, blue: 117.0 / 255.0, alpha: 0.4).cgColor
        viewShadow?.layer.shadowOpacity = 1
        viewShadow?.layer.shadowOffset = CGSize(width: -1, height: 0)
        viewShadow?.layer.shadowRadius = 4
        viewShadow?.layer.masksToBounds = false
        viewShadow?.layer.cornerRadius = 12
    }
    
    func addDropShadowForSaveBtnView(for viewShadow: UIView?) {
        viewShadow?.backgroundColor = UIColor.white
        viewShadow?.layer.shadowColor = UIColor(red: 117.0 / 255.0, green: 117.0 / 255.0, blue: 117.0 / 255.0, alpha: 0.4).cgColor
        viewShadow?.layer.shadowOpacity = 1
        viewShadow?.layer.shadowOffset = CGSize(width: -1, height: 0)
        viewShadow?.layer.shadowRadius = 4
        viewShadow?.layer.masksToBounds = false
    }
    func drawShadow(toAView viewShadow: UIView?, with color: UIColor?, withCornerRadius radius: Int, withShadowRadius shadowRadius: Int, withOffset size: CGSize) {
        viewShadow?.backgroundColor = UIColor.white
        viewShadow?.layer.shadowOpacity = 1
        viewShadow?.layer.shadowOffset = size
        viewShadow?.layer.shadowRadius = CGFloat(shadowRadius)
        viewShadow?.layer.masksToBounds = false
        viewShadow?.layer.cornerRadius = CGFloat(radius)
        viewShadow?.layer.shadowColor = color?.cgColor
    }
    
    @IBAction func saveBtnTapped(sender:UIButton) -> Void{
        self.collapseTableViewBeforeLoadingOther()
        self.validateTheUserChoosenDataToEnableOrDisableSaveButton()
        if( errorMsg == ""){
            self.showPopUpForIdCardUpload()
            var modeStr = "RIDER"
            if(self.choosenModebyUser == modesArray[0]){
                modeStr = "DRIVER"
            }
            //call api for add Travel Preference
            var request:UserPreferencesRequest? 
            if isForEdit {
                request = UserPreferencesRequest.init(with: UPDATEPREFERENCES, andHomeAddress: AppDelegate.getInstance().homeAddress, andOfficeAddress: AppDelegate.getInstance().officeAddress, andStartTime: selectedStartTime, andReturnTime: selectedReturnTime, andOnwardRoute: AppDelegate.getInstance().homeToOffDriveRoute, andReturn: AppDelegate.getInstance().oFFToHomeDriveRoute, andUserMode: modeStr)
                  //request = UserPreferencesRequest.init(with: UPDATEPREFERENCES, andHomeAddress: self.homeGeoDataInfoDict, andOfficeAddress: self.workGeoDataInfoDict, andStartTime: selectedStartTime, andReturnTime: selectedReturnTime, andOnwardRoute: self.onwardRouteInfoDict, andReturnRoute: self.returnRouteInfoDict, andUserMode: modeStr)
            } else {
                request = UserPreferencesRequest.init(with: ADDPREFERENCES, andHomeAddress: AppDelegate.getInstance().homeAddress, andOfficeAddress: AppDelegate.getInstance().officeAddress, andStartTime: selectedStartTime, andReturnTime: selectedReturnTime, andOnwardRoute: AppDelegate.getInstance().homeToOffDriveRoute, andReturn: AppDelegate.getInstance().oFFToHomeDriveRoute, andUserMode: modeStr)
              // request = UserPreferencesRequest.init(with: ADDPREFERENCES, andHomeAddress: self.homeGeoDataInfoDict, andOfficeAddress: self.workGeoDataInfoDict, andStartTime: selectedStartTime, andReturnTime: selectedReturnTime, andOnwardRoute: self.onwardRouteInfoDict, andReturnRoute: self.returnRouteInfoDict, andUserMode: modeStr)
            }
            ServerInterface.sharedInstance().getResponse(request, withHandler: { response, error in
                if response != nil {
                    let prefs = UserDefaults.standard
                    prefs.set(true, forKey: "tpUpdated")
                    prefs.synchronize()
                    MoEngageEventsClass.callTPSaveEvent()
                    self.uploadView.showSuccessView()
                    AppDelegate.getInstance().homeAddress = nil
                    AppDelegate.getInstance().officeAddress = nil
                    AppDelegate.getInstance().homeToOffDriveRoute = nil
                    AppDelegate.getInstance().oFFToHomeDriveRoute = nil
                    _ = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(self.backScreen), userInfo: nil, repeats: false)

//                    let prefId = ((response?.responseObject as! NSDictionary).value(forKey: "travelpreference") as! NSDictionary).value(forKey: "prefId") as! Int
//                    UserDefaults.standard.set(prefId, forKey: "prefId")
//                    UserDefaults.standard.synchronize()
                    
                /*    let userDocuments: UserDocuments? = self.currentUser.userDocuments
                    if userDocuments?.isIdCardDocUploaded.intValue == 0 {
                        _ = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(self.showIDCardUploadView), userInfo: nil, repeats: false)
                    }else{
                        _ = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(self.backScreen), userInfo: nil, repeats: false)
                    }*/
                    if self.isForEdit == false {
                        ReferAndEarnController.serviceCall_ProcessReferralData(eventId: 2, strUserType: modeStr)
                    }
                    
                } else {
                    if(self.uploadView != nil){
                        self.uploadView.removeFromSuperview()
                    }
                    UIAlertController.showError(withMessage:(error as NSError?)?.userInfo["message"] as? String , inViewController: self)
                }
            })
        }else{
            UIAlertController.showError(withMessage: errorMsg, inViewController: self)
        }
    }
    
    func backScreen() -> Void{
        self.uploadView.removeView()
        if(isForEdit){
        self.navigationController?.popViewController(animated: true)
        }else{
            OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)
        }
        
    }
    func showIDCardUploadView() -> Void {
        self.uploadView.showInfoView()
    }
    func validateTheUserChoosenDataToEnableOrDisableSaveButton() -> Void {
        errorMsg = ""
        self.saveBtn.isEnabled = false
        if(choosenModebyUser.count == 0){
            errorMsg = "Please choose your mode"
        }else if(AppDelegate.getInstance().homeAddress == nil){
            errorMsg = "Please choose your home address"
        }else if(AppDelegate.getInstance().officeAddress == nil){
            errorMsg = "Please choose your office address"
        }else if(selectedStartTime.count == 0){
            errorMsg = "Please choose your Start time"
        }else if(selectedReturnTime.count == 0){
            errorMsg = "Please choose your return time"
        }else if(choosenModebyUser != modesArray[1]){
            let userProfile:UserProfile = UserProfile.getCurrentUser()
            
            if(AppDelegate.getInstance().homeToOffDriveRoute == nil){
                errorMsg = "Please choose your onward address"
            }else if(AppDelegate.getInstance().oFFToHomeDriveRoute == nil){
                errorMsg = "Please choose your return address"
            }else if(!isForEdit){
                var vehiclemOdelStr = ""
                if let vehicleDetails = userProfile.userDocuments.vehicleModel{
                    vehiclemOdelStr = vehicleDetails
                }
                if(vehiclemOdelStr.count == 0){
                    errorMsg = "Please submit your vehicle details"
                }
            }
        }
        if(errorMsg == ""){
            self.saveBtn.backgroundColor = UIColor.init(red: 38.0/255.0, green: 98.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            self.saveBtn.isEnabled = true
        }else{
            self.saveBtn.backgroundColor = UIColor.init(red: 0/255.0, green: 122/255.0, blue: 255.0/255.0, alpha: 0.4)
            self.saveBtn.isEnabled = false
        }
    }
    
    
    
    
    func displayChooseModeView() {
        var blurEffect: UIVisualEffect?
        blurEffect = UIBlurEffect(style: .dark)
        
        visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView.alpha = 0.9
        visualEffectView.frame = view.bounds
        navigationController?.view.addSubview(visualEffectView)
        viewChooseMode.frame = CGRect.init(x: 0, y: AppDelegate.screen_HEIGHT(), width: AppDelegate.screen_WIDTH(), height: self.viewChooseMode.frame.size.height)
        
        //CGRect(x: 0, y: AppDelegate.screen_HEIGHT, width: AppDelegate.screen_WIDTH, height: self.viewChooseMode.frame.size.height)
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {
            
            self.viewChooseMode.frame = CGRect.init(x: 0, y: AppDelegate.screen_HEIGHT() - self.viewChooseMode.frame.size.height, width: AppDelegate.screen_WIDTH(), height: self.viewChooseMode.frame.size.height)
        }) { finished in
        }
        navigationController?.view.addSubview(viewChooseMode)
        
        
    }
    
    @IBAction func btnTappedCarOwnerOrRider(_ sender: UIButton) {
        self.viewChooseMode.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseIn, animations: {
            self.viewChooseMode.frame = CGRect.init(x: 0, y: AppDelegate.screen_HEIGHT(), width: AppDelegate.screen_WIDTH(), height: self.viewChooseMode.frame.size.height)
        }) { finished in
            if finished {
                self.viewChooseMode.isHidden = true
            }
            self.visualEffectView.removeFromSuperview()
            var selectedIndex:Int = 0;
            if sender.tag == 0 {
                selectedIndex = 0
                self.choosenModebyUser = self.modesArray[0] //DRIVER
            } else {
                selectedIndex = 1
                self.choosenModebyUser = self.modesArray[1] //RIDER
            }
            AppDelegate.getInstance().travelTypeStr = self.choosenModebyUser
            self.viewChooseMode.isUserInteractionEnabled = true
            self.moveToNewTpMethod(withSelectedIndex: selectedIndex)
            self.setChooseModeSegmentView()
        }
    }
    
    func setChooseModeSegmentView(){
        if(self.choosenModebyUser == modesArray[1]){
            self.modeSwitchCell?.modeChangeSwitch.selectedIndex = 1
        }else{
            self.modeSwitchCell?.modeChangeSwitch.selectedIndex = 0
        }
    }
    
    func showPopUpForIdCardUpload() {
        let viewsArray = Bundle.main.loadNibNamed("IDCardViews", owner: self, options: nil)
        
        for object: Any? in viewsArray ?? [] {
            if (object is IDUploadFromTp) {
                uploadView = object as? IDUploadFromTp
                break
            }
        }
        if let uploadView = uploadView {
            AppDelegate.getInstance().window.addSubview(uploadView)
        }
        uploadView.delegate = self
        uploadView?.translatesAutoresizingMaskIntoConstraints = false
        
        let viewsDictionary:Dictionary = NSDictionary.init(object: uploadView, forKey: "uploadView" as NSCopying) as Dictionary
        let horizontalConsts: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "|[uploadView]|", options: [], metrics: nil, views: viewsDictionary as! [String : Any])
        let verticalConsts: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|[uploadView]|", options: [], metrics: nil, views: viewsDictionary as! [String : Any])
        AppDelegate.getInstance().window.addConstraints(horizontalConsts)
        AppDelegate.getInstance().window.addConstraints(verticalConsts)
        
    }
    
    func removeUploadView() -> Void {
        let viewsArray = Bundle.main.loadNibNamed("IDCardViews", owner: self, options: nil)
        var uploadIDView:IDUploadFromTp?
        for object: Any? in viewsArray ?? [] {
            if (object is IDUploadFromTp) {
                uploadIDView = object as? IDUploadFromTp
                break
            }
        }
        if let view = uploadIDView {
            view.removeFromSuperview()
        }
    }
    
    @IBAction func dismiss(_ sender:UIButton){
        AppDelegate.getInstance().homeAddress = nil
        AppDelegate.getInstance().officeAddress = nil
        AppDelegate.getInstance().homeToOffDriveRoute = nil
        AppDelegate.getInstance().oFFToHomeDriveRoute = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    func setCurrentTimeForDateTimePickerByDefault() -> String{
        
        return timeFormatter1?.string(from: Date()) ?? ""
        
    }
    
    
    
    func skipBtnTapped() {
        if(!isForEdit){
            OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setNowbtntapped() {
        if(isForEdit){
            self.navigationController?.popViewController(animated: false)
            AppDelegate.getInstance().showIDCardUploadScreen()
        }else{
            UserDefaults.standard.setValue(true, forKey: "showIDScreenToUpload")
            UserDefaults.standard.synchronize()
            OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: true)
            
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = optionsTblView.tableHeaderView else {
            return
        }
        
        // The table view header is created with the frame size set in
        // the Storyboard. Calculate the new size and reset the header
        // view to trigger the layout.
        
        // Calculate the minimum height of the header view that allows
        // the text label to fit its preferred width.
        
        
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            
            // Need to set the header view property of the table view
            // to trigger the new layout. Be careful to only do this
            // once when the height changes or we get stuck in a layout loop.
            
            optionsTblView.tableHeaderView = headerView
            
            // Now that the table view header is sized correctly have
            // the table view redo its layout so that the cells are
            // correcly positioned for the new header size.
            
            // This only seems to be necessary on iOS 9.
            
            optionsTblView.layoutIfNeeded()
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

/*
class GetNearestPointsToALocationRequest : ServerRequest {
    var locationAddress:LocalityInfo?
    
    init?(location:LocalityInfo) {
        super.init()
        self.locationAddress = location

    }
    
    override func urlparams() -> [AnyHashable : Any]! {
        var params = self.getUserCommonParams() as! [String : AnyObject]
        let profile:UserProfile = UserProfile.getCurrentUser()
        params["city"] = locationAddress?.city as AnyObject
        params["countryCode"] = profile.country as AnyObject//locationAddress?.country as AnyObject
        params["lat"] = locationAddress?.latLng.latitude as AnyObject
        params["lng"] = locationAddress?.latLng.longitude as AnyObject
        params["maxDistance"] = NSNumber.init(value: 500)
        params["limit"] = NSNumber.init(value: 1)

        return params
    }
    
    override func serverURL() -> String! {
        return "ridematchingdev/user/geo/getNearestAvailablePoints" //ridematchingdev
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
    override func isGet() -> Bool {
        return true
    }
    override func overrideBaseURL() -> Bool {
        return true
    }
    
}


class GetGEOPolylineForRouteRequest : ServerRequest {
    var selectedRoute:DriveRoute?
    
    init?(route:DriveRoute) {
        super.init()
        self.selectedRoute = route
        
    }
    
    override func urlparams() -> [AnyHashable : Any]! {
        var params = self.getUserCommonParams() as! [String : AnyObject]
        let profile:UserProfile = UserProfile.getCurrentUser()
        params["countryCode"] = profile.country as AnyObject
        params["distance"] = self.selectedRoute?.distance as AnyObject
        params["distanceUnit"] = self.selectedRoute?.distanceUnit as AnyObject
        params["polyline"] = self.selectedRoute?.overviewPolylinePoints as AnyObject
        
        return params
    }
    
    override func serverURL() -> String! {
        return "ridematchingdev/user/geo/polyline" //ridematchingdev/
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
 
    override func overrideBaseURL() -> Bool {
        return true
    }
    
}
 
 */
