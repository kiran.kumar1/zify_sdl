//
//  CountryCodeSuggestionController.swift
//  zify
//
//  Created by Anurag on 04/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

protocol CountryCodeSelectionDelegate: NSObjectProtocol {
    func selectedCountryCode(_ countryCode: CountryCode!)
}

class CountryCodeSuggestionController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, CountryCodeSelectionDelegate, NSFetchedResultsControllerDelegate {
    
    func selectedCountryCode(_ countryCode: CountryCode!) {
        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var selectionDelegate: CountryCodeSelectionDelegate!
    var selectedUserRegion:CountryCode?
    
    var isCameFromNonCountries = false
    
    var fetchResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.endEditing(true)
        fetchResultsController = getFetchedResultsFROMISOCodeController("")//getFetchedResultsController(nil)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        print("country name is \(String(describing: self.selectionDelegate))")
        
       let selectedCountryCode = CountryCode.getCountryObject(forISOCode: CurrentLocale.sharedInstance().getISOCode(), in: DBInterface.sharedInstance().sharedContext)
        let countryStr = selectedCountryCode?.isoCode ?? ""
        let arrayOFObje = AppDelegate.getInstance().servicesCountriesArray as? Array<String> ?? []
        if(arrayOFObje .count > 0){
            if(arrayOFObje.contains(countryStr)){
                print("isCameFromCountries list")
            }else{
                print("not available")
                isCameFromNonCountries = true
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dismiss(_ sender: Any) {
        searchBar.resignFirstResponder()
        if(isCameFromNonCountries){
            if(AppDelegate.getInstance().userRegion == nil){
                UIAlertController.showError(withMessage: NSLocalizedString(country_code_selection_error, comment: ""), inViewController: self)
            }else{
                navigationController?.dismiss(animated: true)
            }
        }else{
            navigationController?.dismiss(animated: true)
        }
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedObject = fetchResultsController?.object(at: indexPath) as! NSManagedObject
        searchBar.resignFirstResponder()
        let isoCode = selectedObject.value(forKey: "isoCode") as! String
        let countryCode = CountryCode.getCountryObject(forISOCode: isoCode, in: DBInterface.sharedInstance().sharedMainContext())
        AppDelegate.getInstance().userRegion = countryCode

        selectionDelegate?.selectedCountryCode(countryCode)
        navigationController?.dismiss(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count: Int = (fetchResultsController?.sections![0].numberOfObjects)!
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let CellIdentifier = "Cell"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
        }
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        let countryCode = fetchResultsController?.object(at: indexPath) as! NSManagedObject
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        if let aCode = countryCode.value(forKey: "isdCode"), let aName = countryCode.value(forKey: "name") {
            cell?.textLabel?.text = "\(aCode)  \(aName)"
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        fetchResultsController = getFetchedResultsFROMISOCodeController(searchText)
        var error: Error?
        try? fetchResultsController?.performFetch()
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    
    func getFetchedResultsFromISOCodesController() -> NSFetchedResultsController<NSFetchRequestResult>? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: "CountryCode", in: DBInterface.sharedInstance().sharedMainContext())
        fetchRequest.entity = entity
        let sort = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        fetchRequest.fetchBatchSize = 20
        var predicate: NSPredicate?
        
        //let servicesProvidedCountriesListArray = ["IN","FR","DE","DK"]
        if (AppDelegate.getInstance().servicesCountriesArray?.count ?? 0 > 0) {
            predicate = NSPredicate(format: "isoCode IN  %@", AppDelegate.getInstance().servicesCountriesArray )
            fetchRequest.predicate = predicate
        } else {
            
        }
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DBInterface.sharedInstance().sharedMainContext(), sectionNameKeyPath: nil, cacheName: nil)
        var error: Error?
        try? fetchedResultsController.performFetch()
        return fetchedResultsController
    }
    
    
    
    func getFetchedResultsController(_ text: String?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: "CountryCode", in: DBInterface.sharedInstance().sharedMainContext())
        fetchRequest.entity = entity
        let sort = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        fetchRequest.fetchBatchSize = 20
        var predicate: NSPredicate?
        if (text?.count ?? 0) > 0 {
            predicate = NSPredicate(format: "name BEGINSWITH[cd] %@", text ?? "")
            fetchRequest.predicate = predicate
        } else {
            
        }
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DBInterface.sharedInstance().sharedMainContext(), sectionNameKeyPath: nil, cacheName: nil)
        var error: Error?
        try? fetchedResultsController.performFetch()
        return fetchedResultsController
    }
    
    
    func getFetchedResultsFROMISOCodeController(_ text: String?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: "CountryCode", in: DBInterface.sharedInstance().sharedMainContext())
        fetchRequest.entity = entity
        let sort = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        fetchRequest.fetchBatchSize = 20
        var predicate: NSPredicate?
        if (text?.count ?? 0) > 0 {
            if (AppDelegate.getInstance().servicesCountriesArray?.count ?? 0 > 0){
            predicate = NSPredicate(format: "name BEGINSWITH[cd] %@ AND isoCode in  %@", text ?? "", AppDelegate.getInstance().servicesCountriesArray)
                fetchRequest.predicate = predicate

            }else{
                predicate = NSPredicate(format: "name BEGINSWITH[cd] %@", text ?? "")
                fetchRequest.predicate = predicate

            }
        }else if (AppDelegate.getInstance().servicesCountriesArray?.count ?? 0 > 0) {
            predicate = NSPredicate(format: "isoCode IN  %@", AppDelegate.getInstance().servicesCountriesArray )
            fetchRequest.predicate = predicate
        }else{
            
        }
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DBInterface.sharedInstance().sharedMainContext(), sectionNameKeyPath: nil, cacheName: nil)
        var error: Error?
        try? fetchedResultsController.performFetch()
        return fetchedResultsController
    }
    
    
    
    class func createCountryCodeNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Commons", bundle: Bundle.main)
        let accountNavController = storyBoard.instantiateViewController(withIdentifier: "countryCodeNavController") as? UINavigationController
        return accountNavController
    }
    
    
}
