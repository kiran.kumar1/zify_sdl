//
//  UserIDCardDetailsController.swift
//  zify
//
//  Created by Anurag on 07/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import SDWebImage

class UserIDCardDetailsController: UIViewController {
    
    //@IBOutlet weak var closePageArrow: UIImageView!
    //@IBOutlet weak var pageTitle: UILabel!
    //@IBOutlet var pageControl: UIPageControl!
    @IBOutlet weak var identityCardType: UILabel!
    @IBOutlet weak var identityCardTypeArrow: UIImageView!
    @IBOutlet weak var identityCardTypeView: UIView!
    @IBOutlet weak var identityCardImage: UIImageView!
    @IBOutlet weak var identityCardPlaceholder: UIView!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var uploadIdCardBtn: UIButton!
    @IBOutlet weak var uploadIdCardView: UIView!
    @IBOutlet weak var remindLaterView: UIView!
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var identityCardPickerView: CustomPickerView!
    @IBOutlet weak var lblSafe: UILabel!
    @IBOutlet weak var lblIdCardTypeSelected: UILabel!
    @IBOutlet weak var lblIdCardSecureStoredText: UILabel!
    @IBOutlet weak var lblNotShowAnyOneIDCardText: UILabel!
    
    
    @IBOutlet weak var lblTapToUploadText: UILabel!
    @IBOutlet weak var lblTakePictureText: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnRemindLater: UIButton!
    
    var isUpgradeFlow = false
    
    var identityCardTypeSelectedValue = ""
    var identityCardPickerValues: [AnyObject] = []
    var currentUser: UserProfile?
    var selectedIdentityCardImage: UIImage?
    var isIDCardUploaded = false
    var isViewInitialised = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblSafe.isHidden = true
        lblIdCardTypeSelected.isHidden = true
        isViewInitialised = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UIScreen.main.sizeType == .iPhone5 {
            lblSafe.fontSize = 13
            lblIdCardTypeSelected.fontSize = 13
            uploadIdCardBtn.titleLabel?.fontSize = 14
            lblIdCardSecureStoredText.fontSize = 11
            lblNotShowAnyOneIDCardText.fontSize = 11
            lblTapToUploadText.fontSize = 10
            lblTakePictureText.fontSize = 10
            btnNext.titleLabel?.fontSize = 14
            btnRemindLater.titleLabel?.fontSize = 14
            //pageTitle.fontSize = 14
        } else if UIScreen.main.sizeType == .iPhone6 {
            lblSafe.fontSize = 14
            lblIdCardTypeSelected.fontSize = 14
            uploadIdCardBtn.titleLabel?.fontSize = 16
            lblIdCardSecureStoredText.fontSize = 12
            lblNotShowAnyOneIDCardText.fontSize = 12
            lblTapToUploadText.fontSize = 10
            lblTakePictureText.fontSize = 10
            btnNext.titleLabel?.fontSize = 14
            btnRemindLater.titleLabel?.fontSize = 14
            //pageTitle.fontSize = 15
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            lblSafe.fontSize = 16
            lblIdCardTypeSelected.fontSize = 16
            uploadIdCardBtn.titleLabel?.fontSize = 18
            lblIdCardSecureStoredText.fontSize = 12
            lblNotShowAnyOneIDCardText.fontSize = 12
            lblTapToUploadText.fontSize = 11
            lblTakePictureText.fontSize = 11
            btnNext.titleLabel?.fontSize = 15
            btnRemindLater.titleLabel?.fontSize = 15
            //pageTitle.fontSize = 16
        }
        
        if !isViewInitialised {
            initilaiseView()
        }
        
    }
    
    func initilaiseView() {
        identityCardPickerValues = CurrentLocale.sharedInstance().getIdentityCardTyes()! as [AnyObject]
        identityCardTypeSelectedValue = identityCardPickerValues[0] as! String
        currentUser = UserProfile.getCurrentUser()
        isIDCardUploaded = false
        
        identityCardType.text = identityCardTypeSelectedValue
        errorMessage.text = ""
        identityCardTypeView.layer.cornerRadius = 3.0
        identityCardTypeView.clipsToBounds = true
        identityCardTypeView.layer.borderColor = UIColor.lightGray.cgColor
        identityCardTypeView.layer.borderWidth = 1.0
        let arrowImage = UIImage(named: "left_arrow.png")
        identityCardTypeArrow.image = arrowImage?.withRenderingMode(.alwaysTemplate)
        identityCardTypeArrow.tintColor = UIColor.lightGray
        //identityCardTypeArrow.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_2))
        //closePageArrow.image = arrowImage?.withRenderingMode(.alwaysTemplate)
        //closePageArrow.tintColor = UIColor(red: 231.0 / 255.0, green: 76.0 / 255.0, blue: 58.0 / 255.0, alpha: 1.0)
        if !isUpgradeFlow {
            //pageTitle.text = NSLocalizedString(VC_USERIDCARD_IDENTITYCARD, comment: "")
            //self.navBar.title = NSLocalizedString(VC_USERIDCARD_IDENTITYCARD, nil);
            navigationItem.title = NSLocalizedString(VC_USERIDCARD_IDENTITYCARD, comment: "")
            
            //pageControl.isHidden = true
            if currentUser?.userDocuments.isIdCardDocUploaded != 0 && !(currentUser?.userDocuments.idCardImgUrl == nil) && !(currentUser?.userDocuments.idCardImgUrl == "") {
                identityCardTypeSelectedValue = CurrentLocale.sharedInstance().getIdentityCardTypeValue(currentUser?.userDocuments.idCardType)
                identityCardType.text = identityCardTypeSelectedValue
                identityCardTypeView.isUserInteractionEnabled = false
                identityCardPlaceholder.isHidden = true
                identityCardImage.isUserInteractionEnabled = false
                identityCardImage.isHidden = false
                let identityCardImageURL = URL(string: (currentUser?.userDocuments.idCardImgUrl)!)
                identityCardImage.sd_setImage(with: identityCardImageURL, completed: { image, error, cacheType, imageURL in
                    if image != nil {
                        
                        self.identityCardImage.image = ImageHelper.image(with: image, scaledTo: self.identityCardImage.frame.size)
                        
                        //identityCardImage.image = image?.imageScaled(toFitSize: identityCardImage.frame.size)
                        
                    }
                    self.identityCardImage.isHidden = false
                })
                uploadIdCardBtn.setTitle(NSLocalizedString(VC_USERIDCARD_UPLOADED, comment: ""), for: .normal)
                uploadIdCardBtn.isHidden = true
                identityCardTypeView.isHidden = true
                uploadIdCardBtn.isUserInteractionEnabled = false
                lblSafe.isHidden = false
                lblIdCardTypeSelected.isHidden = false
                lblIdCardTypeSelected.text = identityCardTypeSelectedValue.uppercased()
            }
            remindLaterView.isHidden = true
        } else {
            //closePageArrow.isHidden = true
        }
        isViewInitialised = true
    }
    
    @IBAction func dismiss(_ sender: Any) {
        let prefs = UserDefaults.standard
        let isForPush: Bool = prefs.bool(forKey: "forPush")
        if isForPush {
            navigationController?.popViewController(animated: true)
            return
        }
        dismiss(animated: false)
    }
    
    
    class func createUserIDCardDetailsController() -> UserIDCardDetailsController? {
        let storyBoard = UIStoryboard(name: "UserOnboard", bundle: Bundle.main)
        let userIDCardDetailsController = storyBoard.instantiateViewController(withIdentifier: "userIDCardDetailsController") as? UserIDCardDetailsController
        return userIDCardDetailsController
    }
    
    class func userIDCardDetailsNavCtrl() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "UserOnboard", bundle: Bundle.main)
        let userIDCardNavController = storyBoard.instantiateViewController(withIdentifier: "userIDCardDetailsNavCtrl") as? UINavigationController
        return userIDCardNavController
    }
}
