//
//  SettingsViewController.swift
//  zify
//
//  Created by Anurag on 05/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var messageHandler: MessageHandler!
    
    var totalRows: Int = 0
    var isGlobalLocale = false
    var isGlobalPayment = false
    var currentUser: UserProfile?
    var isFBUserLoggedIn = false
    var cell: UITableViewCell?
    var dataArray: [AnyObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialiseView()
        
        if let aSize = UIFont(name: NormalFont, size: 16) {
            cell?.textLabel?.font = aSize
        }
        
        if UIScreen.main.sizeType == .iPhone6 {
            if let aSize = UIFont(name: NormalFont, size: 17) {
                cell?.textLabel?.font = aSize
            }
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            if let aSize = UIFont(name: NormalFont, size: 18) {
                cell?.textLabel?.font = aSize
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initialiseView() {
        currentUser = UserProfile.getCurrentUser()
        // NSLog(@"mode is %@", currentUser.loginMode);
        if (currentUser?.loginMode == "fb") {
            isFBUserLoggedIn = true
        }
        let currentLocale = CurrentLocale.sharedInstance()
        //isGlobalLocale = currentLocale?.isGlobal()
        isGlobalLocale = (currentLocale?.isGlobalLocale())!
        isGlobalPayment = (currentLocale?.isGlobalPayment())!
        
        dataArray = []
        
        let notifactionsStr = NSLocalizedString(VC_SETTINGS_PUSHNOTIFICATIONS, comment: "")
        let notificationDict = [
            "title" : notifactionsStr,
            "image" : "PushNotifications.png",
            "position" : "0",
            "onOffSwitch" : "1"
        ]
        dataArray.append(notificationDict as AnyObject)
        
        let timeStr = NSLocalizedString(VC_SETTINGS_TIME_FOR_24_FORMAT, comment: "")
        let timeDict = [
            "title" : timeStr,
            "image" : "24-hours",
            "position" : "1",
            "onOffSwitch" : "1"
        ]
        dataArray.append(timeDict as AnyObject)
        
        if !isFBUserLoggedIn {
            let changePwdStr = NSLocalizedString(VC_SETTINGS_CHANGEPASSWORD, comment: "")
            let chnagePwdDict = [
                "title" : changePwdStr,
                "image" : "Change_password.png",
                "position" : "2",
                "onOffSwitch" : "0"
            ]
            dataArray.append(chnagePwdDict as AnyObject)
            
        }
            if !isGlobalLocale {
                let payWithCashStr = NSLocalizedString(VC_SETTINGS_PAYWITHCASH, comment: "")
                let payWithCashDict = [
                    "title" : payWithCashStr,
                    "image" : "Cash.png",
                    "position" : "3",
                    "onOffSwitch" : "1"
                ]
                dataArray.append(payWithCashDict as AnyObject)
                
                let bankAccStr = NSLocalizedString(VC_SETTINGS_BANKACCOUNTS, comment: "")
                let manageBankActsDict = [
                    "title" : bankAccStr,
                    "image" : "Bank.png",
                    "position" : "4",
                    "onOffSwitch" : "0"
                ]
                dataArray.append(manageBankActsDict as AnyObject)
            }
            
            
            let logoutStr = NSLocalizedString(VC_SETTINGS_LOGOUT, comment: "")
            let logoutDict = [
                "title" : logoutStr,
                "image" : "Logout.png",
                "position" : "5",
                "onOffSwitch" : "0"
            ]
            dataArray.append(logoutDict as AnyObject)
            
            print("logiin mode is \(String(describing: currentUser?.loginMode))")
            tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.01))

        
    }
    
    // MARK:- TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "Cell"
        cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.textLabel?.textColor = UIColor(red: 51.0 / 255.0, green: 51.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
        let dataInfoDict = dataArray[indexPath.row] as? [AnyHashable : Any]
        cell?.accessoryType = .disclosureIndicator
        if let aSize = UIFont(name: NormalFont, size: 16.0) {
            cell?.textLabel?.font = aSize
        }
        cell?.imageView?.image = UIImage(named: dataInfoDict?["image"] as? String ?? "")
        cell?.textLabel?.text = dataInfoDict?["title"] as? String
        let isOnandOffAvailable = dataInfoDict?["onOffSwitch"] as? String
        //let position = Int(dataInfoDict?["position"] ?? 0)
        let position: Int = Int(dataInfoDict?["position"] as! String)!
        if (isOnandOffAvailable == "1") {
            let notificationSwitchView = UISwitch(frame: CGRect.zero)
           // notificationSwitchView.onTintColor = UIColor(red: 231.0 / 255.0, green: 76.0 / 255.0, blue: 58.0 / 255.0, alpha: 1.0)
            notificationSwitchView.onTintColor = UIColor(red: 38.0 / 255.0, green: 98.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
            cell?.accessoryView = notificationSwitchView
            switch position {
            case 0:
                //if PushNotificationManager.sharedInstance().isPushNotificationEnabledFromApp()
                let prefs = UserDefaults.standard
                let isEnabled = prefs.bool(forKey: "enabledPush")
                if(isEnabled){
                    notificationSwitchView.setOn(true, animated: false)
                } else {
                    notificationSwitchView.setOn(false, animated: false)
                }
                notificationSwitchView.addTarget(self, action: #selector(self.pushNotificationValueChanged(_:)), for: .valueChanged)
            case 1:
                addOnOffValue(forTime24HoursFormat: notificationSwitchView)
            case 3:
                addOnOffValue(forPaywithCash: notificationSwitchView)
            default:
                break
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dataInfoDict = dataArray[indexPath.row] as? [AnyHashable : AnyObject]
        //let position = Int(dataInfoDict?["position"] ?? 0)
        let position: Int = Int(dataInfoDict?["position"] as! String)!
        switch position {
        case 0:
            break
        case 1:
            break
        case 2:
            performSegue(withIdentifier: "showChangePassword", sender: self)
            break
        case 3:
            break
        case 4:
            performSegue(withIdentifier: "showBankAccounts", sender: self)
            break
        case 5:
            showAlertFortakingConfirmationForLogout()
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func pushNotificationValueChanged(_ sender: Any?) {
        let userPrefs = UserDefaults.standard
        let switchControl = sender as? UISwitch
        let enableSettings: Bool? = switchControl?.isOn
        messageHandler.showBlockingLoadView(handler: {
            PushNotificationManager.sharedInstance().changeUserPushSettings(enableSettings!, andHandler: { successMsg, errorMsg in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if successMsg != nil {
                        if enableSettings ?? false {
                            self.messageHandler.showSuccessMessage(NSLocalizedString(VC_SETTINGS_ENABLEPUSHMESSAGE, comment: ""))
                            userPrefs.set(true, forKey: "enabledPush")
                        } else {
                            self.messageHandler.showSuccessMessage(NSLocalizedString(VC_SETTINGS_DISABLEPUSHMESSAGE, comment: ""))
                            userPrefs.set(false, forKey: "enabledPush")

                        }
                        userPrefs.synchronize()
                    } else {
                        if enableSettings ?? false {
                            switchControl?.isOn = false
                        } else {
                            switchControl?.isOn = true
                        }
                        self.messageHandler.showErrorMessage(NSLocalizedString(VC_SETTINGS_ERRORMESSAGE, comment: ""))
                    }
                })
            })
        })
    }
    
    func pay(byCashValueChanged sender: Any?) {
        let switchControl = sender as? UISwitch
        let enablePayWithCash: Bool? = switchControl?.isOn
        if enablePayWithCash ?? false {
            UserDefaults.standard.set(false, forKey: "enablePayWithCashDisabled")
        } else {
            UserDefaults.standard.set(true, forKey: "enablePayWithCashDisabled")
        }
    }
    
    
    func time24HoursFormatValueChanged(_ sender: Any?) {
        let switchControl = sender as? UISwitch
        let enable24HoursFormat: Bool? = switchControl?.isOn
        let prefs:UserDefaults = UserDefaults.init(suiteName: "group.com.zify.extension")!
        if enable24HoursFormat ?? false {
            UserDefaults.standard.set(false, forKey: "24hoursFormatEnabled")
            prefs.set(false, forKey: "24hoursFormatEnabled")
            prefs.synchronize()
            AppDelegate.getInstance().setTimeIn24HoursFormat()
        } else {
            UserDefaults.standard.set(true, forKey: "24hoursFormatEnabled")
            prefs.set(true, forKey: "24hoursFormatEnabled")
            prefs.synchronize()
            AppDelegate.getInstance().setTimeIn12HoursFormat()
        }
        
        print("bool value is \(prefs.bool(forKey: "24hoursFormatEnabled"))")
    }

    func showAlertFortakingConfirmationForLogout() {
        let alert = UniversalAlert(title: "Zify", withMessage: NSLocalizedString(USS_LOGOUT_ALERT_MESAGE, comment: ""))
        alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_OK, comment: ""), withAction: { action in
            self.callServiceFroLogout()
        })
        alert?.addButton(BUTTON_CANCEL, withTitle: NSLocalizedString(CMON_GENERIC_CANCEL, comment: ""), withAction: { action in
            
        })
        alert?.show(in: self)
    }

    func callServiceFroLogout() {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: LOGOUTREQUEST), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if (response != nil) {
                        MoEngage.sharedInstance().resetUser()
                        let prefs = UserDefaults.standard
                        prefs.set(false, forKey: "isAddedFreshChat")
                        prefs.synchronize()
                        PushNotificationManager.sharedInstance().deregisterDeviceForPushOnLogout() //Should be called before deleting user since userId is required here.
                        UserProfile.deleteCurrentUser()
                        Freshchat.sharedInstance().resetUser(completion: {
                            //Completion block code here
                        })
                        ChatManager.sharedInstance().disconnectChatOnLogout()
                        OnboardFlowHelper.sharedInstance().clearReminder()
                        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                        
                        FacebookLogin.sharedInstance().closeSessionAndClearSavedTokens()
                        UserDataManager.sharedInstance().setDeepLinkFlagsOnLogout()
                        (UIApplication.shared.delegate as? AppDelegate)?.deleteNsUserDefaults()
                        let previousRootViewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
                        (UIApplication.shared.delegate as? AppDelegate)?.showLoginController()
                        (UIApplication.shared.delegate as? AppDelegate)?.addMoEngageMethodInitializers(with: UIApplication.shared, andWithLaunchOptions: nil)
                        // Allow the view controller to be deallocated
                        previousRootViewController?.dismiss(animated: false) {
                            // Remove the root view in case its still showing
                            (previousRootViewController?.childViewControllers as NSArray?)?.enumerateObjects({ controller, idx, stop in
                                (controller as AnyObject).removeFromParentViewController()
                            })
                            previousRootViewController?.view.removeFromSuperview()
                        }
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    func addOnOffValue(forPaywithCash notificationSwitchView: UISwitch?) {
        let cashPaymentDisabled: Bool = UserDefaults.standard.bool(forKey: "enablePayWithCashDisabled")
        notificationSwitchView?.setOn(!cashPaymentDisabled, animated: false)
        notificationSwitchView?.addTarget(self, action: #selector(self.pay(byCashValueChanged:)), for: .valueChanged)
    }
    
    func addOnOffValue(forTime24HoursFormat notificationSwitchView: UISwitch?) {
        let cashPaymentDisabled: Bool = UserDefaults.standard.bool(forKey: "24hoursFormatEnabled")
        notificationSwitchView?.setOn(!cashPaymentDisabled, animated: false)
        notificationSwitchView?.addTarget(self, action: #selector(self.time24HoursFormatValueChanged(_:)), for: .valueChanged)
    }

    class func createSettingsNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "UserSettings", bundle: Bundle.main)
        let moreOptionsNavController = storyBoard.instantiateViewController(withIdentifier: "settingsNavController") as? UINavigationController
        return moreOptionsNavController
    }
    
}





