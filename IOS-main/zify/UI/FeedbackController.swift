//
//  FeedbackController.swift
//  zify
//
//  Created by Anurag on 19/06/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class FeedbackController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet var tblFeedback: UITableView!

    var arrRiders = ["Sanjay Sahu", "Chanchala Devi", "Sampoonesh babu", "Dwayne Johnson"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblFeedback.rowHeight = UITableViewAutomaticDimension
        self.tblFeedback.estimatedRowHeight = 44.0

        

    }
    
    
    
    // MARK: -  UITableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 || section == 4 {
            return arrRiders.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let sectionLabel = UILabel(frame: CGRect(x: 10, y: 10, width:
            tableView.bounds.size.width, height: 40))
        
        switch(section) {
        case 0 :
            sectionLabel.text =  "Ride Details"
        case 1 :
            sectionLabel.text =  "Money Debited"
        case 4 :
            sectionLabel.text =  "Share Your Feedback"
        default :sectionLabel.text =  ""
        }
        
        sectionLabel.font = UIFont(name: "OpenSans-Bold", size: 18)
        sectionLabel.textColor = UIColor.black
        sectionLabel.sizeToFit()
        headerView.addSubview(sectionLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 || section == 4 {
            return 40.0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellRideDetails : RideDetailCell = (self.tblFeedback.dequeueReusableCell(withIdentifier: "RideDetailCell") as! RideDetailCell)
            //cellRideDetails.lblDistance.text = "992.3847568923 kms"
            return cellRideDetails
        } else if indexPath.section == 1 {
            let cellMoneyRecvdDebtdUser : MoneyRecievedDebitedUserCell = (self.tblFeedback.dequeueReusableCell(withIdentifier: "MoneyRecievedDebitedUserCell") as? MoneyRecievedDebitedUserCell)!
            cellMoneyRecvdDebtdUser.lblUserName.text = arrRiders[indexPath.row]
            return cellMoneyRecvdDebtdUser
        } else if indexPath.section == 2 {
            
            let cellMoneyRecvdDebtdTotal : MoneyRecievedDebitedTotalCell = (self.tblFeedback.dequeueReusableCell(withIdentifier: "MoneyRecievedDebitedTotalCell") as? MoneyRecievedDebitedTotalCell)!
            return cellMoneyRecvdDebtdTotal
            
        } else if indexPath.section == 3 {
            let cellZifyCoinsEarned : ZifyCoinsEarnedCell = (self.tblFeedback.dequeueReusableCell(withIdentifier: "ZifyCoinsEarnedCell") as? ZifyCoinsEarnedCell)!
            return cellZifyCoinsEarned
        } else if indexPath.section == 4 {
            let cellUserRatingCell : UserRatingCell = (self.tblFeedback.dequeueReusableCell(withIdentifier: "UserRatingCell") as? UserRatingCell)!
            cellUserRatingCell.lblUserName.text = arrRiders[indexPath.row]
            return cellUserRatingCell
        }
        
        return UITableViewCell()
        
    }
    
    
    
    class func createFeedbackNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Feedback", bundle: Bundle.main)
        let moreOptionsNavController = storyBoard.instantiateViewController(withIdentifier: "FeedbackNavigationController") as? UINavigationController
        return moreOptionsNavController
    }
}

class RideDetailCell : UITableViewCell {
    
    @IBOutlet weak var lblSourceAddress: UILabel!
    @IBOutlet weak var lblDestinationAddress: UILabel!
    @IBOutlet weak var lblDistanceTitle: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblStartTimeTitle: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTimeTitle: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblDriveId: UILabel!
}

class MoneyRecievedDebitedUserCell : UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblKms: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
}

class MoneyRecievedDebitedTotalCell : UITableViewCell {
    
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
}

class ZifyCoinsEarnedCell : UITableViewCell {
    
    @IBOutlet weak var lblZifyCoinsEarnedTitle: UILabel!
    @IBOutlet weak var lblZifyCoinsCount: UILabel!
    @IBOutlet weak var lblzifyCoinsDescription: UILabel!
}

class UserRatingCell : UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var ratingView: RatingView!
}


