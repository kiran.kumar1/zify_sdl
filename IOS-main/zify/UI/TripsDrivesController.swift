//
//  TripsDrivesController.swift
//  zify
//
//  Created by Anurag on 07/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit
import SDWebImage

class TripsDrivesController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDriveView: UIView!
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var driveButtonView: UIView!

    var tripDrives = [AnyObject]()
    var profileImagePlaceHolder: UIImage?
    var dateTimeFormatter1: DateFormatter?
    var dateFormatter1: DateFormatter?
    var currentDayDate: Date?
    var isUserChatConnected = false
    var isViewInitialised = false
    var localNotificationManager: LocalNotificationManager?
    var cell: UpcomingDriveDetailCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIScreen.main.sizeType == .iPhone5 {
            cell?.source.fontSize = 13
            cell?.destination.fontSize = 13
            cell?.tripTime.fontSize = 12
            cell?.driveId.fontSize = 12
            cell?.onTripLabel.fontSize = 12
            cell?.tapToConnentLbl.fontSize = 10
        } else if UIScreen.main.sizeType == .iPhone6 {
            cell?.source.fontSize = 13
            cell?.destination.fontSize = 13
            cell?.tripTime.fontSize = 13
            cell?.driveId.fontSize = 13
            cell?.onTripLabel.fontSize = 13
            cell?.tapToConnentLbl.fontSize = 10
        } else if UIScreen.main.sizeType == .iPhone6Plus || UIScreen.main.sizeType == .iPhoneXAndXS || UIScreen.main.sizeType == .iPhoneXR || UIScreen.main.sizeType == .iPhoneXSMax {
            cell?.source.fontSize = 13
            cell?.destination.fontSize = 13
            cell?.tripTime.fontSize = 14
            cell?.driveId.fontSize = 14
            cell?.onTripLabel.fontSize = 14
            cell?.tapToConnentLbl.fontSize = 11
        }
        
        tripDrives = []
        profileImagePlaceHolder = UIImage(named: "placeholder_profile_image")
        dateTimeFormatter1 = DateFormatter()
        dateTimeFormatter1?.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateTimeFormatter1?.locale = locale as Locale?
        dateFormatter1 = DateFormatter()
        dateFormatter1?.dateFormat = "dd MMM yyyy"
        currentDayDate = getCurrentDayDate()
        isViewInitialised = false
        isUserChatConnected = ChatManager.sharedInstance().isUserChatConnected()
        localNotificationManager = LocalNotificationManager.sharedInstance()
        tableView.estimatedRowHeight = 180.0
        tableView.rowHeight = UITableViewAutomaticDimension

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isViewInitialised = false;
        self.fetchTheServiceDetailsAndLoad();
    }
    
    func fetchTheServiceDetailsAndLoad(){
        if !isViewInitialised {
            tableView.isHidden = false
            noDriveView.isHidden = true
            messageHandler.showBlockingLoadView(handler: {
                ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: UPCOMINGDRIVESREQUEST), withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            if let anObject = response?.responseObject as? [Any] {
                                self.tripDrives = anObject as [AnyObject]
                            }
                            self.handleTripDrivesResponse()
                            self.localNotificationManager?.cancelNotifications(with: DRIVEALARM)
                            self.localNotificationManager?.handleTripDrivesNotifications(self.tripDrives as [AnyObject])
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        }
                    })
                })
            })
            isViewInitialised = true
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        messageHandler.dismissErrorView()
        messageHandler.dismissSuccessView()
    }
    
    // MARK:- TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if tripDrives.count == 0 {
            return 0
        }
        return tripDrives.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cell = self.tableView.dequeueReusableCell(withIdentifier: "upcomingDriveDetailCell") as? UpcomingDriveDetailCell
        
        
        cell?.tapToConnentLbl.isHidden = true
        cell?.srcImageView.backgroundColor = UIColor.white
        cell?.srcImageView.layer.cornerRadius = (cell?.srcImageView.frameHeight)! / 2
      //  cell?.destImageView.image = AppDelegate.getInstance().getNewImage(withOldImage: UIImage(named: "loc_123.png"), withNewColor: UIColor.gray)
        let drive = tripDrives[indexPath.section] as? TripDrive
        cell?.source.text = drive?.srcAddress
        cell?.destination.text = drive?.destAddress
        let departureTime: Date? = dateTimeFormatter1?.date(from: drive?.tripTime ?? "")
        let dateStr = getDateString(fromDepartureTime: departureTime)
        if let aTime = departureTime {
            cell?.tripTime.text = "\((dateStr)!), \(AppDelegate.getInstance().timeFormatter.string(from: aTime))"
        }
        if (NSLocalizedString(CMON_GENERIC_TODAY, comment: "") == dateStr) {
            //  cell.tripDate.textColor = [UIColor colorWithRed:0.0 green:(120.0/255.0) blue:0.0 alpha:1.0];
            cell?.tripTime.textColor = UIColor(red: 0.0, green: 120.0 / 255.0, blue: 0.0, alpha: 1.0)
        } else {
            // cell.tripDate.textColor = [UIColor lightGrayColor];
            cell?.tripTime.textColor = UIColor.lightGray
        }
        //VC_TRIPSHISTORY_DRIVEID
        cell?.driveId.text = String(format: NSLocalizedString(VC_TRIPSHISTORY_DRIVEID, comment: "Drive Id: {Drive ID}"), "\((drive?.driveId)!)")
        let cellImagesCount = cell?.riderImages.count
        
        for index in 0..<cellImagesCount! {
            let riderImage = cell?.riderImages[index]
            let riderImageButton = cell?.riderImageButtons[index]
            riderImage?.isHidden = true
            riderImageButton?.isHidden = true
        }
        
       
        var showContactButton = false
        
        for (idx, obj) in (drive?.driveRiders.enumerated())! {
            
            let riderDetail = obj as? TripDriveRiderDetail
            let riderProfile: RiderProfile? = riderDetail?.riderProfile
            let riderImageView: UIImageView = (cell?.riderImages[idx])!
            let riderImageButton = cell?.riderImageButtons[idx]
            cell?.tapToConnentLbl.isHidden = false
            
            let urlStr = URL(string: riderProfile?.profileImageURL ?? "")
            
            riderImageView.sd_setImage(with: urlStr) { (image, error, cacheType, imgUrl) in
                riderImageView.isHidden = false
            }
            riderImageView.layer.cornerRadius = (riderImageView.frame.size.width ) / 2
            riderImageView.clipsToBounds = true
            riderImageButton?.isHidden = false
            if (riderProfile?.chatProfile != nil || riderProfile?.callId != nil) {
                showContactButton = true
            }
        }
        
        if showContactButton {
            cell?.contactButton.isHidden = false
        } else {
            cell?.contactButton.isHidden = true
        }
        if ("RUNNING" == drive?.status) {
            cell?.cancelButton.isHidden = true
            cell?.cancelButtonSeparator.isHidden = true
            cell?.onTripLabel.isHidden = false
            cell?.shareBtn.isHidden = true
            cell?.shareButtonSeparator.isHidden = true
        } else {
            cell?.cancelButton.isHidden = false
            cell?.cancelButtonSeparator.isHidden = false
            cell?.onTripLabel.isHidden = true
            cell?.shareBtn.isHidden = false
            cell?.shareButtonSeparator.isHidden = false
        }
        cell?.driveRiders = drive?.driveRiders as [AnyObject]?
        cell?.contactButton.isHidden = true
        
        cell?.shareButtonSeparator.backgroundColor = cell?.cancelButtonSeparator.backgroundColor
        addShadow(to: cell?.contentView)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == tripDrives.count - 1 {
            return 10.0
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func showRouteInfo(_ sender: UIButton) {
        let indexPath: IndexPath = tableView.indexPath(for: getTableCell(sender)!)!
        let tripDrive = tripDrives[indexPath.section] as? TripDrive
        RouteInfoView.showRouteInfo(on: navigationController?.view, with: tripDrive?.routeDetail)
        //RouteInfoView.showRouteInfo(on: navigationController?.view, withTripRouteDetail: tripDrive?.routeDetail)
    }
    
    @IBAction func showUserContactDetails(_ sender: UIButton) {
        let contactUsersArray: NSMutableArray = []
        let indexPath: IndexPath? = tableView.indexPath(for: getTableCell(sender)!)
        let tripDrive = tripDrives[indexPath?.section ?? 0] as? TripDrive
        
        for (idx, obj) in (tripDrive?.driveRiders.enumerated())! {
            let riderDetail = obj as? TripDriveRiderDetail
            let riderProfile: RiderProfile? = riderDetail?.riderProfile
            
            if ( riderProfile?.chatProfile != nil || riderProfile?.callId != nil ){
                let userContactDetail = UserContactDetail(chatIdentifier:riderProfile?.chatProfile.chatUserId, andCallIdentifier: riderProfile?.callId, andFirstName: riderProfile?.firstName, andLastName: riderProfile?.lastName, andImageUrl: riderProfile?.profileImageURL)
                contactUsersArray.add(userContactDetail!)
            }
        }
        
        ContactUserListController.createAndOpenContactListController(navigationController, withUserContacts: contactUsersArray as! [Any])
    }
    
    @IBAction func showUserContactDetailsBased(onSelectedUser sender: UIButton) {
        let indexOfSelectedUser: Int = sender.tag
        let contactUsersArray: NSMutableArray = []
        let indexPath: IndexPath? = tableView.indexPath(for: getTableCell(sender)!)
        let tripDrive = tripDrives[indexPath?.section ?? 0] as? TripDrive
        let riderDetail = tripDrive?.driveRiders[indexOfSelectedUser] as? TripDriveRiderDetail
        let riderProfile: RiderProfile? = riderDetail?.riderProfile
        isUserChatConnected = ChatManager.sharedInstance().isUserChatConnected()
        print("isUserChatConnected is \(isUserChatConnected)")
        if let aProfile = riderProfile?.chatProfile {
            print("chatProfile is \(aProfile)")
        }
        if let anId = riderProfile?.callId {
            print("callId is \(anId)")
        }
        
        if (riderProfile?.chatProfile != nil || riderProfile?.callId != nil) {
            if let userContactDetail = UserContactDetail(chatIdentifier: riderProfile?.chatProfile.chatUserId, andCallIdentifier: riderProfile?.callId, andFirstName: riderProfile?.firstName, andLastName: riderProfile?.lastName, andImageUrl: riderProfile?.profileImageURL){
                contactUsersArray.add(userContactDetail)
            }
        }
        if contactUsersArray.count > 0 {
            ContactUserListController.createAndOpenContactListController(navigationController, withUserContacts: contactUsersArray as [AnyObject])
        } else {
            //  Need to show alert message
        }
    }
    
    @IBAction func cancelDrive(_ sender: UIButton) {
        let indexPath: IndexPath? = tableView.indexPath(for: getTableCell(sender)!)
        let tripDrive = tripDrives[indexPath?.section ?? 0] as? TripDrive
        if tripDrive?.driveRiders.count != 0 {
            let alert = UniversalAlert(title: NSLocalizedString(VC_TRIPDRIVES_CANCELALERTTITLE, comment: ""), withMessage: NSLocalizedString(VC_TRIPDRIVES_CANCELALERMESSAGE, comment: ""))
            alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_OK, comment: ""), withAction: { action in
                self.cancelDrive(for: tripDrive, andIndexPath: indexPath)
            })
            alert?.addButton(BUTTON_CANCEL, withTitle: NSLocalizedString(CMON_GENERIC_CANCEL, comment: ""), withAction: { action in
                
            })
            alert?.show(in: self)
        } else {
            let alert = UniversalAlert(title: NSLocalizedString(VC_TRIPDRIVES_CANCELALERTTITLE, comment: ""), withMessage: NSLocalizedString(VC_TRIPDRIVE_CANCELALERMESSAGE, comment: ""))
            alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_OK, comment: ""), withAction: { action in
                self.cancelDrive(for: tripDrive, andIndexPath: indexPath)
            })
            alert?.addButton(BUTTON_CANCEL, withTitle: NSLocalizedString(CMON_GENERIC_CANCEL, comment: ""), withAction: { action in
                
            })
            alert?.show(in: self)
        }
    }

    func cancelDrive(for tripDrive: TripDrive?, andIndexPath indexPath: IndexPath?) {
        messageHandler.showBlockingLoadView(handler: {
            let eventDict = ["source_address":tripDrive?.srcAddress ?? "", "destination_address":tripDrive?.destAddress ?? "", "trip_Time":tripDrive?.tripTime ?? ""] as [String:Any]
            MoEngageEventsClass.callDriveDeleteEvent(withDataDict: eventDict)
            
            ServerInterface.sharedInstance().getResponse(RideDriveActionRequest(requestType: CANCELDRIVE, andDriveId: tripDrive?.driveId), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.tripDrives.remove(at: indexPath?.section ?? 0)
                        self.handleTripDrivesResponse()
                        self.localNotificationManager?.cancelNotification(withIdentifier: "\((tripDrive?.driveId)!)", andType: DRIVEALARM)
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    func handleTripDrivesResponse() {
        if tripDrives.count == 0 {
            tableView.isHidden = true
            noDriveView.isHidden = false
        } else {
            tableView.isHidden = false
            noDriveView.isHidden = true
            tableView.reloadData()
        }
    }
    
    @IBAction func shareDrive(_ sender: UIButton) {
        let indexPath: IndexPath? = tableView.indexPath(for: getTableCell(sender)!)
        let tripDrive = tripDrives[indexPath?.section ?? 0] as? TripDrive
        //UserGetDriveDeepLinkUrlRequest
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(UserGetDriveDeepLinkUrlRequest.init(driveId: tripDrive?.driveId, withDriverId: tripDrive?.driverId), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let messageCode:Int = Int((response?.messageCode)!)!
                        if messageCode == 1 {
                            
                            var resultDict = response?.responseObject as! Dictionary<String, AnyObject>
                            let deepLinkUrl = resultDict["responseText"] as! String
                            AppDelegate.getInstance().shareDriveDeepLinkId(withFriends: deepLinkUrl, fromScreen: self)
                        }
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as? String)
                    }
                })
            })
        })
    }

    
    func getTableCell(_ button: Any?) -> UITableViewCell? {
        var button = button
        while !(button is UITableViewCell) {
            button = (button as AnyObject).superview!//superview()
        }
        return button as? UITableViewCell
    }
    
//    func getTableCell(_ button: AnyObject) -> UITableViewCell? {
//        var btn : AnyObject = button
//        while !(btn is UITableViewCell) {
//            //btn = (btn as AnyObject).superview()
//            btn = btn.superview as AnyObject
//        }
//        return button as? UITableViewCell
//    }
    
    func getCurrentDayDate() -> Date? {
        let calendar = Calendar.init(identifier: .gregorian) //Calendar(calendarIdentifier: .gregorian)
        var components: DateComponents? = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: Date())
        components?.hour = 0
        components?.minute = 0
        components?.second = 0
        if let aComponents = components {
            return calendar.date(from: aComponents)
        }
        return nil
    }
    
    func getDateString(fromDepartureTime date: Date?) -> String? {
        if(date == nil){
            return "";
        }
        let calendar = Calendar.init(identifier: .gregorian) //Calendar(calendarIdentifier: .gregorian)
        var components: DateComponents? = calendar.dateComponents([.day], from: currentDayDate!, to: date!)
        let difference: Int? = components?.day
        if difference == 0 {
            return NSLocalizedString(CMON_GENERIC_TODAY, comment: "")
        } else if difference == 1 {
            return NSLocalizedString(CMON_GENERIC_TOMORROW, comment: "")
        } else {
            if date != nil {
                if let aDate = date {
                    return dateFormatter1?.string(from: aDate)
                }
                return nil
            }
            return nil
        }
    }
    
    func addShadow(to v: UIView?) {
        v?.layer.shadowColor = UIColor.lightGray.cgColor
        v?.layer.shadowOpacity = 0.8
        v?.layer.shadowRadius = 3.0
        // [v.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        v?.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }

}

class UpcomingDriveDetailCell : UITableViewCell {
    @IBOutlet weak var srcImageView: UIImageView!
    @IBOutlet weak var destImageView: UIImageView!
    @IBOutlet weak var source: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var tripDate: UILabel!
    @IBOutlet weak var tripTime: UILabel!
    @IBOutlet weak var driveId: UILabel!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelButtonSeparator: UIView!
    @IBOutlet weak var onTripLabel: UILabel!
    @IBOutlet weak var tapToConnentLbl: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var shareButtonSeparator: UIView!
    @IBOutlet var riderImages: [UIImageView]!
    @IBOutlet var riderImageButtons: [UIButton]!
    var driveRiders: [AnyObject]?

    
}
