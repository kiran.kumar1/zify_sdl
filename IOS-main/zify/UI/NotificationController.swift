//
//  NotificationController.swift
//  zify
//
//  Created by Anurag on 04/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class NotificationController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var noNotificationsView: UIView!
    @IBOutlet weak var tblViewNotifications: UITableView!

    
    var currentUser: UserProfile?
    var dataArray: [AnyObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = NSLocalizedString(CS_NOTIFICATIONS_NAV_TITILE, comment: "")
        self.setBoolInUserPrefences(false)
        navigationController?.isNavigationBarHidden = false
        tblViewNotifications.isMultipleTouchEnabled = false
        noNotificationsView.isHidden = true
        dataArray = []
        tblViewNotifications.reloadData()
        getDateFromUserDetails()
        tblViewNotifications.estimatedRowHeight = 89
        tblViewNotifications.rowHeight = UITableViewAutomaticDimension

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getDateFromUserDetails() {
        
        currentUser = UserProfile.getCurrentUser()
        
        let isIdCardUploaded:Int = (currentUser?.userDocuments.isIdCardDocUploaded as? Int)!

        let isVehicleImgUploaded:Int = (currentUser?.userDocuments.isVehicleImgUploaded as? Int)!

      //  let isIdCardUploadedStr = "\(String(describing: currentUser?.userDocuments.isIdCardDocUploaded))"
      //  let isVehicleImgUploadedStr = "\(String(describing: currentUser?.userDocuments.isVehicleImgUploaded))"
        
        if currentUser!.dob == nil || currentUser!.dob.count == 0 || currentUser!.gender == nil || currentUser!.gender.count == 0 || currentUser!.companyName == nil || currentUser!.companyName.count == 0 || currentUser!.companyEmail == nil || currentUser!.companyEmail.count == 0 || currentUser!.bloodGroup == nil || currentUser!.bloodGroup.count == 0 || currentUser!.emergencyContact == nil || currentUser!.emergencyContact.count == 0 {
            createDictionaryAndAddit(toDataArray: 3)
        }
        
        if currentUser!.userPreferences != nil && (currentUser!.userPreferences.userMode == "DRIVER") && (isVehicleImgUploaded == 0) {
            createDictionaryAndAddit(toDataArray: 4)
        }
        
        if (isIdCardUploaded == 0) {
            createDictionaryAndAddit(toDataArray: 0)
        }
        if !(currentUser!.userPreferences != nil) {
            createDictionaryAndAddit(toDataArray: 1)
        }
        if (currentUser!.profileImgUrl == nil) || currentUser!.profileImgUrl.count == 0 {
            createDictionaryAndAddit(toDataArray: 2)
        }
        
    
      
        if dataArray.count == 0 {
            tblViewNotifications.isHidden = true
            noNotificationsView.isHidden = false
        } else {
            tblViewNotifications.isHidden = false
            noNotificationsView.isHidden = true
            tblViewNotifications.reloadData()
        }
    }

    func createDictionaryAndAddit(toDataArray index: Int) {
        var title: String = ""
        var subTitle: String = ""
        var imageName: String = ""
        var position: String = ""
        switch index {
        case 0:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_UPLOAD_GOVT_ID, comment: "")
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_UPLOAD_GOVT_ID, comment: "")
            imageName = "ID_Card"
            position = "0"
        case 1:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_TP, comment: "")
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_TP, comment: "")
            imageName = "Travel_Preferences"
            position = "1"
        case 2:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_UPLOAD_PROFILE_PHOTO, comment: "")
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_UPLOAD_PROFILE_PHOTO, comment: "")
            imageName = "ProfileNotifi"
            position = "2"
        case 3:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_COMPLETE_PROFILE, comment: "")
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_COMPLETE_PROFILE, comment: "")
            imageName = "ProfileCompleteN"
            position = "3"
        case 4:
            title = NSLocalizedString(CS_NOTIFICATIONS_LBL_FILL_CAR_DETAILS, comment: "")
            subTitle = NSLocalizedString(CS_NOTIFICATIONS_SUB_LBL_FILL_CAR_DETAILS, comment: "")
            imageName = "CarTP"
            position = "4"
        default:
            break
        }
        let infoDict = [
            "title" : title,
            "subTitle" : subTitle,
            "imageName" : imageName,
            "position" : position
        ]
        dataArray.append(infoDict as AnyObject)
        
        
    }

    // MARK:- TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // NotificationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell"];
        
        let cellidentifier = "NotificationTableCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellidentifier) as? NotificationTableCell
        if cell == nil {
            //cell = NotificationViewCell(style: .default, reuseIdentifier: cellidentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        let infoDict = dataArray[indexPath.row] as? [AnyHashable : Any]
        //cell?.dataForCell = infoDict
        
        cell?.imgNotification.image = UIImage(named: infoDict!["imageName"] as? String ?? "")
        cell?.lblTitle.text = infoDict?["title"] as? String
        cell?.lblContent.text = infoDict?["subTitle"] as? String
        cell?.lblTitle.sizeToFit()
        cell?.lblContent.sizeToFit()
        cell?.viewMain.dropShadow(isShowShadow: true)
        cell?.viewMain.backgroundColor = UIColor.white
        

        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let infoDict = dataArray[indexPath.row] as? [AnyHashable : Any]
        move(toAppropriateScreen: infoDict)
        // NSLog(@"selected rpw is %d", indexPath.row);
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    
    func move(toAppropriateScreen infoDict: [AnyHashable : Any]?) {
        let positionSytr = infoDict?["position"] as? String
        var position: Int = Int(positionSytr!)!
        //var position = Int(truncating: positionSytr ?? "") ?? 0
        if (positionSytr?.count ?? 0) <= 0 {
            position = -1
        }
        switch position {
        case 0:
            moveToUploadGovtIDscreen()
        case 1:
            moveToTPScreen()
        case 2:
            moveToUploadProfilePhoto()
        case 3:
            moveToProfileScreen()
        case 4:
            moveToFillCarDetails()
        default:
            break
        }
    }
    
    func moveToUploadGovtIDscreen() {
        self.setBoolInUserPrefences(true)
        let sb = UIStoryboard(name: "IDCard", bundle: nil)
        let vc: UIViewController = sb.instantiateViewController(withIdentifier: "UserIDCardController")
        navigationController?.pushViewController(vc, animated: true)
        //phase - 1
        
    }
    
    func moveToTPScreen() {
        setBoolInUserPrefences(true)
        let addressPreferencesController = UserAddressPreferencesController.create()
        addressPreferencesController?.showPageIndicator = false
        addressPreferencesController?.isUpgradeFlow = false
        navigationController?.pushViewController(addressPreferencesController!, animated: true)
    }
    
    func moveToUploadProfilePhoto() {
        
        moveToProfileScreen()
    }
    
    func moveToProfileScreen() {
        setBoolInUserPrefences(true)
        let storyBoard = UIStoryboard(name: "UserProfile", bundle: Bundle.main)
        let viewController: UIViewController = storyBoard.instantiateViewController(withIdentifier: "editprofile")
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func moveToFillCarDetails() {
        let vehicleScreen = VehicleTextController.createVehcileTextController() as? VehicleTextController
        if let aScreen = vehicleScreen {
            navigationController?.pushViewController(aScreen, animated: true)
        }
    }
    
    func setBoolInUserPrefences(_ isForPush: Bool) {
        let defs = UserDefaults.standard
        defs.set(isForPush, forKey: "forPush")
        defs.synchronize()
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }
    
    class func createNotificationsNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Commons", bundle: Bundle.main)
        let walletNavController = storyBoard.instantiateViewController(withIdentifier: "notificationsNavController") as? UINavigationController
        return walletNavController
    }

    
}

class NotificationTableCell : UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var imgNotification: UIImageView!
    @IBOutlet weak var viewMain: UIView!

}


