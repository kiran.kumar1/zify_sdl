//
//  TripsRidesController.swift
//  zify
//
//  Created by Anurag on 07/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class TripsRidesController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noRideView: UIView!
    @IBOutlet weak var messageHandler: MessageHandler!
    @IBOutlet weak var rideButtonView: UIView!
    var isViewInitialised = false
    
    var tripRides = [AnyObject]()
    var profileImagePlaceHolder: UIImage?
    var dateTimeFormatter1: DateFormatter?
    var dateFormatter1: DateFormatter?
    var currentDayDate: Date?
    var isUserChatConnected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tripRides = []
        profileImagePlaceHolder = UIImage(named: "placeholder_profile_image")
        dateTimeFormatter1 = DateFormatter()
        dateTimeFormatter1?.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateTimeFormatter1?.locale = locale as Locale?
        dateFormatter1 = DateFormatter()
        dateFormatter1?.dateFormat = "dd MMM yyyy"
        
        currentDayDate = getCurrentDayDate()
        isUserChatConnected = ChatManager.sharedInstance().isUserChatConnected()
        isViewInitialised = false
        tableView.estimatedRowHeight = 225.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isViewInitialised = false;
        fetchTheServiceDetailsAndLoad()
        
    }
    
    func fetchTheServiceDetailsAndLoad() {
        if !isViewInitialised {
            tableView.isHidden = false
            noRideView.isHidden = true
            messageHandler.showBlockingLoadView(handler: {
                ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: UPCOMINGRIDESREQUEST), withHandler: { response, error in
                    self.messageHandler.dismissBlockingLoadView(handler: {
                        if response != nil {
                            if let anObject = response?.responseObject as? [Any] {
                                self.tripRides = anObject as [AnyObject]
                            }
                            self.handleTripRidesResponse()
                        } else {
                            self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                        }
                    })
                })
            })
            isViewInitialised = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        messageHandler.dismissErrorView()
        messageHandler.dismissSuccessView()
    }

    // MARK:- Table View Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if tripRides.count == 0 {
            return 0
        }
        return tripRides.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellidentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellidentifier) as? UpcomingRideDetailCell
        if cell == nil {
            cell = UpcomingRideDetailCell(style: .default, reuseIdentifier: cellidentifier)
        }
        let ride = tripRides[indexPath.section] as? TripRide
        cell?.setDataFor(ride)
        
        cell?.cancelButton.addTarget(self, action: #selector(self.cancelRide(_:)), for: .touchUpInside)
        cell?.viewRouteButton.addTarget(self, action: #selector(self.showRouteInfo(_:)), for: .touchUpInside)
        cell?.contactButton.addTarget(self, action: #selector(self.showCall(_:)), for: .touchUpInside)
        cell?.chatButton.addTarget(self, action: #selector(self.showChat(_:)), for: .touchUpInside)
        return cell!
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var isChatOprionAvailable = false
        var isCallOptionAvailable = false
        let ride = tripRides[indexPath.section] as? TripRide
        let isUserChatConnected = ChatManager.sharedInstance().isUserChatConnected()
        if  ride?.driverDetail.chatProfile != nil {
            isChatOprionAvailable = true
        }
        if ride?.driverDetail.callId != nil {
            isCallOptionAvailable = true
        }
        if isChatOprionAvailable || isCallOptionAvailable {
            return 260.0
        }
        return 220.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }

    // MARK:- Actions
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func showUserContactDetails(_ sender: Any) {
        let indexPath: IndexPath? = tableView.indexPath(for: getTableViewCell(sender)!)
        let tripRide = tripRides[indexPath?.section ?? 0] as? TripRide
        var contactUsersArray: [AnyHashable] = []
        let driverDetail: DriverDetail? = tripRide?.driverDetail
        var callId: String = ""
        if !("PENDING" == tripRide?.status) {
            callId = driverDetail?.callId ?? ""
        }
        let userContactDetail = UserContactDetail(chatIdentifier: driverDetail?.chatProfile != nil && isUserChatConnected ? driverDetail?.chatProfile.chatUserId : nil, andCallIdentifier: callId, andFirstName: driverDetail?.firstName, andLastName: driverDetail?.lastName, andImageUrl: driverDetail?.profileImgUrl)
        contactUsersArray.append(userContactDetail!)
        ContactUserListController.createAndOpenContactListController(navigationController, withUserContacts: contactUsersArray)
    }

    func showChat(_ sender: UIButton?) {
        let indexPath: IndexPath? = tableView.indexPath(for: getTableViewCell(sender)!)
        
        let tripRide = tripRides[indexPath?.section ?? 0] as? TripRide
        let driverDetail: DriverDetail? = tripRide?.driverDetail
        if((driverDetail?.chatProfile.chatUserId) == nil){
            UIAlertController.showError(withMessage: "Sorry!. Unable to chat right now. Please try again.", inViewController: self)
            return;
        }
        let userContactDetail = UserContactDetail(chatIdentifier: driverDetail?.chatProfile.chatUserId, andCallIdentifier: driverDetail?.callId, andFirstName: driverDetail?.firstName, andLastName: driverDetail?.lastName, andImageUrl: driverDetail?.profileImgUrl)
        let chatNavController: UINavigationController? = ChatUserListController.createChatNavController()
        let chatUserListContoller = chatNavController?.topViewController as? ChatUserListController
        chatUserListContoller?.receivingUser = userContactDetail
        if let aController = chatNavController {
            present(aController, animated: true)
        }
        
    }
    
    func showCall(_ sender: UIButton?) {
        
        let indexPath: IndexPath? = tableView.indexPath(for: getTableViewCell(sender)!)
        
        let tripRide = tripRides[indexPath?.section ?? 0] as? TripRide
        let driverDetail: DriverDetail? = tripRide?.driverDetail
        var callId: String
        callId = driverDetail?.callId ?? ""
        let contactDetail = UserContactDetail(chatIdentifier: driverDetail?.chatProfile != nil && isUserChatConnected ? driverDetail?.chatProfile.chatUserId : nil, andCallIdentifier: callId, andFirstName: driverDetail?.firstName, andLastName: driverDetail?.lastName, andImageUrl: driverDetail?.profileImgUrl)
        
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(UserCallRequest(requestType: INITIATECALL, andCalleeId: contactDetail?.callId), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil && !(response?.responseObject is NSNull) {
                        AppUtilites.openCallApp(withNumber: response?.responseObject as! String)
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }

    @IBAction func showRouteInfo(_ sender: UIButton) {
        let indexPath: IndexPath? = tableView.indexPath(for: getTableViewCell(sender)!)
        let tripRide = tripRides[indexPath?.section ?? 0] as? TripRide
        RouteInfoView.showRouteInfoOnView(fromUpcomingRides: navigationController?.view, withTripDetail: tripRide)
       // RouteInfoView.showRouteInfo(on: navigationController?.view, with: tripRide?.routeDetail)
    }
    
    @IBAction func cancelRide(_ sender: UIButton) {
        let indexPath: IndexPath? = tableView.indexPath(for: getTableViewCell(sender)!)
        showAlertFortakingConfirmation(forRide: indexPath)
    }
    
    func handleTripRidesResponse() {
        if tripRides.count == 0 {
            tableView.isHidden = true
            noRideView.isHidden = false
            noRideView.frame = CGRect(x: noRideView.frame.origin.x, y: noRideView.frame.origin.y, width: view.frame.size.width, height: view.frame.size.height)
            //self.rideButtonView.frame = CGRectMake(self.rideButtonView.frame.origin.x,self.noRideView.frame.size.height - self.rideButtonView.frame.size.height - 50,self.rideButtonView.frame.size.width, self.rideButtonView.frame.size.height);
        } else {
            tableView.isHidden = false
            noRideView.isHidden = true
        }
        tableView.reloadData()
    }

    func getTableViewCell(_ button: Any?) -> UITableViewCell? {
        var button = button
        while !(button is UITableViewCell) {
            button = (button as AnyObject).superview!
        }
        return button as? UITableViewCell
    }

    func getCurrentDayDate() -> Date? {
        let calendar = Calendar.init(identifier: .gregorian)
        var components: DateComponents? = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: Date())
        components?.hour = 0
        components?.minute = 0
        components?.second = 0
        if let aComponents = components {
            return calendar.date(from: aComponents)
        }
        return nil
    }
    
    func getDateString(fromDepartureTime date: Date?) -> String? {
        let calendar = Calendar.init(identifier: .gregorian)
        var components: DateComponents? = calendar.dateComponents([.day], from: currentDayDate!, to: date!)
        let difference: Int? = components?.day
        if difference == 0 {
            return NSLocalizedString(CMON_GENERIC_TODAY, comment: "")
        } else if difference == 1 {
            return NSLocalizedString(CMON_GENERIC_TOMORROW, comment: "")
        } else {
            if date != nil {
                if let aDate = date {
                    return dateFormatter1?.string(from: aDate)
                }
                return nil
            }
            return nil
        }
    }
    
    func showAlertFortakingConfirmation(forRide indexPath: IndexPath?) {
        let alert = UniversalAlert(title: NSLocalizedString(VC_TRIPRIDES_CANCELALERTTITLE, comment: ""), withMessage: NSLocalizedString(VC_TRIPRIDES_CANCELALERMESSAGE, comment: ""))
        alert?.addButton(BUTTON_OK, withTitle: NSLocalizedString(CMON_GENERIC_OK, comment: ""), withAction: { action in
            self.callService(forDeleteRide: indexPath)
        })
        alert?.addButton(BUTTON_CANCEL, withTitle: NSLocalizedString(CMON_GENERIC_CANCEL, comment: ""), withAction: { action in
            
        })
        alert?.show(in: self)
    }

    func callService(forDeleteRide indexPath: IndexPath?) {
        let tripRide = tripRides[indexPath?.section ?? 0] as? TripRide
        let eventDict = ["CarOwner_Firstname":tripRide?.driverDetail.firstName ?? "", "CarOwner_Lastname":tripRide?.driverDetail.lastName ?? "", "source_address":tripRide?.srcAddress ?? "", "destination_address":tripRide?.destAddress ?? "", "trip_Time":tripRide?.tripTime ?? ""] as [String:Any]
        MoEngageEventsClass.callRideCancelEvent(withDataDict: eventDict)
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(RideDriveActionRequest(requestType: CANCELRIDE, andRideId: tripRide?.rideId, andDriveId: tripRide?.driveId, andDriverId: tripRide?.driverId, andRouteId: tripRide?.routeId), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.tripRides.remove(at: indexPath?.section ?? 0)
                        self.handleTripRidesResponse()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }

}






