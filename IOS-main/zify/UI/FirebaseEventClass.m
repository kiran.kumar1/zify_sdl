//
//  FirebaseEventClass.m
//  zify
//
//  Created by Anurag S Rathor on 06/01/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "FirebaseEventClass.h"

@import Firebase;


@implementation FirebaseEventClass
+(void)storeRideSearchEventFromSearchScreen{
    [FIRAnalytics logEventWithName:kFIREventSelectContent
    parameters:@{ kFIRParameterItemID:[NSString stringWithFormat:@"id-%@", @"Search Ride From Guest Screen"],
    kFIRParameterItemName:@"Search Ride From Guest Screen",
    kFIRParameterContentType:@"Event"}];
}

@end
