//
//  TravelPrefStep1.swift
//  zify
//
//  Created by Anurag S Rathor on 17/11/17.
//  Copyright © 2017 zify. All rights reserved.
//

import UIKit


class TravelPrefStep1: BaseViewController{

   
    var ownerView:UIView = UIView()
    var riderView:UIView = UIView()
    var isAnyOptionSelected:Bool = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.getInstance().paramsDict = NSMutableDictionary.init()
        AppDelegate.getInstance().homeAddress = nil;
        AppDelegate.getInstance().officeAddress = nil;
        AppDelegate.getInstance().homeToOffDriveRoute = nil;
        AppDelegate.getInstance().oFFToHomeDriveRoute = nil;
        self.btnNext.addTarget(self, action: #selector(btnNextTapped), for: .touchUpInside)
        self.headerTitleLbl.text = NSLocalizedString(TP_SCREEN1_TITLE, comment: "")
        self.headerSubTitleLbl.text = NSLocalizedString(TP_SCREEN1_SUB_TITLE, comment: "")
        self.progressView.setProgress(0.25, animated: true)

        
        
        let remainingHeight:CGFloat = (self.bgView.frameHeight - self.headerSubTitleLbl.frameMaxY - self.bottomView.frameHeight)/2 - 1;
        ownerView = createViewForRiderOrOwner(imageName: "icn_onboard_traveltype@3x.png", withTitle: NSLocalizedString(TP_SCREEN1_CAR_TITLE, comment: ""), withFrame:CGRect(x: 0, y: self.headerSubTitleLbl.frameMaxY, width: AppDelegate.screen_WIDTH(), height: remainingHeight),withTag: 45)
        self.bgView.addSubview(ownerView);
        
        let lineSeparator:UILabel = UILabel();
        lineSeparator.frame = CGRect(x:0, y:ownerView.frameMaxY, width:AppDelegate.screen_WIDTH(), height:2);
        lineSeparator.backgroundColor = UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        self.bgView.addSubview(lineSeparator);
        
        riderView = createViewForRiderOrOwner(imageName: "pedestrian-walking.png", withTitle: NSLocalizedString(TP_SCREEN1_RIDE_TITLE, comment: ""), withFrame: ownerView.frame,withTag: 55)
        riderView.frameY = lineSeparator.frameMaxY
        self.bgView.addSubview(riderView);
        

        // Do any additional setup after loading the view.
    }
    func createViewForRiderOrOwner(imageName:String, withTitle:String, withFrame:CGRect, withTag:Int) -> UIView {
        
        let mainview:UIView = UIView.init(frame: withFrame)
        mainview.backgroundColor = UIColor.clear;
        
        let padX:CGFloat = 30;
        let padY:CGFloat = 20;
        let view:UIView = UIView.init(frame: CGRect(x: padX, y: padY, width:(AppDelegate.screen_WIDTH() - 2*padX), height: mainview.frameHeight - 2*padY));
        view.backgroundColor = UIColor.init(red: 234.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0);
        mainview.addSubview(view);

        
        let imageLogo:UIImageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        imageLogo.image = UIImage.init(named: imageName)
        view.addSubview(imageLogo);
        imageLogo.center = CGPoint(x:view.frameWidth/2, y:view.frameHeight/2);

        var fontSize:CGFloat = 16;
        if(AppDelegate.screen_HEIGHT() > 568){
            fontSize = 18;
        }
        let label: UILabel = UILabel.init(frame:CGRect(x: 0
            , y: 10, width: view.frameWidth, height: 25))
        label.textColor = UIColor.init(red: 109.0/255.0, green: 109.0/255.0, blue:109.0/255.0, alpha: 1.0)
        label.text = withTitle;
        label.font = UIFont.init(name: NormalFont, size: fontSize)
        label.textAlignment = NSTextAlignment.center;
        view.addSubview(label);
        
        imageLogo.frameY = (view.frameHeight - (imageLogo.frameHeight + label.frameHeight))/2;
        label.frameY = imageLogo.frameMaxY;
        
        let btn:UIButton = UIButton.init(type: UIButtonType.custom);
        btn.frame = view.frame;
        btn.backgroundColor = UIColor.clear
        btn.frameX = 0;
        btn.frameY = 0;
        view.addSubview(btn);
        btn.layer.cornerRadius = 5.0
        btn.tag = withTag;
        btn.addTarget(self, action: #selector(btnSelecttapped), for: .touchUpInside)
        btn.titleLabel?.font = UIFont(name: NormalFont, size: 15)

        
        let radioImage:UIImageView = UIImageView.init(frame: CGRect(x: 15, y: 20, width: 20, height: 20))
        radioImage.image = UIImage.init(named: "tick-inside-circle.png")
        btn.addSubview(radioImage);
        radioImage.frameY = (btn.frameHeight - radioImage.frameHeight)/2
        radioImage.backgroundColor = UIColor.clear
        radioImage.isHidden = true
        radioImage.tag = withTag + 1;
        
        view.layer.cornerRadius = 5.0
        return mainview;
    }
    func btnSelecttapped(sender:UIButton) -> Void {
        print("selected tappped")
        isAnyOptionSelected = true
        let btn1:UIButton = (self.bgView.viewWithTag(45) as! UIButton)
        let btn2:UIButton = self.bgView.viewWithTag(55) as! UIButton
        btn1.isSelected = false;
        btn2.isSelected = false;
        let imageView1:UIImageView = btn1.viewWithTag(45+1) as! UIImageView
        let imageView2:UIImageView = btn2.viewWithTag(55+1) as! UIImageView
        btn1.backgroundColor = UIColor.clear
        btn2.backgroundColor = UIColor.clear
        imageView1.isHidden = true
        imageView2.isHidden = true

        let selectedImage:UIImageView = sender.viewWithTag(sender.tag + 1) as! UIImageView
        sender.backgroundColor = UIColor.init(red: 234.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 0.3)
        selectedImage.isHidden = false;
        sender.isSelected = true;


        
    }
    
    func btnNextTapped(sender:UIButton) -> Void {
        //
        let btn1:UIButton = (self.bgView.viewWithTag(45) as! UIButton)
        let btn2:UIButton = self.bgView.viewWithTag(55) as! UIButton
        if(btn1.isSelected){
            AppDelegate.getInstance().paramsDict.setObject("DRIVER", forKey: "OwnerOrRider" as NSCopying)
        }else if(btn2.isSelected){
            AppDelegate.getInstance().paramsDict.setObject("RIDER", forKey: "OwnerOrRider" as NSCopying)
        }else{
         // UIAlertController.showError(withMessage: NSLocalizedString(TP_SCREEN1_ERROR_MESSAGE, comment: ""), inViewController: self)
            return;
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil) //if bundle is nil the main bundle will be used
        let obj:TravelPrefStep2 = storyboard.instantiateViewController(withIdentifier: "TpScreen2") as! TravelPrefStep2
        print(obj)
        self.navigationController?.pushViewController(obj, animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
