//
//  MainContentNavSegue.m
//  zify
//
//  Created by Anurag S Rathor on 27/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "MainContentNavSegue.h"
#import "MainContentController.h"

@implementation MainContentNavSegue

-(void)perform{
    MainContentController *sourceController = self.sourceViewController;
    UINavigationController *destinationController = self.destinationViewController;
    sourceController.mainContentNavController = destinationController;
    NSDictionary *viewsDictionary = @{@"destcontrollerView":destinationController.view};
    destinationController.view.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[destcontrollerView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[destcontrollerView]|" options:0 metrics:nil views:viewsDictionary];
    [sourceController.view addConstraints:horizontalConsts];
    [sourceController.view  addConstraints:verticalConsts];
}
@end
