//
//  WalletController.swift
//  zify
//
//  Created by Anurag on 05/09/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit

class WalletController: ScrollableContentViewController, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var messageHandler: MessageHandler!
    //@IBOutlet var amount: UITextField!
    @IBOutlet var availableCash: UILabel!
    @IBOutlet var availablePoints: UILabel!
    //@IBOutlet var amountButtons: [UIButton]!
    //@IBOutlet var addMoney: UIButton!
    var rechargeAmount = ""
    //@IBOutlet var lblAvailableBalanceText: UILabel!
    //@IBOutlet var btnViewStatement: UIButton!
    //@IBOutlet var lblAddMoney: UILabel!
    @IBOutlet var tblView: UITableView!
    var arrList: [Any] = []

    //var amountArray: [Any] = []
    var zifyPaymentRefId = ""
    var isViewInitialised = false
    var currencySymbol = ""
    var currencyCode = ""
    //var razorpayController: RazorpayPaymentController?
    var arrListImages: [Any] = []
    
    let kOFFSET_FOR_KEYBOARD = 80.0
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        arrList = [NSLocalizedString(WA_ADD_MONEY, comment: ""), NSLocalizedString(WA_WITHDRAW, comment: ""), NSLocalizedString(WA_BANK_ACCOUNT, comment: ""), NSLocalizedString(WA_STATEMENT, comment: "")]
        arrListImages = ["addmoney", "withdraw", "bankAccount", "statement"]
    }
    override func viewWillAppear(_ animated: Bool) {
        self.messageHandler.dismissErrorView()
        self.messageHandler.dismissSuccessView()
    }
    
    override  func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isViewInitialised {
            fetchUserWallet()
            isViewInitialised = true
            if rechargeAmount != "" {
                //amount.text = rechargeAmount
            }
        }else{
            fetchUserWallet()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Actions
//    @IBAction func selectAmount(_ sender: UIButton) {
//        for index in 0..<amountButtons.count {
//            let button = amountButtons[index]
//            if sender == button {
//                amount.text = amountArray[index] as? String
//                button.backgroundColor = UIColor(red: 210.0 / 255.0, green: 210.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0)
//            } else {
//                button.backgroundColor = UIColor.white
//            }
//        }
//    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }
/*
    @IBAction func proceed(toPayment sender: UIButton) {
        if validateAmount() {
            let isRazorpayPaymentEnabled = CurrentLocale.sharedInstance().isRazorpayPaymentEnabled()
            if isRazorpayPaymentEnabled {
                messageHandler.showBlockingLoadView(handler: {
                    ServerInterface.sharedInstance().getResponse(RazorpayPaymentPersistRequest(requestType: PAYMENTREQUEST, andAmount: self.amount.text, andCurrency: "INR"), withHandler: { response, error in
                        self.messageHandler.dismissBlockingLoadView(handler: {
                            if response != nil && !(response?.responseObject is NSNull) && !((response?.responseObject as? String) == "") {
                                self.zifyPaymentRefId = response?.responseObject as! String
                                self.razorpayController?.initiatePayment(withAmount: self.amount.text)
                            } else {
                                self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                            }
                        })
                    })
                })
            } else {
                messageHandler.showBlockingLoadView(handler: {
                    ServerInterface.sharedInstance().getResponse(StripePaymentPersistRequest(requestType: STRIPEPAYMENTREQUEST, andAmount: self.amount.text, andCurrency: self.currencyCode), withHandler: { response, error in
                        self.messageHandler.dismissBlockingLoadView(handler: {
                            if response != nil && !(response?.responseObject is NSNull) && !((response?.responseObject as? String) == "") {
                                self.zifyPaymentRefId = response?.responseObject as! String
                                let stripePaymentNavController: UINavigationController? = StripePaymentController.createStripePaymentNavController()
                                let stripePaymentController = stripePaymentNavController?.topViewController as? StripePaymentController
                                stripePaymentController?.amount = self.amount.text
                                stripePaymentController?.paymentDelegate = self as! StripePaymentDelegate
                                if let aController = stripePaymentNavController {
                                    self.present(aController, animated: true)
                                }
                            } else {
                                self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                            }
                        })
                    })
                })
            }
        }
    }
    
    func validateAmount() -> Bool {
        
        if ("" == amount.text) {
            messageHandler.showErrorMessage(NSLocalizedString(VC_WALLET_AMOUNTVAL, comment: ""))
            return false
        }
        
        let iamount: Int = Int(amount.text!)!
        if iamount > 1000 || iamount == 1000 {
            let alert = UIAlertView(title: AppName, message: NSLocalizedString(VC_WALLET_AMOUNTVAL_LESSTHAN_THOUSAND, comment: ""), delegate: self, cancelButtonTitle: "", otherButtonTitles: "Ok")
            alert.show()
            
            return false
        }
        let amountValue: Int = Int(amount.text!)!
        if amountValue <= 0 {
            messageHandler.showErrorMessage(NSLocalizedString(VC_WALLET_AMOUNTVAL_ZERO, comment: ""))
            return false
        }
        return true
        
    }
    */
    
    func initialiseView() {
        scrollview.isHidden = true
        //addMoney.isHidden = true
        isViewInitialised = false
        let currentLocale = CurrentLocale.sharedInstance()
        //amountArray = (currentLocale?.getRechargeAmounts())!
        currencySymbol = (currentLocale?.getCurrencySymbol())!
        currencyCode = (currentLocale?.getCurrencyCode())!
        //
        //let placeHolderStr = "\(NSLocalizedString(WS_WALLET_TXT_ENTERAMOUNT, comment: "")) (\(currencySymbol))"
        //amount.placeholder = placeHolderStr
        //amount.layer.borderColor = UIColor(red: 210.0 / 255.0, green: 210.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0).cgColor
//        for index in 0..<amountButtons.count {
//
//            var button = amountButtons[index] as? UIButton
//            let title = "+ \(currencySymbol)\(amountArray[index])"
//
//            button?.setTitle(title, for: .normal)
//            button?.setTitle(title, for: .selected)
//            button?.layer.borderColor = UIColor(red: 210.0 / 255.0, green: 210.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0).cgColor
//        }
//        razorpayController = RazorpayPaymentController()
//        razorpayController?.paymentDelegate = self
    }
    
    func fetchUserWallet() {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(GenericRequest(requestType: RIDERWALLETREQUEST), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        let wallet = response?.responseObject as? UserWallet
                        let zifyCash:NSNumber = (wallet?.zifyMoney)!
                      /*  if(self.currencySymbol == "€"){
                            self.availableCash.text =      AppDelegate.getInstance().displayTripAmount(inCommaSeparator:zifyCash)
                        }else{
                            self.availableCash.text = String.init(format: "%@ %@", self.currencySymbol, zifyCash.stringValue)
                        }*/
                        self.availableCash.text =      AppDelegate.getInstance().displayTripAmount(inCommaSeparator:zifyCash)

                        
                        //rajesh -- if it comes like this - "99.81999999999999" make it "99.82"
                        let zifyCredits = String(format: "%05.2f",  wallet!.zifyCredits! as? Double  ?? 00)
                        self.availablePoints.text = String(format: NSLocalizedString(VC_WALLET_PROMOCREDITS, comment: "Credits : {UserPromotionalCredits}"), "\(String(describing: zifyCredits))")
                        
                        //rajesh
                        //self.availablePoints.text = String(format: NSLocalizedString(VC_WALLET_PROMOCREDITS, comment: "Credits : {UserPromotionalCredits}"), "\(String(describing: wallet!.zifyCredits!))")
                        
 
                        
                        //self.availablePoints.text = String(format: NSLocalizedString(VC_WALLET_PROMOCREDITS, comment: "Credits : {UserPromotionalCredits}"), "\(wallet?.availablePoints ?? "")")
                        self.scrollview.isHidden = false
                        //self.addMoney.isHidden = false
                        self.moveToAddMoneyScreenFromSearchResultToAdd()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    func moveToAddMoneyScreenFromSearchResultToAdd() {
        let isForAddMoney: Bool = UserDefaults.standard.bool(forKey: "toAddMoney")
        if isForAddMoney {
            //UserDefaults.standard.set(false, forKey: "toAddMoney")
           // UserDefaults.standard.synchronize()
            let sb = UIStoryboard(name: "Wallet", bundle: Bundle.main)
            let userWalletController = sb.instantiateViewController(withIdentifier: "AddMoneyController") as? AddMoneyController
            if rechargeAmount.count > 0{
                userWalletController?.rechargeAmount = rechargeAmount
            }
            if let aController = userWalletController {
                navigationController?.pushViewController(aController, animated: true)
            }
        }
    }
    
    /*
    // MARK:- RazorPayment Delegate Method
    func razorpayPaymentSuccuess(withPaymentId paymentId: String?) {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(RazorpayPaymentPersistRequest(requestType: PAYMENTRESPONSE, andPaymentId: paymentId, andZifyPaymentRefId: self.zifyPaymentRefId, andFailureCode: nil, andFailureDescription: nil), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.fetchUserWallet()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    func razorpayPaymentFailed(withCode code: Int32, andMessage message: String!) {
        ServerInterface.sharedInstance().getResponse(RazorpayPaymentPersistRequest(requestType: PAYMENTRESPONSE, andPaymentId: nil, andZifyPaymentRefId: zifyPaymentRefId, andFailureCode: Int32(code) as NSNumber, andFailureDescription: message), withHandler: { response, error in
            
        })
        messageHandler.showErrorMessage(NSLocalizedString(VC_WALLET_TRYAGAINERROR, comment: ""))
    }
    
    func createStripePayment(with token: STPToken?) {
        messageHandler.showBlockingLoadView(handler: {
            ServerInterface.sharedInstance().getResponse(StripePaymentPersistRequest(requestType: STRIPEPAYMENTRESPONSE, andStripeToken: token?.stripeID, andZifyPaymentRefId: self.zifyPaymentRefId, andFailureCode: nil, andFailureDescription: nil), withHandler: { response, error in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if response != nil {
                        self.fetchUserWallet()
                    } else {
                        self.messageHandler.showErrorMessage((error as NSError?)?.userInfo["message"] as! String)
                    }
                })
            })
        })
    }
    
    func stripePaymentFailed(error : NSError) throws {
        ServerInterface.sharedInstance().getResponse(StripePaymentPersistRequest(requestType: STRIPEPAYMENTRESPONSE, andStripeToken: nil, andZifyPaymentRefId: zifyPaymentRefId, andFailureCode: Int32(Int(error.code )) as NSNumber, andFailureDescription: error.localizedDescription), withHandler: { response, error in
        })
        messageHandler.showErrorMessage("Please try again.")
    }
    */
    
//    override func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == amount && AppDelegate.screen_HEIGHT() <= 568 {
//            if view.frame.origin.y >= 0 {
//                //[self setViewMovedUp:YES];
//            }
//        }
//    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        if AppDelegate.screen_HEIGHT() <= 568 {
            // [self setViewMovedUp:NO];
        }
    }

    func setViewMovedUp(_ movedUp: Bool) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3) // if you want to slide up the view
        
        var rect: CGRect = view.frame
        if movedUp {
            rect.origin.y -= CGFloat(kOFFSET_FOR_KEYBOARD)
            rect.size.height += CGFloat(kOFFSET_FOR_KEYBOARD)
        } else {
            // revert back to the normal state.
            rect.origin.y += CGFloat(kOFFSET_FOR_KEYBOARD)
            rect.size.height -= CGFloat(kOFFSET_FOR_KEYBOARD)
        }
        view.frame = rect
        
        UIView.commitAnimations()
    }
    
    func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        view.endEditing(true)
    }
    
    // MARK:- TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellidentifier = "tableviewcell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellidentifier) as? WalletTableViewCell
        cell?.lblContent.text = arrList[indexPath.section] as? String
        cell?.leftIconImageView.image = UIImage(named: arrListImages[indexPath.section] as? String ?? "")
        cell?.rightIconImageView.image = UIImage(named: "publish_right_arrow.png")
        addDropShadow(for: cell?.contentBgView)
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (arrList[indexPath.section] as? String) == NSLocalizedString(WA_ADD_MONEY, comment: "") {
            
            let sb = UIStoryboard(name: "Wallet", bundle: Bundle.main)
            let userWalletController = sb.instantiateViewController(withIdentifier: "AddMoneyController") as? AddMoneyController
            if let aController = userWalletController {
                let defaults = UserDefaults.standard
                defaults.set(true, forKey: "WalletPush")
                defaults.synchronize()
                navigationController?.pushViewController(aController, animated: true)
            }
            
            //[self.navigationController pushViewController:[UserWalletController createUserWalletNavController] animated:TRUE];
        } else if (arrList[indexPath.section] as? String) == NSLocalizedString(WA_WITHDRAW, comment: "") {
            
            let sb = UIStoryboard(name: "Account", bundle: Bundle.main)
            let accountRedeemMonyVc = sb.instantiateViewController(withIdentifier: "AccountRedeemMoneyController") as? AccountRedeemMoneyController
            if let aVc = accountRedeemMonyVc {
                navigationController?.pushViewController(aVc, animated: true)
            }
            
            //[self performSegueWithIdentifier:@"showWithdrawMoney" sender:self];
        } else if (arrList[indexPath.section] as? String) == NSLocalizedString(WA_BANK_ACCOUNT, comment: "") {
            //check crash
            let sb = UIStoryboard(name: "UserSettings", bundle: Bundle.main)
            let settingBankAccVc = sb.instantiateViewController(withIdentifier: "SettingsBankAccountsController") as? SettingsBankAccountsController
            if let aVc = settingBankAccVc {
                navigationController?.pushViewController(aVc, animated: true)
            }
            
            //[self performSegueWithIdentifier:@"showBankAccounts" sender:self];
        } else if (arrList[indexPath.section] as? String) == NSLocalizedString(WA_STATEMENT, comment: "") {
            let sb = UIStoryboard(name: "Wallet", bundle: Bundle.main)
            let stmtVc = sb.instantiateViewController(withIdentifier: "StmtVC") as? WalletStatementController
            if let aVc = stmtVc {
                navigationController?.pushViewController(aVc, animated: true)
            }
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //if (section == 0) return 10;
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    class func createUserWalletNavController() -> UINavigationController? {
        let storyBoard = UIStoryboard(name: "Wallet", bundle: Bundle.main)
        let rechargeNavController = storyBoard.instantiateViewController(withIdentifier: "userWalletNavController") as? UINavigationController
        return rechargeNavController
    }
    
    func addDropShadow(for viewShadow: UIView?) {
        viewShadow?.backgroundColor = UIColor.white
        viewShadow?.layer.shadowColor = UIColor(red: 117.0 / 255.0, green: 117.0 / 255.0, blue: 117.0 / 255.0, alpha: 0.4).cgColor
        viewShadow?.layer.shadowOpacity = 1
        viewShadow?.layer.shadowOffset = CGSize(width: -1, height: 0)
        viewShadow?.layer.shadowRadius = 4
        viewShadow?.layer.masksToBounds = false
    }

}
