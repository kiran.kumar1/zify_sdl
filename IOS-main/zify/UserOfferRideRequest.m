//
//  UserOfferRideRequest.m
//  zify
//
//  Created by Anurag Rathor on 20/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "UserOfferRideRequest.h"
#import "DriveRoute.h"


#define SRC_CITY_PARAM @"srcCity"
#define DEST_CITY_PARAM @"destCity"
#define SRC_COUNTRY_PARAM @"srcCountry"
#define DEST_COUNTRY_PARAM @"destCountry"
#define SRC_ADD_PARAM @"srcAdd"
#define DEST_ADD_PARAM @"destAdd"
#define SRC_LAT_PARAM @"srcLat"
#define SRC_LNG_PARAM @"srcLong"
#define DEST_LAT_PARAM @"destLat"
#define DEST_LNG_PARAM @"destLong"
#define NUM_SEATS_PARAM @"numOfSeats"
#define DEPARTURE_TIME_PARAM @"departureTime"
#define MERCHANT_ID_PARAM @"merchantId"
#define CHANNEL_ID_PARAM @"channelId"
#define LAST_DEPT_TIME @"lastDepartureTime"

@implementation UserOfferRideRequest
-(id)initWithSourceLocality:(LocalityInfo *)sourceLocality andDestinationLocality:(LocalityInfo *)destinationLocality andRideDate:(NSString *)rideDate andSeats:(NSNumber *)seats andMerchantId:(NSString *)merchantId{
    self = [super init];
    if (self) {
        _sourceLocality = sourceLocality;
        _destinationLocality = destinationLocality;
        _rideDate = rideDate;
        _seats = seats;
        _merchantId = merchantId;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    double sourceLat = (double)_sourceLocality.latLng.latitude;
    double sourceLng = (double)_sourceLocality.latLng.longitude;
    double destLat = (double)_destinationLocality.latLng.latitude;
    double destLng = (double)_destinationLocality.latLng.longitude;
    params[SRC_LAT_PARAM] = [[NSNumber numberWithDouble:sourceLat] stringValue];
    params[SRC_LNG_PARAM] = [[NSNumber numberWithDouble:sourceLng] stringValue];
    params[DEST_LAT_PARAM] = [[NSNumber numberWithDouble:destLat] stringValue];
    params[DEST_LNG_PARAM] = [[NSNumber numberWithDouble:destLng] stringValue];
    params[SRC_CITY_PARAM] = _sourceLocality.city == nil ? @"" : _sourceLocality.city;
    params[DEST_CITY_PARAM] =_destinationLocality.city == nil ? @"" : _destinationLocality.city;
    params[SRC_ADD_PARAM] = _sourceLocality.address;
    params[DEST_ADD_PARAM] = _destinationLocality.address;
    params[SRC_COUNTRY_PARAM] = _sourceLocality.country;
    params[DEST_COUNTRY_PARAM] = _destinationLocality.country;
    return params;
}

-(NSString *) serverURL{
    return @"/driver/getGlobalZifyRoutes.htm";
}

-(NSString *) mappingKey{
    return @"zifyRoutes";
}

-(RKObjectMapping *) objectMapping{
    return [DriveRoute getDriveRouteObjectMapping];
}
@end
