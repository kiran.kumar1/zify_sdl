//
//  DriverDetailEntity.m
//  zify
//
//  Created by Anurag Rathor on 23/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "DriverDetailEntity.h"

@implementation DriverDetailEntity
-(id)initWithDictionary:(NSDictionary *)infoDict{
    if([self init]){
        _firstName = [infoDict objectForKey:@"firstName"];
        _lastName = [infoDict objectForKey:@"lastName"];
        _driverStatus = [infoDict objectForKey:@"driverStatus"];
        _companyName = [infoDict objectForKey:@"companyName"];
        _totalDistance = [infoDict objectForKey:@"totalDistance"];
        _profileImgUrl = [self getValueForKey:@"profileImgUrl" fromDictionary:infoDict];//[infoDict objectForKey:@"profileImgUrl"];
        _vehicleImgUrl = [self getValueForKey:@"vehicleImgUrl" fromDictionary:infoDict];
        _vehicleModel = [infoDict objectForKey:@"vehicleModel"];
        _vehicleRegistrationNum = [infoDict objectForKey:@"vehicleRegistrationNum"];
        _driverRatingFemale = [infoDict objectForKey:@"driverRatingFemale"];
        _driverRatingMale = [infoDict objectForKey:@"driverRatingMale"];
        _numOfFemalesRated = [infoDict objectForKey:@"numOfFemalesRated"];
        _numOfMalesRated = [infoDict objectForKey:@"numOfMalesRated"];
        _userId = [infoDict objectForKey:@"userId"];
        _jabberId = [infoDict objectForKey:@"jabberId"];
        _callId = [infoDict objectForKey:@"callId"];
      //  _documents = [[DriverDetailDocumentsEntity alloc] initWithDictionary:[infoDict objectForKey:@"userDocs"]];
        id chatProfileDict = [self getValueForKey:@"qbUserInfo" fromDictionary:infoDict];
        if([chatProfileDict isKindOfClass:[NSDictionary class]]){
            _chatProfile = [[UserChatProfileEntity alloc] initWithDictionary:chatProfileDict];
        }else{
            _chatProfile = nil;
        }
    }
    return self;
}
-(NSString *)getValueForKey:(NSString *)key fromDictionary:(NSDictionary *)infoDict{
    id keyValue = [infoDict objectForKey:key];
    if(keyValue == (NSString *)[NSNull null] || keyValue == nil){
        keyValue = @"";
    }
    return keyValue;
}
@end
