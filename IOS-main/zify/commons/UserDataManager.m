//
//  UserDataManager.m
//  zify
//
//  Created by Anurag S Rathor on 20/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "UserDataManager.h"
#import "LocalityInfo.h"
#import "CreateRideController.h"
#import "GuestUserRideController.h"
#import "UserAddressPreferencesController.h"
#import "AppDelegate.h"
//#import "UserWalletController.h"
//#import "WalletStatementController.h"
#import "RideRequestsController.h"
#import "zify-Swift.h"

#define DEEPLINK_TYPE_PARAM @"deepLinkType"
#define MERCHANT_ID_PARAM @"merchantId"
#define TYPE_DEEPLINK_SEARCH 100
#define TYPE_DEEPLINK_OPEN_RIDE_REQUEST 101
#define TYPE_DEEPLINK_PREFERENCES 102
#define TYPE_DEEPLINK_SIGN_UP 104
#define TYPE_DEEPLINK_VIEW_STATEMENT 105
#define TYPE_DEEPLINK_RIDE_WITH_ME 106
#define TYPE_DEEPLINK_CHANGE_PASSWORD 107
#define TYPE_DEEPLINK_REFERRAL_LINK 108

#define SEARCH_SRCLAT_PARAM @"srcLat"
#define SEARCH_SRCLONG_PARAM @"srcLong"
#define SEARCH_DESTLAT_PARAM @"destLat"
#define SEARCH_DESTLONG_PARAM @"destLong"
#define SEARCH_SRCCITY_PARAM @"srcCity"
#define SEARCH_DESTCITY_PARAM @"destCity"
#define SEARCH_DEPARTURETIME_PARAM @"departureTime"

@implementation UserDataManager{
    UserSearchData *userSearchData;
    BOOL showDeepLinkController;
    NSDictionary *deepLinkParams;
    NSDateFormatter *dateTimeFormatter;
    NSDateFormatter *dateFormatter;
   // NSDateFormatter *timeFormatter;
    int deeplinkType;
    BOOL isGuestMode;
}
@synthesize deepLinkParams;

+(UserDataManager *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[UserDataManager alloc]init];
    });
    return instance;
}

-(id) init{
    self = [super init];
    showDeepLinkController = false;
    isGuestMode = true;
    dateTimeFormatter = [[NSDateFormatter alloc] init];
   // NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateTimeFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yy"];
//    timeFormatter = [[NSDateFormatter alloc] init];
//    [timeFormatter setDateFormat:@"HH:mm"];
//    [timeFormatter setLocale:locale];
    return self;
}

-(void)handleBranchDeepLinkParams:(NSDictionary *)params{
    //NSLog(@"deep  link params are %@", params);
    UserProfile *profile = [UserProfile getCurrentUser];
    if(!profile){
        [self functionForDisplayingChangePasswordForStandalone:params];
    }else if(profile && [profile.mobileVerified isEqualToNumber: @1] && profile.profileImgUrl.length > 0){
        id deepLinkTypeParam = [params objectForKey:DEEPLINK_TYPE_PARAM];
        NSLog(@"deepLinkType is %@", deepLinkTypeParam);
        if(deepLinkTypeParam != nil){
            deeplinkType = [deepLinkTypeParam intValue];
            if(deeplinkType == TYPE_DEEPLINK_CHANGE_PASSWORD){
                return;
            }
            deepLinkParams = params;
            if(deeplinkType == TYPE_DEEPLINK_SEARCH){
                //Kept the search flow params processing here since in future if we want to populate source address and destination address on the map once after the user launch it will be easy to handle.
                LocalityInfo *sourceLocality = [[LocalityInfo alloc] initWithName:nil andAddress:nil andCity:params[SEARCH_SRCCITY_PARAM] andState:nil andCountry:nil andLatLng:CLLocationCoordinate2DMake([params[SEARCH_SRCLAT_PARAM] doubleValue],[params[SEARCH_SRCLONG_PARAM] doubleValue])];
                LocalityInfo *destLocality = [[LocalityInfo alloc] initWithName:nil andAddress:nil andCity:params[SEARCH_DESTCITY_PARAM] andState:nil andCountry:nil andLatLng:CLLocationCoordinate2DMake([params[SEARCH_DESTLAT_PARAM] doubleValue],[params[SEARCH_DESTLONG_PARAM] doubleValue])];
                
                NSString *departureDateStrValue = params[SEARCH_DEPARTURETIME_PARAM];
                NSDate *departureDate = [dateTimeFormatter dateFromString:departureDateStrValue];
                NSString *rideDateValue = [dateFormatter stringFromDate:departureDate];
                NSString *rideTimeValue = [[AppDelegate getAppDelegateInstance].timeFormatter stringFromDate:departureDate];
                [self setUserSearchData:[[UserSearchData alloc] initWithSourceLocality:sourceLocality andDestLocality:destLocality andRideStartDate:departureDateStrValue andRideDateValue:rideDateValue andRideTimeValue:rideTimeValue andMerchantId:params[MERCHANT_ID_PARAM]]];
                deepLinkParams = nil;
            }
            if(showDeepLinkController){
                UIViewController *rootViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
                if(rootViewController.presentedViewController != nil){
                    [rootViewController dismissViewControllerAnimated:NO completion:^{
                        [self handleDeepLinkParams];
                    }];
                } else{
                    [self handleDeepLinkParams];
                }
            }
        }
    }
}
-(void)functionForDisplayingChangePasswordForStandalone:(NSDictionary *)params{
        id deepLinkTypeParam = [params objectForKey:DEEPLINK_TYPE_PARAM];
        NSLog(@"deepLinkType is %@", deepLinkTypeParam);
        if(deepLinkTypeParam != nil){
            deeplinkType = [deepLinkTypeParam intValue];
            if(deeplinkType == TYPE_DEEPLINK_CHANGE_PASSWORD){
                deepLinkParams = params;
                id rootVc = [AppDelegate getAppDelegateInstance].window.rootViewController;
                if([rootVc isKindOfClass:[UINavigationController class]]){
                    UINavigationController *navController = (UINavigationController *) rootVc;
                    if(navController.presentedViewController != nil){
                        [navController dismissViewControllerAnimated:NO completion:^{
                            [self showForgotPasswordScreen:navController];
                        }];
                    } else{
                        [self showForgotPasswordScreen:navController];
                    }
                }
                deepLinkParams = nil;
            } else if(deeplinkType == TYPE_DEEPLINK_REFERRAL_LINK) {
                // this values are used in Refer and Earn screen saveReferralData service request
                [NSUserDefaults.standardUserDefaults setValue:[params valueForKey:@"userId"] forKey:@"referralUserId"];
                [NSUserDefaults.standardUserDefaults setValue:[params valueForKey:@"countryCode"] forKey:@"referralCountryCode"];
                [NSUserDefaults.standardUserDefaults synchronize];
            }
        } else {
            NSLog(@"Nill deeplink params");
        }
    
}


-(void)showForgotPasswordScreen:(UINavigationController *)navController{
    [navController.view endEditing:YES];
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *obj = (UINavigationController *)[main instantiateViewControllerWithIdentifier:@"navigation"];
    ForgotPasswordController *forgotObj =  (ForgotPasswordController *)[obj topViewController];
    forgotObj.navigationItem.title = NSLocalizedString(USS_CHANGEPASSWORD_NAV_TITLE, nil);
    forgotObj.emailStr = [deepLinkParams objectForKey:@"emailid"];
    [navController presentViewController:obj animated:YES completion:nil];
    
}
-(BOOL)isDeepLinkDataPresent{
    return deepLinkParams != nil;
}

-(void)handleDeepLinkParams{
    UIViewController *rootViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    if(deeplinkType == TYPE_DEEPLINK_SEARCH){
       /* if(isGuestMode){
            UINavigationController *guestRideNavController = [GuestUserRideController createGuestRideNavController];
            GuestUserRideController *guestRideController =  (GuestUserRideController *)[guestRideNavController topViewController];
            guestRideController.userSearchData = userSearchData;
            [rootViewController presentViewController:guestRideNavController animated:NO completion:nil];
        } else{*/
            UINavigationController *createRideNavController = [CreateRideController getcreateRideNavController];
            CreateRideController *createRideController =  (CreateRideController *)[createRideNavController topViewController];
            createRideController.userSearchData = userSearchData;
            [rootViewController presentViewController:createRideNavController animated:NO completion:nil];
       // }
        [self resetUserSearchData];
    }else if(deeplinkType == TYPE_DEEPLINK_PREFERENCES){
        if(!isGuestMode){
            UserAddressPreferencesController *addressPreferencesController = [UserAddressPreferencesController createUserAddressPreferencesController];
            addressPreferencesController.showPageIndicator = NO;
            addressPreferencesController.isUpgradeFlow = NO;
            [rootViewController presentViewController:addressPreferencesController animated:NO completion:nil];
        }
    }else if(deeplinkType == TYPE_DEEPLINK_SIGN_UP){
        /*if(isGuestMode){
           [[AppDelegate getAppDelegateInstance] showLoginController];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SignUpViewController *signVc = [sb instantiateViewControllerWithIdentifier:@"signup"];
            
            UINavigationController *navController = (UINavigationController *) [[UIApplication sharedApplication] keyWindow].rootViewController;
            LoginViewController *loginController = [sb instantiateViewControllerWithIdentifier:@"LoginVc"];
            NSLog(@"obj is ***%@",loginController);
            signVc.registerDelegate = loginController;
            [navController presentViewController:signVc animated:YES completion:nil];
        }*/

    }else if(deeplinkType == TYPE_DEEPLINK_VIEW_STATEMENT){
        if(!isGuestMode){
        UINavigationController *walletNavController = (UINavigationController *) [AddMoneyController createUserWalletNavController];
        [rootViewController presentViewController:walletNavController animated:NO completion:nil];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Wallet" bundle:nil];
            WalletStatementController *stmtVc = [sb instantiateViewControllerWithIdentifier:@"StmtVC"];
            [walletNavController pushViewController:stmtVc animated:NO];
       }
    }else if(deeplinkType == TYPE_DEEPLINK_OPEN_RIDE_REQUEST){
        if(!isGuestMode){
            [rootViewController presentViewController:[RideRequestsController createRideRequestsNavController] animated:YES completion:nil];
        }
    }else if(deeplinkType == TYPE_DEEPLINK_RIDE_WITH_ME){
        /*
         NSNumber *driverId;
         NSNumber *loggedInUserId = user.userId;
         NSString *loggedInUserStr = [loggedInUserId stringValue];
         BOOL isSameUserLoggedIn = false;
         
         id driverIdStr = [deepLinkParams objectForKey:@"driverId"];
         if([driverIdStr isKindOfClass:[NSString class]]){
         NSString *str = (NSString *)driverIdStr;
         if([loggedInUserStr isEqualToString:str])
         {
         isSameUserLoggedIn = YES;
         }
         }else if([driverIdStr isKindOfClass:[NSNumber class]]){
         driverId = (NSNumber *) driverIdStr;
         if( driverId  == loggedInUserId){
         isSameUserLoggedIn = YES;
         }
         }
         if(isSameUserLoggedIn) {
         deepLinkParams = nil;
         return;
         }*/
        UserProfile *user = [UserProfile getCurrentUser];
        NSString *driverIdStr = [deepLinkParams objectForKey:@"driverId"];
        NSString *loggedInUserId = [user.userId stringValue];
        if([driverIdStr isEqualToString:loggedInUserId]) {
            deepLinkParams = nil;
            return;
        }
        UINavigationController *createRideNavController = [CreateRideController getcreateRideNavController];
        CreateRideController *createRideController =  (CreateRideController *)[createRideNavController topViewController];
        createRideController.isFromDeepLinking = YES;
        createRideController.rideWithMeDict = deepLinkParams;
        [rootViewController presentViewController:createRideNavController animated:NO completion:nil];
        deepLinkParams = nil;
    }
}

-(void)setUserSearchData:(UserSearchData *)searchData{
    userSearchData = searchData;
}

-(void)resetUserSearchData{
    userSearchData = nil;
}

-(UserSearchData *)getUserSearchData{
    return userSearchData;
}

-(void)setDeepLinkFlagsOnLogin{
    [self setShowDeepLinkController:YES];
    [self setGuestMode:NO];
}

-(void)setDeepLinkFlagsOnLogout{
    [self setShowDeepLinkController:NO];
    [self setGuestMode:YES];
}

-(void)setShowDeepLinkController:(BOOL)flag{
    showDeepLinkController = flag;
}

-(void)setGuestMode:(BOOL)flag{
    isGuestMode = flag;
}
@end
