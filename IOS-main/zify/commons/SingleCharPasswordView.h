//
//  SingleCharPasswordView.h
//  zify
//
//  Created by Anurag S Rathor on 20/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SingleCharPasswordViewDelegate <NSObject>
-(void) enteredPasswordCode:(NSString *)code;
-(void)displayErrorMessageToSelectValidOTP;
@end

@interface SingleCharPasswordView :  UIView<UIKeyInput>
@property (nonatomic) NSNumber *textLength;
@property (nonatomic) NSNumber *lineWidth;
@property (nonatomic) NSString *otpCode;
@property (nonatomic, weak) id<SingleCharPasswordViewDelegate> passwordDelegate;
-(void)showKeyboard;
-(void)handleErrorCase;
@end

