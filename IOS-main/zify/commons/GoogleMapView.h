//
//  GoogleMapVIew.h
//  zify
//
//  Created by Anurag S Rathor on 23/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalityInfo.h"
//#import "NearestAvailableCarsResponse.h"
#import "MapView.h"

@import GoogleMaps;

@interface GoogleMapView : MapView
//@property (nonatomic) BOOL refreshTrackingpath;
/*-(void) addPolyLine:(NSArray *)pathArray andLat:(NSNumber *)latitude andLong:(NSNumber *)longitude;
-(void)createTrackingMarkerWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude;
-(void) addPolyLineString:(NSString *)path andLat:(NSNumber *)latitude andLong:(NSNumber *)longitude;*/
//-(void)drawMarkersForAvailableCarsResponse:(NearestAvailableCarsResponse *)availableCarsResponse;

-(void)createMarkerForPickupWithhLat:(NSNumber *)latitude andLong:(NSNumber *)longitude andMarkerImage:(UIImage *)markerImage withWalkingDistance:(NSString *)distance;
-(void)putMarkersOnSameMap;
@end
