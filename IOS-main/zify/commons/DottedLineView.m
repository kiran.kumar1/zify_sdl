//
//  DottedLineView.m
//  zify
//
//  Created by Anurag S Rathor on 17/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "DottedLineView.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation DottedLineView

-(void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, [UIColorFromRGB(0X464646) CGColor]);
    CGContextBeginPath (context);
    CGFloat dashLengths[] = { 1, 1 };
    CGContextSetLineDash(context, 0, dashLengths, 2);
    if(self.horizontalLine){
        CGContextMoveToPoint   (context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    } else if(self.verticalLine){
        CGContextMoveToPoint   (context, CGRectGetMaxX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    }
    CGContextStrokePath(context);
}

@end
