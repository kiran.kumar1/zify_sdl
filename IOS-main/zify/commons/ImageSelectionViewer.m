//
//  ImageSelectionViewer.m
//  zify
//
//  Created by Anurag S Rathor on 06/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ImageSelectionViewer.h"
#import "ImageHelper.h"
#import <Photos/Photos.h>
#import "UniversalAlert.h"
#import "LocalisationConstants.h"

@implementation ImageSelectionViewer


#pragma mark-
#pragma mark Shared Instance
#pragma mark-

+(ImageSelectionViewer *)sharedInstance{
    static dispatch_once_t onceToken;
    static id imageViewerInstance;
    dispatch_once(&onceToken, ^{
        imageViewerInstance = [[ImageSelectionViewer alloc] init];
    });
    return imageViewerInstance;
}

-(void) selectImage{
    if([UIAlertController class]){
        UIAlertController *alertController= [UIAlertController alertControllerWithTitle:nil
                                              message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(CMON_IMAGESELCETION_CHOOSEEXISTINGPHOTO, nil)
            style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                 [self openGallery];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(CMON_IMAGESELCETION_OPENCAMERA, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self openCamera];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(CMON_GENERIC_CANCEL, nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        }]];
        
        [[self delegateController] presentViewController:alertController
                           animated:YES
                         completion:NULL];
    } else{
        UIActionSheet *actionSheet= [[UIActionSheet alloc] initWithTitle:nil
                                                                delegate:self
                                                       cancelButtonTitle:NSLocalizedString(CMON_GENERIC_CANCEL, nil)
                                                  destructiveButtonTitle:nil
                                                       otherButtonTitles:NSLocalizedString(CMON_IMAGESELCETION_CHOOSEEXISTINGPHOTO, nil),NSLocalizedString(CMON_IMAGESELCETION_OPENCAMERA, nil), nil];
        [actionSheet showInView:[self delegateController].view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        default:
            break;
        case 0:
            [self openGallery];
            break;
        case 1:
            [self openCamera];
            break;
    }
}

-(void) openCamera{
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted){
        UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_IMAGESELECTION_ENABLEACCESS, nil) WithMessage:NSLocalizedString(CMON_IMAGESELECTION_ENABLECAMAERAACCESS, nil)];
        [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
            
        }];
        [alert showInViewController:[self delegateController]];
        return;
    }
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    cameraUI.mediaTypes =  [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    if([self.imageSelectionDelegate allowImageEditing]){
       cameraUI.allowsEditing = YES;
    }else{
        cameraUI.allowsEditing = NO;
    }
    cameraUI.delegate = self;
    [[self delegateController] presentViewController:cameraUI animated:YES completion:nil];
}

-(void) openGallery{
    float currentVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(currentVersion >= 8.0 && currentVersion < 9.0){
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted){
            UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(CMON_IMAGESELECTION_ENABLEACCESS, nil) WithMessage:NSLocalizedString(CMON_IMAGESELECTION_ENABLEPHOTOACCESS, nil)];
            [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
            
            }];
            [alert showInViewController:[self delegateController]];
            return;
        }
    }
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    if([self.imageSelectionDelegate allowImageEditing]){
        mediaUI.allowsEditing = YES;
    } else {
        mediaUI.allowsEditing = NO;
    }
    mediaUI.delegate = self;
   [[self delegateController] presentViewController:mediaUI animated:YES completion:nil];
}
- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToSave;
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
        editedImage = (UIImage *) [info objectForKey:UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey: UIImagePickerControllerOriginalImage];
        if (editedImage) {
            imageToSave = editedImage;
        } else {
            imageToSave = originalImage;
        }
    }
    if([self.imageSelectionDelegate allowImageEditing]){
         [self.imageSelectionDelegate userSelectedFinalImage:[ImageHelper imageWithImage:imageToSave scaledToSize:self.finalImageSize]];
    } else{
         [self.imageSelectionDelegate userSelectedFinalImage:imageToSave];
    }
   
    [[self delegateController] dismissViewControllerAnimated:NO completion:nil];
}

-(UIViewController *) delegateController{
    return (UIViewController *)self.imageSelectionDelegate;
}
@end
