//
//  FacebookLogin.m
//  zify
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "FacebookLogin.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import  <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@implementation FacebookLogin{
    FacebookLogin *instance;
    FBSDKLoginManager *loginManager;
}
@synthesize faceBookLoginDelgate;


+(FacebookLogin *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance = [[FacebookLogin alloc] init];
    });
    return instance;
}

-(id) init{
    self = [super init];
    loginManager = nil;
    loginManager = [[FBSDKLoginManager alloc] init];
    return self;
}
-(void)loginWithFacebook{
    [self closeSessionAndClearSavedTokens];
    if([FBSDKAccessToken currentAccessToken]){
        [self fetchUserProfile];
    } else{
        
      /*  [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"id, name, first_name, last_name, email, picture.type(large)"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             
             if (!error) {
                 
                 NSLog(@"fetched user:%@  and Email : %@", result,result[@"email"]);
             }
         }];*/
    

        
        [loginManager logInWithReadPermissions:@[@"email",@"public_profile",@"user_friends",@"user_actions.video",@"user_actions.music",@"user_likes"] fromViewController:(UIViewController *)faceBookLoginDelgate handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error || result.isCancelled || ![result.grantedPermissions containsObject:@"email"]) {
                [self.faceBookLoginDelgate unableToLoginFBWithError:error];
            } else {
                [self fetchUserProfile];
            }
        }];
    }
}
-(void)fetchUserProfile {
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"id,first_name, last_name,gender,email,picture.width(480).height(480)"}]
         
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fb result is %@", result);
               //  NSString *nameOfLoginUser = [result valueForKey:@"name"];
                 NSString *imageStringOfLoginUser = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                 [prefs setObject:imageStringOfLoginUser forKey:@"fbURL"];
                 [prefs synchronize];
                 NSLog(@"imageStringOfLoginUser from fb result is %@", imageStringOfLoginUser);

                 [self.faceBookLoginDelgate loggedInWithFaceBook:[FBLoginResponse getFaceBookLoginResponse:result]];
             } else{
                 [self.faceBookLoginDelgate unableToLoginFBWithError:error];
             }
         }];
    }
}
-(void)shareWithFacebook{
    if([FBSDKAccessToken currentAccessToken]){
        [self.faceBookLoginDelgate postFeedToFacebook];
    } else{
        [loginManager logInWithReadPermissions:@[@"email",@"public_profile",@"user_friends",@"user_actions.video",@"user_actions.music",@"user_likes"] fromViewController:(UIViewController *)faceBookLoginDelgate handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error || result.isCancelled) {
                [self.faceBookLoginDelgate unableToLoginFBWithError:error];
            } else {
                [self.faceBookLoginDelgate postFeedToFacebook];
            }
        }];
    }
}
-(void)postFeedToFacebookWithParams:(NSDictionary *)params {
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    shareDialog.fromViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentDescription=[params objectForKey:@"description"];
    content.contentTitle=[params objectForKey:@"name"];
    content.imageURL=[NSURL URLWithString:[params objectForKey:@"picture"]];
    content.contentURL=[NSURL URLWithString:[params objectForKey:@"link"]];
    shareDialog.shareContent=content;
    shareDialog.mode = FBSDKShareDialogModeFeedWeb;
    [shareDialog show];
}

-(void)closeSessionAndClearSavedTokens {
    NSLog(@"Need to uncomment it");
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for(NSHTTPCookie *cookie in [storage cookies])
        {
            NSString *domainName = [cookie domain];
            NSLog(@"domainName %@",domainName);
            NSRange domainRange = [domainName rangeOfString:@"facebook"];
            if(domainRange.length > 0)
            {
                NSLog(@"findddd");
                [storage deleteCookie:cookie];
            }
        }
        [loginManager logOut];
     }
    [loginManager logOut];

}

-(NSString *)currentAccessToken{
    return [[FBSDKAccessToken currentAccessToken] tokenString];
}
@end
