//
//  SingleCharPasswordView.m
//  zify
//
//  Created by Anurag S Rathor on 20/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "SingleCharPasswordView.h"
#import "LineView.h"
#import "LocalisationConstants.h"
#import "UIAlertController+Blocks.h"

#define TAG_CONSTANT 500

@implementation SingleCharPasswordView{
    int nextElementIndex;
    float lineSpacing;
    BOOL isViewInitialised;
    BOOL isLongPressed;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

#pragma mark - View Delagate Methods
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        isViewInitialised = false;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        isViewInitialised = false;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    if(!isViewInitialised){
        lineSpacing = 5.0f;
        nextElementIndex = 0;
        _otpCode = @"";
        float lineViewWidth = (CGRectGetWidth([self frame]) - (_textLength.floatValue - 1)* lineSpacing) / _textLength.floatValue;
        float lineViewPosition = CGRectGetHeight([self frame]);
        for (int counter = 0;counter < _textLength.intValue ;counter++){
            LineView *lineView = [[LineView alloc] initWithFrame:CGRectMake(counter * (lineViewWidth + lineSpacing), lineViewPosition - _lineWidth.floatValue, lineViewWidth , _lineWidth.floatValue)] ;
            lineView.backgroundColor = [UIColor whiteColor];
            lineView.horizontalLine = true;
            lineView.lineWidth = _lineWidth;
            [self addSubview:lineView];
            float labelWidth = (CGRectGetWidth([lineView frame])) - 10.0;
            float labelHeight = 25.0;
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(lineView.frame) + lineSpacing,CGRectGetMinY(lineView.frame)- lineSpacing - labelHeight , labelWidth , labelHeight)];
            label.font = [UIFont fontWithName:@"HelveticaNeue" size:24.0];
            label.textColor = [UIColor darkGrayColor];
            label.tag = counter + TAG_CONSTANT;
            label.text = @"-";
            label.textAlignment = NSTextAlignmentCenter;
            [self addSubview:label];
        }
        UITapGestureRecognizer *tapGestureRecogniser =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(handleSingleTap:)];
        [self  addGestureRecognizer:tapGestureRecogniser];
        
        UILongPressGestureRecognizer *longPressGestureRecogniser = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        [self addGestureRecognizer:longPressGestureRecogniser];
        
        isViewInitialised = true;
        
        
    }
}

#pragma mark -  UIKeyInput delegate methods

- (BOOL)hasText{
    BOOL result = nextElementIndex > 0 ?  true :  false;
    return result;
}

- (void)insertText:(NSString *)text{
    if(nextElementIndex == _textLength.intValue){
        UILabel *label = (UILabel *)[self viewWithTag:TAG_CONSTANT];
        label.textColor = [UIColor darkGrayColor];
        label.text = text;
        for (int index= 1;index < _textLength.intValue;index++) {
            UILabel *label = (UILabel *)[self viewWithTag:index + TAG_CONSTANT];
            label.textColor = [UIColor darkGrayColor];
            label.text = @"-";
        }
        nextElementIndex = 1;
    } else{
        UILabel *label = (UILabel *)[self viewWithTag:nextElementIndex + TAG_CONSTANT];
        label.text = text;
        label.textColor = [UIColor darkGrayColor];
        nextElementIndex += 1;
    }
    
    if (nextElementIndex == _textLength.intValue) {
        NSString *code = [(UILabel *)[self viewWithTag:TAG_CONSTANT] text];
        for (int index= 1;index < nextElementIndex;index++) {
            code = [code stringByAppendingString:[(UILabel *)[self viewWithTag:index + TAG_CONSTANT] text]];
        }
        [self resignFirstResponder];
        self.otpCode = [[NSString alloc] initWithString:code];
        [self.passwordDelegate enteredPasswordCode:code];
    }
}
- (void)deleteBackward{
    if (nextElementIndex > 0) {
        if(nextElementIndex == _textLength.intValue){
            for (int index= 0;index < _textLength.intValue;index++) {
                UILabel *label = (UILabel *)[self viewWithTag:index + TAG_CONSTANT];
                label.textColor = [UIColor darkGrayColor];
            }
        }
        nextElementIndex -= 1;
        UILabel *label = (UILabel *)[self viewWithTag:nextElementIndex + TAG_CONSTANT];
        label.text = @"-";
        
        NSString *code = [[NSString alloc] init];
        for (int index= 0;index < nextElementIndex;index++) {
            code = [code stringByAppendingString:[(UILabel *)[self viewWithTag:index + TAG_CONSTANT] text]];
        }
        self.otpCode = [[NSString alloc] initWithString:code];
        NSLog(@"code is %@",self.otpCode);
    }
}

#pragma mark - UITextInputTraits Delegate methods

-(UIKeyboardType) keyboardType {
    return UIKeyboardTypeNumberPad;
}

#pragma mark - UIResponder Delegate methods

-(BOOL) canBecomeFirstResponder{
    return true;
}

- (UIView *)inputAccessoryView {
    UIToolbar* toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlack;
    toolBar.tintColor = [UIColor colorWithRed:(239.0/255.0) green:(239.0/255.0) blue:(239.0/255.0) alpha:1.0];
    toolBar.items = [NSArray arrayWithObjects:
                     [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                     [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(CMON_GENERIC_DONE, nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)],
                     nil];
    [toolBar sizeToFit];
    return toolBar;
}

-(void)dismiss{
    [self resignFirstResponder];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self becomeFirstResponder];
}
-(void)handleLongPress:(UITapGestureRecognizer *)recognizer {
    NSLog(@"long presss is done");
    
    if(isLongPressed){return;}
    
    isLongPressed = true;
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *copiedStr = pasteboard.string;
    NSLog(@"copied string is %@", copiedStr);
    if(copiedStr.length == 0 || [copiedStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0){
        [self.passwordDelegate displayErrorMessageToSelectValidOTP];
        isLongPressed = false;
        return;
    }
    BOOL isValid =   [self validateString:copiedStr withPattern:@"^[0-9]{6}$"];

    if(!isValid){
        [self.passwordDelegate displayErrorMessageToSelectValidOTP];
        isLongPressed = false;
        return;
    }
    
    nextElementIndex = 0;
    for (int i=0; i< copiedStr.length ;i++){
        NSRange range = NSMakeRange(i, 1);
        NSString *charAtPosition = [copiedStr substringWithRange:range];
        NSLog(@"charAtPosition is %@", charAtPosition);
        [self insertText:charAtPosition];
    }
    isLongPressed = false;
}
- (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = NO;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = YES;
    
    return didValidate;
}
-(void)showKeyboard{
    [self becomeFirstResponder];
}

-(void)handleErrorCase{
    for (int index= 0;index < _textLength.intValue;index++) {
        UILabel *label = (UILabel *)[self viewWithTag:index + TAG_CONSTANT];
        label.textColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
    }
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values = @[@0, @-20,@20,@-20,@0];
    animation.keyTimes = @[@0, @(1.0 /4.0), @(2.0 / 4.0),@(3.0 / 4.0), @1];
    animation.duration = 0.3;
    animation.additive = YES;
    [self.layer addAnimation:animation forKey:nil];
}
@end

