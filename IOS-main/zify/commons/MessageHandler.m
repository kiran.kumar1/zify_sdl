//
//  MessageHandler.m
//  zify
//
//  Created by Anurag S Rathor on 27/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "MessageHandler.h"
#import "AppDelegate.h"
#import "ChatNotificationView.h"
#import "LocalisationConstants.h"

enum MessagePosition {Top,Bottom};

@interface MessageHandler()

@end
@implementation MessageHandler{
    UIGestureRecognizer *appliedErrorGuestureRecognizer;
    UISwipeGestureRecognizer *appliedErrorGuestureRecognizerWithSwipe;
    UIGestureRecognizer *appliedSuccessGuestureRecognizer;
    UISwipeGestureRecognizer *appliedSuccessGuestureRecognizerWithSwipe;
    NSLayoutConstraint *beforeVerticalConstraint;
    NSLayoutConstraint *afterVerticalConstraint;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [[NSBundle mainBundle] loadNibNamed:@"MessageHandling" owner:self options:nil];
}

-(void)showBlockingLoadViewWithHandler:(void (^)())handler{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    NSDictionary *viewsDictionary = @{@"blockingView":self.blockingView};
    [window addSubview:self.blockingView];
    [window bringSubviewToFront:self.blockingView];
    self.blockingView.translatesAutoresizingMaskIntoConstraints = false;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[blockingView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[blockingView]|" options:0 metrics:nil views:viewsDictionary];
    [window addConstraints:horizontalConsts];
    [window addConstraints:verticalConstraints];
    self.blockingView.alpha = 0;
    [UIView animateWithDuration:0.01 animations:^{
        self.blockingView.alpha = 1;
    } completion:^(BOOL finished) {
        handler();
        [self.blockingViewIndicator startAnimating];
    }];
}

-(void)dismissBlockingLoadViewWithHandler:(void (^)())handler{
    [self.blockingViewIndicator stopAnimating];
    [UIView animateWithDuration:0.01 animations:^{
        self.blockingView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.blockingView removeFromSuperview];
        handler();
    }];
}

-(void)showTransparentBlockingLoadViewWithHandler:(void (^)())handler{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isNEedToDisplay = [prefs boolForKey:@"donotDisplayHud"];
    if(isNEedToDisplay){
        if(self.loadingSpinner.superview){
            [self.loadingSpinner.superview removeFromSuperview];
        }
    }
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    NSDictionary *viewsDictionary = @{@"blockingView":self.transparentBlockingView};
    [window addSubview:self.transparentBlockingView];
    [window bringSubviewToFront:self.transparentBlockingView];
    self.transparentBlockingView.translatesAutoresizingMaskIntoConstraints = false;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[blockingView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[blockingView]|" options:0 metrics:nil views:viewsDictionary];
    [window addConstraints:horizontalConsts];
    if(verticalConstraints.count > 0)
        [window addConstraints:verticalConstraints];
    self.transparentBlockingView.alpha = 0;
    [UIView animateWithDuration:0.01 animations:^{
        self.transparentBlockingView.alpha = 1;
    } completion:^(BOOL finished) {
        handler();
        // [self.loadingSpinner startAnimating];
        
        if(isNEedToDisplay){
            [prefs setBool:NO forKey:@"donotDisplayHud"];
            [prefs synchronize];
        }else{
            [self.loadingSpinner startAnimating];
        }
    }];
}

-(void)dismissTransparentBlockingLoadViewWithHandler:(void (^)())handler{
    [self.loadingSpinner stopAnimating];
    [UIView animateWithDuration:0.01 animations:^{
        self.transparentBlockingView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.transparentBlockingView removeFromSuperview];
        handler();
    }];
}

-(void)showErrorMessage:(NSString *)message{
    self.errorLabel.text = message;
    [self showOverLayView:self.errorView atPosition:Top];
    self.container.userInteractionEnabled = NO;
    appliedErrorGuestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissErrorView)];
    appliedErrorGuestureRecognizerWithSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissErrorView)];
    appliedErrorGuestureRecognizerWithSwipe.direction = (UISwipeGestureRecognizerDirectionLeft |UISwipeGestureRecognizerDirectionRight |
                                                         UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp);
    [self.container.superview addGestureRecognizer:appliedErrorGuestureRecognizer];
    [self.container.superview addGestureRecognizer:appliedErrorGuestureRecognizerWithSwipe];
    
}

-(void)dismissErrorView{
    /*AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     for(UIView *view in appdelegate.window.subviews){
     if([view isKindOfClass:[ChatNotificationView class]]){
     [view removeFromSuperview];
     }
     }*/
    if(appliedErrorGuestureRecognizer || appliedErrorGuestureRecognizerWithSwipe){
        [self dismissOverLayView:self.errorView position:Top];
        self.container.userInteractionEnabled = YES;
        if(appliedErrorGuestureRecognizer){
            [self.container.superview removeGestureRecognizer:appliedErrorGuestureRecognizer];
            appliedErrorGuestureRecognizer = nil;
        }
        if(appliedErrorGuestureRecognizerWithSwipe){
            [self.container.superview removeGestureRecognizer:appliedErrorGuestureRecognizerWithSwipe];
            appliedErrorGuestureRecognizerWithSwipe = nil;
        }
    }else{
        if(self.errorView){
            [self dismissOverLayView:self.errorView position:Top];
            self.container.userInteractionEnabled = YES;
            if(appliedErrorGuestureRecognizer){
                [self.container.superview removeGestureRecognizer:appliedErrorGuestureRecognizer];
                appliedErrorGuestureRecognizer = nil;
            }
        }else{
            self.container.userInteractionEnabled = YES;
        }
    }
    
}

-(void)showSuccessMessage:(NSString *)message{
    self.successLabel.text = message;
    [self showOverLayView:self.successView atPosition:Top];
    self.container.userInteractionEnabled = NO;
    appliedSuccessGuestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissSuccessView)];
    appliedSuccessGuestureRecognizerWithSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissSuccessView)];
    appliedSuccessGuestureRecognizerWithSwipe.direction = (UISwipeGestureRecognizerDirectionLeft |UISwipeGestureRecognizerDirectionRight |
                                                           UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp);
    [self.container.superview addGestureRecognizer:appliedSuccessGuestureRecognizer];
    [self.container.superview addGestureRecognizer:appliedSuccessGuestureRecognizerWithSwipe];
}

-(void)dismissSuccessView{
    if(appliedSuccessGuestureRecognizer  || appliedSuccessGuestureRecognizerWithSwipe){
        [self dismissOverLayView:self.successView position:Top];
        self.container.userInteractionEnabled = YES;
        
        if(appliedSuccessGuestureRecognizer){
            [self.container.superview removeGestureRecognizer:appliedSuccessGuestureRecognizer];
            appliedSuccessGuestureRecognizer = nil;
        }if(appliedSuccessGuestureRecognizerWithSwipe){
            [self.container.superview removeGestureRecognizer:appliedSuccessGuestureRecognizerWithSwipe];
            appliedSuccessGuestureRecognizerWithSwipe = nil;
        }
    }else{
        if(self.successView){
            [self dismissOverLayView:self.successView position:Top];
            self.container.userInteractionEnabled = YES;
        }else{
            self.container.userInteractionEnabled = YES;
        }
    }
}

-(void)showOverLayView:(UIView *)view atPosition:(enum MessagePosition)position{
    [self dismissSuccessView];
    [self dismissErrorView];
    int topOffset = _topOffset.intValue;
    if(IS_IPHONE_X){
        topOffset = 90;
    }
    if(IS_IPHONE_XS){
        topOffset = 0;
    }
        
    [self.container.superview insertSubview:view aboveSubview:self.container];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *viewsDictionary = @{@"displayView":view};
    if(viewsDictionary == nil){
        return;
    }
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[displayView]|" options:0 metrics:nil views:viewsDictionary];
    [self.container.superview addConstraints:horizontalConsts];
    if (position == Top) {
        beforeVerticalConstraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.container.superview attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    }
    else if(position == Bottom){
        beforeVerticalConstraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.container.superview attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    }
    
    [self.container.superview addConstraint:beforeVerticalConstraint];
    [self.container.superview layoutIfNeeded];
    [self.container.superview removeConstraint:beforeVerticalConstraint];
    if (position == Top) {
        afterVerticalConstraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.container.superview attribute:NSLayoutAttributeTop multiplier:1.0 constant:topOffset + CGRectGetMinY(self.container.frame)];
    }
    else if(position == Bottom){
        afterVerticalConstraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.container.superview attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    }
    [self.container.superview addConstraint:afterVerticalConstraint];
    view.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.container.superview layoutIfNeeded];
        view.alpha = 1;
    }];
}

-(void)dismissOverLayView:(UIView *)view position:(enum MessagePosition)position{
    if (!view.superview) {
        return;
    }
    if(afterVerticalConstraint){
    [self.container.superview removeConstraint:afterVerticalConstraint];
    }
    if(beforeVerticalConstraint){
    [self.container.superview addConstraint:beforeVerticalConstraint];
    }
    [UIView animateWithDuration:0.3 animations:^{
        view.alpha = 0;
        [self.container.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}
@end

