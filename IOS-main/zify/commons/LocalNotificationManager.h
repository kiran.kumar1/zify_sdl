//
//  LocalNotificationManager.h
//  zify
//
//  Created by Anurag S Rathor on 14/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripDrive.h"

enum LocalNotificationType {
    DRIVEALARM
};

#define NOTIFICATIONTYPESTRINGS @{@(DRIVEALARM) : @"DriveAlarm"}

@interface LocalNotificationManager : NSObject
+(LocalNotificationManager *)sharedInstance;
-(void)handleNotifcationSettingsWithLaunchOptions:(NSDictionary *)launchOptions;
-(void)cancelNotificationsWithType:(enum LocalNotificationType)notificationType;
-(void)cancelNotificationWithIdentifier:(NSString *)identifier andType:(enum LocalNotificationType)notificationType;
-(void)scheduleNotificationWithIdentifier:(NSString *)identifier andType:(enum LocalNotificationType)notificationType andMesssage:(NSString *)message andDate:(NSString *)fireDate;
-(void)refreshLocalNotifications;
-(void)handleTripDrivesNotifications:(NSArray *)tripDrives;
-(void) handleNotificationData:(NSDictionary *)userInfo;
-(void)handleUserSelectedAction:(NSString *)actionIdentifier andUserInfo:(NSDictionary *)userInfo;
-(void)handleTripDrivesNotificationAfterCreatingANewDrive:(TripDrive *)drive;

@end
