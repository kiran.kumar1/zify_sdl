//
//  PickerViewInitialiser.m
//  zify
//
//  Created by Anurag S Rathor on 29/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "PickerViewInitialiser.h"

@implementation PickerViewInitialiser

-(void)showInView:(UIView *)view{
    [self initialisePicker];
    TransparentView *transparentView = [[TransparentView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _actionSheet = transparentView;
    [transparentView addSubview:self];
    self.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *viewsDictionary = @{@"pickerView":self};
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[pickerView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[pickerView]|" options:0 metrics:nil views:viewsDictionary];
    [transparentView addConstraints:horizontalConsts];
    [transparentView addConstraints:verticalConsts];
    [view addSubview:transparentView];
    [view bringSubviewToFront:transparentView];
}
-(void) initialisePicker{

}

@end
