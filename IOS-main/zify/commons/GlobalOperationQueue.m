//
//  GlobalOperationQueue.m
//  zify
//
//  Created by Anurag S Rathor on 28/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "GlobalOperationQueue.h"

@implementation GlobalOperationQueue{
    NSOperationQueue *operationQueue;
}

+(GlobalOperationQueue *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[GlobalOperationQueue alloc]init];
    });
    return instance;
}

+(GlobalOperationQueue *)sharedChatInstance{
    static dispatch_once_t onceChatToken;
    static id chatInstance;
    dispatch_once(&onceChatToken, ^{
        chatInstance=[[GlobalOperationQueue alloc]init];
    });
    return chatInstance;
}

-(id)init{
    self = [super init];
    operationQueue = [[NSOperationQueue alloc] init];
    [operationQueue setMaxConcurrentOperationCount:1];
    return self;
}

-(void)addOpearation:(NSOperation *)opearation{
    [operationQueue addOperation:opearation];
}

-(void)addOperationBlock:(void(^)())block{
    [operationQueue addOperationWithBlock:block];
}
@end
