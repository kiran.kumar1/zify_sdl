//
//  LineView.m
//  zify
//
//  Created by Anurag S Rathor on 10/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#import "LineView.h"

@implementation LineView

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    float lineWidth = 1.0;
    if(_lineWidth) lineWidth = _lineWidth.floatValue;
    CGContextSetLineWidth(context, lineWidth);
    if(self.lineColor){
        CGContextSetStrokeColorWithColor(context, [self.lineColor CGColor]);
    } else{
        CGContextSetStrokeColorWithColor(context, [UIColorFromRGB(0XCCCCCC) CGColor]);
    }
    CGContextBeginPath (context);
    if(self.horizontalLine){
        CGContextMoveToPoint   (context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    }
    
    if(self.verticalLine){
        CGContextMoveToPoint   (context, CGRectGetMaxX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    }
    CGContextStrokePath(context);
}

@end
