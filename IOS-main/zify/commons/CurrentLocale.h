//
//  CurrentLocale.h
//  zify
//
//  Created by Anurag S Rathor on 15/11/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"

@interface CurrentLocale : NSObject
+(CurrentLocale *)sharedInstance;
-(NSArray *)getIdentityCardTyes;
-(NSString *) getIdentityCardType:(NSString *)value;
-(NSString *) getIdentityCardTypeValue:(NSString *)type;
-(NSString *)getMobileISOCode;
-(NSString *)getISOCode;
-(NSArray *)getRechargeAmounts;
-(NSString *)getCurrencyCode;
-(NSString *)getCurrencySymbol;
-(NSString *)getDistanceUnit;
-(NSString *) getDistanceSubUnit;
-(BOOL)isRazorpayPaymentEnabled;
-(NSString *)getLocaleString;
-(NSString *)getCurrencySymbolFromCurrencyCode:(NSString *)currencyCode;
-(BOOL)isGlobalLocale;
-(BOOL)isGlobalPayment;
-(NSString *)getCountryName;
-(NSString *)getUserCountryISOCodeString;
-(void)setLocaleValuesFromUser:(UserProfile *)userProfile;
-(BOOL)setGlobalLocaleFromCountry:(NSString *)country;
@end
