//
//  PushNotificationManager.m
//  zify
//
//  Created by Anurag S Rathor on 23/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushNotificationManager.h"
#import "PushNotificationView.h"
#import "PushAccessData.h"
#import "PushNotificationRequest.h"
#import "ServerInterface.h"
#import "UserProfile.h"
#import "GlobalOperationQueue.h"
#import <Quickblox/Quickblox.h>
#import "LocalisationConstants.h"
#import "AppDelegate.h"

#define CURRENT_PUSH_TOKEN @"currentPushToken"
#define CURRENT_PUSH_TOKEN_DATA @"currentPushTokenData"

@implementation PushNotificationManager{
    NSDictionary *notificationData;
    BOOL isAppLaunched;
    GlobalOperationQueue *globalQueue;
    GlobalOperationQueue *chatQueue;
    BOOL isUserLoggedIn;
    BOOL isUserChatLoggedIn;
}
@synthesize notificationData;

NSString * const RIDREQUESTNOTIFITICATION  = @"RIDEREQUEST";   //RIDEREQUEST
NSString * const RIDEREQUESTACCEPTACTION = @"RIDE_REQUEST_ACCEPT";
NSString * const RIDEREQUESTDECLINEACTION = @"RIDE_REQUEST_DECLINE";
NSString * const DRIVEALRAMLOCALNOTIFITICATION  = @"DRIVEALARM";
NSString * const DRIVEALARMREMINDACTION= @"DRIVE_ALARM_REMIND";
NSString * const CHATNOTIFITICATION  = @"CHAT";

+(PushNotificationManager *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[PushNotificationManager alloc] init];
    });
    return instance;
}

-(id)init{
    self = [super init];
    globalQueue = [GlobalOperationQueue sharedInstance];
    chatQueue = [GlobalOperationQueue sharedChatInstance];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNotificationDataOnResume) name:UIApplicationDidBecomeActiveNotification object:nil];
    isUserLoggedIn = false;
    isUserChatLoggedIn = false;
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)registerForNotifcationSettingsWithLaunchOptions:(NSDictionary *)launchOptions{
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        UIMutableUserNotificationAction *rideRequestDeclineAction;
        rideRequestDeclineAction = [[UIMutableUserNotificationAction alloc] init];
        [rideRequestDeclineAction setActivationMode:UIUserNotificationActivationModeBackground];
        [rideRequestDeclineAction setTitle:NSLocalizedString(CMON_PNMANAGER_DECLINE, nil)];
        [rideRequestDeclineAction setIdentifier:RIDEREQUESTDECLINEACTION];
        [rideRequestDeclineAction setDestructive:NO];
        [rideRequestDeclineAction setAuthenticationRequired:NO];

        UIMutableUserNotificationAction  *rideRequestAcceptAction;
        rideRequestAcceptAction = [[UIMutableUserNotificationAction alloc] init];
        [rideRequestAcceptAction setActivationMode:UIUserNotificationActivationModeBackground];
        [rideRequestAcceptAction setTitle:NSLocalizedString(CMON_PNMANAGER_ACCEPT, nil)];
        [rideRequestAcceptAction setIdentifier:RIDEREQUESTACCEPTACTION];
        [rideRequestAcceptAction setDestructive:NO];
        [rideRequestAcceptAction setAuthenticationRequired:NO];
        
        UIMutableUserNotificationCategory *actionCategory;
        actionCategory = [[UIMutableUserNotificationCategory alloc] init];
        [actionCategory setIdentifier:RIDREQUESTNOTIFITICATION];
        [actionCategory setActions:@[rideRequestAcceptAction, rideRequestDeclineAction]
                        forContext:UIUserNotificationActionContextDefault];
        
        UIMutableUserNotificationAction *driveAlarmRemindAction;
        driveAlarmRemindAction = [[UIMutableUserNotificationAction alloc] init];
        [driveAlarmRemindAction setActivationMode:UIUserNotificationActivationModeBackground];
        [driveAlarmRemindAction setTitle:NSLocalizedString(CMON_PNMANAGER_REMINDME, nil)];
        [driveAlarmRemindAction setIdentifier:DRIVEALARMREMINDACTION];
        [driveAlarmRemindAction setDestructive:NO];
        [driveAlarmRemindAction setAuthenticationRequired:NO];
        
        UIMutableUserNotificationCategory *driveAlarmActionCategory;
        driveAlarmActionCategory = [[UIMutableUserNotificationCategory alloc] init];
        [driveAlarmActionCategory setIdentifier:DRIVEALRAMLOCALNOTIFITICATION];
        [driveAlarmActionCategory setActions:@[driveAlarmRemindAction]
                                  forContext:UIUserNotificationActionContextDefault];
        
        UIMutableUserNotificationAction *chatAction;
        chatAction = [[UIMutableUserNotificationAction alloc] init];
        [chatAction setActivationMode:UIUserNotificationActivationModeForeground];
        [chatAction setTitle:NSLocalizedString(CHATS_CONTACTLIST_LBL_CHAT, nil)];
        [chatAction setIdentifier:CHATNOTIFITICATION];
        [chatAction setDestructive:NO];
        [chatAction setAuthenticationRequired:NO];
        
        UIMutableUserNotificationCategory *chatActionCategory;
        chatActionCategory = [[UIMutableUserNotificationCategory alloc] init];
        [chatActionCategory setIdentifier:CHATNOTIFITICATION];
        [chatActionCategory setActions:@[chatAction]
                                  forContext:UIUserNotificationActionContextDefault];
        
        NSSet *categories = [NSSet setWithObjects:actionCategory,driveAlarmActionCategory, chatActionCategory,nil];
        UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:categories];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]) {
        notificationData = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        NSLog(@"Notification Data is  %@",notificationData);
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_PUSH_TOKEN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_PUSH_TOKEN_DATA];
    isAppLaunched = true;
}

-(void) handleNotificationData:(NSDictionary *)userInfo{
    
    notificationData = userInfo;
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
        [self showNotificationData];
    }
}

-(void) showNotificationDataOnResume{
    if(!isAppLaunched){
        [self showNotificationData];
    }
}

-(void)showNotificationData{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    BOOL showNotificationData = notificationData !=nil ? true : false;
    if(showNotificationData){
        [PushNotificationView showPushNotificationView:notificationData andIsApplaunch:isAppLaunched];
        notificationData = nil;
    }
    isAppLaunched = false;
}

-(void)handleUserSelectedAction:(NSString *)actionIdentifier andUserInfo:(NSDictionary *)userInfo{
    [PushNotificationView handlePushNotificationAction:actionIdentifier andNotificationDict:userInfo];
    
}

-(void)handlePushDeviceToken:(id)deviceTokenData{
    if([deviceTokenData isKindOfClass:[NSData class]]){
       NSLog(@"handle method is called");
       NSString *deviceToken = [[deviceTokenData description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
       deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];

      [[NSUserDefaults standardUserDefaults] setObject:deviceTokenData forKey:CURRENT_PUSH_TOKEN_DATA];
      [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:CURRENT_PUSH_TOKEN];
    }
    if(isUserLoggedIn){
        [self handlePushRegisterWithUser];
    }
    if(isUserChatLoggedIn){
        [self handlePushRegisterForChat];
    }
}

-(void)handlePushRegisterWithUser{
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_PUSH_TOKEN];
    if(!deviceToken){
        deviceToken = @"";
    }
    if(deviceToken && [self isPushNotificationEnabledFromDevice]){
        __weak PushNotificationManager *weakSelf = self;
        [globalQueue addOperationBlock:^{
            [PushAccessData getPushAccessDatawithCompletion:^(NSString *currentToken,BOOL isUserDisabledPush){
                UserProfile *currentUser = [UserProfile getCurrentUser];
                NSString *serverToken = currentUser.pushToken;
                if(!serverToken){
                    serverToken = @"";
                }
                NSNumber *userId = currentUser.userId;
                if(!isUserDisabledPush){
                    if(serverToken != nil && ![@"" isEqualToString:serverToken]){
                        [weakSelf deregisterDeviceForPush:serverToken andUserId:userId andAsync:true andHandler:^(ServerResponse *response, NSError *error){
                            [weakSelf registerDeviceForPush:deviceToken andUserId:userId andAsync:true andHandler:^(ServerResponse *response, NSError *error){
                                if(response){
                                    [PushAccessData createPushAccessData:PUSHTOKENTYPE andValue:deviceToken];
                                }
                            }];
                        }];
                       /* [PushAccessData deletePushAccessData:PUSHTOKENTYPE];
                        [PushAccessData createPushAccessData:PUSHTOKENTYPE andValue:serverToken];//Remove old token if exist in DB and replace with server token.
                        if(![deviceToken isEqualToString:serverToken]){
                            [weakSelf deregisterDeviceForPush:serverToken andUserId:userId andAsync:true andHandler:^(ServerResponse *response, NSError *error){
                                if(response){
                                    [PushAccessData deletePushAccessData:PUSHTOKENTYPE];
                                    [weakSelf registerDeviceForPush:deviceToken andUserId:userId andAsync:true andHandler:^(ServerResponse *response, NSError *error){
                                        if(response){
                                            [PushAccessData createPushAccessData:PUSHTOKENTYPE andValue:deviceToken];
                                        }
                                    }];
                                }
                            }];
                        }*/
                    } else {
                        [PushAccessData deletePushAccessData:PUSHTOKENTYPE]; //If there is no server token and there is a token in app.
                        [weakSelf registerDeviceForPush:deviceToken andUserId:userId andAsync:true andHandler:^(ServerResponse *response, NSError *error){
                            if(response){
                                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                [prefs setBool:YES forKey:@"enabledPush"];
                                [prefs synchronize];
                                [PushAccessData createPushAccessData:PUSHTOKENTYPE andValue:deviceToken];
                            }
                        }];
                    }
                }
            }];
        }];
    }
    isUserLoggedIn = true;
}

-(void)handlePushRegisterForChat{
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_PUSH_TOKEN];
    NSData *deviceTokenData = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_PUSH_TOKEN_DATA];
    if(deviceToken && [self isPushNotificationEnabledFromDevice]){
        __weak PushNotificationManager *weakSelf = self;
        [chatQueue addOperationBlock:^{
            NSString *currentToken = [PushAccessData getPushAccessData:CHATPUSHTOKENTYPE];
            BOOL isUserDisabledPush = [PushAccessData getPushAccessData:USERDISABLEDPUSHTYPE].boolValue;
            if(!isUserDisabledPush){
                if(currentToken !=nil && ![deviceToken isEqualToString:currentToken]){
                    [PushAccessData deletePushAccessData:CHATPUSHTOKENTYPE];
                    [weakSelf deregisterChatForPushWithHandler:^(BOOL isSuccessful){
                        if(isSuccessful){
                            [PushAccessData deletePushAccessData:CHATPUSHTOKENTYPE];
                            [weakSelf registerChatForPush:deviceTokenData andHandler:^(BOOL isSuccessful){
                                if(isSuccessful){
                                    [PushAccessData createPushAccessData:CHATPUSHTOKENTYPE andValue:deviceToken];
                                }
                            }];
                        }
                    }];
                } else if(!currentToken){ //Existing token already registed.So there is a check not to register again if there is a token change.
                    [weakSelf registerChatForPush:deviceTokenData andHandler:^(BOOL isSuccessful){
                        if(isSuccessful){
                            [PushAccessData createPushAccessData:CHATPUSHTOKENTYPE andValue:deviceToken];
                        }
                    }];
                }
            }
        }];
    }
    isUserChatLoggedIn = true;
}


-(BOOL)isPushNotificationEnabledFromDevice{
    UIApplication *application = [UIApplication sharedApplication];
    BOOL enabled;
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        enabled = [application isRegisteredForRemoteNotifications];
    }
    else {
        UIRemoteNotificationType types = [application enabledRemoteNotificationTypes];
        enabled = types & UIRemoteNotificationTypeAlert;
    }
    return enabled;
}

-(BOOL)isPushNotificationEnabledFromApp{
    
    BOOL userDisabeledPush  = [PushAccessData getPushAccessData:USERDISABLEDPUSHTYPE].boolValue;
    BOOL pushAccessDataAvailable = [PushAccessData getPushAccessData:PUSHTOKENTYPE];
    return !userDisabeledPush && pushAccessDataAvailable;
}

-(void)changeUserPushSettings:(BOOL)enableSettings andHandler:(void (^)(NSString *,NSString *))handler{
    NSData *currentPushTokenData = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_PUSH_TOKEN_DATA];
    NSString *currentPushToken = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_PUSH_TOKEN];
    NSNumber *userId = [UserProfile getCurrentUserId];
    if(enableSettings){
        [self registerDeviceForPush:currentPushToken andUserId:userId andAsync:false  andHandler:^(ServerResponse *response, NSError *error){
            if(response){
                [PushAccessData deletePushAccessData:USERDISABLEDPUSHTYPE];
                [PushAccessData createPushAccessData:PUSHTOKENTYPE andValue:currentPushToken];
            }
            handler(response.message,error.userInfo[@"message"]);
        }];
        [self registerChatForPush:currentPushTokenData andHandler:^(BOOL isSuccesful){
            if(isSuccesful){
                [PushAccessData deletePushAccessData:CHATPUSHTOKENTYPE];
                [PushAccessData createPushAccessData:CHATPUSHTOKENTYPE andValue:currentPushToken];
            }
        }];
    } else{
        [self deregisterDeviceForPush:[PushAccessData getPushAccessData:PUSHTOKENTYPE] andUserId:userId andAsync:false andHandler:^(ServerResponse *response, NSError *error){
            if(response){
                [PushAccessData createPushAccessData:USERDISABLEDPUSHTYPE andValue:@"true"];
                [PushAccessData deletePushAccessData:PUSHTOKENTYPE];
            }
            handler(response.message,error.userInfo[@"message"]);
        }];
        [self deregisterChatForPushWithHandler:^(BOOL isSuccesful){
            if(isSuccesful){
                [PushAccessData deletePushAccessData:CHATPUSHTOKENTYPE];
            }
        }];
    }
}

-(void)deregisterDeviceForPushOnLogout{
    NSNumber *userId = [UserProfile getCurrentUserId];
    if(![self isPushNotificationEnabledFromDevice]) return;
    if([self isPushNotificationEnabledFromApp]){
        __weak PushNotificationManager *weakSelf = self;
        NSString *pushToken = [PushAccessData getPushAccessData:PUSHTOKENTYPE];
        PushNotificationRequest *request =[[PushNotificationRequest alloc] initWithRequestType:PUSHDERIGISTERREQUEST andUserId:userId andPushToken:pushToken]; //Moved this out since userobject will be deleted after this call
        [globalQueue addOperationBlock:^{
            [weakSelf deregisterDeviceForPushWithRequest:request andHandler:^(ServerResponse *response, NSError *error){
                
            }];
        }];
        [chatQueue addOperationBlock:^{
            [weakSelf deregisterChatForPushWithHandler:^(BOOL isSuccesful){
                
            }];
        }];
    }
    [PushAccessData deletePushAccessData:PUSHTOKENTYPE];
    [PushAccessData deletePushAccessData:USERDISABLEDPUSHTYPE];
    [PushAccessData deletePushAccessData:CHATPUSHTOKENTYPE];
}

-(void)registerDeviceForPush:(NSString *)pushToken andUserId:(NSNumber *)userId andAsync:(BOOL)async andHandler:(void (^)(ServerResponse *, NSError *))handler{
    PushNotificationRequest *request =[[PushNotificationRequest alloc] initWithRequestType:PUSHREGISTERREQUEST andUserId:userId andPushToken:pushToken];
    if(async){
        [[ServerInterface sharedInstance] getResponseAsync:request withHandler:^(ServerResponse *response, NSError *error){
            handler(response,error);
        }];
    } else{
        [[ServerInterface sharedInstance] getResponse:request withHandler:^(ServerResponse *response, NSError *error){
            handler(response,error);
        }];
    }
    
}

-(void)deregisterDeviceForPush:(NSString *)pushToken andUserId:(NSNumber *)userId andAsync:(BOOL)async andHandler:(void (^)(ServerResponse *, NSError *))handler{
    PushNotificationRequest *request =[[PushNotificationRequest alloc] initWithRequestType:PUSHDERIGISTERREQUEST andUserId:userId andPushToken:pushToken];
    if(async){
        [[ServerInterface sharedInstance] getResponseAsync:request withHandler:^(ServerResponse *response, NSError *error){
            handler(response,error);
        }];
    } else{
        [[ServerInterface sharedInstance] getResponse:request withHandler:^(ServerResponse *response, NSError *error){
            handler(response,error);
        }];
    }
}

-(void)deregisterDeviceForPushWithRequest:(PushNotificationRequest *)notificationRequest andHandler:(void (^)(ServerResponse *, NSError *))handler{
    [[ServerInterface sharedInstance] getResponseAsync:notificationRequest withHandler:^(ServerResponse *response, NSError *error){
        handler(response,error);
    }];
}

-(void)registerChatForPush:(NSData *)pushTokenData andHandler:(void(^)(BOOL))handler{
    NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    QBMSubscription *subscription = [QBMSubscription subscription];
    subscription.notificationChannel = QBMNotificationChannelAPNS;
    subscription.deviceUDID = deviceIdentifier;
    subscription.deviceToken = pushTokenData;
    
    [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
        handler(true);
    } errorBlock:^(QBResponse *response) {
        handler(false);
    }];
}

-(void)deregisterChatForPushWithHandler:(void(^)(BOOL))handler{
    NSString *deviceUdid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [QBRequest unregisterSubscriptionForUniqueDeviceIdentifier:deviceUdid successBlock:^(QBResponse *response) {
        handler(true);
    } errorBlock:^(QBError *error) {
        handler(false);
    }];
}
@end

