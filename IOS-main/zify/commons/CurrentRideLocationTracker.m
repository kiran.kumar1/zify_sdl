//
//  CurrentRideLocationTracker.m
//  zify
//
//  Created by Anurag S Rathor on 25/11/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import "CurrentRideLocationTracker.h"
#import "LocalisationConstants.h"

#define ONE_MINUTE 60000

#define REQUEST_ERROR_USER_INFO [NSDictionary dictionaryWithObject:NSLocalizedString(CMON_GENERIC_INTERNALERROR, nil) forKey:@"message"]
#define REQUEST_ERROR_CODE 1002
#define REQUEST_ERROR_DOMAIN @"ReachabilityError"

#define LOCATION_ERROR_USER_INFO [NSDictionary dictionaryWithObject:NSLocalizedString(CMON_LOCATION_TURNONLOCATION,nil) forKey:@"message"]
#define LOCATION_ERROR_CODE 1003
#define LOCATION_ERROR_DOMAIN @"LocationServiceError"


@implementation CurrentRideLocationTracker{
    CLLocationManager *locationManager;
    NSDate *timeSinceFirstLocationReceived;
    CLLocation *currentLocation;
    NSTimer *locationUpdateTimer;
}

+(CurrentRideLocationTracker *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[CurrentRideLocationTracker alloc]init];
    });
    return instance;
}

-(id) init{
    self = [super init];
    locationManager = [[CLLocationManager alloc] init];
    return self;
}

- (void)updateCurrentLocation{
    currentLocation = nil;
    if([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus]!=kCLAuthorizationStatusDenied){
        if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
            [locationManager requestWhenInUseAuthorization];
        }
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
        [self initialiseLocationUpdateTimer];
    } else{
        if(_currentRideLocationTrackerDelegate){
            NSError *error = [NSError errorWithDomain:LOCATION_ERROR_DOMAIN code:LOCATION_ERROR_CODE userInfo:LOCATION_ERROR_USER_INFO];
            [_currentRideLocationTrackerDelegate unableToFetchCurrentRideLocation:error];
            _currentRideLocationTrackerDelegate = nil;
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self handleLocationUpdateFailed];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    if(currentLocation!=nil){
        if([currentLocation.timestamp timeIntervalSinceDate:timeSinceFirstLocationReceived]<ONE_MINUTE){
            return;
        }
    }
    if(currentLocation==nil){
        currentLocation=newLocation;
        timeSinceFirstLocationReceived=[NSDate date];
        [locationManager stopUpdatingLocation];
        locationManager.delegate = nil;
        [locationUpdateTimer invalidate];
    }
    NSLog(@"%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
    dispatch_async(dispatch_get_main_queue(), ^{
        if(_currentRideLocationTrackerDelegate){
            [_currentRideLocationTrackerDelegate setCurrentRideLocation:currentLocation.coordinate];
            _currentRideLocationTrackerDelegate = nil;
        }
    });
}

-(void)initialiseLocationUpdateTimer{
    locationUpdateTimer = [NSTimer scheduledTimerWithTimeInterval: 20.0f
                                                     target: self
                                                   selector:@selector(handleLocationUpdateFailed)
                                                   userInfo: nil repeats:NO];
}

-(void)handleLocationUpdateFailed{
    // stop updating
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    currentLocation = nil;
    [locationUpdateTimer invalidate];
    if(_currentRideLocationTrackerDelegate){
        NSError *userError;
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            userError = [NSError errorWithDomain:LOCATION_ERROR_DOMAIN code:LOCATION_ERROR_CODE userInfo:LOCATION_ERROR_USER_INFO];
        } else{
            userError = [NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
        }
        [_currentRideLocationTrackerDelegate unableToFetchCurrentRideLocation:userError];
        _currentRideLocationTrackerDelegate = nil;
    }
}
@end
