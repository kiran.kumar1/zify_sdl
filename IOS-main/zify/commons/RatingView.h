//
//  RatingView.h
//  zify
//
//  Created by Anurag S Rathor on 05/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RatingView;

@protocol RateViewDelegate
- (void)rateView:(RatingView *)rateView ratingDidChange:(float)rating;
@end

@interface RatingView : UIView
@property (assign, nonatomic) float rating;
@property (assign) BOOL editable;
@property (assign) id <RateViewDelegate> delegate;
@property (nonatomic) NSNumber *spacing;
@property (nonatomic) NSNumber *leftSpacing;
- (id)initWithFrame:(CGRect)frame;
@end
