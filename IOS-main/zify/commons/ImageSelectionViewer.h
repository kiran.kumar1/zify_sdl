//
//  ImageSelectionViewer.h
//  zify
//
//  Created by Anurag S Rathor on 06/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//


@protocol  ImageSelectionViewerDelegate <NSObject>
-(void) userSelectedFinalImage:(UIImage *)image;
-(BOOL) allowImageEditing;
@end


@interface ImageSelectionViewer : NSObject<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic, weak) id<ImageSelectionViewerDelegate> imageSelectionDelegate;
@property (nonatomic) CGSize finalImageSize;
-(void) selectImage;
-(void) openGallery;
+(ImageSelectionViewer *)sharedInstance;
@end
