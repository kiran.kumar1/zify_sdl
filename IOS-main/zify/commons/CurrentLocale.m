//
//  CurrentLocale.m
//  zify
//
//  Created by Anurag S Rathor on 15/11/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "CurrentLocale.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "DBInterface.h"
#import "CurrencyInfo.h"
#import "CountryCode.h"
#import "LocalisationConstants.h"

@implementation CurrentLocale{
    NSLocale *currentLocale;
    NSString *currentLocaleString;
    NSString *currentCurrencyCode;
    NSString *currentDistanceUnit;
    NSString *currentSubDistanceUnit;
    NSString *currentCountryName;
    NSString *currentCurrencySymbol;
    NSString *countryISOCode;
    BOOL isGlobalLocale;
    BOOL isGlobalPayment;
    NSDictionary *identityCardTypes;
    NSDictionary *identityCardTypeValues;
    NSMutableDictionary *currencyCodes;
}

+(CurrentLocale *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[CurrentLocale alloc] init];
    });
    return instance;
}

-(id) init{
    self = [super init];
    currentLocale = [NSLocale currentLocale];
    currentLocaleString = [currentLocale localeIdentifier];
    currentCurrencyCode = @"INR";
    isGlobalLocale =  NO;
    currentDistanceUnit = @"kms";
    currentSubDistanceUnit = @"m";
    countryISOCode = @"IN";
    identityCardTypes = @{NSLocalizedString(CMON_CURRENTLOCALE_GOVTID,nil):@"GOVT_ID",NSLocalizedString(CMON_CURRENTLOCALE_PASSPORT,nil):@"PASSPORT",
                          NSLocalizedString(CMON_CURRENTLOCALE_DRIVINGLICENSE,nil):@"DRIVING_LICENSE"};
    identityCardTypeValues = @{@"GOVT_ID":NSLocalizedString(CMON_CURRENTLOCALE_GOVTID,nil),@"PASSPORT":NSLocalizedString(CMON_CURRENTLOCALE_PASSPORT,nil),
        @"DRIVING_LICENSE":NSLocalizedString(CMON_CURRENTLOCALE_DRIVINGLICENSE,nil)};
    currencyCodes = [[NSMutableDictionary alloc] init];
    return self;
}

-(NSArray *)getIdentityCardTyes{
    return [NSArray arrayWithObjects:NSLocalizedString(CMON_CURRENTLOCALE_GOVTID,nil),NSLocalizedString(CMON_CURRENTLOCALE_PASSPORT,nil),NSLocalizedString(CMON_CURRENTLOCALE_DRIVINGLICENSE,nil),nil];
}

-(NSString *) getIdentityCardType:(NSString *)value{
    NSString *idCardType = [identityCardTypes objectForKey:value];
    if(!idCardType) idCardType = [identityCardTypes objectForKey:@"Govt ID"];
    return idCardType;
}

-(NSString *) getIdentityCardTypeValue:(NSString *)type{
    NSString *idCardTypeValue = [identityCardTypeValues objectForKey:type];
    if(!idCardTypeValue) idCardTypeValue = [identityCardTypeValues objectForKey:@"GOVT_ID"];
    return idCardTypeValue;
}

-(NSString *)getMobileISOCode{
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = info.subscriberCellularProvider;
    if(carrier.mobileNetworkCode == nil || [carrier.mobileNetworkCode isEqualToString:@""]) {
        return nil;
    } else{
        return carrier.isoCountryCode.uppercaseString;
    }
}

-(NSString *)getISOCode{
    NSString *isoCode = [self getMobileISOCode];
    if(isoCode) return isoCode;
    return [currentLocale objectForKey:NSLocaleCountryCode];
}
-(NSArray *)getRechargeAmounts{
    if(!isGlobalLocale){
        return [NSArray arrayWithObjects:@"99",@"199",@"299",@"499",nil];
    } else{
        return [NSArray arrayWithObjects:@"25",@"50",@"75",@"100",nil];
    }
}

-(NSString *)getCurrencyCode{
    return currentCurrencyCode;
}

-(NSString *)getCurrencySymbol{
    return currentCurrencySymbol;  //[self getCurrencySymbolFromCurrencyCode:currentCurrencyCode];
}

-(NSString *) getDistanceUnit{
    return currentDistanceUnit;
}
-(NSString *) getDistanceSubUnit{
    return currentSubDistanceUnit;
}

-(BOOL)isRazorpayPaymentEnabled{
    if(!isGlobalLocale){
        return true;
    } else{
        return false;
    }
}

-(NSString *)getLocaleString{
    return currentLocaleString;
}
-(NSString *)getUserCountryISOCodeString{
    return countryISOCode;
}


-(NSString *)getCurrencySymbolFromCurrencyCode:(NSString *)currencyCode{
    NSString *currencySymbol = [currencyCodes objectForKey:currencyCode];
    if(!currencySymbol){
        CurrencyInfo *currencyInfo = [CurrencyInfo getCurrencyInfoForCurrencyCode:currencyCode inContext:[[DBInterface sharedInstance] sharedContext]];
        if(currencyInfo.useUnicode.intValue){
            /*NSData *symbolUnicodedData = [currencyInfo.unicode dataUsingEncoding:NSUTF8StringEncoding];
            currencySymbol = [[NSString alloc] initWithData:symbolUnicodedData encoding:NSUTF8StringEncoding];*/
            currencySymbol = currencyInfo.unicode;
        } else{
            NSLocale *locale = [NSLocale localeWithLocaleIdentifier:currencyInfo.locale];
            currencySymbol = [locale objectForKey:NSLocaleCurrencySymbol];
        }
        currencySymbol = currencySymbol != nil ? currencySymbol : @"";
        [currencyCodes setObject:currencySymbol != nil ? currencySymbol : @"" forKey:currencyCode];
    }
    return currencySymbol;
}

-(BOOL) isGlobalLocale{
    return isGlobalLocale;
}

-(BOOL)isGlobalPayment{
    return isGlobalPayment;
}

-(NSString *)getCountryName{
    return currentCountryName;
}


-(void)setLocaleValuesFromUser:(UserProfile *)userProfile{
  /*  NSString *userIsdCode = userProfile.isdCode;
    NSString *path = [[NSBundle mainBundle]pathForResource:@"countryCodes" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    NSArray *jsonArr = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
    NSLog(@"jsonArr is %@", jsonArr);
        for(NSDictionary *obj in jsonArr){
            NSString *isdCode = [obj objectForKey:@"isdCode"];
            if([isdCode isEqualToString:userIsdCode]){
                currentCurrencyCode = [obj objectForKey:@"currency"];
                break;
            }
    }
    NSLog(@"currentCurrencyCode is %@", currentCurrencyCode);
    if(currentCurrencyCode == nil){
        currentCurrencyCode = @"INR";
    }*/
  
    //currentCurrencyCode = userProfile.currency ? userProfile.currency : currentCurrencyCode;
    
    isGlobalLocale = userProfile.isGlobal.intValue ? YES : NO;
    isGlobalPayment = userProfile.isGlobalPayment.intValue ? YES : NO;
    currentDistanceUnit = userProfile.distanceUnit;
    CountryCode *countryObj = [CountryCode getCountryObjectForISDCode:userProfile.isdCode inContext:[[DBInterface sharedInstance] sharedContext]];
    currentCurrencyCode = countryObj.currency;
    countryISOCode = userProfile.country;
    if(currentCurrencyCode == nil){
        currentCurrencyCode = @"INR";
    }
    
    NSUserDefaults *prefs =[[NSUserDefaults alloc] initWithSuiteName:@"group.com.zify.extension"];
    [prefs setObject:currentCurrencyCode forKey:@"userCurrencyCodeFromIsoCode"];
    [prefs synchronize];
    currentCountryName = countryObj != nil ? countryObj.name : @"India";
    
    currentDistanceUnit = countryObj.distanceUnit;
    currentSubDistanceUnit = countryObj.distanceSubUnit;
    
    NSString *isoCode = userProfile.country;
    CurrencyInfo *obj = [CurrencyInfo getCurrencyInfoForCountryIsoCode:isoCode inContext:[[DBInterface sharedInstance] sharedContext]];
    currentLocaleString = obj.locale;
    currentCurrencySymbol = obj.unicode;
}

-(BOOL)setGlobalLocaleFromCountry:(NSString *)country{
    isGlobalLocale = country && [@"India" caseInsensitiveCompare:country] != NSOrderedSame ? YES : NO;
    return isGlobalLocale;
}


-(void)resetLocaleValues{
    currentCurrencyCode = @"INR";
    isGlobalLocale =  NO;
    isGlobalPayment = NO;
    currentDistanceUnit = @"kms";
    currentCountryName = @"India";
    currentSubDistanceUnit = @"m";
    currentLocaleString = @"en_IN";
    countryISOCode = @"IN";
}
@end
