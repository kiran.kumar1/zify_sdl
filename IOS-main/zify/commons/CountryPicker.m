//
//  CountryPicker.m
//  commons
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "CountryPicker.h"
#import "CountryCode.h"
#import "DBInterface.h"

@interface CountryPicker ()

@end

@implementation CountryPicker{
     NSFetchedResultsController *countryCodesResultsController;
     NSInteger selectedRow;
     NSInteger selectedComponent;
}

+(CountryCode*)getCountryObjectForISOCode:(NSString*)isoCode{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context = [[DBInterface sharedInstance] sharedMainContext];
    request.entity = [NSEntityDescription entityForName:@"CountryCode" inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    [predicates addObject:[NSPredicate predicateWithFormat:@"isoCode = %@", isoCode]];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }

}

+(CountryCode*)getCountryObjectForISDCode:(NSString*)isdCode{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context = [[DBInterface sharedInstance] sharedMainContext];
    request.entity = [NSEntityDescription entityForName:@"CountryCode" inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    [predicates addObject:[NSPredicate predicateWithFormat:@"isdCode = %@", isdCode]];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }
  
}
-(void)initialisePicker
{
    if (!countryCodesResultsController) {
        NSManagedObjectContext *managedObjectContext = [[DBInterface sharedInstance] sharedMainContext];
        NSFetchRequest *countryFetchRequest = [[NSFetchRequest alloc] init];
        countryFetchRequest.entity = [NSEntityDescription entityForName:@"CountryCode" inManagedObjectContext:managedObjectContext];
        countryFetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES], nil];
         countryFetchRequest.fetchBatchSize = 20 ;
        countryCodesResultsController =  [[NSFetchedResultsController alloc] initWithFetchRequest:countryFetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:@"countryCodesCache"];
        NSError *error;
        [countryCodesResultsController performFetch:&error];
        if (![countryCodesResultsController performFetch:&error]){
            NSLog(@"Error in fetching data %@", [error userInfo]);
        }
    }
    NSIndexPath *indexPath = [countryCodesResultsController indexPathForObject:self.selectedCountryCode];
    [self.countryCodePicker selectRow:indexPath.row inComponent:indexPath.section animated:NO];
    selectedRow = indexPath.row;
    selectedComponent = indexPath.section;
    [self.countryCodePicker reloadAllComponents];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if([[countryCodesResultsController sections] count] > 0){
        id<NSFetchedResultsSectionInfo> sectionInfo = [countryCodesResultsController sections][0];
        return [sectionInfo numberOfObjects];
    }else{
        return 0;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectedRow = row;
    selectedComponent = component;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    CountryCode *countryCode = [countryCodesResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:row inSection:component]];
    return [NSString stringWithFormat:@"%@ %@",countryCode.isdCode,countryCode.name];
}

- (IBAction)dismiss:(id)sender {
    [self.delegate selectedCountryCode:[countryCodesResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:selectedComponent]]];
    [self.actionSheet removeFromSuperview];
}
@end
