//
//  NSString+Random.h
//  zify
//
//  Created by Anurag S Rathor on 03/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Random)

+ (NSString *)randomAlphanumericStringWithLength:(NSInteger)length;

@end