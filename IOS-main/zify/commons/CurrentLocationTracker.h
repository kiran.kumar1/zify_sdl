//
//  CurrentLocationTracker.h
//  zify
//
//  Created by Anurag S Rathor on 23/09/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocalityInfo.h"
#import "MapLocationHelper.h"

@protocol CurrentLocationTrackerDelegate <NSObject>
-(void) setCurrentLocation:(LocalityInfo *)currentLocation;
-(void) unableToFetchCurrentLocation:(NSError *)error;
-(void)methodForLocationServicesAccessDenied;
@end

@interface CurrentLocationTracker : NSObject<CLLocationManagerDelegate,MapLocationHelperDelegate>
+(CurrentLocationTracker *)sharedInstance;
@property(nonatomic,strong) LocalityInfo *currentLocalityInfo;
@property (nonatomic, weak) id<CurrentLocationTrackerDelegate> currentLocationTrackerDelegate;
-(void)updateCurrentLocation;
-(BOOL) isLocationTrackingEnabled;
@end

