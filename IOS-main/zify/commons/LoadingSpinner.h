//
//  LoadingSpinner.h
//  zify
//
//  Created by Anurag S Rathor on 21/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoadingSpinner : UIView
/** Sets the line width of the spinner's circle. */
@property (nonatomic) CGFloat lineWidth;

/**
 *  Starts animation of the spinner.
 */
- (void)startAnimating;

/**
 *  Stops animation of the spinnner.
 */
- (void)stopAnimating;
@end
