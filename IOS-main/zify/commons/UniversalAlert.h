//
//  UniversalAlert.h
//  zify
//
//  Created by Anurag S Rathor on 26/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

enum UniversalButtonType{
    BUTTON_OK,BUTTON_CANCEL,BUTTON_OTHER
};


@interface UniversalAlert :  NSObject<UIAlertViewDelegate>
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *message;
@property (readonly) NSMutableArray *buttons;

-(id)initWithTitle:(NSString *)title WithMessage:(NSString *)msg;
-(void)addButton:(enum UniversalButtonType)type WithTitle:(NSString *)title WithAction:(void (^)(void *action))handler;
-(void)showInViewController:(UIViewController *)controller;
@end
