//
//  MapView.h
//  zify
//
//  Created by Anurag S Rathor on 20/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalityInfo.h"

@protocol MapViewDelegate <NSObject>
@optional
-(void)changedMapLocation:(LocalityInfo *)newLocation;
@end

@interface MapView : UIView
@property (nonatomic) CLLocationCoordinate2D sourceCoordinate;
@property (nonatomic) CLLocationCoordinate2D destCoordinate;
@property (nonatomic,strong) NSString *overviewPolylinePointsArray;
@property (nonatomic,strong) NSString *overviewPolylinePoints;
@property (nonatomic) BOOL resolveMapMoveGesture;
@property (nonatomic) UIEdgeInsets mapPadding;
@property (nonatomic, weak) id<MapViewDelegate> mapViewDelegate;

-(void)initialiseMapWithSourceCoordinate;
-(void)drawMap;
-(void)drawMarkers;
-(void)drawPathForIndex:(int)index;
-(void)modifyPathForIndex:(int)index;
-(void)updateTrackingMarkerWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude;
-(void)moveToLocation:(CLLocationCoordinate2D)location;
-(void)createMarkerWithhLat:(NSNumber *)latitude andLong:(NSNumber *)longitude andMarkerImage:(UIImage *)markerImage;
-(void)removeMarkersFromMap;
- (void)drawDashedLineOnMapBetweenOrigin:(CLLocation *)originLocation destination:(CLLocation *)destinationLocation;
-(void)createMarkerForPickupWithhLat:(NSNumber *)latitude andLong:(NSNumber *)longitude andMarkerImage:(UIImage *)markerImage withWalkingDistance:(NSString *)distance;
-(void)putMarkersOnSameMap;
@end
