//
//  TextAreaWithDone.m
//  zify
//
//  Created by Anurag S Rathor on 13/08/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "TextAreaWithDone.h"
#import "LocalisationConstants.h"

@implementation TextAreaWithDone
-(id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        UIToolbar* toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        toolBar.barStyle = UIBarStyleBlack;
        toolBar.tintColor = [UIColor colorWithRed:(239.0/255.0) green:(239.0/255.0) blue:(239.0/255.0) alpha:1.0];
        toolBar.items = [NSArray arrayWithObjects:
                         [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(CMON_GENERIC_DONE, nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)],
                         nil];
        [toolBar sizeToFit];
        self.inputAccessoryView = toolBar;
    }
    return self;
}

-(void)dismiss{
    [self resignFirstResponder];
}
@end
