//
//  UniversalAlert.m
//  zify
//
//  Created by Anurag S Rathor on 26/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UniversalAlert.h"

@interface UniversalButton : NSObject
@property (nonatomic,strong) NSString *title;
@property enum UniversalButtonType type;
@property (nonatomic,strong) void (^actionHandler)(void *);
@end

@implementation UniversalButton
@end

@interface UniversalAlertManager : NSObject

+(UniversalAlertManager *)sharedInstance;

-(void)setCurrentAlert:(UniversalAlert *)alert;

@end

@implementation UniversalAlertManager{
    UniversalAlert *currentAlert;
}

+(UniversalAlertManager *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[UniversalAlertManager alloc]init];
    });
    return instance;
}

-(void)setCurrentAlert:(UniversalAlert *)alert{
    currentAlert = alert;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UniversalButton *btn = [currentAlert.buttons objectAtIndex:buttonIndex];
    btn.actionHandler(NULL);
}

@end
@implementation UniversalAlert

-(id)initWithTitle:(NSString *)title WithMessage:(NSString *)msg
{
    self = [super init];
    if (self != nil)
    {
        _buttons = [[NSMutableArray alloc] init];
        _title = title;
        _message = msg;
    }
    
    return self;
}

-(void)addButton:(enum UniversalButtonType)type WithTitle:(NSString *)title WithAction:(void (^)(void *action))handler
{
    UniversalButton *btn = [[UniversalButton alloc] init];
    btn.title = title;
    btn.type = type;
    btn.actionHandler = handler;
    [_buttons addObject:btn];
}

-(void)showInViewController:(UIViewController *)controller
{
    if (NSClassFromString(@"UIAlertController") != nil)
    {
        [self show8:controller];
    }
    else
    {
        [self show7];
    }
}

-(void)show8:(UIViewController *)controller
{
    //iOS >= 8
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:_title
                                                                             message:_message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    for (UniversalButton *btn in _buttons)
    {
        UIAlertActionStyle style = UIAlertActionStyleDefault;
        if (btn.type == BUTTON_CANCEL) style = UIAlertActionStyleCancel;
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:btn.title style:style handler:^(UIAlertAction *action) {
            btn.actionHandler((__bridge void *)(action));
        }];
        [alertController addAction:alertAction];
    }
    [controller presentViewController:alertController animated:YES completion:nil];
    
}

-(void)show7
{
    [[UniversalAlertManager sharedInstance] setCurrentAlert:self];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:_title
                                                        message:_message
                                                       delegate:[UniversalAlertManager sharedInstance]
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil];
    
    for (UniversalButton *btn in _buttons)
    {
        if (btn.type == BUTTON_CANCEL)
        {
            [alertView setCancelButtonIndex:[alertView addButtonWithTitle:btn.title]];
        }
        else
        {
            [alertView addButtonWithTitle:btn.title];
        }
    }
    [alertView show];
}
@end
