//
//  CountryPicker.h
//  commons
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CountryCode.h"
#import "PickerViewInitialiser.h"

@protocol CountryPickerDelegate <NSObject>
-(void)selectedCountryCode:(CountryCode *)countryCode;
@end

@interface CountryPicker : PickerViewInitialiser
@property (nonatomic ,weak) id<CountryPickerDelegate> delegate;
@property (nonatomic,strong) CountryCode *selectedCountryCode;
@property (nonatomic,weak) IBOutlet UIPickerView *countryCodePicker;
+(CountryCode*)getCountryObjectForISDCode:(NSString*)isdCode;
+(CountryCode*)getCountryObjectForISOCode:(NSString*)isoCode;
@end
