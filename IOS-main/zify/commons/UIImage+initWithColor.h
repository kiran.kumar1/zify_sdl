//
//  UIImage+initWithColor.h
//  zify
//
//  Created by Anurag S Rathor on 25/12/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (initWithColor)
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
