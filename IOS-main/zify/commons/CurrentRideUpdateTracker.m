//
//  CurrentRideUpdateTracker.m
//  zify
//
//  Created by Anurag S Rathor on 01/10/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import "CurrentRideUpdateTracker.h"
#import "CurrentRideTracking.h"

#define ONE_MINUTE 60

@implementation CurrentRideUpdateTracker{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSDate *timeSinceLocationReceived;
    NSDate *startTime;
    NSNumber *duration;
    int count;
}

+(CurrentRideUpdateTracker *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[CurrentRideUpdateTracker alloc] init];
    });
    return instance;
}

-(id) init{
    self = [super init];
    locationManager = [[CLLocationManager alloc] init];
    return self;
}

-(void)updateCurrentRideInfoWithStartTime:(NSDate *)rideStartTime andDuration:(NSNumber *)rideDuration{
    startTime = rideStartTime;
    duration = rideDuration;
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
        [locationManager requestAlwaysAuthorization];
    }
    locationManager.allowsBackgroundLocationUpdates = YES;
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    /*    [locationManager stopUpdatingLocation];
     locationManager.delegate = nil;
     currentLocation = nil;*/
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    if(currentLocation && [newLocation.timestamp timeIntervalSinceDate:timeSinceLocationReceived]<ONE_MINUTE){
        return;
    }
    currentLocation=newLocation;
    timeSinceLocationReceived=[NSDate date];
    NSLog(@"%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
    [CurrentRideTracking updateCurrentTrackingObject:newLocation withCompletion:^(CurrentRideTracking *currentRide){
        NSString *status = currentRide.status;
        if(!currentRide || [@"COMPLETED" isEqualToString:status] || (currentLocation && [currentLocation.timestamp timeIntervalSinceDate:startTime]>(duration.intValue + 60)*60)){
            [locationManager stopUpdatingLocation];
            locationManager.delegate = nil;
            if(currentRide) [CurrentRideTracking updateWithLocationTrackingStatus:[NSNumber numberWithInt:1]];
        }
    }];
}

@end
