//
//  ImageHelper.h
//  zify
//
//  Created by Anurag S Rathor on 06/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageHelper : NSObject
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (NSString *)encodeToBase64String:(UIImage *)image;
+(UIImage *)imageWithImage:(UIImage *)image andTintColor:(UIColor *)color;
+(UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original scaledToSize:(CGSize)newSize;
+(UIImage*) drawImage:(UIImage*) fgImage inImage:(UIImage*)bgImage atPoint:(CGPoint)point;
+ (UIImage *)blurredImageWithImage:(UIImage *)sourceImage;
@end
