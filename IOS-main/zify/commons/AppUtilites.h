//
//  AppUtilites.h
//  zify
//
//  Created by Anurag S Rathor on 03/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <MessageUI/MessageUI.h>

@interface AppUtilites : NSObject<MFMailComposeViewControllerDelegate>
+(void)openAppStore;
+(void)shareEmailWithData:(NSDictionary *)emailDict withDelegate:(id<MFMailComposeViewControllerDelegate>)mailDelegate;
+(void)shareEmailWithData:(NSDictionary *)emailDict withDelegate:(id<MFMailComposeViewControllerDelegate>)mailDelegate addAttachmentArray:(NSArray *)attatchmentArray andFileType:(NSString *)fileType andMimeType:(NSString *)mimeType;
+(void)shareSMSWithDelegate:(id<MFMessageComposeViewControllerDelegate>)smsdelegate withMessage:(NSString *) message;
+(void)openCallAppWithNumber:(NSString *)phoneNumber;
+(UIViewController *)applicationVisibleViewController;
+(void)openHereMapsApp;
@end
