//
//  CurrentRideLocationTracker.h
//  zify
//
//  Created by Anurag S Rathor on 25/11/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol CurrentRideLocationTrackerDelegate <NSObject>
-(void) setCurrentRideLocation:(CLLocationCoordinate2D)currentLocation;
-(void) unableToFetchCurrentRideLocation:(NSError *)error;
@end


@interface CurrentRideLocationTracker : NSObject<CLLocationManagerDelegate>
+(CurrentRideLocationTracker *)sharedInstance;
@property (nonatomic, weak) id<CurrentRideLocationTrackerDelegate> currentRideLocationTrackerDelegate;
-(void)updateCurrentLocation;
@end
