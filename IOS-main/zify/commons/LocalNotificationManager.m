//
//  LocalNotificationManager.m
//  zify
//
//  Created by Anurag S Rathor on 14/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "LocalNotificationManager.h"
#import "ServerInterface.h"
#import "GlobalOperationQueue.h"
#import "GenericRequest.h"
#import "TripDrive.h"
#import "RideDriveActionRequest.h"
#import "CurrentDriveController.h"
#import "AppUtilites.h"
#import "CurrentTripNPendingRatingResponse.h"
//#import "DriveInvoiceController.h"
//#import "RideInvoiceController.h"
#import "LocalisationConstants.h"
#import "zify-Swift.h"


#define LOCAL_NOTIFICATION_TYPE @"notificationType"

@implementation LocalNotificationManager{
    GlobalOperationQueue *globalQueue;
    NSDateFormatter *dateTimeFormatter;
    NSDictionary *notificationData;
    BOOL isAppLaunched;
}

NSString * const DRIVEALARMNOTIFITICATION  = @"DRIVEALARM";

+(LocalNotificationManager *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[LocalNotificationManager alloc] init];
    });
    return instance;
}

-(id)init{
    self = [super init];
    globalQueue = [GlobalOperationQueue sharedInstance];
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    [dateTimeFormatter setLocale:locale];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationDataOnResume) name:UIApplicationDidBecomeActiveNotification object:nil];
    return self;
}


-(void)handleNotifcationSettingsWithLaunchOptions:(NSDictionary *)launchOptions{
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]) {
        notificationData = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    }
    isAppLaunched = true;
}

-(void)handleUserSelectedAction:(NSString *)actionIdentifier andUserInfo:(NSDictionary *)userInfo{
    NSDictionary *userInfoDict = [userInfo valueForKey:@"userInfo"];
    NSString* notificationType = [NSString stringWithFormat:@"%@",[userInfoDict valueForKey:LOCAL_NOTIFICATION_TYPE]];
    if ([notificationType isEqualToString:[NOTIFICATIONTYPESTRINGS objectForKey:@(DRIVEALARM)]]){
        if([@"DRIVE_ALARM_REMIND" isEqualToString:actionIdentifier]){
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [[NSDate date] dateByAddingTimeInterval:10 * 60];
            localNotification.alertBody = NSLocalizedString(CMON_LNMANAGER_TRIPSCHEDULED, nil);
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.userInfo = userInfoDict;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
    }
}

-(void) handleNotificationData:(NSDictionary *)userInfo{
    if([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
        notificationData = userInfo;
    }
}

-(void)handleNotificationDataOnResume{
    if(!isAppLaunched){
        BOOL showNotificationData = notificationData !=nil ? true : false;
        if(showNotificationData){
            [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:CURRENTTRIPNPENDINGRATING andUserLat:nil andUserLong:nil] withHandler:^(ServerResponse *response, NSError *error){
                CurrentTripNPendingRatingResponse *rideResponse = (CurrentTripNPendingRatingResponse *)response.responseObject;
                if(rideResponse.pendingDriveRating){
                    UINavigationController *currentDriveNavController = (UINavigationController *) [DriveInvoiceController createDriveInvoiceNavController];
                    DriveInvoiceController *driveInvoiceController = (DriveInvoiceController *)currentDriveNavController.topViewController;
                    driveInvoiceController.ratingResponse =  rideResponse;
                    driveInvoiceController.showEmptyDrive = false;
                    [[AppUtilites applicationVisibleViewController] presentViewController:currentDriveNavController animated:YES completion:nil];
                } else if(rideResponse.pendingRideRating){
                    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
                    UIViewController *visibleController = [AppUtilites applicationVisibleViewController];
                    //if(visibleController.navigationController) visibleController = visibleController.navigationController;
                    //[RideInvoiceController showRideInvoiceController:visibleController andRatingResponse:rideResponse andShowEmptyDrive:false];
                    
                    // push to ride navigation controller
                    UINavigationController *rideInvoiceNavController = (UINavigationController *) [RideInvoiceController createRideInvoiceNavController];
                    RideInvoiceController *rideInvoiceController = (RideInvoiceController *)rideInvoiceNavController.topViewController;
                    
                    
                    rideInvoiceController.ratingResponse = rideResponse;
                    rideInvoiceController.showEmptyDrive = false;
                    
                    
                    if(rootViewController.presentedViewController != nil){
                        [rootViewController dismissViewControllerAnimated:NO completion:nil];
                        visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
                        [visibleController presentViewController:rideInvoiceNavController animated:YES completion:nil];
                        
                    }else{
                        [visibleController presentViewController:rideInvoiceNavController animated:YES completion:nil];
                    }
                    
                    
                }else if(rideResponse.currentDrive){
                    //&& rideResponse.currentDrive.numOfSeats.intValue > 0
                    UINavigationController *currentDriveNavController = (UINavigationController *) [CurrentDriveController createCurrentDriveNavController];
                    CurrentDriveController *currentDriveController = (CurrentDriveController *)currentDriveNavController.topViewController;
                    currentDriveController.currentDrive =  rideResponse.currentDrive;
                    [[AppUtilites applicationVisibleViewController] presentViewController:currentDriveNavController animated:YES completion:nil];
                }
            }];
            notificationData = nil;
        }
    }
    isAppLaunched = false;
}

-(void)cancelNotificationsWithType:(enum LocalNotificationType)notificationType{
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int counter=0; counter<[notificationArray count]; counter++)
    {
        UILocalNotification* notification = [notificationArray objectAtIndex:counter];
        NSDictionary *userInfo = notification.userInfo;
        id notificationIdentifier = [userInfo valueForKey:[NOTIFICATIONTYPESTRINGS objectForKey:@(notificationType)]];
        if (notificationIdentifier){
             [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
}

-(void)cancelNotificationWithIdentifier:(NSString *)identifier andType:(enum LocalNotificationType)notificationType{
    UILocalNotification *notification = [self getNotificationWithIdentifier:identifier andType:notificationType];
    if(notification){
        [[UIApplication sharedApplication] cancelLocalNotification:notification];
    }
}

-(void)scheduleNotificationWithIdentifier:(NSString *)identifier andType:(enum LocalNotificationType)notificationType andMesssage:(NSString *)message andDate:(NSString *)fireDate{
    if(![self isLocalNotificationEnabled]) return;
    UILocalNotification *notification = [self getNotificationWithIdentifier:identifier andType:notificationType];
    NSString *srcAddress = [notification.userInfo objectForKey:@"sourceAddress"];
    NSString *destAddress = [notification.userInfo objectForKey:@"destAddress"];
    NSNumber *driveId = [notification.userInfo objectForKey:@"driveId"];
    NSString *status = [notification.userInfo objectForKey:@"status"];

    NSLog(@"Local notif is scheduled %@", notification);
    if(!notification){
       // [self competeNotificationScheduleWithIdentifier:identifier andType:notificationType andMesssage:message andDate:fireDate];
        [self competeNotificationScheduleWithIdentifier:identifier andType:notificationType andMesssage:message andDate:fireDate withSourceInfo:srcAddress withDestinationAddress:destAddress withDriveID:driveId withStatus:status];
    }
}

-(void)competeNotificationScheduleWithIdentifier:(NSString *)identifier andType:(enum LocalNotificationType)notificationType andMesssage:(NSString *)message andDate:(NSString *)fireDate withSourceInfo:(NSString *)sourceAddress withDestinationAddress:(NSString *)destAddress withDriveID:(NSNumber *)driveID withStatus:(NSString *)status{
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    NSLog(@"date is %@ fire date is %@", fireDate,[[dateTimeFormatter dateFromString:fireDate] dateByAddingTimeInterval:-20* 60] );
    localNotification.fireDate = [[dateTimeFormatter dateFromString:fireDate] dateByAddingTimeInterval:-20* 60];
    localNotification.alertBody = message;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    if(notificationType == DRIVEALARM && [localNotification respondsToSelector:@selector(setCategory:)]){
            localNotification.category = DRIVEALARMNOTIFITICATION;
    }
    if(status.length == 0){
        status = @"PENDING";
    }
    localNotification.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:identifier, [NOTIFICATIONTYPESTRINGS objectForKey:@(notificationType)],[NOTIFICATIONTYPESTRINGS objectForKey:@(notificationType)],LOCAL_NOTIFICATION_TYPE , sourceAddress,@"sourceAddress",destAddress,@"destAddress",driveID,@"driveId",status, @"status", nil];
    NSLog(@"localNotification userInfo is %@", localNotification.userInfo);
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}


-(UILocalNotification *)getNotificationWithIdentifier:(NSString *)identifier andType:(enum LocalNotificationType)notificationType{
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int counter=0; counter<[notificationArray count]; counter++)
    {
        UILocalNotification* notification = [notificationArray objectAtIndex:counter];
        NSDictionary *userInfo = notification.userInfo;
        NSString *notificationIdentifier =[NSString stringWithFormat:@"%@",[userInfo valueForKey:[NOTIFICATIONTYPESTRINGS objectForKey:@(notificationType)]]];
        if (notificationIdentifier && [identifier isEqualToString:notificationIdentifier]){
            return notification;
        }
    }
    return nil;
}

-(void)refreshLocalNotifications{
    if(![self isLocalNotificationEnabled]) return;
    __weak LocalNotificationManager *weakSelf = self;
    notificationData = nil;
    [globalQueue addOperationBlock:^{
        [[ServerInterface sharedInstance]getResponseAsync:[[GenericRequest alloc] initWithRequestType:UPCOMINGDRIVESREQUEST] withHandler:^(ServerResponse *response, NSError *error){
            if(response){
                NSArray *tripDrives = [NSMutableArray arrayWithArray:(NSArray *)response.responseObject];
                [weakSelf cancelNotificationsWithType:DRIVEALARM];
                [weakSelf handleTripDrivesNotifications:tripDrives];
            }
        }];
    }];
}

-(void)handleTripDrivesNotifications:(NSArray *)tripDrives{
    [tripDrives enumerateObjectsUsingBlock:^(id value, NSUInteger idx, BOOL *stop) {
        TripDrive *drive = (TripDrive *)value;
        if(![self isDateAlreadyCompleted:[dateTimeFormatter dateFromString:drive.tripTime]]){
           // [self competeNotificationScheduleWithIdentifier:drive.driveId.stringValue andType:DRIVEALARM andMesssage: NSLocalizedString(CMON_LNMANAGER_TRIPSCHEDULED, nil) andDate:drive.tripTime];
            
            [self competeNotificationScheduleWithIdentifier:drive.driveId.stringValue andType:DRIVEALARM andMesssage:NSLocalizedString(CMON_LNMANAGER_TRIPSCHEDULED, nil) andDate:drive.tripTime withSourceInfo:drive.srcAddress withDestinationAddress:drive.destAddress withDriveID:drive.driveId withStatus:drive.status];
            
        }
    }];
}
-(void)handleTripDrivesNotificationAfterCreatingANewDrive:(TripDrive *)drive{
    if(![self isDateAlreadyCompleted:[dateTimeFormatter dateFromString:drive.departureTime]]){
        
        [self competeNotificationScheduleWithIdentifier:drive.driveId.stringValue andType:DRIVEALARM andMesssage:NSLocalizedString(CMON_LNMANAGER_TRIPSCHEDULED, nil) andDate:drive.departureTime withSourceInfo:drive.srcAddress withDestinationAddress:drive.destAddress withDriveID:drive.driveId withStatus:drive.status];
        
     //   [self competeNotificationScheduleWithIdentifier:drive.driveId.stringValue andType:DRIVEALARM andMesssage: NSLocalizedString(CMON_LNMANAGER_TRIPSCHEDULED, nil) andDate:drive.departureTime];
    }
}

-(BOOL)isLocalNotificationEnabled{
    UIApplication *application = [UIApplication sharedApplication];
    BOOL enabled;
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        enabled = [application isRegisteredForRemoteNotifications];
    }
    else {
        UIRemoteNotificationType types = [application enabledRemoteNotificationTypes];
        enabled = types & UIRemoteNotificationTypeAlert;
    }
    return enabled;
}

-(BOOL)isDateAlreadyCompleted:(NSDate *)date{
    //NSLog(@"date aatha is %@", date);
    NSInteger seconds;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components: NSCalendarUnitSecond fromDate:[[NSDate date]dateByAddingTimeInterval:20* 60] toDate:date options:0];
    seconds = [components second];
    NSLog(@"seconds is %ld", (long)seconds);
    if(seconds < 0) {
        return true;
    }
    return false;
}
@end
