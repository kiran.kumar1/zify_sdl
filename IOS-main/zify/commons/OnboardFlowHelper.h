//
//  OnboardFlowHelper.h
//  zify
//
//  Created by Anurag S Rathor on 15/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnboardFlowHelper : NSObject
+(OnboardFlowHelper *)sharedInstance;
-(void)handleLoginFlow:(UIViewController *)controller andIsNewLogin:(BOOL)isNewLogin;
-(void)handleVerifyMobileFlow:(UIViewController *)controller;
-(void)handeUpgradeProfileTutFlow:(UIViewController *)controller;
-(void)handleIDCardDetailsFlow:(UIViewController *)controller;
-(void)setReminder:(UIViewController *)controller;
-(void)showUserHomeScreen:(UIViewController *)controller;
-(void)clearReminder;
@end
