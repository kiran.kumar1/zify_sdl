//
//  DottedLineView.h
//  zify
//
//  Created by Anurag S Rathor on 17/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DottedLineView : UIView
@property (nonatomic) Boolean horizontalLine;
@property (nonatomic) Boolean verticalLine;
@end
