//
//  MapLocationHelper.h
//  zify
//
//  Created by Anurag S Rathor on 23/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalityInfo.h"
//#import <NMAKit/NMAKit.h>

@protocol MapLocationHelperDelegate <NSObject>
@optional
-(void) selectedLocationInfo:(LocalityInfo *)locationInfo andError:(NSError *)error;
-(void) queryResultsForAutosuggest:(NSArray *)results;
@end

@interface MapLocationHelper : NSObject
@property (nonatomic, weak) id<MapLocationHelperDelegate> mapLocationDelegate;
//-(void)showPickOnMap;
-(void)showPickOnMap:(UINavigationController *)navCtrl;
-(void)getAutoSuggestResultsForQuery:(NSString *)query;
-(void)getLocationInfoFromAutosuggest:(id)autoSuggestResult;
-(void)getLocalityInfoWithCoordinate:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)currentAddress;
-(id)initWithDelegae:(id)delegate;
@end
