//
//  ChatManager.h
//  zify
//
//  Created by Anurag S Rathor on 04/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserContactDetail.h"
#import "QBChatUser.h"
#import "QBChatUserMessage.h"
#import <Quickblox/Quickblox.h>

@protocol ChatMessageDelegate
-(void)newMessageReceived:(NSString *)message andDate:(NSDate *)date;
-(NSString *)messageReceiverChatIdentifier;
@end


@interface ChatManager : NSObject<QBChatDelegate>
@property (nonatomic, assign) id<ChatMessageDelegate> chatMessageDelegate;

-(void)registerForChatSettings;
-(void)initialiseChat;
-(void)disconnectChatOnLogout;
-(void) setIsNewUserLogin:(BOOL)isNewUserLogin;
-(void)sendChatMessage:(NSString *)chatMessage toUser:(UserContactDetail *)userDetail andTime:(NSDate *)messageTime andChatDialog:(QBChatDialog *)dialog;
-(void)getChatConvIdForIdentifier:(NSString *)chatIdentifier withHandler:(void (^)(NSString *))handler;
-(void)createDialogWithConvId:(NSString *)convId andChatIdentifier:(NSString *)chatIdenfitier withHandler:(void (^)(QBChatDialog *))handler;
-(void)fetchMessageDialogs;
-(void)fetchMessagesWithConvId:(NSString *)convId withHandler:(void (^)(NSArray *))handler andReceivingUser:(UserContactDetail *)receivingUser andIsNewUser:(BOOL)isNewUser;
-(QBChatUser *)getChatUserForIdentifier:(NSString *)chatIdentifier;
-(NSArray *)getChatUserList;
-(NSArray *)getChatUserMessagesForChatIdentifier:(NSString *)chatIdentifier;
-(void)resetMessageUnreadCountForChatUser:(QBChatUser *)chatUser;
-(NSString *)currentUserChatIdentifier;
-(BOOL)isUserChatConnected;
-(void)deleteCurrentUserChatProfile;
-(void)moveToChatScreen:(id)sender;
+(ChatManager *)sharedInstance;

-(void)displayCustomNotificationBanner:(UserContactDetail *)detail withMessage:(NSString *)receivedMessage;
@end
