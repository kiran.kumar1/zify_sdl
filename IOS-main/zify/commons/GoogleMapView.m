//
//  GoogleMapVIew.m
//  zify
//
//  Created by Anurag S Rathor on 23/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "GoogleMapView.h"
//#import "NearestAvailableCar.h"
#import <SDWebImage/SDWebImageManager.h>
#import "ImageHelper.h"
#import "MapLocationHelper.h"
#import "AppDelegate.h"

@interface GoogleMapView()<GMSMapViewDelegate,MapLocationHelperDelegate>
@property(strong, nonatomic) GMSMapView *mapView;
@end
@implementation GoogleMapView{
    GMSMarker *sourceMarker;
    GMSMarker *destiationMarker;
    /*GMSPolyline *prevTrackingLine;
    GMSPolyline *currentTrackingLine;*/
    GMSMarker *trackingMarker;
    //GMSMutablePath *trackingPath;
    BOOL isUserNavigatedMap;
    GMSPolyline *prevPathLine;
    NSMutableArray *pathArray;
    MapLocationHelper *mapLocationHelper;
    //NSMutableArray *drawableMarkersArray;
}


-(void)initialiseMapWithSourceCoordinate{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.sourceCoordinate.latitude
                                                            longitude:self.sourceCoordinate.longitude
                                                                 zoom:15
                                                              bearing:0
                                                         viewingAngle:0];
    
    self.mapView = [GMSMapView mapWithFrame:self.frame camera:camera];
    self.mapView.mapType = kGMSTypeNormal;
    //self.mapView.myLocationEnabled = YES;
    self.mapView.settings.compassButton = YES;
    //self.mapView.settings.zoomGestures = YES;
    self.mapView.padding = self.mapPadding;
    NSError *error;
    GMSMapStyle *mapStyle = [GMSMapStyle styleWithContentsOfFileURL:[[NSBundle mainBundle] URLForResource:@"googlemapstylenew" withExtension:@"json"] error:&error];
    if(!mapStyle){
        NSLog(@"map style is not loaded");
    }
    self.mapView.mapStyle = mapStyle;
    [self addSubview:self.mapView];
    NSDictionary *viewsDictionary = @{@"mapView": _mapView};
    _mapView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[mapView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[mapView]|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:horizontalConsts];
    [self addConstraints:verticalConsts];
    [AppDelegate getAppDelegateInstance].googleMapObject = self.mapView;
    self.mapView.delegate = self;
    pathArray = [[NSMutableArray alloc] init];
    mapLocationHelper = [[MapLocationHelper alloc] init];
    mapLocationHelper.mapLocationDelegate = self;
}
-(id)getGoogleMapViewObject{
    return self.mapView;
}
-(void)drawMap{
    [self initialiseMapWithSourceCoordinate];
    [self drawMarkers];
    prevPathLine = nil;
    [self drawPathForIndex:0];
}


-(void)putMarkersOnSameMap{
    
    [self removeMarkersFromMap];
    [self drawMarkers];
    prevPathLine = nil;
    [self drawPathBasedOnPolyline];
}

-(void)drawPathBasedOnPolyline{
    GMSPolyline *pathLine = nil;//(index <= ((int)pathArray.count - 1)) ? [pathArray objectAtIndex:index] : nil;
    if(prevPathLine) {
        prevPathLine.map = nil;
        prevPathLine = nil;
    }
    if(!pathLine){
        GMSPath *path = [GMSPath pathFromEncodedPath:self.overviewPolylinePoints];
        pathLine = [GMSPolyline polylineWithPath:path];
        pathLine.strokeWidth = 3;
        //  pathLine.strokeColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
        pathLine.strokeColor = [UIColor colorWithRed:67.0/255.0 green:91.0/255.0 blue:108.0/255.0 alpha:1.0];
        NSLog(@"need to chnge the stroke color");
        //  pathLine.strokeColor = [UIColor greenColor];
        [pathArray addObject:pathLine];
    }
    pathLine.map = self.mapView;
    prevPathLine = pathLine;
    [self fitAllMarkers:pathLine.path withMap:pathLine.map];
    
}

/*-(void)drawDashedLinesBetweenTowSources:(CLLocation *)origin withDestination:(CLLocation *)destination{
    GMSPath *path = [GMSPath pathFromEncodedPath:self.overviewPolylinePoints];
    GMSPolyline *newPathLine  = [GMSPolyline polylineWithPath:path];
    newPathLine.strokeWidth = 3;
    // newPathLine.strokeColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
    newPathLine.strokeColor = [UIColor colorWithRed:67.0/255.0 green:91.0/255.0 blue:108.0/255.0 alpha:1.0];
    [pathArray setObject:newPathLine atIndexedSubscript:index];
    newPathLine.map = self.mapView;
    prevPathLine = newPathLine;
}*/

- (void)drawDashedLineOnMapBetweenOrigin:(CLLocation *)originLocation destination:(CLLocation *)destinationLocation {
    
    CGFloat distance = [originLocation distanceFromLocation:destinationLocation];
   // if (distance < kMinimalDistance) return;
    
    // works for segmentLength 22 at zoom level 16; to have different length,
    // calculate the new lengthFactor as 1/(24^2 * newLength)
    CGFloat lengthFactor = 2.7093020352450285e-09;
    CGFloat zoomFactor = pow(2, self.mapView.camera.zoom + 8);
   CGFloat segmentLength = 1.f / (lengthFactor * zoomFactor);
    CGFloat dashes = floor(distance / segmentLength);
    CGFloat dashLatitudeStep = (destinationLocation.coordinate.latitude - originLocation.coordinate.latitude) / dashes;
    CGFloat dashLongitudeStep = (destinationLocation.coordinate.longitude - originLocation.coordinate.longitude) / dashes;
    
    CLLocationCoordinate2D (^offsetCoord)(CLLocationCoordinate2D coord, CGFloat latOffset, CGFloat lngOffset) =
    ^CLLocationCoordinate2D(CLLocationCoordinate2D coord, CGFloat latOffset, CGFloat lngOffset) {
        return (CLLocationCoordinate2D) { .latitude = coord.latitude + latOffset,
            .longitude = coord.longitude + lngOffset };
    };
    
    GMSMutablePath *path = GMSMutablePath.path;
    NSMutableArray *spans = NSMutableArray.array;
    CLLocation *currentLocation = originLocation;
    [path addCoordinate:currentLocation.coordinate];
    
    while ([currentLocation distanceFromLocation:destinationLocation] > segmentLength) {
        CLLocationCoordinate2D dashEnd = offsetCoord(currentLocation.coordinate, dashLatitudeStep, dashLongitudeStep);
        [path addCoordinate:dashEnd];
        [spans addObject:[GMSStyleSpan spanWithColor:[UIColor colorWithRed:236.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0]]];
        CLLocationCoordinate2D newLocationCoord = offsetCoord(dashEnd, dashLatitudeStep / 2.f, dashLongitudeStep / 2.f);
        [path addCoordinate:newLocationCoord];
        [spans addObject:[GMSStyleSpan spanWithColor:UIColor.clearColor]];
        
        currentLocation = [[CLLocation alloc] initWithLatitude:newLocationCoord.latitude
                                                     longitude:newLocationCoord.longitude];
    }
    
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.map = self.mapView;
    polyline.spans = spans;
   // polyline.strokeColor = [UIColor orangeColor];
    polyline.strokeWidth = 2;
}
-(void)drawMarkers{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    if(sourceMarker) {
        sourceMarker.map = nil;
        sourceMarker = nil;
    }
    sourceMarker = [[GMSMarker alloc] init];
    sourceMarker.position = self.sourceCoordinate;
    sourceMarker.appearAnimation = kGMSMarkerAnimationPop;
    sourceMarker.map = self.mapView;
    sourceMarker.icon = [UIImage imageNamed:@"entry_icon11.png"];
  //  sourceMarker.icon = [UIImage imageNamed:@"icn_source_marker.png"];
    bounds = [bounds includingCoordinate:sourceMarker.position];
    
    if(destiationMarker) {
        destiationMarker.map = nil;
        destiationMarker = nil;
    }
    destiationMarker = [[GMSMarker alloc] init];
    destiationMarker.position = self.destCoordinate;
    destiationMarker.appearAnimation = kGMSMarkerAnimationPop;
    destiationMarker.map = self.mapView;
    destiationMarker.icon = [UIImage imageNamed:@"Exit_Icon11.png"];

   // destiationMarker.icon = [UIImage imageNamed:@"icn_destination_marker.png"];
    bounds = [bounds includingCoordinate:destiationMarker.position];
    [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
}

-(void)modifyPathForIndex:(int)index{
    prevPathLine.map = nil;
    prevPathLine = nil;
    GMSPath *path = [GMSPath pathFromEncodedPath:self.overviewPolylinePoints];
    GMSPolyline *newPathLine  = [GMSPolyline polylineWithPath:path];
    newPathLine.strokeWidth = 3;
   // newPathLine.strokeColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
    newPathLine.strokeColor = [UIColor colorWithRed:67.0/255.0 green:91.0/255.0 blue:108.0/255.0 alpha:1.0];
    [pathArray setObject:newPathLine atIndexedSubscript:index];
    newPathLine.map = self.mapView;
    prevPathLine = newPathLine;
}

-(void)drawPathForIndex:(int)index{
    GMSPolyline *pathLine = (index <= ((int)pathArray.count - 1)) ? [pathArray objectAtIndex:index] : nil;
    if(prevPathLine) {
        prevPathLine.map = nil;
        prevPathLine = nil;
    }
    if(!pathLine){
        GMSPath *path = [GMSPath pathFromEncodedPath:self.overviewPolylinePoints];
        pathLine = [GMSPolyline polylineWithPath:path];
        pathLine.strokeWidth = 3;
      //  pathLine.strokeColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
        pathLine.strokeColor = [UIColor colorWithRed:67.0/255.0 green:91.0/255.0 blue:108.0/255.0 alpha:1.0];
        NSLog(@"need to chnge the stroke color");
      //  pathLine.strokeColor = [UIColor greenColor];
        [pathArray addObject:pathLine];
    }
    pathLine.map = self.mapView;
    prevPathLine = pathLine;
    [self fitAllMarkers:pathLine.path withMap:pathLine.map];

}
-(void)fitAllMarkers:(GMSPath *)path withMap:(GMSMapView *)map{
    @try{
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        for (int index = 0; index<path.count; index++){
            bounds = [bounds includingCoordinate:[path coordinateAtIndex:index]];
        }
        GMSCameraUpdate *update =[GMSCameraUpdate fitBounds:bounds];
        [map animateWithCameraUpdate:update];
    }@catch(NSException *exception){
        NSLog(@"exception is %@", [exception description]);
    }
}
-(void)updateTrackingMarkerWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
    if(!trackingMarker){
        trackingMarker   = [[GMSMarker alloc] init];
        trackingMarker.appearAnimation = kGMSMarkerAnimationPop;
        trackingMarker.position = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue);
        trackingMarker.icon = [UIImage imageNamed:@"tracking_car.png"];
        trackingMarker.map = self.mapView;
    } else{
        trackingMarker.position = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.
                                                             doubleValue);
    }
    [self.mapView animateToLocation:trackingMarker.position];
}


-(void)moveToLocation:(CLLocationCoordinate2D)location{
    [self.mapView animateToLocation:location];
}

-(void)createMarkerWithhLat:(NSNumber *)latitude andLong:(NSNumber *)longitude andMarkerImage:(UIImage *)markerImage{
    GMSMarker *marker   = [[GMSMarker alloc] init];
    marker.appearAnimation = kGMSMarkerAnimationNone;
    marker.position = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue);
    marker.icon = markerImage;
    marker.map = self.mapView;

    //marker ad
}

-(void)createMarkerForPickupWithhLat:(NSNumber *)latitude andLong:(NSNumber *)longitude andMarkerImage:(UIImage *)markerImage withWalkingDistance:(NSString *)distance{
    GMSMarker *marker   = [[GMSMarker alloc] init];
    marker.appearAnimation = kGMSMarkerAnimationNone;
    marker.position = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue);
    marker.icon = markerImage;
    
    marker.title = @"Distance";
    marker.snippet = distance;
    marker.map = self.mapView;
    marker.map.selectedMarker = marker;
    
    //marker ad
}


-(void)removeMarkersFromMap{
    [self.mapView clear];
    /*
    for(GMSMarker *obj in markersArr){
        obj.map  = nil;
    }*/
}
/*-(void)drawMarkersForAvailableCarsResponse:(NearestAvailableCarsResponse *)availableCarsResponse{
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:availableCarsResponse.carImgUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if(image != nil){
            for(GMSMarker *marker in drawableMarkersArray){
                marker.map = nil;
            }
            drawableMarkersArray = [[NSMutableArray alloc] init];
            for(NearestAvailableCar *car in availableCarsResponse.locationList){
                GMSMarker *carMarker = [[GMSMarker alloc] init];
                carMarker.appearAnimation = kGMSMarkerAnimationPop;
                carMarker.position = CLLocationCoordinate2DMake(car.latitude.doubleValue, car.longitude.doubleValue);
                carMarker.icon = image;
                carMarker.map = self.mapView;
                [drawableMarkersArray addObject:carMarker];
            }
        }
    }];
}*/

#pragma mark - Google Map View Delegate

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    if(gesture) isUserNavigatedMap = true;
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    //if(isUserNavigatedMap)
    {
        isUserNavigatedMap = false;
        if(self.resolveMapMoveGesture) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [mapLocationHelper getLocalityInfoWithCoordinate:position.target andAddress:nil];
            });
        }
    }
}
#pragma mark - MapLocationHelperDelegate methods

-(void) selectedLocationInfo:(LocalityInfo *)locationInfo andError:(NSError *)error{
    if(error != nil){
        
    } else{
        [self.mapViewDelegate changedMapLocation:locationInfo];
    }

}

/*-(void)createTrackingMarkerWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
    trackingMarker   = [[GMSMarker alloc] init];
    trackingMarker.appearAnimation = kGMSMarkerAnimationPop;
    trackingMarker.position = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue);
    trackingMarker.icon = [UIImage imageNamed:@"tracking_car.png"];
    trackingMarker.map = self.mapView;
    [self.mapView animateToZoom:15];
    [self.mapView animateToLocation:trackingMarker.position];
}
-(void) addPolyLine:(NSArray *)pathArray andLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
    prevTrackingLine = currentTrackingLine;
    trackingMarker.position = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue);
    [self.mapView animateToLocation:trackingMarker.position];
   if(!trackingPath || _refreshTrackingpath){
        trackingPath = [GMSMutablePath path];
        for(int index = 0;index < pathArray.count ; index+=2){
           NSNumber *latitude = [pathArray objectAtIndex:index];
           NSNumber *longitude = [pathArray objectAtIndex:index + 1];
           [trackingPath addLatitude:latitude.doubleValue longitude:longitude.doubleValue];
       }
       if(_refreshTrackingpath) _refreshTrackingpath = false;
   } else{
        [trackingPath addLatitude:latitude.doubleValue longitude:longitude.doubleValue];
   }
    currentTrackingLine = [GMSPolyline polylineWithPath:trackingPath];
    currentTrackingLine.strokeWidth = 3;
    currentTrackingLine.strokeColor = [UIColor greenColor];
    currentTrackingLine.map = self.mapView;
    if(prevTrackingLine) prevTrackingLine.map = nil;
}
-(void) addPolyLineString:(NSString *)path andLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
    prevTrackingLine = currentTrackingLine;
    trackingMarker.position = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue);
    [self.mapView animateToLocation:trackingMarker.position];
    currentTrackingLine = [GMSPolyline polylineWithPath:[GMSMutablePath pathFromEncodedPath:path]];
    currentTrackingLine.strokeWidth = 3;
    currentTrackingLine.strokeColor = [UIColor greenColor];
    currentTrackingLine.map = self.mapView;
    if(prevTrackingLine) prevTrackingLine.map = nil;
}*/
@end
