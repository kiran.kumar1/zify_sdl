//
//  UIUnderlinedButton.h
//  zify
//
//  Created by Anurag S Rathor on 04/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIUnderlinedButton : UIButton

@end
