//
//  PickerViewInitialiser.h
//  zify
//
//  Created by Anurag S Rathor on 29/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransparentView.h"

@interface PickerViewInitialiser : UIView
@property(nonatomic, weak) TransparentView *actionSheet;
-(void) initialisePicker;
-(void)showInView:(UIView *)view;
@end
