//
//  MessageHandler.h
//  zify
//
//  Created by Anurag S Rathor on 27/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoadingSpinner.h"

@interface MessageHandler : NSObject
@property (nonatomic,strong) IBOutlet UIView *blockingView;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *blockingViewIndicator;
@property (nonatomic,strong) IBOutlet UIView *errorView;
@property (nonatomic, weak) IBOutlet UILabel *errorLabel;
@property (nonatomic, weak) IBOutlet UIView *container;
@property (nonatomic, weak) IBOutlet UILabel *successLabel;
@property (strong, nonatomic) IBOutlet UIView *successView;
@property (nonatomic,strong) IBOutlet UIView *transparentBlockingView;
@property (nonatomic,strong) IBOutlet LoadingSpinner *loadingSpinner;
@property (nonatomic) NSNumber *topOffset;
-(void)showBlockingLoadViewWithHandler:(void (^)())handler;
-(void)dismissBlockingLoadViewWithHandler:(void (^)())handler;
-(void)showErrorMessage:(NSString *)message;
-(void)dismissErrorView;
-(void)showSuccessMessage:(NSString *)message;
-(void)dismissSuccessView;
-(void)showTransparentBlockingLoadViewWithHandler:(void (^)())handler;
-(void)dismissTransparentBlockingLoadViewWithHandler:(void (^)())handler;
@end
