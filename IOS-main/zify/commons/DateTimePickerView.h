//
//  DateTimePicker.h
//  zify
//
//  Created by Anurag S Rathor on 26/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PickerViewInitialiser.h"

@protocol DateTimePickerDelegate <NSObject>
-(void) selectedDate:(NSDate *)selectedDate;
@end

@interface DateTimePickerView : PickerViewInitialiser
@property (nonatomic,weak) id<DateTimePickerDelegate> delegate;
@property (nonatomic) BOOL isTimePicker;
@property (nonatomic) BOOL isDatePicker;
@property (nonatomic) NSDate *selectedDate;
@end
