//
//  HTTPClient.h
//  zify
//
//  Created by Anurag S Rathor on 04/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface HTTPClient : NSObject
-(NSData *)postResponseDataForRequestURL:(NSURL *)url params:(NSDictionary *)params andError:(NSError **)error andIsJSONRequestFormat:(BOOL)isJSONFormat;
-(NSData *)getResponseDataForRequestURL:(NSURL *)url params:(NSDictionary *)params andError:(NSError **)error;
-(void)setTimeOutForPostRequest:(NSInteger)timeInSeconds;
@end

@interface HTTPSConnectionHandler : NSObject <NSURLConnectionDataDelegate>
@property (nonatomic, assign) BOOL finished;
@property (nonatomic) BOOL ssl;
-(NSData *)fetchRequest:(NSURLRequest *)request returningResponse:(NSURLResponse **)response error:(NSError **)connectionError;
@end