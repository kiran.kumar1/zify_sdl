//
//  RatingView.m
//  zify
//
//  Created by Anurag S Rathor on 05/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "RatingView.h"

@implementation RatingView{
    UIImage *notSelectedImage;
    UIImage *halfSelectedImage;
    UIImage *fullSelectedImage;
    NSMutableArray *imageViews;
    int maxRating;
    int midMargin;
    int leftMargin;
    CGSize minImageSize;
}


- (void)baseInit {
    notSelectedImage = [UIImage imageNamed:@"rating_unselected.png"];
    halfSelectedImage = [UIImage imageNamed:@"rating_selected.png"];
    fullSelectedImage = [UIImage imageNamed:@"rating_selected.png"];;
    _rating = 0;
    _editable = NO;
    imageViews = [[NSMutableArray alloc] init];
    maxRating = 5;
    midMargin = 5;
    leftMargin = 0;
    minImageSize = CGSizeMake(5, 5);
    _delegate = nil;
    [self addImageViews];
}

-(void) addImageViews{
    // Remove old image views
    for(int i = 0; i < imageViews.count; ++i) {
        UIImageView *imageView = (UIImageView *) [imageViews objectAtIndex:i];
        [imageView removeFromSuperview];
    }
    [imageViews removeAllObjects];
    
    // Add new image views
    for(int i = 0; i < maxRating; ++i) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [imageViews addObject:imageView];
        [self addSubview:imageView];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self baseInit];
    }
    return self;
}

-(void) drawRect:(CGRect)rect{
    int spacing = midMargin;
    int leftSpacing = leftMargin;
    if(_spacing) spacing = _spacing.intValue;
    if(_leftSpacing) leftSpacing = _leftSpacing.intValue;
    NSUInteger spacingFactor = _editable ? imageViews.count -1 : imageViews.count;
    float desiredImageWidth = (self.frame.size.width - (leftSpacing*2) - (spacing*spacingFactor)) / imageViews.count;
    float imageWidth = MAX(minImageSize.width, desiredImageWidth);
    float imageHeight = MAX(minImageSize.height, self.frame.size.height);
    
    for (int i = 0; i < imageViews.count; ++i) {
        UIImageView *imageView = [imageViews objectAtIndex:i];
        CGRect imageFrame = CGRectMake(leftSpacing + i*(spacing+imageWidth), 0, imageWidth, imageHeight);
        imageView.frame = imageFrame;
    }
}

- (void)refresh {
    for(int i = 0; i < imageViews.count; ++i) {
        UIImageView *imageView = [imageViews objectAtIndex:i];
        if (self.rating >= i+1) {
            imageView.image = fullSelectedImage;
        } else if (self.rating > i) {
            imageView.image = halfSelectedImage;
        } else {
            imageView.image = notSelectedImage;
        }
    }
}

- (void)setRating:(float)rating {
    _rating = rating;
    [self refresh];
}

- (void)handleTouchAtLocation:(CGPoint)touchLocation {
    if (!self.editable) return;
    
    int newRating = 0;
    for(int i = (int)imageViews.count - 1; i >= 0; i--) {
        UIImageView *imageView = [imageViews objectAtIndex:i];
        if (touchLocation.x > imageView.frame.origin.x) {
            newRating = i+1;
            break;
        }
    }
    self.rating = newRating;
    [self refresh];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.delegate rateView:self ratingDidChange:self.rating];
}
@end
