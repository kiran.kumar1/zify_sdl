//
//  CustomPickerView.h
//  zify
//
//  Created by Anurag S Rathor on 27/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerViewInitialiser.h"

@protocol CustomPickerDelegate <NSObject>
-(void) selectedPickerValue:(NSString *)value;
@end

@interface CustomPickerView : PickerViewInitialiser
@property (nonatomic,strong) NSArray *pickervalues;
@property (nonatomic,strong) NSString *currentValue;
@property (nonatomic,weak) IBOutlet UIPickerView *picker;
@property (nonatomic,weak) id<CustomPickerDelegate> delegate;
@end
