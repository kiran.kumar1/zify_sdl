//
//  UIUnderlinedButton.m
//  zify
//
//  Created by Anurag S Rathor on 04/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UIUnderlinedButton.h"

@implementation UIUnderlinedButton

- (void) drawRect:(CGRect)rect {
    CGRect textRect = self.titleLabel.frame;
    //CGFloat descender = self.titleLabel.font.descender;
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(contextRef, self.titleLabel.textColor.CGColor);
    CGContextSetLineWidth(contextRef, 1.0);
    CGContextMoveToPoint(contextRef, textRect.origin.x, textRect.origin.y + textRect.size.height);
    CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width, textRect.origin.y+ textRect.size.height);
    CGContextStrokePath(contextRef);
}

@end
