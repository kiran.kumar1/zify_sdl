//
//  FacebookLogin.h
//  zify
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//
#import "FBLoginResponse.h"

@protocol FaceBookLoginDelgate <NSObject>
@optional
-(void)loggedInWithFaceBook:(FBLoginResponse *)response;
-(void)unableToLoginFBWithError:(NSError *)error;
-(void)postFeedToFacebook;
@end

@interface FacebookLogin : NSObject{

}
+(FacebookLogin *)sharedInstance;
@property (nonatomic,assign) id<FaceBookLoginDelgate>faceBookLoginDelgate;

-(void)loginWithFacebook;
-(void)shareWithFacebook;
-(void)closeSessionAndClearSavedTokens;
-(void)postFeedToFacebookWithParams:(NSDictionary *)params;
-(NSString *)currentAccessToken;
@end
