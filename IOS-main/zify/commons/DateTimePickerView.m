//
//  DateTimePicker.m
//  zify
//
//  Created by Anurag S Rathor on 26/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "DateTimePickerView.h"

@implementation DateTimePickerView{
    UIDatePicker *picker;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      
    }
    return self;
}

- (IBAction)dismiss:(id)sender {
   // [self convertDateFormat:picker.date];
    [self.delegate selectedDate:picker.date];
    [self.actionSheet removeFromSuperview];
}
-(void)convertDateFormat:(NSDate *)choosenDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"en_GB"];
    dateFormatter.locale = locale;
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *convertedString = [dateFormatter stringFromDate:choosenDate];
    NSLog(@"Converted String : %@",convertedString);
    
    
}
-(void) initialisePicker{
    if(picker){
        [picker removeFromSuperview];
        picker = nil;
    }
    if(!picker){
        picker = [[UIDatePicker alloc] init];
        if(_isTimePicker){
            picker.datePickerMode = UIDatePickerModeTime;
            BOOL isTimein24 = [[NSUserDefaults standardUserDefaults] boolForKey:@"24hoursFormatEnabled"];
            if(!isTimein24){
              picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_GB"];
            }else{
                picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
            }
            //picker.minuteInterval = 15;
        } else if(_isDatePicker){
             picker.datePickerMode = UIDatePickerModeDate;
        }else{
          //  picker.minuteInterval = 15;
            picker.datePickerMode = UIDatePickerModeDateAndTime;
            picker.minimumDate = [NSDate date];
            picker.maximumDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*365];
            //fr_FR
            //en_GB
            NSString * language = [[NSLocale preferredLanguages] firstObject];
            NSLog(@"lang is %@", language);
            BOOL isTimein24 = [[NSUserDefaults standardUserDefaults] boolForKey:@"24hoursFormatEnabled"];
            if(!isTimein24){
                // 24 hours format
               picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_GB"];
            }else{
                // 12 hours format
                picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
            }
        }
        [self addSubview:picker];
        picker.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *viewsDictionary = @{@"picker":picker};
        NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[picker]|" options:0 metrics:nil views:viewsDictionary];
        NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-44-[picker]|" options:0 metrics:nil views:viewsDictionary];
        [picker addConstraint:[NSLayoutConstraint constraintWithItem:picker attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:216.0f]];
        [self addConstraints:horizontalConsts];
        [self addConstraints:verticalConsts];
    }
   // NSLog(@"selected date is %@", _selectedDate);
    if(_selectedDate){
        picker.date = _selectedDate;
    }
}
@end
