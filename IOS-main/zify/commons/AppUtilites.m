//
//  AppUtilites.m
//  zify
//
//  Created by Anurag S Rathor on 03/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "AppUtilites.h"


@implementation AppUtilites

+(void)openAppStore{
    static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@";
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:iOSAppStoreURLFormat, @(1003822874)]];
    [[UIApplication sharedApplication] openURL:url];
}

+(void)shareEmailWithData:(NSDictionary *)emailDict withDelegate:(id<MFMailComposeViewControllerDelegate>)mailDelegate{
    [self presentEmailViewWithParams:emailDict addAttachmentData:nil mimeType:nil fileType:nil delegate:mailDelegate];
}

+(void)shareEmailWithData:(NSDictionary *)emailDict withDelegate:(id<MFMailComposeViewControllerDelegate>)mailDelegate addAttachmentArray:(NSArray *)attatchmentArray andFileType:(NSString *)fileType andMimeType:(NSString *)mimeType{
    [self presentEmailViewWithParams:emailDict addAttachmentData:attatchmentArray mimeType:mimeType fileType:fileType delegate:mailDelegate];
}

+(void)presentEmailViewWithParams:(NSDictionary *)emailParams addAttachmentData:(NSArray *)attatchmentArray mimeType:(NSString *)mimeType fileType:(NSString *)fileType delegate:(id<MFMailComposeViewControllerDelegate>)mailDelegate {
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = mailDelegate;
        [mailViewController setSubject:emailParams[@"subject"]];
        [mailViewController setMessageBody:emailParams[@"messageBody"] isHTML:NO];
        [mailViewController setToRecipients:[NSArray arrayWithObjects:emailParams[@"recipients"], nil]];
        if (attatchmentArray) {
            for(int index = 0;index < attatchmentArray.count;index++){
                NSString *fileName = [NSString stringWithFormat:@"file%d.%@",index,fileType];
                [mailViewController addAttachmentData:attatchmentArray[index] mimeType:mimeType fileName:fileName];
            }
        }
       
        [(UIViewController *)mailDelegate presentViewController:mailViewController animated:YES completion:^{
            
        }];
        
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:"]];
    }
}

+(void)shareSMSWithDelegate:(id<MFMessageComposeViewControllerDelegate>)smsdelegate withMessage:(NSString *) message{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = message;
        controller.recipients = [[NSMutableArray alloc] init];
        controller.messageComposeDelegate = smsdelegate;
        [(UIViewController *)smsdelegate presentViewController:controller animated:YES completion:^{
            
        }];
    }
    else{
        
    }
}


+(void)openCallAppWithNumber:(NSString *)phoneNumber{
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",phoneNumber]]];
    }
}

+(UIViewController *)applicationVisibleViewController{
    return [AppUtilites topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+(UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

+(void)openHereMapsApp{
    NSURL *url = [NSURL URLWithString:@"https://app.adjust.com/atpga4"];
    [[UIApplication sharedApplication] openURL:url];
}
@end
