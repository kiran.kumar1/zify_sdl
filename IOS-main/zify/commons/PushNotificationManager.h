//
//  PushNotificationManager.h
//  zify
//
//  Created by Anurag S Rathor on 23/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PushNotificationManager : NSObject
@property(nonatomic,strong) NSDictionary *notificationData;
+(PushNotificationManager *)sharedInstance;
-(void)registerForNotifcationSettingsWithLaunchOptions:(NSDictionary *)launchOptions;
-(void) handleNotificationData:(NSDictionary *)userInfo;
-(void)showNotificationData;
-(void)handleUserSelectedAction:(NSString *)actionIdentifier andUserInfo:(NSDictionary *)userInfo;
-(void)handlePushDeviceToken:(id)deviceToken;
//-(void)updatePushWithUser;
-(void)handlePushRegisterWithUser;
-(void)handlePushRegisterForChat;
-(BOOL)isPushNotificationEnabledFromDevice;
-(BOOL)isPushNotificationEnabledFromApp;
-(void)changeUserPushSettings:(BOOL)enableSettings andHandler:(void (^)(NSString *,NSString *))handler;
-(void)deregisterDeviceForPushOnLogout;
@end
