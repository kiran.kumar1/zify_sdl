//
//  UIImage+initWithColor.m
//  zify
//
//  Created by Anurag S Rathor on 25/12/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import "UIImage+initWithColor.h"

@implementation UIImage (initWithColor)
+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    // create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end
