//
//  CustomPickerView.m
//  zify
//
//  Created by Anurag S Rathor on 27/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "CustomPickerView.h"

@implementation CustomPickerView{
    NSInteger selectedRow;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_pickervalues count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [_pickervalues objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectedRow = row;
}

- (IBAction)dismiss:(id)sender {
    [self.delegate selectedPickerValue:[_pickervalues objectAtIndex:selectedRow]];
    [self.actionSheet removeFromSuperview];
}

-(void) initialisePicker{
    if(_currentValue){
        for(int index =0;index < _pickervalues.count;index ++){
            NSString *value = [_pickervalues objectAtIndex:index];
            if([value isEqualToString:_currentValue]){
                [self.picker selectRow:index inComponent:0 animated:NO];
                selectedRow = index;
                break;
            }
        }
    } else{
        selectedRow = 0;
        [self.picker selectRow:0 inComponent:0 animated:NO];
    }
    [self.picker reloadAllComponents];
}@end
