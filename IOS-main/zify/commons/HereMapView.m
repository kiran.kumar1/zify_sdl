//
//  HereMapView.m
//  zify
//
//  Created by Anurag S Rathor on 21/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "HereMapView.h"

#import <NMAKit/NMAKit.h>
#import "MapLocationHelper.h"

@import GoogleMaps;

@interface HereMapView ()<NMAMapGestureDelegate,MapLocationHelperDelegate,NMAMapViewDelegate>
@property (strong, nonatomic) NMAMapView *map;
@end

@implementation HereMapView{
    NMAMapMarker *sourceMarker;
    NMAMapMarker *destiationMarker;
    NMAMapMarker *trackingMarker;
    NMAGeocoder *geocoder;
    NMAMapPolyline *prevPathLine;
    NSMutableArray *pathArray;
    MapLocationHelper *mapLocationHelper;
    BOOL isNavigationCompleted;
}

-(void)initialiseMap{
    _map = [[NMAMapView alloc] initWithFrame:self.frame];
    [self addSubview:_map];
    NSDictionary *viewsDictionary = @{@"mapView": _map};
    _map.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[mapView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[mapView]|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:horizontalConsts];
    [self addConstraints:verticalConsts];
    
    _map.mapScheme = NMAMapSchemeNormalDay;
    _map.copyrightLogoPosition = NMALayoutPositionCenterRight;
    _map.gestureDelegate = self;
    _map.delegate = self;
    geocoder = [NMAGeocoder sharedGeocoder];
    pathArray = [[NSMutableArray alloc] init];
    mapLocationHelper = [[MapLocationHelper alloc] init];
    mapLocationHelper.mapLocationDelegate = self;
}

- (void)initialiseMapWithSourceCoordinate{
    [self initialiseMap];
    NMAGeoCoordinates *geoCoordCenter = [[NMAGeoCoordinates alloc] initWithLatitude:self.sourceCoordinate.latitude longitude:self.sourceCoordinate.longitude];
    [_map setGeoCenter:geoCoordCenter withAnimation:NMAMapAnimationNone];
    _map.zoomLevel =  ((NMAMapViewMinimumZoomLevel + NMAMapViewMaximumZoomLevel)/2.0f) + 5.0f;
}
- (void)drawMap{
    [self initialiseMap];
    [self drawMarkers];
    prevPathLine = nil;
    [self drawPathForIndex:0];
}

- (void)drawMarkers{
    if(sourceMarker) { //Kept this check if we want to re-use the same map to draw markers and not to reload map
        [_map removeMapObject:sourceMarker];
        sourceMarker = nil;
    }
    NMAGeoCoordinates *sourceCoordinates = [[NMAGeoCoordinates alloc] initWithLatitude:self.sourceCoordinate.latitude longitude:self.sourceCoordinate.longitude];
    sourceMarker = [[NMAMapMarker alloc] init];
    sourceMarker.coordinates = sourceCoordinates;
    sourceMarker.icon = [UIImage imageNamed:@"icn_source_marker.png"];
    [_map addMapObject:sourceMarker];
    
    
    if(destiationMarker) {
        [_map removeMapObject:destiationMarker];
        destiationMarker = nil;
    }
    NMAGeoCoordinates *destinationCoordinates = [[NMAGeoCoordinates alloc] initWithLatitude:self.destCoordinate.latitude longitude:self.destCoordinate.longitude];
    destiationMarker = [[NMAMapMarker alloc] init];
    destiationMarker.coordinates = destinationCoordinates;
    destiationMarker.icon = [UIImage imageNamed:@"icn_destination_marker.png"];
    [_map addMapObject:destiationMarker];
    
    NMAGeoBoundingBox *boundingBox = [NMAGeoBoundingBox geoBoundingBoxContainingGeoCoordinates:[NSArray arrayWithObjects:sourceCoordinates, destinationCoordinates,nil]];
    boundingBox =  [NMAGeoBoundingBox geoBoundingBoxWithCenter:boundingBox.center width:(boundingBox.width * 1.5) height:(boundingBox.height * 1.5)];
    [_map setBoundingBox:boundingBox insideRect:CGRectMake(self.mapPadding.left + 30, self.mapPadding.top + 30, self.frame.size.width - 60 - self.mapPadding.left-self.mapPadding.right, self.frame.size.height - 60 - self.mapPadding.top - self.mapPadding.bottom) withAnimation:NMAMapAnimationNone];
}

- (void)modifyPathForIndex:(int)index{
    [_map removeMapObject:prevPathLine];
    prevPathLine = nil;
    GMSPath *path = [GMSPath pathFromEncodedPath:self.overviewPolylinePoints];
    NSMutableArray *coordiantesArray = [[NSMutableArray alloc] init];
    for(NSUInteger index = 0;index < path.count;index++) {
        CLLocationCoordinate2D location = [path coordinateAtIndex:index];
        [coordiantesArray addObject:[[NMAGeoCoordinates alloc] initWithLatitude:location.latitude longitude:location.longitude]];
    }
    NMAMapPolyline * newPathLine = [[NMAMapPolyline alloc] initWithVertices:coordiantesArray];
    newPathLine.lineWidth = 3;
    newPathLine.lineColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
    [pathArray setObject:newPathLine atIndexedSubscript:index];
    [_map addMapObject:newPathLine];
    prevPathLine = newPathLine;
}

- (void)drawPathForIndex:(int)index{
    NMAMapPolyline *pathLine = (index <= ((int)pathArray.count - 1)) ? [pathArray objectAtIndex:index] : nil;
    if(prevPathLine) {
        [_map removeMapObject:prevPathLine];
        prevPathLine = nil;
    }
    if(!pathLine){
        GMSPath *path = [GMSPath pathFromEncodedPath:self.overviewPolylinePoints];
        NSMutableArray *coordiantesArray = [[NSMutableArray alloc] init];
        for(NSUInteger index = 0;index < path.count;index++) {
            CLLocationCoordinate2D location = [path coordinateAtIndex:index];
            [coordiantesArray addObject:[[NMAGeoCoordinates alloc] initWithLatitude:location.latitude longitude:location.longitude]];
        }
        pathLine = [[NMAMapPolyline alloc] initWithVertices:coordiantesArray];
        pathLine.lineWidth = 3;
        pathLine.lineColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:58.0/255.0 alpha:1.0];
        [pathArray addObject:pathLine];
    }
    [_map addMapObject:pathLine];
    prevPathLine = pathLine;
}

- (void)updateTrackingMarkerWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
    NMAGeoCoordinates *coordinates = [[NMAGeoCoordinates alloc] initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
    if(!trackingMarker){
        trackingMarker   = [[NMAMapMarker alloc] init];
        trackingMarker.coordinates = coordinates;
        trackingMarker.icon = [UIImage imageNamed:@"tracking_car.png"];
        [_map addMapObject:trackingMarker];
    } else{
        trackingMarker.coordinates = coordinates;
    }
    [_map setGeoCenter:coordinates withAnimation:NMAMapAnimationLinear];
}

- (void)moveToLocation:(CLLocationCoordinate2D)location{
    NMAGeoCoordinates *geoCoordCenter = [[NMAGeoCoordinates alloc] initWithLatitude:location.latitude longitude:location.longitude];
    [_map setGeoCenter:geoCoordCenter withAnimation:NMAMapAnimationLinear];
}

- (void)createMarkerWithhLat:(NSNumber *)latitude andLong:(NSNumber *)longitude andMarkerImage:(UIImage *)markerImage{
    NMAGeoCoordinates *coordinates = [[NMAGeoCoordinates alloc] initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
    NMAMapMarker *marker = [[NMAMapMarker alloc] init];
    marker.coordinates = coordinates;
    marker.icon = markerImage;
    [_map addMapObject:marker];
}

#pragma mark - NMAMapViewDelegate Methods

-(void)mapViewDidEndMovement:(NMAMapView *)mapView{
    if(isNavigationCompleted){
        isNavigationCompleted = false;
        if(self.resolveMapMoveGesture) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [mapLocationHelper getLocalityInfoWithCoordinate:CLLocationCoordinate2DMake(_map.geoCenter.latitude, _map.geoCenter.longitude) andAddress:nil];
            });
        }
    }
}
#pragma mark - NMAMapGestureDelegate Methods

- (void)mapView:(nonnull NMAMapView *)mapView didReceivePan:(CGPoint)translation atLocation:(CGPoint)location{
    isNavigationCompleted = true;
}

- (void)mapViewDidDraw:(NMAMapView *)mapView{
    
}

#pragma mark - MapLocationHelperDelegate methods

-(void) selectedLocationInfo:(LocalityInfo *)locationInfo andError:(NSError *)error{
    if(error != nil){
        
    } else{
        [self.mapViewDelegate changedMapLocation:locationInfo];
    }
    
}
@end
