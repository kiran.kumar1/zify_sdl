//
//  NSDictionary+UrlEncoding.h
///  zify
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface NSDictionary (UrlEncoding)
-(NSString*) urlEncodedString;
@end