//
//  CurrentRideUpdateTracker.h
//  zify
//
//  Created by Anurag S Rathor on 01/10/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CurrentRideUpdateTracker :  NSObject<CLLocationManagerDelegate>
+(CurrentRideUpdateTracker *)sharedInstance;
-(void)updateCurrentRideInfoWithStartTime:(NSDate *)rideStartTime andDuration:(NSNumber *)rideDuration;
@end
