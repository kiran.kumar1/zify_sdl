
//
//  HTTPClient.m
//  zify
//
//  Created by Anurag S Rathor on 04/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "HTTPClient.h"
#import "NSDictionary+UrlEncoding.h"
#import <UIKit/UIKit.h>
#import "LocalisationConstants.h"
#import "AppDelegate.h"

#define GET_TIMEOUT 60
#define POST_TIMEOUT 60


#define NOINTERNET_ERROR_USER_INFO [NSDictionary dictionaryWithObject:NSLocalizedString(CMON_SERVERINTERFACE_NOINTERNETCONNECTION, nil) forKey:@"message"]
#define NOINTERNET_ERROR_CODE 1001
#define NOINTERNET_ERROR_DOMAIN @"ReachabilityError"

#define REQUEST_ERROR_USER_INFO [NSDictionary dictionaryWithObject:NSLocalizedString(CMON_GENERIC_INTERNALERROR, nil) forKey:@"message"]
#define REQUEST_ERROR_CODE 1002
#define REQUEST_ERROR_DOMAIN @"ReachabilityError"

@interface HTTPClient()
@property (nonatomic) NSInteger postTimeOut;
@end
@implementation HTTPClient
@synthesize postTimeOut;

-(id)init {
    if (self = [super init]) {
        postTimeOut=0;
    }
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark-
#pragma mark Handeling Requests
#pragma mark-

#pragma POST Request
-(NSData *)postResponseDataForRequestURL:(NSURL *)url params:(NSDictionary *)params andError:(NSError **)error andIsJSONRequestFormat:(BOOL)isJSONFormat{
//    Reachability* internetReach = [Reachability reachabilityForInternetConnection];
//    [internetReach startNotifier];
//
//    Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
//    [wifiReach startNotifier];
//
//    NetworkStatus internetNetStatus = [internetReach currentReachabilityStatus];
//    NetworkStatus wifiNetStatus = [wifiReach currentReachabilityStatus];
    
    NSData *postBodyData;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.timeoutInterval = postTimeOut>0?postTimeOut:POST_TIMEOUT;
    [request setHTTPMethod: @"POST"];
    if(isJSONFormat){
          [request setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"content-type"];
            NSData *data = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:data
            encoding:NSUTF8StringEncoding];
            postBodyData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
       // NSLog(@"jsonString obj is %@", jsonString);
    } else{
        postBodyData = [params.urlEncodedString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        [request setValue:@"application/x-www-form-urlencoded;charset=utf-8" forHTTPHeaderField:@"content-type"];
    }
    
    if( [url.absoluteString containsString:@"user/geo/saveTravelPreference"]){
        [request setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"content-type"];
    }
    [request setHTTPBody:postBodyData];
    NSLog(@"Request URL --> %@",request);
    NSLog(@"URL encoded params are --> :%@",params.urlEncodedString);
    [Answers logCustomEventWithName:@"Parameters To Call service" customAttributes:params];
    NSHTTPURLResponse *response;
    NSError *responseError;
    if([self isConnectedToInternet])
    {
        NSInteger retryCount = 1;
        while(retryCount <= 3){
            
            if([self isConnectedToInternet]){

            NSData *data;
            data = [[[HTTPSConnectionHandler alloc] init] fetchRequest:request returningResponse:&response error:&responseError];
            if (!responseError) {
                NSInteger statusCode = [response statusCode];
                NSLog(@"status code is %d", statusCode);
                if(statusCode == 200){
                    return data;
                } else if(statusCode == 403 || statusCode == 404){
                    //NSLog(@"message code is %ld", (long)statusCode);
                    retryCount++;
                } else{
                    if(![self isConnectedToInternet]){
                        *error =[NSError errorWithDomain:NOINTERNET_ERROR_DOMAIN code:NOINTERNET_ERROR_CODE userInfo:NOINTERNET_ERROR_USER_INFO];
                    }else{
                    *error =[NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
                    }
                    break;
                }
            } else{
                if(![self isConnectedToInternet]){
                    *error =[NSError errorWithDomain:NOINTERNET_ERROR_DOMAIN code:NOINTERNET_ERROR_CODE userInfo:NOINTERNET_ERROR_USER_INFO];
                }else{
                *error =[NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
                }
                break;
            }
        }else{
            NSLog(@"NO internet connection");
            *error =[NSError errorWithDomain:NOINTERNET_ERROR_DOMAIN code:NOINTERNET_ERROR_CODE userInfo:NOINTERNET_ERROR_USER_INFO];
        }
      }
    }
    else
    {
        NSLog(@"NO internet connection");
        *error =[NSError errorWithDomain:NOINTERNET_ERROR_DOMAIN code:NOINTERNET_ERROR_CODE userInfo:NOINTERNET_ERROR_USER_INFO];
    }
    return nil;
}

-(BOOL)isConnectedToInternet{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    return ([response statusCode] == 200) ? YES : NO;
}


#pragma GET Request
-(NSData *)getResponseDataForRequestURL:(NSURL *)url params:(NSDictionary *)params andError:(NSError **)error{
//
//    Reachability* internetReach = [Reachability reachabilityForInternetConnection];
//    [internetReach startNotifier];
//
//    Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
//    [wifiReach startNotifier];
//
//    NetworkStatus internetNetStatus = [internetReach currentReachabilityStatus];
//    NetworkStatus wifiNetStatus = [wifiReach currentReachabilityStatus];
    NSURL *serveURL=url;
    if (params) {
       serveURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", url, params.urlEncodedString]];
    }
    NSError *responseError;
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:serveURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:GET_TIMEOUT];
    /*if([AppDelegate getAppDelegateInstance].zenparkAuthToken){
      [request addValue:[AppDelegate getAppDelegateInstance].zenparkAuthToken forHTTPHeaderField:@"Zenpark-AuthToken"];
    }*/
    NSLog(@"Request URL FROM GET --> %@",request);
    NSLog(@"URL encoded params are --> :%@",params.urlEncodedString);
    NSHTTPURLResponse *response;
    if([self isConnectedToInternet])
    {
        NSData *data;
        data = [[[HTTPSConnectionHandler alloc] init] fetchRequest:request returningResponse:&response error:&responseError];
        if(!responseError && [response statusCode] == 200){
            return data;
        }else{
           *error =[NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
        }
        return nil;
    }
    else
    {
        *error =[NSError errorWithDomain:NOINTERNET_ERROR_DOMAIN code:NOINTERNET_ERROR_CODE userInfo:NOINTERNET_ERROR_USER_INFO];
    }
    return nil;
}

-(void)setTimeOutForPostRequest:(NSInteger)timeInSeconds{
    postTimeOut=timeInSeconds;
}
@end


@interface HTTPSConnectionHandler()
@property (nonatomic, strong) NSMutableData *mutableData;
@property (nonatomic, strong) NSURLResponse *response;
@property (nonatomic, strong) NSError *error;
@end

@implementation HTTPSConnectionHandler
-(NSData *)fetchRequest:(NSURLRequest *)request returningResponse:(NSURLResponse **)response error:(NSError **)connectionError{
    self.mutableData = [[NSMutableData alloc] init];
    [NSURLConnection connectionWithRequest:request delegate: self];
    NSRunLoop* loop = [NSRunLoop currentRunLoop];
    while (self.finished == NO && [loop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
    *response = self.response;
    if (connectionError != NULL)*connectionError = self.error;
    return self.mutableData;
}

#pragma mark --
#pragma mark Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    self.response =response;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.mutableData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    self.finished = YES;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    self.finished = YES;
    self.error = error;
    NSLog(@"err->%@", error);
}

@end
