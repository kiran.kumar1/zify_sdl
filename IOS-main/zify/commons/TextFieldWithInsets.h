//
//  TextFieldWithInsets.h
//  zify
//
//  Created by Anurag S Rathor on 10/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextFieldWithDone.h"

@interface TextFieldWithInsets : TextFieldWithDone
@property (nonatomic,strong) NSNumber *leftInset;
@property (nonatomic,strong) NSNumber *topInset;
@end
