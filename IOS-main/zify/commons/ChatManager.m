//
//  ChatManager.m
//  zify
//
//  Created by Anurag S Rathor on 04/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "ChatManager.h"
#import "ServerInterface.h"
#import "UserChatRequest.h"
#import "DBUserDataInterface.h"
#import "QBChatUser.h"
#import "QBChatUserMessage.h"
#import "UserChatProfile.h"
#import "UserProfile.h"
#import "ChatNotificationView.h"
#import "GlobalOperationQueue.h"
#import "UserChatDialogRequest.h"
#import "PushNotificationManager.h"
#import "AppDelegate.h"
#import "GuestHomeController.h"
#import "SearchRideViewController.h"
#import "zify-Swift.h"

@implementation ChatManager{
    NSDateFormatter *dateTimeFormatter;
    UserChatProfile *currentUserChatProfile;
    BOOL isChatBeingInitialised,isChatConnected;
    NSDate *notificationShownTime;
    NSTimeInterval notificationInterval;
    NSNumberFormatter *longNumberFormatter;
    GlobalOperationQueue *chatQueue;
    NSOperation *previousChatOpearation;
    BOOL isNewLogin;
    BOOL isChatPushInitialised;
}

/*@synthesize xmppStream;
@synthesize xmppRoster;*/
@synthesize chatMessageDelegate;


+(ChatManager *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[ChatManager alloc]init];
    });
    return instance;
}

-(id)init{
    self = [super init];
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    chatQueue = [GlobalOperationQueue sharedChatInstance];
    notificationShownTime = [NSDate date];
    notificationInterval = 0.0;
    isChatBeingInitialised = false;
    isChatConnected = false;
    isChatPushInitialised = false;
    longNumberFormatter = [[NSNumberFormatter alloc] init];
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    /*[xmppStream removeDelegate:self];
    [xmppRoster removeDelegate:self];*/
}

-(void)registerForChatSettings{
    // Dev chat settings
    /*[QBSettings setApplicationID:49028];
    [QBSettings setAuthKey:@"LgAG25UdFq-HPBR"];
    [QBSettings setAuthSecret:@"zm7kTCJsYNTbfzG"];
    [QBSettings setAccountKey:@"tCxEssTqgqxzzFqJXUHe"];*/
    //Prod chat settings
    [QBSettings setApplicationID:51782];
    [QBSettings setAuthKey:@"dFpACZxEOEP9cLr"];
    [QBSettings setAuthSecret:@"c9MspvYPPL5g3ZD"];
    [QBSettings setAccountKey:@"HsQignB5Ut7S5DShnP1h"];
}

- (BOOL)connect{
    
    
    [[FirebaseChatClass sharedInstance] signInUserEmail:currentUserChatProfile.chatUserEmail password:currentUserChatProfile.chatUserPassword completion:^(FIRAuthDataResult *user) {
        if(user == nil){
            [self signINUserToFirebase:0];
        }
        
    }];

    /*if(isChatConnected || isChatBeingInitialised) return YES;
    isChatBeingInitialised = true;
    NSLog(@"user name is %@ and pwd is %@", currentUserChatProfile.chatUserName,currentUserChatProfile.chatUserPassword);
    [QBRequest logInWithUserLogin:currentUserChatProfile.chatUserName password:currentUserChatProfile.chatUserPassword successBlock:^(QBResponse *response, QBUUser *user){
        user.password = currentUserChatProfile.chatUserPassword;
        [[QBChat instance] connectWithUser:user completion:^(NSError * _Nullable error) {
            __block ChatManager *weakSelf = self;
            if(!isChatPushInitialised){
                isChatPushInitialised = true;
                [[PushNotificationManager sharedInstance] handlePushRegisterForChat];//Moved since push register will be completed first;
            }
            if(isNewLogin){
                NSBlockOperation *fetchDialogOperation = [NSBlockOperation blockOperationWithBlock:^{
                    [weakSelf fetchMessageDialogs];
                }];
                [chatQueue addOpearation:fetchDialogOperation];
                previousChatOpearation = fetchDialogOperation;
            } else{
                isChatConnected = true;
                isChatBeingInitialised = false;
            }
        }];
    } errorBlock:^(QBResponse *response){
        NSLog(@"error is %d ==> %@", response.status, response.error.error.description);
    }];
    [QBSettings setAutoReconnectEnabled:YES];
    [[QBChat instance] addDelegate:self];
    return YES;
     
     */
    return NO;
}

- (void)disconnect {
    isChatBeingInitialised = false;
    isChatConnected = false;
    return;
    if(isChatConnected && [[QBChat instance] isConnected]){
        [[QBChat instance] disconnectWithCompletionBlock:^(NSError * _Nullable error) {
            isChatConnected = false;
        }];
    }
}

-(void)connectUserOnResume{
    [self connect];
}

-(void)disconnectUserOnResume{
    [self disconnect];
}

#pragma mark - QBChatDelegate methods

-(void)displayCustomNotificationBanner:(UserContactDetail *)detail withMessage:(NSString *)receivedMessage{
    if([self isNeedToDisplayNotificationBannerViewFromFirebase:detail]){
        NSTimeInterval interval = [self getNotificationIntervalWithCurrentTime:[NSDate date]];
        if(interval < 10.0 && [self isNeedToDisplayNotificationBannerView]){
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                ChatNotificationView *notification = [ChatNotificationView getChatNotificationViewWithUserName:detail.firstName andMessage:receivedMessage andImage:detail.userImgUrl];
                [self addButtonToChatNotification:notification];
                [notification showWithInterval:interval];
            }];
        }
    }
}

- (void)chatDidReceiveMessage:(QBChatMessage *)message{
    
    NSString *receivedMessage = message.text;
    NSString *fromUser = [NSString stringWithFormat:@"%lu",message.senderID];
    NSDate *receivedTime = message.dateSent;
    BOOL incrementUnreadCount = true;
    BOOL showNotification = false;
    if([[chatMessageDelegate messageReceiverChatIdentifier] isEqualToString:fromUser]){
        incrementUnreadCount = false;
        [chatMessageDelegate newMessageReceived:receivedMessage andDate:receivedTime];
    } else{
        showNotification = true;
    }
    
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    __weak ChatManager *weakSelf = self;
    NSOperation *receiverMessageOperation = [NSBlockOperation blockOperationWithBlock:^{
        QBChatUser *chatUser = [QBChatUser getChatUserWithIdentifier:fromUser inContext:context];
        NSLog(@"chatUser identifier is %@", chatUser.chatIdentifier);
        if(!chatUser){ //Handle the case if the user not exist(User exists in case of chat window opened.
            //Don't insert message since the messages will be loaded on lazy load.
            UserChatRequest *chatRequest = [[UserChatRequest alloc] initWithRequestType:GETCHATUSERDETAIL andChatUserId:fromUser andChatUserName:nil andChatPassword:nil andChatFirstName:nil andChatLastname:nil];
            [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                if(response){
                    NSArray *contactDetailsArray = (NSArray *)response.responseObject;
                    UserContactDetail *detail = [contactDetailsArray objectAtIndex:0];
                    if(showNotification){
                        NSTimeInterval interval = [weakSelf getNotificationIntervalWithCurrentTime:[NSDate date]];
                        if(interval < 10.0 && [self isNeedToDisplayNotificationBannerView]){
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                
                                ChatNotificationView *notification = [ChatNotificationView getChatNotificationViewWithUserName:detail.firstName andMessage:receivedMessage andImage:detail.userImgUrl];
                                [self addButtonToChatNotification:notification];
                                [notification showWithInterval:interval];
                            }];
                        }
                    }
                    [QBChatUser insertChatUserWithIdentifier:fromUser andImageUrl:detail.userImgUrl andLastMessage:receivedMessage andLastMessageTime:receivedTime andFirstName:detail.firstName andLastName:detail.lastName andUnreadCount:[NSNumber numberWithBool:incrementUnreadCount] andDialogId:message.dialogID inContext:context];
                    NSUInteger userCount = [QBChatUser getChatUserCountInContext:context];
                    if(userCount > 20){
                        NSString* receivingChatIdenfier = [QBChatUser deleteOldestChatUserInContext:context];
                        [QBChatUserMessage deletChatUserMessagesWithIdentifier:receivingChatIdenfier inContext:context];
                    }
                    [[DBUserDataInterface sharedInstance] saveSharedContext];
                //   [self checkIfChatForPushExists];
                }
            }];
        } else{//Here you can receive a message for the user who doesn't have any mesage in DB. If the user is there in DB means there is a previous conversation existing in server.
            if(showNotification){
                NSTimeInterval interval = [weakSelf getNotificationIntervalWithCurrentTime:[NSDate date]];
                if(interval < 10.0 && [self isNeedToDisplayNotificationBannerView])
                {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        ChatNotificationView *notification = [ChatNotificationView getChatNotificationViewWithUserName:chatUser.firstName andMessage:receivedMessage andImage:chatUser.imageUrl];
                        [self addButtonToChatNotification:notification];
                        [notification showWithInterval:interval];
                        
                    }];
                }
            }
            NSUInteger messageCount = [QBChatUserMessage getChatUserMessageCountWithIdentifier:fromUser inContext:context];
            chatUser.lastMessage = receivedMessage;
            chatUser.lastMessageTime = receivedTime;
            if(incrementUnreadCount && chatUser.unreadCount.intValue < 20){//Need to handle edge case userlogged in and then he got offine message unreadcount is incrementing twice message.
                chatUser.unreadCount = [NSNumber numberWithInt:chatUser.unreadCount.intValue + 1];
            }
            if(messageCount > 0){
                [QBChatUserMessage insertChatUserMessageWithIdentifier:fromUser andMessage:receivedMessage andMessageTime:receivedTime andIsSelfMessage:[NSNumber numberWithInt:0] inContext:context];
                messageCount++;
                if(messageCount > 50) {
                    [QBChatUserMessage deleteOldestChatUserMessageWithIdentifier:fromUser  inContext:context];
                }
            }
            [[DBUserDataInterface sharedInstance] saveSharedContext];
            UIViewController *vc = [[AppDelegate getAppDelegateInstance] topMostController];
            NSLog(@"last vc is %@", vc);
            if([vc isKindOfClass:[ChatUserListController class]] ){
                ChatUserListController *obj = (ChatUserListController *)vc;
                [obj fetchChatUserList];
            }
            //[self checkIfChatForPushExists];
        }
        
        
        

        
        
        //ccc
    }];
   
    if(previousChatOpearation) [receiverMessageOperation addDependency:previousChatOpearation];
    previousChatOpearation = receiverMessageOperation;
    [chatQueue addOpearation:receiverMessageOperation];
    
}
/*-(void)checkIfChatForPushExists{
    if([AppDelegate getAppDelegateInstance].chatInfoDictFromPush != nil){
        
        UIViewController *visibleController = [AppUtilites applicationVisibleViewController];
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        if( [visibleController isKindOfClass:[ChatUserListController class]]){
            
            ChatUserListController *obj = (ChatUserListController *)visibleController;
            [obj moveToIndividualChatScreenOnPush];
        }else{
            if(rootViewController.presentedViewController != nil){
                [rootViewController dismissViewControllerAnimated:NO completion:nil];
                visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
                [visibleController presentViewController:[ChatUserListController createChatNavController] animated:YES completion:nil];
            }else{
                [visibleController presentViewController:[ChatUserListController createChatNavController] animated:YES completion:nil];
            }
        }
    }
}*/
#pragma mark End QBChatDelegate delegates

-(void)initialiseChat{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    __weak ChatManager *weakSelf = self;
    [chatQueue addOperationBlock:^{
         UserProfile *currentUser= [UserProfile getCurrentUser];
         UserChatProfile *currentChatProfile = [UserChatProfile getCurrentUserChatProfileInContext:context];
        if(currentChatProfile && !currentUser.userChatProfile){
            [UserChatProfile deleteCurrentChatUserProfile];
            currentChatProfile = nil;
        }
        if(!currentChatProfile){
            if(currentUser.userChatProfile){
                currentChatProfile = [[UserChatProfile alloc] initWithChatUserId:currentUser.userChatProfile.chatUserId andUserName:currentUser.userChatProfile.chatUserName andUserEmail:currentUser.emailId andChatPassword:currentUser.userChatProfile.chatUserPassword];
                [UserChatProfile createCurrentChatUserProfile:currentChatProfile];
                [weakSelf completeChatInitialisationWithChatProfile:currentChatProfile];
            } else{
                NSString *qbUserName = [currentUser.emailId stringByReplacingOccurrencesOfString:@"@" withString:@"-"];
                long long timeInMiliSec = (long long)([[NSDate date] timeIntervalSince1970] * 1000);
                NSString *qbUserPassword = [[qbUserName stringByAppendingString:@"_"] stringByAppendingString:[NSNumber numberWithLongLong:timeInMiliSec].stringValue];
                
                __block QBUUser *qbUserProfile = [QBUUser user];
                qbUserProfile.login = qbUserName;
                qbUserProfile.password = qbUserPassword;
                qbUserProfile.email = currentUser.emailId;
                qbUserProfile.fullName = [NSString stringWithFormat:@"%@ %@",currentUser.firstName,currentUser.lastName];
            
                [[FirebaseChatClass sharedInstance] signUpWithUser:qbUserProfile completion:^(FIRAuthDataResult * userObject) {
                    NSLog(@"user is %@", userObject.user.uid);
                    //   }];
             //   [QBRequest signUp:qbUserProfile successBlock:^(QBResponse *response, QBUUser *user) {
                   // qbUserProfile = user;
                    UserChatProfile *newChatProfile = [[UserChatProfile alloc] initWithChatUserId:userObject.user.uid andUserName:qbUserName andUserEmail:currentUser.emailId andChatPassword:qbUserPassword];
                    [UserChatProfile createCurrentChatUserProfile:newChatProfile];
                    [weakSelf completeChatInitialisationWithChatProfile:newChatProfile];
                    UserChatRequest *chatRequest = [[UserChatRequest alloc] initWithRequestType:SAVECHATPROFILEREQUEST andChatUserId:userObject.user.uid andChatUserName:qbUserName andChatPassword:qbUserPassword andChatFirstName:currentUser.firstName andChatLastname:currentUser.lastName];
                    [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                        if(!response){
                            [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                                if(!response){
                                    [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                                    }];
                                }
                            }];
                        }
                    }];
                }];

                
                
              /*  [QBRequest signUp:qbUserProfile successBlock:^(QBResponse *response, QBUUser *user) {
                    qbUserProfile = user;
                 UserChatProfile *newChatProfile = [[UserChatProfile alloc] initWithChatUserId:[NSString stringWithFormat:@"%lu",qbUserProfile.ID] andUserName:qbUserName andUserEmail:currentUser.emailId andChatPassword:qbUserPassword];
                   [UserChatProfile createCurrentChatUserProfile:newChatProfile];
                   [weakSelf completeChatInitialisationWithChatProfile:newChatProfile];
                    UserChatRequest *chatRequest = [[UserChatRequest alloc] initWithRequestType:SAVECHATPROFILEREQUEST andChatUserId:[NSString stringWithFormat:@"%lu",qbUserProfile.ID] andChatUserName:qbUserName andChatPassword:qbUserPassword andChatFirstName:currentUser.firstName andChatLastname:currentUser.lastName];
                    [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                        if(!response){
                            [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                                if(!response){
                                    [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                                     }];
                                }
                            }];
                        }
                     }];
                } errorBlock:^(QBResponse *response) {
                    
                }];*/
            }
        } else{
            [weakSelf completeChatInitialisationWithChatProfile:currentChatProfile];
            if(!currentUser.userChatProfile){
                UserChatRequest *chatRequest = [[UserChatRequest alloc] initWithRequestType:SAVECHATPROFILEREQUEST andChatUserId:currentChatProfile.chatUserId andChatUserName:currentChatProfile.chatUserName andChatPassword:currentChatProfile.chatUserPassword andChatFirstName:currentUser.firstName andChatLastname:currentUser.lastName];
                [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                    
                }];
            }
        }
    }];
}

-(void)signINUserToFirebase:(int)withCount{
    
    if (withCount < 3){
        withCount ++;
        [[FirebaseChatClass sharedInstance] signInUserEmail:currentUserChatProfile.chatUserEmail password:currentUserChatProfile.chatUserPassword completion:^(FIRAuthDataResult *user) {
            if(user == nil){
                [self signINUserToFirebase:withCount];
            }
        }];
    }
}

-(void)completeChatInitialisationWithChatProfile:(UserChatProfile *)chatProfile{
    currentUserChatProfile = chatProfile;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectUserOnResume) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectUserOnResume) name:UIApplicationWillResignActiveNotification object:nil];
        [self connect];
    });
}


-(void)disconnectChatOnLogout{
    [UserChatProfile deleteCurrentChatUserProfile];
    [self disconnect];
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    [context performBlock:^{
        [QBChatUser deleteChatUsersInContext:context];
        [QBChatUserMessage deleteChatUserMessagesInContext:context];
        [[DBUserDataInterface sharedInstance] saveSharedContext];
    }];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    isChatPushInitialised = false; //This need to be done if the user logout an dlogin again push need to be initiliased in the same session.
}

-(void) setIsNewUserLogin:(BOOL)isNewUserLogin{
    isNewLogin = isNewUserLogin;
}

-(void)sendChatMessage:(NSString *)chatMessage toUser:(UserContactDetail *)userDetail andTime:(NSDate *)messageTime andChatDialog:(QBChatDialog *)dialog{
    __weak UserChatProfile *weakCurrentUserChatProfile = currentUserChatProfile;
    QBChatMessage *message = [QBChatMessage message];
    [message setText:chatMessage];
    [message setDateSent:messageTime];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"save_to_history"] = @YES;
    [message setCustomParameters:params];
    [dialog sendMessage:message completionBlock:^(NSError * _Nullable error) {
        
    }];
    
    NSString *dialogId = dialog.ID;
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    [chatQueue addOperationBlock:^{
        QBChatUser *chatUser = [QBChatUser getChatUserWithIdentifier:userDetail.chatIdentifier inContext:context];
        if(!chatUser){
            [QBChatUser insertChatUserWithIdentifier:userDetail.chatIdentifier andImageUrl:userDetail.userImgUrl andLastMessage:chatMessage andLastMessageTime:messageTime andFirstName:userDetail.firstName andLastName:userDetail.lastName andUnreadCount:[NSNumber numberWithInt:0] andDialogId:dialogId inContext:context];
            [QBChatUserMessage insertChatUserMessageWithIdentifier:userDetail.chatIdentifier andMessage:chatMessage andMessageTime:messageTime andIsSelfMessage:[NSNumber numberWithInt:1] inContext:context];
            NSUInteger userCount = [QBChatUser getChatUserCountInContext:context];
            if(userCount > 20){
                NSString* receivingChatIdenfier = [QBChatUser deleteOldestChatUserInContext:context];
                [QBChatUserMessage deletChatUserMessagesWithIdentifier:receivingChatIdenfier inContext:context];
            }
            [[ServerInterface sharedInstance] getResponse:[[UserChatDialogRequest alloc] initWithRequestType:SAVECONVERSATIONINFO andConvUserId1:weakCurrentUserChatProfile.chatUserId andConvUserId2:userDetail.chatIdentifier andConvId:dialogId] withHandler:^(ServerResponse *response, NSError *error){
         
            }]; //Save the conversation id to server for the first message of the user
        } else{
            chatUser.lastMessage = chatMessage;
            chatUser.lastMessageTime = messageTime;
            [QBChatUserMessage insertChatUserMessageWithIdentifier:userDetail.chatIdentifier andMessage:chatMessage andMessageTime:messageTime andIsSelfMessage:[NSNumber numberWithInt:1] inContext:context];
            NSUInteger messageCount = [QBChatUserMessage getChatUserMessageCountWithIdentifier:userDetail.chatIdentifier inContext:context];
            if(messageCount > 50) {
                [QBChatUserMessage deleteOldestChatUserMessageWithIdentifier:userDetail.chatIdentifier inContext:context];
            }
        }
        [[DBUserDataInterface sharedInstance] saveSharedContext];
    }];

}


/*-(NSDate *)getReceivedMessageTime:(NSDate *)timeStamp{
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *receivedDate = [timeStamp dateByAddingTimeInterval:timeZoneSeconds];
    return receivedDate;
}*/

-(NSTimeInterval)getNotificationIntervalWithCurrentTime:(NSDate *)date{
    NSTimeInterval secondsDifference = [date timeIntervalSinceDate:notificationShownTime];
    if(secondsDifference < 1.0){
        notificationInterval ++;
    } else{
        notificationInterval = 0.0;
    }
    notificationShownTime = date;
    return notificationInterval;
}


-(void)getChatConvIdForIdentifier:(NSString *)chatIdentifier withHandler:(void (^)(NSString *))handler{
    [[ServerInterface sharedInstance] getResponse:[[UserChatDialogRequest alloc] initWithRequestType:FETCHCONVERSATIONINFO andConvUserId1:currentUserChatProfile.chatUserId andConvUserId2:chatIdentifier andConvId:nil] withHandler:^(ServerResponse *response, NSError *error){
        NSString *convId;
        if(response && ![response.responseObject isKindOfClass:[NSNull class]]){
            convId = response.responseObject;
        }
        handler(convId);
    }];
}

-(void)createDialogWithConvId:(NSString *)convId andChatIdentifier:(NSString *)chatIdenfitier withHandler:(void (^)(QBChatDialog *))handler{
    QBChatDialog *chatDialog;
    if([convId length] > 0){
        chatDialog = [[QBChatDialog alloc] initWithDialogID:convId type:QBChatDialogTypePrivate];
        [QBRequest updateDialog:chatDialog successBlock:^(QBResponse *responce, QBChatDialog *dialog) {
            handler(dialog);
        } errorBlock:^(QBResponse *response) {
            handler(nil);
        }];
    } else{
        chatDialog = [[QBChatDialog alloc] initWithDialogID:nil type:QBChatDialogTypePrivate];
        chatDialog.occupantIDs = @[@([longNumberFormatter numberFromString:chatIdenfitier].unsignedLongValue)];
        [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
            handler(createdDialog);
        } errorBlock:^(QBResponse *response) {
            handler(nil);
        }];
    }
    
}

-(void)fetchMessageDialogs{
    isNewLogin = NO;
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:20 skip:0];
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    NSMutableDictionary *extendedRequest =[[NSMutableDictionary alloc] init];
    extendedRequest[@"sort_desc"] = @"last_message_date_sent";
    extendedRequest[@"last_message_date_sent[gt]"] = @0;
    [QBRequest dialogsForPage:page extendedRequest:extendedRequest successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        NSBlockOperation *dialogResponseOperation = [NSBlockOperation blockOperationWithBlock:^{
            if(response.status == QBResponseStatusCodeOK){
                NSMutableArray *userIdsArray = [[NSMutableArray alloc] init];
                NSMutableDictionary *chatDialogsDict = [[NSMutableDictionary alloc] init];
                if(dialogObjects.count > 0){
                    for(int index =0;index < dialogObjects.count;index++){
                        QBChatDialog *chatDialog = [dialogObjects objectAtIndex:index];
                        NSString *occupantId1 = [NSString stringWithFormat:@"%lu",[chatDialog.occupantIDs objectAtIndex:0].unsignedLongValue];
                        NSString *occupantId2 = [NSString stringWithFormat:@"%lu",[chatDialog.occupantIDs objectAtIndex:1].unsignedLongValue];
                        NSString *chatIdentifier;
                        if([currentUserChatProfile.chatUserId isEqualToString:occupantId1]){
                            chatIdentifier = occupantId2;
                        } else{
                            chatIdentifier = occupantId1;
                        }
                        [userIdsArray addObject:chatIdentifier];
                        [chatDialogsDict setObject:chatDialog forKey:chatIdentifier];
                    }
                    
                    UserChatRequest *chatRequest = [[UserChatRequest alloc] initWithRequestType:GETCHATUSERDETAIL andChatUserId:[userIdsArray componentsJoinedByString:@","] andChatUserName:nil andChatPassword:nil andChatFirstName:nil andChatLastname:nil];
                    [[ServerInterface sharedInstance] getResponseAsync:chatRequest withHandler:^(ServerResponse *response, NSError *error){
                        if(response){
                            NSArray *contactDetailsArray = (NSArray *)response.responseObject;
                            for(int index =0;index < contactDetailsArray.count;index++){
                                UserContactDetail *contactDetail = [contactDetailsArray objectAtIndex:index];
                                QBChatDialog *chatDialog = [chatDialogsDict objectForKey:contactDetail.chatIdentifier];
                                QBChatUser *existingUser = [QBChatUser getChatUserWithIdentifier:contactDetail.chatIdentifier inContext:context];
                                if(!existingUser){
                                    [QBChatUser insertChatUserWithIdentifier:contactDetail.chatIdentifier andImageUrl:contactDetail.userImgUrl andLastMessage:chatDialog.lastMessageText andLastMessageTime:chatDialog.lastMessageDate andFirstName:contactDetail.firstName andLastName:contactDetail.lastName andUnreadCount:[NSNumber numberWithUnsignedLong: chatDialog.unreadMessagesCount] andDialogId:chatDialog.ID inContext:context];
                                }
                            }
                            [[DBUserDataInterface sharedInstance] saveSharedContext];
                        }
                    }];
                }
            }
        }];
        NSBlockOperation *chatCompletionOperation = [NSBlockOperation blockOperationWithBlock:^{
            isChatConnected = true;
            isChatBeingInitialised = false;
        }];
        if(previousChatOpearation) {
            [dialogResponseOperation addDependency:previousChatOpearation];
        }
        [chatCompletionOperation addDependency:dialogResponseOperation];
        previousChatOpearation = chatCompletionOperation;
        [chatQueue addOpearation:dialogResponseOperation];
        [chatQueue addOpearation:chatCompletionOperation];
    } errorBlock:^(QBResponse *response) {
        
    }];
    NSLog(@"Block completed");
}
-(void)fetchMessagesWithConvId:(NSString *)convId withHandler:(void (^)(NSArray *))handler andReceivingUser:(UserContactDetail *)receivingUser andIsNewUser:(BOOL)isNewUser{
    QBResponsePage *resPage = [QBResponsePage responsePageWithLimit:50 skip:0];
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    NSMutableDictionary *extendedRequest =[[NSMutableDictionary alloc] init];
    extendedRequest[@"sort_desc"] = @"date_sent";
    [QBRequest messagesWithDialogID:convId extendedRequest:extendedRequest forPage:resPage successBlock:^(QBResponse *response, NSArray *messages, QBResponsePage *responcsePage) {
        if(response.status == QBResponseStatusCodeOK){
            NSMutableArray *chatUserMessages = [[NSMutableArray alloc] init];
            if(messages.count > 0){
                for(int index = (int)messages.count - 1;index >= 0;index--){
                    QBChatMessage *message = [messages objectAtIndex:index];
                    NSString *senderId = [NSString stringWithFormat:@"%lu", message.senderID];
                    BOOL isSelfMessage;
                    if([currentUserChatProfile.chatUserId isEqualToString:senderId]){
                        isSelfMessage = YES;
                    } else{
                        isSelfMessage = NO;
                    }
                    QBChatUserMessage *chatUserMessage =  [QBChatUserMessage insertChatUserMessageWithIdentifier:receivingUser.chatIdentifier andMessage:message.text andMessageTime:message.dateSent andIsSelfMessage:[NSNumber numberWithBool:isSelfMessage] inContext:context];
                    [chatUserMessages addObject:chatUserMessage];
                }
                if(isNewUser){
                    NSUInteger userCount = [QBChatUser getChatUserCountInContext:context];
                    if(userCount == 20){
                        NSString* receivingChatIdenfier = [QBChatUser deleteOldestChatUserInContext:context];
                        [QBChatUserMessage deletChatUserMessagesWithIdentifier:receivingChatIdenfier inContext:context];
                    }
                    QBChatUserMessage *lastUserMessage = [chatUserMessages lastObject];
                    [QBChatUser insertChatUserWithIdentifier:lastUserMessage.chatIdentifier andImageUrl:receivingUser.userImgUrl andLastMessage:lastUserMessage.message andLastMessageTime:lastUserMessage.messageTime andFirstName:receivingUser.firstName andLastName:receivingUser.lastName andUnreadCount:[NSNumber numberWithBool:0] andDialogId:convId inContext:context];
                }
                [[DBUserDataInterface sharedInstance] saveSharedContext];
                handler(chatUserMessages);
            }
        }
    } errorBlock:^(QBResponse *response) {
        NSLog(@"error: %@", response.error);
    }];
}

-(QBChatUser *)getChatUserForIdentifier:(NSString *)chatIdentifier{
    return [QBChatUser getChatUserWithIdentifier:chatIdentifier inContext:[[DBUserDataInterface sharedInstance] sharedContext]];
}

-(NSArray *)getChatUserList{
    return [QBChatUser getChatUsersInContext:[[DBUserDataInterface sharedInstance] sharedContext]];
}

-(NSArray *)getChatUserMessagesForChatIdentifier:(NSString *)chatIdentifier{
    return [QBChatUserMessage getChatUserMessagesWithIdentifier:chatIdentifier inContext:[[DBUserDataInterface sharedInstance] sharedContext]];
}

-(void)resetMessageUnreadCountForChatUser:(QBChatUser *)chatUser{
    chatUser.unreadCount = [NSNumber numberWithInt:0];
    [[DBUserDataInterface sharedInstance] saveSharedContext];
}

-(NSString *)currentUserChatIdentifier{
    return currentUserChatProfile.chatUserId;
}

-(BOOL)isUserChatConnected{
    return isChatConnected && [[QBChat instance] isConnected];
}

-(void)deleteCurrentUserChatProfile{
     [UserChatProfile deleteCurrentChatUserProfile];
}

-(void)moveToChatScreen:(id)sender{
    NSLog(@"touch is happened");
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    for(UIView *view in appdelegate.window.subviews){
        if([view isKindOfClass:[ChatNotificationView class]]){
            [view removeFromSuperview];
        }
    }
    UIViewController *vc = [appdelegate topMostController];
    NSLog(@"last vc is %@", vc);
    if([vc isKindOfClass:[GuestHomeController class]] || [vc isKindOfClass:[SearchRideViewController class]]){
        [appdelegate.window.rootViewController presentViewController:[ChatUserListController createChatNavController] animated:YES completion:nil];
    }else{
        [vc dismissViewControllerAnimated:NO completion:nil];
        [appdelegate.window.rootViewController presentViewController:[ChatUserListController createChatNavController] animated:YES completion:nil];

    }
}

-(void)btnCancelTapped:(id)sende{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    for(UIView *view in window.subviews){
        if([view isKindOfClass:[ChatNotificationView class]]){
           /* [UIView animateWithDuration:0.3 delay:0.25
                                options: UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGFloat y = -view.frame.size.height/2;
                                 view.frame = CGRectMake(view.frame.origin.x, y, view.frame.size.width,view.frame.size.height);
                             }completion:^(BOOL finished){
                                 [view removeFromSuperview];
                             }];*/
            
            __weak UIWindow *weakWindow = window;
            
             // [weakWindow removeConstraint:weakAfterVericalConstant];
           //  [weakWindow addConstraint:weakBeforeVerticalConstraint];
             [UIView animateWithDuration:0.3f delay:2.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
             [weakWindow layoutIfNeeded];
             } completion:^(BOOL finished) {
             [view removeFromSuperview];
             }];
        }
    }
}
-(void)addButtonToChatNotification:(ChatNotificationView *)view{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    UIButton *btnView = [UIButton buttonWithType:UIButtonTypeCustom];
    btnView.frame = CGRectMake(0, 0, screenSize.width - 40, view.frame.size.height);
    btnView.backgroundColor = [UIColor clearColor];
    btnView.alpha = 0.3;
    [view addSubview:btnView];
    [btnView addTarget:self action:@selector(moveToChatScreen:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame = CGRectMake(screenSize.width - 40, 0, 40, view.frame.size.height);
    btnCancel.backgroundColor = [UIColor clearColor];
    btnCancel.alpha = 0.3;
    [view addSubview:btnCancel];
    [btnCancel addTarget:self action:@selector(btnCancelTapped:) forControlEvents:UIControlEventTouchUpInside];
}
-(BOOL)isNeedToDisplayNotificationBannerView{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIViewController *vc = [appDelegate topMostController];
    NSLog(@"top vc is %@", vc);
    if([vc isKindOfClass:[ChatUserListController class]]){
       /* if([vc isKindOfClass:[ChatUserListController class]]){
            ChatUserListController *vcObj = (ChatUserListController *)vc;
            [vcObj fetchChatUserList];
        }*/
        return NO;
        
    }
    else return YES;
}

-(BOOL)isNeedToDisplayNotificationBannerViewFromFirebase:(UserContactDetail *)detail{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIViewController *vc = [appDelegate topMostController];
    NSLog(@"top vc is %@", vc);
    if([vc isKindOfClass:[ChatUserListController class]]){
        /* if([vc isKindOfClass:[ChatUserListController class]]){
         ChatUserListController *vcObj = (ChatUserListController *)vc;
         [vcObj fetchChatUserList];
         }*/
        return NO;
        
    }else if([vc isKindOfClass:[ChatMessagesController class]]){
        ChatMessagesController *vcObj = (ChatMessagesController *)vc;
        if(  [vcObj.receivingUser.chatIdentifier isEqualToString:detail.chatIdentifier]){
            return NO;
        }
        return YES;
    }
    else return YES;
}
@end
