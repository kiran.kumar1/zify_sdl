//
//  MapLocationHelper.m
//  zify
//
//  Created by Anurag S Rathor on 23/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "MapLocationHelper.h"
#import "CurrentLocationTracker.h"
#import "AppDelegate.h"
//#import "CurrentLocale.h"
//#import <NMAKit/NMAKit.h>


@import GooglePlaces;
@import GoogleMaps;
@import GooglePlacePicker;

@interface MapLocationHelper()<GMSPlacePickerViewControllerDelegate,GMSAutocompleteViewControllerDelegate>

@end



@implementation MapLocationHelper{
    LocalityInfo *currentLocationInfo;
    //CurrentLocale *currentLocale;
    GMSPlacesClient *placesClient;
    GMSGeocoder *geocoder;
    GMSCoordinateBounds *currentGMSBounds;
    //GMSPlacePicker *gmsPlacePicker; after 3.1.0
    GMSPlacePickerViewController *gmsPlacePicker;
    GMSAutocompleteFilter *_filter;

    // NMAGeocoder *hereGeoCoder;
    //NMAPlaces *herePlacesClient;
}

-(id)init{
    self = [super init];
    // currentLocale = [CurrentLocale sharedInstance];
    placesClient = [[GMSPlacesClient alloc] init];
    geocoder = [GMSGeocoder geocoder];
    //hereGeoCoder = [NMAGeocoder sharedGeocoder];
    //herePlacesClient = [NMAPlaces sharedPlaces];
    return self;
}
-(id)initWithDelegae:(id)delegate{
    self = [super init];
    // currentLocale = [CurrentLocale sharedInstance];
    placesClient = [[GMSPlacesClient alloc] init];
    geocoder = [GMSGeocoder geocoder];
    _mapLocationDelegate = delegate;
    //hereGeoCoder = [NMAGeocoder sharedGeocoder];
    //herePlacesClient = [NMAPlaces sharedPlaces];
    return self;
}

-(void)showPickOnMap:(UINavigationController *)navCtrl {
    NSLog(@"delegate is %@", _mapLocationDelegate);
    [self intialiseCurrentLocationInfoandBounds];
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    GMSPlaceField fields = (GMSPlaceFieldName | GMSPlaceFieldPlaceID | GMSPlaceFieldAll |GMSPlaceFieldCoordinate | GMSPlaceFieldAddressComponents | GMSPlaceFieldFormattedAddress );
    acController.placeFields = fields;
    _filter = [[GMSAutocompleteFilter alloc] init];
    _filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
    acController.autocompleteFilter = _filter;
    [navCtrl presentViewController:acController animated:YES completion:nil];
}

-(void)getAutoSuggestResultsForQuery:(NSString *)query {
    [self intialiseCurrentLocationInfoandBounds];
    /*if([currentLocale isGlobalLocale]){
     if(query.length > 2){
     NMAGeoCoordinates* geoCoodrinates = [[NMAGeoCoordinates alloc] initWithLatitude:currentLocationInfo.latLng.latitude longitude:currentLocationInfo.latLng.latitude];
     NMAAutoSuggestionRequest* request =
     [herePlacesClient createAutoSuggestionRequestWithLocation:geoCoodrinates partialTerm:query];
     request.collectionSize = 5;
     NSError* error = [request startWithListener:self];
     if (error.code != NMARequestErrorNone) {
     [_mapLocationDelegate queryResultsForAutosuggest:nil];
     }
     } else{
     [_mapLocationDelegate queryResultsForAutosuggest:nil];
     }
     } else{*/
    if(query.length > 2){
        GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
        // filter.accessibilityLanguage = @"fr";
        filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
        [placesClient autocompleteQuery:query
                                 bounds:currentGMSBounds
                                 filter:filter
                               callback:^(NSArray *results, NSError *error) {
                                   if (error != nil) {
                                       // NSLog(@"Autocomplete error %@", [error localizedDescription]);
                                       [_mapLocationDelegate queryResultsForAutosuggest:nil];
                                   } else{
                                       [_mapLocationDelegate queryResultsForAutosuggest:results];
                                   }
                               }];
    } else{
        [_mapLocationDelegate queryResultsForAutosuggest:nil];
    }
    //}
}

-(void)getLocationInfoFromAutosuggest:(id)autoSuggestResult{
    //if([autoSuggestResult isKindOfClass:[GMSAutocompletePrediction class]]){
    GMSAutocompletePrediction *result = (GMSAutocompletePrediction *)autoSuggestResult;
    [placesClient lookUpPlaceID:result.placeID callback:^(GMSPlace *place, NSError *error) {
        [self completeGMSPlaceHandling:place andError:error andAutocompleteResult:result];
    }];
    /* } else if([autoSuggestResult isKindOfClass:[NMAAutoSuggest class]]){
     NMAAutoSuggestType type = ((NMAAutoSuggest*)autoSuggestResult).type;
     if(type == NMAAutoSuggestTypePlace){
     NMAAutoSuggestPlace *placeResult = (NMAAutoSuggestPlace *)autoSuggestResult;
     [self getLocalityInfoWithCoordinate:CLLocationCoordinate2DMake(placeResult.position.latitude, placeResult.position.longitude) andAddress:nil];
     } else if(type == NMAAutoSuggestTypeSearch){
     NMAAutoSuggestSearch *searchResult = (NMAAutoSuggestSearch *)autoSuggestResult;
     [self getLocalityInfoWithCoordinate:CLLocationCoordinate2DMake(searchResult.position.latitude, searchResult.position.longitude) andAddress:nil];
     }
     
     }*/
}

-(void)getLocalityInfoWithCoordinate:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)currentAddress{
    /*if([currentLocale isGlobalLocale]){
     NMAGeoCoordinates* geoCoordinates = [[NMAGeoCoordinates alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
     NMAReverseGeocodeRequest* request = [hereGeoCoder createReverseGeocodeRequestWithGeoCoordinates:geoCoordinates];
     NSError* error = [request startWithListener:self];
     if (error.code != NMARequestErrorNone) {
     [self.mapLocationDelegate selectedLocationInfo:nil andError:error];
     }
     } else{*/
    [geocoder reverseGeocodeCoordinate:coordinate completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error) {
        if(error != nil){
            [self.mapLocationDelegate selectedLocationInfo:nil andError:error];
        } else{
            NSArray *array = [response results];
            GMSAddress *address;
            for(int index = 0;index< array.count;index++) {
                address = [array objectAtIndex:index];
                if(address.locality != nil) break;
            }
            NSString *completeAddress = currentAddress;
            if(!completeAddress){
                if(address.lines){
                    completeAddress = [address.lines componentsJoinedByString:@", "];
                } else {
                    completeAddress = address.subLocality;
                }
            }
            NSString *locationName = address.thoroughfare;
            if(!locationName) locationName = address.subLocality;
            LocalityInfo *localityInfo = [[LocalityInfo alloc] initWithName:locationName andAddress:completeAddress andCity:address.locality andState:address.administrativeArea andCountry:address.country andLatLng:coordinate];
            [self.mapLocationDelegate selectedLocationInfo:localityInfo andError:nil];
        }
    }];
    //}
}

#pragma mark - NMAResultListener methods
/*- (void)request:(NMARequest*)request didCompleteWithData:(id)data error:(NSError*)error {
 if ([request isKindOfClass:[NMAReverseGeocodeRequest class]]){
 if (error.code == NMARequestErrorNone ) {
 NMAReverseGeocodeResult *geocodeResult = (NMAReverseGeocodeResult *)[(NSArray*) data objectAtIndex:0];
 NMAPlaceLocation *location = geocodeResult.location;
 NMAAddress *address = location.address;
 LocalityInfo *localityInfo = [[LocalityInfo alloc] initWithName:location.label andAddress:address.formattedAddress andCity:address.city andState:address.state andCountry:address.countryName andLatLng:CLLocationCoordinate2DMake(location.position.latitude, location.position.longitude)];
 [self.mapLocationDelegate selectedLocationInfo:localityInfo andError:nil];
 }
 else {
 [self.mapLocationDelegate selectedLocationInfo:nil andError:error];
 }
 } if ([request isKindOfClass:[NMAAutoSuggestionRequest class]]){
 if (error.code == NMARequestErrorNone) {
 NSArray *autoSuggestList = (NSArray*) data;
 NSMutableArray *filteredAutoSuggestList = [[NSMutableArray alloc] init];
 for(int index=0;index < autoSuggestList.count;index++){
 NMAAutoSuggest *autoSuggest = (NMAAutoSuggest*)[autoSuggestList objectAtIndex:index];
 if(autoSuggest.type != NMAAutoSuggestTypeQuery){
 [filteredAutoSuggestList addObject:autoSuggest];
 }
 }
 [_mapLocationDelegate queryResultsForAutosuggest:filteredAutoSuggestList];
 }
 else {
 [_mapLocationDelegate queryResultsForAutosuggest:nil];
 }
 }
 }*/


-(void)completeGMSPlaceHandling:(GMSPlace *)place andError:(NSError *)error andAutocompleteResult:(GMSAutocompletePrediction *)autocompleteResult{
    if (error != nil) {
        [self.mapLocationDelegate selectedLocationInfo:nil andError:error];
    }
    if (place != nil) {
        [geocoder reverseGeocodeCoordinate:place.coordinate
                         completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error) {
                             if(error != nil){
                                 [self.mapLocationDelegate selectedLocationInfo:nil andError:error];
                             } else{
                                 NSArray *array = [response results];
                                 GMSAddress *address;
                                 for(int index = 0;index< array.count;index++) {
                                     address = [array objectAtIndex:index];
                                     if(address.locality != nil) break;
                                 }
                                 NSString *placeName, *completeAddress;
                                 if(autocompleteResult){
                                     placeName = autocompleteResult.attributedPrimaryText.string;
                                     completeAddress = autocompleteResult.attributedFullText.string;
                                 } else{
                                     placeName = place.name;
                                     completeAddress = place.formattedAddress;
                                     if(placeName && completeAddress){
                                         if(![completeAddress hasPrefix:placeName]){
                                             completeAddress = [[placeName stringByAppendingString:@", "] stringByAppendingString:completeAddress];
                                         }
                                     } else{
                                         completeAddress = [address.lines componentsJoinedByString:@", "];
                                     }
                                     if(!placeName){
                                         placeName= address.thoroughfare;
                                         if(!placeName) placeName = address.subLocality;
                                     }
                                 }
                                 LocalityInfo *localityInfo = [[LocalityInfo alloc] initWithName:placeName andAddress:completeAddress andCity:address.locality andState:address.administrativeArea andCountry:address.country andLatLng:place.coordinate];
                                 NSLog(@"lat laong is %f, %f", localityInfo.latLng.latitude, localityInfo.latLng.longitude);
                                 NSLog(@"delegate is %@", self.mapLocationDelegate);
                                 [self.mapLocationDelegate selectedLocationInfo:localityInfo andError:nil];
                             }
                         }];
    } else {
        [self.mapLocationDelegate selectedLocationInfo:nil andError:nil];
    }
}

-(void)intialiseCurrentLocationInfoandBounds{
    if(!currentLocationInfo){
        currentLocationInfo = [[CurrentLocationTracker sharedInstance] currentLocalityInfo];
        currentGMSBounds = [[GMSCoordinateBounds alloc] init];
        currentGMSBounds = [currentGMSBounds includingCoordinate:currentLocationInfo.latLng];
    }
}
#pragma mark - GMSPlace Delegate -- New Version 
- (void)placePicker:(nonnull GMSPlacePickerViewController *)viewController didPickPlace:(nonnull GMSPlace *)place {
    
    //NSLog(@"error occured is is %@", [error description]);
    //[self completeGMSPlaceHandling:place andError:error andAutocompleteResult:nil];
    
    [self completeGMSPlaceHandling:place andError:nil andAutocompleteResult:nil];
    [viewController dismissViewControllerAnimated:true completion:nil];
}

-(void)placePicker:(GMSPlacePickerViewController *)viewController didFailWithError:(NSError *)error {
    
    [self.mapLocationDelegate selectedLocationInfo:nil andError:nil];
    [viewController dismissViewControllerAnimated:true completion:nil];
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    // Dismiss the place picker, as it cannot dismiss itself.
    [self.mapLocationDelegate selectedLocationInfo:nil andError:nil];
    [viewController dismissViewControllerAnimated:true completion:nil];
    NSLog(@"No place selected");
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    // [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    // NSLog(@"Place name %@", place.name);
    //  NSLog(@"Place ID %@", place.placeID);
    // NSLog(@"Place attributions %@", place.attributions.string);
    
    [self completeGMSPlaceHandling:place andError:nil andAutocompleteResult:nil];
    [viewController dismissViewControllerAnimated:true completion:nil];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    // TODO: handle the error.
    // NSLog(@"Error: %@", [error description]);
    
    [self.mapLocationDelegate selectedLocationInfo:nil andError:nil];
    [viewController dismissViewControllerAnimated:true completion:nil];
    
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    ///[self dismissViewControllerAnimated:YES completion:nil];
    [self.mapLocationDelegate selectedLocationInfo:nil andError:nil];
    [viewController dismissViewControllerAnimated:true completion:nil];
    
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


@end

