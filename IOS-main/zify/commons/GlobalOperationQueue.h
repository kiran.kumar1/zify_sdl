//
//  GlobalOperationQueue.h
//  zify
//
//  Created by Anurag S Rathor on 28/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalOperationQueue : NSObject

+(GlobalOperationQueue *)sharedInstance;
+(GlobalOperationQueue *)sharedChatInstance;
-(void)addOperationBlock:(void(^)())block;
-(void)addOpearation:(NSOperation *)opearation;
@end
