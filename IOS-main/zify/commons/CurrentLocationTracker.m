//
//  CurrentLocationTracker.m
//  zify
//
//  Created by Anurag S Rathor on 23/09/15.
//  Copyright (c) 2015 zify. All rights reserved.
//


#import "CurrentLocationTracker.h"
#import "MapLocationHelper.h"
#import "LocalisationConstants.h"

#define ONE_MINUTE 60000

#define REQUEST_ERROR_USER_INFO [NSDictionary dictionaryWithObject:NSLocalizedString(CMON_GENERIC_INTERNALERROR, nil) forKey:@"message"]
#define REQUEST_ERROR_CODE 1002
#define REQUEST_ERROR_DOMAIN @"ReachabilityError"

#define LOCATION_ERROR_USER_INFO [NSDictionary dictionaryWithObject:NSLocalizedString(CMON_LOCATION_TURNONLOCATION,nil) forKey:@"message"]
#define LOCATION_ERROR_CODE 1003
#define LOCATION_ERROR_DOMAIN @"LocationServiceError"

@implementation CurrentLocationTracker{
    CLLocationManager *locationManager;
    NSDate *timeSinceFirstLocationReceived;
    CLLocation *currentLocation;
    MapLocationHelper *mapLocationHelper;;
}

+(CurrentLocationTracker *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[CurrentLocationTracker alloc]init];
    });
    return instance;
}

-(id) init{
    self = [super init];
    locationManager = [[CLLocationManager alloc] init];
    mapLocationHelper = [[MapLocationHelper alloc] init];
    mapLocationHelper.mapLocationDelegate = self;
    return self;
}

- (void)updateCurrentLocation{
    currentLocation = nil;
    if([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus]!=kCLAuthorizationStatusDenied){
        if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
            [locationManager requestWhenInUseAuthorization];
        }
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
    } else{
        if(_currentLocationTrackerDelegate){
            NSError *error = [NSError errorWithDomain:LOCATION_ERROR_DOMAIN code:LOCATION_ERROR_CODE userInfo:LOCATION_ERROR_USER_INFO];
            [_currentLocationTrackerDelegate unableToFetchCurrentLocation:error];
            _currentLocationTrackerDelegate = nil;
        }
    }
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if(status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse){
        [manager startUpdatingLocation];
    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    // stop updating
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    currentLocation = nil;
    if(_currentLocationTrackerDelegate){
        NSError *userError;
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            userError = [NSError errorWithDomain:LOCATION_ERROR_DOMAIN code:LOCATION_ERROR_CODE userInfo:LOCATION_ERROR_USER_INFO];
            if([_currentLocationTrackerDelegate respondsToSelector:@selector(methodForLocationServicesAccessDenied)]){
            [_currentLocationTrackerDelegate methodForLocationServicesAccessDenied];
            }
        } else{
            userError = [NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
            [_currentLocationTrackerDelegate unableToFetchCurrentLocation:userError];
        }
        _currentLocationTrackerDelegate = nil;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    if(currentLocation!=nil){
        if([currentLocation.timestamp timeIntervalSinceDate:timeSinceFirstLocationReceived]<ONE_MINUTE){
            return;
        }
    }
    if(currentLocation==nil){
        currentLocation=newLocation;
        timeSinceFirstLocationReceived=[NSDate date];
        [locationManager stopUpdatingLocation];
        locationManager.delegate = nil;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [mapLocationHelper getLocalityInfoWithCoordinate:currentLocation.coordinate andAddress:nil];
    });
}

-(BOOL) isLocationTrackingEnabled{
    return [CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] !=kCLAuthorizationStatusDenied;
}

#pragma mark - MapLocationHelperDelegate methods

-(void) selectedLocationInfo:(LocalityInfo *)locationInfo andError:(NSError *)error{
    if(error != nil){
        
    } else{
        _currentLocalityInfo = locationInfo;
        if(_currentLocationTrackerDelegate){
            [_currentLocationTrackerDelegate setCurrentLocation:_currentLocalityInfo];
            _currentLocationTrackerDelegate = nil;
        }
    }
    
}

@end
