//
//  UserDataManager.h
//  zify
//
//  Created by Anurag S Rathor on 20/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserSearchData.h"

@interface UserDataManager : NSObject
+(UserDataManager *)sharedInstance;
-(void)setUserSearchData:(UserSearchData *)searchData;
-(void)resetUserSearchData;
-(UserSearchData *)getUserSearchData;
@property(nonatomic,strong)NSDictionary *deepLinkParams;
-(void)handleBranchDeepLinkParams:(NSDictionary *)params;
-(void)handleDeepLinkParams;
-(BOOL)isDeepLinkDataPresent;
-(void)setDeepLinkFlagsOnLogin;
-(void)setDeepLinkFlagsOnLogout;
-(void)setShowDeepLinkController:(BOOL)flag;
@end
