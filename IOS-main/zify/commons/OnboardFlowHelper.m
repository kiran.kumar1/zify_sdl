//
//  OnboardFlowHelper.m
//  zify
//
//  Created by Anurag S Rathor on 15/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "OnboardFlowHelper.h"
#import "UserProfile.h"
#import "VerifyMobileController.h"
#import "UpgradeProfileTutController.h"
#import "UserAddressPreferencesController.h"
#import "MainContentController.h"
#import "CurrentLocale.h"
#import "ChatManager.h"
#import "AppDelegate.h"
#import "zify-Swift.h"
#import "NSString+Random.h"

@implementation OnboardFlowHelper

+(OnboardFlowHelper *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[OnboardFlowHelper alloc] init];
    });
    return instance;
}

-(void)handleLoginFlow:(UIViewController *)controller andIsNewLogin:(BOOL)isNewLogin{
    UserProfile *currentUser = [UserProfile getCurrentUser];
    NSLog(@"user is gobal %@", currentUser.mobileVerified);
    [[CurrentLocale sharedInstance] setLocaleValuesFromUser:currentUser];
    [[ChatManager sharedInstance] setIsNewUserLogin:isNewLogin];
    
    NSString *sessionId = [[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    if(sessionId == nil){
        sessionId = [NSString randomAlphanumericStringWithLength:10];
        [[NSUserDefaults standardUserDefaults] setObject:sessionId forKey:@"sessionId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if(rootViewController.presentedViewController != nil){
        [rootViewController dismissViewControllerAnimated:NO completion:nil];
        controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    }
     NSLog(@"need to change the condition");
    if(![currentUser.mobileVerified intValue]){
        [controller presentViewController:[VerificationCodeController createVerificationCodeNavController] animated:NO completion:nil];
    } else if (currentUser.profileImgUrl.length == 0 || [currentUser.profileImgUrl isEqualToString:@""] || currentUser.profileImgUrl == nil ){
        [controller presentViewController:[SetProfilePictureController createSetProfilePicNavController] animated:NO completion:nil];
    }else if(currentUser.userPreferences == nil){
        UINavigationController *navController = [NewTPScreenViewController showNewTPNavController];
        [controller presentViewController:navController animated:NO completion:nil];
    }else {
        [self showUserHomeScreen:controller];
        return;
        NSDate *remiderDate =  [[NSUserDefaults standardUserDefaults] objectForKey:@"profileUpgradeReminder"];
        BOOL isIDCardUploaded = [currentUser.userDocuments.isIdCardDocUploaded intValue] ? YES : NO;
        BOOL isPreferencesExist = currentUser.userPreferences ? YES : NO;
        BOOL isFromSignUp = [[NSUserDefaults standardUserDefaults] boolForKey:@"isFromSignUp"];
        if((isIDCardUploaded && isPreferencesExist) || isFromSignUp){
            [self setBoolForAKeyForSignUpFlow];
            [self showUserHomeScreen:controller];
        } else{
           /* if(!remiderDate || [self isDateAlreadyCompleted:remiderDate]){
                if(!isIDCardUploaded && !isPreferencesExist){
                    [controller presentViewController:[UpgradeProfileTutController createUpgradeProfileTutNavController] animated:NO completion:nil];
                } else if(!isPreferencesExist){
                    UserAddressPreferencesController *addressPreferencesController = [UserAddressPreferencesController createUserAddressPreferencesController];
                    addressPreferencesController.showPageIndicator = NO;
                    addressPreferencesController.isUpgradeFlow = YES;
                    [controller presentViewController:addressPreferencesController animated:NO completion:nil];
                } else{
                    [self showUserHomeScreen:controller];
                }
            } else{*/
                [self showUserHomeScreen:controller];
           // }
        }
        
    }
}
-(void)setBoolForAKeyForSignUpFlow{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:NO forKey:@"isFromSignUp"];
    [prefs synchronize];
}
-(void)handleVerifyMobileFlow:(UIViewController *)controller{
    /*UserProfile *currentUser = [UserProfile getCurrentUser];
     if(![currentUser.userDocuments.isIdCardDocUploaded intValue]){
     [controller presentViewController:[UpgradeProfileTutController createUpgradeProfileTutNavController] animated:NO completion:nil];
     } else {
     UserAddressPreferencesController *addressPreferencesController = [UserAddressPreferencesController createUserAddressPreferencesController];
     addressPreferencesController.showPageIndicator = NO;
     addressPreferencesController.isUpgradeFlow = YES;
     [controller presentViewController:addressPreferencesController animated:NO completion:nil];
     }*/
    [self setReminder:controller];
}

-(void)handeUpgradeProfileTutFlow:(UIViewController *)controller{
    UserIDCardDetailsController *IDCardDetailsController = [UserIDCardDetailsController createUserIDCardDetailsController] ;
    IDCardDetailsController.isUpgradeFlow = YES;
    [controller presentViewController:IDCardDetailsController animated:NO completion:nil];
}

-(void)handleIDCardDetailsFlow:(UIViewController *)controller{
    UserAddressPreferencesController *addressPreferencesController = [UserAddressPreferencesController createUserAddressPreferencesController];
    addressPreferencesController.showPageIndicator = YES;
    addressPreferencesController.isUpgradeFlow = YES;
    [controller presentViewController:addressPreferencesController animated:NO completion:nil];
}


-(void)setReminder:(UIViewController *)controller{
    [[NSUserDefaults standardUserDefaults] setObject:[[NSDate date] dateByAddingTimeInterval:(2 * 86400)] forKey:@"profileUpgradeReminder"];
    [self showUserHomeScreen:controller];
}

-(void)showUserHomeScreen:(UIViewController *)controller{
    [controller dismissViewControllerAnimated:NO completion:nil];
    [(AppDelegate *)[UIApplication sharedApplication].delegate showUserHomeController];
}

-(void)clearReminder{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profileUpgradeReminder"];
}

-(BOOL)isDateAlreadyCompleted:(NSDate *)date{
    NSInteger seconds;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components: NSCalendarUnitSecond fromDate:[NSDate date] toDate:date options:0];
    seconds = [components second];
    if(seconds < 0) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profileUpgradeReminder"];
        return true;
    }
    return false;
}
@end

