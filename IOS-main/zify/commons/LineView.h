//
//  LineView.h
//  zify
//
//  Created by Anurag S Rathor on 10/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineView : UIView
@property (nonatomic) Boolean horizontalLine;
@property (nonatomic) Boolean verticalLine;
@property (nonatomic) UIColor *lineColor;
@property (nonatomic) NSNumber *lineWidth;
@end
