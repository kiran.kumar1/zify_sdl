//
//  TextFieldWithInsets.m
//  zify
//
//  Created by Anurag S Rathor on 10/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TextFieldWithInsets.h"

@implementation TextFieldWithInsets
-(CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds,self.leftInset.intValue,self.topInset.intValue);
}
- (CGRect)editingRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds,self.leftInset.intValue,self.topInset.intValue);
}
@end
