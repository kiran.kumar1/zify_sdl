//
//  TransparentView.m
//  zify
//
//  Created by Anurag S Rathor on 26/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TransparentView.h"

@interface TransparentView()
@property (nonatomic,strong) UIView *transparentLayer;
@end

@implementation TransparentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.transparentLayer = [[UIView alloc] initWithFrame:frame];
        self.transparentLayer.backgroundColor = [UIColor blackColor];
        self.transparentLayer.alpha = 0.4;
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.transparentLayer];
    }
    return self;
}

@end
