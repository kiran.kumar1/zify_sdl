//
//  MapView.m
//  zify
//
//  Created by Anurag S Rathor on 20/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "MapView.h"

@implementation MapView

- (void)initialiseMapWithSourceCoordinate{
    
}

- (void)drawMap{
    
}

- (void)drawMarkers{
    
}

-(void)drawPathForIndex:(int)index{
    
}

-(void)modifyPathForIndex:(int)index{
    
}

- (void)updateTrackingMarkerWithLat:(NSNumber *)latitude andLong:(NSNumber *)longitude{
    
}

- (void)moveToLocation:(CLLocationCoordinate2D)location{
    
}

- (void)createMarkerWithhLat:(NSNumber *)latitude andLong:(NSNumber *)longitude andMarkerImage:(UIImage *)markerImage{
    
}
-(void)createMarkerForPickupWithhLat:(NSNumber *)latitude andLong:(NSNumber *)longitude andMarkerImage:(UIImage *)markerImage withWalkingDistance:(NSString *)distance{
    
}
- (void)drawDashedLineOnMapBetweenOrigin:(CLLocation *)originLocation destination:(CLLocation *)destinationLocation{
    
}

-(void)removeMarkersFromMap{
}

-(void)putMarkersOnSameMap{
}
@end
