//
//  DriverDetailDocumentsEntity.m
//  zify
//
//  Created by Anurag Rathor on 23/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "DriverDetailDocumentsEntity.h"

@implementation DriverDetailDocumentsEntity
-(id)initWithDictionary:(NSDictionary *)infoDict{
    if([self init]){
        _drivingLicenceNum = [infoDict objectForKey:@"drivingLicenceNum"];
        _idCardNum = [infoDict objectForKey:@"idCardNum"];
        _idCardType = [infoDict objectForKey:@"idCardType"];
        _isVehicleImgUploaded = [infoDict objectForKey:@"isVehicleImgUploaded"];
        _vehicleImgUrl = [self getValueForKey:@"vehicleImgUrl" fromDictionary:infoDict];//[infoDict objectForKey:@"vehicleImgUrl"];
        _vehicleInsuranceNum = [infoDict objectForKey:@"vehicleInsuranceNum"];
        _vehicleModel = [infoDict objectForKey:@"vehicleModel"];
        _vehicleRegistrationNum = [infoDict objectForKey:@"vehicleRegistrationNum"];
    }
    return self;
}
-(NSString *)getValueForKey:(NSString *)key fromDictionary:(NSDictionary *)infoDict{
    id keyValue = [infoDict objectForKey:key];
    if(keyValue == (NSString *)[NSNull null] || keyValue == nil){
        keyValue = @"";
    }
    return keyValue;
}
@end
