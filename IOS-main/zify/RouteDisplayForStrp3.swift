//
//  RouteDisplayForStrp3.swift
//  zify
//
//  Created by Anurag S Rathor on 21/11/17.
//  Copyright © 2017 zify. All rights reserved.
//

import UIKit

class RouteDisplayForStrp3: UIViewController {

    @IBOutlet var messageHandler:MessageHandler! = MessageHandler()
    var isFromHomeToOffice:Bool = Bool()
    var driveRoutes:NSArray = NSArray()
    var routesCount:Int = Int()
    var currentRouteIndex:Int = Int()
    var backward:UIButton = UIButton()
    var forward:UIButton = UIButton()
    var doneBtn:UIButton = UIButton()
    var distanceLbl:UILabel = UILabel()
    var routeLbl:UILabel = UILabel()
    var presentRoute:DriveRoute = DriveRoute()

    var mapContainerView:MapContainerView = MapContainerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.red
       // self.navigationItem.backBarButtonItem?.title = ""
        self.title = NSLocalizedString(VC_PUBLISHRIDE_SELECTROUTE, comment: "")
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white,
        NSFontAttributeName:UIFont.init(name: MediumFont, size: 16) ?? 16.0 ]
        self.addBarButtonItem()
        self.getRoutesFromSourceToDestination()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func showRoutesOnMap() -> Void {
        presentRoute = self.driveRoutes.object(at: 0) as! DriveRoute
        routesCount = self.driveRoutes.count
        currentRouteIndex = 1
        self.createMapView(route: presentRoute)
        self.addFontsForControls()
        self.backward.isEnabled = false
        if(routesCount == 1){self.forward.isEnabled = false}
        else{self.forward.isEnabled = true}
    }
    func createMapView(route:DriveRoute) -> Void {
        mapContainerView = MapContainerView.init()
        mapContainerView.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frameMaxY)!, width:AppDelegate.screen_WIDTH(), height: AppDelegate.screen_HEIGHT()-(self.navigationController?.navigationBar.frameMaxY)!)
        mapContainerView.backgroundColor = UIColor .green
        self.view.addSubview(mapContainerView)
        mapContainerView.createMapView()
        
        
        doneBtn = UIButton.init(type: UIButtonType.custom);
        doneBtn.frame =  CGRect(x: 0, y: 0, width:AppDelegate.screen_WIDTH(), height: 45);
        doneBtn.frameY = AppDelegate.screen_HEIGHT() - doneBtn.frameHeight
        doneBtn.backgroundColor = UIColor.blue
        doneBtn.setTitle(NSLocalizedString(CMON_GENERIC_DONE, comment: ""), for: .normal)
        doneBtn.setTitle(NSLocalizedString(CMON_GENERIC_DONE, comment: ""), for: .selected)
        self.view.addSubview(doneBtn);
        doneBtn.addTarget(self, action: #selector(doneBtnTapped), for: .touchUpInside)
        
        let viewHeight:CGFloat = doneBtn.frameHeight;
        let routeDetailsView:UIView = UIView.init()
        routeDetailsView.frame = CGRect(x: 15, y: doneBtn.frameY -  viewHeight - 10, width:AppDelegate.screen_WIDTH() - 30, height: viewHeight)
        routeDetailsView.backgroundColor = UIColor.white
        self.view.addSubview(routeDetailsView)
        
        
        let backBtnHeight:CGFloat = 25;
        backward = UIButton.init(type: UIButtonType.custom);
        backward.frame =  CGRect(x: 10, y: (routeDetailsView.frameHeight - backBtnHeight)/2, width:backBtnHeight, height: backBtnHeight);
        backward.setImage(UIImage.init(named: "left_arrow.png"), for: .normal)
        backward.setImage(UIImage.init(named: "left_arrow.png"), for: .selected)
        routeDetailsView.addSubview(backward);
        backward.addTarget(self, action: #selector(moveBackward), for: .touchUpInside)
        backward.backgroundColor = UIColor.lightGray
        
       
        
        
        forward = UIButton.init(type: UIButtonType.custom);
        forward.frame =  backward.frame;
        forward.frameX = routeDetailsView.frameWidth - forward.frameWidth - backward.frameX
        forward.setImage(UIImage.init(named: "right_arrow.png"), for: .normal)
        forward.setImage(UIImage.init(named: "right_arrow.png"), for: .selected)
        routeDetailsView.addSubview(forward);
        forward.addTarget(self, action: #selector(moveForward), for: .touchUpInside)
        forward.backgroundColor = UIColor.lightGray

        let lblWidth:CGFloat = (forward.frameX - backward.frameMaxX)/2
        distanceLbl = UILabel.init()
        distanceLbl.frame = CGRect(x: backward.frameMaxX+5, y: 0, width:lblWidth-5, height: routeDetailsView.frameHeight);
        distanceLbl.textAlignment = .left
        distanceLbl.backgroundColor = UIColor.clear
        distanceLbl.textColor = UIColor.red
        routeDetailsView.addSubview(distanceLbl)
        
        routeLbl = UILabel.init()
        routeLbl.frame = distanceLbl.frame;
        routeLbl.frameX = distanceLbl.frameMaxX
        routeLbl.textAlignment = .right
        routeLbl.backgroundColor = UIColor.clear
        routeDetailsView.addSubview(routeLbl)
       
        let mapView = mapContainerView.mapView
        mapView?.sourceCoordinate = CLLocationCoordinate2DMake(Double(route.srcLat)!, Double(route.srcLong)!)
        mapView?.destCoordinate = CLLocationCoordinate2DMake(Double(route.destLat)!, Double(route.destLong)!)
        mapView?.overviewPolylinePoints = route.overviewPolylinePoints
        mapView?.drawMap()
        self.displayTextOnView()
        
    }
    func addFontsForControls() -> Void {
        let titleFont:CGFloat = 14;
        let buttonFont:CGFloat = 16;
        distanceLbl.font = UIFont.init(name: NormalFont, size: titleFont)
        routeLbl.font = UIFont.init(name: NormalFont, size: titleFont)
        doneBtn.titleLabel?.font = UIFont.init(name: MediumFont, size: buttonFont)
    }
    func moveForward() -> Void {
        currentRouteIndex = currentRouteIndex + 1
        backward.isEnabled = true
        if(currentRouteIndex == routesCount){forward.isEnabled = false}
        self.redrawMapPath()
    }
    
    func moveBackward() -> Void {
        currentRouteIndex = currentRouteIndex - 1
        forward.isEnabled = true
        if(currentRouteIndex == 1){backward.isEnabled = false}
        self.redrawMapPath()
    }
    func displayTextOnView() -> Void {
        routeLbl.text = String(format: "%@ %d/%d", NSLocalizedString(VC_PUBLISHRIDE_ROUTELBL, comment: ""),currentRouteIndex,routesCount)
        distanceLbl.text = String(format: "%@ %@", presentRoute.distance,CurrentLocale.sharedInstance().getDistanceUnit())
    }
    
    func redrawMapPath() -> Void {
        presentRoute = driveRoutes.object(at: currentRouteIndex - 1) as! DriveRoute
        self.displayTextOnView()
        mapContainerView.mapView.overviewPolylinePoints = presentRoute.overviewPolylinePoints
        mapContainerView.mapView.drawPath(for: Int32(currentRouteIndex - 1))
    }
    func doneBtnTapped(sender:UIButton!) -> Void {
        //*,*oFFToHomeDriveRoute;

        if(isFromHomeToOffice){
            AppDelegate.getInstance().homeToOffDriveRoute = presentRoute
        }else{
            AppDelegate.getInstance().oFFToHomeDriveRoute = presentRoute
        }
        self.dismiss(animated: true) {
        }
    }
    func dismiss(sender:UIButton!){
        self.dismiss(animated: true) {
        }
    }
    
    func getRoutesFromSourceToDestination() -> Void {
        self.messageHandler.showBlockingLoadView {
            let userRideReq:UserRideRequest
            if(self.isFromHomeToOffice){
                userRideReq =  UserRideRequest.init(requestType: OFFERRIDE, andSourceLocality: AppDelegate.getInstance().homeAddress, andDestinationLocality: AppDelegate.getInstance().officeAddress, andRideDate: "", andSeats:1, andMerchantId: "") as UserRideRequest
            }else{
                userRideReq = UserRideRequest.init(requestType: OFFERRIDE, andSourceLocality: AppDelegate.getInstance().officeAddress, andDestinationLocality: AppDelegate.getInstance().homeAddress, andRideDate: "", andSeats:1, andMerchantId: "") as UserRideRequest
            }
            ServerInterface.sharedInstance().getResponse(userRideReq, withHandler: { (response:ServerResponse?, error) in
                self.messageHandler.dismissBlockingLoadView(handler: {
                    if(response != nil){
                        let routesArr = response?.responseObject as! NSArray
                        if(routesArr.count == 0){
                            self.messageHandler.showErrorMessage(NSLocalizedString(VC_SEARCHRIDE_NODRIVESMESSAGE, comment: ""))
                        }else{
                            print(routesArr);
                            self.driveRoutes = routesArr;
                            self.showRoutesOnMap()
                        }
                    }else{
                        self.messageHandler.showErrorMessage(NSLocalizedString(CMON_GENERIC_INTERNALERROR, comment: ""))
                    }
                })
            })
        }
    }
    func addBarButtonItem(){
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "left_arrow.png"), for: .normal)
        btn1.setImage(UIImage(named: "left_arrow.png"), for: .selected)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)

    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
