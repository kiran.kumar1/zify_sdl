//
//  LocalAddressSearch.h
//  commons
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface OneSignalPlayerid : NSManagedObject
@property (nonatomic, retain) NSString  *playerId;

+(OneSignalPlayerid*)insertOneSignalPlayerIdIntoDB:(NSString *)playerId inContext:(NSManagedObjectContext *)context;
+(OneSignalPlayerid*)getPlayerIdObjForDeviceInContext:(NSManagedObjectContext *)context;
+(void)updatePlayerIdAfterReceived:(OneSignalPlayerid *)obj;



@end
