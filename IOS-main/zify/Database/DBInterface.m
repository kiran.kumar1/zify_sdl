
//
//  DBInterface.m
//  zify
//
//  Created by Anurag S Rathor on 12/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "DBInterface.h"

@implementation DBInterface

@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedStore = _managedStore;
@synthesize sharedContext = _sharedContext;

+(DBInterface *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[DBInterface alloc]init];
    });
    return instance;
}

-(id) init{
    self = [super init];
    [self sharedManageStore];
    return self;
}
-(RKManagedObjectStore*) sharedManageStore{
    if(_managedStore==nil){
        NSURL *documentDirURL = [self applicationDocumentsDirectory];
        NSURL *storeURL = [documentDirURL URLByAppendingPathComponent:@"Zify.sqlite"];
        NSLog(@"sqlite url is %@",storeURL);
        NSError *error;
        NSString *sQLiteBundlePath = [[NSBundle mainBundle] pathForResource:@"Zify" ofType:@"sqlite"];

        NSDictionary *storeMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeURL error:&error];
        BOOL isDbVersionChanged =  [self isDbVersionChanged];
        if (storeMetadata == nil || [[self managedObjectModel] isConfiguration:nil compatibleWithStoreMetadata:storeMetadata] == NO || isDbVersionChanged) {
            if ([[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]]) {
                [[NSFileManager defaultManager] removeItemAtPath:[storeURL path] error:&error];
            }
            if(![[NSFileManager defaultManager] copyItemAtPath:sQLiteBundlePath toPath:[storeURL path] error:&error]){
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            } else {
              //  [[NSFileManager defaultManager] removeItemAtPath:sQLiteBundlePath error:&error];
            }
            if(isDbVersionChanged) [self changeStaticDbVersion];
        }
        _managedStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:[self managedObjectModel]];
        NSPersistentStore *persistentStore = [_managedStore addSQLitePersistentStoreAtPath:[storeURL path] fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:&error];
        if (persistentStore == nil) {
            NSLog(@"Store Configuration Failure %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
        } else{
            [_managedStore createManagedObjectContexts];
            [self createSharedContext];
        }
        if ([[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[storeURL path] error:&error];
        }
        if(![[NSFileManager defaultManager] copyItemAtPath:sQLiteBundlePath toPath:[storeURL path] error:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } else {
              [[NSFileManager defaultManager] removeItemAtPath:sQLiteBundlePath error:&error];
        }
    }
    return _managedStore;
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ZifyDataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}


-(NSManagedObjectContext *)sharedMainContext{
    return _managedStore.mainQueueManagedObjectContext;
}

- (void)saveSharedContext{
    //[ _sharedContext performBlockAndWait:^{
         NSError *error = nil;
        if([_sharedContext hasChanges] && ![_sharedContext save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self saveChangesToStore];
    //}];
}

- (void)saveSharedContextInBackground{
    [ _sharedContext performBlock:^{
        NSError *error = nil;
        if([_sharedContext hasChanges] && ![_sharedContext save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self saveChangesToStore];
    }];
}


-(NSManagedObjectContext *)sharedContext{
    return _sharedContext;
}

- (void)saveChangesToStore {
    NSManagedObjectContext *writeManagedObjectContext = _managedStore.persistentStoreManagedObjectContext;
    NSManagedObjectContext *mainManagedObjectContext = _managedStore.mainQueueManagedObjectContext;
    [mainManagedObjectContext performBlock:^{
        NSError *error = nil;
        if ([mainManagedObjectContext hasChanges] && ![mainManagedObjectContext save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
        [writeManagedObjectContext performBlock:^{
            NSError *error = nil;
            if ([writeManagedObjectContext hasChanges] && ![writeManagedObjectContext save:&error])
            {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
        }];
    }];
}


-(BOOL)isDbVersionChanged{
    return true;
     NSString *version =  [[NSUserDefaults standardUserDefaults] objectForKey:@"staticDbVersion"];
    if(!version) return true;
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    if(![version isEqualToString:majorVersion]) return true;
    return false;
}

-(void)changeStaticDbVersion{
    NSString *version =  [[NSUserDefaults standardUserDefaults] objectForKey:@"staticDbVersion"];
    if(version){
      [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"staticDbVersion"];
    }
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    [[NSUserDefaults standardUserDefaults] setValue:majorVersion forKey:@"staticDbVersion"];
}

-(void)createSharedContext{
    _sharedContext=[[NSManagedObjectContext alloc]initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    _sharedContext.parentContext=_managedStore.mainQueueManagedObjectContext;
    _sharedContext.mergePolicy=NSMergeByPropertyStoreTrumpMergePolicy;
}
@end
