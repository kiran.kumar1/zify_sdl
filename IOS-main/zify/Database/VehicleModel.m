//
//  VehicleModel.m
//  zify
//
//  Created by Anurag S Rathor on 02/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "VehicleModel.h"

@implementation VehicleModel
@dynamic modelName;
+(VehicleModel *)createVehicleModel:(NSString *)modelName inContext:(NSManagedObjectContext *)context{
    VehicleModel *vehicleModel = [NSEntityDescription insertNewObjectForEntityForName:@"VehicleModel" inManagedObjectContext:context];
    vehicleModel.modelName = modelName;
    return vehicleModel;
}
@end
