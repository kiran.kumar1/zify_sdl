//
//  DBUserDataInterface.m
//  zify
//
//  Created by Anurag S Rathor on 14/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "DBUserDataInterface.h"

@implementation DBUserDataInterface

@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedStore = _managedStore;
@synthesize sharedContext = _sharedContext;

+(DBUserDataInterface *)sharedInstance{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance=[[DBUserDataInterface alloc]init];
    });
    return instance;
}

-(id) init{
    self = [super init];
    [self sharedManageStore];
    return self;
}
-(RKManagedObjectStore*) sharedManageStore{
    if(_managedStore==nil){
        NSURL *documentDirURL = [self applicationDocumentsDirectory];
        NSURL *storeURL = [documentDirURL  URLByAppendingPathComponent:@"ZifyUserData.sqlite"];
        NSError *error;
        NSDictionary *storeMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeURL error:&error];
        NSDictionary *migrationOptions = nil;
        if (storeMetadata == nil) {
            NSString *sQLiteBundlePath = [[NSBundle mainBundle] pathForResource:@"ZifyUserData" ofType:@"sqlite"];
            if(![[NSFileManager defaultManager] copyItemAtPath:sQLiteBundlePath toPath:[storeURL path] error:&error]){
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            } else {
                [[NSFileManager defaultManager] removeItemAtPath:sQLiteBundlePath error:&error];
            }
        } else if([[self managedObjectModel] isConfiguration:nil compatibleWithStoreMetadata:storeMetadata] == NO){
            migrationOptions = @{NSMigratePersistentStoresAutomaticallyOption : @YES,
                                 NSInferMappingModelAutomaticallyOption : @YES};
            NSString *sQLiteBundlePath = [[NSBundle mainBundle] pathForResource:@"ZifyUserData" ofType:@"sqlite"];
            if ([[NSFileManager defaultManager] fileExistsAtPath:sQLiteBundlePath]) {
                [[NSFileManager defaultManager] removeItemAtPath:sQLiteBundlePath error:&error];
            }
        }
        _managedStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:[self managedObjectModel]];
        NSPersistentStore *persistentStore = [_managedStore addSQLitePersistentStoreAtPath:[storeURL path] fromSeedDatabaseAtPath:nil withConfiguration:nil options:migrationOptions error:&error];
        if (persistentStore == nil) {
            NSLog(@"Store Configuration Failure %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
        } else{
            [_managedStore createManagedObjectContexts];
            [self createSharedContext];
        }
    }
    return _managedStore;
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ZifyUserDataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}


-(NSManagedObjectContext *)sharedMainContext{
    return _managedStore.mainQueueManagedObjectContext;
}

- (void)saveSharedContext{
    [ _sharedContext performBlockAndWait:^{
        @try{
            NSError *error = nil;
            if([_sharedContext hasChanges] && ![_sharedContext save:&error]){
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
            [self saveChangesToStore];
        }@catch(NSException *ex){
            NSLog(@"excep is %@", [ex description]);
        }
    }];
}

- (void)saveSharedContextInBackground{
    [ _sharedContext performBlock:^{
        NSError *error = nil;
        if([_sharedContext hasChanges] && ![_sharedContext save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self saveChangesToStore];
    }];
}


-(NSManagedObjectContext *)sharedContext{
    return _sharedContext;
}

- (void)saveChangesToStore {
    NSManagedObjectContext *writeManagedObjectContext = _managedStore.persistentStoreManagedObjectContext;
    NSManagedObjectContext *mainManagedObjectContext = _managedStore.mainQueueManagedObjectContext;
    //  [mainManagedObjectContext performBlock:^{
    NSError *error = nil;
    if ([mainManagedObjectContext hasChanges] && ![mainManagedObjectContext save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    [writeManagedObjectContext performBlock:^{
        NSError *error = nil;
        if ([writeManagedObjectContext hasChanges] && ![writeManagedObjectContext save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }];
    //}];
}

-(void)createSharedContext{
    _sharedContext=[[NSManagedObjectContext alloc]initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    _sharedContext.parentContext=_managedStore.mainQueueManagedObjectContext;
    _sharedContext.mergePolicy=NSMergeByPropertyStoreTrumpMergePolicy;
}


@end


