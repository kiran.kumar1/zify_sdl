//
//  QBChatUserMessage.h
//  zify
//
//  Created by Anurag S Rathor on 08/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface QBChatUserMessage : NSManagedObject
@property (nonatomic,retain) NSString *chatIdentifier;
@property (nonatomic,retain) NSString *message;
@property (nonatomic,retain) NSDate *messageTime;
@property (nonatomic,retain) NSNumber *isSelfMessage;
+(QBChatUserMessage *)insertChatUserMessageWithIdentifier:(NSString *)chatIdentifier andMessage:(NSString *)message andMessageTime:(NSDate *)messageTime andIsSelfMessage:(NSNumber *)isSelfMessage inContext:(NSManagedObjectContext *)context;
+(NSUInteger)getChatUserMessageCountWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context;
+(void)deleteOldestChatUserMessageWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context;
+(void)deletChatUserMessagesWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context;
+(NSArray *)getChatUserMessagesWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context;
+(void)deleteChatUserMessagesInContext:(NSManagedObjectContext *)context;
@end
