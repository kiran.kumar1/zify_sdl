//
//  PushAccessData.m
//  zify
//
//  Created by Anurag S Rathor on 19/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushAccessData.h"
#import "UserAccessData.h"
#import "DBUserDataInterface.h"

#define PUSH_TOKEN @"pushToken"
#define USER_DISABLED_PUSH @"userDisabledPush"
#define CHAT_PUSH_TOKEN @"chatPushToken"

@implementation PushAccessData
+(void)getPushAccessDatawithCompletion:(void(^)(NSString *, BOOL))completion{
    
    @try{
    __block NSArray *fetchedObjects = nil;
    __block NSError *error = nil;
     NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity=[NSEntityDescription entityForName:@"UserAccessData" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dataType = %@ or dataType = %@", PUSH_TOKEN, USER_DISABLED_PUSH];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    __block NSString *pushToken;
    __block BOOL isuserDisabledPush = NO;
    if(!error && fetchedObjects.count != 0){
        [fetchedObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UserAccessData *accessData = (UserAccessData *)obj;
            if([PUSH_TOKEN isEqualToString:accessData.dataType]){
                pushToken = accessData.value;
            } else if([USER_DISABLED_PUSH isEqualToString:accessData.dataType]){
                isuserDisabledPush = accessData.value.boolValue;
            }
        }];
    }
    completion(pushToken,isuserDisabledPush);
    }@catch(NSException *ex){
        NSLog(@"exception is %@", [ex description]);
    }
}

+(void)createPushAccessData:(enum PushDataType)dataType andValue:(NSString *)value{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    //[context performBlockAndWait:^{
        NSString *key = [PushAccessData getAccessDataKey:dataType];
        NSLog(@"key is %@ and value is %@", key, value);
        [UserAccessData createUserAccessDataWithType:key andValue:value inContext:context];
        [[DBUserDataInterface sharedInstance] saveSharedContext];
    //}];
}

+(void)deletePushAccessData:(enum PushDataType)dataType{
     NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    //[context performBlockAndWait:^{
        NSString *key = [PushAccessData getAccessDataKey:dataType];
        [UserAccessData deleteUserAccessDataWithType:key inContext:context];
        [[DBUserDataInterface sharedInstance] saveSharedContext];
    //}];
}

+(NSString *)getPushAccessData:(enum PushDataType)dataType{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    __block UserAccessData *accessData;
    //[context performBlockAndWait:^{
        NSString *key = [PushAccessData getAccessDataKey:dataType];
        accessData = [UserAccessData getUserAccessDataWithType:key inContext:context];
    //}];
    if(accessData) return accessData.value;
    return nil;
}

+(NSString *)getAccessDataKey:(enum PushDataType)dataType{
    NSString *key;
    if(PUSHTOKENTYPE == dataType){
        key = PUSH_TOKEN;
    } else if(USERDISABLEDPUSHTYPE == dataType){
        key = USER_DISABLED_PUSH;
    } else if(CHATPUSHTOKENTYPE == dataType){
        key = CHAT_PUSH_TOKEN;
    }
    return key;
}
@end
