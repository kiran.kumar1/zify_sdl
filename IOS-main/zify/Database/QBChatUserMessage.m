//
//  QBChatUserMessage.m
//  zify
//
//  Created by Anurag S Rathor on 08/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "QBChatUserMessage.h"

@implementation QBChatUserMessage
@dynamic chatIdentifier;
@dynamic isSelfMessage;
@dynamic message;
@dynamic messageTime;

+(QBChatUserMessage *)insertChatUserMessageWithIdentifier:(NSString *)chatIdentifier andMessage:(NSString *)message andMessageTime:(NSDate *)messageTime andIsSelfMessage:(NSNumber *)isSelfMessage inContext:(NSManagedObjectContext *)context{
    QBChatUserMessage *chatUserMessage = [NSEntityDescription insertNewObjectForEntityForName:@"QBChatUserMessage" inManagedObjectContext:context];
    chatUserMessage.chatIdentifier = chatIdentifier;
    chatUserMessage.message = message;
    chatUserMessage.messageTime = messageTime;
    chatUserMessage.isSelfMessage = isSelfMessage;
    return chatUserMessage;
}

+(NSUInteger)getChatUserMessageCountWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"QBChatUserMessage" inManagedObjectContext: context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatIdentifier = %@",chatIdentifier];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:fetchRequest error: &error];
    return count;
}

+(void)deleteOldestChatUserMessageWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"QBChatUserMessage" inManagedObjectContext: context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatIdentifier = %@ ",chatIdentifier];
    [fetchRequest setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"messageTime" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(!error && fetchedObjects.count != 0){
        QBChatUserMessage *chatUserMessage =  (QBChatUserMessage *)[fetchedObjects objectAtIndex:0];
        [context deleteObject:chatUserMessage];
    }
}

+(void)deletChatUserMessagesWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context{
    NSArray *chatUserMessages = [self getChatUserMessagesWithIdentifier:chatIdentifier inContext:context];
    if(chatUserMessages){
        for (NSManagedObject *chatUserMessage in chatUserMessages) {
            [context deleteObject:chatUserMessage];
        }
    }
}

+(NSArray *)getChatUserMessagesWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"QBChatUserMessage" inManagedObjectContext: context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatIdentifier = %@ ",chatIdentifier];
    [fetchRequest setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"messageTime" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(!error && fetchedObjects.count != 0){
        return fetchedObjects;
    }
    return [[NSArray alloc] init];
}

+(void)deleteChatUserMessagesInContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"QBChatUserMessage" inManagedObjectContext: context]];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(!error && fetchedObjects.count != 0){
        for (NSManagedObject *chatUserMessage in fetchedObjects) {
            [context deleteObject:chatUserMessage];
        }
    }
}
@end
