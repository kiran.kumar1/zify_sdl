//
//  UserAccessData.m
//  zify
//
//  Created by Anurag S Rathor on 14/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserAccessData.h"
#import "DBUserDataInterface.h"

@implementation UserAccessData
@dynamic dataType;
@dynamic value;
+(UserAccessData *)createUserAccessDataWithType:(NSString *)dataType andValue:(NSString *)value inContext:(NSManagedObjectContext *)context{
    UserAccessData *accessData = [NSEntityDescription insertNewObjectForEntityForName:@"UserAccessData" inManagedObjectContext:context];
    accessData.dataType = dataType;
    accessData.value = value;
    return accessData;
}

+(void)deleteUserAccessDataWithType:(NSString *)dataType inContext:(NSManagedObjectContext *)context{
    UserAccessData *accessData= [UserAccessData getUserAccessDataWithType:dataType inContext:context];
    if(accessData){
        [context deleteObject:accessData];
    }
}

+(UserAccessData *)getUserAccessDataWithType:(NSString *)dataType inContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity=[NSEntityDescription entityForName:@"UserAccessData" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dataType = %@",dataType];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(!error && fetchedObjects.count != 0){
        return (UserAccessData *)[fetchedObjects objectAtIndex:0];
    }
    return nil;
}
@end
