//
//  QBChatUser.m
//  zify
//
//  Created by Anurag S Rathor on 08/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "QBChatUser.h"

@implementation QBChatUser
@dynamic chatIdentifier;
@dynamic imageUrl;
@dynamic lastMessage;
@dynamic lastMessageTime;
@dynamic firstName;
@dynamic lastName;
@dynamic unreadCount;
@dynamic dialogId;

+(void)insertChatUserWithIdentifier:(NSString *)chatIdentifier andImageUrl:(NSString *)imageUrl andLastMessage:(NSString *)lastMessage andLastMessageTime:(NSDate *)lastMessageTime andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andUnreadCount:(NSNumber *)unreadcount andDialogId:(NSString *)dialogId inContext:(NSManagedObjectContext *)context{
    QBChatUser *chatUser = [NSEntityDescription insertNewObjectForEntityForName:@"QBChatUser" inManagedObjectContext:context];
    chatUser.chatIdentifier = chatIdentifier;
    chatUser.imageUrl = imageUrl;
    chatUser.firstName = firstName;
    chatUser.lastName = lastName;
    chatUser.dialogId = dialogId;
    chatUser.lastMessage = lastMessage;
    chatUser.lastMessageTime = lastMessageTime;
    chatUser.unreadCount = unreadcount;
}

+(QBChatUser *)getChatUserWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity=[NSEntityDescription entityForName:@"QBChatUser" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatIdentifier = %@",chatIdentifier];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSLog(@"fetched Obj count is %d", (int)[fetchedObjects count]);
    if(!error && fetchedObjects.count != 0){
        return (QBChatUser *)[fetchedObjects objectAtIndex:0];
    }
    return nil;
}

+(NSUInteger)getChatUserCountInContext:(NSManagedObjectContext *)context;{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"QBChatUser" inManagedObjectContext: context]];
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:fetchRequest error: &error];
    return count;
}

+(NSString *)deleteOldestChatUserInContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"QBChatUser" inManagedObjectContext:context]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"lastMessageTime" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSString *chatReceiverIdentifier;
    if(!error && fetchedObjects.count != 0){
        QBChatUser *chatUser =  (QBChatUser *)[fetchedObjects objectAtIndex:0];
        chatReceiverIdentifier = chatUser.chatIdentifier;
        [context deleteObject:chatUser];
    }
    return chatReceiverIdentifier;
}

+(NSArray *)getChatUsersInContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"QBChatUser" inManagedObjectContext:context]];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"lastMessageTime" ascending:NO];
    NSArray *sortDescription = [[NSArray alloc] initWithObjects:sort, nil];
    [fetchRequest setSortDescriptors:sortDescription];
    
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(!error && fetchedObjects.count != 0){
        return fetchedObjects;
    }
    return [[NSArray alloc] init];
}

+(void)deleteChatUsersInContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"QBChatUser" inManagedObjectContext:context]];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(!error && fetchedObjects.count != 0){
        for (NSManagedObject *chatUser in fetchedObjects) {
            [context deleteObject:chatUser];
        }
    }
}


@end
