//
//  CurrencyInfo.h
//  zify
//
//  Created by Anurag S Rathor on 08/09/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface CurrencyInfo : NSManagedObject
@property (nonatomic, retain) NSString *isoCode;
@property (nonatomic, retain) NSString *currencyCode;
@property (nonatomic, retain) NSString *locale;
@property (nonatomic, retain) NSString *unicode;
@property (nonatomic, retain) NSNumber *useUnicode;
+(CurrencyInfo *)createCurrencyInfoWithCurrencyCode:(NSString *)currencyCode andLocale:(NSString *)locale andUnicode:(NSString *)unicode andUseUnicode:(NSNumber *)useUnicode withIsoCode:(NSString *)isoCode inContext:(NSManagedObjectContext *)context;
+(CurrencyInfo*)getCurrencyInfoForCurrencyCode:(NSString*)currencyCode inContext:(NSManagedObjectContext *)context;
+(CurrencyInfo*)getCurrencyInfoForCountryIsoCode:(NSString*)isoCode inContext:(NSManagedObjectContext *)context;
@end
