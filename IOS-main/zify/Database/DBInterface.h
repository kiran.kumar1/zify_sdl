//
//  DBInterface.h
//  zify
//
//  Created by Anurag S Rathor on 12/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  "RestKit/RestKit.h"

@interface DBInterface : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) RKManagedObjectStore *managedStore;
@property (readonly, strong, nonatomic) NSManagedObjectContext *sharedContext;

+(DBInterface *)sharedInstance;
-(RKManagedObjectStore*) sharedManageStore;
-(NSManagedObjectContext *)sharedMainContext;
- (void)saveSharedContext;
- (void)saveSharedContextInBackground;
@end
