//
//  TutorialShownDB.m
//  commons
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TutorialShownDB.h"
#import "DBUserDataInterface.h"

#define entityName @"TutorialShownDB"

@implementation TutorialShownDB
@dynamic isInGuestHomeShown;
@dynamic isInCreateRideShown;
@dynamic isInPublishRideShown;

+(TutorialShownDB*)getTutorialsObjectForuserInContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
   // [predicates addObject:[NSPredicate predicateWithFormat:@"isoCode = %@", isoCode]];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }
    
}

+(TutorialShownDB *)insertTutorialIntoDB:(NSString *)isInGuestHomeShown andisInCreateRideShown:(NSString *)isInCreateRideShown andisInPublishRideShown:(NSString *)isInPublishRideShown inContext:(NSManagedObjectContext *)context{
    TutorialShownDB *search = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    search.isInGuestHomeShown = isInGuestHomeShown;
    search.isInCreateRideShown = isInCreateRideShown;
    search.isInPublishRideShown = isInPublishRideShown;
    [[DBUserDataInterface sharedInstance] saveSharedContext];
    return search;
}


+(void)getCurrentRideWithCompletion:(void(^)(TutorialShownDB *))completion{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    [context performBlock:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity=[NSEntityDescription entityForName:entityName inManagedObjectContext:context];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        if(!error && fetchedObjects.count != 0){
            TutorialShownDB *updating = (TutorialShownDB *)[fetchedObjects objectAtIndex:0];
            completion(updating);
        } else{
            completion(nil);
        }
    }];
}

+(void)updateWithAppropriateValues:(TutorialShownDB *)obj{
    [TutorialShownDB getCurrentRideWithCompletion:^(TutorialShownDB *updating){
        updating.isInGuestHomeShown = obj.isInGuestHomeShown;
        updating.isInCreateRideShown = obj.isInCreateRideShown;
        updating.isInPublishRideShown = obj.isInPublishRideShown;     [[DBUserDataInterface sharedInstance] saveSharedContextInBackground];
    }];
}


@end
