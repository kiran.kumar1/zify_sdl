//
//  TutorialShownDB.m
//  commons
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "OneSignalPlayerid.h"
#import "DBUserDataInterface.h"

#define entityName @"OneSignalPlayerid"

@implementation OneSignalPlayerid
@dynamic playerId;


+(OneSignalPlayerid*)insertOneSignalPlayerIdIntoDB:(NSString *)playerId inContext:(NSManagedObjectContext *)context{

    OneSignalPlayerid *oneSignalObj = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    oneSignalObj.playerId = playerId;
    [[DBUserDataInterface sharedInstance] saveSharedContext];
    return oneSignalObj;
}

+(OneSignalPlayerid*)getPlayerIdObjForDeviceInContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }
    
}
+(void)getCurrentObjWithCompletion:(void(^)(OneSignalPlayerid *))completion{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    [context performBlock:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity=[NSEntityDescription entityForName:entityName inManagedObjectContext:context];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        if(!error && fetchedObjects.count != 0){
            OneSignalPlayerid *updating = (OneSignalPlayerid *)[fetchedObjects objectAtIndex:0];
            completion(updating);
        } else{
            completion(nil);
        }
    }];
}
+(void)updatePlayerIdAfterReceived:(OneSignalPlayerid *)obj{
    NSLog(@"obj pplayer id is %@", obj.playerId);
    [OneSignalPlayerid getCurrentObjWithCompletion:^(OneSignalPlayerid *updating){
        updating.playerId = obj.playerId;
        [[DBUserDataInterface sharedInstance] saveSharedContextInBackground];
    }];
}


@end
