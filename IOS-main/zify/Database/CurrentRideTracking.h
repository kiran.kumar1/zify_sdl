//
//  CurrentRideTracking.h
//  zify
//
//  Created by Anurag S Rathor on 01/10/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GoogleMaps;
#import "RideDriveActionRequest.h"
#import <CoreLocation/CoreLocation.h>
#import "DBInterface.h"

@interface CurrentRideTracking : NSManagedObject
@property (nonatomic,retain) NSDate *createdTime;
@property (nonatomic,retain) NSNumber *driveId;
@property (nonatomic,retain) NSNumber *endLat;
@property (nonatomic,retain) NSNumber *endLong;
@property (nonatomic,retain) NSNumber *isUpdatedSuccessful;
@property (nonatomic,retain) NSNumber *isLocationTrackingCompleted;
@property (nonatomic,retain) NSArray *pathArray;
@property (nonatomic,retain) NSNumber *rideDuration;
@property (nonatomic,retain) NSNumber *rideId;
@property (nonatomic,retain) NSString *rideType;
@property (nonatomic,retain) NSNumber *startLat;
@property (nonatomic,retain) NSNumber *startLong;
@property (nonatomic,retain) NSString *status;
@property (nonatomic,retain) NSDate *updatedTime;
@property (nonatomic,retain) NSNumber *userId;
@property (nonatomic,retain) NSString *userToken;
@property (nonatomic,retain) NSNumber *currentLat;
@property (nonatomic,retain) NSNumber *currentLong;
@property (nonatomic,retain) NSString *encodedPolyLine;
+ (NSString *)encodeStringWithCoordinates:(NSArray *)coordinates;
+(CurrentRideTracking *)createCurrentRideTrackingObject:(RideDriveActionRequest *)request withDuration:(NSNumber *)duration;
+(CurrentRideTracking *)getCurrentRide;
+(void)updateCurrentTrackingObject:(CLLocation *)location withCompletion:(void(^)(CurrentRideTracking *))completion;
+(void)getCurrentRideWithCompletion:(void(^)(CurrentRideTracking *))completion;
+(void)completeWithLat:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withCompletion:(void(^)(CurrentRideTracking *))completion;
+(void)updateWithLocationTrackingStatus:(NSNumber *)status;
+(void)deleteCurrentRide;
@end
