//
//  CurrencyInfo.m
//  zify
//
//  Created by Anurag S Rathor on 08/09/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "CurrencyInfo.h"

@implementation CurrencyInfo
@dynamic currencyCode;
@dynamic locale;
@dynamic unicode;
@dynamic useUnicode;
@dynamic isoCode;
+(CurrencyInfo *)createCurrencyInfoWithCurrencyCode:(NSString *)currencyCode andLocale:(NSString *)locale andUnicode:(NSString *)unicode andUseUnicode:(NSNumber *)useUnicode withIsoCode:(NSString *)isoCode inContext:(NSManagedObjectContext *)context{
    CurrencyInfo *currencyInfo = [NSEntityDescription insertNewObjectForEntityForName:@"CurrencyInfo" inManagedObjectContext:context];
    currencyInfo.currencyCode = currencyCode;
    currencyInfo.locale = locale;
    currencyInfo.unicode = unicode;
    currencyInfo.useUnicode = useUnicode;
    currencyInfo.isoCode = isoCode;
    return currencyInfo;
}

+(CurrencyInfo*)getCurrencyInfoForCurrencyCode:(NSString*)currencyCode inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"CurrencyInfo" inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    [predicates addObject:[NSPredicate predicateWithFormat:@"currencyCode = %@", currencyCode]];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }
    
}
+(CurrencyInfo*)getCurrencyInfoForCountryIsoCode:(NSString*)isoCode inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"CurrencyInfo" inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    [predicates addObject:[NSPredicate predicateWithFormat:@"isoCode = %@", isoCode]];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }
}

@end
