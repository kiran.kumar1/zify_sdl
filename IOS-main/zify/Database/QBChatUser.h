//
//  QBChatUser.h
//  zify
//
//  Created by Anurag S Rathor on 08/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface QBChatUser : NSManagedObject
@property (nonatomic,retain) NSString *chatIdentifier;
@property (nonatomic,retain) NSString *imageUrl;
@property (nonatomic,retain) NSString *lastMessage;
@property (nonatomic,retain) NSDate *lastMessageTime;
@property (nonatomic,retain) NSString *firstName;
@property (nonatomic,retain) NSString *lastName;
@property (nonatomic,retain) NSNumber *unreadCount;
@property (nonatomic,retain) NSString *dialogId;
+(void)insertChatUserWithIdentifier:(NSString *)chatIdentifier andImageUrl:(NSString *)imageUrl andLastMessage:(NSString *)lastMessage andLastMessageTime:(NSDate *)lastMessageTime andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andUnreadCount:(NSNumber *)unreadcount andDialogId:(NSString *)dialogId inContext:(NSManagedObjectContext *)context;
+(QBChatUser *)getChatUserWithIdentifier:(NSString *)chatIdentifier inContext:(NSManagedObjectContext *)context;
+(NSUInteger)getChatUserCountInContext:(NSManagedObjectContext *)context;
+(NSString*)deleteOldestChatUserInContext:(NSManagedObjectContext *)context;
+(NSArray *)getChatUsersInContext:(NSManagedObjectContext *)context;
+(void)deleteChatUsersInContext:(NSManagedObjectContext *)context;

@end
