//
//  DBUserDataInterface.h
//  zify
//
//  Created by Anurag S Rathor on 14/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  "RestKit/RestKit.h"

@interface DBUserDataInterface : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) RKManagedObjectStore *managedStore;
@property (readonly, strong, nonatomic) NSManagedObjectContext *sharedContext;

+(DBUserDataInterface *)sharedInstance;
-(RKManagedObjectStore*) sharedManageStore;
-(NSManagedObjectContext *)sharedMainContext;
- (void)saveSharedContext;
- (void)saveSharedContextInBackground;
@end
