//
//  UserAccessData.h
//  zify
//
//  Created by Anurag S Rathor on 14/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface UserAccessData : NSManagedObject
@property (nonatomic,retain) NSString *dataType;
@property (nonatomic,retain) NSString *value;
+(UserAccessData *)createUserAccessDataWithType:(NSString *)dataType andValue:(NSString *)value inContext:(NSManagedObjectContext *)context;
+(void)deleteUserAccessDataWithType:(NSString *)dataType inContext:(NSManagedObjectContext *)context;
+(UserAccessData *)getUserAccessDataWithType:(NSString *)dataType inContext:(NSManagedObjectContext *)context;
@end
