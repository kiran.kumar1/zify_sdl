//
//  PushAccessData.h
//  zify
//
//  Created by Anurag S Rathor on 19/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

enum PushDataType{
    PUSHTOKENTYPE,USERDISABLEDPUSHTYPE,CHATPUSHTOKENTYPE
};

@interface PushAccessData : NSObject
+(void)getPushAccessDatawithCompletion:(void(^)(NSString *, BOOL))completion;
+(void)deletePushAccessData:(enum PushDataType)dataType;
+(void)createPushAccessData:(enum PushDataType)dataType andValue:(NSString *)value;
+(NSString *)getPushAccessData:(enum PushDataType)dataType;
@end
