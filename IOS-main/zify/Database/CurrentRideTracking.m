//
//  CurrentRideTracking.m
//  zify
//
//  Created by Anurag S Rathor on 01/10/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import "CurrentRideTracking.h"
#import "UserProfile.h"

@implementation CurrentRideTracking
@dynamic createdTime;
@dynamic driveId;
@dynamic endLat;
@dynamic endLong;
@dynamic isUpdatedSuccessful;
@dynamic pathArray;
@dynamic rideDuration;
@dynamic rideId;
@dynamic rideType;
@dynamic startLat;
@dynamic startLong;
@dynamic status;
@dynamic updatedTime;
@dynamic userId;
@dynamic userToken;
@dynamic currentLat;
@dynamic currentLong;
@dynamic encodedPolyLine;
@dynamic isLocationTrackingCompleted;

+(CurrentRideTracking *)createCurrentRideTrackingObject:(RideDriveActionRequest *)request withDuration:(NSNumber *)duration{
    [CurrentRideTracking deleteCurrentRide];
    NSManagedObjectContext *context = [[DBInterface sharedInstance] sharedContext];
    CurrentRideTracking *currentRide = [NSEntityDescription insertNewObjectForEntityForName:@"CurrentRideTracking" inManagedObjectContext:context];
    UserProfile *userProfile = [UserProfile getCurrentUser];
    currentRide.userId = userProfile.userId;
    currentRide.userToken = userProfile.userToken;
    currentRide.status = [NSString stringWithFormat:@"RUNNING"];
    currentRide.startLat = request.userLat;
    currentRide.startLong = request.userLong;
    currentRide.currentLat = request.userLat;
    currentRide.currentLong = request.userLong;
    if(request.requestType == STARTDRIVE){
        currentRide.rideType = [NSString stringWithFormat:@"CURRENTDRIVE"];
        currentRide.driveId = request.driveId;
    } else{
        currentRide.rideType = [NSString stringWithFormat:@"CURRENTRIDE"];
        currentRide.rideId = request.rideId;
    }
    currentRide.rideDuration = duration;
    currentRide.isUpdatedSuccessful = [NSNumber numberWithInt:0];
    currentRide.isLocationTrackingCompleted = [NSNumber numberWithInt:0];
    NSDate *currentDate = [NSDate date];
    NSMutableArray *userpathArray = [[NSMutableArray alloc] init];
    [userpathArray addObject:request.userLat];
    [userpathArray addObject:request.userLong];
    currentRide.encodedPolyLine = @"";
    currentRide.pathArray = userpathArray;
    currentRide.createdTime = currentDate;
    currentRide.updatedTime = currentDate;
    [[DBInterface sharedInstance] saveSharedContext];
    return currentRide;
}

+(CurrentRideTracking *)getCurrentRide{
    NSManagedObjectContext *context = [[DBInterface sharedInstance] sharedContext];
    __block NSArray *fetchedObjects = nil;
    __block NSError *error = nil;
    [context performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity=[NSEntityDescription entityForName:@"CurrentRideTracking" inManagedObjectContext:context];
        fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    }];
    if(!error && fetchedObjects.count != 0)  return (CurrentRideTracking *)[fetchedObjects objectAtIndex:0];
    return nil;
}

+(void)updateCurrentTrackingObject:(CLLocation *)location withCompletion:(void(^)(CurrentRideTracking *))completion{
    NSManagedObjectContext *context = [[DBInterface sharedInstance] sharedContext];
    [context performBlock:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity=[NSEntityDescription entityForName:@"CurrentRideTracking" inManagedObjectContext:context];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        if(!error && fetchedObjects.count != 0){
            CurrentRideTracking *tracking = (CurrentRideTracking *)[fetchedObjects objectAtIndex:0];
            tracking.currentLat = [NSNumber numberWithDouble:location.coordinate.latitude];
            tracking.currentLong = [NSNumber numberWithDouble:location.coordinate.longitude];
            tracking.updatedTime = [NSDate date];
            NSMutableArray *pathArray = [[NSMutableArray alloc] initWithArray:tracking.pathArray];
            [pathArray addObject:tracking.currentLat];
            [pathArray addObject:tracking.currentLong];
            tracking.pathArray = pathArray;
            [[DBInterface sharedInstance] saveSharedContextInBackground];
            completion(tracking);
        } else {
           completion(nil);
        }
    }];
}

+(void)completeWithLat:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withCompletion:(void(^)(CurrentRideTracking *))completion{
    CurrentRideTracking *currentRide = [CurrentRideTracking getCurrentRide];
    currentRide.currentLat = latitude;
    currentRide.currentLong = longitude;
    currentRide.endLat = latitude;
    currentRide.endLong = longitude;
    currentRide.updatedTime = [NSDate date];
    NSMutableArray *pathArray = [[NSMutableArray alloc] initWithArray:currentRide.pathArray];
    [pathArray addObject:currentRide.currentLat];
    [pathArray addObject:currentRide.currentLong];
    currentRide.pathArray = pathArray;
    currentRide.encodedPolyLine = [CurrentRideTracking encodeStringWithCoordinates:currentRide.pathArray];
    currentRide.status = [NSString stringWithFormat:@"COMPLETED"];
    [[DBInterface sharedInstance] saveSharedContext];
    completion(currentRide);
}

+(void)getCurrentRideWithCompletion:(void(^)(CurrentRideTracking *))completion{
    NSManagedObjectContext *context = [[DBInterface sharedInstance] sharedContext];
    [context performBlock:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity=[NSEntityDescription entityForName:@"CurrentRideTracking" inManagedObjectContext:context];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        if(!error && fetchedObjects.count != 0){
            CurrentRideTracking *tracking = (CurrentRideTracking *)[fetchedObjects objectAtIndex:0];
            completion(tracking);
        } else{
           completion(nil);
        }
    }];
}

+(void)updateWithLocationTrackingStatus:(NSNumber *)status{
    [CurrentRideTracking getCurrentRideWithCompletion:^(CurrentRideTracking *tracking){
        tracking.isLocationTrackingCompleted = status;
        [[DBInterface sharedInstance] saveSharedContextInBackground];
    }];
}

+(void)deleteCurrentRide{
    CurrentRideTracking *currentRide = [CurrentRideTracking getCurrentRide];
    NSManagedObjectContext *context = [[DBInterface sharedInstance] sharedContext];
    if(currentRide){
        [context performBlockAndWait:^{
            [context deleteObject:currentRide];
            [[DBInterface sharedInstance] saveSharedContext];
        }];
    }
}

+ (NSString *)encodeStringWithCoordinates:(NSArray *)coordinates
{
    NSNumber *prevLatitude = [NSNumber numberWithDouble:0];
    NSNumber *prevLongitude = [NSNumber numberWithDouble:0];
    NSMutableString *encodedString = [NSMutableString string];
    int val = 0;
    int value = 0;
    
    for (int index = 0 ; index < coordinates.count ; index+=2) {
        // Encode latitude
        NSNumber *currentLatitude = [coordinates objectAtIndex:index];
        NSNumber *currentLongitude = [coordinates objectAtIndex:index + 1];
        val = round((currentLatitude.doubleValue - prevLatitude.doubleValue) * 1e5);
        val = (val < 0) ? ~(val<<1) : (val <<1);
        while (val >= 0x20) {
            int value = (0x20|(val & 31)) + 63;
            [encodedString appendFormat:@"%c", value];
            val >>= 5;
        }
        [encodedString appendFormat:@"%c", val + 63];
        
        // Encode longitude
        val = round((currentLongitude.doubleValue - prevLongitude.doubleValue) * 1e5);
        val = (val < 0) ? ~(val<<1) : (val <<1);
        while (val >= 0x20) {
            value = (0x20|(val & 31)) + 63;
            [encodedString appendFormat:@"%c", value];
            val >>= 5;
        }
        [encodedString appendFormat:@"%c", val + 63];
        
        prevLatitude = currentLatitude;
        prevLongitude = currentLongitude;
    }
    return encodedString;
}
@end
