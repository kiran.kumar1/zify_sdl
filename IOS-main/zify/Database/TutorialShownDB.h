//
//  LocalAddressSearch.h
//  commons
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface TutorialShownDB : NSManagedObject
@property (nonatomic, retain) NSString  *isInGuestHomeShown;
@property (nonatomic, retain) NSString  *isInCreateRideShown;
@property (nonatomic, retain) NSString  *isInPublishRideShown;
+(TutorialShownDB *)insertTutorialIntoDB:(NSString *)isInGuestHomeShown andisInCreateRideShown:(NSString *)isInCreateRideShown andisInPublishRideShown:(NSString *)isInPublishRideShown inContext:(NSManagedObjectContext *)context;

+(TutorialShownDB*)getTutorialsObjectForuserInContext:(NSManagedObjectContext *)context;
+(void)updateWithAppropriateValues:(TutorialShownDB *)obj;





@end
