//
//  CountryCode.h
//  commons
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface CountryCode : NSManagedObject
@property (nonatomic, retain) NSString *isdCode;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *isoCode;
@property (nonatomic, retain) NSString *currency;
@property (nonatomic, retain) NSString *distanceUnit;
@property (nonatomic, retain) NSString *distanceSubUnit;


+(CountryCode *)createCountryCodeWithISDCode:(NSString *)isdCode andName:(NSString *)name andISOCode:(NSString *)isoCode andCurrency:(NSString *)currency andDistanceUnit:(NSString *)distanceUnit andDistanceSubUnit:(NSString *)distanceSubUnit inContext:(NSManagedObjectContext *)context;
+(CountryCode*)getCountryObjectForISOCode:(NSString*)isoCode inContext:(NSManagedObjectContext *)context;
+(CountryCode*)getCountryObjectForISDCode:(NSString*)isdCode inContext:(NSManagedObjectContext *)context; //Passing context as a param since the same object is used for Data Genaration also.Importing DBInterface is not that much easy here.
@end
