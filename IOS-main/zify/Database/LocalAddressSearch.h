//
//  LocalAddressSearch.h
//  commons
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface LocalAddressSearch : NSManagedObject
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *state;
@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSString *latitude;
@property (nonatomic, retain) NSString *longitude;


+(LocalAddressSearch *)insertAddressSearchIntoDB:(NSString *)name andAddress:(NSString *)address andCity:(NSString *)city andState:(NSString *)state andCountry:(NSString *)country andLatitude:(NSString *)latitude andLongitude:(NSString *)longitude inContext:(NSManagedObjectContext *)context;
+(LocalAddressSearch *)createLocalAddressSearchWithName:(NSString *)name andAddress:(NSString *)address andCity:(NSString *)city andState:(NSString *)state andCountry:(NSString *)country andLatitude:(NSString *)latitude andLongitude:(NSString *)longitude inContext:(NSManagedObjectContext *)context;
+(NSUInteger)getUserSearchAddressCountInContext:(NSManagedObjectContext *)context;
+(void)deleteOldFirstRecordIfCountIsGreaterThanFIVEInContext:(NSManagedObjectContext *)context atIndex:(int)index;
+(NSArray*)getSearchDataArrayFrmDBInContext:(NSManagedObjectContext *)context;

@end
