//
//  VehicleModel.h
//  zify
//
//  Created by Anurag S Rathor on 02/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface VehicleModel : NSManagedObject
@property (nonatomic,retain) NSString *modelName;
+(VehicleModel *)createVehicleModel:(NSString *)modelName inContext:(NSManagedObjectContext *)context;
@end
