//
//  LocalAddressSearch.m
//  commons
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "LocalAddressSearch.h"
#import "DBUserDataInterface.h"

#define entityName @"LocalAddressSearch"

@implementation LocalAddressSearch
@dynamic name;
@dynamic address;
@dynamic city;
@dynamic state;
@dynamic country;
@dynamic latitude;
@dynamic longitude;
+(LocalAddressSearch *)createLocalAddressSearchWithName:(NSString *)name andAddress:(NSString *)address andCity:(NSString *)city andState:(NSString *)state andCountry:(NSString *)country andLatitude:(NSString *)latitude andLongitude:(NSString *)longitude inContext:(NSManagedObjectContext *)context{
    LocalAddressSearch *searchAddress = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    searchAddress.name = name;
    searchAddress.address = address;
    searchAddress.city = city;
    searchAddress.state = state;
    searchAddress.country = country;
    searchAddress.latitude = latitude;
    searchAddress.longitude = longitude;
    return searchAddress;
}
+(LocalAddressSearch *)insertAddressSearchIntoDB:(NSString *)name andAddress:(NSString *)address andCity:(NSString *)city andState:(NSString *)state andCountry:(NSString *)country andLatitude:(NSString *)latitude andLongitude:(NSString *)longitude inContext:(NSManagedObjectContext *)context{
    LocalAddressSearch *search = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    search.name = name;
    search.address = address;
    search.city = city;
    search.state = state;
    search.country = country;
    search.latitude = latitude;
    search.longitude = longitude;
    [[DBUserDataInterface sharedInstance] saveSharedContext];
    return search;
}
+(NSArray*)getSearchDataArrayFrmDBInContext:(NSManagedObjectContext *)context{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    //NSLog(@"fetchedObjects are %@", fetchedObjects);
    if(!error && fetchedObjects.count != 0){
        return fetchedObjects;
    }
    return nil;
    
}
+(NSUInteger)getUserSearchAddressCountInContext:(NSManagedObjectContext *)context;{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:entityName inManagedObjectContext: context]];
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:fetchRequest error: &error];
    return count;
}

+(void)deleteOldFirstRecordIfCountIsGreaterThanFIVEInContext:(NSManagedObjectContext *)context atIndex:(int)index{
    NSArray *fetchedObjects = nil;
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(!error && fetchedObjects.count != 0 && index < fetchedObjects.count){
        LocalAddressSearch *userSearch =  (LocalAddressSearch *)[fetchedObjects objectAtIndex:index];
        [context deleteObject:userSearch];
    }
    [[DBUserDataInterface sharedInstance] saveSharedContext];
}
/*+(CountryCode*)getCountryObjectForISDCode:(NSString*)isdCode inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"CountryCode" inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    [predicates addObject:[NSPredicate predicateWithFormat:@"isdCode = %@", isdCode]];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }
}*/
@end
