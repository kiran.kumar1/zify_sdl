//
//  CountryCode.m
//  commons
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "CountryCode.h"

@implementation CountryCode
@dynamic name;
@dynamic isdCode;
@dynamic isoCode;
@dynamic currency;
@dynamic distanceUnit;
@dynamic distanceSubUnit;

+(CountryCode *)createCountryCodeWithISDCode:(NSString *)isdCode andName:(NSString *)name andISOCode:(NSString *)isoCode andCurrency:(NSString *)currency andDistanceUnit:(NSString *)distanceUnit andDistanceSubUnit:(NSString *)distanceSubUnit inContext:(NSManagedObjectContext *)context{
    CountryCode *countryCode = [NSEntityDescription insertNewObjectForEntityForName:@"CountryCode" inManagedObjectContext:context];
    countryCode.isdCode = isdCode;
    countryCode.name = name;
    countryCode.isoCode = isoCode;
    countryCode.currency = currency;
    countryCode.distanceUnit = distanceUnit;
    countryCode.distanceSubUnit = distanceSubUnit;
    return countryCode;
}

+(CountryCode*)getCountryObjectForISOCode:(NSString*)isoCode inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"CountryCode" inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    [predicates addObject:[NSPredicate predicateWithFormat:@"isoCode = %@", isoCode]];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }
    
}

+(CountryCode*)getCountryObjectForISDCode:(NSString*)isdCode inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"CountryCode" inManagedObjectContext:context];
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    [predicates addObject:[NSPredicate predicateWithFormat:@"isdCode = %@", isdCode]];
    request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    request.fetchLimit = 1;
    NSError *error;
    NSArray *arr = [context executeFetchRequest:request error:&error];
    if (arr.count > 0) {
        return arr[0];
    }
    else{
        return nil;
    }
}
@end
