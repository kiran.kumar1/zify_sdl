//
//  UITableView+iOS.h
//  retailreward
//
//  Created by Premkumar on 30/04/14.
//  Copyright (c) 2014 Sreekumar R S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (iOS)
@property (nonatomic) CGFloat frameX;       // frame.origin.x
@property (nonatomic) CGFloat frameY;       // frame.origin.y
@property (nonatomic) CGFloat frameWidth;   // frame.size.width
@property (nonatomic) CGFloat frameHeight;  // frame.size.height

@property (nonatomic) CGSize frameSize;     // frame.size
@property (nonatomic) CGPoint frameOrigin;  // frame.origin

@property (nonatomic) CGFloat centerX;      // center.x
@property (nonatomic) CGFloat centerY;      // center.y

@property (nonatomic, readonly) CGFloat frameMaxX;  // CGRectGetMaxX(frame)
@property (nonatomic, readonly) CGFloat frameMaxY;  // CGRectGetMaxY(frame)

@property (nonatomic) CGFloat boundsX;       // bounds.origin.x
@property (nonatomic) CGFloat boundsY;       // bounds.origin.y
@property (nonatomic) CGFloat boundsWidth;   // bounds.size.width
@property (nonatomic) CGFloat boundsHeight;  // bounds.size.height
@end
