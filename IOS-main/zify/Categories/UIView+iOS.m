//
//  UIView+ADC.m
//  ADCExtensions
//
//  Created by Agustín de Cabrera on 19/02/2013.
//  Copyright (c) 2013 Agustín de Cabrera. All rights reserved.
//

#import "UIView+iOS.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (iOS)

-(CGFloat)frameX        { return self.frame.origin.x; }
-(CGFloat)frameY        { return self.frame.origin.y; }
-(CGFloat)frameWidth    { return self.frame.size.width; }
-(CGFloat)frameHeight   { return self.frame.size.height; }

-(CGFloat)boundsX        { return self.bounds.origin.x; }
-(CGFloat)boundsY        { return self.bounds.origin.y; }
-(CGFloat)boundsWidth    { return self.bounds.size.width; }
-(CGFloat)boundsHeight   { return self.bounds.size.height; }

-(CGSize)frameSize      { return self.frame.size; }
-(CGPoint)frameOrigin   { return self.frame.origin; }

-(CGFloat)centerX   { return self.center.x; }
-(CGFloat)centerY   { return self.center.y; }

-(CGFloat)frameMaxX     { return CGRectGetMaxX(self.frame); }
-(CGFloat)frameMaxY     { return CGRectGetMaxY(self.frame); }

-(void)setFrameX:(CGFloat)x
{
    CGRect rect = self.frame;
    rect.origin.x = x;
    self.frame = rect;
}
-(void)setFrameY:(CGFloat)y
{
    CGRect rect = self.frame;
    rect.origin.y = y;
    self.frame = rect;
}
-(void)setFrameWidth:(CGFloat)width
{
    CGRect rect = self.frame;
    rect.size.width = width;
    self.frame = rect;
}
-(void)setFrameHeight:(CGFloat)height
{
    CGRect rect = self.frame;
    rect.size.height = height;
    self.frame = rect;
}

-(void)setBoundsX:(CGFloat)x
{
    CGRect rect = self.bounds;
    rect.origin.x = x;
    self.bounds = rect;
}
-(void)setBoundsY:(CGFloat)y
{
    CGRect rect = self.bounds;
    rect.origin.y = y;
    self.bounds = rect;
}
-(void)setBoundsWidth:(CGFloat)width
{
    CGRect rect = self.bounds;
    rect.size.width = width;
    self.bounds = rect;
}
-(void)setBoundsHeight:(CGFloat)height
{
    CGRect rect = self.bounds;
    rect.size.height = height;
    self.bounds = rect;
}

-(void)setFrameSize:(CGSize)size
{
    CGRect rect = self.frame;
    rect.size = size;
    self.frame = rect;
}
-(void)setFrameOrigin:(CGPoint)origin
{
    CGRect rect = self.frame;
    rect.origin = origin;
    self.frame = rect;
}



-(void)setCenterX:(CGFloat)centerX
{
    self.center = CGPointMake(centerX, self.center.y);
}
-(void)setCenterY:(CGFloat)centerY
{
    self.center = CGPointMake(self.center.x, centerY);
}



- (void)addDropShadow
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.layer.shadowOpacity = 0.5f;
    self.layer.shadowPath = shadowPath.CGPath;
}

- (void)addFullShadow
{
    CALayer *layer = self.layer;
    layer.shadowOffset = CGSizeZero;
    layer.shadowColor = [[UIColor blackColor] CGColor];
    layer.shadowRadius = 6.0f;
    layer.shadowOpacity = 0.95f;
    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];;
}

- (void)addFancyShadow
{
    CALayer *layer = self.layer;
    //changed to zero for the new fancy shadow
    layer.shadowOffset = CGSizeZero;
    layer.shadowColor = [[UIColor blackColor] CGColor];
    //changed for the fancy shadow
    layer.shadowRadius = 1.0f;
    layer.shadowOpacity = 0.50f;
    
    CGSize size = self.frame.size;
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    //right
    [path moveToPoint:CGPointZero];
    [path addLineToPoint:CGPointMake(size.width, 0.0f)];
    [path addLineToPoint:CGPointMake(size.width, size.height + 3.0f)];
    
    //curved bottom
    [path addCurveToPoint:CGPointMake(0.0, size.height + 3.0f)
            controlPoint1:CGPointMake(size.width - 3.0f, size.height)
            controlPoint2:CGPointMake(3.0f, size.height)];
    
    [path closePath];
    //call our new fancy shadow method
    layer.shadowPath = path.CGPath;
}

- (void)addFancyShadowWithColor:(UIColor *)shadowColor
{
    CALayer *layer = self.layer;
    //changed to zero for the new fancy shadow
    layer.shadowOffset = CGSizeZero;
    layer.shadowColor = [shadowColor CGColor];
    //changed for the fancy shadow
    layer.shadowRadius = 1.0f;
    layer.shadowOpacity = 0.50f;
    
    CGSize size = self.frame.size;
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    //right
    [path moveToPoint:CGPointZero];
    [path addLineToPoint:CGPointMake(size.width, 0.0f)];
    [path addLineToPoint:CGPointMake(size.width, size.height + 3.0f)];
    
    //curved bottom
    [path addCurveToPoint:CGPointMake(0.0, size.height + 3.0f)
            controlPoint1:CGPointMake(size.width - 3.0f, size.height)
            controlPoint2:CGPointMake(3.0f, size.height)];
    
    [path closePath];
    //call our new fancy shadow method
    layer.shadowPath = path.CGPath;
}

- (void)setShadow:(UIColor *)shadowColor shadowRadius:(CGFloat )shadowRadius cornerRadius:(CGFloat )cornerRadius
{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = cornerRadius;
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(1, 1);
    self.layer.shadowColor = [shadowColor CGColor];
    self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.layer.shadowOpacity = 0.5f;
    self.layer.shadowRadius = shadowRadius;
    self.layer.shadowPath = shadowPath.CGPath;
    self.layer.shouldRasterize = true;
    
}

- (void)showShadow:(UIView *)viewShadow shadowColor:(UIColor *)shadowColor offSet:(CGSize)sizemake shadowRadius:(CGFloat)shadowRadius
{
    viewShadow.layer.masksToBounds = FALSE;
    viewShadow.layer.shadowColor = shadowColor.CGColor;
    viewShadow.layer.shadowOpacity = 0.5;
    viewShadow.layer.shadowOffset = sizemake;
    viewShadow.layer.shadowRadius = shadowRadius;
    
}

/*
 func showShadow(view: UIView, color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 5, scale: Bool = true) {
 view.layer.masksToBounds = false
 view.layer.shadowColor = color.cgColor
 view.layer.shadowOpacity = opacity
 view.layer.shadowOffset = offSet
 view.layer.shadowRadius = radius
 
 //        view.layer.shadowPath = UIBezierPath(rect: self.view.bounds).cgPath
 //  view.layer.shouldRasterize = true
 // view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
 
 }
 
 */

@end
