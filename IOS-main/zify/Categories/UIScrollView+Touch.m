//
//  UIScrollView+Touch.m
//  NARS
//
//  Created by Kiran on 2/3/17.
//  Copyright © 2017 Kiran. All rights reserved.
//

#import "UIScrollView+Touch.h"

@implementation UIScrollView (Touch)
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!self.dragging) {
        [self.nextResponder touchesBegan:touches withEvent:event];
    }
    
    [super touchesEnded: touches withEvent: event];
}


-(void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event
{
    if (!self.dragging) {
        [self.nextResponder touchesEnded: touches withEvent:event];
    }
    
    [super touchesEnded: touches withEvent: event];
}
@end
