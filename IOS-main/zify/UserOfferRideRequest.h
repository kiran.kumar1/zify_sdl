//
//  UserOfferRideRequest.h
//  zify
//
//  Created by Anurag Rathor on 20/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "ServerRequest.h"
#import "LocalityInfo.h"


@interface UserOfferRideRequest : ServerRequest
@property(nonatomic,strong) LocalityInfo *sourceLocality;
@property(nonatomic,strong) LocalityInfo *destinationLocality;
@property(nonatomic,strong) NSString *rideDate;
@property(nonatomic,strong) NSNumber *seats;
@property(nonatomic,strong) NSString *lastDeptTime;
@property(nonatomic,strong) NSString *merchantId;
-(id)initWithSourceLocality:(LocalityInfo *)sourceLocality andDestinationLocality:(LocalityInfo *)destinationLocality andRideDate:(NSString *)rideDate andSeats:(NSNumber *)seats andMerchantId:(NSString *)merchantId;

@end
