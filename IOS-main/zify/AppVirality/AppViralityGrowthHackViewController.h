//
//  AppViralityGrowthHackViewController.h
//  testAV
//
//  Created by Ram on 07/05/15.
//  Copyright (c) 2015 AppVirality. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppVirality.h"
#import "AppViralityUIUtility.h"
#import "MessageHandler.h"

@interface AppViralityGrowthHackViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) UILabel * greetingLabel,*messageLabel,*shareLabel,*shareURLLabel,*termsLabel;
@property (nonatomic,strong) UIButton* facebookButton,*messageButton,*whatsappButton,*mailButton;
@property (nonatomic,strong) UIImageView *urlCopyImage;
-(id)initWithCampaignDetails:(NSDictionary *)campaignDetails ForGrowthHack:(GrowthHackType)growthHack;
@end

