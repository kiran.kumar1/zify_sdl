//
//  AppViralityGrowthHackViewController.m
//  testAV
//
//  Created by Ram on 07/05/15.
//  Copyright (c) 2015 AppVirality. All rights reserved.
//

#import "AppViralityGrowthHackViewController.h"
#import  <MessageUI/MessageUI.h>
#import  <MessageUI/MFMailComposeViewController.h>
#import "FacebookLogin.h"
#import "AppUtilites.h"
#import "MessageHandler.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "LocalisationConstants.h"

@interface AppViralityGrowthHackViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FaceBookLoginDelgate>
@property (strong) NSMutableArray *selectedSections;
@property (strong) NSArray *referredusers;
@property (strong) NSDictionary *userPoints,*campaginDetails;
@property (strong) NSMutableDictionary *shareMesgs;
@property (strong) NSMutableDictionary *shareTitles;
@property (strong) NSString *terms;
@property (strong) UIWebView *termsView;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) MessageHandler *messageHandler;
@property NSInteger termsHeight;
@property GrowthHackType growthHack;
@property BOOL isUserPointsUpdated;
@end

@implementation AppViralityGrowthHackViewController


-(id)initWithCampaignDetails:(NSDictionary *)campaignDetails ForGrowthHack:(GrowthHackType)growthHack
{
    self.growthHack = growthHack;
    self.campaginDetails = campaignDetails;
    
    if (self) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        [self.view addSubview:self.tableView];

        self.referredusers = [NSArray array];
        self.shareMesgs = [NSMutableDictionary dictionary];
        self.shareTitles = [NSMutableDictionary dictionary];
        self.termsHeight=0;
        self.selectedSections = [NSMutableArray array];
     
        UIView * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 260)];
        
        self.greetingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 120)];
        self.greetingLabel.textAlignment = NSTextAlignmentCenter;
        self.greetingLabel.numberOfLines =0;
        self.greetingLabel.font = [UIFont boldSystemFontOfSize:18];
        [headerView addSubview:self.greetingLabel];
        
        self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.greetingLabel.frame), SCREEN_WIDTH-20, 120)];
        self.messageLabel.textAlignment = NSTextAlignmentCenter;
        self.messageLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
        self.messageLabel.numberOfLines=0;
        [headerView addSubview:self.messageLabel];
        
        
        self.shareLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.messageLabel.frame)+10, SCREEN_WIDTH, 120)];
        self.shareLabel.textAlignment = NSTextAlignmentCenter;
        self.shareLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
        self.shareLabel.numberOfLines=0;
        [headerView addSubview:self.shareLabel];
        
        self.shareURLLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.messageLabel.frame)+10, SCREEN_WIDTH - 20 , 120)];
        self.shareURLLabel.textAlignment = NSTextAlignmentCenter;
        self.shareURLLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
        self.shareURLLabel.numberOfLines=0;
        self.shareURLLabel.layer.cornerRadius = 5.0;
        self.shareURLLabel.layer.borderWidth = 1.0;
        self.shareURLLabel.layer.borderColor = [[UIColor whiteColor] CGColor];
        [headerView addSubview:self.shareURLLabel];
        
        self.urlCopyImage = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"copy_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        self.urlCopyImage.frame = CGRectMake(CGRectGetMaxX(self.shareURLLabel.frame) - 21, CGRectGetMinY(self.shareURLLabel.frame) + 7, 16, 16);
        [headerView addSubview:self.urlCopyImage];

        
        CGFloat space;
        NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=hello"]];
        if (![[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
            space= (SCREEN_WIDTH-135)/4;
        }else {
            space= (SCREEN_WIDTH-180)/5;
        }
        self.facebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.facebookButton.frame = CGRectMake(space, SCREEN_HEIGHT-250, 45, 45);
        [self.facebookButton setImage:[UIImage imageNamed:@"av_fb_icon.png"] forState:UIControlStateNormal];
        [self.facebookButton addTarget:self action:@selector(faceBookButtonClicked:) forControlEvents:UIControlEventTouchDown];
        [headerView addSubview:self.facebookButton];
     
       
        self.mailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.mailButton.frame = CGRectMake(CGRectGetMaxX(self.facebookButton.frame)+space, SCREEN_HEIGHT-250, 45, 45);
        [self.mailButton setImage:[UIImage imageNamed:@"av_mail_icon.png"] forState:UIControlStateNormal];
        [self.mailButton addTarget:self action:@selector(mailButtonClicked:) forControlEvents:UIControlEventTouchDown];
        [headerView addSubview:self.mailButton];
        
        
        self.messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.messageButton.frame = CGRectMake(CGRectGetMaxX(self.mailButton.frame)+space, SCREEN_HEIGHT-250, 45, 45);
         [self.messageButton setImage:[UIImage imageNamed:@"av_message_icon.png"] forState:UIControlStateNormal];
        [self.messageButton addTarget:self action:@selector(messageButtonClicked:) forControlEvents:UIControlEventTouchDown];
         [headerView addSubview:self.messageButton];
        
        self.whatsappButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.whatsappButton.frame = CGRectMake(CGRectGetMaxX(self.messageButton.frame)+space, SCREEN_HEIGHT-250, 45, 45);
        [self.whatsappButton setImage:[UIImage imageNamed:@"av_whatsapp_icon.png"] forState:UIControlStateNormal];
        self.whatsappButton.layer.borderColor = [UIColor blackColor].CGColor;
        [self.whatsappButton addTarget:self action:@selector(whatsappButtonClicked:) forControlEvents:UIControlEventTouchDown];
        
        if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
            [headerView addSubview:self.whatsappButton];
        }
        
        self.tableView.tableHeaderView = headerView;

        if ([campaignDetails objectForKey:@"OfferTitle"]) {
            NSString * titleText= [campaignDetails valueForKey:@"OfferTitle"];
            titleText = [titleText stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            titleText = [titleText stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            unsigned color = [AppViralityUIUtility checkAndGetColorAtKey:@"OfferTitleColor" InDictionary:campaignDetails];
            self.greetingLabel.textColor =UIColorFromRGB(color);
            self.greetingLabel.text = titleText;
            [AppViralityUIUtility resetLabelHeight:self.greetingLabel];
        }
        
        if ([campaignDetails objectForKey:@"OfferDescription"]) {
            NSString * descriptionText= [campaignDetails valueForKey:@"OfferDescription"];
            descriptionText = [descriptionText stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            descriptionText = [descriptionText stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            unsigned color = [AppViralityUIUtility checkAndGetColorAtKey:@"OfferDescriptionColor" InDictionary:campaignDetails];
            self.messageLabel.textColor =UIColorFromRGB(color);
            self.messageLabel.text = descriptionText;
            [AppViralityUIUtility resetLabelHeight:self.messageLabel];
        }
        
        unsigned color = [AppViralityUIUtility checkAndGetColorAtKey:@"CampaignBGColor" InDictionary:campaignDetails];
        headerView.backgroundColor =UIColorFromRGB(color);
        self.view.backgroundColor = UIColorFromRGB(color);
        self.tableView.backgroundColor = UIColorFromRGB(color);
        
        if ([campaignDetails objectForKey:@"socialactions"]) {
            NSArray * socialActions = [campaignDetails objectForKey:@"socialactions"];
            for (NSDictionary * socialAction in socialActions) {
                NSString * message = [socialAction valueForKey:@"shareMessage"];
                NSString *title = [socialAction valueForKey:@"shareTitle"];
                message = [message stringByReplacingOccurrencesOfString:@"SHARE_URL" withString:[socialAction valueForKey:@"shareUrl"]];
                [self.shareMesgs  setValue:message forKey:[socialAction valueForKey:@"socialActionName"]];
                [self.shareTitles  setValue:title forKey:[socialAction valueForKey:@"socialActionName"]];
            }
            
            if (socialActions.count!=0) {
                NSString *shareUrl =[[socialActions valueForKey:@"shareUrl"] firstObject];
                NSURL *myURL = [NSURL URLWithString:shareUrl];
                myURL = [myURL URLByDeletingLastPathComponent];
                shareUrl = [myURL absoluteString];
                self.shareLabel.text = NSLocalizedString(VC_AVGROWTHHACK_INVITELINK, nil);
                self.shareURLLabel.text = shareUrl;
                self.shareURLLabel.text = [self.shareURLLabel.text substringToIndex:self.shareURLLabel.text.length-1];
            }
            [AppViralityUIUtility resetLabelHeight:self.shareLabel];
            [AppViralityUIUtility resetLabelHeight:self.shareURLLabel];
            self.shareLabel.textColor = self.greetingLabel.textColor;
            self.shareURLLabel.textColor = self.greetingLabel.textColor;
            UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareLabelSelected:)];
            tapGesture.numberOfTapsRequired =1;
            self.shareURLLabel.userInteractionEnabled = YES;
            [self.shareURLLabel addGestureRecognizer:tapGesture];
           // headerView.tintColor = self.greetingLabel.textColor;
        }
        
        if ([campaignDetails objectForKey:@"CampaignBGImage"]) {
            [AppViralityUIUtility downloadImageWithURL:[NSURL  URLWithString:[campaignDetails objectForKey:@"CampaignBGImage"]] completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded&&image) {
                    UIImageView * imageView = [[UIImageView alloc] initWithImage:image];
                    self.tableView.backgroundView = imageView;
                    headerView.backgroundColor = [UIColor clearColor];
                }
            }];
        }
        
        [AppVirality recordImpressionsForGrowthHack:self.growthHack WithParams:@{@"click":@"true",@"impression":@"false"} completion:^(NSDictionary *response, NSError* error) {
            
        }];
        [self.view setNeedsDisplay];
        self.tableView.separatorColor = [UIColor grayColor];
    }
    return self;
}

-(void)shareLabelSelected:(id)sender
{
    [self recordSocialActionForActionType:@"CustomLink"];
    NSString *copyStringverse = [[self.shareURLLabel.text componentsSeparatedByString:@"\n"] lastObject];
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:copyStringverse];
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4, CGRectGetMaxY(self.shareLabel.frame), SCREEN_WIDTH/2, 30)];
    label.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    label.text = NSLocalizedString(VC_AVGROWTHHACK_COPIED, nil);
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor whiteColor];
    label.alpha =0;
    label.layer.cornerRadius = 5;
    label.layer.masksToBounds = YES;
    
    [UIView animateWithDuration:1 animations:^{
        [self.tableView addSubview:label];
        label.alpha =1;
    } completion:^(BOOL finished) {
        [label removeFromSuperview];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelButtonClicked:)];
    _isUserPointsUpdated = false;
 }

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ;
    if(!self.messageHandler){
        self.messageHandler = [[MessageHandler alloc] init];
        self.messageHandler.container = self.tableView;
        [self.messageHandler awakeFromNib];
    }
}

-(void)cancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)recordSocialActionForActionType:(NSString*)socialActionName
{
    if ([self.campaginDetails objectForKey:@"socialactions"]) {
        for (NSDictionary * socialAction in [self.campaginDetails valueForKey:@"socialactions"]) {
            if ([[socialAction valueForKey:@"socialActionName"] isEqualToString:socialActionName]) {
           
                [AppVirality recordSocialActionForGrowthHack:self.growthHack WithParams:@{@"shareMessage":[socialAction valueForKey:@"shareMessage"],@"shortcode":[[[socialAction valueForKey:@"shareUrl"] stringByDeletingLastPathComponent] lastPathComponent],@"socialActionId":[socialAction valueForKey:@"socialActionId"]} completion:^(BOOL success,NSError*error) {
                    
                }];
            }
        }
    }
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [AppVirality registerAsDebugDevice:^(BOOL success,NSError*error) {
            NSLog(@" debug mode %d",success);
        }];
        
    }
}

-(void)faceBookButtonClicked:(id)sender
{
    [self recordSocialActionForActionType:@"Facebook"];
    FacebookLogin *facebookShare = [FacebookLogin sharedInstance];
    facebookShare.faceBookLoginDelgate = self;
    [facebookShare shareWithFacebook];
}

-(void)mailButtonClicked:(id)sender
{
    [self recordSocialActionForActionType:@"Mail"];
    NSDictionary *dictionary = @{
                                 @"subject":[self.shareTitles objectForKey:@"Mail"],
                                 @"messageBody":[self.shareMesgs objectForKey:@"Mail"]?[self.shareMesgs valueForKey:@"Mail"]:@"",
                                 @"recipients":@"",
                                 };
    [AppUtilites shareEmailWithData:dictionary withDelegate:self];
}

-(void)messageButtonClicked:(id)sender
{
    [self recordSocialActionForActionType:@"Messaging"];
    [AppUtilites shareSMSWithDelegate:self withMessage:[self.shareMesgs objectForKey:@"Messaging"]?[self.shareMesgs valueForKey:@"Messaging"]:@""];
}

-(void)whatsappButtonClicked:(id)sender
{
    //NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=%@",[self.shareMesgs objectForKey:@"WhatsApp"]?[[self.shareMesgs valueForKey:@"WhatsApp"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]:@""]];
    NSString *encodedString = @"";
    if([self.shareMesgs objectForKey:@"WhatsApp"]){
        encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)[self.shareMesgs objectForKey:@"WhatsApp"],NULL,
        (CFStringRef)@"!*'();:@&=+$,/?%#[]~.",kCFStringEncodingUTF8 ));;
    }
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=%@",encodedString]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [self recordSocialActionForActionType:@"WhatsApp"];
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)postFeedToFacebook{
    NSString *descriptionStr = [self.shareMesgs objectForKey:@"Facebook"]?[self.shareMesgs valueForKey:@"Facebook"]:@"text";
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithDictionary:@{
                                                        @"name" : @"Zify",
                                                        @"caption":@"",
                                                        @"description":descriptionStr,
                                                        @"link":@"http://zify.co",
                                                        @"picture":@"https://m.zify.co/content/zifyHome/logo.png"}];
    [[FacebookLogin sharedInstance] postFeedToFacebookWithParams:params];

}

-(void)unableToLoginFBWithError:(NSError *)error{
    [self.messageHandler showErrorMessage:@"Unable to authenticate using facebook."];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -Table View Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![self.selectedSections containsObject:[NSNumber numberWithInteger:section]]) {
        return 0;
    }
    if (section==2) {
        return 1;
    }
    if (section==1) {
        return self.referredusers.count;
    }
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==2&&indexPath.row==0) {
        if(self.termsHeight == 0){
             self.termsHeight = [AppViralityUIUtility getHeightForText:self.terms withFont: [UIFont systemFontOfSize:14] andWidth:SCREEN_WIDTH - 20];
        }
        return self.termsHeight;
    }
    return 45;
}

-(void)viewWillLayoutSubviews
{
    self.greetingLabel.frame = CGRectMake(CGRectGetMinX(self.greetingLabel.frame),CGRectGetMinY(self.greetingLabel.frame), CGRectGetWidth(self.greetingLabel.frame), CGRectGetHeight(self.greetingLabel.frame));
    
    self.messageLabel.frame = CGRectMake(CGRectGetMinX(self.messageLabel.frame),CGRectGetMaxY(self.greetingLabel.frame)+20, CGRectGetWidth(self.messageLabel.frame), CGRectGetHeight(self.messageLabel.frame));
    
    self.shareLabel.frame = CGRectMake(CGRectGetMinX(self.shareLabel.frame),CGRectGetMaxY(self.messageLabel.frame)+10, CGRectGetWidth(self.shareLabel.frame), CGRectGetHeight(self.shareLabel.frame));

    self.shareURLLabel.frame = CGRectMake(CGRectGetMinX(self.shareURLLabel.frame),CGRectGetMaxY(self.shareLabel.frame), CGRectGetWidth(self.shareURLLabel.frame), CGRectGetHeight(self.shareURLLabel.frame));

    self.urlCopyImage.frame = CGRectMake(CGRectGetMaxX(self.shareURLLabel.frame) - 21, CGRectGetMinY(self.shareURLLabel.frame) + 7, 16, 16);
    
    self.facebookButton.frame = CGRectMake(CGRectGetMinX(self.facebookButton.frame),CGRectGetMaxY(self.shareURLLabel.frame)+10, CGRectGetWidth(self.facebookButton.frame), CGRectGetHeight(self.facebookButton.frame));
    self.messageButton.frame = CGRectMake(CGRectGetMinX(self.messageButton.frame),CGRectGetMaxY(self.shareURLLabel.frame)+10, CGRectGetWidth(self.messageButton.frame), CGRectGetHeight(self.messageButton.frame));
    self.whatsappButton.frame = CGRectMake(CGRectGetMinX(self.whatsappButton.frame),CGRectGetMaxY(self.shareURLLabel.frame)+10, CGRectGetWidth(self.whatsappButton.frame), CGRectGetHeight(self.whatsappButton.frame));
    self.mailButton.frame = CGRectMake(CGRectGetMinX(self.mailButton.frame),CGRectGetMaxY(self.shareURLLabel.frame)+10, CGRectGetWidth(self.mailButton.frame), CGRectGetHeight(self.mailButton.frame));

}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSArray * headerArray = @[NSLocalizedString(VC_AVGROWTHHACK_EARNINGS, nil),NSLocalizedString(VC_AVGROWTHHACK_SUCCESSFULREFERRALS, nil), NSLocalizedString(VC_AVGROWTHHACK_HOWITWORKS, nil)];
    UIView * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 45)];
    headerView.backgroundColor = [UIColor clearColor];
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(21, 0, SCREEN_WIDTH-42, 45)];
    headerLabel.text = [headerArray objectAtIndex:section];
    headerLabel.textColor = self.greetingLabel.textColor;
    headerLabel.font  = [UIFont boldSystemFontOfSize:18.0];
    [headerView addSubview:headerLabel];
    UIImageView * imageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"av_arrow.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    imageView.tintColor = self.greetingLabel.textColor;
    imageView.transform = CGAffineTransformMakeRotation([self.selectedSections containsObject:[NSNumber numberWithInteger:section]]?-M_PI_2:M_PI_2);
    imageView.frame = CGRectOffset(imageView.frame, SCREEN_WIDTH-60, 15);
    [headerView addSubview:imageView];
    UIButton *button = [[UIButton alloc] initWithFrame: CGRectMake(0.0, 0.0, SCREEN_WIDTH, 45)];
    button.alpha = 0.7;
    button.tag = section;
    /* Prepare target-action */
    [button addTarget: self action: @selector(headerSelected:)
     forControlEvents: UIControlEventTouchUpInside];
    UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(5, CGRectGetHeight(headerView.frame)-1, SCREEN_WIDTH-10, 1)];
    lineView.backgroundColor = [UIColor grayColor];
    [headerView addSubview:lineView];
    [headerView addSubview: button];

    return headerView;
}

-(void)headerSelected:(UIButton*)sender
{
    int tag = (int)[sender tag];
    if ([self.selectedSections containsObject:[NSNumber numberWithInteger:tag]]) {
        [self.selectedSections removeObject:[NSNumber numberWithInteger:tag]];
    }else
        [self.selectedSections addObject:[NSNumber numberWithInteger:tag]];
    if (tag<2 && !_isUserPointsUpdated) {
            [self.messageHandler showBlockingLoadViewWithHandler:^{
                [AppVirality getUserBalance:self.growthHack completion:^(NSDictionary *userInfo,NSError*error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                            if(error){
                                [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                            }else{
                                self.userPoints = [(NSArray*)[userInfo valueForKey:@"userpoints"] firstObject];
                                self.referredusers = [userInfo valueForKey:@"referredusers"];
                                _isUserPointsUpdated = true;
                                if(tag ==1 && self.referredusers.count == 0){
                                    [self.messageHandler showErrorMessage:NSLocalizedString(VC_AVGROWTHHACK_NOREFERREDUSERSVAL, nil)];
                                    [self.selectedSections removeObject:[NSNumber numberWithInteger:tag]];
                                } else{
                                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:tag] withRowAnimation:UITableViewRowAnimationAutomatic];
                                    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:tag] atScrollPosition:UITableViewScrollPositionTop
                                                                  animated:YES];
                                }
                            }
                        }];
                    });
                }];
            }];
    }else if (tag==2 && !self.terms){
         [self.messageHandler showBlockingLoadViewWithHandler:^{
             [AppVirality getTerms:self.growthHack completion:^(NSDictionary *userTerms,NSError*error) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.messageHandler dismissBlockingLoadViewWithHandler:^{
                         if(error){
                             [self.messageHandler showErrorMessage:error.userInfo[@"message"]];
                         }else{
                             self.terms = [userTerms valueForKey:@"message"];
                             [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:tag] withRowAnimation:UITableViewRowAnimationAutomatic];
                             [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:tag] atScrollPosition:UITableViewScrollPositionTop
                                                           animated:YES];
                         }
                     }];
                 });
             }];
         }];
    } else{
        if(tag ==1 && self.referredusers.count == 0){
            [self.messageHandler showErrorMessage:NSLocalizedString(VC_AVGROWTHHACK_NOREFERREDUSERSVAL, nil)];
             [self.selectedSections removeObject:[NSNumber numberWithInteger:tag]];
        } else{
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:tag] withRowAnimation:UITableViewRowAnimationAutomatic];
            if ([self.selectedSections containsObject:[NSNumber numberWithInteger:tag]]){
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:tag]
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
            }
        }
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * reuseIdentifier = [@"reuseIdentifier" stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    //NSLog(@"%ld %ld %@",(long)indexPath.section,(long)indexPath.row,reuseIdentifier);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    

    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    }
    cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0.1];
    if (indexPath.section==0) {
        NSArray * titleArray = @[NSLocalizedString(VC_AVGROWTHHACK_TOTAL, nil),NSLocalizedString(VC_AVGROWTHHACK_CLAIMED, nil),NSLocalizedString(VC_AVGROWTHHACK_PENDING, nil)];
        cell.textLabel.text = [titleArray objectAtIndex:indexPath.row];
        cell.textLabel.textColor = self.greetingLabel.textColor;
        
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
        label.text =@"0";
        if (self.userPoints&&[self.userPoints objectForKey:[titleArray objectAtIndex:indexPath.row]]) {
            label.text = [self.userPoints valueForKey:[titleArray objectAtIndex:indexPath.row]];
        }
        label.textColor = self.greetingLabel.textColor;

        cell.accessoryView = label;
    }
    
    if (indexPath.section==2) {
        UILabel *tempLabel = self.termsLabel;
        self.termsLabel= [[UILabel alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20,50)];
        self.termsLabel.font = [UIFont systemFontOfSize:14];
        self.termsLabel.numberOfLines=0;
        self.termsLabel.text =self.terms;
        self.termsLabel.textColor = self.greetingLabel.textColor;
        [AppViralityUIUtility resetLabelHeight: self.termsLabel];
        [cell.contentView addSubview: self.termsLabel];
        double delayInSeconds=1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if(tempLabel)[tempLabel removeFromSuperview];
        });
    }
    
    if (indexPath.section==1) {
        cell.textLabel.text = @"no name";
        if ([[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"emailid"]&&![[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"emailid"] isEqual:[NSNull null]]) {
            cell.textLabel.text =[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"emailid"];
        }
        if ([[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"name"]&&![[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"name"] isEqual:[NSNull null]]) {
            cell.textLabel.text =[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"name"];
        }
        if ([[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"regdate"]&&![[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"regdate"] isEqual:[NSNull null]]) {
            cell.textLabel.text = [cell.textLabel.text stringByAppendingFormat:@"\n%@",[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"regdate"]];
            cell.textLabel.numberOfLines = 0;
        }
        if ([[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"profileimage"]&&![[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"profileimage"] isEqual:[NSNull null]]) {
            UIImage *profileImagePlaceHolder = [UIImage imageNamed:@"placeholder_profile_image"];
            cell.imageView.hidden = YES;
            [cell.imageView sd_setImageWithURL:[NSURL  URLWithString:[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"profileimage"]] placeholderImage:profileImagePlaceHolder completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
                cell.imageView.hidden = NO;
                cell.imageView.layer.cornerRadius = CGRectGetWidth(cell.imageView.frame)/4;
                cell.imageView.layer.masksToBounds = YES;
            }];

        }
        
        cell.textLabel.textColor = self.greetingLabel.textColor;

    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
