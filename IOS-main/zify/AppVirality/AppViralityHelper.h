//
//  AppViralityHelper.h
//  zify
//
//  Created by Anurag S Rathor on 04/11/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppViralityHelper : NSObject
+(void) initialiseAppViralitySettings;
+(void)handleSignupFlow;
+(void)getCamapaignDetails:(void (^)(NSDictionary *))handler;
+(void)showGrowthHackOnController:(UIViewController *)controller withDetails:(NSDictionary *)campaignDetails;
@end
