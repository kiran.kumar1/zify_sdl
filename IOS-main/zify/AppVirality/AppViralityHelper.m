//
//  AppViralityHelper.m
//  zify
//
//  Created by Anurag S Rathor on 04/11/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "AppViralityHelper.h"
#import "AppVirality.h"
#import "UserProfile.h"
#import "CountryCode.h"
#import "DBInterface.h"
#import "AppViralityGrowthHackViewController.h"
#import "CurrentLocale.h"

@implementation AppViralityHelper
+(void)initialiseAppViralitySettings{
    @try {
        [AppVirality getReferrerDetails:^(NSDictionary *referrerDetails, NSError *error) {
            if(referrerDetails){
                if ([referrerDetails objectForKey:@"WelcomeMessage"] && ![@"" isEqualToString:[referrerDetails objectForKey:@"WelcomeMessage"]]) {
                    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
                    [userDefaults setBool:YES forKey:@"isAppViralityFriendReferral"];
                }
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
+(void)handleSignupFlow{
    UserProfile *currentUser = [UserProfile getCurrentUser];
    NSString *userId = [AppViralityHelper getUserIdFromUser:currentUser];
    CountryCode *countryObj = [CountryCode getCountryObjectForISDCode:currentUser.isdCode inContext:[[DBInterface sharedInstance] sharedContext]];
    NSString *countryName = countryObj != nil ? countryObj.name : @"India";
    @try {
        NSDictionary * userDetails = @{@"EmailId":currentUser.emailId, @"AppUserName":[NSString stringWithFormat:@"%@ %@",currentUser.firstName,currentUser.lastName], @"UserIdInstore":userId,  @"ProfileImage":currentUser.profileImgUrl != nil ? currentUser.profileImgUrl : @"",@"country":countryName};
        [AppVirality setUserDetails:userDetails Oncompletion:^(BOOL success,NSError *error){
            NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
            BOOL isAppviralityFriendReferral = [userDefaults boolForKey:@"isAppViralityFriendReferral"];
            if(isAppviralityFriendReferral){
                [AppVirality saveConversionEvent:@{@"eventName":@"Signup"} completion:^(NSDictionary *conversionResult,NSError* error) {
                    [userDefaults removeObjectForKey:@"isAppViralityFriendReferral"];
                }];
            }
        }];
    }@catch(NSException *exception){
        
    }@finally{
        
    }
}
+(void)getCamapaignDetails:(void (^)(NSDictionary *))handler{
    @try {
        UserProfile *user = [UserProfile getCurrentUser];
        NSString *userId = [AppViralityHelper getUserIdFromUser:user];
        NSString *countryName = [[CurrentLocale sharedInstance] getCountryName];
        NSDictionary * userDetails = @{@"EmailId":user.emailId, @"AppUserName":[NSString stringWithFormat:@"%@ %@",user.firstName,user.lastName], @"UserIdInstore":userId ,@"ProfileImage":user.profileImgUrl == nil ? @"" : user.profileImgUrl,@"country":countryName};
        [AppVirality setUserDetails:userDetails Oncompletion:^(BOOL success,NSError *error){
            [AppVirality getGrowthHack:GrowthHackTypeWordOfMouth completion:^(NSDictionary *campaignDetails,NSError*error) {
                handler(campaignDetails);
            }];
        }];
    } @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

+(void)showGrowthHackOnController:(UIViewController *)controller withDetails:(NSDictionary *)campaignDetails{
    if (campaignDetails) {
        AppViralityGrowthHackViewController * growthHackVC = [[AppViralityGrowthHackViewController alloc] initWithCampaignDetails:campaignDetails ForGrowthHack:GrowthHackTypeWordOfMouth];
        UINavigationController * navVC = [[UINavigationController alloc] initWithRootViewController:growthHackVC];
        [controller presentViewController:navVC animated:YES completion:nil];
    }
}

+(NSString *)getUserIdFromUser:(UserProfile *)currentUser{
    NSString *userId = currentUser.userId.stringValue;
    if(!userId) return @"";
    if(currentUser.isGlobal.intValue){
       return [userId stringByAppendingString:@":1"];
    } else{
       return [userId stringByAppendingString:@":0"];
    }
}
@end
