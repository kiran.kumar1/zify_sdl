//
//  CreateRideRequest2.swift
//  zify
//
//  Created by Anurag on 14/09/19.
//  Copyright © 2019 zify. All rights reserved.
//

import Foundation
/***/
var searchRideEntity: SearchRideEntity?
//var searchRide: SearchRide?

var driveidNum: NSNumber?
var driveridNum: NSNumber?
var srcnearlatNum: NSString?
var srcnearlongNum: NSString?
var destnearlatNum: NSString?
var destnearlongNum: NSString?

var userSearchData:UserSearchData?

let SEATS_NUM_PARAM = "numOfSeats"
let DEPARTURE_TIME_PARAM = "departureTime"
let ROUTE_ID_PARAM = "routeId"
let CITY_PARAM = "city"
let DRIVE_ID_PARAM = "driveId"
let DRIVER_ID_PARAM = "driverId"
let COUNTRY_CODE_PARAM = "countryCode"
let COST_PER_SEAT = "costToRider"
let SRC_NEAR_LAT = "srcNearLat"
let SRC_NEAR_LNG = "srcNearLng"
let DEST_NEAR_LAT = "destNearLat"
let DEST_NEAR_LNG = "destNearLng"

let SEARCH_SRC_NEAR_LAT = "srcActualLat"
let SEARCH_SRC_NEAR_LNG = "srcActualLng"
let SEARCH_DEST_NEAR_LAT = "destActualLat"
let SEARCH_DEST_NEAR_LNG = "destActualLng"


class CreateRideRequest2 : ServerRequest {
    init(searchRideEntity searchRide: SearchRideEntity?, driveid : NSNumber, driverid : NSNumber, srcnearlat : NSString, srcnearlong : NSString, destnearlat : NSString,destnearlong : NSString, withUserSearchData:UserSearchData) {
        searchRideEntity = searchRide
        driveidNum = driveid
        driveridNum = driverid
        srcnearlatNum = srcnearlat
        srcnearlongNum = srcnearlong
        destnearlatNum = destnearlat
        destnearlongNum = destnearlong
        userSearchData = withUserSearchData
    }
    
//    init(searchRide: SearchRide?) {
//        self.searchRide = searchRide
//    }
    
    override func serverURL() -> String? {
        return "/rider/V2/requestRide"
    }
    

    override func urlparams() -> [AnyHashable : Any]? {
        
        let params =  self.getUserCommonParams() as NSMutableDictionary
//        if searchRide {
//            params[ROUTE_ID_PARAM] = searchRide.routeId
//            params[DEPARTURE_TIME_PARAM] = searchRide.departureTime
//            params[SEATS_NUM_PARAM] = NSNumber(value: 1)
//            params[CITY_PARAM] = searchRide.city
//            params[DRIVE_ID_PARAM] = searchRide.driveId
//            params[DRIVER_ID_PARAM] = searchRide.driverId
//        } else
        
        if (searchRideEntity != nil) {
            params[ROUTE_ID_PARAM] = searchRideEntity?.routeId
            params[DEPARTURE_TIME_PARAM] = AppDelegate.getInstance().converDateToISOStandard(forRideRequest: searchRideEntity?.departureTime) //
            params[SEATS_NUM_PARAM] = NSNumber(value: 1)
            params[CITY_PARAM] = searchRideEntity?.city
            params[DRIVE_ID_PARAM] = searchRideEntity?.driveId//driveidNum//searchRideEntity?.driveId
            params[DRIVER_ID_PARAM] = searchRideEntity?.driverId//driveridNum//searchRideEntity?.driverId
            params[COST_PER_SEAT] = searchRideEntity?.amountPerSeat
            params[SRC_NEAR_LAT] = srcnearlatNum!//searchRideEntity?.srcNearLat
            params[SRC_NEAR_LNG] = srcnearlongNum!//searchRideEntity?.srcNearLong//NSNumber(value: searchRideEntity?.srcNearLong.doubleValue)
            params[DEST_NEAR_LAT] = destnearlatNum!//searchRideEntity?.destNearLat//NSNumber(value: searchRideEntity?.destNearLat.doubleValue)
            params[DEST_NEAR_LNG] = destnearlongNum!//searchRideEntity?.destNearLong//NSNumber(value: searchRideEntity?.destNearLong.doubleValue)
            
            
            params[SEARCH_SRC_NEAR_LAT] = userSearchData?.sourceLocality.latLng.latitude
            params[SEARCH_SRC_NEAR_LNG] = userSearchData?.sourceLocality.latLng.longitude
            params[SEARCH_DEST_NEAR_LAT] = userSearchData?.destinationLocality.latLng.latitude
            params[SEARCH_DEST_NEAR_LNG] = userSearchData?.destinationLocality.latLng.longitude

        }
        let profile = UserProfile.getCurrentUser()
        if let country = profile?.country {
            params[COUNTRY_CODE_PARAM] = country
        }
        return params as? [AnyHashable : Any]
    }
    
    override func mappingKey() -> String? {
        return "rechargeAmount"
    }
    
    override func isActualResponseSingleAttribute() -> Bool {
        return true
    }
    
    override func isSuccessResponseCode(_ responseCode: Int32) -> Bool {
        return responseCode == 1 || responseCode == -30

    }
    
//    func isSuccessResponseCode(_ responseCode: Int) -> Bool {
//        return responseCode == 1 || responseCode == -30
//    }

    
    override func isForZenParkService() -> Bool {
        return true
    }
    
    /**/
}
