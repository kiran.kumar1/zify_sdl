//
//  UserChatProfileEntity.h
//  zify
//
//  Created by Anurag Rathor on 23/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>


@interface UserChatProfileEntity : NSObject
@property (nonatomic,strong) NSString *chatUserId;
@property (nonatomic,strong) NSString *chatUserPassword;
@property (nonatomic,strong) NSString *chatUserName;
@property (nonatomic,strong) NSString *chatUserEmail;
+(RKObjectMapping*)getUserChatProfileObjectMapping;
+(void)createCurrentChatUserProfile:(UserChatProfileEntity *)userChatProfile;
+(UserChatProfileEntity *)getCurrentUserChatProfileInContext:(NSManagedObjectContext *)context;
+(void) deleteCurrentChatUserProfile;
-(id)initWithChatUserId:(NSString *)userId  andUserName:(NSString *)userName andUserEmail:(NSString *)userEmail andChatPassword:(NSString *)chatPassword;

-(id)initWithDictionary:(NSDictionary *)infoDict;
@end
