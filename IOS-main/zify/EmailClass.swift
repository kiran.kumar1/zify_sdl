//
//  EmailClass.swift
//  zify
//
//  Created by Anurag on 07/08/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

class EmailClass: NSObject {
    
    var emailRequestType2 = (REDEEM : "REDEEM", WITHDRAWL : "WITHDRAWL")
    
    
    class func sendMail(strLRCoins : String, iRequestType : Int, strWithdrawlAmount : String) {
        
        var requestType = "REDEEM"
        if iRequestType == 1 {
            requestType = "WITHDRAWL"
        }
        
        let reqSendMail = SendEmailRequest.init(lrCoins: strLRCoins, requestType: requestType, withDrawlAmount: strWithdrawlAmount)
        
        debugPrint("---reqSendMail  service -----\(String(describing: reqSendMail))")
        
        ServerInterface.sharedInstance()?.getResponse(reqSendMail, withHandler: { (response, error) in
            if error == nil {
                debugPrint("---success send mail service -----\(String(describing: response))")
            } else {
                debugPrint("---error in send mail request---\(String(describing: error?.localizedDescription))")
            }
        })
    }
    
}

class SendEmailRequest : ServerRequest {
    let userProfileObj = UserProfile.getCurrentUser()
    
    let regionDetailObj = UserProfileRegionObject.getMapping()
    var dictParamsTemp : [String : AnyObject] = [:]

    init(lrCoins : String, requestType : String, withDrawlAmount : String) {
        
        dictParamsTemp = [
                      "lrCoins" : lrCoins,
                      "requestType" : requestType,
                      "withDrawlAmount" : withDrawlAmount
            ] as [String : AnyObject]
        
       /* dictParams = [
            "countryCode" : userProfileObj?.isdCode ?? "" as AnyObject,
            "firstName" : userProfileObj?.firstName ?? "" as AnyObject,
            "isdCode" : userProfileObj?.isdCode ?? "" as AnyObject,
            "languageISO" : "\(CurrentLocale.sharedInstance()?.getString() ?? "")",
            "lname" : userProfileObj?.lastName ?? "" as AnyObject,
            "lrCoins" : lrCoins,
            "mobileNo" : userProfileObj?.mobile ?? "" as AnyObject,
            "requestType" : requestType,
            "userEmail" : userProfileObj?.emailId ?? "" as AnyObject,
            "userId" : userProfileObj?.userId ?? "" as AnyObject,
            "withDrawlAmount" : withDrawlAmount
            ] as [String : AnyObject]
        */
    }
    
    
    override func urlparams() -> [AnyHashable : Any]! {
        //let urlParams = [dictParam] as [String : Any]
        

        dictParamsTemp.updateValue((userProfileObj?.country ?? "") as AnyObject, forKey: "countryCode")
        dictParamsTemp.updateValue((userProfileObj?.firstName ?? "") as AnyObject, forKey: "firstName")
        dictParamsTemp.updateValue((userProfileObj?.isdCode ?? "") as AnyObject, forKey: "isdCode")
        dictParamsTemp.updateValue("\(CurrentLocale.sharedInstance()?.getString() ?? "")" as AnyObject, forKey: "languageISO")
        dictParamsTemp.updateValue((userProfileObj?.lastName ?? "") as AnyObject, forKey: "lname")
        dictParamsTemp.updateValue((userProfileObj?.mobile ?? "") as AnyObject, forKey: "mobileNo")
        dictParamsTemp.updateValue((userProfileObj?.emailId ?? "") as AnyObject, forKey: "userEmail")
        dictParamsTemp.updateValue((userProfileObj?.userId ?? "" as AnyObject) as AnyObject, forKey: "userId")
        
        debugPrint("---dict params------", dictParamsTemp)
        return dictParamsTemp
    }
    
    override func overrideBaseURL() -> Bool {
        return true
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
    
    override func isJSONRequestFormat() -> Bool {
        return true
    }
    
    override func serverURL() -> String! {
        
        //http://test.zify.co/zifyemaildev/email/generateMail
        //https://m.zify.co/zifyemail/email/generateMail
        //For Test Server
        //return "zifyemaildev/email/generateMail"
        //For Live Server
        return "/zifyemail/email/generateMail"
    }
    
    override func isForReferAndEarn() -> Bool {
        return true
    }
    
}
