//
//  DriverDetailEntity.h
//  zify
//
//  Created by Anurag Rathor on 23/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DriverDetailDocumentsEntity.h"
#import "UserChatProfileEntity.h"

@interface DriverDetailEntity : NSObject
@property (nonatomic,strong) NSString *firstName;
@property (nonatomic,strong) NSString *lastName;
@property (nonatomic,strong) NSString *driverStatus;
@property (nonatomic,strong) NSString *companyName;
@property (nonatomic,strong) NSNumber *totalDistance;
@property (nonatomic,strong) NSString *profileImgUrl;
@property (nonatomic,strong) NSNumber *driverRatingFemale;
@property (nonatomic,strong) NSNumber *driverRatingMale;
@property (nonatomic,strong) NSNumber *numOfFemalesRated;
@property (nonatomic,strong) NSNumber *numOfMalesRated;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,strong) NSString *jabberId;
@property (nonatomic,strong) NSString *callId;
@property (nonatomic,strong) NSString *vehicleImgUrl;
@property (nonatomic,strong) NSString *vehicleModel;
@property (nonatomic,strong) NSString *vehicleRegistrationNum;

@property (nonatomic,strong) DriverDetailDocumentsEntity *documents;
@property (nonatomic,strong) UserChatProfileEntity *chatProfile;

-(id)initWithDictionary:(NSDictionary *)infoDict;
@end
