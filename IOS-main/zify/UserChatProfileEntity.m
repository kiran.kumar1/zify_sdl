//
//  UserChatProfileEntity.m
//  zify
//
//  Created by Anurag Rathor on 23/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "UserChatProfileEntity.h"

#import "DBUserDataInterface.h"
#import "UserAccessData.h"

#define USER_CHAT_PROFILE_KEY @"qbUserChatProfile"

@implementation UserChatProfileEntity
-(id)initWithDictionary:(NSDictionary *)infoDict{
    if([self init]){
        _chatUserId = [infoDict objectForKey:@"qbChatUserId"];
        _chatUserPassword = [infoDict objectForKey:@"qbChatPswd"];
        _chatUserName = [infoDict objectForKey:@"qbUserName"];
        _chatUserEmail = [infoDict objectForKey:@"qbUserEmail"];
    }
    return self;
}

+(RKObjectMapping*)getUserChatProfileObjectMapping{
    RKObjectMapping *userChatProfileObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [userChatProfileObjectMapping addAttributeMappingsFromDictionary:@{@"qbChatUserId":@"chatUserId",
                                                                       @"qbUserName":@"chatUserName",
                                                                       @"qbUserEmail":@"chatUserEmail",
                                                                       @"qbChatPswd":@"chatUserPassword"}];
    return userChatProfileObjectMapping;
}


-(id)initWithChatUserId:(NSString *)userId  andUserName:(NSString *)userName andUserEmail:(NSString *)userEmail andChatPassword:(NSString *)chatPassword{
    self = [super init];
    if (self) {
        _chatUserId = userId;
        _chatUserName = userName;
        _chatUserEmail = userEmail;
        _chatUserPassword = chatPassword;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)coder{
    self=[[UserChatProfileEntity alloc]init];
    if (self!=nil) {
        _chatUserId = [coder decodeObjectForKey:@"chatUserId"];
        _chatUserName = [coder decodeObjectForKey:@"chatUserName"];
        _chatUserEmail = [coder decodeObjectForKey:@"chatUserEmail"];
        _chatUserPassword = [coder decodeObjectForKey:@"chatUserPassword"];
        
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)coder{
    [coder encodeObject:_chatUserId forKey:@"chatUserId"];
    [coder encodeObject:_chatUserName forKey:@"chatUserName"];
    [coder encodeObject:_chatUserEmail forKey:@"chatUserEmail"];
    [coder encodeObject:_chatUserPassword forKey:@"chatUserPassword"];
}

+(void)createCurrentChatUserProfile:(UserChatProfileEntity *)userChatProfile{
    NSData *data=[NSKeyedArchiver archivedDataWithRootObject:userChatProfile];
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    NSString *userChatDataString = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    //[context performBlockAndWait:^{
    [UserAccessData createUserAccessDataWithType:USER_CHAT_PROFILE_KEY andValue:userChatDataString inContext:context];
    [[DBUserDataInterface sharedInstance] saveSharedContext];
    //}];
}

+(UserChatProfileEntity *)getCurrentUserChatProfileInContext:(NSManagedObjectContext *)context{
    UserAccessData *accessData = [UserAccessData getUserAccessDataWithType:USER_CHAT_PROFILE_KEY inContext:context];
    if(accessData) {
        UserChatProfileEntity *userChatProfile = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSData alloc] initWithBase64EncodedString:accessData.value options:(NSDataBase64DecodingIgnoreUnknownCharacters)]];
        return userChatProfile;
    }
    return nil;
}

+(void) deleteCurrentChatUserProfile{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    [context performBlock:^{
        UserAccessData *accessData = [UserAccessData getUserAccessDataWithType:USER_CHAT_PROFILE_KEY inContext:context]; //If chate registration is failed there will be no access data.
        if(accessData){
            [context deleteObject:accessData];
            [[DBUserDataInterface sharedInstance] saveSharedContext];
        }
    }];
}
@end
