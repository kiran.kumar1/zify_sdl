//
//  AppDelegate.m
//  zify
//
//  Created by Kevin Vishal on 30/01/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+Random.h"
#import "UserProfile.h"
#import "AppVirality.h"
#import <Stripe/Stripe.h>
#import "PushNotificationManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "UserChatProfile.h"
#import "DBUserDataInterface.h"
#import "DBInterface.h"
#import "LocalNotificationManager.h"
#import "CurrentLocale.h"
#import "ChatManager.h"
#import <NMAKit/NMAKit.h>
#import "MainContentController.h"
#import <Branch/Branch.h>
#import "UserDataManager.h"
#import "ChatNotificationView.h"
#import "IQKeyboardManager.h"
#import "zify-Swift.h"
#import "UserAddressPreferencesController.h"
#import "TutorialShownDB.h"
#import "FirebaseEventClass.h"
#import "LocationDisabledViewController.h"
#import "SearchRideViewController.h"
#import "LocalisationConstants.h"
#import "GuestHomeController.h"
#import "SearchRideViewController.h"
#import "CurrentDriveController.h"
#import "UIView+iOS.h"
#import "PushNotificationView.h"
#import "OneSignalPlayerid.h"
#import "PushOfferView.h"
#import "PushAppUpgradeView.h"
#import "PushRideRequest.h"
#import "TripRatingsRequest.h"
#import "UIImage+initWithColor.h"


@import GoogleMaps;
@import GooglePlaces;
@import Firebase;


@interface AppDelegate ()

@end

@implementation AppDelegate
static AppDelegate *singletonObject = nil;

#define TRUE_CALLER_APP_KEY @"SYsWoc1aa249ce4e54314b4b46d3054626120"
#define TRUE_CALLER_APP_LINK @"https://si9a41d1e9e76e4126ac317dc1de373b4e.truecallerdevs.com"


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSLog(@"height is %f", [AppDelegate SCREEN_HEIGHT]);
    [IQKeyboardManager sharedManager].shouldHidePreviousNext = NO;
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [GMSServices provideAPIKey:@"AIzaSyCdxMUZw2MsDWzzUTA9m4Wi3SSZEEzJ8Vo"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyCdxMUZw2MsDWzzUTA9m4Wi3SSZEEzJ8Vo"];
    [NMAApplicationContext setAppId:@"6NDSFoeIvkpqsGx7yLQ2" appCode:@"G_bNzzw9QokfHDInW7whJA"];
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
    [AppVirality attributeUserBasedonCookie:@"5350794299d74a579426a521008f9f46" OnCompletion:^(BOOL success, NSError *error) {
        [AppVirality initWithApiKey:@"5350794299d74a579426a521008f9f46" WithParams:nil OnCompletion:^(NSDictionary *referrerDetails,NSError*error) {
        }];
    }];
    [self clearNotifications];
    [self addONeSignalMethodInitializersWithApplication:application andWithLaunchOptions:launchOptions];
    [self addMoEngageMethodInitializersWithApplication:application andWithLaunchOptions:launchOptions];
    /*[AppVirality registerAsDebugDevice:^(BOOL success, NSError *error) {
     NSLog(@"Register Test Device Response: %d ", success);
     }];*/
    NSString *sessionId = [[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    if(sessionId){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"sessionId"];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSLog(@"path is %@", documentsDirectory);
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString randomAlphanumericStringWithLength:10] forKey:@"sessionId"];
    // live
    //   [Stripe setDefaultPublishableKey:@"pk_live_l7qf8xDnXZvPHJ0Edcy5pjJE"]; old
    [Stripe setDefaultPublishableKey:@"pk_live_afhjNm7NHU53ba06t4BTVV2B0087buS750"]; //New
    //Development
    // [Stripe setDefaultPublishableKey:@"pk_test_OwNtY2KpeDdFaKi9P5wDtmzS"]; old
    // [Stripe setDefaultPublishableKey:@"pk_test_KGrXCUEMw81IvUZ1bZuJIVhb001RyEGk34"];  //New
    
    [Fabric with:@[[Crashlytics class]]];
    //Initialising shared DB Interfaces on Main Thread
    [DBInterface sharedInstance];
    [DBUserDataInterface sharedInstance];
    [CurrentLocale sharedInstance];
    [UserDataManager sharedInstance];
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // NSLog(@"Branch Params are %@",params);
        [[UserDataManager sharedInstance] handleBranchDeepLinkParams:params];
    }];
    [self showRootViewController];
    
    /* int deleteCount = [[[NSUserDefaults standardUserDefaults] valueForKey:@"oneTimeWorkaroundKey"] intValue];
     //  if(deleteCount == 0){
     
     if([FirebaseChatClass sharedInstance].fcmTokenFromServer != nil){
     [[FIRInstanceID instanceID] deleteIDWithHandler:^(NSError * _Nullable error) {
     if (error != nil){
     // [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d", deleteCount] forKey:@"oneTimeWorkaroundKey"];
     }
     }];
     }
     //  [[NSUserDefaults standardUserDefaults] synchronize];
     
     */
    
    
    [[Crashlytics sharedInstance] logEvent:@"" attributes:nil];
    [[PushNotificationManager sharedInstance] registerForNotifcationSettingsWithLaunchOptions:launchOptions];
    [[LocalNotificationManager sharedInstance] handleNotifcationSettingsWithLaunchOptions:launchOptions];
    [[ChatManager sharedInstance] registerForChatSettings];
    [self methodForTutorialsDB];
    [self methodForOneSignalDB];
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    
    _timeFormatter = [[NSDateFormatter alloc] init];
    
    
    BOOL isTimein24 = [[NSUserDefaults standardUserDefaults] boolForKey:@"24hoursFormatEnabled"];
    if(!isTimein24){
        [self setTimeIn24HoursFormat];
    }else{
        [self setTimeIn12HoursFormat];
    }
    [self addFreshChatIntegration:launchOptions withApplication:application];
    [self setUpTrueCallerSDK];
    [self convertDOBFROMServiceTOReqFormat];
    //[self displayTripAmountInCommaSeparator:@"18.3"];
    self.remoteConfig = [FIRRemoteConfig remoteConfig];
    FIRRemoteConfigSettings *remoteConfigSettings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:NO];
    self.remoteConfig.configSettings = remoteConfigSettings;
    [self.remoteConfig setDefaultsFromPlistFileName:@"RemoteConfigDefaults"];
    [self remoteConfigValuesFetched];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    UIViewController *topVc = [self topMostController];
    if([topVc isKindOfClass:[UIAlertController class]]){
        UIAlertController *obj = (UIAlertController *)topVc;
        if(obj.view.tag == 1000){
            [topVc dismissViewControllerAnimated:NO completion:nil];
        }
    }
}
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    //  [UIAlertController showErrorWithMessage:fcmToken inViewController:self.window.rootViewController];
    [[FirebaseChatClass sharedInstance] receivedFCMTokenWithToken:fcmToken];
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
    
    
    
    //  For getting old token from DB...
    
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error fetching remote instance ID: %@", error);
        } else {
            NSLog(@"Remote instance ID token: %@", result.token);
            NSString* message =[NSString stringWithFormat:@"Remote InstanceID token: %@", result.token];
            [[FirebaseChatClass sharedInstance] receivedFCMTokenWithToken:result.token];
            NSLog(@"message token is %@", message);
            //If both the token are not same then update the old one with new
            
            
            // self.instanceIDTokenMessage.text = message;
        }
    }];
    
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self isShowUpdatedAlert];
    //   [[ChatManager sharedInstance] registerForChatSettings];
    BOOL isTimein24 = [[NSUserDefaults standardUserDefaults] boolForKey:@"24hoursFormatEnabled"];
    if(!isTimein24){
        [self setTimeIn24HoursFormat];
    }else{
        [self setTimeIn12HoursFormat];
    }
    if([CLLocationManager locationServicesEnabled]){
        UIViewController *topVc = [self topMostController];
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            if(bgViewForLocationsDenied){
                [bgViewForLocationsDenied removeFromSuperview];
                bgViewForLocationsDenied = nil;
            }
            //NSLog(@"**topVc** %@",topVc);
            [self showLocationAccessScreen:_topNavController];
        }else{
            [self removeLocationAccessScreenIfEsists];
            /*if([topVc isKindOfClass:[CurrentDriveController class]]){
             CurrentDriveController *obj = (CurrentDriveController *)topVc;
             [obj showCurrentDriveCoachMarks];
             }*/
        }
    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self clearNotifications];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray * __nullable restorableObjects))restorationHandler{
    if(_isForTrueCall){
        _isForTrueCall = NO;
        return [[TCTrueSDK sharedManager] application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
    }
    BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
    return handledByBranch;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if (![[Branch getInstance] handleDeepLink:url]) {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"Device Token is:%@",deviceToken);
    [[PushNotificationManager sharedInstance] handlePushDeviceToken:deviceToken];
    [[Freshchat sharedInstance] setPushRegistrationToken:deviceToken];
    [[MoEngage sharedInstance] setPushToken:deviceToken];
    
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Error is called");
    [[MoEngage sharedInstance]didFailToRegisterForPush];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    //[[PushNotificationManager sharedInstance] handleNotificationData:userInfo];
    
    NSLog(@"userInfo is %@", userInfo);
    if ([[Freshchat sharedInstance]isFreshchatNotification:userInfo]) {
        NSString *string = [NSString stringWithFormat:@"my dictionary is %@", userInfo];
        [UIAlertController showErrorWithMessage:string inViewController:_window];
        [[Freshchat sharedInstance]handleRemoteNotification:userInfo andAppstate:application.applicationState];
        
    }
    [self clearNotifications];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(NSDictionary *)userInfo {
    NSLog(@"user info is %@", userInfo);
    [[LocalNotificationManager sharedInstance] handleNotificationData:userInfo];
}
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    NSDictionary *infoDict = [[userInfo objectForKey:@"custom"] objectForKey:@"a"];
    NSDictionary *payloadDict = [[NSDictionary alloc] initWithObjectsAndKeys:infoDict,@"payload", nil];
    [[PushNotificationManager sharedInstance] handleUserSelectedAction:identifier andUserInfo:payloadDict];
    if (completionHandler) {
        completionHandler();
    }
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    [[LocalNotificationManager sharedInstance] handleUserSelectedAction:identifier andUserInfo:userInfo];
    if (completionHandler) {
        completionHandler();
    }
}
-(void)showRootViewController{
    if([UserProfile getCurrentUser]){
        [self showUserBaseController];
    } else{
        //[self showGuestHomeController];
        [self showLoginController];
    }
    //     [self.window setBackgroundColor:[UIColor clearColor]];
    //     self.window.windowLevel = 1.2;
}

-(void)showUserBaseController{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
    UIViewController *rootViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"userBaseController"];
    [self.window setRootViewController:rootViewController];
    // [self.window setBackgroundColor:[UIColor clearColor]];
    //self.window.windowLevel = 1.2;
    [self.window makeKeyAndVisible];
}

-(void)showGuestHomeController{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIViewController *rootViewController = [MainContentController createMainContentGuestController];
    [self.window setRootViewController:rootViewController];
    [self.window makeKeyAndVisible];
}


-(void)showTravelPrefAfterSignUpOnly{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    TravelPrefStep1 *obj = [[TravelPrefStep1 alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:obj];
    navController.navigationBarHidden = YES;
    [self.window setRootViewController:navController];
    [self.window makeKeyAndVisible];
}

-(void)showUserHomeController{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIViewController *rootViewController = [MainContentController createMainContentController];
    [self.window setRootViewController:rootViewController];
    [self.window makeKeyAndVisible];
}

-(void)showLoginController{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UINavigationController *rootController = [SignUpController createSignUpNavController];
    [self.window setRootViewController:rootController];
    [self.window makeKeyAndVisible];
}
- (UIViewController*) topMostController1
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    return topController;
}
- (UIViewController*)topMostController {
    UIViewController *vc = [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
    NSLog(@"top vcccc is %@", vc);
    return vc;
    
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

+(void)applyVerticalGradient:(UIView *)view{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(0, 0, view.frame.size.width, 2.5*view.frame.size.height);//view.bounds;
    // gradientLayer.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:231/255.0 green:76/255.0 blue:58/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:217/255.0 green:79/255.0 blue:63/255.0 alpha:0.8].CGColor,(id)[UIColor colorWithRed:214/255.0 green:109/255.0 blue:97/255.0 alpha:1.0].CGColor, nil];
    gradientLayer.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:38/255.0 green:98/255.0 blue:255/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:38/255.0 green:98/255.0 blue:255/255.0 alpha:0.8].CGColor,(id)[UIColor colorWithRed:38/255.0 green:98/255.0 blue:255/255.0 alpha:1.0].CGColor, nil];
    [view.layer addSublayer:gradientLayer];
}
+ (AppDelegate*) getAppDelegateInstance
{
    if (singletonObject == nil)
    {
        singletonObject = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    }
    return singletonObject;
}

+(CGFloat)SCREEN_WIDTH{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.width;
}
+(CGFloat)SCREEN_HEIGHT{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //  NSLog(@"screen height is %f", screenRect.size.height);
    return screenRect.size.height;
}

-(void)moveTOTravelPrefenceScreen:(UIViewController *)vc{
    UserAddressPreferencesController *addressPreferencesController = [UserAddressPreferencesController createUserAddressPreferencesController];
    addressPreferencesController.showPageIndicator = NO;
    addressPreferencesController.isUpgradeFlow = NO;
    [vc presentViewController:addressPreferencesController animated:NO completion:nil];
}

+(UINavigationController *)createFeedbackNavController:(UIViewController *)fromVc{
    UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"UserSettings" bundle:[NSBundle mainBundle]];
    UINavigationController *settingsNavController = (UINavigationController *)[storyBoard instantiateViewControllerWithIdentifier:@"settingsNavController"];
    AboutSupportController *feedBackVc =  [storyBoard instantiateViewControllerWithIdentifier:@"feedback"];
    feedBackVc.messageDelegate = fromVc;
    NSArray *vcsArr = [[NSArray alloc] initWithObjects:feedBackVc, nil];
    [settingsNavController setViewControllers:vcsArr animated:YES];
    return settingsNavController;
}

-(UIImage *)getNewImageWithOldImage:(UIImage *)oldImage withNewColor:(UIColor *)newColor{
    UIColor *color = newColor;
    UIImage *image = oldImage;// Image to mask with
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, CGRectMake(0, 0, image.size.width, image.size.height), [image CGImage]);
    CGContextFillRect(context, CGRectMake(0, 0, image.size.width, image.size.height));
    UIImage*  coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return coloredImg;
}
-(void)methodForTutorialsDB{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    TutorialShownDB *obj = [self getTutorialsDBObject];
    if(obj.isInGuestHomeShown.length == 0 || obj.isInCreateRideShown.length == 0 || obj.isInPublishRideShown.length == 0){
        [TutorialShownDB insertTutorialIntoDB:@"0" andisInCreateRideShown:@"0" andisInPublishRideShown:@"0" inContext:context];
    }
}
-(void)methodForOneSignalDB{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    OneSignalPlayerid* obj = [OneSignalPlayerid getPlayerIdObjForDeviceInContext:context];
    if(obj.playerId.length == 0){
        [OneSignalPlayerid insertOneSignalPlayerIdIntoDB:@"" inContext:context];
    }
}
-(TutorialShownDB *)getTutorialsDBObject{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    return  [TutorialShownDB getTutorialsObjectForuserInContext:context];
}
-(void)updateTutorialDBObject:(TutorialShownDB *)obj{
    [TutorialShownDB updateWithAppropriateValues:obj];
}
-(void)setTimeIn24HoursFormat{
    //NSLog(@"time is in 24 hours format");
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [self.timeFormatter setDateFormat:@"HH:mm"];
    [self.timeFormatter setLocale:locale];
}
-(void)setTimeIn12HoursFormat{
    //NSLog(@"time is in 12 hours format");
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [self.timeFormatter setDateFormat:@"hh:mm a"];
    [self.timeFormatter setLocale:locale];
}

-(void)showLocationAccessScreen:(UIViewController *)superViewController{
    [self createViewForNoNAccessLocationServices:superViewController.view];
}
-(void)removeLocationAccessScreenIfEsists{
    if(bgViewForLocationsDenied){
        NSLog(@"removing from superview");
        [bgViewForLocationsDenied removeFromSuperview];
        bgViewForLocationsDenied = nil;
        if([_topNavController isKindOfClass:[GuestHomeController class]]){
            GuestHomeController *obj = (GuestHomeController *)_topNavController;
            if([obj respondsToSelector:@selector(displayproperViewForLocationEnabledOrDisabled)]){
                [obj displayproperViewForLocationEnabledOrDisabled];
            }
        }else if([_topNavController isKindOfClass:[SearchRideViewController class]]){
            SearchRideViewController *obj = (SearchRideViewController *)_topNavController;
            if([obj respondsToSelector:@selector(displayproperViewForLocationEnabledOrDisabled)]){
                [obj displayproperViewForLocationEnabledOrDisabled];
            }
        }
    }
}

-(void)createViewForNoNAccessLocationServices:(UIView *)superview{
    if(bgViewForLocationsDenied){
        [bgViewForLocationsDenied removeFromSuperview];
        bgViewForLocationsDenied = nil;
    }
    NSLog(@"Adding from superview %@", _topNavController);
    bgViewForLocationsDenied = [[UIView alloc] init];
    bgViewForLocationsDenied.frame = superview.frame;
    bgViewForLocationsDenied.backgroundColor = [UIColor redColor];
    [superview addSubview:bgViewForLocationsDenied];
    bgViewForLocationsDenied.userInteractionEnabled = true;
    bgViewForLocationsDenied.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:251.0/255.0 blue:251.0/255.0 alpha:1.0];
    
    NSString *btnTurnOnTitle = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_BTN_TITLE, nil);
    NSString *btnTypeAddressTitle = NSLocalizedString(LOCATION_SERVICES_TYPE_ADDRESSES_BTN_TITLE, nil);
    NSString *titleStr = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_MSG_TITLE, nil);
    NSString *contentStr = NSLocalizedString(LOCATION_SERVICES_NOTENABLE_MSG, nil);
    
    CGFloat width = 150;
    UIImageView *mapInImage = [[UIImageView alloc] init];
    mapInImage.frame = CGRectMake((bgViewForLocationsDenied.frameWidth - width)/2, 100, width, width);
    mapInImage.image = [UIImage imageNamed:@"futuro_icons_344.png"];
    [bgViewForLocationsDenied addSubview:mapInImage];
    // mapInImage.hidden = YES;
    
    CGFloat posY = 30;
    if(IS_IPHONE_X){
        posY = 70;
    }
    CGFloat btnHeight = 50;
    UIButton *btnTypeAddress = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTypeAddress.frame = CGRectMake(15,bgViewForLocationsDenied.frameHeight - btnHeight - posY, bgViewForLocationsDenied.frameWidth - 30, btnHeight);
    btnTypeAddress.backgroundColor = [UIColor colorWithRed:67.0/355.0 green:91.0/255.0 blue:108.0/255.0 alpha:1.0];
    [btnTypeAddress setTitle:btnTypeAddressTitle forState:UIControlStateNormal];
    [btnTypeAddress setTitle:btnTypeAddressTitle forState:UIControlStateSelected];
    btnTypeAddress.layer.cornerRadius = btnTypeAddress.frameHeight/2;
    btnTypeAddress.titleLabel.font = [UIFont fontWithName:NormalFont size:15];
    [btnTypeAddress addTarget:self action:@selector(btnTypeAddresstapped:) forControlEvents:UIControlEventTouchUpInside];
    [bgViewForLocationsDenied addSubview:btnTypeAddress];
    
    UIButton *btnTurnOnLocationServices = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTurnOnLocationServices.frame = btnTypeAddress.frame;
    btnTurnOnLocationServices.frameY = btnTypeAddress.frameY -btnTurnOnLocationServices.frameHeight - 20;
    [btnTurnOnLocationServices setTitle:btnTurnOnTitle forState:UIControlStateNormal];
    [btnTurnOnLocationServices setTitle:btnTurnOnTitle forState:UIControlStateSelected];
    btnTurnOnLocationServices.backgroundColor = [UIColor colorWithRed:38.0/355.0 green:98.0/255.0 blue:255.0/255.0 alpha:1.0];
    btnTurnOnLocationServices.layer.cornerRadius = btnTurnOnLocationServices.frameHeight/2;
    //    btnTurnOnLocationServices.backgroundColor = [UIColor colorWithRed:222.0/255.0 green:53.0/255.0 blue:45.0/255.0 alpha:1.0];
    [btnTurnOnLocationServices addTarget:self action:@selector(btnLocationServicesEnabledTapped:) forControlEvents:UIControlEventTouchUpInside];
    btnTurnOnLocationServices.titleLabel.font = [UIFont fontWithName:NormalFont size:15];
    
    [bgViewForLocationsDenied addSubview:btnTurnOnLocationServices];
    
    UILabel *msgContentLable = [[UILabel alloc] init];
    msgContentLable.frame = CGRectMake(15, 0, bgViewForLocationsDenied.frameWidth - 30, 10);
    msgContentLable.textColor = [UIColor colorWithRed:147.0/255.0 green:146.0/255.0 blue:165.0/255.0 alpha:1.0];
    msgContentLable.text = contentStr;
    msgContentLable.numberOfLines = 0;
    msgContentLable.font = [UIFont fontWithName:NormalFont size:15];
    msgContentLable.textAlignment = NSTextAlignmentCenter;
    [msgContentLable sizeToFit];
    [bgViewForLocationsDenied addSubview:msgContentLable];
    msgContentLable.frameX = (bgViewForLocationsDenied.frameWidth - msgContentLable.frameWidth)/2;
    msgContentLable.frameY = btnTurnOnLocationServices.frameY - msgContentLable.frameHeight - 10;
    
    UILabel *msgTitle = [[UILabel alloc] init];
    msgTitle.frame = CGRectMake(10, msgContentLable.frameY - 30, bgViewForLocationsDenied.frameWidth - 20, 30);
    msgTitle.text = titleStr;
    msgTitle.font = [UIFont fontWithName:NormalFont size:16];
    msgTitle.textAlignment = NSTextAlignmentCenter;
    [bgViewForLocationsDenied addSubview:msgTitle];
}
- (void)btnTypeAddresstapped:(UIButton *)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:@"isFromDeniedScreen"];
    [prefs synchronize];
    [self removeLocationAccessScreenIfEsists];
}

- (void)btnLocationServicesEnabledTapped:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:
     [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}
-(UIViewController *)getParentViewControllerFromSubView:(id)responder{
    while ([responder isKindOfClass:[UIView class]])
        responder = [responder nextResponder];
    return (UIViewController *)responder;
}

-(NSString *)converDateToISOStandard:(NSString *)choosenDateInStrFormat{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd HH:mm:ss";
    NSDate *choosenDateIndateFormat = [dateFormatter dateFromString:choosenDateInStrFormat];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    NSString *formattedDateString = [dateFormatter stringFromDate:choosenDateIndateFormat];
    NSTimeZone* timeZone = [NSTimeZone defaultTimeZone];
    NSString *dateAndTimeStrWithTimeZone = [NSString stringWithFormat:@"%@[%@]",formattedDateString,timeZone.name];
    NSLog(@"str: %@", dateAndTimeStrWithTimeZone);
    return dateAndTimeStrWithTimeZone;
}
-(NSString *)converDateToISOStandardForSearch:(NSString *)choosenDateInStrFormat{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm:ss a";
    // NSLog(@"UTC  date in  --%@", choosenDateInStrFormat);
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone localTimeZone]];
    [df_local setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDate *choosenDateIndateFormat22 = [df_local dateFromString:choosenDateInStrFormat];
    NSString *local2 = [NSString stringWithFormat:@"%@[%@]",[df_local stringFromDate:choosenDateIndateFormat22], df_local.timeZone.name];
    // NSLog(@"After Utc to LOCAL--%@", local2);
    return local2;
}
-(NSString *)converDateToISOStandardForRideRequest:(NSString *)choosenDateInStrFormat{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    // NSDate *choosenDateIndateFormat = [dateFormatter dateFromString:choosenDateInStrFormat];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone localTimeZone]];
    [df_local setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDate *choosenDateIndateFormat22 = [df_local dateFromString:choosenDateInStrFormat];
    NSString *local2 = [NSString stringWithFormat:@"%@[%@]",[df_local stringFromDate:choosenDateIndateFormat22], df_local.timeZone.name];
    NSLog(@"After Utc to LOCAL222--%@", local2);
    // NSString *formattedDateString = [dateFormatter stringFromDate:choosenDateIndateFormat];
    return local2;
}
-(void) addMoEngageMethodInitializersWithApplication:(UIApplication *)application andWithLaunchOptions:(NSDictionary *)launchOptions{
    // Separate initialization methods for Dev and Prod initializations
    // openDeeplinkUrlAutomatically tells us whether you want the SDK to call handleOpenUrl for deeplinks specified while creating a campaign
    NSString *moEngageAppId = @"XU59OT0E2MVY30BNCWCQMNOJ"; //@"XU59OT0E2MVY30BNCWCQMNOJ";//
    //  [[MoEngage sharedInstance] initializeProdWithApiKey:moEngageAppId inApplication:application withLaunchOptions:launchOptions openDeeplinkUrlAutomatically:YES];
    
#ifdef DEBUG
    //moEngageAppId = @"XU59OT0E2MVY30BNCWCQMNOJ_DEBUG";
    [[MoEngage sharedInstance] initializeDevWithApiKey:moEngageAppId inApplication:application withLaunchOptions:launchOptions openDeeplinkUrlAutomatically:YES];
#else
    [[MoEngage sharedInstance] initializeProdWithApiKey:moEngageAppId inApplication:application withLaunchOptions:launchOptions openDeeplinkUrlAutomatically:YES];
#endif
    [MoEngage debug:LOG_ALL];
    
    if (@available(iOS 10.0, *)) {
        [[MoEngage sharedInstance] registerForRemoteNotificationWithCategories:nil withUserNotificationCenterDelegate:self];
    } else {
        [[MoEngage sharedInstance] registerForRemoteNotificationForBelowiOS10WithCategories:nil];
    }
    
    /*  NSMutableDictionary *purchaseDict = [NSMutableDictionary dictionaryWithDictionary:@{@"product":@"Moto E",@"category":@"Mobiles", @"Price": @50}];
     
     [[MoEngage sharedInstance]trackEvent:@"Made Purchase1" andPayload:purchaseDict];
     [[MoEngage sharedInstance] setUserAttribute:@"Kiran Kota" forKey:@"FirstName"];
     // MoEngage.SetUserAttribute(MoEngageConstants.USER_ATTRIBUTE_UNIQUE_ID, uniqueId);
     
     MOPayloadBuilder *eventTracker = [[MOPayloadBuilder alloc]initWithDictionary:nil];
     // Here myDict can be nil or a dictionary with key value pairs for events. myDict will be assigned to eventTracker.eventDict and will be accessible for you to add more events. It is a NSMutableDictionary.
     
     // Set the timestamp
     [eventTracker setTimeStamp:1439322197 forKey:@"startTime"];
     
     // Set the date
     [eventTracker setDate:[NSDate date] forKey:@"startDate"];
     
     // Set the location
     [eventTracker setLocationLat:12.23 lng:9.23 forKey:@"startingLocation"];
     
     // Track the complete dictionary, with one event name.
     [[MoEngage sharedInstance]trackEvent:@"Parking" builderPayload:eventTracker];
     
     [[MoEngage sharedInstance] setUserMobileNo:@"1234567890"];
     [[MoEngage sharedInstance] setUserUniqueID:@"uniqueFromKiran"];
     //[[MoEngage sharedInstance] setUserEmailID:@"kiran.kumar@zify.co"];
     [[MoEngage sharedInstance] setUserAttribute:@"kiran.kumar@zify.co" forKey:USER_ATTRIBUTE_USER_EMAIL];
     */
    [[MoEngage sharedInstance] registerForRemoteNotificationWithCategories:nil andCategoriesForPreviousVersions:nil andWithUserNotificationCenterDelegate:self];
    [[MoEngage sharedInstance] setFlushInterval:5];
    [[MoEngage sharedInstance] syncNow];
    //For tracking if it's a new install or on update by user
    
}
-(void)addONeSignalMethodInitializersWithApplication:(UIApplication *)application andWithLaunchOptions:(NSDictionary *)launchOptions{
    id notificationReceiverBlock = ^(OSNotification *notification) {
        if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
            [self handlePushNotificationOnReciverBlock:notification];
        }};
    id notificationOpenedBlock = ^(OSNotificationOpenedResult *result) {
        [self handlePushNotificationOnOpenedBlock:result];
    };
    id onesignalInitSettings = @{kOSSettingsKeyAutoPrompt : @NO};
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:@"25c3787e-3507-4dc4-9216-7939d5c2d80b"
          handleNotificationReceived:notificationReceiverBlock
            handleNotificationAction:notificationOpenedBlock
                            settings:onesignalInitSettings];
    [OneSignal addSubscriptionObserver:self];
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNone;
}
// Add this new method
- (void)onOSSubscriptionChanged:(OSSubscriptionStateChanges*)stateChanges {
    if (!stateChanges.from.subscribed && stateChanges.to.subscribed) {
        NSLog(@"stateChanges.to.userId is %@",stateChanges.to.userId);
        NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
        OneSignalPlayerid* obj = [OneSignalPlayerid getPlayerIdObjForDeviceInContext:context];
        obj.playerId = stateChanges.to.userId;
        [OneSignalPlayerid updatePlayerIdAfterReceived:obj];
        [[PushNotificationManager sharedInstance] handlePushDeviceToken:obj.playerId];
    }
    // NSLog(@"SubscriptionStateChanges:\n%@", stateChanges);
}

-(void)handlePushNotificationOnReciverBlock:(OSNotification *)notification{
    //int notificationType = [[notification.payload.additionalData objectForKey:@"notification_type"] intValue];
    //NSLog(@"notification payload is %@", notification.payload.additionalData);
    NSDictionary *payLoadDict = [[NSDictionary alloc] initWithObjectsAndKeys:notification.payload.additionalData,@"payload", nil];
    for (UIView *subview in _window.subviews){ // for removing exised push notification view if any
        if([subview isKindOfClass:[PushNotificationView class]] || [subview isKindOfClass:[PushOfferView class]] || [subview isKindOfClass:[PushAppUpgradeView class]]){
            [subview removeFromSuperview];
        }
        [[PushNotificationManager sharedInstance] handleNotificationData:payLoadDict];
        [self clearNotifications];
    }
}
-(void)handlePushNotificationOnOpenedBlock:(OSNotificationOpenedResult *)result{
    UserProfile *currentUser = [UserProfile getCurrentUser];
    // This block gets called when the user reacts to a notification received
    OSNotificationPayload* payload = result.notification.payload;
    for (UIView *subview in _window.subviews){ // for removing exised push notification view if any
        if([subview isKindOfClass:[PushNotificationView class]] || [subview isKindOfClass:[PushOfferView class]] || [subview isKindOfClass:[PushAppUpgradeView class]]){
            [subview removeFromSuperview];
        }
    }
    NSDictionary *payLoadDict = [[NSDictionary alloc] initWithObjectsAndKeys:payload.additionalData,@"payload", nil];
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
        return;
    }
    if(result.action.actionID){
        if(currentUser){
            [[PushNotificationManager sharedInstance] handleUserSelectedAction:result.action.actionID andUserInfo:payLoadDict];
        }
    }else{
        [[PushNotificationManager sharedInstance] handleNotificationData:payLoadDict];
    }
    // [PushNotificationManager sharedInstance].notificationData = nil;
    [self clearNotifications];
    
}
- (void) clearNotifications {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

-(void)addFreshChatIntegration:(NSDictionary *)launchOptions withApplication:(UIApplication *)application{
    /* Initialize Freshchat*/
    FreshchatConfig *config = [[FreshchatConfig alloc]initWithAppID:@"7bc9144b-5e8e-4dc2-9d08-bb649311dd29"  andAppKey:@"ba968f83-5500-4373-86ac-aa9e0afebae7"];
    // FreshchatConfig *config = [[FreshchatConfig alloc] initWithAppID:@"1bea9f2c-dc92-4876-b24d-57e9e9bbfc9c" andAppKey:@"3962d5d2-58e9-4c56-b319-e32d40ce9f65"];
    [[Freshchat sharedInstance] initWithConfig:config];
    
    /* Enable remote notifications */
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    else{
        
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        
    }
    
    
    /*  Set your view before the following snippet executes */
    
    /* Handle remote notifications */
    if ([[Freshchat sharedInstance]isFreshchatNotification:launchOptions]) {
        [[Freshchat sharedInstance]handleRemoteNotification:launchOptions
                                                andAppstate:application.applicationState];
        
        //  NSString *string = [NSString stringWithFormat:@"my dictionary is %@", launchOptions];
        //  NSLog(@"received notification is %@", string);
        // [UIAlertController showErrorWithMessage:string inViewController:_window];
        
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    
    NSLog(@"notification is %@", notification.request.content.userInfo);
    UserProfile *profile = [UserProfile getCurrentUser];
    if(profile == nil){
        return;
    }
    
    if ([[Freshchat sharedInstance]isFreshchatNotification:notification.request.content.userInfo]) {
        [[Freshchat sharedInstance]handleRemoteNotification:notification.request.content.userInfo andAppstate:[[UIApplication sharedApplication] applicationState]];
    } else {
        
        if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
            
            NSDictionary *userInfo = notification.request.content.userInfo;
            //  NSLog(@"action block will be called %@", userInfo);
            if([[userInfo allKeys] containsObject:@"dialog_id"]){
                [self showChatCustomBannerIfAppIsinForeground:userInfo];
            }
            if([[userInfo allKeys] containsObject:@"notificationType"]){
                NSString *type = [userInfo objectForKey:@"notificationType"];
                if([type isEqualToString:@"DriveAlarm"]){
                    [[SDLConnection sharedManager] showAlertForStartTripWithTripInfo:userInfo];
                    completionHandler( UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge );
                }
            }
            
        }else{
            completionHandler( UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge );
        }
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler {
    
    UserProfile *profile = [UserProfile getCurrentUser];
    if(profile == nil){
        return;
    }
    if ([[Freshchat sharedInstance]isFreshchatNotification:response.notification.request.content.userInfo]) {
        [[Freshchat sharedInstance]handleRemoteNotification:response.notification.request.content.userInfo andAppstate:[[UIApplication sharedApplication] applicationState]];
    } else {
        
        if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
            NSDictionary *userInfo = response.notification.request.content.userInfo;
            if([[userInfo allKeys] containsObject:@"dialog_id"]){
                //  [self showChatCustomBannerIfAppIsinForeground:userInfo];
                
            }
        }else{
            
            NSDictionary *userInfo = response.notification.request.content.userInfo;
            NSLog(@"action block will be called %@", userInfo);
            if([[userInfo allKeys] containsObject:@"dialog_id"]){ /// push for Chat
                //   self.chatuserIdFromPush = [[userInfo valueForKey:@"user_id"] stringValue];
                UIViewController *visibleController = [AppUtilites applicationVisibleViewController];
                UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
                if(rootViewController.presentedViewController != nil){
                    [rootViewController dismissViewControllerAnimated:NO completion:nil];
                    visibleController = [UIApplication sharedApplication].keyWindow.rootViewController;
                    [self setReceivingUserFromPushInChatList:userInfo withViewController:visibleController];
                    // [visibleController presentViewController:[ChatUserListController createChatNavController] animated:YES completion:nil];
                }else{
                    [self setReceivingUserFromPushInChatList:userInfo withViewController:visibleController];
                    // [visibleController presentViewController:[ChatUserListController createChatNavController] animated:YES completion:nil];
                }
            }
            completionHandler();
        }
    }
}

-(void)showChatCustomBannerIfAppIsinForeground:(NSDictionary *)userInfo{
    UserContactDetail *detail = [self getDataFromChatPush:userInfo];
    NSString *msgReceived = [userInfo objectForKey:@"message"];
    [[ChatManager sharedInstance] displayCustomNotificationBanner:detail withMessage:msgReceived];
}
-(void)setReceivingUserFromPushInChatList:(NSDictionary *)chatPush withViewController:(UIViewController *)vc{
    UINavigationController *chatNavController = [ChatUserListController createChatNavController];
    ChatUserListController *chatUserListContoller = (ChatUserListController *)[chatNavController topViewController];
    UserContactDetail *detail = [self getDataFromChatPush:chatPush];
    chatUserListContoller.receivingUser = detail;
    [vc presentViewController:chatNavController animated:YES completion:nil];
    
}

-(UserContactDetail *)getDataFromChatPush:(NSDictionary *)chatPush{
    NSString *chatIdentifier = [chatPush objectForKey:@"dialog_id"];
    NSString *firstName = [chatPush objectForKey:@"fName"];
    NSString *lastName = [chatPush objectForKey:@"lName"];
    NSString *imageUrl = [chatPush objectForKey:@"image"];
    return [[UserContactDetail alloc] initWithChatIdentifier:chatIdentifier andCallIdentifier:nil andFirstName:firstName andLastName:lastName andImageUrl:imageUrl];
}

-(NSString *)convertDOBFROMServiceTOReqFormat{
    UserProfile *profile = [UserProfile getCurrentUser];
    NSString *dobStr = profile.dob;
    return dobStr;
    NSDateFormatter *dateFormatter;
    NSDateFormatter *dateTimeFormatter;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:locale];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
    [dateFormatter setLocale:locale];
    NSDate *reqDate = [dateFormatter dateFromString:dobStr];
    dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"MMM dd, yyyy"];
    // [dateTimeFormatter setDateFormat:@"dd MMM yyyy"];
    [dateTimeFormatter setLocale:locale];
    NSString *dobStrInReqFormat = [dateTimeFormatter stringFromDate:reqDate];
    return dobStrInReqFormat;
}

- (void)shareDriveDeepLinkIdWithFriends:(NSString *)driveDeepLinkUrl fromScreen:(UIViewController *)vc {
    if(driveDeepLinkUrl.length == 0){
        return;
    }
    NSString *shareLink = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(ride_with_me_share_text, nil),  driveDeepLinkUrl];
    NSArray *activityItems = @[shareLink];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    [activityViewControntroller setValue:NSLocalizedString(MS_CREATERIDE_NAV_TITLE_FOR_RIDE_WITH_ME, nil) forKey:@"Subject"];
    [vc presentViewController:activityViewControntroller animated:true completion:nil];
}

-(void)setUpTrueCallerSDK{
    if ([[TCTrueSDK sharedManager] isSupported]) {
        [[TCTrueSDK sharedManager] setupWithAppKey:TRUE_CALLER_APP_KEY appLink:TRUE_CALLER_APP_LINK];
    }
}

-(void)deleteNsUserDefaults{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [userDefaults dictionaryRepresentation];
    for (id key in dict) {
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
}

-(NSString *)displayTripAmountInCommaSeparator:(id)numberStr{
    
    NSNumber *numberRequired;
    if( [numberStr isKindOfClass:[NSString class]]){
        double doublValueinStr = [numberStr doubleValue];
        numberRequired = [NSNumber numberWithDouble:doublValueinStr];
        
    }else if([numberStr isKindOfClass:[NSNumber class]]){
        numberRequired = (NSNumber *)numberStr;
    }
    if(numberRequired == nil){
        return (NSString *)numberStr;
    }
    
    //NSString *localIdentifer = [NSLocale currentLocale].localeIdentifier;
    NSString *localIdentifer1 = [[CurrentLocale sharedInstance] getLocaleString];
    //NSLog(@"identifier is==%@== identifier111 ==%@==", localIdentifer,localIdentifer1);
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:localIdentifer1];
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
    [frmtr setLocale:locale];
    [frmtr setNumberStyle:NSNumberFormatterCurrencyStyle]; // this line is important!
    NSString *commaString = [frmtr stringFromNumber:numberRequired];
    // NSLog(@"formatted number is %@", commaString);
    return commaString;
}
-(void)showViewForIdCardUpload{
    NSArray *viewsArray = [[NSBundle mainBundle] loadNibNamed:@"IDCardViews" owner:self options:nil];
    IDUploadCompulseryView *uploadView;
    for (id object in viewsArray) {
        if ([object isKindOfClass:[IDUploadCompulseryView class]]) {
            uploadView = (IDUploadCompulseryView *)object;
            [uploadView setAttributesAndFontForControls];
            break;
        }
    }
    [[AppDelegate getAppDelegateInstance].window addSubview:uploadView];
    uploadView.translatesAutoresizingMaskIntoConstraints = false;
    NSDictionary *viewsDictionary = @{@"uploadView":uploadView};
    NSArray *horizontalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"|[uploadView]|" options:0 metrics:nil views:viewsDictionary];
    NSArray *verticalConsts = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[uploadView]|" options:0 metrics:nil views:viewsDictionary];
    [[AppDelegate getAppDelegateInstance].window addConstraints:horizontalConsts];
    [[AppDelegate getAppDelegateInstance].window addConstraints:verticalConsts];
    
    
    // [idUploadView addDropShadow];
}

-(void) showIDCardUploadScreen {
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref synchronize];
    UIViewController *topVc = [[AppDelegate getAppDelegateInstance] topMostController];
    //   UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"IDCard" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"UserIDCardController"];
    [pref setBool:FALSE forKey:@"forPush"];
    [pref setBool:YES forKey:@"forPresent"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [nav.navigationBar setBarTintColor:[UIColor colorWithRed:38/255 green:98/255 blue:255/255 alpha:1]];
    [nav.navigationBar setTintColor:[UIColor whiteColor]];
    [nav.navigationBar setTranslucent:NO];
    [topVc presentViewController:nav animated:YES completion:nil];
    /* if(rootViewController.presentedViewController != nil){
     [pref setBool:YES forKey:@"forPush"];
     [pref setBool:FALSE forKey:@"forPresent"];
     [vc.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:38/255 green:98/255 blue:255/255 alpha:1]];
     [vc.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
     [topVc.navigationController pushViewController:vc animated:TRUE];
     }else{
     [pref setBool:FALSE forKey:@"forPush"];
     [pref setBool:YES forKey:@"forPresent"];
     UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
     [nav.navigationBar setBarTintColor:[UIColor colorWithRed:38/255 green:98/255 blue:255/255 alpha:1]];
     [nav.navigationBar setTintColor:[UIColor whiteColor]];
     [topVc presentViewController:nav animated:YES completion:nil];
     }*/
    [pref synchronize];
}

#pragma mark - Firebase Remote Config

- (void)remoteConfigValuesFetched {
    long expirationDuration = 300;
    // If your app is using developer mode, expirationDuration is set to 0, so each fetch will retrieve values from the Remote Config service.
    if (self.remoteConfig.configSettings.isDeveloperModeEnabled) {
        expirationDuration = 0;
    }
    [self.remoteConfig fetchWithExpirationDuration:expirationDuration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
        if (status == FIRRemoteConfigFetchStatusSuccess) {
            [self.remoteConfig activateFetched];
            
            NSLog(@"welcome_message keys ---%@---", [[self remoteConfig] configValueForKey:@"services_enabled_countries_list"].stringValue);
            NSString *countriesListStr = [[self remoteConfig] configValueForKey:@"services_enabled_countries_list"].stringValue;
            NSData *data = [countriesListStr dataUsingEncoding:NSUTF8StringEncoding];
            NSMutableArray *arrCountries = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            if(arrCountries.count > 0){
                self.servicesCountriesArray = [[NSArray alloc] initWithArray:arrCountries];
            }else{
                self.servicesCountriesArray = [[NSArray alloc]init];
            }
            /* NSLog(@"Config fetched!");
             NSLog(@"all keys ---%@---", [[self remoteConfig] keysWithPrefix:@""]);
             NSLog(@"welcome_message keys ---%@---", [[self remoteConfig] configValueForKey:@"welcome_message"]);
             NSLog(@"my updated json dictionary ---%@---", [[self remoteConfig] configValueForKey:@"my_updated_json"]);
             
             NSLog(@"is_version_update_available ---%@---", self.remoteConfig[@"is_version_update_available"].stringValue);
             NSLog(@"app_version ---%@---", self.remoteConfig[@"app_version"].stringValue);
             NSLog(@"is_force_update_require ---%@---", self.remoteConfig[@"is_force_update_require"].stringValue);
             NSLog(@"version_update_dialog_title ---%@---", self.remoteConfig[@"version_update_dialog_title"].stringValue);
             NSLog(@"version_update_dialog_message_ios ---%@---", self.remoteConfig[@"version_update_dialog_message_ios"].stringValue);
             
             NSLog(@"--CFBUndle Version--%@---", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]);
             */
            
        } else {
            NSLog(@"Config not fetched --- Load from the RemoteConfigDefaults.plist");
            NSLog(@"Error %@", error.localizedDescription);
        }
    }];
}

- (void)isShowUpdatedAlert {
    NSString *strAppVersionFromRemoteConfig = self.remoteConfig[@"app_version"].stringValue;
    BOOL isVersionUpdateAvailable = self.remoteConfig[@"is_version_update_available"].boolValue;
    BOOL isForceUpdateRequire = self.remoteConfig[@"is_force_update_require"].boolValue;
    NSLog(@"is_force_update_require ---%@---", self.remoteConfig[@"is_force_update_require"].stringValue);
    //Check is Version update Available
    if (isVersionUpdateAvailable == true) {
        //Check appversion is greater than or not
        NSString *strCFBundleVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        //NSString *strAppVer = [NSString stringWithString:strAppVersionFromRemoteConfig];
        
        //  NSLog(@"-__app_verison---%@--cfbundleversion--%@---", strAppVer, strCFBundleVersion);
        
        if ([strAppVersionFromRemoteConfig compare:strCFBundleVersion options:NSNumericSearch] == NSOrderedDescending) {
            // strAppVersionFromRemoteConfig is lower than the strCFBundleVersion
            //Check is Force update available or not, if not then show general update alert
            if (isForceUpdateRequire == true) {
                [self showAppUpdateAlertType:@"force"];
            } else if (isForceUpdateRequire == false) {
                [self showAppUpdateAlertType:@"general"];
            }
        } else {
            NSLog(@"-Equal version no need to update------");
        }
    } else {
        // do not show any update alert
        //because the value of isVersionUpdateAvailable == false
    }
}

-(void)loadImageOfferSplashScreen:(NSString *)strOfferImgUrl isShowUpdatedAlert:(BOOL)isShowAppUpdateAlert {
    NSString *imgUrl = strOfferImgUrl;
    NSString *strWelcomeMessage = @"";
    NSLog(@"Iage URL---%@--", imgUrl);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ReferAndEarn" bundle:[NSBundle mainBundle]];
    SplashViewController *splashViewController = [storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
    UIViewController *topVc = [self topMostController];
    
    [SDWebImageDownloader.sharedDownloader  downloadImageWithURL:[NSURL URLWithString:imgUrl] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (image) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                NSLog(@"Iage Avialbel");
                
                splashViewController.imgSplashTemp = image;
                splashViewController.strTemp = @"";
                
                [topVc addChildViewController:splashViewController];
                splashViewController.view.bounds = [AppDelegate getAppDelegateInstance].window.bounds; //topVc.view.bounds;
                [topVc.view addSubview:splashViewController.view];
                [splashViewController didMoveToParentViewController:topVc];
                //  [splashViewController.view setBackgroundColor:[UIColor grayColor]];
                if (isShowAppUpdateAlert == true) {
                    [[AppDelegate getAppDelegateInstance] isShowUpdatedAlert];
                }
            });
            
            /*  if(topVc.presentedViewController){
             [topVc dismissViewControllerAnimated:YES completion:nil];
             }
             [topVc presentViewController:splashViewController animated:YES completion:^{
             if (isShowAppUpdateAlert == true) {
             [[AppDelegate getAppDelegateInstance] isShowUpdatedAlert];
             }
             }];*/
        } else  {
            NSLog(@"Splash Image getting error");
        }
    }];
}

- (void)showAppUpdateAlertType:(NSString *)strAlertType {
    
    // UIViewController *topVc = [self topMostController];
    UIViewController *visibleController = [self topMostController];//[AppUtilites applicationVisibleViewController];
    if ([visibleController isKindOfClass:[UIAlertController class]]){
        UIAlertController *alertVc = (UIAlertController *)visibleController;
        if(alertVc.view.tag == 1000){
            [alertVc dismissViewControllerAnimated:NO completion:nil];
        }
    }
    
    NSString *strAlertTitle = self.remoteConfig[@"version_update_dialog_title"].stringValue;
    NSString *strAlertDescription = self.remoteConfig[@"version_update_dialog_message_ios"].stringValue;
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:strAlertTitle
                                                                   message:strAlertDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* upgradeBtnAction = [UIAlertAction actionWithTitle:NSLocalizedString(PUSXH_APPUPGRADE_BTN_OPENAPPSTORE, nil) style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 [alert dismissViewControllerAnimated:NO completion:nil];
                                                                 [self openUrlToAppStore];
                                                             }];
    
    
    [alert addAction:upgradeBtnAction];
    
    if (![strAlertType isEqualToString:@"force"]) {
        UIAlertAction* cancelBtnAction = [UIAlertAction actionWithTitle:NSLocalizedString(PUSXH_APPUPGRADE_BTN_CANCEL, nil) style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action) {
                                                                    [alert dismissViewControllerAnimated:NO completion:nil];
                                                                    
                                                                }];
        alert.view.tag = 1000;
        [alert addAction:cancelBtnAction];
    }
    visibleController = [self topMostController];//[AppUtilites applicationVisibleViewController];
    [visibleController presentViewController:alert animated:YES completion:nil];
    
    
    /*  UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:strAlertTitle WithMessage:strAlertDescription];
     
     if ([strAlertType isEqualToString:@"force"]) {
     
     [alert addButton:BUTTON_OK WithTitle:@"Upgrade" WithAction:^(void *action) {
     [self openUrlToAppStore];
     }];
     } else if ([strAlertType isEqualToString:@"general"]) {
     [alert addButton:BUTTON_OK WithTitle:@"Upgrade" WithAction:^(void *action) {
     [self openUrlToAppStore];
     }];
     [alert addButton:BUTTON_CANCEL WithTitle:@"Cancel" WithAction:^(void *action) {
     
     }];
     }*/
    // [alert showInViewController:[AppDelegate getAppDelegateInstance].window.rootViewController];
}

- (void)openUrlToAppStore {
    NSString *storedURL = @"https://itunes.apple.com/in/app/zify-home-office-carpool/id1003822874?mt=8";
    NSString *urlstring = [NSString stringWithFormat:@"%@",storedURL];
    NSURL *url = [NSURL URLWithString:urlstring];
    NSLog(@"url = %@",url);
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
        NSLog(@"success open url");
    }];
}

-(void)callApiForAcceptRideRequestFromPushNotification{
    if(self.notificationInfoFromPush == nil){
        return;
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isFromPush = [prefs boolForKey:@"IdFromPush"];
    if(isFromPush){
        [prefs setBool:NO forKey:@"IdFromPush"];
        [prefs synchronize];
        
        UserProfile *userProfile = [UserProfile getCurrentUser];
        UserDocuments *userDocuments = userProfile.userDocuments;
        if(![userDocuments.isIdCardDocUploaded intValue]){
            self.notificationInfoFromPush = nil;
            return;
        }
        PushRideRequest *rideRequest = self.notificationInfoFromPush;
        [[ServerInterface sharedInstance] getResponse:[[RideDriveActionRequest alloc] initWithRequestType:CONFIRMRIDE andRideId:rideRequest.tripDetails.rideId andDriveId:rideRequest.tripDetails.driveId andSeatsNum:@1] withHandler:^(ServerResponse *response, NSError *error){
            self.notificationInfoFromPush = nil;
            if(response){
                UIViewController *topVc = [[AppDelegate getAppDelegateInstance] topMostController];
                if([topVc isKindOfClass:[CurrentDriveController class]]){
                    SearchRideViewController *obj = (SearchRideViewController *)[AppDelegate getAppDelegateInstance].topNavController;
                    [CurrentRideLocationTracker sharedInstance].currentRideLocationTrackerDelegate = obj;
                    [[CurrentRideLocationTracker sharedInstance] updateCurrentLocation];
                    
                }else{
                    UniversalAlert *alert = [[UniversalAlert alloc] initWithTitle:NSLocalizedString(VC_PUSH_RIDEREQUEST_RIDEREQUEST, nil) WithMessage:NSLocalizedString(VC_PUSH_RIDEREQUEST_REQUESTACCEPTED,nil)];
                    [alert addButton:BUTTON_OK WithTitle:NSLocalizedString(CMON_GENERIC_OK, nil) WithAction:^(void *action){
                        
                    }];
                    [alert showInViewController:[AppUtilites applicationVisibleViewController]];
                }
            }
        }];
    }
}

-(void)setbackgroungdImageForNavigatuionBar:(UINavigationBar *)bar{
    UIImage *navigationBackgroundImage = [UIImage imageWithColor:[UIColor clearColor]];//[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0]];
    [bar setBackgroundImage:navigationBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [bar setShadowImage:[UIImage new]];
}


@end
