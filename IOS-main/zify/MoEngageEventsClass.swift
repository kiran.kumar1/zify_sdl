//
//  MoEngageEventsClass.swift
//  zify
//
//  Created by Anurag Rathor on 22/04/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit

let  USER_LOGIN = "USER_LOGIN"
let USER_AUTO_LOGIN = "USER_AUTO_LOGIN"
let USER_SIGN_UP_INIT = "USER_SIGN_UP_INIT"
let USER_SIGN_UP_COMPLETED = "USER_SIGN_UP_COMPLETED"
let MOBILE_VERIFIED = "MOBILE_VERIFIED"
let PROFILE_PIC_UPLOADED = "PROFILE_PIC_UPLOADED"
let TRAVEL_PREF_COMPLETED = "TRAVEL_PREF_COMPLETED"
let ID_CARD_UPLOADED = "ID_CARD_UPLOADED"
let RECHARGE_SUCCESS = "RECHARGE_SUCCESS"
let RECHARGE_FAILED = "RECHARGE_FAILED"
let WITHDRAWAL_REQUEST_INITIATED = "WITHDRAWAL_REQUEST_INITIATED"
let WITHDRAWAL_REQUEST_COMPLETED = "WITHDRAWAL_REQUEST_COMPLETED"
let LR_REEDEM_REQUEST_INITIATED = "LR_REEDEM_REQUEST_INITIATED"

let RIDE_SEARCH_RESULTS_NOT_FOUND = "RIDE_SEARCH_RESULTS_NOT_FOUND"
let RIDE_SEARCH_EVENT = "RIDE_SEARCH_EVENT"
let RIDE_SEARCH_RESULTS_FOUND = "RIDE_SEARCH_RESULTS_FOUND"

let RIDE_PRE_REQUEST = "RIDE_PRE_REQUEST"
let RIDE_REQUEST_SENT = "RIDE_REQUEST_SENT"
let RIDE_LOW_BALANCE_EVENT = "RIDE_LOW_BALANCE_EVENT"
let RIDE_REQUEST_ACCEPT_EVENT = "RIDE_REQUEST_ACCEPT_EVENT"
let RIDE_REQUEST_DECLINE_EVENT = "RIDE_REQUEST_DECLINE_EVENT"

let REFER_YOUR_ORGANIZATION = "REFER_YOUR_ORGANIZATION"
let DRIVE_START_EVENT = "DRIVE_START"
let RIDE_BOARD_EVENT = "RIDE_BOARD"
let RIDE_COMPLETED_EVENT = "RIDE_COMPLETED"
let DRIVE_COMPLETED_EVENT = "DRIVE_COMPLETED"
let DRIVE_DELETED_EVENT = "DRIVE_DELETED"
let RIDE_CANCELLED_EVENT = "RIDE_CANCELLED"
let REFER_INVITE_EVENT = "REFER_INVITE"



class MoEngageEventsClass: NSObject {
    
   
    class func setUserValuesToMoengageDashboard(){
        let userProfile = UserProfile.getCurrentUser()
        DispatchQueue.main.async {

     //   let userDictionary = ["email":userProfile?.emailId, "userId":userProfile?.userId] as [String : Any]
        MoEngage.sharedInstance().setUserUniqueID(userProfile?.userId)
        MoEngage.sharedInstance().setUserName(userProfile?.userName)
        MoEngage.sharedInstance().setUserEmailID(userProfile?.emailId)
        MoEngage.sharedInstance().setUserFirstName(userProfile?.firstName)
        MoEngage.sharedInstance().setUserLastName(userProfile?.lastName)
       // MoEngage.sharedInstance().setUserAttribute(userProfile?.dob, forKey: "dob")
        
        MoEngage.sharedInstance().setUserAttribute(userProfile?.country, forKey: "countryCode")
        MoEngage.sharedInstance().setUserAttribute(userProfile?.companyName, forKey: "Company Name")
        MoEngage.sharedInstance().setUserAttribute(userProfile?.isdCode, forKey: "isdCode")
        MoEngage.sharedInstance().setUserAttribute(userProfile?.companyEmail, forKey: "Company Email")
        MoEngage.sharedInstance().setUserAttribute(userProfile?.userType, forKey: "User Mode")
        MoEngage.sharedInstance().setUserAttribute(userProfile?.emailId, forKey: "customer_id")
        }
        

      /*  if let gender = userProfile?.gender {
            MoEngage.sharedInstance().setUserGender()
        }else{
            MoEngage.sharedInstance().setUserGender("")
        }
       MoEngage.sharedInstance().setUserDateOfBirth(<#T##date: Date?##Date?#>)
*/
    
    }
    
    class func callLoginNormalEvent(){
        DispatchQueue.main.async {

        let userInfoDict = NSMutableDictionary()
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(USER_LOGIN, andPayload: userInfoDict)
        MoEngageEventsClass.setUserValuesToMoengageDashboard()
        }
    }
    
    class func callAutoLoginEvent(){
        DispatchQueue.main.async {

        let userInfoDict = NSMutableDictionary()
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(USER_AUTO_LOGIN, andPayload: userInfoDict)
        MoEngageEventsClass.setUserValuesToMoengageDashboard()
        }
    }
    
    class func callSignUPInitiateEvent(withFirstname:String, andLastName:String){
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary()
        userInfoDict.setObject(withFirstname, forKey: "first_name" as NSCopying)
        userInfoDict.setObject(andLastName, forKey: "last_name" as NSCopying)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(USER_SIGN_UP_INIT, andPayload: userInfoDict )
        }
    }
    
    class func callSignUPCompleteEvent(){
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary()
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(USER_SIGN_UP_COMPLETED, andPayload: userInfoDict )
        MoEngageEventsClass.setUserValuesToMoengageDashboard()
        }
    }
    
    class func setDeafultValuesToEvent(userInfoDict:NSMutableDictionary){
        DispatchQueue.main.async {

        let currentTimeStamp = MoEngageEventsClass.getCurrentDateAndTime()
        userInfoDict.setObject(currentTimeStamp, forKey: "timestamp" as NSCopying)
       // print("moengage data info is \(userInfoDict)")
        }
    }
    
    class func callMobileVerifiedEvent(){
        DispatchQueue.main.async {

        let userInfoDict = NSMutableDictionary()
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(MOBILE_VERIFIED, andPayload: userInfoDict )
        }
    }
    
    class func callProfilePICUploadedEvent(){
        DispatchQueue.main.async {

        let userInfoDict = NSMutableDictionary()
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(PROFILE_PIC_UPLOADED, andPayload: userInfoDict )
        }
    }
    
    class func callTPSaveEvent(){
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary()
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(TRAVEL_PREF_COMPLETED, andPayload: userInfoDict )
        }
    }
    
    class func callIDCardUploadedEvent(){
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary()
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(ID_CARD_UPLOADED, andPayload: userInfoDict )
        }
    }
    
    class func callRechargeSuccessEvent(withDataDict:Dictionary<String,Any>){
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RECHARGE_SUCCESS, andPayload: userInfoDict )
        }
    }
    
    class func callRechargeFailEvent(withDataDict:Dictionary<String, Any>){
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RECHARGE_FAILED, andPayload: userInfoDict )
        }
    }
    
    
    class func callLRRedeemRequestEvent(withPoints:Int) {
        DispatchQueue.main.async {
        
        let userInfoDict = NSMutableDictionary.init()
        userInfoDict.setValue(withPoints, forKey: "redeem_coins")
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(LR_REEDEM_REQUEST_INITIATED, andPayload: userInfoDict )
        }
        
    }
    
    class func callLRRedeemProcessCompletedEvent() {
        
    }
    
    class func callReferInviteEvent() {
        
    }
    
    class func callRefereeJoinedEvent() {
        
    }
    
    class func callReferYourOrganisationEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(REFER_YOUR_ORGANIZATION, andPayload: userInfoDict )
        }
    }
    
    class func callRideSearchEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_SEARCH_EVENT, andPayload: userInfoDict )
        }
    }
    
    class func callRideSearchResultsFoundEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_SEARCH_RESULTS_FOUND, andPayload: userInfoDict )
        }
    }
    
    class func callRideSearchNotFoundEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_SEARCH_RESULTS_NOT_FOUND, andPayload: userInfoDict )
        }
    }
    
    class func callPreRideRequestEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_PRE_REQUEST, andPayload: userInfoDict)
        }
    }
    
    class func callRideRequestEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_REQUEST_SENT, andPayload: userInfoDict)
        }
    }
    
    class func callRideRequestLowBalanceEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_LOW_BALANCE_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callRideRequestAcceptEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_REQUEST_ACCEPT_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callRideRequestDeclineEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_REQUEST_DECLINE_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callDriveStartEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(DRIVE_START_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callRideBoardEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_BOARD_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callRideCompletedEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_COMPLETED_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callDriveCompletedEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(DRIVE_COMPLETED_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callDriveDeleteEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(DRIVE_DELETED_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callRideCancelEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(RIDE_CANCELLED_EVENT, andPayload: userInfoDict)
        }
    }
    
    class func callAccountWithdrawRequestInitiatedEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(WITHDRAWAL_REQUEST_INITIATED, andPayload: userInfoDict)
        }
    }
    
    
    class func callReferInviteEvent(withDataDict:Dictionary<String, Any>) {
        DispatchQueue.main.async {
        let userInfoDict = NSMutableDictionary.init(dictionary: withDataDict)
        MoEngageEventsClass.setDeafultValuesToEvent(userInfoDict: userInfoDict)
        MoEngage.sharedInstance().trackEvent(REFER_INVITE_EVENT, andPayload: userInfoDict)
        }
    }
    
    
    class func getCurrentDateAndTime() -> String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let dateStr = formatter.string(from: NSDate() as Date)
        return AppDelegate.getInstance().converDate(toISOStandard: dateStr) ?? ""
      //  onverDateToISOStandard:(NSString *)choosenDateInStrFormat{
      //      NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
      //      dateFormatter.dateFormat = @"";
    
    }

}
