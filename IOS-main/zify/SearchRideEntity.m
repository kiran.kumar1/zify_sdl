//
//  SearchRideEntity.m
//  zify
//
//  Created by Anurag Rathor on 23/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import "SearchRideEntity.h"

@implementation SearchRideEntity
-(id)initWithDictionary:(NSDictionary *)infoDict{
    if([self init]){
        _routeId = [self getValueForKey:@"routeId" fromDictionary:infoDict]; //[infoDict objectForKey:@"routeId"];
        
        _driverId = [infoDict objectForKey:@"driverId"];
        _driveId = [infoDict objectForKey:@"driveId"];
        _departureTime = [infoDict objectForKey:@"departureTime"];
        _srcAdd = [infoDict objectForKey:@"srcAdd"];
        _destAdd = [infoDict objectForKey:@"destAdd"];
        _numOfSeats = [infoDict objectForKey:@"numOfSeats"];
        _city = [infoDict objectForKey:@"city"];
        _destCity = [infoDict objectForKey:@"destCity"];
        _availableSeats = [infoDict objectForKey:@"availableSeats"];
        _amountPerSeat = [infoDict objectForKey:@"amountPerSeat"];
        _currency = [infoDict objectForKey:@"currency"];;
        _distance = [infoDict objectForKey:@"distance"];;
        _distanceUnit = [infoDict objectForKey:@"distanceUnit"];;
        _srcLat = [infoDict objectForKey:@"srcLat"];;
        _srcLong = [infoDict objectForKey:@"srcLong"];;
        _destLat = [infoDict objectForKey:@"destLat"];;
        _destLong = [infoDict objectForKey:@"destLong"];;
        _overviewPolylinePoints = [infoDict objectForKey:@"overviewPolylinePoints"];
        _isFavDrive = [infoDict objectForKey:@"isFavDrive"];
        _driverRouteId = [infoDict objectForKey:@"driverRouteId"];
        _isExpiredDrive = [infoDict objectForKey:@"isExpiredDrive"];
        _pickupDelta = [infoDict objectForKey:@"pickupDelta"];
        _dropDelta = [infoDict objectForKey:@"dropDelta"];
        _destNearLat = [infoDict objectForKey:@"destNearLat"];
        _destNearLong = [infoDict objectForKey:@"destNearLong"];
        _srcNearLat = [infoDict objectForKey:@"srcNearLat"];
        _srcNearLong = [infoDict objectForKey:@"srcNearLong"];
        _driverDetail = [[DriverDetailEntity alloc] initWithDictionary:[infoDict objectForKey:@"carOwnerDetails"]];
       
        
    }
    return self;
}
-(NSString *)getValueForKey:(NSString *)key fromDictionary:(NSDictionary *)infoDict{
    id keyValue = [infoDict objectForKey:key];
    if(keyValue == (NSString *)[NSNull null] || keyValue == nil){
        keyValue = @"";
    }
    return keyValue;
}

@end
