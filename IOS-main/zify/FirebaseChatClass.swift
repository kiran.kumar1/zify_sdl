//
//  FirebaseMainController.swift
//  zify
//
//  Created by Anurag on 05/08/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseAuth
import FirebaseCore
import FirebaseStorage
import FirebaseDatabase


class FirebaseChatClass: NSObject {
    
    
    static var firebaseInstance:FirebaseChatClass?
    

    var fcmTokenFromServer:String = ""
    
    class var sharedInstance: FirebaseChatClass  {
        if(firebaseInstance == nil){
             firebaseInstance = FirebaseChatClass()
        }
        return firebaseInstance!
    }
    
 
    
    func receivedFCMToken(token:String){
        self.fcmTokenFromServer = token
        let currentuser = UserProfile.getCurrentUser()
        if(currentuser == nil){
            return
        }
        if let userID = Auth.auth().currentUser?.uid{
            let userValues:[String:AnyObject] = ["deviceToken":self.fcmTokenFromServer as AnyObject]
            self.registerUserInDatabaseWithUid(uid: userID, values: userValues)
        }
    }
    
    func sendPushNotification(to token: String, title: String, body: String, dataDict:[String:String]) {
            let urlString = "https://fcm.googleapis.com/fcm/send"
            let url = NSURL(string: urlString)!
        let dialog_idStr = dataDict["dialog_id"] ?? ""

        let paramString: [String : Any] = ["to" : token,"notification" : ["title" : title, "body" : body, "sound":"default"],"data" : dataDict ,"dialog_id":dialog_idStr]
       /* let paramString: [String : Any] = ["to" : token,
                                           "data" : dataDict,
                                           "dialog_id":dialog_idStr
         ]*/
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAA9r4yMT8:APA91bElF_AQ5MnlsXZHBM436j89ybZwOCMuObQnbI3WRfYEHLktvjopMDqlsiMYZfy5Rs7xHFcV2O3neIss6sRZnpsFzb9lmE_glcZkLGCgf2_yehYRjIbof2OM2CGj9thrdaHR8yFJ2GuigYxmglF7Z7ZxKDEN3Q", forHTTPHeaderField: "Authorization")
            let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
                do {
                    if let jsonData = data {
                        if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                            NSLog("Received data:\n\(jsonDataDict))")
                        }
                    }
                } catch let err as NSError {
                    print(err.debugDescription)
                }
            }
            task.resume()
        }
    
    
    
    func signIn(userEmail : String, password : String, completion: @escaping (AuthDataResult) -> Void) {
        
        Auth.auth().signIn(withEmail: userEmail, password: password, completion: { (user, error) in
            if error != nil {
                return
            }
            guard (user?.user.uid) != nil else {
                return
            }
            if let uid = user?.user.uid {
                 let userValues:[String:AnyObject] = ["deviceToken":self.fcmTokenFromServer as AnyObject]
                self.registerUserInDatabaseWithUid(uid: uid, values: userValues)
            }

            completion(user!)
        })
    }
    
    func doSignUpIntoFireBAseChat(withUser:QBUUser) {
        
    }
    
    
    func signUp(user:QBUUser, completion: @escaping (AuthDataResult) -> Void) {
        
        let currentUser = user
        if let  email = user.email, let pwd = user.password{
            Auth.auth().createUser(withEmail: email, password: pwd) { ( user , error) in
            if error != nil {
                let objError = error! as NSError
                print(objError )
                print("errorroorr \(objError.userInfo)")
                let errorStr = objError.userInfo["FIRAuthErrorUserInfoNameKey"] as? String ?? ""
                if(errorStr == "ERROR_EMAIL_ALREADY_IN_USE"){
                    //sign In
                }else{
                }
                return
            }
            guard let uid = user?.user.uid else {
                return
            }
               // let userValues:[String:AnyObject] = ["email":email as AnyObject,"qbChatUserId":uid as AnyObject,"name":currentUser.fullName as AnyObject,"profileImageUrl":self.currentUserProfile?.profileImgUrl as AnyObject,"deviceToken":self.fcmTokenFromServer as AnyObject]
            let userValues:[String:AnyObject] = ["deviceToken":self.fcmTokenFromServer as AnyObject,"qbUserName":currentUser.login as AnyObject,"qbUserPassword":currentUser.password as AnyObject]
            self.registerUserInDatabaseWithUid(uid: uid, values: userValues)
            completion(user!)
            }
        }
    }
    
     func registerUserInDatabaseWithUid(uid: String, values: [String:AnyObject]) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        var values1 :[String :AnyObject] = values
        let currentUserProfile = UserProfile.getCurrentUser()

        if((currentUserProfile) != nil){
            var isGlobalBool:Bool = false
            if(currentUserProfile?.isGlobal == 1){
                isGlobalBool = true
            }
            var isGlobalPaymentBool:Bool = false
            if(currentUserProfile?.isGlobalPayment == 1){
                isGlobalPaymentBool = true
            }
           
            let messageCount = NSNumber.init(value: 0)
            var userValues :[String: AnyObject] = ["isGlobal":isGlobalBool as AnyObject,"isGlobalPayment":isGlobalPaymentBool as AnyObject, "messageCode":messageCount as AnyObject,"qbChatUserId":uid as AnyObject,"qbFirstName":(currentUserProfile?.firstName ?? "") as AnyObject, "qbLastName":(currentUserProfile?.lastName ?? "") as AnyObject, "qbUserEmail":(currentUserProfile?.emailId ?? "") as AnyObject,"qbProfilePicUrl":(currentUserProfile?.profileImgUrl ?? "") as AnyObject]
            
            
            if((currentUserProfile?.userChatProfile) != nil){
                let userValues1:[String:AnyObject] = ["qbUserName":currentUserProfile?.userChatProfile.chatUserName as AnyObject,"qbUserPassword":currentUserProfile?.userChatProfile.chatUserPassword as AnyObject]
                userValues = userValues.merged(with: userValues1)
            }else{
                let context = DBUserDataInterface.sharedInstance()?.sharedContext
                let chatProfile = UserChatProfile.getCurrentUserChatProfile(in: context) as? UserChatProfile
                let userValues1:[String:AnyObject] = ["qbUserName":chatProfile?.chatUserName as AnyObject,"qbUserPassword":chatProfile?.chatUserPassword as AnyObject]
                userValues = userValues.merged(with: userValues1)

            }
            
            values1 = values.merged(with: userValues)

            
        }
        
        let ref = Database.database().reference()
        let userRef = ref.child("Users").child(uid) // this helps to separate user based on uid
        userRef.updateChildValues(values1, withCompletionBlock: { (err, ref) in
            if err != nil {
                print(err ?? "Not Trackable Error")
                return
            }
        })
    }
    
    func getChatHistoryBasedOnUserId(completion: @escaping ([LastMessage]) -> Void){
        let userID = Auth.auth().currentUser!.uid
        let contactsSchemaRef = Database.database().reference().child("Contacts").child(userID)
        var messages = [LastMessage]()
        var countOfRows = 0
        contactsSchemaRef.observe(.value, with: { (snapshot) in
            guard let dict = snapshot.value as? [String: AnyObject] else {
                return
            }
            countOfRows = Int(snapshot.childrenCount)
            // let childrensArray = snapshot.children
           // print("messages are \(dict) and ID is \(snapshot.key),count is \(snapshot.childrenCount)")
            contactsSchemaRef.observe(.childAdded, with: { (snapshot) in
                guard let dict = snapshot.value as? [String: AnyObject] else {
                    return
                }
               // print("messages are \(dict) and ID is \(snapshot.key),count is \(snapshot.childrenCount)")
                let messageDict = dict["LastActivity"] as! [String : AnyObject]
                let message = LastMessage()
                message.id = snapshot.key
                message.setValuesForKeys(messageDict)
                messages.append(message)
                if (messages.count == countOfRows){
                    completion(messages)
                }
            })
            
        })
        
    }
    
    
    func getMessagesHistoryForOneToOne(otherPersonId:String, completion: @escaping ([Message]) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid  else {
            return
        }
        var messages = [Message]()
        let userMessageRef = Database.database().reference().child("Messages").child(uid).child(otherPersonId)
        
        
        userMessageRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dict = snapshot.value as? [String: AnyObject] else {
                completion(messages)
                return
            }
            let messagesCount = snapshot.childrenCount
           // print("snapchat value is\(dict) and count is \(snapshot.childrenCount)")
            userMessageRef.observe(.childAdded, with: { (snapshot) in
                guard let dict = snapshot.value as? [String: AnyObject] else {
                    completion(messages)
                    return
                }
                let message = Message()
                message.setValuesForKeys(dict)
                messages.append(message)
                if(messagesCount == messages.count){
                    completion(messages)
                }
                
            }, withCancel: nil)
        }, withCancel: nil)
        
    }
}
extension Dictionary {
    
    mutating func merge(with dictionary: Dictionary) {
        dictionary.forEach { updateValue($1, forKey: $0) }
    }
    
    func merged(with dictionary: Dictionary) -> Dictionary {
        var dict = self
        dict.merge(with: dictionary)
        return dict
    }
}
