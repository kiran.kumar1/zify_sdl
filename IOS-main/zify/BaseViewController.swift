//
//  BaseViewController.swift
//  zify
//
//  Created by Anurag S Rathor on 17/11/17.
//  Copyright © 2017 zify. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var bgView:UIView = UIView()
    var headerTitleLbl = UILabel()
    var headerSeparator = UILabel()
    var headerSubTitleLbl = UILabel()
    var bottomView:UIView = UIView()
    var btnSkip:UIButton = UIButton()
    var btnNext:UIButton = UIButton()
    var progressView:UIProgressView = UIProgressView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.green
        bgView = UIView.init(frame: self.view.frame)
        bgView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue:244.0/255.0, alpha: 1.0)
        self.view.addSubview(bgView);
        
        
        self.createHeaderSubViews()
        self.createBottomView()
        
        // Do any additional setup after loading the view.
    }
    
    func createHeaderSubViews() -> Void {
        
        var posX:CGFloat = 15;
        self.headerTitleLbl = UILabel.init(frame: CGRect(x: posX, y: 50, width: AppDelegate.screen_WIDTH() - 2*posX, height: 60))
        self.headerTitleLbl.textColor = UIColor.init(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0);
        self.headerTitleLbl.textAlignment = .center
        self.headerTitleLbl.numberOfLines = 0;
        self.bgView.addSubview(self.headerTitleLbl);
        
        
        self.headerSeparator = UILabel.init(frame: CGRect(x: self.headerTitleLbl.frameX + posX , y: self.headerTitleLbl.frameMaxY, width: self.headerTitleLbl.frameWidth - 2*posX, height: 1))
        self.headerSeparator.backgroundColor = self.headerTitleLbl.textColor
        self.headerSeparator.textAlignment = .center
        self.bgView.addSubview(self.headerSeparator);
        
        self.headerSubTitleLbl = UILabel.init(frame: self.headerTitleLbl.frame)
        self.headerSubTitleLbl.frameY = self.headerSeparator.frameMaxY
        self.headerSubTitleLbl.textAlignment = .center
        self.headerSubTitleLbl.numberOfLines = 0;
        self.headerSubTitleLbl.textColor = self.headerTitleLbl.textColor
        self.bgView.addSubview(self.headerSubTitleLbl);
        
        var titleFont:CGFloat = 15;
        self.headerTitleLbl.font = UIFont.init(name: NormalFont, size: titleFont);
        self.headerSubTitleLbl.font = self.headerTitleLbl.font

        
        
    }
    func createBottomView() -> Void{
        var bottomViewHeight:CGFloat = 50;
        let posX:CGFloat = 15;
        var buttonWidth:CGFloat = 100;
        var btnTitleFont:CGFloat = 17;
        if(AppDelegate.screen_HEIGHT() == 568){
            buttonWidth = 100;
            bottomViewHeight = 44;
            btnTitleFont = 15;
        }
        
        
        bottomView = UIView.init(frame: self.view.frame)
        bottomView.frameHeight = bottomViewHeight
        bottomView.frameY = bgView.frameHeight - bottomView.frameHeight;
        bottomView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 69.0/255.0, blue: 59.0/255.0, alpha: 1.0);
        bgView.addSubview(bottomView);
        
        
        
        btnSkip = UIButton.init(type: UIButtonType.custom);
        btnSkip.frame =  CGRect(x: posX, y: 0, width: buttonWidth, height: bottomView.frameHeight);
        btnSkip.backgroundColor = UIColor.clear;
        
        let stringSkip = NSLocalizedString(TP_SCREEN_BASE_SKIP, comment: "");
        let stringNext = NSLocalizedString(TP_SCREEN_BASE_NEXT, comment: "");
        btnSkip.setTitle(stringSkip, for:.normal)
        btnSkip.setTitle(stringSkip, for:.selected)
        btnSkip.setImage(UIImage.init(named: "left_arrow.png"), for: .normal)
        btnSkip.setImage(UIImage.init(named: "left_arrow.png"), for: .normal)
        btnSkip.titleLabel?.textColor = UIColor.white
        btnSkip.addTarget(self, action: #selector(btnSkipTapped), for: .touchUpInside)
        btnSkip.titleLabel?.font = UIFont.init(name: NormalFont, size: btnTitleFont)
        
        btnSkip.contentHorizontalAlignment = .left
        
        //        btnSkip.addTarget(self, action: "btnSkipTapped:", for: .touchUpInside)
        bottomView.addSubview(btnSkip);
        
        
        let rightImage:UIImage = UIImage.init(named: "right_arrow.png")!
        btnNext = UIButton.init(type: UIButtonType.custom);
        btnNext.frame = btnSkip.frame;
        btnNext.frameX = bottomView.frameWidth - btnNext.frameWidth - posX
        btnNext.backgroundColor = UIColor.clear;
        bottomView.addSubview(btnNext);
        btnNext.titleLabel?.textColor = UIColor.white
        btnNext.setTitle(stringNext, for:.normal)
        btnNext.setTitle(stringNext, for:.selected)
        btnNext.setImage(rightImage, for: .normal)
        btnNext.setImage(rightImage, for: .normal)
        btnNext.titleLabel?.font = btnSkip.titleLabel?.font
      //  btnNext.imageEdgeInsets = UIEdgeInsetsMake(0, 95, 0, 0);
      /*  btnNext.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        btnNext.titleLabel?.transform = CGAffineTransform(scaleX:-1.0, y:1.0);
        btnNext.imageView?.transform = CGAffineTransform(scaleX:-1.0, y:1.0);*/
        btnNext.contentHorizontalAlignment = .right
        
        btnNext.imageEdgeInsets = UIEdgeInsetsMake(0, btnNext.frame.size.width - (rightImage.size.width ), 0, 0);
        btnNext.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, rightImage.size.width + 1);

        
        progressView = UIProgressView.init(progressViewStyle: .bar) //UIProgressView(progressViewStyle: .bar)
        progressView.frame = CGRect(x: btnSkip.frameMaxX + 10, y: 10, width: btnNext.frameX - 10, height: bottomView.frameHeight);
        progressView.transform = CGAffineTransform.init(scaleX: 1, y: 5)
        progressView.frameY = (bottomView.frameHeight - progressView.frameHeight)/2 + 5
       // progressView.frameY = 0
        progressView.frameX = btnSkip.frameMaxX;
        progressView.frameWidth = btnNext.frameX - progressView.frameX
        progressView.setProgress(0.0, animated: true)
        progressView.trackTintColor = UIColor.red
        progressView.tintColor = UIColor.white
        bottomView.addSubview(progressView)
    }

    func btnSkipTapped(sender:UIButton!) {
        //self.navigationController?.popViewController(animated: true)
       // return
        print("skip Button Clicked")
        
        let alert = UIAlertController(title:NSLocalizedString(TP_SCREEN_BASE_ARE_YOU_SURE_TXT, comment: ""), message:NSLocalizedString(TP_SCREEN_BASE_CONTENT_MSG, comment: ""),  preferredStyle: UIAlertControllerStyle.alert)
       // let alert = UIAlertController(title: AppName, message: "Are you sure to skip this one?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(TP_SCREEN_BASE_NO_TITLE, comment: ""), style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction.init(title: NSLocalizedString(TP_SCREEN_BASE_YES_TITLE, comment: ""), style: .default, handler: { (alert: UIAlertAction!) in
            print("YEs  tapppppedddd...")
            self.moveToHOmeScreen()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func moveToHOmeScreen() -> Void{
        OnboardFlowHelper.sharedInstance().handleLoginFlow(self, andIsNewLogin: false)
        PushNotificationManager.sharedInstance().handlePushRegisterWithUser()
    }
    
   
    /*func btnNextTapped() {
        print("Next xxx Button Clicked")
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
