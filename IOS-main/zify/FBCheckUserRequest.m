//
//  FBUserLoginRequest.m
//  zify
//
//  Created by Anurag S Rathor on 10/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "FBCheckUserRequest.h"
#import "UserProfile.h"
#import "CurrentLocale.h"
#import "CurrentLocationTracker.h"

#define EMAIL_ID @"userEmail"
#define FB_PROFILE_ID @"fbProfileId"
#define FB_TOKEN @"fbaccesstoken"


@implementation FBCheckUserRequest
-(id)initWithEmail:(NSString *)email andFbId:(NSString *)fbId andFbToken:(NSString *)fbToken{
    self = [super init];
    if (self) {
        self.emailId = email;
        self.fbId = fbId;
        _fbToken = fbToken;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[EMAIL_ID] = _emailId;
    params[FB_PROFILE_ID] = _fbId;
    params[FB_TOKEN] = _fbToken;
    
    return params;
}
-(NSString *) serverURL{
    return @"/user/V2/checkfacebookuser";
}
-(NSString *) mappingKey{
    return @"user";
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL) saveInDatabase{
    return true;
}
-(BOOL)isForV2Version{
    return true;
}

-(BOOL)isSuccessResponseCode:(int)responseCode{
    return responseCode == 0 || responseCode == 1;
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    [UserProfile createCurrentUser:responseData];
}
@end
