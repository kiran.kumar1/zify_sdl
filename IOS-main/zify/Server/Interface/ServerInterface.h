//
//  ServerInterface.h
//  zify
//
//  Created by Anurag S Rathor on 06/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerResponse.h"
#import "HTTPClient.h"
#import  "ServerRequest.h"


@interface ServerInterface : NSObject
@property (strong, nonatomic, readonly) HTTPClient *httpClient;
+(ServerInterface *)sharedInstance;
-(void)getResponse:(ServerRequest *)request withHandler:(void (^)(ServerResponse *, NSError *))handler;
-(void)getResponseAsync:(ServerRequest *)request withHandler:(void (^)(ServerResponse *, NSError *))handler;
-(void)getResponse:(ServerRequest *)request withHandler:(void (^)(ServerResponse *, NSError *))handler andImage:(UIImage *)image;

-(BOOL)isConnectedToInternet;
@end
