//
//  ServerInterface.m
//  zify
//
//  Created by Anurag S Rathor on 06/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ServerInterface.h"
#import "Reachability.h"
#import "LocalisationConstants.h"
#import "AppDelegate.h"
#import "NSDictionary+UrlEncoding.h"


//LIVE ==> https://m.zify.co

/****/
#define SERVER_ADD @"https://m.zify.co/zify/zifyIndia/zifyApp"
#define SERVER_BASE_ADD @"http://cs.zify.co"
#define SERVER_BASE_URL_FOR_IMAGES_SERVICES @"https://m.zify.co"
#define PAYMENT_BANK_SERVER_ADD @"https://payment.zify.co/zifyPMT"
//#define PAYMENT_BANK_SERVER_ADD @"https://m.zify.co/zifyPMT/zifyPayment/V2"
#define PAYMENT_SERVER_ADD @"https://payment.zify.co/zifyPMT/zifyPayment/V2"
#define SERVER_ADD_CONTEXT @"/zify/zifyIndia/zifyApp"
#define SERVER_ADD_REFERANDEARN @"https://m.zify.co"
/****/

/*LOCAL = >http://192.168.0.122:8080**///now http://192.168.1.12:8080

/****
 #define SERVER_ADD @"http://test.zify.co"
 #define SERVER_BASE_ADD @"http://192.168.0.140:8080"
 #define SERVER_BASE_URL_FOR_IMAGES_SERVICES @"http://test.zify.co"
 #define PAYMENT_BANK_SERVER_ADD @"http://test.zify.co/zifyPMT"
 #define PAYMENT_SERVER_ADD @"http://test.zify.co/zifyPMT"
 #define SERVER_ADD_CONTEXT @"/zifydev"
 #define SERVER_ADD_REFERANDEARN @"http://test.zify.co"
****/

/****
 #define SERVER_ADD @"http://192.168.0.158:8080"
 #define SERVER_BASE_ADD @"http://192.168.0.158:8080"
 #define SERVER_BASE_URL_FOR_IMAGES_SERVICES @"http://192.168.0.158:8080"
 #define PAYMENT_BANK_SERVER_ADD @"http://192.168.0.158:8080/zifyPMT"
 #define PAYMENT_SERVER_ADD @"http://192.168.0.158:8080/zifyPMT"
 #define SERVER_ADD_CONTEXT @"/zifydev"
 #define SERVER_ADD_REFERANDEARN @"http://192.168.0.158:8080"
 
 ****/

#define MESSAGE @"message"
#define MESSAGE_CODE @"messageCode"
//#define PAYMENT_SERVER_ADD @"https://payment.zify.co/zifyPMT"
#define FDJ_BASE_URL @"https://www.fdj.fr"

#define NOINTERNET_ERROR_USER_INFO [NSDictionary dictionaryWithObject:NSLocalizedString(CMON_SERVERINTERFACE_NOINTERNETCONNECTION, nil) forKey:@"message"]
#define NOINTERNET_ERROR_CODE 1001
#define NOINTERNET_ERROR_DOMAIN @"ReachabilityError"

#define REQUEST_ERROR_USER_INFO [NSDictionary dictionaryWithObject:NSLocalizedString(CMON_GENERIC_INTERNALERROR, nil) forKey:@"message"]
#define REQUEST_ERROR_CODE 1002
#define REQUEST_ERROR_DOMAIN @"ReachabilityError"

@implementation ServerInterface
-(id)init{
    self = [super init];
    if (self) {
        _httpClient = [[HTTPClient alloc] init];
    }
    return self;
}

#pragma mark-
#pragma mark Shared Instance
#pragma mark-

+(ServerInterface *)sharedInstance{
    static dispatch_once_t onceToken;
    static id serverInstance;
    dispatch_once(&onceToken, ^{
        serverInstance = [[ServerInterface alloc] init];
    });
    return serverInstance;
}

-(void)getResponse:(ServerRequest *)request withHandler:(void (^)(ServerResponse *, NSError *))handler{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSURL *serverURL;
        if(request.isForFDJService){
            serverURL = [NSURL URLWithString:FDJ_BASE_URL];
        }else if(request.isPaymentRequest){
            serverURL = [NSURL URLWithString:PAYMENT_SERVER_ADD];
        }else if(request.isForBankRequest){
            serverURL = [NSURL URLWithString:PAYMENT_BANK_SERVER_ADD];
        } else if (request.isForReferAndEarnRequest) {
            serverURL = [NSURL URLWithString:SERVER_ADD_REFERANDEARN];
        } else {
            if(request.overrideBaseURL){
                serverURL = [NSURL URLWithString:SERVER_BASE_ADD];
            } else{
                serverURL = [NSURL URLWithString:SERVER_ADD];
            }
        }
        NSError *error;
        NSData *responseData;
        if(request.isGetRequest){
            responseData = [self.httpClient getResponseDataForRequestURL:[serverURL URLByAppendingPathComponent:request.serverURL] params:request.urlparams andError:&error];
        }else{
            responseData =[self.httpClient postResponseDataForRequestURL:[serverURL URLByAppendingPathComponent:request.serverURL] params:request.urlparams andError:&error andIsJSONRequestFormat:[request isJSONRequestFormat]];
        }
        NSDictionary *temp;
        if(responseData){
            temp = [NSJSONSerialization JSONObjectWithData:responseData options:(NSJSONReadingMutableContainers) error:&error];
            NSLog(@"Response is %@",temp);
        }
        ServerResponse *serverResponse = [self handleResponseData:responseData  withRequest:request withError:error withHandler:handler];
        
        if(serverResponse){
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([request isSuccessResponseCode:[serverResponse.messageCode intValue]] || [@"Success" isEqualToString:serverResponse.status]) {
                    // NSLog(@"handler is %@", handler);
                    if(handler != nil)
                        handler(serverResponse,nil);
                }else{
                    [self handleServerResponseError:serverResponse withHandler:handler];
                }
            });
        }
    });
}

-(ServerResponse *)handleResponseData:(NSData *)responseData  withRequest:(ServerRequest *)request withError:(NSError *)error withHandler:(void (^)(id, NSError *))handler{
    if (error || !responseData) {
        if(!error){
            if(![self isConnectedToInternet]){
                error =[NSError errorWithDomain:NOINTERNET_ERROR_DOMAIN code:NOINTERNET_ERROR_CODE userInfo:NOINTERNET_ERROR_USER_INFO];
            }else{
                error =[NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
            }
        }
        // NSLog(@"Error - - - -> %@",error);
        dispatch_async(dispatch_get_main_queue(), ^{
            handler(nil, error);
        });
        return nil;
    }
    NSDictionary *temp;
    if(responseData){
        temp = [NSJSONSerialization JSONObjectWithData:responseData options:(NSJSONReadingMutableLeaves) error:&error];
        //NSLog(@"Response is %@",temp);
    }
    /*else if(request.isForZenParkService){
     ServerResponse *serverResponse = [[ServerResponse alloc] initWithDataForZenparkPlaces:temp];
     return serverResponse;
     }*/
    ServerResponse *serverResponse;
    if(request.isForZenParkService){
        serverResponse = [[ServerResponse alloc]  initWithServerResponseFromLaterZenparkServicesWithData:temp withRequest:request];
    }else if(request.isForFDJService){
        serverResponse = [[ServerResponse alloc] initWithFDjDataResponse:temp];
    }else{
        if(request.isForV2Version){
            responseData = [self getDataFromResponseForV2Phase:temp];
        }
        serverResponse = [ServerResponse getServerResponse:responseData withRequest:request];
    }
    if (!serverResponse) {
        error =[NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
        dispatch_async(dispatch_get_main_queue(), ^{
            handler(nil, error);
        });
        return nil;
    }
    return serverResponse;
}


-(void)getResponseAsync:(ServerRequest *)request withHandler:(void (^)(ServerResponse *, NSError *))handler{
    NSURL *serverURL;
    if(request.isPaymentRequest){
        serverURL = [NSURL URLWithString:PAYMENT_SERVER_ADD];
    }else if(request.isForBankRequest){
        serverURL = [NSURL URLWithString:PAYMENT_BANK_SERVER_ADD];
    }else if(request.overrideBaseURL){
        serverURL = [NSURL URLWithString:SERVER_BASE_ADD];
    } else{
        serverURL = [NSURL URLWithString:SERVER_ADD];
    }
    NSError *error;
    NSData *responseData=[self.httpClient postResponseDataForRequestURL:[serverURL URLByAppendingPathComponent:request.serverURL] params:request.urlparams andError:&error andIsJSONRequestFormat:[request isJSONRequestFormat]];
    ServerResponse *serverResponse;
    if(request.isForZenParkService){
        NSDictionary *temp;
        if(responseData){
            temp = [NSJSONSerialization JSONObjectWithData:responseData options:(NSJSONReadingMutableLeaves) error:&error];
            // NSLog(@"Response is %@",temp);
        }
        serverResponse = [[ServerResponse alloc]  initWithServerResponseFromLaterZenparkServicesWithData:temp withRequest:request];
    }else{
        serverResponse = [self handleResponseDataAsync:responseData  withRequest:request withError:error withHandler:handler];
    }
    if(serverResponse){
        if ([request isSuccessResponseCode:[serverResponse.messageCode intValue]] || [@"Success" isEqualToString:serverResponse.status]) {
            handler(serverResponse,nil);
        }else{
            [self handleServerResponseError:serverResponse withHandler:handler];
        }
    }
}

-(ServerResponse *)handleResponseDataAsync:(NSData *)responseData  withRequest:(ServerRequest *)request withError:(NSError *)error withHandler:(void (^)(id, NSError *))handler{
    if (error || !responseData) {
        if(!error){
            error =[NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
        }
        // NSLog(@"Error - - - -> %@",error);
        handler(nil, error);
        return nil;
    }
    ServerResponse *serverResponse = [ServerResponse getServerResponse:responseData withRequest:request];
    if (!serverResponse) {
        error =[NSError errorWithDomain:REQUEST_ERROR_DOMAIN code:REQUEST_ERROR_CODE userInfo:REQUEST_ERROR_USER_INFO];
        handler(nil, error);
        return nil;
    }
    return serverResponse;
}


-(void)handleServerResponseError:(ServerResponse *)serverResponse withHandler:(void (^)(id, NSError *))handler{
    NSString *resMessage = serverResponse.message;
    if(resMessage == (NSString *) [NSNull null] || resMessage == nil){
        resMessage = @"";
    }
    NSInteger resCode = [serverResponse.messageCode integerValue];
    NSDictionary *errorUserInfo;
    errorUserInfo = @{@"message":resMessage};
    NSError *error = [NSError errorWithDomain:@"ServerValidationError" code:resCode userInfo:errorUserInfo];
    [Answers logCustomEventWithName:@"ServerValidationError" customAttributes:errorUserInfo];
    // [Crashlytics logEvent:@"ServerValidationError" attributes:errorUserInfo];
    handler(nil, error);
}

-(void)getResponse:(ServerRequest *)request withHandler:(void (^)(ServerResponse *, NSError *))handler andImage:(UIImage *)image{
    if(![self isConnectedToInternet]){
        NSError *error =[NSError errorWithDomain:NOINTERNET_ERROR_DOMAIN code:NOINTERNET_ERROR_CODE userInfo:NOINTERNET_ERROR_USER_INFO];
        [self handleResponseData:nil  withRequest:request withError:error withHandler:handler];
        return;
    }
    AFRKHTTPClient *client= [AFRKHTTPClient clientWithBaseURL:[NSURL URLWithString:SERVER_BASE_URL_FOR_IMAGES_SERVICES]];
    NSMutableURLRequest *urlRequest = [client multipartFormRequestWithMethod:@"POST" path:[SERVER_ADD_CONTEXT stringByAppendingString:request.serverURL] parameters:request.urlparams constructingBodyWithBlock: ^(id <AFRKMultipartFormData>formData) {
        if(image){
            NSLog(@"image name is %@", request.imageParamName);
            [formData appendPartWithFileData:UIImagePNGRepresentation(image) name:request.imageParamName fileName:@"temp.png" mimeType:@"image/png"];
        }
    }];
    // NSLog(@"Request URL --> %@",urlRequest);
    NSDictionary *params = request.urlparams;
    NSLog(@"URL encoded params are --> :%@",params.urlEncodedString);
    
    AFRKJSONRequestOperation *operation = [[AFRKJSONRequestOperation alloc] initWithRequest:urlRequest];
    [operation setCompletionBlockWithSuccess:^(AFRKHTTPRequestOperation *operation, id responseData)
     {
         NSError *error;
         
         ServerResponse *serverResponse = [self handleResponseData: responseData ? [NSJSONSerialization dataWithJSONObject:responseData options:0 error:&error] : nil withRequest:request withError:error withHandler:handler];
         if(serverResponse){
             dispatch_async(dispatch_get_main_queue(), ^{
                 if ([request isSuccessResponseCode:[serverResponse.messageCode intValue]] || [@"Success" isEqualToString:serverResponse.status]) {
                     handler(serverResponse,nil);
                 }else{
                     [self handleServerResponseError:serverResponse withHandler:handler];
                 }
             });
         }
     }
                                     failure:^(AFRKHTTPRequestOperation *operation, NSError *error) {
                                         [self handleResponseData:nil  withRequest:request withError:nil withHandler:handler];
                                     }];
    
    [operation start];
}

-(BOOL)isConnectedToInternet{
    
    /*Reachability *reachability = [Reachability reachabilityForInternetConnection];
     NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
     if (remoteHostStatus == NotReachable)
     {
     return FALSE;
     }
     else
     {
     return TRUE;
     }*/
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    if([response statusCode] == 200){
        // NSLog(@"active net is available and status response is %@", response);
        return  YES;
    }
    return  NO;
}

-(NSData *)getDataFromResponseForV2Phase:(NSDictionary *)responseDict{
    NSMutableDictionary *mDict =[[NSMutableDictionary alloc] initWithDictionary:responseDict];
    NSDictionary *statusDictionary = [responseDict objectForKey:@"statusInfo"];
    if (statusDictionary == nil) {
        statusDictionary = [responseDict objectForKey:@"status"];
    }
    NSString *statusCode = [statusDictionary objectForKey:@"statusCode"];
    NSString* message = [statusDictionary objectForKey:@"messageKey"];
    [mDict setObject:statusCode forKey:@"messageCode"];
    [mDict setObject:statusCode forKey:@"status"];
    message = NSLocalizedString(message, nil);
    if(message == nil){
        message = [statusDictionary objectForKey:@"messageKey"];
    }
    [mDict setObject:message forKey:@"message"];
    NSError *error;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:mDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
    return dataFromDict;
    
}
@end
