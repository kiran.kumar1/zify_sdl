//
//  RideDriveActionRequest.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "RideDriveActionRequest.h"
#import "CurrentTripNPendingRatingResponse.h"
#import "TripDrive.h"

#define DRIVE_ID_PARAM @"driveId"
#define RIDE_ID_PARAM @"rideId"
#define DRIVER_ID_PARAM @"driverId"
#define ROUTE_ID_PARAM @"routeId"
#define NUM_OFPARAM @"routeId"
#define NUM_SEATS_PARAM @"numOfSeats"
#define START_LAT_PARAM @"startLat"
#define START_LONG_PARAM @"startLong"
#define END_LAT_PARAM @"endLat"
#define END_LONG_PARAM @"endLong"
#define ENCODED_POLYLINE @"encodedPolylineStr"
#define CURRENT_LAT_PARAM @"currentLat"
#define CURRENT_LONG_PARAM @"currentLong"
    

@implementation RideDriveActionRequest
-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andDriveId:(NSNumber *)driveId{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _driveId = driveId;
    }
    return self;
}

-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andDriveId:(NSNumber *)driveId  andRideId:(NSNumber *)rideId{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _driveId = driveId;
        _rideId = rideId;
    }
    return self;
}

-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andRideId:(NSNumber *)rideId andDriveId:(NSNumber *)driveId andDriverId:(NSNumber *)driverId andRouteId:(NSString *)routeId{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _driveId = driveId;
        _rideId = rideId;
        _driverId = driverId;
        _routeId = routeId;
    }
    return self;
}

-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andRideId:(NSNumber *)rideId andDriveId:(NSNumber *)driveId andSeatsNum:(NSNumber *)numOfSeats{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _driveId = driveId;
        _rideId = rideId;
        _numOfSeats = numOfSeats;
    }
    return self;
}

-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andDriveId:(NSNumber *)driveId andUserLat:(NSNumber *)userLat andUserLong:(NSNumber *)userLong andPathLine:(NSString *)pathLine{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _driveId = driveId;
        _userLat = userLat;
        _userLong = userLong;
        _pathLine = pathLine;
    }
    return self;
}

-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andDriveId:(NSNumber *)driveId andRideId:(NSNumber *)rideId andUserLat:(NSNumber *)userLat andUserLong:(NSNumber *)userLong andPathLine:(NSString *)pathLine{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _driveId = driveId;
        _rideId = rideId;
        _userLat = userLat;
        _userLong = userLong;
        _pathLine = pathLine;
    }
    return self;
}

-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andUserLat:(NSNumber *)userLat andUserLong:(NSNumber *)userLong{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _userLat = userLat;
        _userLong = userLong;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    switch (_requestType) {
        case CANCELRIDE:
            params[DRIVE_ID_PARAM] = _driveId;
            params[RIDE_ID_PARAM] = _rideId;
            params[ROUTE_ID_PARAM] = _routeId;
            params[DRIVER_ID_PARAM] = _driverId;
            break;
        case CANCELDRIVE:
            params[DRIVE_ID_PARAM] = _driveId;
            break;
        case STARTDRIVE:
            params[DRIVE_ID_PARAM] = _driveId;
            params[START_LAT_PARAM] = _userLat == nil ? @0 : _userLat;
            params[START_LONG_PARAM] = _userLong == nil ? @0 : _userLong;
            break;
        case COMPLETEDRIVE:
            params[DRIVE_ID_PARAM] = _driveId;
            params[END_LAT_PARAM] = _userLat == nil ? @0 : _userLat;
            params[END_LONG_PARAM] =  _userLong == nil ? @0 : _userLong;
            params[ENCODED_POLYLINE] = _pathLine == nil ? @"" :_pathLine;
            break;
        case CONFIRMRIDE:
            params[DRIVE_ID_PARAM] = _driveId;
            params[RIDE_ID_PARAM] = _rideId;
            params[NUM_SEATS_PARAM] = _numOfSeats;
            break;
        case DECLINERIDE:
            params[DRIVE_ID_PARAM] = _driveId;
            params[RIDE_ID_PARAM] = _rideId;
        case STARTRIDE:
            params[DRIVE_ID_PARAM] = _driveId;
            params[RIDE_ID_PARAM] = _rideId;
            params[START_LAT_PARAM] = _userLat == nil ? @0 : _userLat;
            params[START_LONG_PARAM] = _userLong == nil ? @0 : _userLong;;
            break;
        case COMPLETERIDE:
            params[DRIVE_ID_PARAM] = _driveId;
            params[RIDE_ID_PARAM] = _rideId;
            params[END_LAT_PARAM] = _userLat == nil ? @0 : _userLat;
            params[END_LONG_PARAM] = _userLong == nil ? @0 : _userLong;;
            params[ENCODED_POLYLINE] = _pathLine == nil ? @"" :_pathLine;
            break;
        case CURRENTTRIPNPENDINGRATING:
            params[CURRENT_LAT_PARAM] = _userLat == nil ? @"" : _userLat;
            params[CURRENT_LONG_PARAM] = _userLong == nil ? @"" : _userLong;
        default:
            break;
    }
    return params;
}

-(NSString *) serverURL{
    switch (_requestType) {
        case CANCELDRIVE:
            return @"/driver/cancelDrive.htm";
        case STARTDRIVE:
            return @"/driver/startRealTimeDrive.htm";
        case COMPLETEDRIVE:
            return @"/driver/completeRealTimeGlobalDrive.htm";
        case CANCELRIDE:
            return @"/rider/cancelRide.htm";
        case CONFIRMRIDE:
            return @"/driver/confirmRideRequest.htm";
        case DECLINERIDE:
            return @"/driver/declineRide.htm";
        case STARTRIDE:
            return @"/rider/startRealTimeRide.htm";
        case COMPLETERIDE:
            return @"/rider/completeRealTimeRide.htm";
        case CURRENTTRIPNPENDINGRATING:
           return @"/user/getCurrentTripNPendingRating.htm";
        default:
            return nil;
    }
}

-(NSString *) mappingKey{
    switch (_requestType) {
        case CURRENTTRIPNPENDINGRATING:
            return @"currentTripNPendingRating";
        case COMPLETEDRIVE:
            return @"completedDrive";
        default:
            return nil;
    }
}

-(RKObjectMapping *) objectMapping{
    switch (_requestType) {
        case CURRENTTRIPNPENDINGRATING:
            return [CurrentTripNPendingRatingResponse getCurrentTripNPendingRatingObjectMapping];
        case COMPLETEDRIVE:
            return [TripDrive getTripDriveObjectMapping];
        default:
            return nil;
    }
}
@end
