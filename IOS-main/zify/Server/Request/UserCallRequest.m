//
//  UserCallRequest.m
//  zify
//
//  Created by Anurag S Rathor on 06/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserCallRequest.h"
#import "UserProfile.h"
#define CALLEE_ID_PARAM @"calleeId"
#define CALLER_ID_PARAM @"callerId"

@implementation UserCallRequest
-(id)initWithRequestType:(enum CallRequestType)requestType andCalleeId:(NSString *)calleeId{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _calleeId = calleeId;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    switch (_requestType) {
        case INITIATECALL:
            params[CALLEE_ID_PARAM] = _calleeId;
            params[CALLER_ID_PARAM] = [UserProfile getCurrentUserCallId];
        default:
            break;
    }
    return params;
}

-(NSString *) serverURL{
    switch (_requestType) {
        case INITIATECALL:
            return @"/user/triggerZifyCall.htm";
        default:
            return nil;
    }
}

-(NSString *) mappingKey{
    switch (_requestType) {
        case INITIATECALL:
            return @"maskedNumber";
        default:
            return nil;
    }
}

-(BOOL) isActualResponseSingleAttribute{
    switch (_requestType) {
        case INITIATECALL:
            return true;
        default:
            return false;
    }
}
@end
