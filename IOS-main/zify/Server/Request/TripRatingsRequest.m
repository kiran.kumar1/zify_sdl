//
//  TripRatingsRequest.m
//  zify
//
//  Created by Anurag S Rathor on 21/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "TripRatingsRequest.h"
#import "UserProfile.h"
#import "CurrentLocale.h"
#import "NSString+Random.h"

#define USER_TOKEN_PARAM @"userToken"
#define SESSION_ID_PARAM @"sessionId"
#define RATING_BEAN_PARAM @"ratingBean"
#define DRIVE_ID_PARAM @"driveId"
#define TRIP_ID_PARAM @"tripId"
#define RATING_PARAM @"rating"
#define COMMENTS_PARAM @"comments"
#define IS_GLOBAL_PARAM @"isGlobal"

@implementation TripRatingsRequest
-(id)initWithWithRequestType:(enum TripRatingsRequestType)requestType andTripId:(NSNumber *)tripId andRatingTripIds:(NSArray *)ratingTripIds andRatings:(NSArray *)ratings andComments:(NSArray *)comments{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _ratingTripIds = ratingTripIds;
        _ratings = ratings;
        _comments = comments;
        _tripId = tripId;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    params[USER_TOKEN_PARAM] = [UserProfile getCurrentUserToken];
    
    NSString *sessionId = [[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    if(sessionId == nil){
        sessionId = [NSString randomAlphanumericStringWithLength:10];
        [[NSUserDefaults standardUserDefaults] setObject:sessionId forKey:@"sessionId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    params[SESSION_ID_PARAM] =[[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    params[IS_GLOBAL_PARAM] = [NSNumber numberWithBool:[currentLocale isGlobalLocale]].stringValue;
    switch (_requestType) {
        case DRIVERATING:
            params[DRIVE_ID_PARAM] = _tripId;
            params[RATING_BEAN_PARAM] = [self getDriveRatingArray];
            break;
        case RIDERATING:
           params[RATING_BEAN_PARAM] = [self getRideRatingBean];
            break;
        default:
            break;
    }
    return params;
}

-(NSString *) serverURL{
    switch (_requestType) {
        case DRIVERATING:
            return @"/driver/rateRiders.htm";
        case RIDERATING:
            return @"/rider/rateDriver.htm";
        default:
            return nil;
    }
}

-(BOOL)isJSONRequestFormat {
    switch (_requestType) {
        case DRIVERATING:
        case RIDERATING:
            return true;
        default:
            return false;
    }
}

-(NSMutableArray *)getDriveRatingArray{
    NSMutableArray *ratingsArray = [[NSMutableArray alloc] init];
    for(int index = 0;index < _ratingTripIds.count;index++){
        NSMutableDictionary *ratingDict = [[NSMutableDictionary alloc] init];
        [ratingDict setValue:_ratingTripIds[index] forKey:TRIP_ID_PARAM];
        [ratingDict setValue:_ratings[index] forKey:RATING_PARAM];
        [ratingDict setValue:_comments[index] forKey:COMMENTS_PARAM];
        [ratingsArray addObject:ratingDict];
    }
    return ratingsArray;
}

-(NSMutableDictionary *)getRideRatingBean{
    NSMutableDictionary *ratingDict = [[NSMutableDictionary alloc] init];
    [ratingDict setValue:_ratingTripIds[0] forKey:TRIP_ID_PARAM];
    [ratingDict setValue:_ratings[0] forKey:RATING_PARAM];
    [ratingDict setValue:_comments[0] forKey:COMMENTS_PARAM];
    return ratingDict;
}
@end
