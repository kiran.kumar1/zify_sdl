//
//  CreateRideRequest.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ServerRequest.h"
#import "SearchRideEntity.h"
#import "SearchRide.h"
#import "UserSearchData.h"
//#import "zify-Swift.h"

@interface CreateRideRequest : ServerRequest
@property (nonatomic, strong) SearchRideEntity *searchRideEntity;
@property (nonatomic, strong) SearchRide *searchRide;
@property (nonatomic, strong) UserSearchData *userData;


-(id)initWithSearchRideEntity:(SearchRideEntity *)searchRide withSearchData:(UserSearchData *)userData;
-(id)initWithSearchRide:(SearchRide *)searchRide withSearchData:(UserSearchData *)userData;

@end
