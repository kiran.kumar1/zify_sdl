//
//  UserRideRequest.m
//  zify
//
//  Created by Anurag S Rathor on 22/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserUploadMultiData.h"
#import "CurrentLocale.h"


#define ID_CARD_UPLOAD_KEY @"idcardFile"
#define VEHICLE_UPLOAD_KEY @"vehicleImgFile"
#define PROFILE_UPLOAD_KEY @"profilePicFile"
#define COUNTRY_CODE @"countryCode"





@implementation UserUploadMultiData
-(id)initWithUploadType:(enum UPLOADTYPE)type
{
    self = [super init];
    if (self) {
        _uploadType = type;
    }
    return self;
}
-(NSDictionary *)urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    //NSString *isoCode = [[CurrentLocale sharedInstance] getMobileISOCode];
    UserProfile *user = [UserProfile getCurrentUser];
    NSString *country = user.country;
    if(country == nil){
        country = @"";
    }
    [params setObject:country forKey:COUNTRY_CODE];
    return params;
}

-(NSString *) serverURL{
    
    switch (_uploadType) {
        case ID_CARD_TYPE:
            return @"/upload/idcard";
            break;
        case VEHICLE_TYPE:
            return @"/upload/vehicle";
            break;
        case PROFILE_TYPE:
            return @"/upload/profile";
            break;
        default: return nil;
            break;
    }
}
-(NSString *) imageParamName{
    switch (_uploadType) {
        case ID_CARD_TYPE:
            return ID_CARD_UPLOAD_KEY;
            break;
        case VEHICLE_TYPE:
            return VEHICLE_UPLOAD_KEY;
            break;
        case PROFILE_TYPE:
            return PROFILE_UPLOAD_KEY;
            break;
        default: return nil;
            break;
    }
}
-(BOOL)isForZenParkService{
    return true;
}
@end
