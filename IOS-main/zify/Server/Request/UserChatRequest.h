//
//  UserChatRequest.h
//  zify
//
//  Created by Anurag S Rathor on 12/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"

enum ChatRequestType{
    SAVECHATPROFILEREQUEST,GETCHATUSERDETAIL
};


@interface UserChatRequest : ServerRequest
@property (nonatomic,strong) NSString *chatUserId;
@property (nonatomic,strong) NSString *chatPassword;
@property (nonatomic,strong) NSString *chatUserName;
@property (nonatomic,strong) NSString *chatFirstName;
@property (nonatomic,strong) NSString *chatLastName;
@property (nonatomic) enum ChatRequestType requestType;
-(id)initWithRequestType:(enum ChatRequestType)requestType andChatUserId:(NSString *)userId andChatUserName:(NSString *)userName andChatPassword:(NSString *)password andChatFirstName:(NSString *)firstName andChatLastname:(NSString *)lastName;
@end
