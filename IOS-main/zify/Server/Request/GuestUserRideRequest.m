//
//  GuestUserRideRequest.m
//  zify
//
//  Created by Anurag S Rathor on 20/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "GuestUserRideRequest.h"
#import "GuestUserSearchRide.h"

#define SRC_LAT_PARAM @"srcLat"
#define SRC_LNG_PARAM @"srcLong"
#define DEST_LAT_PARAM @"destLat"
#define DEST_LNG_PARAM @"destLong"
#define NUM_SEATS_PARAM @"numOfSeats"
#define DEPARTURE_TIME_PARAM @"departureTime"
#define IS_GLOBAL_PARAM @"isGlobal"
#define MERCHANT_ID_PARAM @"merchantId"
#define CHANNEL_ID_PARAM @"channelId"

@implementation GuestUserRideRequest
-(id)initWithSourceLocality:(LocalityInfo *)sourceLocality andDestinationLocality:(LocalityInfo *)destinationLocality andRideDate:(NSString *)rideDate andSeats:(NSNumber *)seats andMerchantId:(NSString *)merchantId{
    self = [super init];
    if (self) {
        _sourceLocality = sourceLocality;
        _destinationLocality = destinationLocality;
        _rideDate = rideDate;
        _seats = seats;
        _merchantId = merchantId;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    double sourceLat = (double)_sourceLocality.latLng.latitude;
    double sourceLng = (double)_sourceLocality.latLng.longitude;
    double destLat = (double)_destinationLocality.latLng.latitude;
    double destLng = (double)_destinationLocality.latLng.longitude;
    params[SRC_LAT_PARAM] = [[NSNumber numberWithDouble:sourceLat] stringValue];
    params[SRC_LNG_PARAM] = [[NSNumber numberWithDouble:sourceLng] stringValue];
    params[DEST_LAT_PARAM] = [[NSNumber numberWithDouble:destLat] stringValue];
    params[DEST_LNG_PARAM] = [[NSNumber numberWithDouble:destLng] stringValue];
    params[DEPARTURE_TIME_PARAM] = _rideDate;
    params[NUM_SEATS_PARAM] = _seats;
    params[IS_GLOBAL_PARAM] = _sourceLocality.country != nil && [@"India" caseInsensitiveCompare:_sourceLocality.country] != NSOrderedSame ? @1 : @0;
    params[MERCHANT_ID_PARAM] =  _merchantId ? _merchantId : @"Zify-Self-GuMe@PrAn-1982-1985";
    params[CHANNEL_ID_PARAM] = @"IOS_APP";
    return params;
}

-(NSString *) serverURL{
    return @"/zify/guest/searchAvailableRides.htm";
}

-(NSString *) mappingKey{
    return @"drives";
}

-(RKObjectMapping *) objectMapping{
    return [GuestUserSearchRide getGuestUserSearchRideObjectMapping];
}

-(BOOL)overrideBaseURL{
    return true;
}
@end
