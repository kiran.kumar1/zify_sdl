//
//  VehicleDetailUpdateRequest.m
//  zify
//
//  Created by Anurag S Rathor on 02/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "VehicleDetailUpdateRequest.h"
#import "UserProfile.h"
#import "AppDelegate.h"

#define VEHICLE_REGISTRATION_NUM_PARAM @"vehicleRegistrationNum"
#define VEHICLE_INSURANCE_NUM_PARAM @"vehicleInsuranceNum"
#define DRIVING_LICENSE_NUM_PARAM @"drivingLicenceNum"
#define VEHICLE_MODEL_PARAM @"vehicleModel"
#define VEHICLE_IMG_FILE_PARAM @"vehicleImgFile"
#define VEHICLE_IMG_KEY @"vehicleImgUrl"
#define COUNTRY @"country"
@implementation VehicleDetailUpdateRequest
-(id)initWithVehicleModel:(NSString *)vehicleModel andVehicleRegistrationNumber:(NSString *)vehicleRegistrationNumber andInsuranceNumber:(NSString *)insuranceNumber andDrivingLicenseNumber:(NSString *)drivingLicenseNumber andWithVehicleImagePah:(NSString *)imagePath withUpdateType:(enum UPDATE_TYPE)type withMobileNumber:(NSString *)mobileNum withIsoCode:(NSString *)isoCode withCountryCode:(NSString *)countryCode{
    self = [super init];
    if (self) {
        _vehicleModel = vehicleModel;
        _vehicleRegistrationNumber = vehicleRegistrationNumber;
        _insuranceNumber = insuranceNumber;
        _drivingLicenseNumber = drivingLicenseNumber;
        _vehicleImagelink = imagePath;
        _updateType = type;
        _mobileNUmber = mobileNum;
        _isdCode = isoCode;
        _countryCode = countryCode;
    }
    return self;
}
-(NSDictionary *) urlparams{
    NSMutableDictionary *params = (NSMutableDictionary *)[self getProfileParamsDict];
    

    UserProfile *userProfile =[UserProfile getCurrentUser];
    NSMutableDictionary *params1 = [[NSMutableDictionary alloc] init];
    params1[@"userId"] = [UserProfile getCurrentUserId];
    params1[@"idCardNum"] = userProfile.userDocuments.idCardNum;
    params1[@"idCardType"] = userProfile.userDocuments.idCardType;
    params1[@"isIdCardDocUploaded"] = userProfile.userDocuments.isIdCardDocUploaded;
   
    params1[@"idCardImgUrl"] = userProfile.userDocuments.idCardImgUrl;
    
    switch (_updateType) {
        case VEHICLE_IMAGE_TYPE:
            params1[@"profileImgUrl"] = userProfile.profileImgUrl;
            params1[@"vehicleRegistrationNum"] = _vehicleRegistrationNumber;
            params1[@"vehicleInsuranceNum"] = _insuranceNumber;
            params1[@"drivingLicenceNum"] = _drivingLicenseNumber;
            params1[@"vehicleModel"] = _vehicleModel;
            params1[@"vehicleImgUrl"] = _vehicleImagelink;
            params1[@"isVehicleImgUploaded"] = @"1";
            params[@"isdCode"]= userProfile.isdCode;
            params[@"mobile"]= userProfile.mobile;
            break;
        case PROFILE_IMAGE_TYPE:
            params1[@"profileImgUrl"] = _vehicleImagelink;
            params1[@"vehicleRegistrationNum"] = userProfile.userDocuments.vehicleRegistrationNum;
            params1[@"vehicleInsuranceNum"] = userProfile.userDocuments.vehicleInsuranceNum;
            params1[@"drivingLicenceNum"] = userProfile.userDocuments.drivingLicenceNum;
            params1[@"vehicleModel"] = userProfile.userDocuments.vehicleModel;
            params1[@"vehicleImgUrl"] = userProfile.userDocuments.vehicleImgUrl;
            params1[@"isVehicleImgUploaded"] = userProfile.userDocuments.isVehicleImgUploaded;
            params[@"isdCode"]= userProfile.isdCode;
            params[@"mobile"]= userProfile.mobile;
            break;
        case MOBILE_NUMBER_UPDATE:
            params1[@"profileImgUrl"] = userProfile.profileImgUrl;
            params1[@"vehicleRegistrationNum"] = userProfile.userDocuments.vehicleRegistrationNum;
            params1[@"vehicleInsuranceNum"] = userProfile.userDocuments.vehicleInsuranceNum;
            params1[@"drivingLicenceNum"] = userProfile.userDocuments.drivingLicenceNum;
            params1[@"vehicleModel"] = userProfile.userDocuments.vehicleModel;
            params1[@"vehicleImgUrl"] = userProfile.userDocuments.vehicleImgUrl;
            params1[@"isVehicleImgUploaded"] = userProfile.userDocuments.isVehicleImgUploaded;
            params[@"mobile"] = _mobileNUmber;
            params[@"isdCode"] = _isdCode;
            params[@"countryCode"] = _countryCode;
            break;

        default:
            break;
    }
    
    params[@"userDocs"] = params1;
    
    NSDictionary *travelPreferencesDict = [self getUserPreferencesDictToRequest];
    if(travelPreferencesDict){
        params[@"userPreferences"] = travelPreferencesDict;
    }
    return params;
}
-(NSString*) convertDictionaryTOJSon:(NSDictionary *) paramsDict {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:paramsDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = @"";
    if (! jsonData) {
        NSLog(@"Got an error= %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}
-(NSString *) serverURL{
    return @"/user/V2/updateprofile";
}
-(NSString *) mappingKey{
    return @"user";
}
-(BOOL)isForV2Version{
    return true;
}
-(BOOL)isJSONRequestFormat {
    return true;
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL) saveInDatabase{
    return true;
}

-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    [UserProfile deleteCurrentUser];
    [UserProfile createCurrentUser:responseData];
}

-(NSString *) imageParamName{
    return VEHICLE_IMG_FILE_PARAM;
}

-(NSDictionary *)getProfileParamsDict{
    
    NSMutableDictionary *params = [self getUserCommonParams];
    UserProfile *profile = [UserProfile getCurrentUser];
    params[@"loginMode"]= profile.loginMode;
    params[@"userId"] = [UserProfile getCurrentUserId];
    params[@"firstName"]= profile.firstName;
    params[@"lastName"]= profile.lastName;
    params[@"userName"]= profile.userName;
    params[@"emailId"]= profile.emailId;
    params[@"gender"]= profile.gender;
    params[@"zifyPromotionalPoints"]= profile.zifyPromotionalPoints;
    params[@"zifyUserPoints"]= profile.zifyUserPoints;
    params[@"zifyCash"]= profile.zifyCash;
    params[@"currency"]= profile.currency;
    params[@"profileCompleteness"]= profile.profileCompleteness;
    params[@"userRatingMale"]= profile.userRatingMale;
    params[@"userRatingFemale"]= profile.userRatingFemale;
    params[@"numOfMalesRated"]= profile.numOfMalesRated;
    params[@"numOfFemalesRated"]= profile.numOfFemalesRated;
    params[@"userType"]= profile.userType;
    params[@"userStatus"]= profile.userStatus;
    params[@"userStatusReason"]= profile.userStatusReason;
    params[@"totalRides"]= profile.totalRides;
    params[@"totalDistance"]=  profile.totalDistance;
    params[@"companyEmail"]= profile.companyEmail;
    params[@"city"]= profile.city;
    params[@"country"]= profile.country;
    params[@"pincode"]= profile.pincode;
    params[@"mobileVerified"]= profile.mobileVerified;
    params[@"callId"]= profile.callId;
    params[@"distanceUnit"]= profile.distanceUnit;
    params[@"isGlobal"] = profile.isGlobal;
    params[@"isGlobalPayment"] = profile.isGlobalPayment;
    params[@"dob"] = [[AppDelegate getAppDelegateInstance] convertDOBFROMServiceTOReqFormat];
    params[@"companyName"] = profile.companyName;
    params[@"bloodGroup"] = profile.bloodGroup;
    params[@"emergencyContact"] = profile.emergencyContact;
    NSLog(@"profile complete ness is %@", profile.profileCompleteness);
    
    NSLog(@"params are %@", params);
    return params;
}

-(NSDictionary *)getUserPreferencesDictToRequest{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    UserProfile *profile = [UserProfile getCurrentUser];
    UserPreferences *travelPreferences = profile.userPreferences;
    if(!travelPreferences){
        return nil;
    }
    params[@"city"]= travelPreferences.city;
    params[@"destinationAddress"] = travelPreferences.destinationAddress;
    params[@"destinationLatitude"]= travelPreferences.destinationLatitude;
    params[@"destinationLongitude"]= travelPreferences.destinationLongitude;
    params[@"sourceAddress"]= travelPreferences.sourceAddress;
    params[@"sourceLatitude"]= travelPreferences.sourceLatitude;
    params[@"sourceLongitude"] = travelPreferences.sourceLongitude;
    params[@"returnTime"] = travelPreferences.returnTime;
    params[@"startTime"] = travelPreferences.startTime;
    params[@"onwardRouteId"] = travelPreferences.onwardRouteId;
    params[@"onwardPolyline"] = travelPreferences.onwardPolyline;
    params[@"returnRouteId"] = travelPreferences.returnRouteId;
    params[@"returnPolyline"] = travelPreferences.returnPolyline;
    params[@"userMode"] = travelPreferences.userMode;
    params[@"userId"] = [UserProfile getCurrentUserId];
    
    return params;
}
@end
