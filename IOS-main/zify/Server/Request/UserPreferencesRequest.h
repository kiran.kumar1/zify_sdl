//
//  UserPreferencesRequest.h
//  zify
//
//  Created by Anurag S Rathor on 15/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalityInfo.h"
#import "ServerRequest.h"
#import "DriveRoute.h"

enum PreferencesRequestType{
    ADDPREFERENCES,UPDATEPREFERENCES
};

@interface UserPreferencesRequest : ServerRequest
//@property (nonatomic, strong) NSDictionary *homeAddress;
//@property (nonatomic, strong) NSDictionary *officeAddress;
@property (nonatomic, strong) LocalityInfo *homeAddress;
@property (nonatomic, strong) LocalityInfo *officeAddress;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *returnTime;
//@property (nonatomic, strong) NSDictionary *onwardRoute;
//@property (nonatomic, strong) NSDictionary *returnRoute;
@property (nonatomic, strong) DriveRoute *onwardRoute;
@property (nonatomic, strong) DriveRoute *returnRoute;
@property (nonatomic, strong) NSString *userMode;
@property (nonatomic) enum PreferencesRequestType requestType;
-(id)initWithWithRequestType:(enum PreferencesRequestType)requestType andHomeAddress:(LocalityInfo *)homeAddress andOfficeAddress:(LocalityInfo *)officeAddress andStartTime:(NSString *)startTime andReturnTime:(NSString *)returnTime andOnwardRoute:(DriveRoute *)onwardRoute andReturnRoute:(DriveRoute *)returnRoute andUserMode:(NSString *)userMode;
@end
