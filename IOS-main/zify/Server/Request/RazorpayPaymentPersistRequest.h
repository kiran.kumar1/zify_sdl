//
//  RazorpayPaymentPersistRequest.h
//  zify
//
//  Created by Anurag S Rathor on 21/12/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"


enum RazorpayRequestType{
    PAYMENTREQUEST,PAYMENTRESPONSE
};

@interface RazorpayPaymentPersistRequest : ServerRequest
@property (nonatomic) enum RazorpayRequestType requestType;
@property (nonatomic, strong) NSString * amount;
@property (nonatomic, strong) NSString * currency;
@property (nonatomic, strong) NSString * paymentId;
@property (nonatomic, strong) NSString * zifyPaymentRefId;
@property (nonatomic, strong) NSNumber * failureCode;
@property (nonatomic, strong) NSString * failureDescription;
-(id)initWithRequestType:(enum RazorpayRequestType)requestType andAmount:(NSString *)amount andCurrency:(NSString *)currency;
-(id)initWithRequestType:(enum RazorpayRequestType)requestType andPaymentId:(NSString *)paymentId andZifyPaymentRefId:(NSString *)zifyPaymentRefId andFailureCode:(NSNumber *)code andFailureDescription:(NSString *)description;
@end

