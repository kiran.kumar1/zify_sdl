//
//  UserRegisterRequest.m
//  zify
//
//  Created by Anurag S Rathor on 09/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "GetZenParkpointsRequest.h"
#import "UserProfile.h"
#import "ZenparkErrorObj.h"
#import "AppDelegate.h"

#define NorthLatitude @"northLatitude"
#define SouthLatitude @"southLatitude"
#define WestLongitude @"westLongitude"
#define EastLongitude @"eastLongitude"
#define AUTH_TOKEN @"authToken"


@implementation GetZenParkpointsRequest
-(id)initWithNorth:(NSString *)north andSouth:(NSString *)south andWest:(NSString *)west andEast:(NSString *)east{
    self = [super init];
    if (self) {
        _south = south;
        _north = north;
        _west = west;
        _east = east;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[AUTH_TOKEN] = [AppDelegate getAppDelegateInstance].zenparkAuthToken;
    params[NorthLatitude] = _north;
    params[SouthLatitude] = _south;
    params[WestLongitude] = _west;
    params[EastLongitude] = _east;


    return params;
}
-(NSString *) serverURL{
    return @"zifyServices/zenpark/getParkingLotsbyZone.htm";
}
-(NSString *) mappingKey{
    return @"parkingLots";
}

-(BOOL)isForZenParkService{
    return true;
}
-(BOOL)overrideBaseURL{
    return true;
}
-(RKObjectMapping *) objectMapping{
    return  [ZenparkErrorObj getZenParkErrorObjectMapping];
}


@end
