//
//  UserIdentityCardSaveRequest.h
//  zify
//
//  Created by Anurag S Rathor on 02/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerRequest.h"

@interface UserIdentityCardSaveRequest : ServerRequest
@property (nonatomic, strong) NSString * identityCardType;
@property (nonatomic, strong) NSString * idCardImagePath;

-(id)initWithIdentityCardType:(NSString *)identityCardType withImagePath:(NSString *)imagePath;
@end
