//
//  UserCallRequest.h
//  zify
//
//  Created by Anurag S Rathor on 06/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"

enum CallRequestType{
    INITIATECALL
};

@interface UserCallRequest : ServerRequest
@property (nonatomic,strong) NSString *calleeId;
@property (nonatomic) enum CallRequestType requestType;
-(id)initWithRequestType:(enum CallRequestType)requestType andCalleeId:(NSString *)calleeId;
@end
