//
//  RazorpayPaymentPersistRequest.m
//  zify
//
//  Created by Anurag S Rathor on 21/12/15.
//  Copyright © 2015 zify. All rights reserved.
//

#import "RazorpayPaymentPersistRequest.h"
#import "UserProfile.h"

#define AMOUNT_PARAM @"amount"
#define CURRENCY_PARAM @"currency"
#define COUNTRY_CODE_PARAM @"countryCode"
#define ZIFY_PAYMENT_REF_ID_PARAM @"zifyPaymentRefId"
#define PAYMENT_ID_PARAM @"paymentId"
#define FAILURE_CODE_PARAM @"failureCode"
#define FAILURE_DESCRIPTION_PARAM @"failureDescription"

@implementation RazorpayPaymentPersistRequest
-(id)initWithRequestType:(enum RazorpayRequestType)requestType andAmount:(NSString *)amount andCurrency:(NSString *)currency{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _amount = amount;
        _currency = currency;
    }
    return self;
}

-(id)initWithRequestType:(enum RazorpayRequestType)requestType andPaymentId:(NSString *)paymentId andZifyPaymentRefId:(NSString *)zifyPaymentRefId andFailureCode:(NSNumber *)code andFailureDescription:(NSString *)description{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _paymentId = paymentId == nil ? @"" : paymentId;
        _zifyPaymentRefId = zifyPaymentRefId;
        _failureCode = code == nil ? @0 : code;
        _failureDescription = description == nil ? @"" : description;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    UserProfile *profile = [UserProfile getCurrentUser];
    params[COUNTRY_CODE_PARAM] = profile.country;
    switch (_requestType) {
        case PAYMENTREQUEST:
            params[AMOUNT_PARAM] =  _amount;
            params[CURRENCY_PARAM] = _currency;
            break;
        case PAYMENTRESPONSE:
            params[PAYMENT_ID_PARAM] = _paymentId;
            params[ZIFY_PAYMENT_REF_ID_PARAM] = _zifyPaymentRefId;
            params[FAILURE_CODE_PARAM] = _failureCode;
            params[FAILURE_DESCRIPTION_PARAM] = _failureDescription;
            break;
        default:
            return nil;
    }
    return params;
}
-(NSString *) serverURL{
    switch (_requestType) {
        case PAYMENTREQUEST:
            return  @"/genZifyRefIdNPersistRequestRazorPay";    //@"/zifyPayment/genZifyRefIdNPersistRequestRazorPay.htm";
        case PAYMENTRESPONSE:
            return @"/persistRazorPayPaymentResp";
        default:
            return nil;
    }
    
}

-(NSString *) mappingKey{
    switch (_requestType) {
        case PAYMENTREQUEST:
            return @"zifyPaymentRefId";
        default:
            return nil;
    }
}

-(BOOL)isPaymentRequest{
    return true;
}
-(BOOL)isForZenParkService{
    return true;

  /*  switch (_requestType) {
        case PAYMENTREQUEST:
            return true;
        case PAYMENTRESPONSE:
            return false;
        default: return false;
    }*/
}

-(BOOL) isActualResponseSingleAttribute{
    switch (_requestType) {
        case PAYMENTREQUEST:
            return true;
        default:
            return false;
    }
}
@end
