//
//  UserFavouriteRequest.h
//  zify
//
//  Created by Anurag S Rathor on 23/02/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "ServerRequest.h"
#import "LocalityInfo.h"
#import "SearchRideEntity.h"
#import "SearchRide.h"

enum UserFavouriteRequestType{
    GETUSERFAVOURITES,SUBSCRIBEFAVDRIVER
};

@interface UserFavouriteRequest : ServerRequest
@property (nonatomic,strong) SearchRideEntity *searchRide;
@property (nonatomic,strong) LocalityInfo *sourceLocality;
@property (nonatomic,strong) LocalityInfo *destinationLocality;
@property (nonatomic,strong) NSNumber *subscribeFlag;
@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic) enum UserFavouriteRequestType requestType;
-(id)initWithRequestType:(enum UserFavouriteRequestType)requestType andSearchRide:(SearchRideEntity *)searchRide andSourceLocality:(LocalityInfo *)sourceLocality andDestLocality:(LocalityInfo *)destinationLocality andSubscibeFlag:(NSNumber *)subscribeFlag;
-(id)initWithRequestType:(enum UserFavouriteRequestType)requestType andDepartureTime:(NSString *)departureTime;
@end
