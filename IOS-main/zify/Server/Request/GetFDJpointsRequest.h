//
//  UserRegisterRequest.h
//  zify
//
//  Created by Anurag S Rathor on 09/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"

@interface GetFDJpointsRequest : ServerRequest
@property (nonatomic, strong) NSString * sourceLat;
@property (nonatomic, strong) NSString * sourceLong;

-(id)initWithSrcLatitude:(NSString *)srcLat andSrcLongitude:(NSString *)srcLong;
@end
