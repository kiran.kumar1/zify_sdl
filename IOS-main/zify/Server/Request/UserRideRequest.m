//
//  UserRideRequest.m
//  zify
//
//  Created by Anurag S Rathor on 22/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserRideRequest.h"
#import "DriveRoute.h"
#import "SearchRide.h"

#define SRC_CITY_PARAM @"srcCity"
#define DEST_CITY_PARAM @"destCity"
#define SRC_COUNTRY_PARAM @"srcCountry"
#define DEST_COUNTRY_PARAM @"destCountry"
#define SRC_ADD_PARAM @"srcAddress"
#define DEST_ADD_PARAM @"destAddress"
#define SRC_LAT_PARAM @"srcLat"
#define SRC_LNG_PARAM @"srcLong"
#define DEST_LAT_PARAM @"destLat"
#define DEST_LNG_PARAM @"destLong"
#define NUM_SEATS_PARAM @"numOfSeats"
#define DEPARTURE_TIME_PARAM @"departureTime"
#define MERCHANT_ID_PARAM @"merchantId"
#define CHANNEL_ID_PARAM @"channelId"
#define LAST_DEPT_TIME @"lastDepartureTime"
#define START_TIME @"startTime"
#define END_TIME @"endTime"
#define OFFSET @"offset"
#define isV2 @"isV2"

@implementation UserRideRequest
-(id)initWithRequestType:(enum RideRequestType)requestType andSourceLocality:(LocalityInfo *)sourceLocality andDestinationLocality:(LocalityInfo *)destinationLocality andRideDate:(NSString *)rideDate andSeats:(NSNumber *)seats andMerchantId:(NSString *)merchantId withOffsetValue:(NSNumber *)offset withStartTime:(NSString *)startTime withEndTime:(NSString *)endTime{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _sourceLocality = sourceLocality;
        _destinationLocality = destinationLocality;
        _rideDate = rideDate;
        _seats = seats;
        _merchantId = merchantId;
        _offSet = offset;
        _startTime = startTime;
        _endTime = endTime;
    }
    return self;
}



-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    double sourceLat = (double)_sourceLocality.latLng.latitude;
    double sourceLng = (double)_sourceLocality.latLng.longitude;
    double destLat = (double)_destinationLocality.latLng.latitude;
    double destLng = (double)_destinationLocality.latLng.longitude;
    params[SRC_LAT_PARAM] = [[NSNumber numberWithDouble:sourceLat] stringValue];
    params[SRC_LNG_PARAM] = [[NSNumber numberWithDouble:sourceLng] stringValue];
    params[DEST_LAT_PARAM] = [[NSNumber numberWithDouble:destLat] stringValue];
    params[DEST_LNG_PARAM] = [[NSNumber numberWithDouble:destLng] stringValue];
    params[SRC_CITY_PARAM] = _sourceLocality.city == nil ? @"" : _sourceLocality.city;
    params[DEST_CITY_PARAM] =_destinationLocality.city == nil ? @"" : _destinationLocality.city;
    params[NUM_SEATS_PARAM] = _seats;
    params[MERCHANT_ID_PARAM] =  _merchantId ? _merchantId :@"Zify-Self-GuMe@PrAn-1982-1985";
    params[CHANNEL_ID_PARAM] = @"IOS_APP";
    params[SRC_ADD_PARAM] = _sourceLocality.address;
    params[DEST_ADD_PARAM] = _destinationLocality.address;
    switch (_requestType) {
            case SEARCHRIDE:
            params[DEPARTURE_TIME_PARAM] = _rideDate;
            break;
        case SEARCHPAGINATEDRIDE:
            params[START_TIME] = _startTime;
            params[END_TIME] = _endTime;
            params[OFFSET] = _offSet;
            break;
    }

    return params;
}

-(NSString *) serverURL{
    
    switch (_requestType) {
        case SEARCHRIDE:
            return @"/driver/V2/searchForDrives";
            break;
        case SEARCHPAGINATEDRIDE:
            return @"/driver/V2/getPaginatedDrives";
            break;
        default: return nil;
            break;
    }
    ///driver/V2/searchForDrives.htm";
    ///driver/getMatchedGlobalZifyUpcomingDrives.htm
}
-(BOOL)isForZenParkService{
    return true;
}

-(NSString *) mappingKey{
    return @"drives";
}

-(RKObjectMapping *) objectMapping{
    return [SearchRide getSearchRideObjectMapping];
}
@end
