//
//  FBUserLoginRequest.m
//  zify
//
//  Created by Anurag S Rathor on 10/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "FBUserLoginRequest.h"
#import "UserProfile.h"
#import "CurrentLocale.h"
#import "CurrentLocationTracker.h"

#define FIRST_NAME_PARAM @"firstName"
#define LAST_NAME_PARAM @"lastName"
#define USER_EMAIL_PARAM @"userEmail"
#define USER_PASSWORD_PARAM @"userPswd"
#define ISD_CODE_PARAM @"isdCode"
#define MOBILE_PARAM @"mobile"
#define GENDER_PARAM @"gender"
#define FB_PROFILE_ID_PARAM @"fbProfileId"
#define ISO_CODE_PARAM @"isoCode"
#define COUNTRY_PARAM @"country"
#define USER_LAT_PARAM @"userLat"
#define USER_LONG_PARAM @"userLong"

@implementation FBUserLoginRequest
-(id)initWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName andUserEmail:(NSString *)userEmail andisdCode:(NSString *)isdCode andMobile:(NSString *)mobile andGender:(NSString *)gender andFbProfileId:(NSString *)fbProfileId{
    self = [super init];
    if (self) {
        _firstName = firstName;
        _lastName = lastName;
        _userEmail = userEmail;
        _isdCode = isdCode;
        _mobile = mobile;
        _gender = gender;
        _fbProfileId = fbProfileId;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSString *isoCode = [[CurrentLocale sharedInstance] getMobileISOCode];
    params[FIRST_NAME_PARAM] = _firstName;
    params[LAST_NAME_PARAM] = _lastName;
    params[USER_EMAIL_PARAM] = _userEmail;
    params[ISD_CODE_PARAM] = _isdCode;
    params[MOBILE_PARAM] = _mobile;
    params[GENDER_PARAM] = _gender;
    params[FB_PROFILE_ID_PARAM] = _fbProfileId;
    if(isoCode){
        params[ISO_CODE_PARAM] = isoCode;
    } else{
        LocalityInfo *localityInfo = [CurrentLocationTracker sharedInstance].currentLocalityInfo;
        if(localityInfo){
            params[COUNTRY_PARAM] = localityInfo.country == nil ? @"" : localityInfo.country;
            params[USER_LAT_PARAM] = [[NSNumber numberWithDouble:localityInfo.latLng.latitude] stringValue];
            params[USER_LONG_PARAM] = [[NSNumber numberWithDouble:localityInfo.latLng.longitude] stringValue];
            
        }
    }
    return params;
}
-(NSString *) serverURL{
    return @"/user/fbLoginWithoutRegister.htm";
}
-(NSString *) mappingKey{
    return @"user";
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL) saveInDatabase{
    return true;
}
-(BOOL)isSuccessResponseCode:(int)responseCode{
    return responseCode == 0 || responseCode == 1;
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    [UserProfile createCurrentUser:responseData];
}
@end
