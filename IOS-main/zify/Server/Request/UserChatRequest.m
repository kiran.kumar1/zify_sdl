//
//  UserChatRequest.m
//  zify
//
//  Created by Anurag S Rathor on 12/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserChatRequest.h"
#import "UserChatProfile.h"
#import "UserContactDetail.h"

#define CHAT_USER_ID_PARAM @"qbChatUserId"
#define CHAT_USER_NAME_PARAM @"qbUserName"
#define CHAT_PASSWORD_PARAM @"qbChatPswd"
#define CHAT_FIRST_NAME_PARAM @"qbFirstName"
#define CHAT_LAST_NAME_PARAM @"qbLastName"
#define CHAT_USER_IDS_PARAM @"qbUserIds"

@implementation UserChatRequest
-(id)initWithRequestType:(enum ChatRequestType)requestType andChatUserId:(NSString *)userId  andChatUserName:(NSString *)userName andChatPassword:(NSString *)password andChatFirstName:(NSString *)firstName andChatLastname:(NSString *)lastName{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _chatUserId = userId;
        _chatUserName = userName;
        _chatPassword = password;
        _chatFirstName = firstName;
        _chatLastName = lastName;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
   switch (_requestType) {
        case SAVECHATPROFILEREQUEST:
            params[CHAT_USER_ID_PARAM] = _chatUserId;
            params[CHAT_USER_NAME_PARAM] = _chatUserName;
            params[CHAT_PASSWORD_PARAM] = _chatPassword;
            params[CHAT_FIRST_NAME_PARAM] = _chatFirstName;
            params[CHAT_LAST_NAME_PARAM] = _chatLastName;
           break;
        case GETCHATUSERDETAIL:
            params[CHAT_USER_IDS_PARAM] = _chatUserId;
        default:
            break;
    }
    return params;
}

-(NSString *) serverURL{
    switch (_requestType) {
        case SAVECHATPROFILEREQUEST:
            return @"/user/saveQBChatUserInfo.htm";
        case GETCHATUSERDETAIL:
            return @"/user/getQBUserDetail.htm";
        default:
            return nil;
    }
}

-(NSString *) mappingKey{
    switch (_requestType) {
        case GETCHATUSERDETAIL:
            return @"qbUserDetail";
        default:
            return nil;
    }
}

-(RKObjectMapping *) objectMapping{
    switch (_requestType) {
        case GETCHATUSERDETAIL:
            return [UserContactDetail getUserContactDetailObjectMapping];
        default:
            return nil;
    }
}
@end
