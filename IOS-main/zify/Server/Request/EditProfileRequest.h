//
//  EditProfileRequest.h
//  zify
//
//  Created by Anurag S Rathor on 30/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ServerRequest.h"

@interface EditProfileRequest : ServerRequest
@property (nonatomic, strong) NSString * gender;
@property (nonatomic, strong) NSString * dob;
@property (nonatomic, strong) NSString * companyName;
@property (nonatomic, strong) NSString * companyEmail;
@property (nonatomic, strong) NSString * bloodGroup;
@property (nonatomic, strong) NSString * emergencyContact;
@property (nonatomic, strong) NSString * countryCode;
-(id)initWithGender:(NSString *)gender andDOB:(NSString *)dob andCompanyName:(NSString *)companyName andCompanyEmail:(NSString *)companyEmail andBloodGroup:(NSString *)bloodGroup andEmergencyContact:(NSString *)emergencyContact andCountryCode:(NSString *)countryCode;
@end
