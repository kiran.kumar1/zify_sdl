//
//  StripePaymentPersistRequest.h
//  zify
//
//  Created by Anurag S Rathor on 13/10/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"

enum StripePaymentRequestType{
    STRIPEPAYMENTREQUEST,STRIPEPAYMENTRESPONSE
};

@interface StripePaymentPersistRequest : ServerRequest
@property (nonatomic) enum StripePaymentRequestType requestType;
@property (nonatomic, strong) NSString * amount;
@property (nonatomic, strong) NSString * currency;
@property (nonatomic, strong) NSString * zifyPaymentRefId;
@property (nonatomic, strong) NSNumber * failureCode;
@property (nonatomic, strong) NSString * stripeToken;
@property (nonatomic, strong) NSString * failureDescription;
-(id)initWithRequestType:(enum StripePaymentRequestType)requestType andAmount:(NSString *)amount andCurrency:(NSString *)currency;
-(id)initWithRequestType:(enum StripePaymentRequestType)requestType andStripeToken:(NSString *)stripeToken andZifyPaymentRefId:(NSString *)zifyPaymentRefId andFailureCode:(NSNumber *)code andFailureDescription:(NSString *)description;
@end
