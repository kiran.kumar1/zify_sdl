//
//  PublishDriveRequest.m
//  zify
//
//  Created by Anurag S Rathor on 23/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "PublishDriveRequest.h"
#define SEATS_NUM_PARAM @"numOfSeats"
#define DEPARTURE_TIME_PARAM @"departureTime"
#define ROUTINE_PARAM @"routine"
#define RETURN_TIME_PARAM @"returnTime"
#define ROUTE_ID_PARAM @"routeId"
#define COUNTRY_CODE_PARAM @"countryCode"
#import "AppDelegate.h"

@implementation PublishDriveRequest
-(id)initWithDepartureTime:(NSString *)departureTime andRoutine:(NSString *)routine andReturnTime:(NSString *)returnTime{
    self = [super init];
    if (self) {
        _departureTime = departureTime;
        _routine = routine;
        _returnTime = returnTime;
    }
    return self;
}


//-(id)initWithSTartPoint:(NSDictionary *)startPoint andEndPoint:(NSDictionary *)endPoint andPolylineRoute:(NSDictionary *)polylineRouteInfo andTravelMode:(NSString *)travelMode andNumberOfSeats:(NSNumber *)seatsCountStr andDepartureTime:(NSString *)departureTime {
//    self = [super init];
//    if (self) {
//        _departureTime = departureTime;
//        _startPoint = startPoint;
//        _endPoint = endPoint;
//        _polilineRouteInfo = polylineRouteInfo;
//        _userTravelMode = travelMode;
//        _numOfSeats = seatsCountStr;
//    }
//    return self;
//}



-(void)setSeats:(NSNumber *)numOfSeats andRouteId:(NSString *)routeId{
    _numOfSeats = numOfSeats;
    _routeId = routeId;
}

-(NSString *) serverURL{
    // return @"ridematchingdev/driver/V3/drive";
    return @"/driver/V2/createManualDrive";
}
-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    UserProfile *profile = [UserProfile getCurrentUser];
    params[SEATS_NUM_PARAM] = _numOfSeats;
   // params[DEPARTURE_TIME_PARAM] = [[AppDelegate getAppDelegateInstance] converDateToISOStandard:_departureTime];
    params[DEPARTURE_TIME_PARAM] = _departureTime;
    params[ROUTINE_PARAM] = _routine;
    params[RETURN_TIME_PARAM] = _returnTime;
    params[ROUTE_ID_PARAM] = _routeId;
    params[COUNTRY_CODE_PARAM] = profile.country;
    
    /*
    params[@"departureTime"] = _departureTime;
    params[@"startPoint"] = _startPoint;
    params[@"endPoint"] =_endPoint;
    params[@"route"] = _polilineRouteInfo;
    params[@"country"] = profile.country;
    params[@"travelMode"] = _userTravelMode;
    params[@"numOfSeats"] = _numOfSeats;
    */
    
    return params;
}
-(NSString *) mappingKey{
    return @"zifyDrive";
}
-(BOOL)isForZenParkService{
    return YES;
}

//-(BOOL)isJSONRequestFormat{
//    return true;
//}
//-(BOOL)overrideBaseURL{
//    return true;
//}
@end
