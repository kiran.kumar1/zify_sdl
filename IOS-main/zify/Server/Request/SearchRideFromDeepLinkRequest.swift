//
//  SearchRideFromDeepLinkRequest.swift
//  zify
//
//  Created by Anurag Rathor on 09/10/18.
//  Copyright © 2018 zify. All rights reserved.
//

import UIKit


class SearchRideFromDeepLinkRequest: ServerRequest {
    let DRIVER_ID_KEY = "driverId"
    let DRIVE_ID_KEY = "driveId"
    let RIDER_ID_KEY = "riderId"
    let COUNTRY_CODE_KEY = "countryCode"

    var userDriverId:NSNumber = NSNumber()
    var userDriveId:NSNumber = NSNumber()
    
    override init () {
        // uncomment this line if your class has been inherited from any other class
        super.init()
    }
    
   /* func initForRequest(driverId:String, driveId:String) -> self {
        self.init()
        
        return self;
    }*/
    
    convenience init(withDriverId:NSNumber, withDriveId:NSNumber) {
        self.init()
        self.userDriverId = withDriverId;
        self.userDriveId = withDriveId;
    }
    override func urlparams() -> [AnyHashable : Any] {
        let params =  self.getUserCommonParams() as NSMutableDictionary
        params[DRIVER_ID_KEY] = self.userDriverId
        params[DRIVE_ID_KEY] = self.userDriveId;
        let profile = UserProfile.getCurrentUser()
        params[RIDER_ID_KEY] = (profile?.userId)!
        params[COUNTRY_CODE_KEY] = profile?.country
        
        return params as! [AnyHashable : Any]
    }
    override func serverURL() -> String! {
        return "driver/V2/getRideWithMeDrive"
    }
    
    override func isForZenParkService() -> Bool {
        return true
    }
}
