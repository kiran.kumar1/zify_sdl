//
//  LoginRequest.h
//  zify
//
//  Created by Anurag S Rathor on 05/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  "ServerRequest.h"

@interface LoginRequest : ServerRequest
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * password;
-(id)initWithEmail:(NSString *)email andPassword:(NSString *)password;
@end
