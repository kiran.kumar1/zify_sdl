//
//  ServerRequest.h
//  zify
//
//  Created by Anurag S Rathor on 27/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface ServerRequest : NSObject
-(NSDictionary *) urlparams;
-(NSString *) serverURL;
-(NSString *) mappingKey;
-(BOOL) saveInDatabase;
-(RKObjectMapping *) objectMapping;
-(BOOL) isActualResponseSingleAttribute;
-(BOOL)isSuccessResponseCode:(int)responseCode;
-(BOOL)isPaymentRequest;
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData;
-(BOOL)isJSONRequestFormat;
-(NSString *) imageParamName;
-(NSMutableDictionary *)getUserCommonParams;
-(BOOL)overrideBaseURL;
-(BOOL)isForFDJService;
-(BOOL)isForZenParkService;
-(BOOL)isForZenParkSignIn;
-(BOOL)isForV2Version;
-(BOOL)isGetRequest;
-(BOOL)isForBankRequest;
-(BOOL)isForReferAndEarnRequest;


@end
