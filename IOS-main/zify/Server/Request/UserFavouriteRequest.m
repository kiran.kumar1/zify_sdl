//
//  UserFavouriteRequest.m
//  zify
//
//  Created by Anurag S Rathor on 23/02/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "UserFavouriteRequest.h"

#define SRC_LAT_PARAM @"srcLat"
#define SRC_LNG_PARAM @"srcLong"
#define DEST_LAT_PARAM @"destLat"
#define DEST_LNG_PARAM @"destLong"
#define DRIVER_ID_PARAM @"driverId"
#define ROUTE_ID_PARAM @"routeId"
#define FLAG_PARAM @"flag"
#define DEPARTURE_TIME_PARAM @"departureTime"

@implementation UserFavouriteRequest
-(id)initWithRequestType:(enum UserFavouriteRequestType)requestType andSearchRide:(SearchRideEntity *)searchRide andSourceLocality:(LocalityInfo *)sourceLocality andDestLocality:(LocalityInfo *)destinationLocality andSubscibeFlag:(NSNumber *)subscribeFlag{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _searchRide = searchRide;
        _sourceLocality = sourceLocality;
        _destinationLocality = destinationLocality;
        _subscribeFlag = subscribeFlag;
    }
    return self;
}

-(id)initWithRequestType:(enum UserFavouriteRequestType)requestType andDepartureTime:(NSString *)departureTime{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _departureTime = departureTime;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    switch (_requestType) {
        case SUBSCRIBEFAVDRIVER:{
            double sourceLat = _sourceLocality != nil ? (double)_sourceLocality.latLng.latitude : 0;
            double sourceLng = _sourceLocality != nil ? (double)_sourceLocality.latLng.longitude : 0;
            double destLat = _destinationLocality != nil ? (double)_destinationLocality.latLng.latitude  : 0;
            double destLng = _destinationLocality != nil ? (double)_destinationLocality.latLng.longitude : 0;
            params[SRC_LAT_PARAM] = [[NSNumber numberWithDouble:sourceLat] stringValue];
            params[SRC_LNG_PARAM] = [[NSNumber numberWithDouble:sourceLng] stringValue];
            params[DEST_LAT_PARAM] = [[NSNumber numberWithDouble:destLat] stringValue];
            params[DEST_LNG_PARAM] = [[NSNumber numberWithDouble:destLng] stringValue];
            params[DRIVER_ID_PARAM] = _searchRide.driverId;
            params[ROUTE_ID_PARAM] = _searchRide.driverRouteId;
            params[FLAG_PARAM] = _subscribeFlag;
            params[DEPARTURE_TIME_PARAM] = _searchRide.departureTime;
            break;
        }
        case GETUSERFAVOURITES:
            params[DEPARTURE_TIME_PARAM] = _departureTime;
            break;
        default:
            break;
    }
    return params;
}

-(NSString *) serverURL{
    switch (_requestType) {
        case SUBSCRIBEFAVDRIVER:
            return @"/user/subscribeFavDriver.htm";
        case GETUSERFAVOURITES:
            return @"/driver/getFavDrives.htm";
        default:
            return nil;
    }
}

-(NSString *) mappingKey{
    switch (_requestType) {
        case GETUSERFAVOURITES:
            return @"drives";
        default:
            return nil;
    }
}

-(RKObjectMapping *) objectMapping{
    switch (_requestType) {
        case GETUSERFAVOURITES:
            return [SearchRide getSearchRideObjectMapping];
        default:
            return nil;
    }
}
@end
