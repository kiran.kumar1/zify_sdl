//
//  GenericRequest.m
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "GenericRequest.h"
#import "TripRide.h"
#import "TripDrive.h"
#import "UserWallet.h"
#import "WalletRechargeStatement.h"
#import "TripHistory.h"
#import "RateCardResponse.h"
#import "WalletTransactionStatement.h"
#import "UserProfile.h"
#import "UserNotification.h"

@implementation GenericRequest
-(id)initWithRequestType:(enum RequestType)requestType{
    self = [super init];
    if (self) {
        _requestType = requestType;
    }
    return self;
}
-(NSDictionary *) urlparams{
    return [self getUserCommonParams];
}
-(NSString *) serverURL{
    switch (_requestType) {
        case UPCOMINGRIDESREQUEST:
            return @"/rider/getUpcomingRidesJson.htm";
        case UPCOMINGDRIVESREQUEST:
            return @"/driver/getUpcomingDrivesJson.htm";
        case TRIPHISTORYREQUEST:
            return @"/rider/getTripHistoryJson.htm";
        case RIDERWALLETREQUEST:
            return @"/rider/getRiderWallet.htm";
        case RATECARDREQUEST:
            return @"/user/getGlobalRateCard.htm";
        case TRANSACTIONSTATEMENTREQUEST:
             return @"/user/getTransactionHistory.htm";
        case RECHARGESTATEMENTREQUEST:
            return @"/rider/getPaymentHistoryJson.htm";
        case LOGOUTREQUEST:
            return @"/user/logout.htm";
        case USERNOTIFICATIONSREQUEST:
            return @"/user/getNotifications.htm";
        case CURRENTORNEXTRIDEDEPARTURETIMEREQUEST:
            return @"/driver/getCurrentOrNextRideDepartureTime.htm";
        case AUTOLOGINREQUEST:
            return @"/user/autoLogin.htm";
        default:
            return nil;
    }
}
-(NSString *) mappingKey{
    switch (_requestType) {
        case UPCOMINGRIDESREQUEST:
            return @"upcomingRides";
        case UPCOMINGDRIVESREQUEST:
            return @"upcomingDrives";
        case TRIPHISTORYREQUEST:
            return @"tripHistory";
        case RIDERWALLETREQUEST:
            return @"wallet";
        case RATECARDREQUEST:
            return @"rateCard";
        case TRANSACTIONSTATEMENTREQUEST:
            return @"transactionHistory";
        case RECHARGESTATEMENTREQUEST:
            return @"paymentHistory";
        case LOGOUTREQUEST:
            return nil;
        case USERNOTIFICATIONSREQUEST:
            return @"notifications";
        case CURRENTORNEXTRIDEDEPARTURETIMEREQUEST:
            return @"currentOrNextRideDepartureTime";
        case AUTOLOGINREQUEST:
            return @"user";
        default:
            return nil;
    }
}
-(RKObjectMapping *) objectMapping{
    switch (_requestType) {
        case UPCOMINGRIDESREQUEST:
            return [TripRide getTripRideObjectMapping];
        case UPCOMINGDRIVESREQUEST:
            return [TripDrive getTripDriveObjectMapping];
        case TRIPHISTORYREQUEST:
            return [TripHistory getTripHistoryObjectMapping];
        case RIDERWALLETREQUEST:
            return [UserWallet getUserWalletMapping];
        case TRANSACTIONSTATEMENTREQUEST:
            return [WalletTransactionStatement getWalletTransactionStatementObjectMapping];
        case RECHARGESTATEMENTREQUEST:
            return [WalletRechargeStatement getWalletRechargeStatementObjectMapping];
        case RATECARDREQUEST:
            return [RateCardResponse getRateCardResponseObjectMapping];
        case LOGOUTREQUEST:
            return nil;
        case USERNOTIFICATIONSREQUEST:
            return [UserNotification getUserNotificationObjectMapping];
        case AUTOLOGINREQUEST:
            return [UserProfile getUserProfileObjectMapping];
        default:
            return nil;
    }
}
-(BOOL) isActualResponseSingleAttribute{
    switch (_requestType) {
        case CURRENTORNEXTRIDEDEPARTURETIMEREQUEST:
            return true;
        default:
            return false;
    }
}

-(BOOL)isSuccessResponseCode:(int)responseCode{
    switch (_requestType) {
        case AUTOLOGINREQUEST:
            return true;
        default:
            return responseCode == 1;
    }
}

-(BOOL) saveInDatabase{
    switch (_requestType) {
        case AUTOLOGINREQUEST:
            return true;
        default:
            return false;
    }
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    switch (_requestType) {
        case AUTOLOGINREQUEST:{
            [UserProfile deleteCurrentUser];
            [UserProfile createCurrentUser:responseData];
            break;
        }
        default:
            break;
    }
}
@end
