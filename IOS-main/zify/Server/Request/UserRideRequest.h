//
//  UserRideRequest.h
//  zify
//
//  Created by Anurag S Rathor on 22/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"
#import "LocalityInfo.h"

enum RideRequestType{
    SEARCHRIDE,SEARCHPAGINATEDRIDE
};

@interface UserRideRequest : ServerRequest
@property(nonatomic,strong) LocalityInfo *sourceLocality;
@property(nonatomic,strong) LocalityInfo *destinationLocality;
@property(nonatomic,strong) NSString *rideDate;
@property(nonatomic,strong) NSNumber *seats;
@property (nonatomic) enum RideRequestType requestType;
@property(nonatomic,strong) NSString *lastDeptTime;
@property(nonatomic,strong) NSString *merchantId;
@property(nonatomic,strong) NSNumber *offSet;
@property(nonatomic,strong) NSString *startTime;
@property(nonatomic,strong) NSString *endTime;


-(RKObjectMapping *) objectMapping;
-(id)initWithRequestType:(enum RideRequestType)requestType andSourceLocality:(LocalityInfo *)sourceLocality andDestinationLocality:(LocalityInfo *)destinationLocality andRideDate:(NSString *)rideDate andSeats:(NSNumber *)seats andMerchantId:(NSString *)merchantId withOffsetValue:(NSNumber *)offset withStartTime:(NSString *)startTime withEndTime:(NSString *)endTime;

@end
