//
//  VerifyMobileRequest.m
//  zify
//
//  Created by Anurag S Rathor on 14/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "VerifyMobileRequest.h"
#import "UserProfile.h"

#define USER_TOKEN_PARAM @"userToken"
#define OTP_PARAM @"otp"

@interface VerifyMobileRequest ()

@end

@implementation VerifyMobileRequest

-(id)initWithOtp:(NSString *)otp andRequestType:(enum VerifyMobileRequestType) requestType{
    self = [super init];
    if (self) {
        _otp = otp;
        _requestType = requestType;
    }
    return self;
}


-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    switch (_requestType) {
        case VERIFYMOBILE:
            params[OTP_PARAM] = _otp;
            params[USER_TOKEN_PARAM] = [UserProfile getCurrentUserToken];
            break;
        default:
            break;
    }
    return params;
}

-(NSString *) serverURL{
    switch (_requestType) {
        case VERIFYMOBILEOTP:
            return @"/user/getVerifyMobileOTP.htm";
        case VERIFYMOBILE:
            return @"/user/verifyMobile.htm";
        default:
            return nil;
    }
}

-(NSString *) mappingKey{
    return @"user";
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL) saveInDatabase{
    if(_requestType == VERIFYMOBILE){
        return true;
    }
    return false;
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    if(_requestType == VERIFYMOBILE){
        [UserProfile deleteCurrentUser];
        [UserProfile createCurrentUser:responseData];
    }
}
@end
