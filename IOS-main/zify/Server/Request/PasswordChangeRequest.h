//
//  PasswordChangeRequest.h
//  zify
//
//  Created by Anurag S Rathor on 10/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ServerRequest.h"
//#import "zify-Swift.h"

enum PasswordRequestType{
    GETFORGOTPASSWORDOTP,SAVEFORGOTPASSWORD,GETRESETPASSWORDOTP,SAVERESETPASSWORD
};

@interface PasswordChangeRequest : ServerRequest
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userPassword;
@property (nonatomic, strong) NSString *otp;
@property (nonatomic) enum PasswordRequestType requestType;
-(id)initWithdUserEmail:(NSString *)userEmail andUserPassword:(NSString *)userPassword andOtp:(NSString *)otp andRequestType:(enum PasswordRequestType) requestType;
@end
