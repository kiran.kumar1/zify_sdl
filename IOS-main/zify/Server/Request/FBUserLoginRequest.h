//
//  FBUserLoginRequest.h
//  zify
//
//  Created by Anurag S Rathor on 10/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"

@interface FBUserLoginRequest : ServerRequest
@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * lastName;
@property (nonatomic, strong) NSString * userEmail;
@property (nonatomic, strong) NSString * isdCode;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * gender;
@property (nonatomic, strong) NSString * fbProfileId;
-(id)initWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName andUserEmail:(NSString *)userEmail andisdCode:(NSString *)isdCode andMobile:(NSString *)mobile andGender:(NSString *)gender andFbProfileId:(NSString *) fbProfileId;
@end
