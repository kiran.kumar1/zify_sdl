//
//  PasswordChangeRequest.m
//  zify
//
//  Created by Anurag S Rathor on 10/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "PasswordChangeRequest.h"
#import "CurrentLocale.h"
#import "CurrentLocationTracker.h"
#import "zify-Swift.h"

#define OTP_PARAM @"otp"
#define USER_EMAIL_PARAM @"userEmail"
#define NEW_PASSWORD_PARAM @"newPswd"
#define ISO_CODE_PARAM @"isoCode"
#define COUNTRY_PARAM @"country"
#define USER_LAT_PARAM @"userLat"
#define USER_LONG_PARAM @"userLong"

@implementation PasswordChangeRequest
-(id)initWithdUserEmail:(NSString *)userEmail andUserPassword:(NSString *)userPassword andOtp:(NSString *)otp andRequestType:(enum PasswordRequestType) requestType{
    self = [super init];
    if (self) {
        _userEmail = userEmail;
        _userPassword = userPassword;
        _otp = otp;
        _requestType = requestType;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSString *isoCode = [[CurrentLocale sharedInstance] getMobileISOCode];
    switch (_requestType) {
        case SAVEFORGOTPASSWORD:
            params[OTP_PARAM] = _otp;
            params[NEW_PASSWORD_PARAM] = _userPassword;
        case GETFORGOTPASSWORDOTP:
            params[USER_EMAIL_PARAM] = _userEmail;
            if(isoCode){
                params[ISO_CODE_PARAM] = isoCode;
            } else{
                LocalityInfo *localityInfo = [CurrentLocationTracker sharedInstance].currentLocalityInfo;
                if(localityInfo){
                    params[COUNTRY_PARAM] = localityInfo.country == nil ? @"" : localityInfo.country;
                    params[USER_LAT_PARAM] = [[NSNumber numberWithDouble:localityInfo.latLng.latitude] stringValue];
                    params[USER_LONG_PARAM] = [[NSNumber numberWithDouble:localityInfo.latLng.longitude] stringValue];
                }
            }
            break;
        case GETRESETPASSWORDOTP:
            params = [self getUserCommonParams];
            params[USER_EMAIL_PARAM] = _userEmail;
            break;
        case SAVERESETPASSWORD:
            params = [self getUserCommonParams];
            params[OTP_PARAM] = _otp;
            params[NEW_PASSWORD_PARAM] = _userPassword;
            break;
        default:
            break;
    }
    return params;
}
-(NSString *) serverURL{
    switch (_requestType) {
        case GETFORGOTPASSWORDOTP:
            return @"/user/getForgotPswdOTP.htm";
        case SAVEFORGOTPASSWORD:
            return @"/user/saveForgotPswd.htm";
        case GETRESETPASSWORDOTP:
            return  @"/user/getForgotPswdOTP.htm";      //@"/user/getResetPswdOTP.htm";
        case SAVERESETPASSWORD:
            return @"/user/saveResetPswd.htm";
        default:
            return nil;
    }
}
@end
