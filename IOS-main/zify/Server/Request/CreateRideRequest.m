//
//  CreateRideRequest.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "CreateRideRequest.h"
#import "AppDelegate.h"
//#import "zify-Swift.h"

#define SEATS_NUM_PARAM @"numOfSeats"
#define DEPARTURE_TIME_PARAM @"departureTime"
#define ROUTE_ID_PARAM @"routeId"
#define CITY_PARAM @"city"
#define DRIVE_ID_PARAM @"driveId"
#define DRIVER_ID_PARAM @"driverId"
#define COUNTRY_CODE_PARAM @"countryCode"
#define COST_PER_SEAT @"costToRider"
#define SRC_NEAR_LAT @"srcNearLat"
#define SRC_NEAR_LNG @"srcNearLng"
#define DEST_NEAR_LAT @"destNearLat"
#define DEST_NEAR_LNG @"destNearLng"


#define SEARCH_SRC_NEAR_LAT @"srcActualLat"
#define SEARCH_SRC_NEAR_LNG @"srcActualLng"
#define SEARCH_DEST_NEAR_LAT @"destActualLat"
#define SEARCH_DEST_NEAR_LNG @"destActualLng"

@implementation CreateRideRequest
-(id)initWithSearchRideEntity:(SearchRideEntity *)searchRide withSearchData:(UserSearchData *)userData{
    self = [super init];
    if (self) {
        _searchRideEntity = searchRide;
        _userData = userData;
    }
    return self;
}
-(id)initWithSearchRide:(SearchRide *)searchRide withSearchData:(UserSearchData *)userData{
    self = [super init];
    if (self) {
        _searchRide = searchRide;
        _userData = userData;

    }
    return self;
}


-(NSString *) serverURL{
    return @"/rider/V2/requestRide";
}
-(NSDictionary *) urlparams{
    
    NSMutableDictionary *params = [self getUserCommonParams];
    if(_searchRide){
    params[ROUTE_ID_PARAM] = _searchRide.routeId;
    params[DEPARTURE_TIME_PARAM] = _searchRide.departureTime;
    params[SEATS_NUM_PARAM] = @1;
    params[CITY_PARAM] = _searchRide.city;
    params[DRIVE_ID_PARAM] = _searchRide.driveId;
    params[DRIVER_ID_PARAM] = _searchRide.driverId;
    }else if(_searchRideEntity){
        params[ROUTE_ID_PARAM] = _searchRideEntity.routeId;
        params[DEPARTURE_TIME_PARAM] = [[AppDelegate getAppDelegateInstance] converDateToISOStandardForRideRequest:_searchRideEntity.departureTime];  //
        params[SEATS_NUM_PARAM] = @1;
        params[CITY_PARAM] = _searchRideEntity.city;
        params[DRIVE_ID_PARAM] = _searchRideEntity.driveId;
        params[DRIVER_ID_PARAM] = _searchRideEntity.driverId;
        params[COST_PER_SEAT] = _searchRideEntity.amountPerSeat;
        params[SRC_NEAR_LAT] = [[NSNumber alloc] initWithDouble:_searchRideEntity.srcNearLat.doubleValue];
        params[SRC_NEAR_LNG] = [[NSNumber alloc] initWithDouble:_searchRideEntity.srcNearLong.doubleValue];
        params[DEST_NEAR_LAT] = [[NSNumber alloc] initWithDouble:_searchRideEntity.destNearLat.doubleValue];
        params[DEST_NEAR_LNG] = [[NSNumber alloc] initWithDouble:_searchRideEntity.destNearLong.doubleValue];
        
       // CLLocationCoordinate2D *searchSource = _userData.sourceLocality.latLng;

        params[SEARCH_SRC_NEAR_LAT] = [[NSNumber alloc] initWithDouble:_userData.sourceLocality.latLng.latitude];
        params[SEARCH_SRC_NEAR_LNG] = [[NSNumber alloc] initWithDouble:_userData.sourceLocality.latLng.longitude];
        params[SEARCH_DEST_NEAR_LAT] = [[NSNumber alloc] initWithDouble:_userData.destinationLocality.latLng.latitude];
        params[SEARCH_DEST_NEAR_LNG] = [[NSNumber alloc] initWithDouble:_userData.destinationLocality.latLng.longitude];
    }
    UserProfile *profile = [UserProfile getCurrentUser];
    params[COUNTRY_CODE_PARAM] = profile.country;
    return params;
}

-(NSString *) mappingKey{
    return @"rechargeAmount";
}

-(BOOL) isActualResponseSingleAttribute{
    return true;
}

-(BOOL)isSuccessResponseCode:(int)responseCode{
    return responseCode == 1 || responseCode == -30;
}
-(BOOL)isForZenParkService{
    return true;
}

@end
