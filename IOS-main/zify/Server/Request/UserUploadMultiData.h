//
//  UserRideRequest.h
//  zify
//
//  Created by Anurag S Rathor on 22/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"
#import "LocalityInfo.h"

enum UPLOADTYPE{
    ID_CARD_TYPE,VEHICLE_TYPE,PROFILE_TYPE
};

@interface UserUploadMultiData : ServerRequest
@property (nonatomic) enum UPLOADTYPE uploadType;
-(id)initWithUploadType:(enum UPLOADTYPE)type;

@end
