//
//  RideDriveActionRequest.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ServerRequest.h"

enum RideDriveActionRequestType{
    CANCELRIDE,CANCELDRIVE,CONFIRMRIDE,DECLINERIDE,STARTDRIVE,COMPLETEDRIVE,STARTRIDE,COMPLETERIDE,CURRENTTRIPNPENDINGRATING
};

@interface RideDriveActionRequest : ServerRequest
@property (nonatomic,strong) NSNumber *rideId;
@property (nonatomic,strong) NSNumber *driveId;
@property (nonatomic,strong) NSNumber *driverId;
@property (nonatomic,strong) NSString *routeId;
@property (nonatomic,strong) NSNumber *numOfSeats;
@property (nonatomic,strong) NSNumber *userLat;
@property (nonatomic,strong) NSNumber *userLong;
@property (nonatomic,strong) NSString *pathLine;
@property (nonatomic) enum RideDriveActionRequestType requestType;
-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andDriveId:(NSNumber *)driveId;
-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andDriveId:(NSNumber *)driveId  andRideId:(NSNumber *)rideId;
-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andRideId:(NSNumber *)rideId andDriveId:(NSNumber *)driveId andDriverId:(NSNumber *)driverId andRouteId:(NSString *)routeId;
-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andRideId:(NSNumber *)rideId andDriveId:(NSNumber *)driveId andSeatsNum:(NSNumber *)numOfSeats;
-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andDriveId:(NSNumber *)driveId andUserLat:(NSNumber *)userLat andUserLong:(NSNumber *)userLong andPathLine:(NSString *)pathLine;
-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andDriveId:(NSNumber *)driveId andRideId:(NSNumber *)rideId andUserLat:(NSNumber *)userLat andUserLong:(NSNumber *)userLong andPathLine:(NSString *)pathLine;
-(id)initWithRequestType:(enum RideDriveActionRequestType)requestType andUserLat:(NSNumber *)userLat andUserLong:(NSNumber *)userLong;
@end
