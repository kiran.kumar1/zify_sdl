//
//  BankAccountRequest.h
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "ServerRequest.h"

enum BankAccountRequestType{
    ADDBANK,REDEMPTIONDETAILS,SAVEDBANKDETAILS,DELETEBANK,REGISTERREDEEM
};

@interface BankAccountRequest : ServerRequest
@property (nonatomic) enum BankAccountRequestType requestType;
@property (nonatomic, strong) NSString * accountType;
@property (nonatomic, strong) NSString * accountHolderName;
@property (nonatomic, strong) NSString * accountNumber;
@property (nonatomic, strong) NSString * ifscCode;
@property (nonatomic, strong) NSString * bankName;
@property (nonatomic, strong) NSNumber * bankDetailId;
@property (nonatomic, strong) NSString * redeemAmount;

-(id)initWithRequestType:(enum BankAccountRequestType)requestType;
-(id)initWithRequestType:(enum BankAccountRequestType)requestType andBankAccountDetaiId:(NSNumber *)bankDetailId andRedeemAmount:(NSString *)redeemAmount;
-(id)initWithRequestType:(enum BankAccountRequestType)requestType andAccountType:(NSString *)accountType andAccountHolderName:(NSString *)accountHolderName andAccountNumber:(NSString *)accountNumber andifscCode:(NSString *)ifscCode andBankname:(NSString *)bankName;
@end
