//
//  UserIdentityCardSaveRequest.m
//  zify
//
//  Created by Anurag S Rathor on 02/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserIdentityCardSaveRequest.h"
#import "UserProfile.h"
#import "CurrentLocale.h"
#import "AppDelegate.h"
#define COUNTRY @"country"

@interface UserIdentityCardSaveRequest ()

@end

#define ID_CARD_TYPE_PARAM @"idCardType"
#define ID_CARD_IMG_FILE_PARAM @"idcardImgFile"

@implementation UserIdentityCardSaveRequest
-(id)initWithIdentityCardType:(NSString *)identityCardType withImagePath:(NSString *)imagePath{
    self = [super init];
    if (self) {
         _identityCardType = identityCardType;
        _idCardImagePath = imagePath;
    }
    return self;
}
-(NSDictionary *) urlparams{
    NSMutableDictionary *params = (NSMutableDictionary *)[self getProfileParamsDict];
    params[ID_CARD_TYPE_PARAM] =  [[CurrentLocale sharedInstance] getIdentityCardType:_identityCardType];
    UserProfile *userProfile =[UserProfile getCurrentUser];
    NSMutableDictionary *params1 = [[NSMutableDictionary alloc] init];
    params1[@"userId"] = [UserProfile getCurrentUserId];
    params1[@"profileImgUrl"] = userProfile.profileImgUrl;
    params1[@"idCardNum"] = userProfile.userDocuments.idCardNum;
    params1[@"idCardType"] = _identityCardType;
    params1[@"isVehicleImgUploaded"] = userProfile.userDocuments.isVehicleImgUploaded;
    params1[@"isIdCardDocUploaded"] = @"1";
    params1[@"vehicleRegistrationNum"] = userProfile.userDocuments.vehicleRegistrationNum;
    params1[@"vehicleInsuranceNum"] = userProfile.userDocuments.vehicleInsuranceNum;
    params1[@"drivingLicenceNum"] = userProfile.userDocuments.drivingLicenceNum;
    params1[@"vehicleModel"] = userProfile.userDocuments.vehicleModel;
    params1[@"vehicleImgUrl"] = userProfile.userDocuments.vehicleImgUrl;
    params1[@"idCardImgUrl"] = _idCardImagePath;
    params[@"userDocs"] = params1;
    NSDictionary *travelPreferencesDict = [self getUserPreferencesDictToRequest];
    if(travelPreferencesDict){
        params[@"userPreferences"] = travelPreferencesDict;
    }
    return params;
}

-(NSString *) serverURL{
    return @"/user/V2/updateprofile";
}

-(NSString *) mappingKey{
    return @"user";
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL) saveInDatabase{
    return true;
}
-(BOOL)isForV2Version{
    return true;
}
-(BOOL)isJSONRequestFormat{
    return YES;
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    [UserProfile deleteCurrentUser];
    [UserProfile createCurrentUser:responseData];
}
-(NSString *) imageParamName{
    return ID_CARD_IMG_FILE_PARAM;
}
-(NSDictionary *)getProfileParamsDict{
    
    NSMutableDictionary *params = [self getUserCommonParams];
    UserProfile *profile = [UserProfile getCurrentUser];
    params[@"loginMode"]= profile.loginMode;
    params[@"userId"] = [UserProfile getCurrentUserId];
    params[@"firstName"]= profile.firstName;
    params[@"lastName"]= profile.lastName;
    params[@"userName"]= profile.userName;
    params[@"emailId"]= profile.emailId;
    params[@"isdCode"]= profile.isdCode;
    params[@"mobile"]= profile.mobile;
    params[@"gender"]= profile.gender;
    params[@"zifyPromotionalPoints"]= profile.zifyPromotionalPoints;
    params[@"zifyUserPoints"]= profile.zifyUserPoints;
    params[@"zifyCash"]= profile.zifyCash;
    params[@"currency"]= profile.currency;
    params[@"profileCompleteness"]= profile.profileCompleteness;
    params[@"userRatingMale"]= profile.userRatingMale;
    params[@"userRatingFemale"]= profile.userRatingFemale;
    params[@"numOfMalesRated"]= profile.numOfMalesRated;
    params[@"numOfFemalesRated"]= profile.numOfFemalesRated;
    params[@"userType"]= profile.userType;
    params[@"userStatus"]= profile.userStatus;
    params[@"userStatusReason"]= profile.userStatusReason;
    params[@"totalRides"]= profile.totalRides;
    params[@"totalDistance"]=  profile.totalDistance;
    params[@"companyEmail"]= profile.companyEmail;
    params[@"city"]= profile.city;
    params[@"country"]= profile.country;
    params[@"pincode"]= profile.pincode;
    params[@"mobileVerified"]= profile.mobileVerified;
    params[@"callId"]= profile.callId;
    params[@"distanceUnit"]= profile.distanceUnit;
    params[@"isGlobal"]= profile.isGlobal;
    params[@"isGlobalPayment"]= profile.isGlobalPayment;
    params[@"dob"] = [[AppDelegate getAppDelegateInstance] convertDOBFROMServiceTOReqFormat];
    params[@"companyName"] = profile.companyName;
    params[@"bloodGroup"] = profile.bloodGroup;
    params[@"emergencyContact"] = profile.emergencyContact;

    return params;
}

-(NSDictionary *)getUserPreferencesDictToRequest{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    UserProfile *profile = [UserProfile getCurrentUser];
    UserPreferences *travelPreferences = profile.userPreferences;
    if(!travelPreferences){
        return nil;
    }
    params[@"city"]= travelPreferences.city;
    params[@"destinationAddress"] = travelPreferences.destinationAddress;
    params[@"destinationLatitude"]= travelPreferences.destinationLatitude;
    params[@"destinationLongitude"]= travelPreferences.destinationLongitude;
    params[@"sourceAddress"]= travelPreferences.sourceAddress;
    params[@"sourceLatitude"]= travelPreferences.sourceLatitude;
    params[@"sourceLongitude"] = travelPreferences.sourceLongitude;
    params[@"returnTime"] = travelPreferences.returnTime;
    params[@"startTime"] = travelPreferences.startTime;
    params[@"onwardRouteId"] = travelPreferences.onwardRouteId;
    params[@"onwardPolyline"] = travelPreferences.onwardPolyline;
    params[@"returnRouteId"] = travelPreferences.returnRouteId;
    params[@"returnPolyline"] = travelPreferences.returnPolyline;
    params[@"userMode"] = travelPreferences.userMode;
    params[@"userId"] = [UserProfile getCurrentUserId];
    
    return params;
}
@end
