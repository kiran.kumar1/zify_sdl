//
//  ServerRequest.m
//  zify
//
//  Created by Anurag S Rathor on 27/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ServerRequest.h"
#import "CurrentLocale.h"
#import "UserProfile.h"
#import "NSString+Random.h"

#define USER_TOKEN_PARAM @"userToken"
#define SESSION_ID_PARAM @"sessionId"
#define LOCALE_STR_PARAM @"localeStr"
#define IS_GLOBAL_PARAM @"isGlobal"
#define IS_GLOBAL_PAYMENT_PARAM @"isGlobalPayment"
#define USER_ID @"userId"



@implementation ServerRequest
-(NSDictionary *) urlparams{
    return nil;
}
-(NSString *) serverURL{
    return nil;
}
-(NSString *) mappingKey{
    return nil;
}
-(RKObjectMapping *) objectMapping{
    return nil;
}
-(BOOL)saveInDatabase{
    return false;
}
-(BOOL) isActualResponseSingleAttribute{
    return false;
}
-(BOOL)isSuccessResponseCode:(int)responseCode{
    return responseCode == 1;
}
-(BOOL)isPaymentRequest{
    return false;
}
-(BOOL)isForFDJService{
    return false;
}
-(BOOL)isForV2Version{
    return false;
}

-(BOOL)isForZenParkService{
    return false;
}
-(BOOL)isForZenParkSignIn{
    return false;
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    
}
-(BOOL)isJSONRequestFormat{
    return false;
}
-(NSString *) imageParamName{
     return nil;
}


-(NSMutableDictionary *)getUserCommonParams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    NSString *userToken = [UserProfile getCurrentUserToken];
    if(userToken.length == 0 || userToken == nil){
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        userToken = [prefs objectForKey:@"userTokenValue"];
    }
    params[USER_TOKEN_PARAM] = userToken;
    
    NSString *sessionId = [[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    if(sessionId == nil){
        sessionId = [NSString randomAlphanumericStringWithLength:10];
        [[NSUserDefaults standardUserDefaults] setObject:sessionId forKey:@"sessionId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    params[SESSION_ID_PARAM] =[[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    params[LOCALE_STR_PARAM] = [currentLocale getLocaleString];
    params[IS_GLOBAL_PARAM] = [NSNumber numberWithBool:[currentLocale isGlobalLocale]];
    params[IS_GLOBAL_PAYMENT_PARAM] = [NSNumber numberWithBool:[currentLocale isGlobalPayment]];
    params[USER_ID] = [UserProfile getCurrentUserId];
    return params;
}

-(BOOL)overrideBaseURL{
    return false;
}
-(BOOL)isGetRequest{
    return false;
}
-(BOOL)isForBankRequest{
    return false;
}
- (BOOL)isForReferAndEarnRequest {
    return false;
}

@end
