//
//  UserChatDialogRequest.m
//  zify
//
//  Created by Anurag S Rathor on 08/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "UserChatDialogRequest.h"

#define CHAT_CONV_USER_ID1_PARAM @"qbUserId1"
#define CHAT_CONV_USER_ID2_PARAM @"qbUserId2"
#define CHAT_CONV_ID_PARAM @"qbConvId"

@implementation UserChatDialogRequest
-(id)initWithRequestType:(enum ChatDialogRequestType)requestType andConvUserId1:(NSString *)convUserId1 andConvUserId2:(NSString *)convUserId2 andConvId:(NSString *)convId{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _convUserId1 = convUserId1;
        _convUserId2 = convUserId2;
        _convId = convId;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    params[CHAT_CONV_USER_ID1_PARAM] = _convUserId1;
    params[CHAT_CONV_USER_ID2_PARAM] = _convUserId2;
    switch (_requestType) {
        case SAVECONVERSATIONINFO:
            params[CHAT_CONV_ID_PARAM] = _convId;
            break;
        default:
            break;
    }
    return params;
}


-(NSString *) serverURL{
    switch (_requestType) {
        case SAVECONVERSATIONINFO:
            return @"/user/saveQBConvInfo.htm";
        case FETCHCONVERSATIONINFO:
            return @"/user/fetchQBConvInfo.htm";
        default:
            return nil;
    }
}

-(NSString *) mappingKey{
    switch (_requestType) {
        case FETCHCONVERSATIONINFO:
            return @"qbConvId";
        default:
            return nil;
    }
}

-(BOOL) isActualResponseSingleAttribute{
    switch (_requestType) {
        case FETCHCONVERSATIONINFO:
            return true;
        default:
            return false;
    }
}
@end
