//
//  UserRegisterRequest.h
//  zify
//
//  Created by Anurag S Rathor on 09/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"

@interface GetZenParkpointsRequest : ServerRequest
@property (nonatomic, strong) NSString * south;
@property (nonatomic, strong) NSString * north;
@property (nonatomic, strong) NSString * east;
@property (nonatomic, strong) NSString * west;

-(id)initWithNorth:(NSString *)north andSouth:(NSString *)south andWest:(NSString *)west andEast:(NSString *)east;

@end
