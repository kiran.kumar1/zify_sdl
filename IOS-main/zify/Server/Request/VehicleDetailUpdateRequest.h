//
//  VehicleDetailUpdateRequest.h
//  zify
//
//  Created by Anurag S Rathor on 02/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ServerRequest.h"
enum UPDATE_TYPE{
    VEHICLE_IMAGE_TYPE,PROFILE_IMAGE_TYPE,MOBILE_NUMBER_UPDATE
};
@interface VehicleDetailUpdateRequest : ServerRequest
@property (nonatomic, strong) NSString * vehicleModel;
@property (nonatomic, strong) NSString * vehicleRegistrationNumber;
@property (nonatomic, strong) NSString * insuranceNumber;
@property (nonatomic, strong) NSString * drivingLicenseNumber;
@property (nonatomic, strong) NSString * vehicleImagelink;

@property (nonatomic, strong) NSString * mobileNUmber;
@property (nonatomic, strong) NSString * isdCode;
@property (nonatomic, strong) NSString * countryCode;

@property (nonatomic) enum UPDATE_TYPE updateType;

-(id)initWithVehicleModel:(NSString *)vehicleModel andVehicleRegistrationNumber:(NSString *)vehicleRegistrationNumber andInsuranceNumber:(NSString *)insuranceNumber andDrivingLicenseNumber:(NSString *)drivingLicenseNumber andWithVehicleImagePah:(NSString *)imagePath withUpdateType:(enum UPDATE_TYPE)type withMobileNumber:(NSString *)mobileNum withIsoCode:(NSString *)isdCode withCountryCode:(NSString *)countryCode;
@end
