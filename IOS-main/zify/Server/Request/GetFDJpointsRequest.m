//
//  UserRegisterRequest.m
//  zify
//
//  Created by Anurag S Rathor on 09/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "GetFDJpointsRequest.h"
#import "UserProfile.h"
#import "FDJLocationObject.h"

#define SRC_LAT @"x"
#define SRC_LONG @"y"
#define RADIUS @"r"
#define F @"f"

@implementation GetFDJpointsRequest
-(id)initWithSrcLatitude:(NSString *)srcLat andSrcLongitude:(NSString *)srcLong{
    self = [super init];
    if (self) {
        _sourceLat = srcLat;
        _sourceLong = srcLong;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[SRC_LAT] = _sourceLat;
    params[SRC_LONG] = _sourceLong;
    params[RADIUS] = @"200";
    params[F] = @"n";
    //params[SRC_LAT] = @"48.861087";
    //params[SRC_LONG] = @"2.351180";

    
    return params;
}
-(NSString *) serverURL{
    return @"/mapi/pdv/get";
}
-(NSString *) mappingKey{
    return @"fdjPos";
}

-(BOOL)isForFDJService{
    return true;
}
-(BOOL)overrideBaseURL{
    return true;
}
-(RKObjectMapping *) objectMapping{
    return [FDJLocationObject getFDJLocationObjectMapping];
}


@end
