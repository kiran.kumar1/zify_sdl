//
//  UserPreferencesRequest.m
//  zify
//
//  Created by Anurag S Rathor on 15/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserPreferencesRequest.h"
#import "UserProfile.h"
#import "CurrentLocale.h"
#import "NSString+Random.h"

#define USER_TOKEN_PARAM @"userToken"
#define SESSION_ID_PARAM @"sessionId"
#define IS_GLOBAL_PARAM @"isGlobal"
#define USER_PREFEENCES_PARAM @"userPreferences"//@"travelPreferenceVO"
#define SOURCE_ADDRESS_PARAM @"sourceAddress"
#define SOURCE_LATITUDE_PARAM @"sourceLatitude"
#define SOURCE_LONGITUDE_PARAM @"sourceLongitude"
#define DESTINATION_ADDRESS_PARAM @"destinationAddress"
#define DESTINATION_LATITUDE_PARAM @"destinationLatitude"
#define DESTINATION_LONGITUDE_PARAM @"destinationLongitude"
#define CITY_PARAM @"city"
#define START_TIME_PARAM @"startTime"
#define RETURN_TIME_PARAM @"returnTime"
#define ONWARD_ROUTE_ID_PARAM @"onwardRouteId"
#define RETURN_ROUTE_ID_PARAM @"returnRouteId"
#define ONWARD_POLYLINE_PARAM @"onwardPolyline" //onwardOverviewPolyPoints
#define RETURN_POLYLINE_PARAM @"returnPolyline" //returnOverviewPolyPoints
#define USER_MODE_PARAM @"userMode"

@implementation UserPreferencesRequest
-(id)initWithWithRequestType:(enum PreferencesRequestType)requestType andHomeAddress:(LocalityInfo *)geoHomeAddress andOfficeAddress:(LocalityInfo *)geoOfficeAddress andStartTime:(NSString *)startTime andReturnTime:(NSString *)returnTime andOnwardRoute:(DriveRoute *)onwardRoute andReturnRoute:(DriveRoute *)returnRoute andUserMode:(NSString *)userMode{
    self = [super init];
    if (self) {
        _homeAddress = geoHomeAddress;
        _officeAddress = geoOfficeAddress;
        _startTime = startTime;
        _returnTime = returnTime;
        _requestType = requestType;
        _onwardRoute = onwardRoute;
        _returnRoute = returnRoute;
        _userMode = userMode;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[USER_TOKEN_PARAM] = [UserProfile getCurrentUserToken];
    
    NSString *sessionId = [[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    if(sessionId == nil){
        sessionId = [NSString randomAlphanumericStringWithLength:10];
        [[NSUserDefaults standardUserDefaults] setObject:sessionId forKey:@"sessionId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    params[SESSION_ID_PARAM] =[[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
    params[IS_GLOBAL_PARAM] = [NSNumber numberWithBool:[currentLocale isGlobalLocale]].stringValue;
    params[USER_PREFEENCES_PARAM] = [self getPreferencesBean];
    return params;
}

-(NSDictionary *) urlparamsTemp{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    UserProfile *user = [UserProfile getCurrentUser];
    NSString *sessionId = [[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    if(sessionId == nil){
        sessionId = [NSString randomAlphanumericStringWithLength:10];
        [[NSUserDefaults standardUserDefaults] setObject:sessionId forKey:@"sessionId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
  //  params[SESSION_ID_PARAM] = [[NSUserDefaults standardUserDefaults] stringForKey:@"sessionId"];
    params[@"countryCode"] = user.country;
    params[USER_TOKEN_PARAM] = [UserProfile getCurrentUserToken];

    //CurrentLocale *currentLocale = [CurrentLocale sharedInstance];
   // params[IS_GLOBAL_PARAM] = [NSNumber numberWithBool:[currentLocale isGlobalLocale]].stringValue;
    
    // [self getPreferencesBean];
   params[USER_PREFEENCES_PARAM] = [self getPreferencesBean];

    NSLog(@"params are %@", params);
    return params;//[self getPreferencesBean];
}

-(NSString *) serverURL{
    switch (_requestType) {
        case ADDPREFERENCES:
             //@"user/saveTravelPreference";
            
            //@"ridematchingdev/user/geo/saveTravelPreference";
           return @"/user/saveUserPrefs.htm";
            
        case UPDATEPREFERENCES:
             //@"user/saveTravelPreference";
            //@"ridematchingdev/user/geo/saveTravelPreference";
           return @"/user/updateUserPrefs.htm";
        default:
            return nil;
    }
}

-(BOOL)isJSONRequestFormat {
    switch (_requestType) {
        case ADDPREFERENCES:
        case UPDATEPREFERENCES:
            return true;
        default:
            return false;
    }
}

-(NSString *) mappingKey{
    return @"user";
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL) saveInDatabase{
    return true;
}

-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    [UserProfile deleteCurrentUser];
    [UserProfile createCurrentUser:responseData];
}

-(NSDictionary *)getPreferencesBean{
    /****
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *homePointparams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *workPointparams = [[NSMutableDictionary alloc] init];
    UserProfile *profile = [UserProfile getCurrentUser];
    params[USER_TOKEN_PARAM] = [UserProfile getCurrentUserToken];
    params[@"userId"] = [UserProfile getCurrentUserId];
    params[@"countryCode"] = profile.country;
    homePointparams[@"address"] = [_homeAddress objectForKey:@"address"];//@"Pavan Prime Towers Rd, DLF Cyber City, Indira Nagar, Gachibowli, Hyderabad, Telangana 500032, India";
    homePointparams[@"city"] = [_homeAddress objectForKey:@"city"];//@"Hyderabad";
    homePointparams[@"exists"] = [_homeAddress objectForKey:@"exists"];//@1;
    homePointparams[@"geoDataId"] = [_homeAddress objectForKey:@"geoDataId"];//1667262213];
    homePointparams[@"lat"] = [_homeAddress objectForKey:@"lat"];//17.4469906];
    homePointparams[@"lng"] = [_homeAddress objectForKey:@"lng"]; //@78.3541304;

    params[@"homePoint"] = homePointparams;
    params[@"perKmRate"] = @0.0;
    params[@"returnTime"] = _returnTime;
    params[@"shouldAutomate"]= @1;
    params[@"startTime"] = _startTime;
    params[@"prefId"] = [[NSUserDefaults standardUserDefaults] valueForKey:@"prefId"];//@"";
    params[@"userRouteDataOnward"] = _onwardRoute;
    params[@"userRouteDataReturn"] = _returnRoute;
    [params setObject:_userMode forKey:@"userType"];
  //  params[@"userType"] = _userMode;
    
    workPointparams[@"address"] = [_officeAddress objectForKey:@"address"];//@"Google, Survey No. 13, DivyaSree Omega, Kondapur Village, Kothaguda, Hyderabad, Telangana 500084, India";
    workPointparams[@"city"] = [_officeAddress objectForKey:@"city"]; //@"Hyderabad";
    workPointparams[@"exists"] = [_officeAddress objectForKey:@"exists"]; //@1;
    workPointparams[@"geoDataId"] = [_officeAddress objectForKey:@"geoDataId"];
    workPointparams[@"lat"] = [_officeAddress objectForKey:@"lat"];
     //[NSNumber numberWithDouble:17.4583583];
    workPointparams[@"lng"] = [_officeAddress objectForKey:@"lng"];
     //[NSNumber numberWithDouble:78.3724429];
    params[@"workPoint"] = workPointparams;
    
    return params;
    ****/
    /*
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = @"";
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
         jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
   */
    
    /******/
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[SOURCE_ADDRESS_PARAM] = _homeAddress.address;
    double sourceLat = (double)_homeAddress.latLng.latitude;
    double sourceLng = (double)_homeAddress.latLng.longitude;
    params[SOURCE_LATITUDE_PARAM] = [[NSNumber numberWithDouble:sourceLat] stringValue];
    params[SOURCE_LONGITUDE_PARAM] = [[NSNumber numberWithDouble:sourceLng] stringValue];
    params[DESTINATION_ADDRESS_PARAM] = _officeAddress.address;
    double destLat = (double)_officeAddress.latLng.latitude;
    double destLng = (double)_officeAddress.latLng.longitude;
    params[DESTINATION_LATITUDE_PARAM] = [[NSNumber numberWithDouble:destLat] stringValue];
    params[DESTINATION_LONGITUDE_PARAM] = [[NSNumber numberWithDouble:destLng] stringValue];
    params[CITY_PARAM] = _homeAddress.city;
    params[START_TIME_PARAM] = _startTime;
    params[RETURN_TIME_PARAM] = _returnTime;
    params[ONWARD_ROUTE_ID_PARAM] = _onwardRoute != nil ? _onwardRoute.routeId : @"";
    params[RETURN_ROUTE_ID_PARAM] = _returnRoute != nil ? _returnRoute.routeId : @"";
    params[ONWARD_POLYLINE_PARAM] = @"";
    params[RETURN_POLYLINE_PARAM] = @"";
    params[USER_MODE_PARAM] = _userMode;
    return params;
   /*****/
    
}

//-(BOOL)overrideBaseURL{
//    return true;
//}
//-(BOOL)isForZenParkService{
//    return true;
//}


@end
