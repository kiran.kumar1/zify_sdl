//
//  UserRideRequest.h
//  zify
//
//  Created by Anurag S Rathor on 22/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"



@interface UserGetDriveDeepLinkUrlRequest : ServerRequest
@property(nonatomic,strong)NSNumber *userDriveId;
@property(nonatomic,strong)NSNumber *userDriverId;


-(id)initWithDriveId:(NSNumber *)driveID withDriverId:(NSNumber *)driverId;

@end
