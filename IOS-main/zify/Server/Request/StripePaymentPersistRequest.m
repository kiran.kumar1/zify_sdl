//
//  StripePaymentPersistRequest.m
//  zify
//
//  Created by Anurag S Rathor on 13/10/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "StripePaymentPersistRequest.h"
#import "UserProfile.h"


#define AMOUNT_PARAM @"amount"
#define CURRENCY_PARAM @"currency"
#define ZIFY_PAYMENT_REF_ID_PARAM @"zifyPaymentRefId"
#define STRIPE_TOKEN_PARAM @"stripeToken"
#define FAILURE_CODE_PARAM @"failureCode"
#define FAILURE_DESCRIPTION_PARAM @"failureDescription"
#define COUNTRY_CODE_PARAM @"countryCode"


@implementation StripePaymentPersistRequest

-(id)initWithRequestType:(enum StripePaymentRequestType)requestType andAmount:(NSString *)amount andCurrency:(NSString *)currency{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _amount = amount;
        _currency = currency;
    }
    return self;
}

-(id)initWithRequestType:(enum StripePaymentRequestType)requestType andStripeToken:(NSString *)stripeToken andZifyPaymentRefId:(NSString *)zifyPaymentRefId andFailureCode:(NSNumber *)code andFailureDescription:(NSString *)description;{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _zifyPaymentRefId = zifyPaymentRefId;
        _stripeToken = stripeToken == nil ? @"" : stripeToken;
        _failureCode = _failureCode == nil ? @0 : _failureCode;
        _failureDescription = description == nil ? @"" : description;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    UserProfile *profile = [UserProfile getCurrentUser];
    params[COUNTRY_CODE_PARAM] = profile.country;
    switch (_requestType) {
        case STRIPEPAYMENTREQUEST:
            params[AMOUNT_PARAM] =  _amount;
            params[CURRENCY_PARAM] = _currency;
            break;
        case STRIPEPAYMENTRESPONSE:
            params[ZIFY_PAYMENT_REF_ID_PARAM] = _zifyPaymentRefId;
            params[STRIPE_TOKEN_PARAM] = _stripeToken;
            params[FAILURE_CODE_PARAM] = _failureCode;
            params[FAILURE_DESCRIPTION_PARAM] = _failureDescription;
            break;
        default:
            return nil;
    }
    return params;
}
-(NSString *) serverURL{
    switch (_requestType) {
        case STRIPEPAYMENTREQUEST:
            return @"/genZifyRefIdNPersistRequestStripePay";
        case STRIPEPAYMENTRESPONSE:
            return @"/chargeNPersistStripePayment";
        default:
            return nil;
    }
    
}

-(NSString *) mappingKey{
    switch (_requestType) {
        case STRIPEPAYMENTREQUEST:
            return @"zifyPaymentRefId";
        default:
            return nil;
    }
}

-(BOOL)isPaymentRequest{
    return true;
}
-(BOOL)isForZenParkService{
    return true;
}

-(BOOL) isActualResponseSingleAttribute{
    switch (_requestType) {
        case STRIPEPAYMENTREQUEST:
            return true;
        default:
            return false;
    }
}
@end
