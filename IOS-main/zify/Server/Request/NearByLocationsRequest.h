//
//  NearByLocationsRequest.h
//  zify
//
//  Created by Anurag S Rathor on 23/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerRequest.h"

@interface NearByLocationsRequest : ServerRequest
@property (nonatomic,strong) NSNumber *userLat;
@property (nonatomic,strong) NSNumber *userLong;
-(id)initWithUserLat:(NSNumber *)userLat andUserLong:(NSNumber *)userLong;
@end
