//
//  EditProfileRequest.m
//  zify
//
//  Created by Anurag S Rathor on 30/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "EditProfileRequest.h"
#import "UserProfile.h"

#define GENDER_PARAM @"gender"
#define DOB_PARAM @"dob"
#define COMPANY_NAME_PARAM @"companyName"
#define COMPANY_EMAIL_PARAM @"companyEmail"
#define BLOOD_GROUP_PARAM @"bloodGroup"
#define EMERGENCY_CONTACT_PARAM @"emergencyContact"
#define USER_IMG_FILE_PARAM @"userImgFile"
#define COUNTRY_CODE @"countryCode"

@implementation EditProfileRequest
-(id)initWithGender:(NSString *)gender andDOB:(NSString *)dob andCompanyName:(NSString *)companyName andCompanyEmail:(NSString *)companyEmail andBloodGroup:(NSString *)bloodGroup andEmergencyContact:(NSString *)emergencyContact andCountryCode:(NSString *)countryCode {
    self = [super init];
    if (self) {
        _gender = gender;
        _dob = dob;
        _companyName = companyName == nil ? @"" : companyName;
        _companyEmail = companyEmail == nil ? @"" : companyEmail;
        _bloodGroup = bloodGroup == nil ? @"":bloodGroup;
        _emergencyContact = emergencyContact == nil ? @"" :emergencyContact;
        _countryCode = countryCode;
    }
    return self;
}
-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    UserProfile *profile = [UserProfile getCurrentUser];
    params[GENDER_PARAM] = _gender;
    params[DOB_PARAM] = _dob;
    params[COMPANY_NAME_PARAM] = _companyName;
    params[COMPANY_EMAIL_PARAM] = _companyEmail;
    params[BLOOD_GROUP_PARAM] = _bloodGroup;
    params[EMERGENCY_CONTACT_PARAM] = _emergencyContact;
    params[COUNTRY_CODE] = profile.country;

    return params;
}
-(NSString *) serverURL{
    return @"/user/updateUserProfileWithMultipart.htm";
}
-(NSString *) mappingKey{
    return @"user";
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL) saveInDatabase{
    return true;
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    [UserProfile deleteCurrentUser];
    [UserProfile createCurrentUser:responseData];
}
-(NSString *) imageParamName{
    return USER_IMG_FILE_PARAM;
}

@end
