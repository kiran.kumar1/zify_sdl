//
//  LoginRequest.m
//  zify
//
//  Created by Anurag S Rathor on 05/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "LoginRequest.h"
#import "UserProfile.h"
#import "UserProfile.h"
#import "CurrentLocale.h"
#import "CurrentLocationTracker.h"

#define EMAIL_PARAM @"userEmail"
#define PASSWORD_PARAM @"userPswd"
#define ISO_CODE_PARAM @"isoCode"
#define COUNTRY_PARAM @"country"
#define USER_LAT_PARAM @"userLat"
#define USER_LONG_PARAM @"userLong"


@implementation LoginRequest

-(id)initWithEmail:(NSString *)email andPassword:(NSString *)password{
    self = [super init];
    if (self) {
        _email = email;
        _password = password;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSString *isoCode = [[CurrentLocale sharedInstance] getMobileISOCode];
    params[EMAIL_PARAM] = _email;
    params[PASSWORD_PARAM] = _password;
    if(isoCode){
        params[ISO_CODE_PARAM] = isoCode;
    } else{
        LocalityInfo *localityInfo;// = [CurrentLocationTracker sharedInstance].currentLocalityInfo;
        if(localityInfo){
            params[COUNTRY_PARAM] = localityInfo.country == nil ? @"" : localityInfo.country;
            params[USER_LAT_PARAM] = [[NSNumber numberWithDouble:localityInfo.latLng.latitude] stringValue];
            params[USER_LONG_PARAM] = [[NSNumber numberWithDouble:localityInfo.latLng.longitude] stringValue];

        }
    }
    return params;
}
-(NSString *) serverURL{
    return @"/user/login.htm";
}
-(NSString *) mappingKey{
    return @"user";
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL) saveInDatabase{
    return true;
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    [UserProfile createCurrentUser:responseData];
}

@end
