//
//  PushNotificationRequest.m
//  zify
//
//  Created by Anurag S Rathor on 18/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushNotificationRequest.h"


#define PUSH_TOKEN_PARAM @"pushToken"
#define USER_ID_PARAM @"userId"
#define PLATFORM_PARAM @"platform"
#define DEVICE_ID_PARAM @"deviceId"
#define IMEI_PARAM @"imei"
#define DEVICE_FINGER_PRINT_PARAM @"deviceFingerPrint"
#define OS_NAME_PARAM @"osName"
#define OS_VERSION_PARAM @"osVersion"
#define APP_VERSION_CODE_PARAM @"appVersionCode"
#define IS_LOGGED_IN @"isLoggedIn"
#import "AppDelegate.h"
#import "DBUserDataInterface.h"
#import "OneSignalPlayerid.h"

@implementation PushNotificationRequest
-(id)initWithRequestType:(enum PushNotificationRequestType)requestType andUserId:(NSNumber *)userId andPushToken:(NSString *)pushToken{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _userId = userId;
        NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
        OneSignalPlayerid* obj = [OneSignalPlayerid getPlayerIdObjForDeviceInContext:context];
        _pushToken = obj.playerId;//pushToken;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    params[PUSH_TOKEN_PARAM] = _pushToken;
    params[USER_ID_PARAM] =_userId == nil ? @"-1" : _userId;
    params[PLATFORM_PARAM] = @"iOS";
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];

    switch (_requestType) {
        case PUSHREGISTERREQUEST:
            params[DEVICE_ID_PARAM] = @"-1";
            params[IMEI_PARAM] = @"-1";
            params[DEVICE_FINGER_PRINT_PARAM] = @"-1";
            params[OS_NAME_PARAM] = @"iOS";
            params[OS_VERSION_PARAM]= [[UIDevice currentDevice] systemVersion];
            params[APP_VERSION_CODE_PARAM] = majorVersion;
            params[IS_LOGGED_IN] = @"true";
            break;
        case PUSHDERIGISTERREQUEST:
            //params[IS_LOGGED_IN] = @"false";
        default:
            break;
    }
    return params;
}


-(NSString *) serverURL{
    switch (_requestType) {
        case PUSHREGISTERREQUEST:
           // return @"/zifyPush/registerForPushNotification.htm";  // for testing
            return @"/zifyServices/zifyPush/registerForPushNotification.htm";
        case PUSHDERIGISTERREQUEST:
            //return @"/zifyPush/pushDeregistration.htm";   // for testing
            return @"/zifyServices/zifyPush/pushDeregistration.htm";
        default:
            return nil;
    }
}
-(BOOL)overrideBaseURL{
    return true;
}
-(BOOL)isForZenParkService{
    return true;
}


@end


