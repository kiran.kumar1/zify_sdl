//
//  UserRegisterRequest.m
//  zify
//
//  Created by Anurag S Rathor on 09/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserRegisterRequest.h"
#import "UserProfile.h"


#define FIRST_NAME_PARAM @"fname"
#define LAST_NAME_PARAM @"lname"
#define USER_EMAIL_PARAM @"userEmail"
#define USER_PASSWORD_PARAM @"userPswd"
#define ISD_CODE_PARAM @"isdCode"
#define MOBILE_PARAM @"mobile"
#define GENDER_PARAM @"gender"
#define LOGIN_MODE_PARAM @"loginMode"
#define PROMO_CODE_PARAM @"promoCode"
#define PLATFORM_PARAM @"platform"
#define COUNTRYCODE_PARAM @"countrycode"
#define COUNTRYCODE_FB_PARAM @"countryCode"
#define FB_PROFILE_ID_KEY @"fbProfileId"
#define TRUE_CALLER_KEY @"verifiedByTrueCaller"


@implementation UserRegisterRequest
-(id)initWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName andUserEmail:(NSString *)userEmail andUserPswd:(NSString *)userPswd andisdCode:(NSString *)isdCode
    andMobile:(NSString *)mobile andGender:(NSString *)gender andLoginMode:(NSString *)loginMode andPromoCode:(NSString *)promoCode andCountryCode:(NSString *)countryCode andFbProfileId:(NSString *)fbProfileId andisTrueCaller:(NSString *)isTCProfile {
    self = [super init];
    if (self) {
        _firstName = firstName;
        _lastName = lastName;
        _userEmail = userEmail;
        _userPswd = userPswd;
        _isdCode = isdCode;
        _mobile = mobile;
        _gender = gender;
       _loginMode = loginMode;
        _promoCode = promoCode;
        _countryCode  = countryCode;
        _fbProfileId = fbProfileId;
        _isTrueCallerProfile = isTCProfile;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[FIRST_NAME_PARAM] = _firstName;
    params[LAST_NAME_PARAM] = _lastName;
    params[USER_EMAIL_PARAM] = _userEmail;
    params[USER_PASSWORD_PARAM] = _userPswd;
    params[ISD_CODE_PARAM] = _isdCode;
    params[MOBILE_PARAM] = _mobile;
    params[GENDER_PARAM] = _gender;
    params[LOGIN_MODE_PARAM] = _loginMode;
    params[PROMO_CODE_PARAM] = _promoCode == nil ? @"" : _promoCode;
    params[PLATFORM_PARAM] = @"iOS";
    params[COUNTRYCODE_FB_PARAM] = _countryCode;
    params[COUNTRYCODE_PARAM] = _countryCode;
    if([_loginMode isEqualToString:@"fb"]){
        params[FB_PROFILE_ID_KEY] = _fbProfileId;
    }else{
        params[TRUE_CALLER_KEY] = _isTrueCallerProfile;
    }
    return params;
}
-(NSString *) serverURL{
    if([_loginMode isEqualToString:@"fb"]){
        return @"/user/V2/signupwithfacebook";
    }
    return @"/user/V2/signupwithemail";
}
-(NSString *) mappingKey{
    return @"user";
}
-(RKObjectMapping *) objectMapping{
    return [UserProfile getUserProfileObjectMapping];
}
-(BOOL)isForV2Version{
    return YES;
}
-(BOOL) saveInDatabase{
    return true;
}
-(void)perfomPostResponseDatabaseOpeartion:(NSData *)responseData{
    [UserProfile createCurrentUser:responseData];
}

@end
