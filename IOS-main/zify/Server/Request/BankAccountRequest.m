//
//  BankAccountRequest.m
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "BankAccountRequest.h"
#import "BankRedemptionDetails.h"

#define BANK_NAME_PARAM @"bankName"
#define ACCOUNT_TYPE_PARAM @"accountType"
#define ACCOUNT_HOLDER_NAME_PARAM @"accountHolderName"
#define ACCOUNT_NUMBER_PARAM @"accountNumber"
#define IFSC_CODE_PARAM @"ifscCode"
#define BANK_DETAIL_ID_PARAM @"bankDetailId"
#define REDEEM_AMOUNT_PARAM @"redeemAmount"

@implementation BankAccountRequest

-(id)initWithRequestType:(enum BankAccountRequestType)requestType{
    self = [super init];
    if (self) {
        _requestType = requestType;
    }
    return self;
}

-(id)initWithRequestType:(enum BankAccountRequestType)requestType andBankAccountDetaiId:(NSNumber *)bankDetailId andRedeemAmount:(NSString *)redeemAmount{
    if (self) {
        _requestType = requestType;
        _bankDetailId = bankDetailId;
        _redeemAmount = redeemAmount;
    }
    return self;
}

-(id)initWithRequestType:(enum BankAccountRequestType)requestType andAccountType:(NSString *)accountType andAccountHolderName:(NSString *)accountHolderName andAccountNumber:(NSString *)accountNumber andifscCode:(NSString *)ifscCode andBankname:(NSString *)bankName{
    self = [super init];
    if (self) {
        _requestType = requestType;
        _accountType = accountType;
        _accountHolderName = accountHolderName;
        _accountNumber = accountNumber;
        _ifscCode = ifscCode;
        _bankName = bankName;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    switch (_requestType) {
        case ADDBANK:
            params[ACCOUNT_TYPE_PARAM] = _accountType;
            params[ACCOUNT_HOLDER_NAME_PARAM] = _accountHolderName;
            params[ACCOUNT_NUMBER_PARAM] = _accountNumber;
            params[IFSC_CODE_PARAM] = _ifscCode;
            params[BANK_NAME_PARAM] = _bankName;
            break;
        case DELETEBANK:
            params[BANK_DETAIL_ID_PARAM] = _bankDetailId;
            break;
        case REGISTERREDEEM:
            params[BANK_DETAIL_ID_PARAM] = _bankDetailId;
            params[REDEEM_AMOUNT_PARAM] = _redeemAmount;
            break;
        case SAVEDBANKDETAILS:
            break;
        case REDEMPTIONDETAILS:
            break;
        default:
            return nil;
    }
    return params;
}

-(NSString *) serverURL{
    switch (_requestType) {
        case ADDBANK:
            return @"/zifyBank/addBankDetails.htm";
        case REDEMPTIONDETAILS:
            return @"/zifyBank/getRedemptionDetails.htm";
        case SAVEDBANKDETAILS:
            return @"/zifyBank/getSavedBankDetailList.htm";
        case DELETEBANK:
            return @"/zifyBank/deleteSavedBankDetail.htm";
        case REGISTERREDEEM:
            return @"/zifyBank/registerRedeemRequest.htm";
        default:
            return nil;
    };
}

-(NSString *) mappingKey{
    switch (_requestType) {
        case REDEMPTIONDETAILS:
            return @"redemptionDetails";
        case SAVEDBANKDETAILS:
            return @"bankDetails";
        default:
            return nil;
    }
}
-(RKObjectMapping *) objectMapping{
    switch (_requestType) {
        case REDEMPTIONDETAILS:
            return [BankRedemptionDetails getBankRedemptionDetailsObjectMapping];
        case SAVEDBANKDETAILS:
            return [BankAccountDetails getBankAccountDetailsObjectMapping];
        default:
            return nil;
    }
}
-(BOOL)isPaymentRequest{
    return false;
}
-(BOOL)isForBankRequest{
    return YES;
}
@end
