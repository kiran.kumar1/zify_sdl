//
//  PushNotificationRequest.h
//  zify
//
//  Created by Anurag S Rathor on 18/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "ServerRequest.h"

enum PushNotificationRequestType{
    PUSHREGISTERREQUEST,PUSHDERIGISTERREQUEST
};
@interface PushNotificationRequest : ServerRequest
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,strong) NSString *pushToken;
@property (nonatomic) enum PushNotificationRequestType requestType;
-(id)initWithRequestType:(enum PushNotificationRequestType)requestType andUserId:(NSNumber *)userId andPushToken:(NSString *)pushToken;
@end
