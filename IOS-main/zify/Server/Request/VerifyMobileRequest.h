//
//  VerifyMobileRequest.h
//  zify
//
//  Created by Anurag S Rathor on 14/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerRequest.h"

enum VerifyMobileRequestType{
    VERIFYMOBILEOTP,VERIFYMOBILE
};


@interface VerifyMobileRequest : ServerRequest
@property (nonatomic, strong) NSString *otp;
@property (nonatomic) enum VerifyMobileRequestType requestType;
-(id)initWithOtp:(NSString *)otp andRequestType:(enum VerifyMobileRequestType) requestType;
@end
