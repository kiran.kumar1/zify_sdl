//
//  NearByLocationsRequest.m
//  zify
//
//  Created by Anurag S Rathor on 23/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "NearByLocationsRequest.h"
#import "NearestAvailableCarsResponse.h"

#define USER_LAT_PARAM @"userLat"
#define USER_LONG_PARAM @"userLong"

@implementation NearByLocationsRequest
-(id)initWithUserLat:(NSNumber *)userLat andUserLong:(NSNumber *)userLong{
    self = [super init];
    if (self) {
        _userLat = userLat;
        _userLong = userLong;
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    params[USER_LAT_PARAM] = _userLat;
    params[USER_LONG_PARAM] = _userLong;
    return params;
}

-(NSString *) serverURL{
     return @"/route/getNearByLocations.htm";
}

-(NSString *) mappingKey{
     return @"avaibaleCars";
}

-(RKObjectMapping *) objectMapping{
    return [NearestAvailableCarsResponse getNearestAvailableCarsResponseObjectMapping];
}
@end
