//
//  UserRideRequest.m
//  zify
//
//  Created by Anurag S Rathor on 22/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserGetDriveDeepLinkUrlRequest.h"


#define DRIVE_ID_KEY @"driveId"
#define DRIVER_ID_KEY @"driverId"



@implementation UserGetDriveDeepLinkUrlRequest
-(id)initWithDriveId:(NSNumber *)driveID withDriverId:(NSNumber *)driverId;
{
    self = [super init];
    if (self) {
        self.userDriveId = driveID;
        self.userDriverId = driverId;
    }
    return self;
}
-(NSDictionary *)urlparams{
    NSMutableDictionary *params = [self getUserCommonParams];
    params[DRIVE_ID_KEY] = self.userDriveId;
    params[DRIVER_ID_KEY] = self.userDriverId;

    return params;
}

-(NSString *) serverURL{
    return @"/zifyServices/gen/getRideWithMeLink.htm";
}
-(BOOL)overrideBaseURL{
    return YES;
}

-(BOOL)isForZenParkService{
    return true;
}
-(BOOL)isGetRequest{
    return true;
}
@end
