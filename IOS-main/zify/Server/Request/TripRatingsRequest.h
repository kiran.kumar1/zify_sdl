//
//  TripRatingsRequest.h
//  zify
//
//  Created by Anurag S Rathor on 21/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "ServerRequest.h"
#import "TripDrive.h"

enum TripRatingsRequestType{
    DRIVERATING,RIDERATING
};

@interface TripRatingsRequest : ServerRequest
@property (nonatomic, strong) NSArray *ratingTripIds;
@property (nonatomic, strong) NSArray *ratings;
@property (nonatomic, strong) NSArray *comments;
@property (nonatomic, strong) NSNumber *tripId;
@property (nonatomic) enum TripRatingsRequestType requestType;
-(id)initWithWithRequestType:(enum TripRatingsRequestType)requestType andTripId:(NSNumber *)tripId andRatingTripIds:(NSArray *)ratingTripIds andRatings:(NSArray *)ratings andComments:(NSArray *)comments;
@end
