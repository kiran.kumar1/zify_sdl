//
//  PublishDriveRequest.h
//  zify
//
//  Created by Anurag S Rathor on 23/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ServerRequest.h"
#import "Drive.h"

@interface PublishDriveRequest : ServerRequest
@property (nonatomic, strong) NSString * routeId;
@property (nonatomic, strong) NSString * routine;
@property (nonatomic, strong) NSString * returnTime;

@property (nonatomic, strong) NSNumber * numOfSeats;
@property (nonatomic, strong) NSString * departureTime;
@property (nonatomic, strong) NSDictionary *startPoint;
@property (nonatomic, strong) NSDictionary *endPoint;
@property (nonatomic, strong) NSDictionary *polilineRouteInfo;
@property (nonatomic, strong) NSString * userTravelMode;



-(id)initWithDepartureTime:(NSString *)departureTime andRoutine:(NSString *)routine andReturnTime:(NSString *)returnTime;
-(void)setSeats:(NSNumber *)numOfSeats andRouteId:(NSString *)routeId;

-(id)initWithSTartPoint:(NSDictionary *)startPoint andEndPoint:(NSDictionary *)endPoint andPolylineRoute:(NSDictionary *)polylineRouteInfo andTravelMode:(NSString *)travelMode andNumberOfSeats:(NSNumber *)seatsCountStr andDepartureTime:(NSString *)departureTime;
@end
