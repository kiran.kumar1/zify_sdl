//
//  UserRegisterRequest.m
//  zify
//
//  Created by Anurag S Rathor on 09/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ZenParkSignInRequest.h"
#import "UserProfile.h"
#import "ZenparkSignInObj.h"

#define USERNAME @"userName"
#define PASSWORD @"password"



@implementation ZenParkSignInRequest
-(id)initWithSignInRequest{
    self = [super init];
    if (self) {
       
    }
    return self;
}

-(NSDictionary *) urlparams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[USERNAME] = @"dev@zify.co";
    params[PASSWORD] = @"!@nurAg5916!";
    return params;
}
-(NSString *) serverURL{
    return @"zifyServices/zenpark/login.htm";
}
-(NSString *) mappingKey{
    return @"token";
}

-(BOOL)isForZenParkService{
    return true;
}
-(BOOL)isJSONRequestFormat{
    return true;
}
-(BOOL)isForZenParkSignIn{
    return true;
}
-(BOOL)overrideBaseURL{
    return true;
}
-(RKObjectMapping *) objectMapping{
    return nil;//[ZenparkSignInObj getZenParkSignObjectMapping];
}


@end
