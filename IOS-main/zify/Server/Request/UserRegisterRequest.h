//
//  UserRegisterRequest.h
//  zify
//
//  Created by Anurag S Rathor on 09/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"


@interface UserRegisterRequest : ServerRequest
@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * lastName;
@property (nonatomic, strong) NSString * userEmail;
@property (nonatomic, strong) NSString * userPswd;
@property (nonatomic, strong) NSString * isdCode;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * gender;
@property (nonatomic, strong) NSString * loginMode;
@property (nonatomic, strong) NSString * promoCode;
@property (nonatomic, strong) NSString * countryCode;
@property (nonatomic, strong) NSString * fbProfileId;
@property (nonatomic, strong) NSString * isTrueCallerProfile;


-(id)initWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName andUserEmail:(NSString *)userEmail andUserPswd:(NSString *)userPswd andisdCode:(NSString *)isdCode andMobile:(NSString *)mobile andGender:(NSString *)gender andLoginMode:(NSString *)loginMode andPromoCode:(NSString *)promoCode andCountryCode:(NSString *)countryCode andFbProfileId:(NSString *)fbProfileId andisTrueCaller:(NSString *)isTCProfile;
@end
