//
//  UserChatDialogRequest.h
//  zify
//
//  Created by Anurag S Rathor on 08/01/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "ServerRequest.h"

enum ChatDialogRequestType{
    SAVECONVERSATIONINFO,FETCHCONVERSATIONINFO
};

@interface UserChatDialogRequest : ServerRequest
@property (nonatomic,strong) NSString *convUserId1;
@property (nonatomic,strong) NSString *convUserId2;
@property (nonatomic,strong) NSString *convId;
@property (nonatomic) enum ChatDialogRequestType requestType;
-(id)initWithRequestType:(enum ChatDialogRequestType)requestType andConvUserId1:(NSString *)convUserId1 andConvUserId2:(NSString *)convUserId2 andConvId:(NSString *)convId;
@end
