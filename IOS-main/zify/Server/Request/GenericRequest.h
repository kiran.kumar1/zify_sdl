//
//  GenericRequest.h
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"

enum RequestType{
    UPCOMINGRIDESREQUEST,UPCOMINGDRIVESREQUEST,TRIPHISTORYREQUEST,RIDERWALLETREQUEST,RATECARDREQUEST,TRANSACTIONSTATEMENTREQUEST,RECHARGESTATEMENTREQUEST,LOGOUTREQUEST,USERNOTIFICATIONSREQUEST,CURRENTORNEXTRIDEDEPARTURETIMEREQUEST,AUTOLOGINREQUEST
};

@interface GenericRequest : ServerRequest
@property (nonatomic) enum RequestType requestType;
-(id)initWithRequestType:(enum RequestType)requestType;
@end
