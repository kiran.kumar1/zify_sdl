//
//  BankRedemptionDetails.m
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "BankRedemptionDetails.h"

@implementation BankRedemptionDetails
+(RKObjectMapping*)getBankRedemptionDetailsObjectMapping{
    RKObjectMapping *bankRedemptionDetailsmapping = [RKObjectMapping mappingForClass:[self class]];
    [bankRedemptionDetailsmapping addAttributeMappingsFromArray:@[@"redeemableAmount"]];
    [bankRedemptionDetailsmapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"bankDetails" toKeyPath:@"bankDetails" withMapping:[BankAccountDetails getBankAccountDetailsObjectMapping]]];
    return bankRedemptionDetailsmapping;
}
@end
