//
//  TripRide.m
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripRide.h"

@implementation TripRide
+(RKObjectMapping*)getTripRideObjectMapping{
    RKObjectMapping *tripRideObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [tripRideObjectMapping addAttributeMappingsFromArray:@[@"srcAddress",@"destAddress",@"departureTime",
            @"distance",@"zifyPoints",@"rideId",@"driveId",@"driverId",@"routeId",@"status",
            @"srcLat",@"srcLong",@"destLat",@"destLong",
                @"overviewPolylinePoints",@"hasBoarded",@"duration",@"distanceUnit",@"tripTime", @"zifyCoinsEarned", @"rideStartTime", @"rideEndTime",@"srcNearLat",@"srcNearLng",@"destNearLat",@"destNearLng", @"srcActualLat",@"srcActualLng",@"destActualLat",@"destActualLng"]];
     [tripRideObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"driveDetail" toKeyPath:@"driveDetail" withMapping:[TripRideDriveDetail getTripRideDriveDetailObjectMapping]]];
     [tripRideObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"driverDetail" toKeyPath:@"driverDetail" withMapping:[DriverDetail getDriverDetailObjectMapping]]];
    [tripRideObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"routeDetail" toKeyPath:@"routeDetail" withMapping:[TripRouteDetail getTripRouteDetailObjectMapping]]];
    return tripRideObjectMapping;

}
@end
