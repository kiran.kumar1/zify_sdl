//
//  DriverDetailDocuments.h
//  zify
//
//  Created by Anurag S Rathor on 03/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface DriverDetailDocuments : NSObject
@property (nonatomic,strong) NSString *drivingLicenceNum;
@property (nonatomic,strong) NSString *idCardNum;
@property (nonatomic,strong) NSString *idCardType;
@property (nonatomic,strong) NSNumber *isVehicleImgUploaded;
@property (nonatomic,strong) NSString *vehicleImgUrl;
@property (nonatomic,strong) NSString *vehicleInsuranceNum;
@property (nonatomic,strong) NSString *vehicleModel;
@property (nonatomic,strong) NSString *vehicleRegistrationNum;
+(RKObjectMapping*)getDriverDetailDocumentsObjectMapping;
@end
