//
//  UserNotificationDetail.m
//  zify
//
//  Created by Anurag S Rathor on 28/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserNotificationDetail.h"

@implementation UserNotificationDetail
+(RKObjectMapping*)getUserNotificationDetailObjectMapping {
    RKObjectMapping *userNotificationDetailObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [userNotificationDetailObjectMapping addAttributeMappingsFromArray:@[@"srcAddress",@"destAddress",@"departureTime",@"driveId",
                            @"rideId",@"numOfSeats",@"status",@"tripTime"]];
    [userNotificationDetailObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"riderProfile" toKeyPath:@"riderProfile" withMapping:[RiderProfile getRiderProfileObjectMapping]]];
     [userNotificationDetailObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"driverDetail" toKeyPath:@"driverDetail" withMapping:[DriverDetail getDriverDetailObjectMapping]]];
    return userNotificationDetailObjectMapping;
}
@end
