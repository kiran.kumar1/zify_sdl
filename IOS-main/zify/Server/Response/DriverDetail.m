//
//  DriverDetail.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "DriverDetail.h"
#import "UserProfileRegionObject.h"

@implementation DriverDetail
+(RKObjectMapping*)getDriverDetailObjectMapping{
    RKObjectMapping *serchRideDriverProfileObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [serchRideDriverProfileObjectMapping addAttributeMappingsFromArray:@[@"firstName",@"lastName",@"driverStatus",@"companyName",
        @"totalDistance",@"profileImgUrl",@"driverRatingFemale",@"jabberId",
        @"driverRatingMale",@"numOfFemalesRated",@"numOfMalesRated",@"userId",
        @"callId"]];
    [serchRideDriverProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"userDocs" toKeyPath:@"documents" withMapping:[DriverDetailDocuments getDriverDetailDocumentsObjectMapping]]];
    [serchRideDriverProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"qbUserInfo" toKeyPath:@"chatProfile" withMapping:[UserChatProfile getUserChatProfileObjectMapping]]];
    [serchRideDriverProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"regionDetail" toKeyPath:@"regionDetail" withMapping:[UserProfileRegionObject getUserProfileRegionObjectMapping]]];
    return serchRideDriverProfileObjectMapping;
}
@end
