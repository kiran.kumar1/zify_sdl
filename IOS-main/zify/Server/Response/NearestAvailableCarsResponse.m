//
//  NearestAvailableCarsResponse.m
//  zify
//
//  Created by Anurag S Rathor on 23/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "NearestAvailableCarsResponse.h"
#import "NearestAvailableCar.h"

@implementation NearestAvailableCarsResponse
+(RKObjectMapping*)getNearestAvailableCarsResponseObjectMapping{
    RKObjectMapping *nearestAvailableCarsResponseMapping = [RKObjectMapping mappingForClass:[self class]];
    [nearestAvailableCarsResponseMapping addAttributeMappingsFromArray:@[@"carImgUrl"]];
    [nearestAvailableCarsResponseMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"locationList" toKeyPath:@"locationList" withMapping:[NearestAvailableCar getNearestAvailableCarObjectMapping]]];
    return nearestAvailableCarsResponseMapping;
}
@end
