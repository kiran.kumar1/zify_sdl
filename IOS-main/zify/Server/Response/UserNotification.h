//
//  UserNotification.h
//  zify
//
//  Created by Anurag S Rathor on 28/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserNotificationDetail.h"
#import <RestKit/RestKit.h>

@interface UserNotification: NSObject
@property (nonatomic,strong) NSString *notificationType;
@property (nonatomic,strong) UserNotificationDetail *rideRequest;
+(RKObjectMapping*)getUserNotificationObjectMapping;
@end
