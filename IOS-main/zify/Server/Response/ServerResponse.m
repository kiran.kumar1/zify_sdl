//
//  ServerResponse.m
//  zify
//
//  Created by Anurag S Rathor on 06/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ServerResponse.h"
#import "DBInterface.h"

@implementation ServerResponse
+(ServerResponse *)getServerResponse:(NSData *)responseData withRequest:(ServerRequest *)request{
    NSError *error;
    //NSLog(@"service url is %@", request.serverURL);
    RKObjectMapping *serverResponseMapping=[RKObjectMapping mappingForClass:[self class]];
    [serverResponseMapping addAttributeMappingsFromArray:@[@"message",
                                                           @"messageCode",
                                                           @"status"]];
    NSString *MIMEType = @"application/json";
    id parsedData = [RKMIMETypeSerialization objectFromData:responseData MIMEType:MIMEType error:&error];
    if(error) return nil;
    ServerResponse *serverResponse;
    if(request.saveInDatabase){
        serverResponse = [self getServerResponseFromMapping:serverResponseMapping andData:parsedData];
        if(!serverResponse) return nil;
        if ([request isSuccessResponseCode:[serverResponse.messageCode intValue]] || [@"Success" isEqualToString:serverResponse.status]) {
            [request perfomPostResponseDatabaseOpeartion:responseData];
            /*RKManagedObjectStore *manageStore=[[DBInterface sharedInstance] sharedManageStore];
             NSDictionary *dbMappingDictionary=@{request.mappingKey:request.objectMapping};
             RKMapperOperation *dbMappingOperation = [[RKMapperOperation alloc] initWithRepresentation:parsedData mappingsDictionary:dbMappingDictionary];
             RKManagedObjectMappingOperationDataSource *mappingDS = [[RKManagedObjectMappingOperationDataSource alloc] initWithManagedObjectContext:[[DBInterface sharedInstance] sharedContext] cache:manageStore.managedObjectCache];
             dbMappingOperation.mappingOperationDataSource = mappingDS;
             [dbMappingOperation execute:&error];
             if(!error) [[DBInterface sharedInstance] saveSharedContext];*/
        }
    } else{
        if(request.isActualResponseSingleAttribute){
            [serverResponseMapping addAttributeMappingsFromDictionary:@{request.mappingKey:@"responseObject"}];
        } else{
            [serverResponseMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:request.mappingKey toKeyPath:@"responseObject" withMapping:request.objectMapping]];
        }
        serverResponse = [self getServerResponseFromMapping:serverResponseMapping andData:parsedData];
    }
    return serverResponse;
}

+(ServerResponse *)getServerResponseFromMapping:(RKObjectMapping *)serverResponseMapping andData:(id)parsedData{
    NSDictionary *mappingDictionary=@{[NSNull null]:serverResponseMapping};
    //NSLog(@"parse data is %@", parsedData);
    RKMapperOperation *mappingOperation=[[RKMapperOperation alloc]initWithRepresentation:parsedData mappingsDictionary:mappingDictionary];
    NSError *error;
    [mappingOperation execute:&error];
    if(error) return nil;
    ServerResponse *serverResponse = (ServerResponse*)[mappingOperation.mappingResult.array objectAtIndex:0];
    return serverResponse;
}

-(ServerResponse *)initWithServerResponseFromLaterZenparkServicesWithData:(NSDictionary *)responseData withRequest:(ServerRequest *)request{
    
    NSDictionary *statusDictionary = [responseData objectForKey:@"statusInfo"];
    if (statusDictionary == nil) {
        statusDictionary = [responseData objectForKey:@"status"];
    }
    
    int statusCode = [[statusDictionary objectForKey:@"statusCode"] intValue];
    self.messageCode = [NSString stringWithFormat:@"%d", statusCode];
    NSString *message = [statusDictionary objectForKey:@"messageKey"];
    message = NSLocalizedString(message, nil);
    if(message == nil){
        message = [statusDictionary objectForKey:@"messageKey"];
    }
    self.message = message;
    self.responseObject = responseData;
    return  self;
    
    
    /* NSDictionary *statusDictionary = [responseData objectForKey:@"statusInfo"];
     if ([responseData objectForKey:@"statusInfo"] == nil)  {
     statusDictionary = [responseData objectForKey:@"status"];
     
     int statusCode = [[statusDictionary objectForKey:@"statusCode"] intValue];
     self.messageCode = [NSString stringWithFormat:@"%d", statusCode];
     self.message = [statusDictionary objectForKey:@"messageKey"];
     self.responseObject = responseData;
     } else {
     ///V2 login services - 17-aug-2018
     int statusCode = [[responseData objectForKey:@"messageCode"] intValue];
     self.messageCode = [NSString stringWithFormat:@"%d", statusCode];
     self.message = [statusDictionary objectForKey:@"message"];
     self.status = [statusDictionary objectForKey:@"status"];
     }*/
    
    
    //    int statusCode = [[statusDictionary objectForKey:@"statusCode"] intValue];
    //    self.messageCode = [NSString stringWithFormat:@"%d", statusCode];
    //    self.message = [statusDictionary objectForKey:@"messageKey"];
    self.responseObject = responseData;
    return  self;
}

-(ServerResponse *)initWithFDjDataResponse:(NSDictionary *)dataResponse{
    self.messageCode = @"1";
    self.message = @"";
    self.responseObject = dataResponse;
    return  self;
}
@end
