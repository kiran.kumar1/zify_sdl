//
//  WalletRechargeStatement.h
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface WalletRechargeStatement : NSObject
@property (nonatomic,strong) NSString *transactionId;
@property (nonatomic,strong) NSString *transactionDate;
@property (nonatomic,strong) NSString *transactionStatus;
@property (nonatomic,strong) NSNumber *transactionAmount;
@property (nonatomic,strong) NSString *currency;
@property (nonatomic,strong) NSString *rechargeTimestamp;
+(RKObjectMapping*)getWalletRechargeStatementObjectMapping;
@end
