//
//  DriveRoute.m
//  zify
//
//  Created by Anurag S Rathor on 23/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "DriveRoute.h"

@implementation DriveRoute
+(RKObjectMapping*)getDriveRouteObjectMapping{
    RKObjectMapping *driveRouteObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [driveRouteObjectMapping addAttributeMappingsFromArray:@[@"srcLat",@"srcLong",@"destLat",@"destLong",
                    @"srcAdd",@"destAdd",@"distance",@"distanceUnit",
                    @"overviewPolylinePoints",@"routeId"]];
    return driveRouteObjectMapping;
}

-(id)initWithSource:(LocalityInfo *)sourceLocality andDestination:(LocalityInfo *)destinationLocality andRouteId:(NSString *)routeId andPolyline:(NSString *)polyline{
    self = [super init];
    if (self) {
        _srcLat = [NSNumber numberWithDouble:sourceLocality.latLng.latitude].stringValue;
        _srcLong = [NSNumber numberWithDouble:sourceLocality.latLng.longitude].stringValue;
        _destLat = [NSNumber numberWithDouble:destinationLocality.latLng.latitude].stringValue;
        _destLong = [NSNumber numberWithDouble:destinationLocality.latLng.longitude].stringValue;
        _srcAdd = sourceLocality.address;
        _destAdd = destinationLocality.address;
        _routeId = routeId;
        _overviewPolylinePoints = polyline;
    }
    return self;
}
@end
