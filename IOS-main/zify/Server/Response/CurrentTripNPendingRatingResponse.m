//
//  CurrentTripNPendingRatingResponse.m
//  zify
//
//  Created by Anurag S Rathor on 23/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "CurrentTripNPendingRatingResponse.h"

@implementation CurrentTripNPendingRatingResponse

+(RKObjectMapping*)getCurrentTripNPendingRatingObjectMapping{
    RKObjectMapping *currentRunningRideObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [currentRunningRideObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pendingDriveRating" toKeyPath:@"pendingDriveRating" withMapping:[TripDrive getTripDriveObjectMapping]]];
    [currentRunningRideObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pendingRideRating" toKeyPath:@"pendingRideRating" withMapping:[TripRide getTripRideObjectMapping]]];
    [currentRunningRideObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"currentRide" toKeyPath:@"currentRide" withMapping:[TripRide getTripRideObjectMapping]]];
    [currentRunningRideObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"currentDrive" toKeyPath:@"currentDrive" withMapping:[TripDrive getTripDriveObjectMapping]]];
    return currentRunningRideObjectMapping;
}
@end
