//
//  NearestAvailableCar.h
//  zify
//
//  Created by Anurag S Rathor on 23/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface NearestAvailableCar : NSObject
@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,strong) NSNumber *longitude;
+(RKObjectMapping*)getNearestAvailableCarObjectMapping;
@end
