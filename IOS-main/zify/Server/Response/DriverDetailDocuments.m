//
//  DriverDetailDocuments.m
//  zify
//
//  Created by Anurag S Rathor on 03/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "DriverDetailDocuments.h"

@implementation DriverDetailDocuments
+(RKObjectMapping*)getDriverDetailDocumentsObjectMapping{
    RKObjectMapping *driverDetailDocumentsMapping = [RKObjectMapping mappingForClass:[self class]];
    [driverDetailDocumentsMapping addAttributeMappingsFromArray:@[@"drivingLicenceNum",@"idCardNum",@"idCardType",
                        @"isVehicleImgUploaded",@"vehicleImgUrl",@"vehicleInsuranceNum",
                    @"vehicleModel",@"vehicleRegistrationNum"]];
    return driverDetailDocumentsMapping;
}
@end
