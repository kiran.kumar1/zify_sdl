//
//  UserNotificationDetail.h
//  zify
//
//  Created by Anurag S Rathor on 28/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "RiderProfile.h"
#import "DriverDetail.h"

@interface UserNotificationDetail : NSObject
@property (nonatomic,strong) NSString *srcAddress;
@property (nonatomic,strong) NSString *destAddress;
@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic,strong) NSNumber *driveId;
@property (nonatomic,strong) NSNumber *rideId;
@property (nonatomic,strong) NSNumber *numOfSeats;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *tripTime;
@property (nonatomic,strong) DriverDetail *driverDetail;
@property (nonatomic,strong) RiderProfile *riderProfile;
+(RKObjectMapping*)getUserNotificationDetailObjectMapping;
@end
