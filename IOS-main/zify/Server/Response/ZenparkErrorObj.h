//
//  SearchRide.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface  ZenparkErrorObj: NSObject
@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSString *Name;
//@property (nonatomic,strong) NSDictionary *Location;
+(RKObjectMapping*)getZenParkErrorObjectMapping;
@end
