//
//  RiderProfile.m
//  zify
//
//  Created by Anurag S Rathor on 29/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "RiderProfile.h"

@implementation RiderProfile
+(RKObjectMapping*)getRiderProfileObjectMapping{
    RKObjectMapping *riderProfileObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [riderProfileObjectMapping addAttributeMappingsFromArray:@[@"firstName",
            @"lastName",@"profileImageURL",@"companyName",@"totalDistance",
            @"numOfFemalesRated",@"numOfMalesRated",@"userRatingFemale",@"userRatingMale",@"userStatus",
            @"riderId",@"callId",@"jabberId"]];
     [riderProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"qbUserInfo" toKeyPath:@"chatProfile" withMapping:[UserChatProfile getUserChatProfileObjectMapping]]];
    [riderProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"regionDetail" toKeyPath:@"regionDetail" withMapping:[UserProfileRegionObject getUserProfileRegionObjectMapping]]];
    return riderProfileObjectMapping;
}
@end
