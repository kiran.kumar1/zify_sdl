//
//  WalletRechargeStatement.m
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "WalletRechargeStatement.h"

@implementation WalletRechargeStatement
+(RKObjectMapping*)getWalletRechargeStatementObjectMapping{
    RKObjectMapping *accountRechargeStatementObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [accountRechargeStatementObjectMapping addAttributeMappingsFromDictionary:@{@"zifyPaymentRefId":@"transactionId",
                                            @"createdOn":@"transactionDate",
                                            @"pgTransactionStatus":@"transactionStatus",
                                            @"amountPaid":@"transactionAmount",
                                            @"currency":@"currency",
                                            @"rechargeTimestamp":@"rechargeTimestamp"
                                                                                }];
    return accountRechargeStatementObjectMapping;
}
@end
