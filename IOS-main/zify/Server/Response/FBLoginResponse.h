//
//  FBLoginResponse.h
//  zify
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@interface FBLoginResponse : NSObject
@property(nonatomic,strong) NSString *firstName;
@property(nonatomic,strong) NSString *lastName;
@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *facebookId;
+(FBLoginResponse *)getFaceBookLoginResponse:(NSDictionary *)dictionary;
@end
