//
//  SearchRide.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface  FDJLocationObject: NSObject
@property (nonatomic,strong) NSString *ID;
@property (nonatomic,strong) NSString *CX;
@property (nonatomic,strong) NSString *CY;
@property (nonatomic,strong) NSString *ADRESSE;
+(RKObjectMapping*)getFDJLocationObjectMapping;
@end
