//
//  BankAccountDetails.h
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface BankAccountDetails : NSObject
@property (nonatomic,strong) NSNumber *bankDetailId;
@property (nonatomic,strong) NSString *bankName;
@property (nonatomic,strong) NSString *accountType;
@property (nonatomic,strong) NSString *accountHolderName;
@property (nonatomic,strong) NSString *accountNumber;
@property (nonatomic,strong) NSString *ifscCode;
+(RKObjectMapping*)getBankAccountDetailsObjectMapping;
@end
