//
//  RiderProfile.h
//  zify
//
//  Created by Anurag S Rathor on 29/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "UserChatProfile.h"
#import "UserProfileRegionObject.h"

@interface RiderProfile : NSObject
@property (nonatomic,strong) NSString *firstName;
@property (nonatomic,strong) NSString *lastName;
@property (nonatomic,strong) NSString *profileImageURL;
@property (nonatomic,strong) NSString *companyName;
@property (nonatomic,strong) NSNumber *totalDistance;
@property (nonatomic,strong) NSNumber *numOfFemalesRated;
@property (nonatomic,strong) NSNumber *numOfMalesRated;
@property (nonatomic,strong) NSNumber *userRatingFemale;
@property (nonatomic,strong) NSNumber *userRatingMale;
@property (nonatomic,strong) NSString *userStatus;
@property (nonatomic,strong) NSString *callId;
@property (nonatomic,strong) NSString *jabberId;
@property (nonatomic,strong) NSNumber *riderId;
@property (nonatomic,strong) UserChatProfile *chatProfile;
@property (nonatomic,strong) UserProfileRegionObject *regionDetail;

+(RKObjectMapping*)getRiderProfileObjectMapping;
@end
