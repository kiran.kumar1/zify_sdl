//
//  UserNotification.m
//  zify
//
//  Created by Anurag S Rathor on 28/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserNotification.h"

@implementation UserNotification
+(RKObjectMapping*)getUserNotificationObjectMapping{
        RKObjectMapping *userNotificationObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [userNotificationObjectMapping addAttributeMappingsFromArray:@[@"notificationType"]];
     [userNotificationObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"rideRequest" toKeyPath:@"rideRequest" withMapping:[UserNotificationDetail getUserNotificationDetailObjectMapping]]];
    return userNotificationObjectMapping;
}
@end
