//
//  TripHistory.m
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripHistory.h"


@implementation TripHistory
+(RKObjectMapping*) getTripHistoryObjectMapping{
    RKObjectMapping *tripHistoryObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [tripHistoryObjectMapping addAttributeMappingsFromArray:@[@"travelType"]];
    [tripHistoryObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:nil toKeyPath:@"tripRide" withMapping:[TripRide getTripRideObjectMapping]]];
    [tripHistoryObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:nil toKeyPath:@"tripDrive" withMapping:[TripDrive getTripDriveObjectMapping]]];
    return tripHistoryObjectMapping;
}
@end
