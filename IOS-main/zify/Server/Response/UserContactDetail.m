//
//  UserContactDetail.m
//  zify
//
//  Created by Anurag S Rathor on 06/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserContactDetail.h"

@implementation UserContactDetail
+(RKObjectMapping*)getUserContactDetailObjectMapping{
    RKObjectMapping *userChatIdentifierDetailObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [userChatIdentifierDetailObjectMapping addAttributeMappingsFromDictionary:@{@"qbChatUserId":@"chatIdentifier",
                                             @"qbFirstName":@"firstName",
                                             @"qbLastName":@"lastName",
                                             @"qbProfilePicUrl":@"userImgUrl"}];
    return userChatIdentifierDetailObjectMapping;
}

-(id)initWithChatIdentifier:(NSString *)chatIdentifier andCallIdentifier:(NSString *)callIdentifier andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andImageUrl:(NSString *)imageUrl{
    self = [super init];
    if (self) {
        _chatIdentifier = chatIdentifier;
        _callId = callIdentifier;
        _firstName = firstName;
        _lastName = lastName;
        _userImgUrl = imageUrl;
    }
    return self;
}
@end
