//
//  GuestUserSearchRide.h
//  zify
//
//  Created by Anurag S Rathor on 20/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface GuestUserSearchRide : NSObject
@property (nonatomic,strong) NSString *srcAdd;
@property (nonatomic,strong) NSString *destAdd;
@property (nonatomic,strong) NSNumber *availableSeats;
@property (nonatomic,strong) NSNumber *amountPerSeat;
@property (nonatomic,strong) NSString *currency;
@property (nonatomic,strong) NSString *profileImgUrl;
@property (nonatomic,strong) NSString *vehicleImgUrl;

@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *destCity;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *distanceUnit;
@property(nonatomic,strong)  NSString *pickupDelta;
@property(nonatomic,strong)  NSString *dropDelta;


+(RKObjectMapping*)getGuestUserSearchRideObjectMapping;
@end
