//
//  TripDrive.m
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripDrive.h"
#import "TripDriveRiderDetail.h"

@implementation TripDrive
+(RKObjectMapping*)getTripDriveObjectMapping{
    RKObjectMapping *tripDriveObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [tripDriveObjectMapping addAttributeMappingsFromArray:@[@"srcAddress",@"destAddress",
          @"departureTime",@"distance",@"zifyPoints",@"driveId",@"driverId",@"srcLat",
            @"srcLong",@"destLat",@"destLong",@"overviewPolylinePoints",
            @"status",@"duration",@"distanceUnit",@"enableStartBtn",@"numOfSeats",@"tripTime", @"rideStartTime", @"rideEndTime", @"driveStartTime", @"driveEndTime", @"zifyCoinsEarned"]];
    [tripDriveObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"rideDetailList" toKeyPath:@"driveRiders" withMapping:[TripDriveRiderDetail getTripDriveRiderDetailObjectMapping]]];
    [tripDriveObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"routeDetail" toKeyPath:@"routeDetail" withMapping:[TripRouteDetail getTripRouteDetailObjectMapping]]];
    return tripDriveObjectMapping;
}


-(id)initWithDriveDict:(NSDictionary *)driveInfo{
    if([self init]){
        _srcAddress = [driveInfo objectForKey:@"srcAdd"];
        _destAddress = [driveInfo objectForKey:@"destAdd"];
        _departureTime = [driveInfo objectForKey:@"departureTime"];
        _distance = [driveInfo objectForKey:@"distance"];
        _zifyPoints = [driveInfo objectForKey:@"zifyPoints"];
        _driveId = @([[driveInfo objectForKey:@"driveId"] intValue]);
        _driverId = @([[driveInfo objectForKey:@"driverId"] intValue]);
        _srcLat = [driveInfo objectForKey:@"srcLat"];
        _srcLong = [driveInfo objectForKey:@"srcLong"];
        _destLat = [driveInfo objectForKey:@"destLat"];
        _destLong = [driveInfo objectForKey:@"destLong"];
        _overviewPolylinePoints = [driveInfo objectForKey:@"overviewPolylinePoints"];
        _status = [driveInfo objectForKey:@"status"];
        _duration = @([[driveInfo objectForKey:@"duration"] doubleValue]);
        _distanceUnit = [driveInfo objectForKey:@"distanceUnit"];
        _enableStartBtn = @([[driveInfo objectForKey:@"enableStartBtn"] intValue]);
        _numOfSeats = @([[driveInfo objectForKey:@"numOfSeats"] intValue]);
        _tripTime = [driveInfo objectForKey:@"tripTime"];
        _rideStartTime = [driveInfo objectForKey:@"rideStartTime"];
        _rideEndTime = [driveInfo objectForKey:@"rideEndTime"];
        _driveStartTime = [driveInfo objectForKey:@"driveStartTime"];
        _driveEndTime = [driveInfo objectForKey:@"driveEndTime"];
        _zifyCoinsEarned = [driveInfo objectForKey:@"zifyCoinsEarned"];

        _routeDetail = [[TripRouteDetail alloc] initWithRouteDetailinfo:driveInfo];
        
        NSArray *ridersDetailsArr = [driveInfo objectForKey:@"rideDetailList"];
        if(ridersDetailsArr.count > 0){
            NSMutableArray *ridersDataMutableArr = [[NSMutableArray alloc] init];
            for(int i=0; i< ridersDetailsArr.count; i++){
                NSDictionary *riderDetaiObj = [ridersDetailsArr objectAtIndex:i];
                TripDriveRiderDetail *obj = [[TripDriveRiderDetail alloc] initWithTripDriveRiderDetail:riderDetaiObj];
                [ridersDataMutableArr addObject:obj];
            }
            _driveRiders = [[NSArray alloc] initWithArray:ridersDataMutableArr];
        }
    }
    return self;
}

@end
