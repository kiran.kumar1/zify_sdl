//
//  WalletTransactionStatement.m
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "WalletTransactionStatement.h"

@implementation WalletTransactionStatement
+(RKObjectMapping*)getWalletTransactionStatementObjectMapping{
    RKObjectMapping *accountTransactionStatementObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [accountTransactionStatementObjectMapping addAttributeMappingsFromDictionary:@{@"transactionId":@"transactionId",
                                            @"departureTime":@"departureTime",
                                            @"travelType":@"travelType",
                                            @"zifyPoints" :@"zifyPoints",
                                            @"TransactionType":@"transactionType",
                                            @"paymentTimestamp":@"paymentTimestamp"}];
    return accountTransactionStatementObjectMapping;
}
@end
