//
//  TripRide.h
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "TripRideDriveDetail.h"
#import "DriverDetail.h"
#import "TripRouteDetail.h"

@interface TripRide : NSObject
@property (nonatomic,strong) NSString *srcAddress;
@property (nonatomic,strong) NSString *destAddress;
@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *zifyPoints;
@property (nonatomic,strong) NSString *zifyCoinsEarned;
@property (nonatomic,strong) NSNumber *rideId;
@property (nonatomic,strong) NSNumber *driveId;
@property (nonatomic,strong) NSNumber *driverId;
@property (nonatomic,strong) NSString *routeId;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *srcLat;
@property (nonatomic,strong) NSString *srcLong;
@property (nonatomic,strong) NSString *destLat;
@property (nonatomic,strong) NSString *destLong;
@property (nonatomic,strong) NSString *overviewPolylinePoints;
@property (nonatomic,strong) NSNumber *hasBoarded;
@property (nonatomic,strong) NSNumber *duration;
@property (nonatomic,strong) NSString *distanceUnit;
@property (nonatomic,strong) NSString *tripTime;
@property (nonatomic,strong) NSString *rideStartTime;
@property (nonatomic,strong) NSString *rideEndTime;

@property (nonatomic,strong) NSString *srcNearLat;
@property (nonatomic,strong) NSString *srcNearLng;
@property (nonatomic,strong) NSString *destNearLat;
@property (nonatomic,strong) NSString *destNearLng;

@property (nonatomic,strong) NSString *srcActualLat;
@property (nonatomic,strong) NSString *srcActualLng;
@property (nonatomic,strong) NSString *destActualLat;
@property (nonatomic,strong) NSString *destActualLng;


@property (nonatomic,strong) TripRideDriveDetail *driveDetail;
@property (nonatomic,strong) DriverDetail *driverDetail;
@property (nonatomic,strong) TripRouteDetail *routeDetail;
+(RKObjectMapping*)getTripRideObjectMapping;
@end
