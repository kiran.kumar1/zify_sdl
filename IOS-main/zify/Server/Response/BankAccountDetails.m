//
//  BankAccountDetails.m
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "BankAccountDetails.h"

@implementation BankAccountDetails
+(RKObjectMapping*)getBankAccountDetailsObjectMapping{
    RKObjectMapping *bankAccountDetailsMapping = [RKObjectMapping mappingForClass:[self class]];
    [bankAccountDetailsMapping addAttributeMappingsFromArray:@[@"bankName",@"accountType",@"accountHolderName",
                        @"accountNumber",@"ifscCode",@"bankDetailId"]];
    return bankAccountDetailsMapping;
}
@end
