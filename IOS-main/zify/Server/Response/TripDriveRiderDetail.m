//
//  TripDriveRiderDetail.m
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripDriveRiderDetail.h"

@implementation TripDriveRiderDetail
+(RKObjectMapping*)getTripDriveRiderDetailObjectMapping{
    RKObjectMapping *tripDriveRiderDetailObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [tripDriveRiderDetailObjectMapping addAttributeMappingsFromArray:@[@"srcAddress",
                                        @"destAddress",@"hasBoarded",@"rideId",
                                         @"srcLat",@"srcLong",@"destLat",@"destLong", @"points"]];
    [tripDriveRiderDetailObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"riderProfile" toKeyPath:@"riderProfile" withMapping:[RiderProfile getRiderProfileObjectMapping]]];
    return tripDriveRiderDetailObjectMapping;
}
-(id)initWithTripDriveRiderDetail:(NSDictionary *)tripInfo{
    if([self init]){
        _srcAddress = [tripInfo objectForKey:@"srcAddress"];
        _destAddress = [tripInfo objectForKey:@"destAddress"];
        _hasBoarded = @([[tripInfo objectForKey:@"hasBoarded"] intValue]);
        _rideId = @([[tripInfo objectForKey:@"rideId"] intValue]);
        _srcLat = [tripInfo objectForKey:@"srcLat"];
        _srcLong = [tripInfo objectForKey:@"srcLong"];
        _destLat = [tripInfo objectForKey:@"destLat"];
        _destLong = [tripInfo objectForKey:@"destLong"];
        _points = [tripInfo objectForKey:@"points"];

    }
    return self;
}
@end
