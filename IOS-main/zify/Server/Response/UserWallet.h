//
//  UserWallet.h
//  zify
//
//  Created by Anurag S Rathor on 29/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface UserWallet : NSObject
@property (nonatomic,strong) NSNumber *availablePoints;
@property (nonatomic,strong) NSNumber *totalRides;
@property (nonatomic,strong) NSNumber *totalDrives;
@property (nonatomic,strong) NSNumber *distanceTravelled;
@property (nonatomic,strong) NSNumber *distanceOffered;
@property (nonatomic,strong) NSNumber *zifyCash;
@property (nonatomic,strong) NSString *totalRidesLabel;
@property (nonatomic,strong) NSString *totalDrivesLabel;
@property (nonatomic,strong) NSString *sharedDistanceLabel;
@property (nonatomic,strong) NSString *offeredDistanceLabel;
@property (nonatomic,strong) NSString *currency;

@property (nonatomic,strong) NSNumber *zifyCredits;
@property (nonatomic,strong) NSNumber *zifyMoney;


+(RKObjectMapping*)getUserWalletMapping;
@end
