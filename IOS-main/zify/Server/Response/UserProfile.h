//
//  UserProfile.h
//  zify
//
//  Created by Anurag S Rathor on 06/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDriverProfile.h"
#import "UserDocuments.h"
#import "UserPreferences.h"
#import "UserChatProfile.h"
#import <RestKit/RestKit.h>

@interface UserProfile : NSObject
@property (nonatomic,strong) NSString *loginMode;
@property (nonatomic,strong) NSString *userToken;
@property (nonatomic,strong) NSString *firstName;
@property (nonatomic,strong) NSString *lastName;
@property (nonatomic,strong) NSString *userName;
@property (nonatomic,strong) NSString *emailId;
@property (nonatomic,strong) NSString *callId;
@property (nonatomic,strong) NSString *isdCode;
@property (nonatomic,strong) NSString *mobile;
@property (nonatomic,strong) NSString *profileImgUrl;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSNumber *zifyCash;
@property (nonatomic,strong) NSNumber *zifyPromotionalPoints;
@property (nonatomic,strong) NSNumber *zifyUserPoints;
@property (nonatomic,strong) NSNumber *profileCompleteness;
@property (nonatomic,strong) NSNumber *userRatingMale;
@property (nonatomic,strong) NSNumber *userRatingFemale;
@property (nonatomic,strong) NSNumber *numOfMalesRated;
@property (nonatomic,strong) NSNumber *numOfFemalesRated;
@property (nonatomic,strong) NSString *userType;
@property (nonatomic,strong) NSString *userStatus;
@property (nonatomic,strong) NSString *userStatusReason;
@property (nonatomic,strong) NSNumber *totalRides;
@property (nonatomic,strong) NSNumber *totalDistance;
@property (nonatomic,strong) NSString *companyEmail;
@property (nonatomic,strong) NSNumber *mobileVerified;
@property (nonatomic,strong) NSString *homeAddress;
@property (nonatomic,strong) NSString *companyName;
@property (nonatomic,strong) NSString *officeAddress;
@property (nonatomic,strong) NSString *bloodGroup;
@property (nonatomic,strong) NSString *emergencyContact;
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *pincode;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,strong) NSNumber *isGlobal;
@property (nonatomic,strong) NSNumber *isGlobalPayment;
@property (nonatomic,strong) NSString *currency;
@property (nonatomic,strong) NSString *distanceUnit;
@property (nonatomic,strong) NSString *dob;
@property (nonatomic,strong) NSString *pushToken;
@property(nonatomic,strong) NSString *country;
@property (nonatomic,strong) UserDriverProfile *driverProfile;
@property (nonatomic,strong) UserDocuments *userDocuments;
@property (nonatomic,strong) UserPreferences *userPreferences;
@property (nonatomic,strong) UserChatProfile *userChatProfile;

+(RKObjectMapping*)getUserProfileObjectMapping;
+(UserProfile *) getCurrentUser;
+(void) deleteCurrentUser;
+(UserProfile *) getCurrentUserFromMainContext;
+(void)createCurrentUser:(NSData *)responseData;
+(NSString *)getCurrentUserToken;
+(NSNumber *)getCurrentUserId;
+(NSString *)getCurrentUserCallId;
@end
