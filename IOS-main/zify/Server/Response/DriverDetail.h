//
//  DriverDetail.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "DriverDetailDocuments.h"
#import "UserChatProfile.h"
#import "UserProfileRegionObject.h"

@interface DriverDetail : NSObject
@property (nonatomic,strong) NSString *firstName;
@property (nonatomic,strong) NSString *lastName;
@property (nonatomic,strong) NSString *driverStatus;
@property (nonatomic,strong) NSString *companyName;
@property (nonatomic,strong) NSNumber *totalDistance;
@property (nonatomic,strong) NSString *profileImgUrl;
@property (nonatomic,strong) NSNumber *driverRatingFemale;
@property (nonatomic,strong) NSNumber *driverRatingMale;
@property (nonatomic,strong) NSNumber *numOfFemalesRated;
@property (nonatomic,strong) NSNumber *numOfMalesRated;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,strong) NSString *jabberId;
@property (nonatomic,strong) NSString *callId;

@property (nonatomic,strong) DriverDetailDocuments *documents;
@property (nonatomic,strong) UserChatProfile *chatProfile;

@property (nonatomic,strong) UserProfileRegionObject *regionDetail;

+(RKObjectMapping*)getDriverDetailObjectMapping;
@end
