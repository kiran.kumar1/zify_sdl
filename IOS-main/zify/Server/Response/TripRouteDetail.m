//
//  TripRouteDetails.m
//  zify
//
//  Created by Anurag S Rathor on 05/02/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "TripRouteDetail.h"
@implementation TripRouteDetail
+(RKObjectMapping*)getTripRouteDetailObjectMapping{
    RKObjectMapping *routeDetailMapping = [RKObjectMapping mappingForClass:[self class]];
    [routeDetailMapping addAttributeMappingsFromArray:@[@"srcAdd",@"destAdd",@"srcLat",
                                    @"srcLong",@"destLat",@"destLong",@"overviewPolylinePoints"]];
    return routeDetailMapping;
}

-(id)initWithRouteDetailinfo:(NSDictionary *)routeInfo{
    if([self init]){
        _srcAdd = [routeInfo objectForKey:@"srcAdd"];
        _destAdd = [routeInfo objectForKey:@"destAdd"];
        _srcLat = [routeInfo objectForKey:@"srcLat"];
        _srcLong = [routeInfo objectForKey:@"srcLong"];
        _destLat = [routeInfo objectForKey:@"destLat"];
        _destLong = [routeInfo objectForKey:@"destLong"];
        _overviewPolylinePoints = [routeInfo objectForKey:@"overviewPolylinePoints"];
    }
    return self;
}
@end
