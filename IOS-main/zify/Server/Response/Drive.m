//
//  SearchRide.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "Drive.h"


@implementation Drive

-(id)initWithDriveDict:(NSDictionary *)driveInfo{
    if([self init]){
        _driveId = [driveInfo objectForKey:@"driveId"];
        _departureTime = [driveInfo objectForKey:@"departureTime"];
    }
    return self;
}
@end
