//
//  TripRouteDetails.h
//  zify
//
//  Created by Anurag S Rathor on 05/02/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface TripRouteDetail : NSObject
@property (nonatomic,strong) NSString *srcAdd;
@property (nonatomic,strong) NSString *destAdd;
@property (nonatomic,strong) NSString *srcLat;
@property (nonatomic,strong) NSString *srcLong;
@property (nonatomic,strong) NSString *destLat;
@property (nonatomic,strong) NSString *destLong;
@property (nonatomic,strong) NSString *overviewPolylinePoints;


+(RKObjectMapping*)getTripRouteDetailObjectMapping;

-(id)initWithRouteDetailinfo:(NSDictionary *)routeInfo;
@end
