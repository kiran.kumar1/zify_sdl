//
//  UserDocuments.h
//  zify
//
//  Created by Anurag S Rathor on 02/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>

@interface UserDocuments : NSObject

@property (nonatomic,strong) NSString *idCardNum;
@property (nonatomic,strong) NSString *idCardType;
@property (nonatomic,strong) NSString *idCardImgUrl;
@property (nonatomic,strong) NSNumber *isIdCardDocUploaded;
@property (nonatomic,strong) NSString *drivingLicenceNum;
@property (nonatomic,strong) NSString *vehicleInsuranceNum;
@property (nonatomic,strong) NSString *vehicleModel;
@property (nonatomic,strong) NSString *vehicleRegistrationNum;
@property (nonatomic,strong) NSString *vehicleImgUrl;
@property (nonatomic,strong) NSNumber *isVehicleImgUploaded;
+(RKObjectMapping*)getUserDocumentsObjectMapping;
@end
