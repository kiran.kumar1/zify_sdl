//
//  CurrentTripNPendingRatingResponse.h
//  zify
//
//  Created by Anurag S Rathor on 23/06/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TripDrive.h"
#import "TripRide.h"
#import "RestKit/RestKit.h"

@interface CurrentTripNPendingRatingResponse : NSObject
@property (nonatomic,strong) TripDrive *pendingDriveRating;
@property (nonatomic,strong) TripRide *pendingRideRating;
@property (nonatomic,strong) TripDrive *currentDrive;
@property (nonatomic,strong) TripRide *currentRide;
+(RKObjectMapping*)getCurrentTripNPendingRatingObjectMapping;
@end
