//
//  SearchRide.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "DriverDetail.h"

@interface  SearchRide: NSObject
@property (nonatomic,strong) NSString *routeId;
@property (nonatomic,strong) NSString *driverId;
@property (nonatomic,strong) NSString *driveId;
@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic,strong) NSString *srcAdd;
@property (nonatomic,strong) NSString *destAdd;
@property (nonatomic,strong) NSNumber *numOfSeats;
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *destCity;
@property (nonatomic,strong) NSNumber *availableSeats;
@property (nonatomic,strong) NSNumber *amountPerSeat;
@property (nonatomic,strong) NSString *currency;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *distanceUnit;
@property (nonatomic,strong) NSString *srcLat;
@property (nonatomic,strong) NSString *srcLong;
@property (nonatomic,strong) NSString *destLat;
@property (nonatomic,strong) NSString *destLong;
@property (nonatomic,strong) NSString *overviewPolylinePoints;
@property (nonatomic,strong) NSNumber *isFavDrive;
@property (nonatomic,strong) NSString *driverRouteId;
@property (nonatomic,strong) NSNumber *isExpiredDrive;
@property(nonatomic,strong)  NSString *pickupDelta;
@property(nonatomic,strong)  NSString *dropDelta;
@property (nonatomic,strong) NSString *destNearLat;
@property (nonatomic,strong) NSString *destNearLong;
@property (nonatomic,strong) NSString *srcNearLat;
@property (nonatomic,strong) NSString *srcNearLong;
@property (nonatomic,strong) DriverDetail *driverDetail;


+(RKObjectMapping*)getSearchRideObjectMapping;
@end
