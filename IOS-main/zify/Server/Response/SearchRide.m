//
//  SearchRide.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "SearchRide.h"


@implementation SearchRide
+(RKObjectMapping*)getSearchRideObjectMapping{
    RKObjectMapping *serchRideObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [serchRideObjectMapping addAttributeMappingsFromArray:@[@"routeId",@"driverId",@"driveId",@"departureTime",@"numOfSeats",@"city",@"availableSeats",@"srcAdd",@"destAdd",@"destCity",@"amountPerSeat",@"currency",@"distance",@"distanceUnit",@"srcLat",@"srcLong",@"destLat",@"destLong",@"overviewPolylinePoints",
        @"isFavDrive",@"driverRouteId",@"isExpiredDrive",
        @"pickupDelta",@"dropDelta",@"lastDepartureTime",@"destNearLat",@"destNearLong", @"srcNearLat", @"srcNearLong"]];
    [serchRideObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"driverDetail" toKeyPath:@"driverDetail" withMapping:[DriverDetail getDriverDetailObjectMapping]]];
    //NSLog(@"serchRideObjectMapping is %@", serchRideObjectMapping);
    return serchRideObjectMapping;
}
@end
