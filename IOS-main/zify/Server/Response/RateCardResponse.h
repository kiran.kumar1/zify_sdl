//
//  RateCardResponse.h
//  zify
//
//  Created by Anurag S Rathor on 05/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface RateCardResponse : NSObject
@property (nonatomic,strong) NSString *riderCostingLocal;
@property (nonatomic,strong) NSString *driverEarningLocal;
@property (nonatomic,strong) NSString *riderCostingIntercity;
@property (nonatomic,strong) NSString *driverEarningIntercity;
@property(nonatomic,strong) NSString *currency;
@property(nonatomic,strong) NSString *label;
+(RKObjectMapping*)getRateCardResponseObjectMapping;
@end
