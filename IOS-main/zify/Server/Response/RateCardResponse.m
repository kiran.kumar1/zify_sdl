//
//  RateCardResponse.m
//  zify
//
//  Created by Anurag S Rathor on 05/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "RateCardResponse.h"
@implementation RateCardResponse
+(RKObjectMapping*)getRateCardResponseObjectMapping{
    RKObjectMapping *rateCardResponseObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [rateCardResponseObjectMapping addAttributeMappingsFromArray:@[@"riderCostingLocal",
                        @"driverEarningLocal",@"currency",@"riderCostingIntercity",
                        @"driverEarningIntercity",@"label"]];
    return rateCardResponseObjectMapping;
}
@end
