//
//  BankRedemptionDetails.h
//  zify
//
//  Created by Anurag S Rathor on 27/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "BankAccountDetails.h"

@interface BankRedemptionDetails : NSObject
@property (nonatomic,strong) NSNumber *redeemableAmount;
@property (nonatomic,strong) NSArray *bankDetails;
+(RKObjectMapping*)getBankRedemptionDetailsObjectMapping;
@end
