//
//  UserDriverProfile.m
//  zify
//
//  Created by Anurag S Rathor on 06/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserDriverProfile.h"


@implementation UserDriverProfile

+(RKObjectMapping*)getUserDriverProfileObjectMapping{
    RKObjectMapping *driverProfileMapping = [RKObjectMapping mappingForClass:[self class]];
    [driverProfileMapping addAttributeMappingsFromArray:@[@"driverId",
                                                          @"driverRatingMale",
                                                          @"driverRatingFemale",
                                                          @"numOfMalesRated",
                                                          @"numOfFemalesRated",
                                                          @"driverStatus",
                                                          @"driverStatusReason",
                                                          @"totalDrives",
                                                          @"totalDistance"]];
    return driverProfileMapping;
}
@end
