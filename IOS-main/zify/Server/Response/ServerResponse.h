//
//  ServerResponse.h
//  zify
//
//  Created by Anurag S Rathor on 06/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "ServerRequest.h"

@interface ServerResponse : NSObject
@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSString *messageCode;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) id responseObject;
@property(nonatomic,strong) NSDictionary *statusDict;

+(ServerResponse *)getServerResponse:(NSData *)responseData withRequest:(ServerRequest *)request;
-(ServerResponse *)initWithServerResponseFromLaterZenparkServicesWithData:(NSDictionary *)responseData withRequest:(ServerRequest *)request;
-(ServerResponse *)initWithFDjDataResponse:(NSDictionary *)dataResponse;



@end
