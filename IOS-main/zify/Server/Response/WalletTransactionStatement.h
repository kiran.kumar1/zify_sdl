//
//  WalletTransactionStatement.h
//  zify
//
//  Created by Anurag S Rathor on 29/01/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface WalletTransactionStatement : NSObject
@property (nonatomic,strong) NSNumber *transactionId;
@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic,strong) NSString *travelType;
@property (nonatomic,strong) NSString *zifyPoints;
@property (nonatomic,strong) NSString *transactionType;
@property (nonatomic,strong) NSString *paymentTimestamp;
+(RKObjectMapping*)getWalletTransactionStatementObjectMapping;
@end
