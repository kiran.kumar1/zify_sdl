//
//  TripRideDriveDetails.h
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface  TripRideDriveDetail: NSObject
@property (nonatomic,strong) NSString *srcAdd;
@property (nonatomic,strong) NSString *destAdd;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *etaUnit;
@property (nonatomic,strong) NSString *etaVal;
+(RKObjectMapping*)getTripRideDriveDetailObjectMapping;
@end
