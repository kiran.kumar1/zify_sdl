//
//  UserPreferences.m
//  zify
//
//  Created by Anurag S Rathor on 14/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "UserPreferences.h"

@implementation UserPreferences
+(RKObjectMapping*)getUserPreferencesResponseObjectMapping{
    RKObjectMapping *userPreferencesResponseMapping = [RKObjectMapping mappingForClass:[self class]];
    [userPreferencesResponseMapping addAttributeMappingsFromArray:@[@"city",
            @"destinationAddress",@"destinationLatitude",@"destinationLongitude",
            @"sourceAddress",@"sourceLatitude",@"sourceLongitude",@"startTime",@"returnTime",
            @"onwardRouteId",@"onwardPolyline",@"returnRouteId",@"returnPolyline",@"userMode"]];
    return userPreferencesResponseMapping;
}
@end
