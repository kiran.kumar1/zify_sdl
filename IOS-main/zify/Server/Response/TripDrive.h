//
//  TripDrive.h
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "TripRouteDetail.h"

@interface TripDrive : NSObject
@property (nonatomic,strong) NSString *srcAddress;
@property (nonatomic,strong) NSString *destAddress;
@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *zifyPoints;
@property (nonatomic,strong) NSArray *driveRiders;
@property (nonatomic,strong) NSNumber *driveId;
@property (nonatomic,strong) NSNumber *driverId;
@property (nonatomic,strong) NSNumber *duration;
@property (nonatomic,strong) NSNumber *enableStartBtn;
@property (nonatomic,strong) NSString *srcLat;
@property (nonatomic,strong) NSString *srcLong;
@property (nonatomic,strong) NSString *destLat;
@property (nonatomic,strong) NSString *destLong;
@property (nonatomic,strong) NSString *overviewPolylinePoints;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *distanceUnit;
@property (nonatomic,strong) NSNumber *numOfSeats;
@property (nonatomic,strong) NSString *tripTime;
@property (nonatomic,strong) TripRouteDetail *routeDetail;

@property (nonatomic,strong) NSString *rideStartTime;
@property (nonatomic,strong) NSString *rideEndTime;
@property (nonatomic,strong) NSString *driveStartTime;
@property (nonatomic,strong) NSString *driveEndTime;

@property (nonatomic,strong) NSNumber *zifyCoinsEarned;


+(RKObjectMapping*)getTripDriveObjectMapping;

-(id)initWithDriveDict:(NSDictionary *)driveInfo;

@end
