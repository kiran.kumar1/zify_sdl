//
//  UserDriverProfile.h
//  zify
//
//  Created by Anurag S Rathor on 06/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface UserDriverProfile : NSObject
@property (nonatomic,strong) NSNumber *driverId;
@property (nonatomic,strong) NSNumber *driverRatingMale;
@property (nonatomic,strong) NSNumber *driverRatingFemale;
@property (nonatomic,strong) NSNumber *numOfMalesRated;
@property (nonatomic,strong) NSNumber *numOfFemalesRated;
@property (nonatomic,strong) NSString *driverStatus;
@property (nonatomic,strong) NSString *driverStatusReason;
@property (nonatomic,strong) NSNumber *totalDrives;
@property (nonatomic,strong) NSNumber *totalDistance;
+(RKObjectMapping*)getUserDriverProfileObjectMapping;
@end
