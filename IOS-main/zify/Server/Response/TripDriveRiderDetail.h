//
//  TripDriveRiderDetail.h
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <RestKit/RestKit.h>
#import  "RiderProfile.h"

@interface TripDriveRiderDetail : NSObject
@property (nonatomic,strong) NSString *srcAddress;
@property (nonatomic,strong) NSString *destAddress;
@property (nonatomic,strong) NSString *srcLat;
@property (nonatomic,strong) NSString *srcLong;
@property (nonatomic,strong) NSString *destLat;
@property (nonatomic,strong) NSString *destLong;
@property (nonatomic,strong) NSString *points;
@property (nonatomic,strong) NSNumber *hasBoarded;
@property (nonatomic,strong) NSNumber *rideId;
@property (nonatomic,strong) RiderProfile *riderProfile;
+(RKObjectMapping*)getTripDriveRiderDetailObjectMapping;

-(id)initWithTripDriveRiderDetail:(NSDictionary *)tripInfo;
@end
