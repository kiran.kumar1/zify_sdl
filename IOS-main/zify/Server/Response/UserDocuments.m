//
//  UserDocuments.m
//  zify
//
//  Created by Anurag S Rathor on 02/08/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserDocuments.h"

@implementation UserDocuments

+(RKObjectMapping*)getUserDocumentsObjectMapping{
    RKObjectMapping *userDocumentsMapping = [RKObjectMapping mappingForClass:[self class]];
    [userDocumentsMapping addAttributeMappingsFromArray:@[@"drivingLicenceNum",@"idCardNum",@"idCardType",
                    @"isVehicleImgUploaded",@"vehicleImgUrl",@"vehicleInsuranceNum",
                    @"vehicleModel",@"vehicleRegistrationNum",@"isIdCardDocUploaded",@"idCardImgUrl"]];
    return userDocumentsMapping;
}
@end
