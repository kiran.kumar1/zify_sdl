//
//  SearchRide.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ZenparkSignInObj.h"


@implementation ZenparkSignInObj
+(RKObjectMapping*)getZenParkSignObjectMapping{
    RKObjectMapping *objectMapping=[RKObjectMapping mappingForClass:[self class]];
    [objectMapping addAttributeMappingsFromArray:@[@"error",@"result"]];
    //NSLog(@"serchRideObjectMapping is %@", serchRideObjectMapping);
    return objectMapping;
}
@end
