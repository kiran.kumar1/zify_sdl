//
//  SearchRide.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface  ZenparkSignInObj: NSObject
@property (nonatomic,strong) NSString *error;
@property (nonatomic,strong) NSString *result;
+(RKObjectMapping*)getZenParkSignObjectMapping;
@end
