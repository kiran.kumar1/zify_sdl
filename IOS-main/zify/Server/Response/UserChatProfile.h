//
//  UserChatProfile.h
//  zify
//
//  Created by Anurag S Rathor on 12/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface UserChatProfile : NSObject
@property (nonatomic,strong) NSString *chatUserId;
@property (nonatomic,strong) NSString *chatUserPassword;
@property (nonatomic,strong) NSString *chatUserName;
@property (nonatomic,strong) NSString *chatUserEmail;
+(RKObjectMapping*)getUserChatProfileObjectMapping;
+(void)createCurrentChatUserProfile:(UserChatProfile *)userChatProfile;
+(UserChatProfile *)getCurrentUserChatProfileInContext:(NSManagedObjectContext *)context;
+(void) deleteCurrentChatUserProfile;
-(id)initWithChatUserId:(NSString *)userId  andUserName:(NSString *)userName andUserEmail:(NSString *)userEmail andChatPassword:(NSString *)chatPassword;
@end
