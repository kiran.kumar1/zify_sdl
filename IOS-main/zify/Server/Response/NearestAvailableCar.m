//
//  NearestAvailableCar.m
//  zify
//
//  Created by Anurag S Rathor on 23/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "NearestAvailableCar.h"

@implementation NearestAvailableCar
+(RKObjectMapping*)getNearestAvailableCarObjectMapping{
    RKObjectMapping *nearestAvailableCarObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [nearestAvailableCarObjectMapping addAttributeMappingsFromDictionary:@{@"latitude":@"latitude",
                                            @"longitute":@"longitude"}];
    return nearestAvailableCarObjectMapping;
}
@end
