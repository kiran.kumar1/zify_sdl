//
//  TripRideDriveDetails.m
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "TripRideDriveDetail.h"

@implementation TripRideDriveDetail
+(RKObjectMapping*)getTripRideDriveDetailObjectMapping{
    RKObjectMapping *tripRideDriveDetailsObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [tripRideDriveDetailsObjectMapping addAttributeMappingsFromArray:@[@"srcAdd",
                                    @"destAdd",@"status",@"etaUnit",@"etaVal"]];
    return tripRideDriveDetailsObjectMapping;
}
@end
