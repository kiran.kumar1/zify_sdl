//
//  TripHistory.h
//  zify
//
//  Created by Anurag S Rathor on 28/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripDrive.h"
#import "TripRide.h"

@interface TripHistory : NSObject
@property (nonatomic,strong) NSString *travelType;
@property (nonatomic,strong) TripDrive *tripDrive;
@property (nonatomic,strong) TripRide *tripRide;
+(RKObjectMapping*)getTripHistoryObjectMapping;
@end
