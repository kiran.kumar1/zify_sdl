//
//  UserContactDetail.h
//  zify
//
//  Created by Anurag S Rathor on 06/05/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface UserContactDetail : NSObject
@property (nonatomic,strong) NSString *chatIdentifier;
@property (nonatomic,strong) NSString *callId;
@property (nonatomic,strong) NSString *firstName;
@property (nonatomic,strong) NSString *lastName;
@property (nonatomic,strong) NSString *userImgUrl;
+(RKObjectMapping*)getUserContactDetailObjectMapping;
-(id)initWithChatIdentifier:(NSString *)chatIdenfier andCallIdentifier:(NSString *)callIdentifier andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andImageUrl:(NSString *)imageUrl;
@end
