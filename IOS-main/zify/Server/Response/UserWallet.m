//
//  UserWallet.m
//  zify
//
//  Created by Anurag S Rathor on 29/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserWallet.h"

@implementation UserWallet
+(RKObjectMapping*)getUserWalletMapping{
    RKObjectMapping *userWalletMapping = [RKObjectMapping mappingForClass:[self class]];
    [userWalletMapping addAttributeMappingsFromArray:@[@"availablePoints",
                    @"totalRides",@"totalDrives",@"distanceTravelled",@"distanceOffered",
                    @"totalRidesLabel",@"totalDrivesLabel",@"sharedDistanceLabel",
                    @"offeredDistanceLabel",@"zifyCash",@"currency", @"zifyCredits", @"zifyMoney"]];
    return userWalletMapping;
}
@end
