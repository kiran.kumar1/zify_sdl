


//
//  UserProfile.m
//  zify
//
//  Created by Anurag S Rathor on 06/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "UserProfile.h"
#import "DBUserDataInterface.h"
#import "UserAccessData.h"
#import "zify-Swift.h"

@implementation UserProfile
+(RKObjectMapping*)getUserProfileObjectMapping{
    RKObjectMapping *userProfileObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [userProfileObjectMapping addAttributeMappingsFromArray:@[@"loginMode",
                                                              @"userToken",@"firstName",@"lastName",@"userName",@"emailId",@"isdCode",@"mobile",
                                                              @"gender",@"zifyCash",@"zifyPromotionalPoints",@"zifyUserPoints",@"profileCompleteness",
                                                              @"userRatingMale",@"userRatingFemale",@"numOfMalesRated",@"numOfFemalesRated",
                                                              @"userType",@"userStatus",@"userStatusReason",@"totalRides",@"totalDistance",
                                                              @"companyEmail",@"mobileVerified",@"profileImgUrl",@"homeAddress",
                                                              @"companyName",@"officeAddress",@"bloodGroup",@"emergencyContact",@"city",@"pincode",
                                                              @"userId",@"callId",@"currency",@"distanceUnit",@"isGlobal",@"isGlobalPayment",@"dob",@"pushToken",@"country"]];
    [userProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"driverProfile" toKeyPath:@"driverProfile" withMapping:[UserDriverProfile getUserDriverProfileObjectMapping]]];
    [userProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"userDocs" toKeyPath:@"userDocuments" withMapping:[UserDocuments getUserDocumentsObjectMapping]]];
    [userProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"userPreferences" toKeyPath:@"userPreferences" withMapping:[UserPreferences getUserPreferencesResponseObjectMapping]]];
    [userProfileObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"qbUserInfo" toKeyPath:@"userChatProfile" withMapping:[UserChatProfile getUserChatProfileObjectMapping]]];
    return userProfileObjectMapping;
}

+(UserProfile *) getCurrentUser{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    return [UserProfile getCurrentUserFromContext:context];
}

+(void) deleteCurrentUser{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    UserAccessData *accessData = [UserProfile getCurrentUserAccessDataFromContext:context];
    
    //[context performBlockAndWait:^{
    if(accessData){
        [context deleteObject:accessData];
        [[DBUserDataInterface sharedInstance] saveSharedContext];
    }
    //}];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUserToken"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUserId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUserCallId"];
}


+(UserProfile *) getCurrentUserFromMainContext{
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedMainContext];
    return [UserProfile getCurrentUserFromContext:context];
}

+(void)createCurrentUser:(NSData *)responseData{
    NSError *error;
    NSDictionary * temp = [NSJSONSerialization JSONObjectWithData:responseData options:(NSJSONReadingMutableLeaves) error:&error];
    
    NSManagedObjectContext *context = [[DBUserDataInterface sharedInstance] sharedContext];
    NSString *userDataString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //[context performBlockAndWait:^{
    [UserAccessData createUserAccessDataWithType:@"userProfile" andValue:userDataString inContext:context];
    [[DBUserDataInterface sharedInstance] saveSharedContext];
    if(temp != nil){
        [UserProfile saveUserTokenInPrefs:temp];
    }
    //}];
}
+(void)saveUserTokenInPrefs:(NSDictionary *)response{
    
    NSArray *allKeys = [response allKeys];
    if([allKeys containsObject:@"user"]){
        NSDictionary *userInfo = [response objectForKey:@"user"];
        if(userInfo == (NSString *)[NSNull null] || userInfo == nil ){
            return;
        }
        if([[userInfo allKeys] containsObject:@"userToken"]){
            NSString *usertoken = [userInfo objectForKey:@"userToken"];
            if(usertoken != nil){
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:usertoken forKey:@"userTokenValue"];
                [prefs synchronize];
            }
        }
    }
}

+(UserProfile *)getCurrentUserFromContext:(NSManagedObjectContext *)context{
    @try{
        UserAccessData *accessData = [UserProfile getCurrentUserAccessDataFromContext:context];
        if(accessData) {
            if(accessData.value){
                NSDictionary *mappingDictionary=@{@"user":[UserProfile getUserProfileObjectMapping]};
                NSError *error;
                NSData *data = [accessData.value dataUsingEncoding:NSUTF8StringEncoding];
                if(!data) return nil;
                id parsedData = [RKMIMETypeSerialization objectFromData:data MIMEType:@"application/json" error:&error];
                RKMapperOperation *mappingOperation=[[RKMapperOperation alloc]initWithRepresentation:parsedData mappingsDictionary:mappingDictionary];
                [mappingOperation execute:&error];
                if(error) return nil;
                UserProfile *userProfile = (UserProfile *)[mappingOperation.mappingResult.array objectAtIndex:0];
                return userProfile;
            }else{
                return nil;
            }
        }else{
            return nil;
        }
    }@catch(NSException *exp){
        
    }
    return nil;
}

+(UserAccessData *) getCurrentUserAccessDataFromContext:(NSManagedObjectContext *)context{
    __block NSArray *fetchedObjects = nil;
    __block NSError *error = nil;
    //[context performBlockAndWait:^{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity=[NSEntityDescription entityForName:@"UserAccessData" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dataType = %@",@"userProfile"];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    //}];
    if(!error && fetchedObjects.count != 0) return (UserAccessData *)[fetchedObjects objectAtIndex:0];
    return nil;
}

+(NSString *)getCurrentUserToken{
    NSString *currentUserToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"currentUserToken"];
    if(!currentUserToken){
        UserProfile *currentUser = [UserProfile getCurrentUser];
        if(currentUser){
            currentUserToken = currentUser.userToken;
            [[NSUserDefaults standardUserDefaults] setValue:currentUserToken forKey:@"currentUserToken"];
        }
    }
    if(currentUserToken.length == 0 || currentUserToken == nil){
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        currentUserToken = [prefs objectForKey:@"userTokenValue"];
    }
    return currentUserToken;
}

+(NSNumber *)getCurrentUserId{
    NSNumber *currentUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentUserId"];
    if(!currentUserId){
        UserProfile *currentUser = [UserProfile getCurrentUser];
        if(currentUser){
            currentUserId = currentUser.userId;
            [[NSUserDefaults standardUserDefaults] setValue:currentUserId forKey:@"currentUserId"];
        }
    }
    return currentUserId;
}

+(NSString *)getCurrentUserCallId{
    NSString *currentUserCallId = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentUserCallId"];
    if(!currentUserCallId){
        UserProfile *currentUser = [UserProfile getCurrentUser];
        if(currentUser){
            currentUserCallId = currentUser.callId;
            [[NSUserDefaults standardUserDefaults] setValue:currentUserCallId forKey:@"currentUserCallId"];
        }
    }
    return currentUserCallId;
}
@end
