//
//  DriveRoute.h
//  zify
//
//  Created by Anurag S Rathor on 23/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "LocalityInfo.h"

@interface DriveRoute : NSObject
@property (nonatomic,strong) NSString *srcLat;
@property (nonatomic,strong) NSString *srcLong;
@property (nonatomic,strong) NSString *destLat;
@property (nonatomic,strong) NSString *destLong;
@property (nonatomic,strong) NSString *srcAdd;
@property (nonatomic,strong) NSString *destAdd;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *distanceUnit;
@property (nonatomic,strong) NSString *routeId;
@property (nonatomic,strong) NSString *overviewPolylinePoints;
-(id)initWithSource:(LocalityInfo *)sourceLocality andDestination:(LocalityInfo *)destinationLocality andRouteId:(NSString *)routeId andPolyline:(NSString *)polyline;
+(RKObjectMapping*)getDriveRouteObjectMapping;
@end
