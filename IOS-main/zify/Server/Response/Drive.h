//
//  SearchRide.h
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface  Drive: NSObject

@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic,strong) NSString *driveId;

-(id)initWithDriveDict:(NSDictionary *)driveInfo;
@end
