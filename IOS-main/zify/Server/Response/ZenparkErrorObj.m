//
//  SearchRide.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "ZenparkErrorObj.h"


@implementation ZenparkErrorObj
+(RKObjectMapping*)getZenParkErrorObjectMapping{
    RKObjectMapping *objectMapping=[RKObjectMapping mappingForClass:[self class]];
    [objectMapping addAttributeMappingsFromArray:@[@"Id",@"Name"]];
    //NSLog(@"serchRideObjectMapping is %@", serchRideObjectMapping);
    return objectMapping;
}
@end
