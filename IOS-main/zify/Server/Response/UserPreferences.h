//
//  UserPreferences.h
//  zify
//
//  Created by Anurag S Rathor on 14/07/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface UserPreferences : NSObject
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *destinationAddress;
@property (nonatomic,strong) NSNumber *destinationLatitude;
@property (nonatomic,strong) NSNumber *destinationLongitude;
@property (nonatomic,strong) NSString *sourceAddress;
@property (nonatomic,strong) NSNumber *sourceLatitude;
@property (nonatomic,strong) NSNumber *sourceLongitude;
@property (nonatomic,strong) NSString *returnTime;
@property (nonatomic,strong) NSString *startTime;
@property (nonatomic,strong) NSString *onwardRouteId;
@property (nonatomic,strong) NSString *onwardPolyline;
@property (nonatomic,strong) NSString *returnRouteId;
@property (nonatomic,strong) NSString *returnPolyline;
@property (nonatomic,strong) NSString *userMode;
+(RKObjectMapping*)getUserPreferencesResponseObjectMapping;
@end

