//
//  FBLoginResponse.m
//  zify
//
//  Created by Anurag S Rathor on 08/06/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "FBLoginResponse.h"

@implementation FBLoginResponse
+(FBLoginResponse *)getFaceBookLoginResponse:(NSDictionary *)dictionary{
    NSError *error;
    RKObjectMapping *loginResponseMapping=[RKObjectMapping mappingForClass:[self class]];
    [loginResponseMapping addAttributeMappingsFromDictionary:@{@"email":@"email",
                                                               @"gender":@"gender",
                                                               @"first_name":@"firstName",
                                                               @"last_name":@"lastName",
                                                               @"id":@"facebookId"}];
    NSDictionary *mappingDictionary=@{[NSNull null]:loginResponseMapping};
    RKMapperOperation *mappingOperation=[[RKMapperOperation alloc]initWithRepresentation:dictionary mappingsDictionary:mappingDictionary];
    [mappingOperation execute:&error];
    FBLoginResponse *loginResponse = (FBLoginResponse*)[mappingOperation.mappingResult.array objectAtIndex:0];
    return loginResponse;
}
@end
