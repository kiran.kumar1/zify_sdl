//
//  NearestAvailableCarsResponse.h
//  zify
//
//  Created by Anurag S Rathor on 23/04/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface NearestAvailableCarsResponse : NSObject
@property (nonatomic,strong) NSString *carImgUrl;
@property (nonatomic,strong) NSArray *locationList;
+(RKObjectMapping*)getNearestAvailableCarsResponseObjectMapping;
@end
