//
//  GuestUserSearchRide.m
//  zify
//
//  Created by Anurag S Rathor on 20/04/17.
//  Copyright © 2017 zify. All rights reserved.
//

#import "GuestUserSearchRide.h"

@implementation GuestUserSearchRide
+(RKObjectMapping*)getGuestUserSearchRideObjectMapping{
    RKObjectMapping *guestUserSearchRideObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [guestUserSearchRideObjectMapping addAttributeMappingsFromArray:@[@"amountPerSeat",@"currency",@"availableSeats",@"srcAdd",@"destAdd",@"vehicleImgUrl",@"profileImgUrl", @"departureTime", @"city", @"destCity", @"distance", @"distanceUnit", @"pickupDelta", @"dropDelta"]];
    return guestUserSearchRideObjectMapping;
}
@end

