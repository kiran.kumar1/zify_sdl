//
//  SearchRide.m
//  zify
//
//  Created by Anurag S Rathor on 25/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import "FDJLocationObject.h"


@implementation FDJLocationObject
+(RKObjectMapping*)getFDJLocationObjectMapping{
    RKObjectMapping *fdjObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [fdjObjectMapping addAttributeMappingsFromArray:@[@"ID",@"CX",@"CY",@"ADRESSE"]];
    //NSLog(@"serchRideObjectMapping is %@", serchRideObjectMapping);
    return fdjObjectMapping;
}
@end
