//
//  PushNotificationInfo.h
//  zify
//
//  Created by Anurag S Rathor on 10/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PushNotificationInfo.h"

@interface PushNotificationInfo : NSObject
-(BOOL)showNotification:(BOOL)isAppLaunch;
@end
