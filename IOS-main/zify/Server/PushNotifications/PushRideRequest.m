//
//  PushRideRequest.m
//  zify
//
//  Created by Anurag S Rathor on 10/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushRideRequest.h"

@implementation PushRideRequest
+(RKObjectMapping*)getPushRideRequestObjectMapping{
     RKObjectMapping *rideRequestObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [rideRequestObjectMapping addAttributeMappingsFromArray:@[@"title"]];
    [rideRequestObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"tripDetails" toKeyPath:@"tripDetails" withMapping:[PushTripDetails getPushTripDetailsObjectMapping]]];
    [rideRequestObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"userDetails" toKeyPath:@"userDetails" withMapping:[PushUserDetails getPushUserDetailsObjectMapping]]];
    return rideRequestObjectMapping;
}
@end
