//
//  PushTripNotification.m
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushTripNotification.h"

@implementation PushTripNotification
+(RKObjectMapping*)getPushTripNotificationObjectMapping{
    RKObjectMapping *tripNotificationObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [tripNotificationObjectMapping addAttributeMappingsFromArray:@[@"title",@"category"]];
    [tripNotificationObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"tripDetails" toKeyPath:@"tripDetails" withMapping:[PushTripDetails getPushTripDetailsObjectMapping]]];
    [tripNotificationObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"userDetails" toKeyPath:@"userDetails" withMapping:[PushUserDetails getPushUserDetailsObjectMapping]]];
    return tripNotificationObjectMapping;
}
@end
