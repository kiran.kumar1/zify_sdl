//
//  PushTripCompleteNotification.m
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushTripCompleteNotification.h"

#define CATEGORY_DRIVE_COMPLETED 9
#define CATEGORY_RIDE_COMPLETED 10

@implementation PushTripCompleteNotification

+(RKObjectMapping*)getPushTripCompleteNotificationObjectMapping{
    RKObjectMapping *tripCompleteNotificationObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [tripCompleteNotificationObjectMapping addAttributeMappingsFromArray:@[@"title",@"category"]];
    [tripCompleteNotificationObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"tripDetails" toKeyPath:@"tripDetails" withMapping:[PushTripDetails getPushTripDetailsObjectMapping]]];
    return tripCompleteNotificationObjectMapping;
}

-(BOOL)showNotification:(BOOL)isAppLaunch{
    int category = _category.intValue;
    if(category == CATEGORY_DRIVE_COMPLETED || (isAppLaunch && category == CATEGORY_RIDE_COMPLETED)) return false;
    return true;
}
@end
