//
//  PushOfferInfo.h
//  zify
//
//  Created by Anurag S Rathor on 10/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "PushNotificationInfo.h"

@interface PushOfferInfo : PushNotificationInfo
@property(nonatomic,strong) NSNumber *hasImage;
@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSString *imageUrl;
@property(nonatomic,strong) NSString *title;
+(RKObjectMapping*)getPushOfferInfoObjectMapping;
@end
