//
//  PushAppUpgradeInfo.h
//  zify
//
//  Created by Anurag S Rathor on 10/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "PushNotificationInfo.h"

@interface PushAppUpgradeInfo : PushNotificationInfo
@property(nonatomic,strong) NSNumber *hasImage;
@property(nonatomic,strong) NSString *imageUrl;
@property(nonatomic,strong) NSNumber *forcedUpgrade;
@property(nonatomic,strong) NSString *message;
+(RKObjectMapping*)getPushAppUpgradeInfoObjectMapping;
@end
