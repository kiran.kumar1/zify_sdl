//
//  PushOfferInfo.m
//  zify
//
//  Created by Anurag S Rathor on 10/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushOfferInfo.h"

@implementation PushOfferInfo
+(RKObjectMapping*)getPushOfferInfoObjectMapping{
    RKObjectMapping *offerObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [offerObjectMapping addAttributeMappingsFromDictionary:@{@"has_image":@"hasImage",
                                                             @"message":@"message",
                                                             @"image_url":@"imageUrl",
                                                             @"title":@"title"
                                                             }];
    return offerObjectMapping;
}
@end
