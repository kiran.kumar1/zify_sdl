//
//  PushTripDetails.m
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushTripDetails.h"

@implementation PushTripDetails
+(RKObjectMapping*)getPushTripDetailsObjectMapping{
    RKObjectMapping *tripDetailsObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [tripDetailsObjectMapping addAttributeMappingsFromArray:@[@"srcAddress",@"destAddress",@"departureTime",@"tripTime",@"driveId",
                                @"rideId",@"amount",@"currency",@"driverFirstName",
                                @"driverLastName",@"driverProfileImg"]];
    return tripDetailsObjectMapping;
}
@end
