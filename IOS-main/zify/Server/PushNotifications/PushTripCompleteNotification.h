//
//  PushTripCompleteNotification.h
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushNotificationInfo.h"
#import "RestKit/RestKit.h"
#import "PushTripDetails.h"
#import "PushNotificationInfo.h"

@interface PushTripCompleteNotification : PushNotificationInfo
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSNumber *category;
@property (nonatomic,strong) PushTripDetails *tripDetails;
+(RKObjectMapping*)getPushTripCompleteNotificationObjectMapping;
@end
