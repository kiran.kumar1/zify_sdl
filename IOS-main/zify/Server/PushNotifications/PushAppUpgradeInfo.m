//
//  PushAppUpgradeInfo.m
//  zify
//
//  Created by Anurag S Rathor on 10/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushAppUpgradeInfo.h"

@implementation PushAppUpgradeInfo
+(RKObjectMapping*)getPushAppUpgradeInfoObjectMapping{
    RKObjectMapping *appUpgradeObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [appUpgradeObjectMapping addAttributeMappingsFromDictionary:@{@"forced_upgrade":@"forcedUpgrade"
                                            ,@"message":@"message",
                                            @"has_image":@"hasImage",
                                            @"image_url":@"imageUrl"}];
    return appUpgradeObjectMapping;
}
@end
