//
//  PushTripNotification.h
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "PushTripDetails.h"
#import "PushUserDetails.h"
#import "PushNotificationInfo.h"

@interface PushTripNotification : PushNotificationInfo
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSNumber *category;
@property (nonatomic,strong) PushTripDetails *tripDetails;
@property (nonatomic,strong) PushUserDetails *userDetails;
+(RKObjectMapping*)getPushTripNotificationObjectMapping;
@end
