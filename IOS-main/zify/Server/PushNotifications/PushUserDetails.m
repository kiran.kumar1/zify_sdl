//
//  PushUserDetails.m
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import "PushUserDetails.h"

@implementation PushUserDetails
+(RKObjectMapping*)getPushUserDetailsObjectMapping{
    RKObjectMapping *userDetailsObjectMapping=[RKObjectMapping mappingForClass:[self class]];
    [userDetailsObjectMapping addAttributeMappingsFromArray:@[@"firstName",@"lastName",@"profileImageURL"]];
    return userDetailsObjectMapping;
}
@end
