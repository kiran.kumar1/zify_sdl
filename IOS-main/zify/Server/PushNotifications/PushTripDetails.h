//
//  PushTripDetails.h
//  zify
//
//  Created by Anurag S Rathor on 16/03/16.
//  Copyright © 2016 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface PushTripDetails : NSObject
@property (nonatomic,strong) NSString *srcAddress;
@property (nonatomic,strong) NSString *destAddress;
@property (nonatomic,strong) NSString *departureTime;
@property (nonatomic,strong) NSString *tripTime;
@property (nonatomic,strong) NSNumber *driveId;
@property (nonatomic,strong) NSNumber *rideId;
@property (nonatomic,strong) NSNumber *amount;
@property (nonatomic,strong) NSString *currency;
@property (nonatomic,strong) NSString *driverFirstName;
@property (nonatomic,strong) NSString *driverLastName;
@property (nonatomic,strong) NSString *driverProfileImg;
+(RKObjectMapping*)getPushTripDetailsObjectMapping;
@end
