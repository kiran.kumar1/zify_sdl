//
//  FBUserLoginRequest.h
//  zify
//
//  Created by Anurag S Rathor on 10/07/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequest.h"

@interface FBCheckUserRequest : ServerRequest
@property (nonatomic, strong) NSString * emailId;
@property (nonatomic, strong) NSString * fbId;
@property (nonatomic, strong) NSString * fbToken;

-(id)initWithEmail:(NSString *)email andFbId:(NSString *)fbId andFbToken:(NSString *)fbToken;
@end
