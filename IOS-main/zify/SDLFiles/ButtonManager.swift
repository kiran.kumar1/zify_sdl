//
//  ButtonManager.swift
//  SmartDeviceLink
//
//  Created by Nicole on 4/11/18.
//  Copyright © 2018 smartdevicelink. All rights reserved.
//

import Foundation
import SmartDeviceLink
import UIKit

typealias RefreshUIHandler = (() -> Void)

protocol StateOFDriveProtocal{
    func didStartTheDrive()
    func didStopTheDrive()
}

class ButtonManager: NSObject {
    

     let sdlManager: SDLManager!
    fileprivate var refreshUIHandler: RefreshUIHandler?
    
    var softTogglebutton: RefreshUIHandler?
    /// SDL UI textfields are visible if true; hidden if false
    public fileprivate(set) var textEnabled: Bool {
        didSet {
            guard let refreshUIHandler = refreshUIHandler else { return }
            refreshUIHandler()
        }
    }
    
    var stateoFDriveDelegate:StateOFDriveProtocal?

    /// SDL UI images are visible if true; hidden if false
    public fileprivate(set) var imagesEnabled: Bool {
        didSet {
            guard let refreshUIHandler = refreshUIHandler, let alertSoftButton = sdlManager.screenManager.softButtonObjectNamed(AlertSoftButton) else { return }
            alertSoftButton.transitionToNextState()
            refreshUIHandler()
        }
    }

    /// Keeps track of the toggle soft button current state. The image or text changes when the button is selected
    fileprivate var toggleEnabled: Bool {
        didSet {
            guard let hexagonSoftButton = sdlManager.screenManager.softButtonObjectNamed(ToggleSoftButton), hexagonSoftButton.transition(toState: toggleEnabled ? ToggleSoftButtonImageOnState : ToggleSoftButtonImageOffState) else { return }
        }
    }

    init(sdlManager: SDLManager, updateScreenHandler: RefreshUIHandler? = nil) {
        self.sdlManager = sdlManager
        self.refreshUIHandler = updateScreenHandler
        textEnabled = true
        imagesEnabled = true
        toggleEnabled = true

        super.init()
    }

    /// Creates and returns an array of all soft buttons for the UI
    ///
    /// - Parameter manager: The SDL Manager
    /// - Returns: An array of all soft buttons for the UI
    func allScreenSoftButtons(with manager: SDLManager, withDelegate:StateOFDriveProtocal) -> [SDLSoftButtonObject] {
        self.stateoFDriveDelegate = withDelegate
        return [softButtonAlert(with: manager), softButtonToggle(with: manager), softButtonTextVisible(with: manager), softButtonImagesVisible()]
    }
}

// MARK: - Custom Soft Buttons

private extension ButtonManager {
    /// Returns a soft button that shows an alert when tapped.
    ///
    /// - Parameter manager: The SDL Manager for showing the alert
    /// - Returns: A soft button
    func softButtonAlert(with manager: SDLManager) -> SDLSoftButtonObject {
        let imageSoftButtonState = SDLSoftButtonState(stateName: AlertSoftButtonImageState, text: VCOptionShareDrive, image: UIImage(named: AlertBWIconName)?.withRenderingMode(.alwaysTemplate))
        let textSoftButtonState = SDLSoftButtonState(stateName: AlertSoftButtonTextState, text: AlertSoftButtonText, image: UIImage(named: AlertBWIconName)?.withRenderingMode(.alwaysTemplate))
        return SDLSoftButtonObject(name: AlertSoftButton, states: [imageSoftButtonState, textSoftButtonState], initialStateName:imageSoftButtonState.name ) { (buttonPress, buttonEvent) in
            guard buttonPress != nil else { return }
            SDLConnection.sharedManager.shareDriveBtnPressed()
        //    SDLConnection.sharedManager.showLocationOnNavigation()
            //
          //  ProxyManager.sharedManager.showSomeStaticUsesDataListForRideRequest()
        }
    }

    /// Returns a soft button that toggles between two states: on and off. If images are currently visible, the button image toggles; if images aren't visible, the button text toggles.
    ///
    /// - Returns: A soft button
    func softButtonToggle(with manager: SDLManager) -> SDLSoftButtonObject {
        let imageOnState = SDLSoftButtonState(stateName: ToggleSoftButtonImageOnState, text: VCOptionStartDrive, image: UIImage(named: ToggleOnBWIconName)?.withRenderingMode(.alwaysTemplate))
        let imageOffState = SDLSoftButtonState(stateName: ToggleSoftButtonImageOffState, text: VCOptionStopDrive, image: UIImage(named: ToggleOffBWIconName)?.withRenderingMode(.alwaysTemplate))
        return SDLSoftButtonObject(name: ToggleSoftButton, states: [imageOnState,imageOffState], initialStateName: imageOnState.name) { [unowned self] (buttonPress, buttonEvent) in
            guard buttonPress != nil else { return }
            self.toggleEnabled = !self.toggleEnabled
        //    self.toggleEnabled = false
            var alertmsg = "Your drive is started successfully." //Please click on Add Passenger(s) button to add the Riders as way points"
            if(self.toggleEnabled){
                 alertmsg = "Your drive is completed successfully."
            }
            let alert = AlertManager.alertWithMessage(alertmsg)
            manager.send(request: alert) { (request, response, error) in
                print("response is \(response)")
            }
            return
           
          

            
    /*        let nextPick = SDLSoftButton(type: .text, text: "Add Passenger(s)", image: nil, highlighted: true, buttonId: 411, systemAction: .defaultAction, handler: { buttonPress, buttonEvent in
                guard buttonPress != nil else { return }
                print("click on button add next")
                if(self.toggleEnabled){
                    self.stateoFDriveDelegate?.didStopTheDrive()
                }else{
                    self.stateoFDriveDelegate?.didStartTheDrive()
                }
                
            })
 
            let alertAddNext = SDLAlert.init(ttsChunks: nil, alertText1: alertmsg, alertText2: nil, alertText3: nil, playTone: true, softButtons: [nextPick])
            
         //   SDLAlert(alertText: "Next pick", softButtons: [nextPick], playTone: true, ttsChunks: nil, alertIcon: nil, cancelID: 1023)
            
            manager.send(request: alertAddNext) { (request, response, error) in
                guard response?.success.boolValue == true else { return }
                print("AddNextUser")
            }*/
        }
    }

    /// Returns a soft button that toggles the textfield visibility state for the SDL UI. The button's text toggles based on the current text visibility.
    ///
    /// - Returns: A soft button
    func softButtonTextVisible(with manager: SDLManager) -> SDLSoftButtonObject {
        
      //  let imageSoftButtonState = SDLSoftButtonState(stateName: AlertSoftButtonImageState, text: "First Option", image: UIImage(named: AlertBWIconName)?.withRenderingMode(.alwaysTemplate))

        
        let textVisibleState = SDLSoftButtonState.init(stateName: TextVisibleSoftButtonTextOnState, text: VCOptionRideRequest, image:UIImage(named: "Ride_request_new_1")?.withRenderingMode(.alwaysTemplate))//SDLSoftButtonState(stateName: TextVisibleSoftButtonTextOnState, text: "Third Option", artwork: nil)
        let textNotVisibleState = SDLSoftButtonState(stateName: TextVisibleSoftButtonTextOffState, text: VCOptionRideRequest, image: UIImage(named: "Ride_request_new_1")?.withRenderingMode(.alwaysTemplate))
        return SDLSoftButtonObject(name: TextVisibleSoftButton, states: [textVisibleState, textNotVisibleState], initialStateName: textVisibleState.name) { [unowned self] (buttonPress, buttonEvent) in
            guard buttonPress != nil else { return }
           self.textEnabled = !self.textEnabled
            // Update the button state
            let softButton = self.sdlManager.screenManager.softButtonObjectNamed(TextVisibleSoftButton)
            softButton?.transitionToNextState()
            SDLConnection.sharedManager.showListOfRideRequestsFromUsers()
            
        }
    }

    
    
    /// Returns a soft button that toggles the image visibility state for the SDL UI. The button's text toggles based on the current image visibility.
    ///
    /// - Returns: A soft button
    func softButtonImagesVisible() -> SDLSoftButtonObject {
        let imagesVisibleState = SDLSoftButtonState(stateName: ImagesVisibleSoftButtonImageOnState, text: "Fourth option", image: nil)
        let imagesNotVisibleState = SDLSoftButtonState(stateName: ImagesVisibleSoftButtonImageOffState, text: ImagesVisibleSoftButtonImageOffText, image: nil)
        return SDLSoftButtonObject(name: ImagesVisibleSoftButton, states: [imagesVisibleState, imagesNotVisibleState], initialStateName: imagesVisibleState.name) { [unowned self] (buttonPress, buttonEvent) in
            guard buttonPress != nil else { return }
            self.imagesEnabled = !self.imagesEnabled
         //   let audio = AudioManager.init(sdlManager: self.sdlManager)
          //  audio.startRecording(withTitle: "Tilte", withSource: "SOurce", withDestination: "Destination")
            // Update the button state
          //  let softButton = self.sdlManager.screenManager.softButtonObjectNamed(ImagesVisibleSoftButton)
         //   softButton?.transitionToNextState()
        }
    }
    
    
    
   /* func showSaticmenu(){
        NSMutableSet *titleCheckSet = [NSMutableSet set];
        NSMutableSet<NSString *> *allMenuVoiceCommands = [NSMutableSet set];
        NSUInteger voiceCommandCount = 0;
        for (SDLMenuCell *cell in menuCells) {
            [titleCheckSet addObject:cell.title];
            if (cell.voiceCommands == nil) { continue; }
            [allMenuVoiceCommands addObjectsFromArray:cell.voiceCommands];
            voiceCommandCount += cell.voiceCommands.count;
        }
        
        // Check for duplicate titles
        if (titleCheckSet.count != menuCells.count) {
            SDLLogE(@"Not all cell titles are unique. The menu will not be set.");
            return;
        }
        
        // Check for duplicate voice recognition commands
        if (allMenuVoiceCommands.count != voiceCommandCount) {
            SDLLogE(@"Attempted to create a menu with duplicate voice commands. Voice commands must be unique. The menu will not be set.");
            return;
        }
        
        _oldMenuCells = _menuCells;
        _menuCells = menuCells;
        
        if ([self sdl_isDynamicMenuUpdateActive:self.dynamicMenuUpdatesMode]) {
            [self sdl_startDynamicMenuUpdate];
        } else {
            [self sdl_startNonDynamicMenuUpdate];
        }
    }
    */
    
}
