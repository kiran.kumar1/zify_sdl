//
//  AlertManager.swift
//  SmartDeviceLink-Example-Swift
//
//  Created by Nicole on 4/12/18.
//  Copyright © 2018 smartdevicelink. All rights reserved.
//

import Foundation
import SmartDeviceLink

protocol buttonTappedWithTitleDelegate {
    func sdlButtonTappedWithTitle(withTitle:String)
}


class AlertManager {
    
    static var delegate:buttonTappedWithTitleDelegate?
    private class var okSoftButton: SDLSoftButton {
        return SDLSoftButton(type: .text, text: "OK", image: nil, highlighted: true, buttonId: 1, systemAction: nil, handler: nil)
    }
    
    private class var acceptSoftButton: SDLSoftButton {
        return SDLSoftButton(type: .text, text: "Accept", image: nil, highlighted: true, buttonId: 2, systemAction: SDLSystemAction.defaultAction, handler: { buttonPress, buttonEvent in
            guard buttonPress != nil else { return }
            
              //acceptButtonTapped()
            self.delegate?.sdlButtonTappedWithTitle(withTitle: "Accept")
        })
    }
    
    private class var declineSoftButton: SDLSoftButton {
        return SDLSoftButton(type: .text, text: "Decline", image: nil, highlighted: true, buttonId: 3, systemAction: SDLSystemAction.defaultAction, handler: { buttonPress, buttonEvent in
            guard buttonPress != nil else { return }
            self.delegate?.sdlButtonTappedWithTitle(withTitle: "Decline")

             // declineButtonTapped()
        })
        
    }
    
    
    
    private class var startSoftButton: SDLSoftButton {
        return SDLSoftButton(type: .text, text: "Start", image: nil, highlighted: true, buttonId: 4, systemAction: SDLSystemAction.defaultAction, handler: { buttonPress, buttonEvent in
            guard buttonPress != nil else { return }
            // startButtonTapped()
            self.delegate?.sdlButtonTappedWithTitle(withTitle: "Start")

        })
    }
    
    private class var publishSoftButton: SDLSoftButton {
        return SDLSoftButton(type: .text, text: "Publish", image: nil, highlighted: true, buttonId: 6, systemAction: SDLSystemAction.defaultAction, handler: { buttonPress, buttonEvent in
            guard buttonPress != nil else { return }
            // startButtonTapped()
            self.delegate?.sdlButtonTappedWithTitle(withTitle: "Publish")
            
        })
    }
    
    private class var cancelSoftButton: SDLSoftButton {
        return SDLSoftButton(type: .text, text: "Cancel", image: nil, highlighted: true, buttonId: 5, systemAction: SDLSystemAction.defaultAction, handler: { buttonPress, buttonEvent in
            guard buttonPress != nil else { return }
            //cancelButtonTapped()
            self.delegate?.sdlButtonTappedWithTitle(withTitle: "Cancel")
        })
        
    }
    
    
   
    


    /// Creates an alert with one or two lines of text.
    ///
    /// - Parameters:
    ///   - textField1: The first line of a message to display in the alert
    ///   - textField2: The second line of a message to display in the alert
    /// - Returns: An SDLAlert object
    class func alertWithMessage(_ textField1: String, textField2: String? = nil) -> SDLAlert {
        return SDLAlert(alertText1: textField1, alertText2: nil, alertText3: nil)
    }

    /// Creates an alert with up to two lines of text and a close button that will dismiss the alert when tapped
    ///
    /// - Parameters:
    ///   - textField1: The first line of a message to display in the alert
    ///   - textField2: The second line of a message to display in the alert
    /// - Returns: An SDLAlert object
    class func alertWithMessageAndCloseButton(_ textField1: String, textField2: String? = nil) -> SDLAlert {
        return SDLAlert(alertText1: textField1, alertText2: textField2, alertText3: nil, duration: 5000, softButtons: [AlertManager.okSoftButton])
    }
    
    class func alertForRequestWithAcceptAndDeclineButtons(_ textField1: String, textField2: String? = nil,textField3: String? = nil,withDelegate:Any) -> SDLAlert {
        self.delegate = withDelegate as? SDLConnection
        AppDelegate.getInstance().alertObj =  SDLAlert(alertText1: textField1, alertText2: textField2, alertText3: textField3, duration: 5000, softButtons: [AlertManager.acceptSoftButton, AlertManager.declineSoftButton])
        return AppDelegate.getInstance().alertObj
    }
    
    class func alertForStartDrive(_ textField1: String, textField2: String? = nil,textField3: String? = nil,withDelegate:Any) -> SDLAlert {
        self.delegate = withDelegate as! SDLConnection
        return  SDLAlert(alertText1: textField1, alertText2: textField2, alertText3: textField3, duration: 5000, softButtons: [AlertManager.startSoftButton, AlertManager.cancelSoftButton])
    }
    
    class func alertForPublishDrive(_ textField1: String, textField2: String? = nil,textField3: String? = nil,withDelegate:Any) -> SDLAlert {
        self.delegate = withDelegate as! SDLConnection
        return  SDLAlert(alertText1: textField1, alertText2: textField2, alertText3: textField3, duration: 5000, softButtons: [AlertManager.publishSoftButton, AlertManager.cancelSoftButton])
    }
}
