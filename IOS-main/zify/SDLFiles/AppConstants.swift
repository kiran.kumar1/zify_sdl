//
//  AppConstants.swift
//  zify
//
//  Created by Anurag Rathor on 14/10/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit
//#pragma martk - SDL Voice Commands

//#pragma mark - SDL Configuration
let ExampleAppName = "Zify"
let ExampleAppNameShort = "Zify"
let ExampleAppNameTTS = "Zify"
let ExampleFullAppId = "76a8f634-0332-4a5b-8e36-1e8e03dc6c06" // Dummy App Id

//#pragma mark - SDL Textfields
let SmartDeviceLinkText = "SmartDeviceLink (SDL)"
let ExampleAppText = "Example App"

//#pragma mark - SDL Soft Buttons
let ToggleSoftButton = "ToggleSoftButton"
let ToggleSoftButtonImageOnState = "ToggleSoftButtonImageOnState"
let ToggleSoftButtonImageOffState = "ToggleSoftButtonImageOffState"
let ToggleSoftButtonTextOnState = "ToggleSoftButtonTextOnState"
let ToggleSoftButtonTextOffState = "ToggleSoftButtonTextOffState"
let ToggleSoftButtonTextTextOnText = "➖"
let ToggleSoftButtonTextTextOffText = "➕"

let AlertSoftButton = "AlertSoftButton"
let AlertSoftButtonImageState = "AlertSoftButtonImageState"
let AlertSoftButtonTextState = "AlertSoftButtonTextState"
let AlertSoftButtonText = "Tap Me"

let TextVisibleSoftButton = "TextVisibleSoftButton"
let TextVisibleSoftButtonTextOnState = "TextVisibleSoftButtonTextOnState"
let TextVisibleSoftButtonTextOffState = "TextVisibleSoftButtonTextOffState"
let TextVisibleSoftButtonTextOnText = "➖Text"
let TextVisibleSoftButtonTextOffText = "➕Text"

let ImagesVisibleSoftButton = "ImagesVisibleSoftButton"
let ImagesVisibleSoftButtonImageOnState = "ImagesVisibleSoftButtonImageOnState"
let ImagesVisibleSoftButtonImageOffState = "ImagesVisibleSoftButtonImageOffState"
let ImagesVisibleSoftButtonImageOnText = "➖Icons"
let ImagesVisibleSoftButtonImageOffText = "➕Icons"

//#pragma mart - SDL Text-To-Speech
let TTSGoodJob = "Good Job"
let TTSYouMissed = "You Missed"

//#pragma martk - SDL Voice Commands
let VCStart = "Start"
let VCStop = "Stop"


let VCOptionShareDrive = "Share Drive"
let VCOptionStartDrive = "Start Drive"
let VCOptionStopDrive = "Stop Drive"
let VCOptionRideRequest = "Ride Requests"








//#pragma mark - SDL Perform Interaction Choice Set Menu
let PICSInitialText = "Perform Interaction Choice Set Menu Example"
let PICSInitialPrompt = "Select an item from the menu"
let PICSHelpPrompt = "Select a menu row using your voice or by tapping on the screen"
let PICSTimeoutPrompt = "Closing the menu"
let PICSFirstChoice = "First Choice"
let PICSSecondChoice = "Second Choice"
let PICSThirdChoice = "Third Choice"

//#pragma mark - SDL Add Command Menu
let ACSpeakAppNameMenuName = "Speak App Name"
let ACShowChoiceSetMenuName = "Show Perform Interaction Choice Set"
let ACGetVehicleDataMenuName = "Get Vehicle Speed"
let ACGetAllVehicleDataMenuName = "Get All Vehicle Data"
let ACRecordInCarMicrophoneAudioMenuName = "Record In-Car Microphone Audio"
let ACDialPhoneNumberMenuName = "Dial Phone Number"
let ACSubmenuMenuName = "Submenu"
let ACSubmenuItemMenuName = "Item"
let ACSubmenuTemplateMenuName = "Change Template"

let ACAccelerationPedalPositionMenuName = "Acceleration Pedal Position"
let ACAirbagStatusMenuName = "Airbag Status"
let ACBeltStatusMenuName = "Belt Status"
let ACBodyInformationMenuName = "Body Information"
let ACClusterModeStatusMenuName = "Cluster Mode Status"
let ACDeviceStatusMenuName = "Device Status"
let ACDriverBrakingMenuName = "Driver Braking"
let ACECallInfoMenuName = "eCall Info"
let ACElectronicParkBrakeStatus = "Electronic Parking Brake Status"
let ACEmergencyEventMenuName = "Emergency Event"
let ACEngineOilLifeMenuName = "Engine Oil Life"
let ACEngineTorqueMenuName = "Engine Torque"
let ACExternalTemperatureMenuName = "External Temperature"
let ACFuelLevelMenuName = "Fuel Level"
let ACFuelLevelStateMenuName = "Fuel Level State"
let ACFuelRangeMenuName = "Fuel Range"
let ACGPSMenuName = "GPS"
let ACHeadLampStatusMenuName = "Head Lamp Status"
let ACInstantFuelConsumptionMenuName = "Instant Fuel Consumption"
let ACMyKeyMenuName = "MyKey"
let ACOdometerMenuName = "Odometer"
let ACPRNDLMenuName = "PRNDL"
let ACRPMMenuName = "RPM"
let ACSpeedMenuName = "Speed"
let ACSteeringWheelAngleMenuName = "Steering Wheel Angle"
let ACTirePressureMenuName = "Tire Pressure"
let ACTurnSignalMenuName = "Turn Signal"
let ACVINMenuName = "VIN"
let ACWiperStatusMenuName = "Wiper Status"

//#pragma mark - SDL Image Names
let AlertBWIconName = "alert"
let CarBWIconImageName = "car"
let ExampleAppLogoName = "1024"
let MenuBWIconImageName = "choice_set"
let MicrophoneBWIconImageName = "microphone"
let PhoneBWIconImageName = "phone"
let SpeakBWIconImageName = "speak"
let ToggleOffBWIconName = "toggle_off"
let ToggleOnBWIconName = "toggle_on"

//#pragma mark - SDL App Name in Different Languages
let ExampleAppNameSpanish = "SDL Aplicación de ejemplo"
let ExampleAppNameFrench = "SDL Exemple App"

//#pragma mark - SDL Vehicle Data
let VehicleDataOdometerName = "Odometer"
let VehicleDataSpeedName = "Speed"


class AppConstants: NSObject {

}
