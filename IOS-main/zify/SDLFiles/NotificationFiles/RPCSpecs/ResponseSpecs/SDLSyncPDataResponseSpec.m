//
//  SDLSyncPDataResponseSpec.m
//  SmartDeviceLink


#import <Foundation/Foundation.h>

#import <Quick/Quick.h>
#import <Nimble/Nimble.h>
#import <Nimble/Nimble-Swift.h>

#import "SDLSyncPDataResponse.h"
#import "SDLRPCParameterNames.h"

QuickSpecBegin(SDLSyncPDataResponseSpec)

QuickSpecEnd
