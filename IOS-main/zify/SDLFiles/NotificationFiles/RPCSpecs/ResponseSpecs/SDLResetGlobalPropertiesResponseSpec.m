//
//  SDLResetGlobalPropertiesResponseSpec.m
//  SmartDeviceLink


#import <Foundation/Foundation.h>

#import <Quick/Quick.h>
#import <Nimble/Nimble.h>
#import <Nimble/Nimble-Swift.h>

#import "SDLResetGlobalPropertiesResponse.h"
#import "SDLRPCParameterNames.h"

QuickSpecBegin(SDLResetGlobalPropertiesResponseSpec)

QuickSpecEnd
