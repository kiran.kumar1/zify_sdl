//
//  SDLEncodedSyncPDataResponseSpec.m
//  SmartDeviceLink


#import <Foundation/Foundation.h>

#import <Quick/Quick.h>
#import <Nimble/Nimble.h>
#import <Nimble/Nimble-Swift.h>

#import "SDLEncodedSyncPDataResponse.h"
#import "SDLRPCParameterNames.h"

QuickSpecBegin(SDLEncodedSyncPDataResponseSpec)

QuickSpecEnd
