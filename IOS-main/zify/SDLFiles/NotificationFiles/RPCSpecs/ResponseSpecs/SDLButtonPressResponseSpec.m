//
//  SDLButtonPressResponseSpec.m
//  SmartDeviceLink-iOS
//

#import <Foundation/Foundation.h>

#import <Quick/Quick.h>
#import <Nimble/Nimble.h>
#import <Nimble/Nimble-Swift.h>

#import "SDLButtonPressResponse.h"
#import "SDLRPCParameterNames.h"

QuickSpecBegin(SDLButtonPressResponseSpec)

QuickSpecEnd
