//
//  SDLConnection.swift
//  zify
//
//  Created by Anurag Rathor on 14/10/19.
//  Copyright © 2019 zify. All rights reserved.
//

import UIKit
import SmartDeviceLink
import GooglePlaces;






class SDLConnection: NSObject, ProxyManagerDelegate, buttonTappedWithTitleDelegate, StateOFDriveProtocal{
    func didStartTheDrive() {
      //  DispatchQueue.main.async {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            self.showRidersListBasedOnPickUpLocations()
        })
            //   }
    }
    
    func didStopTheDrive() {
        
    }
    
    
    
    var performInteractionManager: PerformInteractionManager!
    var oldinteractionSetNumber:UInt32 = 0
    var isAtStartTrip:Bool = true
    
    func sdlButtonTappedWithTitle(withTitle: String) {
        print("selected title is \(withTitle)")
        if(withTitle == "Accept"){
            self.acceptButtonTapped()
        }else if(withTitle == "Decline"){
            self.declineButtonTapped()
        }else if(withTitle == "Start"){
            self.startButtonTapped()
        }else if(withTitle == "Cancel"){
            self.cancelButtonTapped()
        }
    }
    
    
     func acceptButtonTapped(){
        print("Accept button pressed")
        
        if (AppDelegate.getInstance().notificationViewFromAppdelegate != nil){
            AppDelegate.getInstance().alertObj = nil
            AppDelegate.getInstance().notificationViewFromAppdelegate.acceptRideRequest(nil)
        }
    }
    
     func declineButtonTapped(){
        print("Decline button pressed")
        if (AppDelegate.getInstance().notificationViewFromAppdelegate != nil){
            AppDelegate.getInstance().alertObj = nil
            AppDelegate.getInstance().notificationViewFromAppdelegate.declineRideRequest(nil)
        }
    }
    
    
     func startButtonTapped(){
        
        DispatchQueue.main.async {
        print("Start button pressed")
        let visibleController = AppUtilites.applicationVisibleViewController()
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        print("visible is \(visibleController) and rootViewController ids \(rootViewController)")
        if (visibleController is CurrentDriveController){
            let obj = visibleController as! CurrentDriveController
            obj.startDrive(fromSDL: self.tripInfo)
        }
        }
    }
    
     func cancelButtonTapped(){
        print("Cancel button pressed")
        
    }
    
    func generateRandomNumber(numDigits:Int) -> UInt32{
        var place = 1
        var finalNumber = 0;
        for var i:Int in 0..<numDigits{
            place *= 10
            var randomNumber = arc4random_uniform(10)
            finalNumber += Int(randomNumber) * place

        }
     
        return UInt32(finalNumber)
    }
    var proxyState = ProxyState.stopped
    static let sharedManager = SDLConnection()
    var tripDepartureTime:String?
    var needTOShpowAlert:Bool = true
    var isConnectedToSDL:Bool = false
    var tripInfo:TripDrive?
    var isSubsribedToWayPoints:Bool = false
    
    
  
    // MARK: - IBActions
     func connectToSDL() {
            ProxyManager.sharedManager.delegate = self
            ProxyManager.sharedManager.stateDelegate = self
            switch proxyState {
            case .stopped:
                ProxyManager.sharedManager.start(with: .iap)
            case .searching:
                ProxyManager.sharedManager.stopConnection()
            case .connected:
                ProxyManager.sharedManager.stopConnection()
            }
    }
    
    // MARK: - Delegate Functions
    func didChangeProxyState(_ newState: ProxyState) {
        proxyState = newState
       // var newColor: UIColor? = nil
        var newTitle: String? = nil
        
        switch newState {
        case .stopped:
            newTitle = "Connect"
            break;
        case .searching:
            newTitle = "Stop Searching"
            break;
        case .connected:
            self.isConnectedToSDL = true
            self.alertForStartTRip()
            newTitle = "Disconnect"
            break
        }
        print("newTitle isss \(newTitle)")
        
       
    }
    
    
    func showAlertForTheRideRequest(WithTripInfo:NSDictionary) {
        if let manager = ProxyManager.sharedManager.sdlManager {
            let payloadDict = WithTripInfo["payload"] as? [String:Any]
            let tripDetails = payloadDict?["tripDetails"] as? [String:Any]
            let srcAdd = "Source: " + (tripDetails?["srcAddress"]  as? String ?? "")
            let destAdd =  "Destination: " + (tripDetails?["destAddress"] as? String ?? "")
            let audioManager = AudioManager.init(sdlManager: manager, fromScreen: self)
            audioManager.startRecording(withTitle: "Hello...Say Accept to accept or decline to reject the request", withSource: srcAdd, withDestination: destAdd, isForRideRequestConfirmation: true)
            
        }
        
         /*   let alert = AlertManager.alertForRequestWithAcceptAndDeclineButtons("Hello.", textField2: srcAdd, textField3: destAdd, withDelegate: self)
            manager.send(request: alert) { (request, response, error) in
                print("response is \(response)")
                self.voicRecordingActionStart()
            }*/
            
            
        }
       // self.voicRecordingActionStart()
    
    
   /* func voicRecordingActionStart(){
        if let manager = ProxyManager.sharedManager.sdlManager {
            if #available(iOS 10.0, *) {
                let audioManager = AudioManager.init(sdlManager: manager)
                audioManager.startRecording(withTitle: <#T##String#>, withSource: <#T##String#>, withDestination: <#T##String#>)            }
        }
    }*/
    
    func checkIfTripAvailableInbufferTime() -> Bool{
        
        let tripDate = self.getDateFromString(dateStr: self.tripDepartureTime!)
        let nowDate = Date.init()
        print("nowDate is \(nowDate)")
        let minutesDiffer = nowDate.minutes(from: tripDate)
        print("minutesDiffer is \(minutesDiffer)")
        if (minutesDiffer >= 0){
            if(minutesDiffer <= 15){
                return true
            }
        }else{
            if(minutesDiffer >=  -15){
                return true
            }
        }
       return false
        
    }
    
    
    func getDateFromString(dateStr:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm:ss a"
        let locale = Locale.init(identifier: "en_US_POSIX")
        dateFormatter.locale = locale
        let date = dateFormatter.date(from: dateStr)
        return date ?? Date.init()
    }
    
    
    func alertForStartTRip(){
        
        if(!isConnectedToSDL){
            print("not connected to SDL sttilllll.....")
            return
        }
        if(self.tripDepartureTime == nil){
            print("no trip is available......")
            return
        }
        needTOShpowAlert = self.checkIfTripAvailableInbufferTime()
        if (needTOShpowAlert) {
            let status = self.tripInfo?.status ?? ""
            if (status == "PENDING"){
                let srcAdd = "Source: " + (self.tripInfo?.srcAddress ?? "")
                let destAdd = "Destination: " + (self.tripInfo?.destAddress ?? "")
                self.displayStartTripAlert(withSrcAddress: srcAdd, withDestinationAddress: destAdd)
            }
        }
    }
    
    
    func showAlertForStartTrip(withTripInfo:NSDictionary){
        self.tripInfo = TripDrive.init(driveDict: withTripInfo as? [AnyHashable : Any])
        let srcAdd = "Source: " + (withTripInfo["sourceAddress"] as? String ?? "")
        let destAdd = "Destination: " + (withTripInfo["destAddress"] as? String ?? "")
        self.displayStartTripAlert(withSrcAddress: srcAdd, withDestinationAddress: destAdd)
    }
    
    func displayStartTripAlert(withSrcAddress:String, withDestinationAddress:String){
        if let manager = ProxyManager.sharedManager.sdlManager {
            let alert = AlertManager.alertForStartDrive("Start Drive", textField2: withSrcAddress, textField3: withDestinationAddress, withDelegate: self)
            manager.send(request: alert) { (request, response, error) in
                print("response is \(response)")
            }
        }
    }
    
    
    func showLocationOnNavigation(){
        guard hasPermissionToSendLocation(with: ProxyManager.sharedManager.sdlManager) else { return }
        self.setTheLocationOnNavigation()
    }
    
    func setTheLocationOnNavigation(){
        if let manager = ProxyManager.sharedManager.sdlManager {
        let sendLocation = SDLSendLocation(longitude: 10.556387, latitude: 51.144742, locationName: "Neuehrenfeld", locationDescription: "Germany", address: ["Choosen from germany"], phoneNumber: nil, image: nil)
        
        
        manager.send(request: sendLocation) { (request, response, error) in
            guard let response = response as? SDLSendLocationResponse, error == nil else {
                print("Encountered error sending SendLocation")
                return
            }
            
            guard response.resultCode == .success else {
                switch response.resultCode {
                case .invalidData:
                    print("SendLocation was rejected. The request contained invalid data.")
                case .disallowed:
                    print("Your app is not allowed to use SendLocation")
                default:
                    print("Some unknown error has occured")
                }
                return
            }
            
            print("SendLocation successfully sent")
        }
        }
        
    }
    
    func checkHeadUnitSupportsGetWayPoints(completion:@escaping (Bool) -> ()){
        if let manager = ProxyManager.sharedManager.sdlManager {
            
            manager.systemCapabilityManager.updateCapabilityType(.navigation) { (error, systemCapabilityManager) in
                var isNavigationSupported:Bool = false
                if error == nil {
                    isNavigationSupported = systemCapabilityManager.navigationCapability?.getWayPointsEnabled?.boolValue ?? false;
                } else {
                    isNavigationSupported = systemCapabilityManager.hmiCapabilities?.navigation?.boolValue ?? false
                }
                completion(isNavigationSupported)
              //  <#If navigation is supported, send the `GetWayPoints` RPC#>
            }
        }
    }
    
    func getWayPints(){
        checkHeadUnitSupportsGetWayPoints { (isSupported) in
            if (isSupported){
                print("supported Navigation getway ppoints")
                self.subscribeToWayPoints()
            }else{
                print("NOT HEHHEHE supported Navigation getway ppoints")

            }
        }
    }
    
    func subscribeToWayPoints(){

        let waypoint = SDLGetWayPoints.init(type: .all)
        
        if let manager = ProxyManager.sharedManager.sdlManager {
           // manager.subscribe(to: .SDLDidReceiveWaypoint, observer: self, selector: #selector(waypointsDidUpdate(_:)))

            let subscribeWaypoints = SDLSubscribeWayPoints()
            
            manager.send(request: subscribeWaypoints) { (request, response, error) in
                guard error == nil, let response = response, response.success == true else {
                    print("erroorrorr is Handle the errors \(error.debugDescription)")
                    return
                }
                print(" You are now subscribed!....")
            }
        }
    }
    
    // Create this method to receive the subscription callback
    func waypointsDidUpdate(_ notification: SDLRPCNotificationNotification) {
        guard let waypointUpdate = notification.notification as? SDLOnWayPointChange else { return }
        
        let waypoints:[SDLLocationDetails] = waypointUpdate.waypoints
        if( waypoints.count != 0){
            print(" Use the waypoint data \(waypoints)")

        }else{
            print(" Dont....Use the waypoint data")
        }
    }
    func unsubscribeToWayPoints(){
        if let manager = ProxyManager.sharedManager.sdlManager {

        // Whenever you want to unsubscribe
        let unsubscribeWaypoints = SDLUnsubscribeWayPoints()
        manager.send(request: unsubscribeWaypoints) { (request, response, error) in
            guard error == nil, let response = response, response.success == true else {
                // Handle the errors
                return
            }
            
            // You are now subscribed!
        }
        }
    }
    
    func showListOfRideRequestsFromUsers() {
        

        if let manager = ProxyManager.sharedManager.sdlManager {
            let deleteRequest = SDLDeleteInteractionChoiceSet(id: 3333)
            manager.send(request:deleteRequest) { (request, response, error) in
                if response?.resultCode == .success {
                }else{
                }
            }
            
            self.performInteractionManager = PerformInteractionManager(sdlManager: manager)
            
            var choices = [SDLChoice]()
            // for obj in results{
            
            for index in 0..<8 {
                let choiceTitle = "Ride Request from Rider \(index + 1)"
                let choice = SDLChoice(id: 1000 + index  , menuName: choiceTitle, vrCommands: nil)
                choices.append(choice)
            }
            
            let createRequest = SDLCreateInteractionChoiceSet(id: 3333
                , choiceSet: choices)
            manager.send(request: createRequest) { (request, response, error) in
                if response?.resultCode == .success {
                    // The request was successful, now send the
                    // SDLPerformInteraction RPC
                    
                    let performInteraction = SDLPerformInteraction(initialPrompt:
                        "List of Ride Requests", initialText: "List of Ride Requests", interactionChoiceSetID: 3333)
                    performInteraction.interactionMode = .manualOnly
                    performInteraction.interactionLayout = .listOnly
                    performInteraction.timeout = 60000
                    
                    manager.send(request: performInteraction) { (request, response, error) in
                        guard let performInteractionResponse = response as?
                            SDLPerformInteractionResponse else {
                                return;
                        }
                        // Wait for user's selection or for timeout
                        if performInteractionResponse.resultCode == .timedOut {
                            // The custom menu timed out before the user could select an item
                        } else if performInteractionResponse.resultCode == .success {
                            let choiceId = performInteractionResponse.choiceID as? Int ?? 1000
                            // The user selected an item in the custom menu
                            print("choice id is \(choiceId)")
                            let choosenUserId = choiceId - 1000
                            self.showAlertForRideRequest(withIndex: choosenUserId)
                            
                        }
                    }
                }
            }
        }
    }
    
    func addUsersToTheArrayBasedOnPickupLocations() -> Array<[String:Any]>{
    
        var usersList = [[String: Any]]()
        
        let user0Dict = ["username":"Your Destination, you need to select this at first so that Route will be calculated directly", "profileImageName":"", "latitude":48.941530, "longitude":9.127368, "addressName":"HBG Kompressoren GmbH"] as [String : Any]
        
        let user1Dict = ["username":"Kiran", "profileImageName":"", "latitude":51.038730, "longitude":6.946020, "addressName":"Camping Service Bernd Bast, Kasselberger Weg 101, 50769 Köln, Germany"] as [String : Any]
        let user2Dict = ["username":"Rajesh", "profileImageName":"", "latitude":49.752690, "longitude":7.646280, "addressName":"Kasselberger Hühnerhof"] as [String : Any]
        let user3Dict = ["username":"Giri", "profileImageName":"", "latitude":28.012440, "longitude":-82.425160, "addressName":"Gerd Fuchs und Sohn GbR"] as [String : Any]
        let user4Dict = ["username":"Pavan", "profileImageName":"", "latitude":51.908260, "longitude":11.771390, "addressName":"Katholische Kirche St. Amandus"] as [String : Any]

        if(isAtStartTrip){
        usersList.append(user0Dict)
            isAtStartTrip = false
        }else{
        usersList.append(user1Dict)
        usersList.append(user2Dict)
        usersList.append(user3Dict)
        usersList.append(user4Dict)
        }
        
        return usersList

    }
    
    
    func showRidersListBasedOnPickUpLocations() {
        


        if let manager = ProxyManager.sharedManager.sdlManager {
            let deleteRequest = SDLDeleteInteractionChoiceSet(id: oldinteractionSetNumber)
            manager.send(request:deleteRequest) { (request, response, error) in
               // print("response info is \(response?.info)")
            }
            
            oldinteractionSetNumber = self.generateRandomNumber(numDigits: 3)
            self.performInteractionManager = PerformInteractionManager(sdlManager: manager)
            let usersArray = self.addUsersToTheArrayBasedOnPickupLocations()
            var choices = [SDLChoice]()
            
            for index in 0..<usersArray.count {
                let userObj = usersArray[index]
                let choiceTitle = userObj["username"] ?? ""
                let choice = SDLChoice(id: 2000 + index  , menuName: choiceTitle as! String, vrCommands: nil)
                choice.image = SDLImage.init(staticIconName: .addWaypoint)
                choices.append(choice)
            }
            
            let createRequest = SDLCreateInteractionChoiceSet(id: oldinteractionSetNumber
                , choiceSet: choices)
            manager.send(request: createRequest) { (request, response, error) in
                if response?.resultCode == .success {
                    let performInteraction = SDLPerformInteraction(initialPrompt:
                        "Rider(s) Order based on PIckup Location", initialText: "Rider(s) Order based on PIckup Location", interactionChoiceSetID: UInt16(self.oldinteractionSetNumber))
                    performInteraction.interactionMode = .manualOnly
                    performInteraction.interactionLayout = .listOnly
                    performInteraction.timeout = 60000
                    
                    manager.send(request: performInteraction) { (request, response, error) in
                        guard let performInteractionResponse = response as?
                            SDLPerformInteractionResponse else {
                                return;
                        }
                        // Wait for user's selection or for timeout
                        if performInteractionResponse.resultCode == .timedOut {
                            // The custom menu timed out before the user could select an item
                        } else if performInteractionResponse.resultCode == .success {
                            let choiceId = performInteractionResponse.choiceID as? Int ?? 2000
                            // The user selected an item in the custom menu
                            print("choice id is \(choiceId)")
                            let choosenUserId = choiceId - 2000
                            let selecteduserObj = usersArray[choosenUserId]
                            
                           /* let deleteMenuItem = SDLDeleteCommand(id: UInt32(choiceId))
                            manager.send(request: deleteMenuItem) { (request, response, error) in
                                if response?.resultCode == .success {
                                    // The menu item was successfully deleted
                                }
                            }*/
                            self.moveToSendLocationBasedOnUserSelection(withUserObj: selecteduserObj)
                        }
                    }
                }
            }
        }
    }
    
    func moveToSendLocationBasedOnUserSelection(withUserObj:[String:Any]){
        
        if(!isSubsribedToWayPoints){
            isSubsribedToWayPoints = true
            ProxyManager.sharedManager.subscribeSL()
        }
        let latitude = withUserObj["latitude"]  as! Double
        let longitude = withUserObj["longitude"] as! Double
        let addressname = withUserObj["addressName"]  as! String

        ProxyManager.sharedManager.sendLocation(lat: latitude, long: longitude, name: addressname)
    }
    
    
    
    func showAlertForRideRequest(withIndex:Int){
        if let manager = ProxyManager.sharedManager.sdlManager {
            
            let alert = AlertManager.alertForRequestWithAcceptAndDeclineButtons("Hello Kiran. You have a ride request from Rider \(withIndex+1)", textField2: "Pickup :Kondapur, Hyderabad, Telangana", textField3: "Drop At :Saidabad, Hyderabad, Telangana", withDelegate: self)
            manager.send(request: alert) { (request, response, error) in
                
            }
        }
        // print("response is \(response)")
    }
    func showAlertForStartOrStopDrive(isForStart:Bool){
        
        
        if let manager = ProxyManager.sharedManager.sdlManager {
            var alertmsg = "Your drive is started successfully."
            if(!isForStart){
                alertmsg = "Your drive is completed successfully."
            }
            let alert = AlertManager.alertWithMessage(alertmsg)
            manager.send(request: alert) { (request, response, error) in
             }
        }
    }
    
    func hasPermissionToSendLocation(with manager: SDLManager) -> Bool {
        SDLLog.d("Checking if app has permission to Send Location...")
        
        guard manager.permissionManager.isRPCAllowed("SendLocation") else {
            let alert = AlertManager.alertWithMessageAndCloseButton("This app does not have the required permissions to SendLocation")
            manager.send(request: alert)
            return false
        }
        
        return true
    }
    
    
    
    func shareDriveBtnPressed(){
        
       // self.showLocationOnNavigation()
      //  ProxyManager.sharedManager.sendLocation(lat: 51.001703, long: 6.957198, name: "Ford-Werke GmbH, MR-Gebaude")
      
        if let manager = ProxyManager.sharedManager.sdlManager {
            let audioManager = AudioManager.init(sdlManager: manager, fromScreen: self)
            audioManager.startRecording(withTitle: "Say any 2 word addresses e.g. Dlf gate", withSource: "Say any 2 word addresses e.g. Dlf gate", withDestination: "Listen for 10 sec", isForRideRequestConfirmation: false)
           // audioManager.startRecording(withTitle: "Hello...Say Accept to accept or decline to reject the request", withSource: srcAdd, withDestination: destAdd)
        }
    }
    
    func getLocationsFromVoiceSearch(voiceDataForDestination:String){
        
        let voiceDataForDestination1 = voiceDataForDestination
       // voiceDataForDestination1 = "psr"
        if(voiceDataForDestination1.count > 2){
            let mapLocationHelper = MapLocationHelper.init()
            mapLocationHelper.mapLocationDelegate = self
            mapLocationHelper.getAutoSuggestResults(forQuery: voiceDataForDestination1)
        }
    }
    
    
    func publishDriveWithDestination(withAddress:GMSAutocompletePrediction){
        if let manager = ProxyManager.sharedManager.sdlManager {
            let alert = AlertManager.alertForPublishDrive("Publish Drive", textField2: "Destination: \(withAddress.attributedPrimaryText.string)", textField3: "Do you want to Publish Drive?", withDelegate: self)
            manager.send(request: alert) { (request, response, error) in
               // print("response is \(response)")
            }
        }
    }
}

extension Date {
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
}

extension SDLConnection:MapLocationHelperDelegate{
    
    func queryResults(forAutosuggest searchResults: [GMSAutocompletePrediction]!) {
        print("resuts are\(searchResults)")
        if let manager = ProxyManager.sharedManager.sdlManager {
            let deleteRequest = SDLDeleteInteractionChoiceSet(id: 2222)
            manager.send(request:deleteRequest) { (request, response, error) in
                if response?.resultCode == .success {
                    print("successfully deleted from source")
                }else{
                    print("NOTsuccessfully deleted from source")
                }
            }
            
            self.performInteractionManager = PerformInteractionManager(sdlManager: manager)
            
            var choices = [SDLChoice]()
            // for obj in results{
            
            for index in 0..<searchResults.count {
                let placeObj = searchResults[index]
                let choice = SDLChoice(id: 100 + index  , menuName: placeObj.attributedPrimaryText.string, vrCommands: nil)
                choices.append(choice)
            }
            
            let createRequest = SDLCreateInteractionChoiceSet(id: 2222
                , choiceSet: choices)
            manager.send(request: createRequest) { (request, response, error) in
                if response?.resultCode == .success {
                    // The request was successful, now send the
                    // SDLPerformInteraction RPC
                    
                    let performInteraction = SDLPerformInteraction(initialPrompt:
                        "Choose any one of the Addresses", initialText: "List of Addresses", interactionChoiceSetID: 2222)
                    performInteraction.interactionMode = .manualOnly
                    performInteraction.interactionLayout = .listOnly
                    performInteraction.timeout = 60000
                    
                    manager.send(request: performInteraction) { (request, response, error) in
                        guard let performInteractionResponse = response as?
                            SDLPerformInteractionResponse else {
                                return;
                        }
                        // Wait for user's selection or for timeout
                        if performInteractionResponse.resultCode == .timedOut {
                            // The custom menu timed out before the user could select an item
                        } else if performInteractionResponse.resultCode == .success {
                            let choiceId = performInteractionResponse.choiceID as? Int ?? 100
                            // The user selected an item in the custom menu
                            print("choice id is \(choiceId)")
                            let destinationAddress = searchResults?[choiceId - 100]
                            self.publishDriveWithDestination(withAddress:destinationAddress!)
                            
                        }
                    }
                }
            }
        }
    }
}



