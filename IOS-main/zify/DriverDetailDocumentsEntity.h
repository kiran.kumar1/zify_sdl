//
//  DriverDetailDocumentsEntity.h
//  zify
//
//  Created by Anurag Rathor on 23/04/18.
//  Copyright © 2018 zify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DriverDetailDocumentsEntity : NSObject
@property (nonatomic,strong) NSString *drivingLicenceNum;
@property (nonatomic,strong) NSString *idCardNum;
@property (nonatomic,strong) NSString *idCardType;
@property (nonatomic,strong) NSNumber *isVehicleImgUploaded;
@property (nonatomic,strong) NSString *vehicleImgUrl;
@property (nonatomic,strong) NSString *vehicleInsuranceNum;
@property (nonatomic,strong) NSString *vehicleModel;
@property (nonatomic,strong) NSString *vehicleRegistrationNum;
-(id)initWithDictionary:(NSDictionary *)infoDict;
@end
