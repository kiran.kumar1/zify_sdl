//
//  AppDelegate.h
//  zify
//
//  Created by Kevin Vishal on 30/01/15.
//  Copyright (c) 2015 zify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "zify-Prefix.pch"
#import "UIView+iOS.h"
#import "LocalityInfo.h"
#import "DriveRoute.h"
#import "UIAlertController+Blocks.h"
#import "FirebaseEventClass.h"
#import "TutorialShownDB.h"
#import "UserOfferRideRequest.h"
#import <MoEngage/MoEngage.h>
#import "BankAccountDetails.h"
#import "BankAccountRequest.h"
#import "StepSlider.h"
#import "WSCoachMarksView.h"
#import "MPCoachMarks.h"
#import <OneSignal/OneSignal.h>
#import "UserNotification.h"
#import "PushNotificationView.h"
#import "LocalityAutoSuggestionController.h"
#import "MenuViewController.h"
//#import "NotificationsController.h"
#import "RideRequestsController.h"
#import "MessageHandler.h"
//#import "ForgotPasswordController.h" 
#import "LocalityInfo.h"
#import "UserSearchData.h"
#import "UserDataManager.h"
#import "UniversalAlert.h"
#import "GenericRequest.h"
#import "UserRegisterRequest.h"
#import "ZenParkSignInRequest.h"
#import "UIBarButtonItem+Badge.h"
#import "GetFDJpointsRequest.h"
#import "GetZenParkpointsRequest.h"
#import "ChatManager.h"
#import "LocalNotificationManager.h"
#import "CurrentRideLocationTracker.h"


#import "CurrentTripNPendingRatingResponse.h"
#import "AppUtilites.h"
//#import "DriveInvoiceController.h"
//#import "RideInvoiceController.h"
#import "CurrentDriveController.h"
#import "RideDriveActionRequest.h"
#import "LocalisationConstants.h"
#import "TripsTabController.h"
#import "AccountController.h"
#import "UserProfileController.h"
#import "AppViralityHelper.h"
#import "UserIdentityCardSaveRequest.h"
#import "DriverDetailEntity.h"
#import "LoginRequest.h"
#import "OnboardFlowHelper.h"
#import "FBUserLoginRequest.h"
#import "FacebookLogin.h"
#import "VerifyMobileRequest.h"
#import "SingleCharPasswordView.h"
#import "DBInterface.h"
#import "SearchRideViewController.h"
#import "MainContentController.h"
#import "FBCheckUserRequest.h"
#import "FacebookLogin.h"
#import "UserUploadMultiData.h"
#import "VehicleDetailUpdateRequest.h"
#import "UserRegisterRequest.h"
#import <FreshchatSDK.h>
#import "TextFieldWithDone.h"
#import "PasswordChangeRequest.h"
#import "RazorpayPaymentPersistRequest.h"
#import "StripePaymentPersistRequest.h"
#import "AccountRedeemMoneyController.h"
#import "RazorpayPaymentController.h"
#import "StripePaymentController.h"
#import "WalletRechargeStatement.h"
#import "WalletTransactionStatement.h"
#import "ImageSelectionViewer.h"
#import "ImageHelper.h"
#import "TripDriveRiderDetail.h"
#import "ContactUserListController.h"
#import "RouteInfoView.h"
#import "UserCallRequest.h"
#import "UpcomingRideDetailCell.h"
#import "DBUserDataInterface.h"
#import <SDWebImage/SDWebImageDownloader.h>
#import "MapContainerView.h"
#import "TripRide.h"
#import "MessageHandler.h"
#import "CurrentRideLocationTracker.h"
#import "CountryCode.h"
#import "BankAccountDetailsCell.h"
#import "UserGetDriveDeepLinkUrlRequest.h"
#import <TrueSDK/TrueSDK.h>
#import "PushRideRequest.h"
#import "TripRatingsRequest.h"
#import "PushRideRequestView.h"
#import <SmartDeviceLink/SmartDeviceLink.h>

@import Firebase;
@import GoogleMaps;

#define IS_IPHONE_4_APP ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON )
#define IS_IPHONE_6_APP            ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS_APP       ([[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_IPAD_APP       ([[UIScreen mainScreen] bounds].size.height == 1024.0)
#define IS_IPHONE_5_APP ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )



@interface AppDelegate : UIResponder <UIApplicationDelegate,OSSubscriptionObserver,UNUserNotificationCenterDelegate>{
    UIView *bgViewForLocationsDenied;

}

@property (strong,nonatomic) UIWindow *window;
@property(nonatomic,strong) NSMutableDictionary *paramsDict;
@property(nonatomic,strong) LocalityInfo *homeAddress, *officeAddress;
@property(nonatomic,strong)DriveRoute *homeToOffDriveRoute,*oFFToHomeDriveRoute;
@property(nonatomic,strong) NSString *emailStrFromFB;
@property(nonatomic,strong) NSString *zenparkAuthToken;
@property(nonatomic,strong) GMSMapView *googleMapObject;
@property(nonatomic,strong) NSString *emailStr;
@property(nonatomic,strong) NSArray *servicesCountriesArray;
@property(nonatomic,strong) CountryCode *userRegion;
@property(nonatomic,strong) SDLAlert *alertObj;


-(void)showLoginController;
-(void)showUserHomeController;
- (UIViewController*) topMostController;
+(void)applyVerticalGradient:(UIView *)view;
+ (nonnull AppDelegate *) getAppDelegateInstance;
+(CGFloat)SCREEN_WIDTH;
+(CGFloat)SCREEN_HEIGHT;
-(void)showTravelPrefAfterSignUpOnly;

-(void)moveTOTravelPrefenceScreen:(UIViewController *)vc;
+(UINavigationController *)createFeedbackNavController:(UIViewController *)fromVc;
-(UIImage *)getNewImageWithOldImage:(UIImage *)oldImage withNewColor:(UIColor *)newColor;

-(TutorialShownDB *)getTutorialsDBObject;
-(void)updateTutorialDBObject:(TutorialShownDB *)obj;
@property(nonnull,nonatomic,strong) UIViewController *topNavController;
@property(nonnull,nonatomic,strong) NSString *lastDeptTIme;
@property(nonnull,nonatomic,strong) NSDateFormatter *timeFormatter;
@property(nonnull,nonatomic,strong) NSString *travelTypeStr;
@property(nonatomic,assign) BOOL isForTrueCall;
@property(nonatomic, strong) FIRRemoteConfig *remoteConfig;
@property(nonatomic,strong) PushRideRequest *notificationInfoFromPush;
@property(nonatomic,strong) PushRideRequestView *notificationViewFromAppdelegate;

-(void)showRootViewController;
-(void)setTimeIn24HoursFormat;
-(void)setTimeIn12HoursFormat;
-(void)showLocationAccessScreen:(UIViewController *)superViewController;
-(void)removeLocationAccessScreenIfEsists;
-(NSString *)converDateToISOStandard:(NSString *)choosenDateInStrFormat;
-(NSString *)converDateToISOStandardForSearch:(NSString *)choosenDateInStrFormat;
-(NSString *)converDateToISOStandardForRideRequest:(NSString *)choosenDateInStrFormat;
-(NSString *)convertDOBFROMServiceTOReqFormat;
- (void)shareDriveDeepLinkIdWithFriends:(NSString *)driveDeepLinkUrl fromScreen:(UIViewController *)vc;
-(void)deleteNsUserDefaults;
-(NSString *)displayTripAmountInCommaSeparator:(id)numberStr;
-(void)showViewForIdCardUpload;
-(void) showIDCardUploadScreen;
-(void)loadImageOfferSplashScreen:(NSString *)strOfferImgUrl isShowUpdatedAlert:(BOOL)isShowAppUpdateAlert;

- (void)isShowUpdatedAlert;
-(void)callApiForAcceptRideRequestFromPushNotification;
- (UIViewController*) topMostController1;

-(void) addMoEngageMethodInitializersWithApplication:(UIApplication *)application andWithLaunchOptions:(NSDictionary *)launchOptions;
-(void)setbackgroungdImageForNavigatuionBar:(UINavigationBar *)bar;
@end

