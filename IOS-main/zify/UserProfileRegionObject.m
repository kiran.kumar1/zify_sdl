//
//  UserProfileRegionObject.m
//  zify
//
//  Created by Anurag Rathor on 12/07/19.
//  Copyright © 2019 zify. All rights reserved.
//

#import "UserProfileRegionObject.h"


@implementation UserProfileRegionObject

+(RKObjectMapping*)getUserProfileRegionObjectMapping{
    RKObjectMapping *obbj = [RKObjectMapping mappingForClass:[self class]];
    [obbj addAttributeMappingsFromArray:@[@"countryCode", @"countryName",@"currency",@"distanceUnit", @"isdCode",@"languageISOCode", @"perKmRate", @"timezone"]];
    return obbj;
}

@end
